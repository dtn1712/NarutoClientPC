﻿using System;

namespace src.lib
{
    public static class Converter
    {
        public static byte ConvertSbyteToByte(sbyte var)
        {
            if (var > 0)
                return (byte) var;

            return (byte) (var + 256);
        }

        public static byte[] ConvertSbyteToByte(sbyte[] var)
        {
            var temp = new byte[var.Length];
            for (var i = 0; i < var.Length; i++)
            {
                if (var[i] > 0)
                    temp[i] = (byte) var[i];

                else temp[i] = (byte) (var[i] + 256);
            }

            return temp;
        }

        public static sbyte[] ConvertByteToSbyte(byte[] var)
        {
            var result = new sbyte[var.Length];
            for (var i = 0; i < var.Length; i++)
            {
                result[i] = unchecked((sbyte) var[i]);
            }
            return result;
        }
        
        public static sbyte ConvertByteToSbyte(byte var)
        {
            return unchecked((sbyte) var);
        }

        public static byte[] ConvertIntToBytes(int intValue)
        {
            byte[] intBytes = BitConverter.GetBytes(intValue);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(intBytes);
            byte[] result = intBytes;
            return result;
        }
    }
}