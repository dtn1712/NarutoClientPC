using System;
using System.Collections;
using src.main;
using UnityEngine;

namespace src.lib
{
    public class FontSys
    {

        private int space;
        private Image imgFont;
        private string strFont;
        private int[][] fImages;
        public int yAddFont;


        public Font myFont;
        private int height;
        private int wO;

        public int cl1, cl2;
        public Color color1 = Color.white;
        public Color color2 = Color.gray;
        public sbyte id;

        public FontSys(string name, sbyte ID, int cl1, int size)
        {
            name += "_" + size;
            if ((ID >= 10 && ID <= 18) || (ID >= 20 && ID <= 24))
            {
                if (TCanvas.TileZoom == 1)
                    this.yAddFont = 0;
            }
            this.yAddFont = 0 * TCanvas.TileZoom;
            if (size == 6)
                this.yAddFont = 2 * TCanvas.TileZoom;
            if (ID == 11)
                this.yAddFont = -1 * TCanvas.TileZoom;
            if (ID == 12)
                this.yAddFont = -1 * TCanvas.TileZoom;
            if (ID == 49)
            {
                this.yAddFont = 3 * TCanvas.TileZoom;
            }
            if (size == 7)
                this.yAddFont = 1 * TCanvas.TileZoom;

            if (size == 8)
                this.yAddFont = 4 * TCanvas.TileZoom;
            if (TCanvas.TileZoom == 1) this.yAddFont -= 2;
            this.id = ID;
            this.cl1 = cl1;
            name = "FontSys/x" + MGraphics.zoomLevel + "/" + name;

            myFont = (Font) UnityEngine.Resources.Load(name);
            this.color1 = SetColor(this.cl1);
            this.color2 = SetColor(this.cl2);
            wO = GetWidthExactOf("o");
        }


        public static Color SetColor(int rgb)
        {
            int blue = rgb & 0xFF;
            int green = (rgb >> 8) & 0xFF;
            int red = (rgb >> 16) & 0xFF;
            float b = (float) blue / 256;
            float g = (float) green / 256;
            float r = (float) red / 256;
            Color cl = new Color(r, g, b);
            return cl;
        }


        private void SetTypePaint(MGraphics g, string st, int x, int y, int align)
        {
            x -= 1;
            this.color1 = SetColor(this.cl1);
            this.color2 = SetColor(this.cl2);
            _drawString(g, st, x, y, align);
        }


        public void DrawString(MGraphics g, string st, int x, int y, int align)
        {
            SetTypePaint(g, st, x, (y + yAddFont * MGraphics.zoomLevel), align);
        }


        public int GetWidth(string s)
        {
            return GetWidthExactOf(s) / MGraphics.zoomLevel;
        }

        private int GetWidthExactOf(string s)
        {
            try
            {
                //if null return default font(arial)

                var style = new GUIStyle {font = myFont};
                var content = new GUIContent {text = s};
                var dem = style.CalcSize(content);
                var dem222 = (int) dem.x + 20;
                return (int) dem222;
            }
            catch (Exception e)
            {
                return GetWidthNotExactOf(s);
            }
        }

        private int GetWidthNotExactOf(string s)
        {
            return s.Length * wO;
        }

        public int GetHeight()
        {
            if (height > 0) return (height / MGraphics.zoomLevel);
            var style = new GUIStyle {font = myFont};
            try
            {
                height = (int) (style.CalcSize(new GUIContent("Adg")).y) + 2;
            }
            catch (Exception e)
            {
                Debug.LogError("FAIL GET HEIGHT " + e.StackTrace);
                height = 20;
            }
            return (height / MGraphics.zoomLevel);
        }

        private void _drawString(MGraphics g, string st, int x0, int y0, int align)
        {
            var style = new GUIStyle(GUI.skin.label) {font = myFont};
            float x = 0, y = 0;
            switch (align)
            {
                case 0:
                    x = x0;
                    y = y0;
                    style.alignment = TextAnchor.UpperLeft;
                    break;
                case 1:
                    x = x0 - GameCanvas.w * 2;
                    y = y0;
                    style.alignment = TextAnchor.UpperRight;
                    break;
                case 2:
                case 3:
                    x = x0 - GameCanvas.w;
                    y = y0;
                    style.alignment = TextAnchor.UpperCenter;
                    break;
            }
            var wstr = GetWidth(st);
            style.normal.textColor = color1;
            g.drawString(st, (int) x, (int) y, style, wstr);
        }
    }
}