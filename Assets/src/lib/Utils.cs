﻿using System;
using src.model;
using src.network;
using UnityEngine;

namespace src.lib
{
    public static class Utils
    {
        public static sbyte[] ReadByteArray(Message msg)
        {
            try
            {
                var lengh = msg.Reader().Available();
                var data = new sbyte[lengh];
                msg.Reader().Read(ref data);
                return data;
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
            return null;
        }


        public static string Replace(String text, String regex, String replacement)
        {
            var sBuffer = new System.Text.StringBuilder();
            var pos = 0;
            while ((pos = text.IndexOf(regex, StringComparison.Ordinal)) != -1)
            {
                sBuffer.Append(MSystem.substring(text, 0, pos) + replacement);
                text = MSystem.substring(text, pos + regex.Length);
            }
            sBuffer.Append(text);
            return sBuffer.ToString();
        }

        public static string NumberToString(String number)
        {
            string value = "", value1 = "";
            if (number.Equals(""))
                return value;

            if (number[0] == '-')
            {
                value1 = "-";
                number = MSystem.substring(number, 1);
            }
            for (int i = number.Length - 1; i >= 0; i--)
            {
                if ((number.Length - 1 - i) % 3 == 0
                    && (number.Length - 1 - i) > 0)
                    value = number[i] + "." + value;
                else
                    value = number[i] + value;
            }
            return value1 + value;
        }

        public static string[] Split(String original, String separator)
        {
            var nodes = new Vector();
            var index = original.IndexOf(separator, StringComparison.Ordinal);
            while (index >= 0)
            {
                nodes.addElement(MSystem.substring(original, 0, index));
                original = MSystem.substring(original, index + separator.Length);
                index = original.IndexOf(separator, StringComparison.Ordinal);
            }
            nodes.addElement(original);
            string[] result = new string[nodes.size()];
            if (nodes.size() > 0)
            {
                for (int loop = 0; loop < nodes.size(); loop++)
                {
                    result[loop] = (String) nodes.elementAt(loop);
                }
            }
            return result;
        }


        public static int Distance(int x1, int y1, int x2, int y2)
        {
            return Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
        }

        private static int Sqrt(int a)
        {
            int x, x1;

            if (a <= 0)
                return 0;
            x = (a + 1) / 2;
            do
            {
                x1 = x;
                x = x / 2 + a / (2 * x);
            } while (CRes.abs(x1 - x) > 1);
            return x;
        }


        public static sbyte[] ReadByteArray(Message msg, int leng)
        {
            try
            {
                sbyte[] data = new sbyte[leng];
                msg.Reader().Read(ref data);
                return data;
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
            return null;
        }
    }
}