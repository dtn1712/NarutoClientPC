using System.Collections;
using src.main;
using src.model;
using UnityEngine;

namespace src.lib
{
    public class MGraphics
    {
        public static int HCENTER = 1;
        public static int VCENTER = 2;
        public static int LEFT = 4;
        public static int RIGHT = 8;
        public static int TOP = 16;
        public static int BOTTOM = 32;
        private float r, g, b, a;
        public int clipX, clipY, clipW, clipH;
        bool isClip, isTranslate = true;
        int translateX, translateY;
        public static int zoomLevel = 1;

        public const int TRANS_MIRROR_ROT180 = 1;
        public const int TRANS_MIRROR_ROT270 = 4;
        public const int TRANS_MIRROR_ROT90 = 7;
        public const int TRANS_ROT180 = 3;
        public const int TRANS_ROT270 = 6;
        public const int TRANS_ROT90 = 5;
        public static Hashtable cachedTextures = new Hashtable();
        public static int addYWhenOpenKeyBoard;

        void cache(string key, Texture value)
        {
            if (cachedTextures.Count > 400)
            {
                cachedTextures.Clear();
            }
            if (value.width * value.height < GameCanvas.w * GameCanvas.h)
                cachedTextures.Add(key, value);
        }

        public void translate(int tx, int ty)
        {
            tx *= zoomLevel;
            ty *= zoomLevel;
            translateX += tx;
            translateY += ty;
            isTranslate = true;
            if (translateX == 0 && translateY == 0) isTranslate = false;
        }

        public int getTranslateX()
        {
            return translateX / zoomLevel;
        }

        public int getTranslateY()
        {
            return (translateY / zoomLevel) + addYWhenOpenKeyBoard;
        }

        private int clipTX, clipTY;

        public void setClip(int x, int y, int w, int h)
        {
            x *= zoomLevel;
            y *= zoomLevel;
            w *= zoomLevel;
            h *= zoomLevel;
            clipTX = translateX;
            clipTY = translateY;
            clipX = x;
            clipY = y;
            clipW = w;
            clipH = h;
            isClip = true;
        }

        public void disableBlending()
        {
            a = 256f;
        }

        public void drawLine(int x1, int y1, int x2, int y2)
        {
            for (int ii = 0; ii < zoomLevel; ii++)
            {
                _drawLine(x1 + ii, y1 + ii, x2 + ii, y2 + ii);
                if (ii > 0)
                {
                    _drawLine(x1 + ii, y1, x2 + ii, y2);
                    _drawLine(x1, y1 + ii, x2, y2 + ii);
                }
            }
        }

        private Material lineMaterial;

        public void CreateLineMaterial()
        {
            if (!lineMaterial)
            {
            }
        }

        public void _drawLine(int x1, int y1, int x2, int y2)
        {
            if (y1 == y2)
            {
                if (x1 > x2)
                {
                    int tem = x2;
                    x2 = x1;
                    x1 = tem;
                }
                fillRect(x1, y1, x2 - x1, 1);
                return;
            }
            if (x1 == x2)
            {
                if (y1 > y2)
                {
                    int tem = y2;
                    y2 = y1;
                    y1 = tem;
                }
                fillRect(x1, y1, 1, y2 - y1);
                return;
            }

            x1 *= zoomLevel;
            y1 *= zoomLevel;
            x2 *= zoomLevel;
            y2 *= zoomLevel;
            if (isTranslate)
            {
                x1 += translateX;
                y1 += translateY;
                x2 += translateX;
                y2 += translateY;
            }
            Texture2D rgb_texture;
            string key = "dl" + r + g + b;
            rgb_texture = (Texture2D) cachedTextures[key];
            if (rgb_texture == null)
            {
                rgb_texture = new Texture2D(1, 1);
                Color rgb_color = new Color(r, g, b);
                rgb_texture.SetPixel(0, 0, rgb_color);
                rgb_texture.Apply();
                cache(key, rgb_texture);
            }
            Vector2 start = new Vector2(x1, y1);
            Vector2 end = new Vector2(x2, y2);
            Vector2 d = end - start;
            float a = Mathf.Rad2Deg * Mathf.Atan(d.y / d.x);
            if (d.x < 0)
                a += 180;
            int width2 = (int) Mathf.Ceil(1 / 2);
            GUIUtility.RotateAroundPivot(a, start);
            //=============================
            int cx = 0, cy = 0, cw = 0, ch = 0;
            if (isClip)
            {
                cx = clipX;
                cy = clipY;
                cw = clipW;
                ch = clipH;
                if (isTranslate)
                {
                    cx += clipTX;
                    cy += clipTY;
                }
            }
            if (isClip)
                GUI.BeginGroup(new Rect(cx, cy, cw, ch));
            Graphics.DrawTexture(new Rect(start.x - cx, start.y - width2 - cy, d.magnitude, 1), rgb_texture);
            if (isClip)
                GUI.EndGroup();

            GUIUtility.RotateAroundPivot(-a, start);
        }

        public void drawRect(int x, int y, int w, int h)
        {
            int xx = 1;
            fillRect(x, y, w, xx);
            fillRect(x, y, xx, h);
            fillRect(x + w, y, xx, h + 1);
            fillRect(x, y + h, w + 1, xx);
        }

        public void fillRect(int x, int y, int w, int h)
        {
            x *= zoomLevel;
            y *= zoomLevel;
            w *= zoomLevel;
            h *= zoomLevel;
            if (w < 0 || h < 0)
                return;
            if (isTranslate)
            {
                x += translateX;
                y += translateY;
            }
            int ww = 1, hh = 1;
            Texture2D rgb_texture;
            string key = "fr" + ww + hh + r + g + b + a;
            rgb_texture = (Texture2D) cachedTextures[key];
            if (rgb_texture == null)
            {
                rgb_texture = new Texture2D(ww, hh);
                Color rgb_color = new Color(r, g, b, a);
                rgb_texture.SetPixel(0, 0, rgb_color);
                rgb_texture.Apply();
                cache(key, rgb_texture);
            }
            int cx = 0, cy = 0, cw = 0, ch = 0;
            if (isClip)
            {
                cx = clipX;
                cy = clipY;
                cw = clipW;
                ch = clipH;
                if (isTranslate)
                {
                    cx += clipTX;
                    cy += clipTY;
                }
            }
            if (isClip)
                GUI.BeginGroup(new Rect(cx, cy, cw, ch));
            GUI.DrawTexture(new Rect(x - cx, y - cy, w, h), rgb_texture);
            if (isClip)
                GUI.EndGroup();
        }

        public void fillRect(int x, int y, int w, int h, bool isClip)
        {
            x *= zoomLevel;
            y *= zoomLevel;
            w *= zoomLevel;
            h *= zoomLevel;
            if (w < 0 || h < 0)
                return;
            if (isTranslate)
            {
                x += translateX;
                y += translateY;
            }
            int ww = 1, hh = 1;
            Texture2D rgb_texture;
            string key = "fr" + ww + hh + r + g + b + a;
            rgb_texture = (Texture2D) cachedTextures[key];
            if (rgb_texture == null)
            {
                rgb_texture = new Texture2D(ww, hh);
                Color rgb_color = new Color(r, g, b, a);
                rgb_texture.SetPixel(0, 0, rgb_color);
                rgb_texture.Apply();
                cache(key, rgb_texture);
            }
            int cx = 0, cy = 0, cw = 0, ch = 0;
            if (isClip)
            {
                cx = clipX;
                cy = clipY;
                cw = clipW;
                ch = clipH;
                if (isTranslate)
                {
                    cx += clipTX;
                    cy += clipTY;
                }
            }
            if (isClip)
                GUI.BeginGroup(new Rect(cx, cy, cw, ch));
            GUI.DrawTexture(new Rect(x - cx, y - cy, w, h), rgb_texture);
            if (isClip)
                GUI.EndGroup();
        }

        public void setColor(int rgb)
        {
            int blue = rgb & 0xFF;
            int green = (rgb >> 8) & 0xFF;
            int red = (rgb >> 16) & 0xFF;
            b = (float) blue / 256;
            g = (float) green / 256;
            r = (float) red / 256;
            a = 255f;
        }

        private int currentBGColor;


        public void setColor(int rgb, float alpha)
        {
            //0.1f-0.9f
            int blue = rgb & 0xFF;
            int green = (rgb >> 8) & 0xFF;
            int red = (rgb >> 16) & 0xFF;
            b = (float) blue / 256;
            g = (float) green / 256;
            r = (float) red / 256;
            a = alpha / 100;
            ;
        }

        public void drawString(string s, int x, int y, GUIStyle style, int wString)
        {
            wString *= zoomLevel;
            if (isTranslate)
            {
                x += translateX;
                y += translateY;
            }
            int cx = 0, cy = 0, cw = 0, ch = 0;
            if (isClip)
            {
                cx = clipX;
                cy = clipY;
                cw = clipW;
                ch = clipH;
                if (isTranslate)
                {
                    cx += clipTX;
                    cy += clipTY;
                }
            }
            if (isClip)
                GUI.BeginGroup(new Rect(cx, cy, cw, ch));
            int aa = 0;
            if (wString > Screen.width)
                aa = wString;
            GUI.Label(new Rect(x - cx, y - cy - 4, Screen.width + aa, 100), s, style);
            if (isClip)
                GUI.EndGroup();
        }

        void UpdatePos(int anchor)
        {
            Vector2 cornerPos = new Vector2(0, 0);

            //overwrite the items position
            switch (anchor)
            {
                case 3: //VCENTER|HCENTER:
                    cornerPos = new Vector2(size.x / 2, size.y / 2);
                    break;
                case 20: //TOP|LEFT:
                    cornerPos = new Vector2(0, 0);
                    break;
                case 17: //TOP|HCENTER:
                    cornerPos = new Vector2(TScreen.width / 2, 0);
                    break;
                case 24: //TOP|RIGHT:
                    cornerPos = new Vector2(TScreen.width, 0);
                    break;
                case 6: //VCENTER|LEFT:
                    cornerPos = new Vector2(0, size.y / 2);
                    break;
                case 10: //VCENTER|RIGHT:
                    cornerPos = new Vector2(TScreen.width, TScreen.height / 2);
                    break;
                case 36: //BOTTOM|LEFT:
                    cornerPos = new Vector2(0, TScreen.height);
                    break;
                case 33: //BOTTOM|HCENTER:
                    cornerPos = new Vector2(TScreen.width / 2, TScreen.height);
                    break;
                case 40: //BOTTOM|RIGHT:
                    cornerPos = new Vector2(TScreen.width, TScreen.height);
                    break;
            }
            pos = cornerPos + relativePosition;
            rect = new Rect(pos.x - size.x * 0.5f, pos.y - size.y * 0.5f, size.x, size.y);
            pivot = new Vector2(rect.xMin + rect.width * 0.5f, rect.yMin + rect.height * 0.5f);
        }

        Vector2 pos = new Vector2(0, 0);
        Rect rect;
        Matrix4x4 matrixBackup;
        Vector2 pivot;
        public Vector2 size = new Vector2(128, 128);
        public Vector2 relativePosition = new Vector2(0, 0);

        public void drawRegion(MBitmap arg0, int x0, int y0, int w0, int h0, int arg5, int x, int y, int arg8)
        {
            x *= zoomLevel;
            y *= zoomLevel;
            x0 *= zoomLevel;
            y0 *= zoomLevel;
            w0 *= zoomLevel;
            h0 *= zoomLevel;
            _drawRegion(arg0.image, x0, y0, w0, h0, arg5, x, y, arg8);
        }

        public void drawRegion(MBitmap arg0, int x0, int y0, int w0, int h0, int arg5, int x, int y, int arg8,
            bool isUclip)
        {
            x *= zoomLevel;
            y *= zoomLevel;
            x0 *= zoomLevel;
            y0 *= zoomLevel;
            w0 *= zoomLevel;
            h0 *= zoomLevel;
            _drawRegion(arg0.image, x0, y0, w0, h0, arg5, x, y, arg8);
        }

        public void _drawRegionOpacity2(Image image, float x0, float y0, int w, int h, int transform, int x, int y,
            int anchor, int opacity)
        {
            if (image == null) return;
            if (isTranslate)
            {
                x += translateX;
                y += translateY;
            }
            Texture2D rgb_texture;
            string key = "dg" + x0 + y0 + w + h + transform + image.GetHashCode();
            rgb_texture = (Texture2D) cachedTextures[key];

            if (rgb_texture == null)
            {
                Image imgRegion = Image.createImage(image, (int) x0, (int) y0, w, h, transform);
                rgb_texture = imgRegion.texture;
                cache(key, rgb_texture);
            }

            //=============================

            int cx = 0, cy = 0, cw = 0, ch = 0;

            float nw = w;
            float nh = h;
            float tx = 0, ty = 0;

            if ((anchor & HCENTER) == HCENTER)
                tx -= nw / 2;
            if ((anchor & VCENTER) == VCENTER)
                ty -= nh / 2;
            if ((anchor & RIGHT) == RIGHT)
                tx -= nw;
            if ((anchor & BOTTOM) == BOTTOM)
                ty -= nh;

            x += (int) tx;
            y += (int) ty;

            if (isClip)
            {
                cx = clipX;
                cy = clipY;
                cw = clipW;
                ch = clipH;
                if (isTranslate)
                {
                    cx += clipTX;
                    cy += clipTY;
                }
            }
            if (isClip)
                GUI.BeginGroup(new Rect(cx, cy, cw, ch));
            GUI.color = new Color32(255, 255, 255, (byte) opacity);
            GUI.DrawTexture(new Rect(x - cx, y - cy, w, h), rgb_texture);
            GUI.color = new Color32(255, 255, 255, 255);
            if (isClip)
                GUI.EndGroup();
        }


        public void _drawRegion(Image image, float x0, float y0, int w, int h, int transform, int x, int y, int anchor)
        {
            if (image == null) return;
            if (isTranslate)
            {
                x += translateX;
                y += translateY;
            }

            x0 = (x0 < 0 ? 0 : x0);
            y0 = (y0 < 0 ? 0 : y0);

            w = (x0 + w > image.getRealImageWidth() ? (int) (image.getRealImageWidth() - x0) : w);
            h = (y0 + h > image.getRealImageHeight() ? (int) (image.getRealImageHeight() - y0) : h);

            float nw = w;
            float nh = h;
            float dx = 0;
            float dy = 0;
            float tx = 0, ty = 0;
            float f = 1, dxf = 0;
            int yyy = 1;
            if ((anchor & HCENTER) == HCENTER)
            {
                tx -= nw / 2;
            }
            if ((anchor & VCENTER) == VCENTER)
            {
                ty -= nh / 2;
            }
            if ((anchor & RIGHT) == RIGHT)
            {
                tx -= nw;
            }
            if ((anchor & BOTTOM) == BOTTOM)
            {
                ty -= nh;
            }
            if ((anchor & TOP) == TOP && (anchor & LEFT) == LEFT && transform == TRANS_ROT270)
            {
                tx = nh / 2 - nw / 2;
                ty = nw / 2 - nh / 2;
            }
            if ((anchor & BOTTOM) == BOTTOM && (anchor & LEFT) == LEFT && transform == TRANS_ROT270)
            {
                ty = -nw / 2 - nh / 2;
                tx = nh / 2 - nw / 2 + -1;
            }
            if ((anchor & BOTTOM) == BOTTOM && (anchor & LEFT) == LEFT && transform == TRANS_ROT180)
            {
                ty = -nh;
                tx = 0;
            }
            if ((anchor & TOP) == TOP && (anchor & RIGHT) == RIGHT && transform == TRANS_ROT90)
            {
                tx = (nh / 2 - nw / 2 - nh);
                ty = nw / 2 - nh / 2;
            }
            if ((anchor & BOTTOM) == BOTTOM && (anchor & RIGHT) == RIGHT && transform == TRANS_ROT90)
            {
                ty = -nw / 2 - nh / 2;
                tx = (nh / 2 - nw / 2 - nh);
            }
            x += (int) tx;
            y += (int) ty;
            int cx = 0, cy = 0, cw = 0, ch = 0;
            if (isClip)
            {
                cx = clipX;
                cy = clipY;
                cw = clipW;
                ch = clipH;
                if (isTranslate)
                {
                    cx += clipTX;
                    cy += clipTY;
                }
                Rect r1 = new Rect(x, y, w, h);
                Rect r2 = new Rect(cx, cy, cw, ch);
                Rect r3 = intersectRect(r1, r2);
                if (r3.width <= 0 || r3.height <= 0) return;
                nw = r3.width;
                nh = r3.height;
                dx = r3.x - r1.x;
                dy = r3.y - r1.y;
            }
            float fx = 0, fy = 0;
            if (transform == 2)
            {
                fx += nw;
                f = -1;
                if (isClip)
                {
                    if (cx > x)
                        dxf = -dx;
                    else if (cx + cw < x + w)
                        dxf = -(cx + cw - x - w);
                }
            }
            else if (transform == TRANS_MIRROR_ROT180)
            {
                yyy = -1;
                fy += nh;
            }
            else if (transform == TRANS_ROT180)
            {
                yyy = -1;
                fy += nh;
                f = -1;
                fx += nw;
            }
            int tempX = 0;
            int tempY = 0;
            if (transform == TRANS_ROT90 || transform == TRANS_ROT270 || transform == TRANS_MIRROR_ROT270 ||
                transform == TRANS_MIRROR_ROT90) // rotate 90, 180, 270 degree
            {
                matrixBackup = GUI.matrix;
                size = new Vector2(w, h);
                relativePosition = new Vector2(x, y);
                UpdatePos(3);
                if (transform == TRANS_ROT270)
                {
                    size = new Vector2(w, h - 1);
                    UpdatePos(3);
                    GUIUtility.RotateAroundPivot(0, pivot);
                }
                else if (transform == TRANS_ROT90)
                {
                    size = new Vector2(w, h - 1);
                    UpdatePos(3);
                    GUIUtility.RotateAroundPivot(90, pivot);
                }
                if (transform == TRANS_ROT270)
                {
                    GUIUtility.RotateAroundPivot(270, pivot);
                }
                else if (transform == TRANS_MIRROR_ROT270)
                {
                    size = new Vector2(w, h - 1);
                    UpdatePos(3);
                    GUIUtility.RotateAroundPivot(90, pivot);
                    yyy = -1;
                    fy += nh;
                }
                else if (transform == TRANS_MIRROR_ROT90)
                {
                    size = new Vector2(w, h - 1);
                    UpdatePos(3);
                    GUIUtility.RotateAroundPivot(270, pivot);
                    yyy = -1;
                    fy += nh;
                }
            }

            Graphics.DrawTexture(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy),
                image.texture,
                new Rect((float) (x0 + dx + dxf) / image.texture.width,
                    ((float) (image.texture.height - nh - (y0 + dy))) / image.texture.height,
                    (float) nw / image.texture.width, (float) nh / image.texture.height), 0, 0, 0, 0);
            if (transform == TRANS_ROT90 || transform == TRANS_ROT270 || transform == TRANS_MIRROR_ROT270 ||
                transform == TRANS_MIRROR_ROT90)
            {
                GUI.matrix = matrixBackup;
            }
        }

        public void drawRegionRotate(MBitmap arg0, float x0, float y0, int w, int h, int transform, int x, int y,
            int anchor, int rotate2)
        {
            if (arg0 == null || arg0.image == null) return;
            Image image = arg0.image;

            x *= zoomLevel;
            y *= zoomLevel;
            x0 *= zoomLevel;
            y0 *= zoomLevel;
            w *= zoomLevel;
            h *= zoomLevel;

            if (isTranslate)
            {
                x += translateX;
                y += translateY;
            }
            float nw = w;
            float nh = h;
            float dx = 0;
            float dy = 0;
            float tx = 0, ty = 0;
            float f = 1, dxf = 0;
            int yyy = 1;
            float fx = 0, fy = 0;
            int tempX = 0;
            int tempY = 0;
            if ((anchor & VCENTER) == VCENTER && (anchor & LEFT) == LEFT)
            {
                //x -= w / 2; //xoa giữa
                y -= h / 2; //xoa giữa
            }
            else if ((anchor & VCENTER) == VCENTER && (anchor & HCENTER) == HCENTER)
            {
                x -= w / 2; //xoa giữa
                y -= h / 2; //xoa giữa
            }
            matrixBackup = GUI.matrix;
            size = new Vector2(w, h);
            relativePosition = new Vector2(x, y);

            size = new Vector2(w, h);
            if ((anchor & VCENTER) == VCENTER && (anchor & LEFT) == LEFT)
            {
                UpdatePos(6);
            }
            else if ((anchor & VCENTER) == VCENTER && (anchor & HCENTER) == HCENTER)
            {
                UpdatePos(3); //3xoay ngay giua 20 xoay goc
            }

            GUIUtility.RotateAroundPivot(rotate2, pivot);

            Graphics.DrawTexture(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy),
                image.texture,
                new Rect((float) (x0 + dx + dxf) / image.texture.width,
                    ((float) (image.texture.height - nh - (y0 + dy))) / image.texture.height,
                    (float) nw / image.texture.width, (float) nh / image.texture.height), 0, 0, 0, 0);

            GUI.matrix = matrixBackup;
        }


        public void drawImage(MBitmap image, int x, int y, int anchor)
        {
            if (image == null)
                return;
            drawRegion(image, 0, 0, getImageWidth(image), getImageHeight(image), 0, x, y, anchor);
        }

        public void drawImage(MBitmap image, int x, int y, int anchor, bool isUClip)
        {
            if (image == null)
                return;
            drawRegion(image, 0, 0, getImageWidth(image), getImageHeight(image), 0, x, y, anchor);
        }

        public void reset()
        {
            isClip = false;
            isTranslate = false;
            translateX = 0;
            translateY = 0;
        }

        public Rect intersectRect(Rect r1, Rect r2)
        {
            float tx1 = r1.x;
            float ty1 = r1.y;
            float rx1 = r2.x;
            float ry1 = r2.y;
            float tx2 = tx1;
            tx2 += r1.width;
            float ty2 = ty1;
            ty2 += r1.height;
            float rx2 = rx1;
            rx2 += r2.width;
            float ry2 = ry1;
            ry2 += r2.height;
            if (tx1 < rx1) tx1 = rx1;
            if (ty1 < ry1) ty1 = ry1;
            if (tx2 > rx2) tx2 = rx2;
            if (ty2 > ry2) ty2 = ry2;
            tx2 -= tx1;
            ty2 -= ty1;
            if (tx2 < -30000) tx2 = -30000;
            if (ty2 < -30000) ty2 = -30000;
            return new Rect(tx1, ty1, (int) tx2, (int) ty2);
        }

        public static int getImageWidth(MBitmap image)
        {
            if (image == null) return 1;
            return getImageWidth(image.image);
        }

        public static int getImageHeight(MBitmap image)
        {
            if (image == null) return 1;
            return getImageHeight(image.image);
        }

        public static int getImageWidth(Image image)
        {
            if (image == null) return 1;
            return image.getWidth();
        }

        public static int getImageHeight(Image image)
        {
            if (image == null) return 1;
            return image.getHeight();
        }

        private void _drawRegionRectOpacity(Image image, float x0, float y0, int w, int h, int transform, int x, int y,
            int anchor, int wRect, int hRect, bool p)
        {
            if (image == null) return;
            if (isTranslate)
            {
                x += translateX;
                y += translateY;
            }
            Texture2D rgb_texture;
            string key = "dg" + x0 + y0 + w + h + transform + image.GetHashCode();
            rgb_texture = (Texture2D) cachedTextures[key];

            if (rgb_texture == null)
            {
                Image imgRegion = Image.createImage(image, (int) x0, (int) y0, w, h, transform);
                rgb_texture = imgRegion.texture;
                imgRegion = null;
                cache(key, rgb_texture);
            }

            //=============================

            int cx = 0, cy = 0, cw = 0, ch = 0;

            float nw = w;
            float nh = h;
            float tx = 0, ty = 0;

            if ((anchor & HCENTER) == HCENTER)
                tx -= nw / 2;
            if ((anchor & VCENTER) == VCENTER)
                ty -= nh / 2;
            if ((anchor & RIGHT) == RIGHT)
                tx -= nw;
            if ((anchor & BOTTOM) == BOTTOM)
                ty -= nh;

            x += (int) tx;
            y += (int) ty;

            if (isClip)
            {
                cx = clipX;
                cy = clipY;
                cw = clipW;
                ch = clipH;
                if (isTranslate)
                {
                    cx += clipTX;
                    cy += clipTY;
                }
            }
            if (isClip)
                GUI.BeginGroup(new Rect(cx, cy, cw, ch));
            GUI.color = new Color32(255, 255, 255, 100);
            GUI.DrawTexture(new Rect(x - cx, y - cy, w, h - hRect), rgb_texture);
            GUI.color = new Color32(255, 255, 255, 255);
            if (isClip)
                GUI.EndGroup();
        }

        private void _drawRegionRect(Image image, float x0, float y0, int w, int h, int transform, int x, int y,
            int anchor, int wRect, int hRect)
        {
            if (image == null) return;
            if (isTranslate)
            {
                x += translateX;
                y += translateY;
            }
            float nw = w;
            float nh = h;
            float dx = 0;
            float dy = 0;
            float tx = 0, ty = 0;
            float f = 1, dxf = 0;
            int yyy = 1;
            if ((anchor & HCENTER) == HCENTER)
            {
                tx -= nw / 2;
            }
            if ((anchor & VCENTER) == VCENTER)
            {
                ty -= nh / 2;
            }
            if ((anchor & RIGHT) == RIGHT)
            {
                tx -= nw;
            }
            if ((anchor & BOTTOM) == BOTTOM)
            {
                ty -= nh;
            }
            if ((anchor & TOP) == TOP && (anchor & LEFT) == LEFT && transform == TRANS_ROT270)
            {
                tx = nh / 2 - nw / 2 - 1;
                ty = nw / 2 - nh / 2;
            }
            if ((anchor & BOTTOM) == BOTTOM && (anchor & LEFT) == LEFT && transform == TRANS_ROT270)
            {
                ty = -nw / 2 - nh / 2;
                tx = nh / 2 - nw / 2 + -1;
            }
            if ((anchor & TOP) == TOP && (anchor & RIGHT) == RIGHT && transform == TRANS_ROT90)
            {
                tx = (nh / 2 - nw / 2 - nh);
                ty = nw / 2 - nh / 2;
            }
            if ((anchor & BOTTOM) == BOTTOM && (anchor & RIGHT) == RIGHT && transform == TRANS_ROT90)
            {
                ty = -nw / 2 - nh / 2;
                tx = (nh / 2 - nw / 2 - nh);
            }
            x += (int) tx;
            y += (int) ty;
            int cx = 0, cy = 0, cw = 0, ch = 0;
            if (isClip)
            {
                cx = clipX;
                cy = clipY;
                cw = clipW;
                ch = clipH;
                if (isTranslate)
                {
                    cx += clipTX;
                    cy += clipTY;
                }
                Rect r1 = new Rect(x, y, w, h);
                Rect r2 = new Rect(cx, cy, cw, ch);
                Rect r3 = intersectRect(r1, r2);
                if (r3.width <= 0 || r3.height <= 0) return;
                nw = r3.width;
                nh = r3.height;
                dx = r3.x - r1.x;
                dy = r3.y - r1.y;
            }
            float fx = 0, fy = 0;
            if (transform == 2)
            {
                fx += nw;
                f = -1;
                if (isClip)
                {
                    if (cx > x)
                        dxf = -dx;
                    else if (cx + cw < x + w)
                        dxf = -(cx + cw - x - w);
                }
            }
            else if (transform == TRANS_MIRROR_ROT180)
            {
                yyy = -1;
                fy += nh;
            }
            else if (transform == TRANS_ROT180)
            {
                yyy = -1;
                fy += nh;
                f = -1;
                fx += nw;
            }
            int tempX = 0;
            int tempY = 0;
            if (transform == TRANS_ROT90 || transform == TRANS_ROT270 || transform == TRANS_MIRROR_ROT270 ||
                transform == TRANS_MIRROR_ROT90) // rotate 90, 180, 270 degree
            {
                matrixBackup = GUI.matrix;
                size = new Vector2(w, h);
                relativePosition = new Vector2(x, y);
                UpdatePos(3);
                if (transform == TRANS_ROT270)
                {
                    size = new Vector2(w, h - 1);
                    UpdatePos(3);
                    GUIUtility.RotateAroundPivot(0, pivot);
                }
                else if (transform == TRANS_ROT90)
                {
                    size = new Vector2(w, h - 1);
                    UpdatePos(3);
                    GUIUtility.RotateAroundPivot(90, pivot);
                }
                if (transform == TRANS_ROT270)
                {
                    GUIUtility.RotateAroundPivot(270, pivot);
                }
                else if (transform == TRANS_MIRROR_ROT270)
                {
                    size = new Vector2(w, h - 1);
                    UpdatePos(3);
                    GUIUtility.RotateAroundPivot(90, pivot);
                    yyy = -1;
                    fy += nh;
                }
                else if (transform == TRANS_MIRROR_ROT90)
                {
                    size = new Vector2(w, h - 1);
                    UpdatePos(3);
                    GUIUtility.RotateAroundPivot(270, pivot);
                    yyy = -1;
                    fy += nh;
                }
            }

            Graphics.DrawTexture(new Rect(x + dx + fx + tempX, y + dy + tempY + fy, nw * f, nh * yyy - hRect),
                image.texture,
                new Rect((float) (x0 + dx + dxf) / image.texture.width,
                    ((float) (image.texture.height - nh - (y0 + dy))) / image.texture.height,
                    (float) nw / image.texture.width, (float) nh / image.texture.height), 0, 0, 0, 0);
            if (transform == TRANS_ROT90 || transform == TRANS_ROT270 || transform == TRANS_MIRROR_ROT270 ||
                transform == TRANS_MIRROR_ROT90)
            {
                GUI.matrix = matrixBackup;
            }
        }

        public void drawRegionScalse(MBitmap arg0, int x0, int y0, int w0, int h0, int arg5, int x, int y, int arg8,
            bool isUclip, float tileScale)
        {
            x *= zoomLevel;
            y *= zoomLevel;
            x0 *= zoomLevel;
            y0 *= zoomLevel;
            w0 *= zoomLevel;
            h0 *= zoomLevel;
            _drawRegion(arg0.image, x0, y0, w0, h0, arg5, x, y, arg8);
        }

        public void drawRegion(MBitmap img, int x0, int y0, int w0, int h0, int arg5, int x, int y, int anchor,
            bool isUclip, int opacity)
        {
            _drawRegionOpacity2(img.image, x0, y0, w0, h0, arg5, x, y, anchor, opacity);
        }
    }
}