using System.IO;
using System.Text;
using UnityEngine;

namespace src.lib
{
	public class MyWriter
	{
		public sbyte[] buffer = new sbyte[2048];
		private int posWrite = 0;
		private int length = 2048;
	
		public  void writeSByteUncheck( sbyte value)
		{
			buffer[posWrite++] = value;
		}
	
		public  void writeShort( short value)
		{
			checkLength(2);
			for (int i = 1; i >= 0; i--) {
				writeSByteUncheck((sbyte) (value >> i * 8));
			}
		}
	
		public  void writeUTF( string value)
		{
		
			Encoding unicode = Encoding.Unicode;
		
			Encoding utf8 = Encoding.GetEncoding(65001);
			
			byte[] tempC = unicode.GetBytes(value);
		
			byte[] tempC2 = Encoding.Convert(unicode, utf8, tempC);
		
		
			writeShort((short)tempC2.Length);
		
			checkLength(tempC2.Length);
		
			for( int i = 0; i < tempC2.Length; i++)
			{
				sbyte sb = unchecked((sbyte)tempC2[i]);
				writeSByteUncheck(sb);	
			} 
		}
	
		public sbyte[] getData()
		{
			if ( posWrite <= 0)
				return null;
		
			sbyte[] temp = new sbyte[posWrite];
			for(int i = 0; i < posWrite; i++)
			{
				temp[i] = buffer[i];
			}
		
			return temp;
		}
	
		public void checkLength(int ltemp)
		{
			if ( posWrite + ltemp > length)
			{
				sbyte[] temp = new sbyte[length + 1024 + ltemp];
			
				for( int i = 0; i < length; i++)
				{
					temp[i] = buffer[i];	
				}
			
				buffer = null;
				buffer = temp;
			
				length += (1024 + ltemp);
			}
		}
	
		public void Close()
		{
			buffer = null;	
		}

	}
}


