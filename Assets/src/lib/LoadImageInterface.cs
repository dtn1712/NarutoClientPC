using src.main;

namespace src.lib
{
    public class LoadImageInterface
    {
        //mini game
        public static MBitmap imgMiniGame;
        //chat
        public static MBitmap[] imgInside = new MBitmap[13];

        public static MBitmap[] imgLine = new MBitmap[4];
        public static MBitmap[] imgEmo = new MBitmap[64];
        public static MBitmap[] imgOnline = new MBitmap[2];
        public static MBitmap imgPopup, imgGocChat;
        public static MBitmap imgfocusActor; //

        //tab main inventory
        public static MBitmap[] imgConerGui = new MBitmap[3]; //background tab

        public static MBitmap imgBoxName, imgBoxName_1; //background chat
        public static MBitmap closeTab;
        public static MBitmap btnTab;
        public static MBitmap btnTabFocus;
        public static MBitmap ImgItem;
        public static MBitmap imgFocusSelectItem, imgFocusSelectItem0, imgFocusSelectItem1;


        //frame sub info
        public static MBitmap imgsub_frame_info;

        public static MBitmap imgsub_frame_info_2;
        public static MBitmap imgsub_info_conner;
        public static MBitmap imgsub_stone_01;

        public static MBitmap img_use;
        public static MBitmap img_use_focus;

        //bg trang bi
        public static MBitmap img_bg_char_wearing;

        //quest
        public static MBitmap imgShortQuest, imgShortQuest_Close;

        public static MBitmap[] coins = new MBitmap[2];

        //gui main
        public static MBitmap imgAttack, imgAttack_1, imgCharacter_info, imgBlood, imgMp, imgExp;

        //gui frame chat
        public static MBitmap imgChatConner,
            imgChatRec,
            imgChatRec_1,
            imgChatConner_1,
            imgChatButton,
            imgChatButtonFocus,
            btnSendChat,
            btnSendChatFocus;

        public static MBitmap imgCharPoint;

        //gui move
        public static MBitmap imgMoveCenter, imgMoveNormal, imgMoveFocus, imgName, imgLineTrade;

        // gui eight gate
        public static MBitmap[] imgHumanEightGate = new MBitmap[25];

        public static MBitmap[] imgBGMenuIcon = new MBitmap[10];

        public static MBitmap charPic, smallTest;

        public static MBitmap bgQuestConner, bgQuestLine;
        public static MBitmap bgQuestConnerFocus, bgQuestLineFocus;

        //sub menu
        public static MBitmap[] imgSubMenu = new MBitmap[3];

        //icon  menu
        public static MBitmap imgCharIcon,
            imgMissionIcon,
            imgShopIcon,
            imgContactIcon,
            imgImproveIcon,
            imgTradeIcon,
            imgFriendIcon,
            imgTeamIcon,
            imgLogout,
            imgIconDeoCo,
            imgSettingIcon,
            imgClanIcon;

        // image textfield

        public static MBitmap imgTf;
        public static MBitmap imgTf0, imgTf1, imgTf2, imgTf3;

        // image Screen 
        public static MBitmap imgTatus, imgRock, imgTrangtri, khu, btnChangeFocus0, btnChangeFocus1;

        public static MBitmap imgXinCho, imgMap, chardie, bongChar, iconpk, icn_Mail;
        public static MBitmap imgIconFb0, imgIconFb1, imgIconGoogle0, imgIconGoogle1, btnLogin0, btnLogin1;
        public static MBitmap imgCheckSetting, imgLa;

        public static MBitmap[] imgHotKey = new MBitmap[2];
        public static MBitmap[] changeFocus = new MBitmap[2];
        public static MBitmap[] btnUseHP = new MBitmap[2];
        public static MBitmap[] btnUseMP = new MBitmap[2];
        public static MBitmap[] btnChatMap = new MBitmap[2];
        public static MBitmap imgGoc, bt_Plus;
        public static MBitmap[] imgStar = new MBitmap[2];
        public static MBitmap[] imgStoneMove = new MBitmap[2];

        public static MBitmap[] list_thunder4 = new MBitmap[12];
        public static MBitmap[] list_thunder5 = new MBitmap[12];
        public static MBitmap[] list_thunder6 = new MBitmap[12];
        public static MBitmap[] list_thunder7 = new MBitmap[12];
        public static MBitmap[] list_thunder8 = new MBitmap[12];


        public static void loadImage()
        {
            // load img textfield
            imgLa = GameCanvas.loadImage("/eff/cobay.png");
            imgTf = GameCanvas.loadImage("/screen/tf.png");
            imgTf0 = GameCanvas.loadImage("/screen/tf/0.png");
            imgTf1 = GameCanvas.loadImage("/screen/tf/1.png");
            imgTf2 = GameCanvas.loadImage("/screen/tf/2.png");
            imgTf3 = GameCanvas.loadImage("/screen/tf/3.png");

            //load Screen 
            imgRock = GameCanvas.loadImage("/screen/rockchar1.png");
            imgTatus = GameCanvas.loadImage("/screen/statue.png");
            imgTrangtri = GameCanvas.loadImage("/screen/trangtri.png");


            bongChar = GameCanvas.loadImage("/GuiNaruto/bong.png");
            khu = GameCanvas.loadImage("/GuiNaruto/khu.png");
            imgIconDeoCo = GameCanvas.loadImage("/GuiNaruto/iconMap/pk.png");
            imgMiniGame = GameCanvas.loadImage("/GuiNaruto/minigameX2.png");
            //load sub menu
            for (var i = 0; i < 3; i++)
                imgSubMenu[i] = GameCanvas.loadImage("/GuiNaruto/iconMap/frame_sub_icon_" + (i + 1) + ".png");
            imgfocusActor = GameCanvas.loadImage("/GuiNaruto/focusActor.png");
            imgCharIcon = GameCanvas.loadImage("/GuiNaruto/iconMap/char.png");
            imgMissionIcon = GameCanvas.loadImage("/GuiNaruto/iconMap/mission.png");
            imgShopIcon = GameCanvas.loadImage("/GuiNaruto/iconMap/shop.png");
            imgContactIcon = GameCanvas.loadImage("/GuiNaruto/iconMap/contact.png");
            imgImproveIcon = GameCanvas.loadImage("/GuiNaruto/iconMap/improve.png");
            imgTradeIcon = GameCanvas.loadImage("/GuiNaruto/iconMap/trade.png");
            imgLogout = GameCanvas.loadImage("/GuiNaruto/iconMap/logout.png");
            imgSettingIcon = GameCanvas.loadImage("/GuiNaruto/iconMap/setting.png");
            imgClanIcon = GameCanvas.loadImage("/GuiNaruto/iconMap/clan.png");
            imgFriendIcon = GameCanvas.loadImage("/GuiNaruto/iconMap/bestfriend.png");
            imgTeamIcon = GameCanvas.loadImage("/GuiNaruto/iconMap/team.png");

            bgQuestConner = GameCanvas.loadImage("/GuiNaruto/quest/listConner.png");
            bgQuestLine = GameCanvas.loadImage("/GuiNaruto/quest/list.png");

            bgQuestConnerFocus = GameCanvas.loadImage("/GuiNaruto/quest/listConner_focus.png");
            bgQuestLineFocus = GameCanvas.loadImage("/GuiNaruto/quest/list_focus.png");
            for (var i = 0; i < coins.Length; i++)
                coins[i] = GameCanvas.loadImage("/GuiNaruto/myseft/coins" + (i + 1) + ".png");
            smallTest = GameCanvas.loadImage("/GuiNaruto/Small20.png");
            charPic = GameCanvas.loadImage("/GuiNaruto/char_pic.png");
            imgLineTrade = GameCanvas.loadImage("/GuiNaruto/Trade/line.png");
            imgName = GameCanvas.loadImage("/GuiNaruto/Trade/name.png");
            imgGocChat = GameCanvas.loadImage("/gocChat.png");
            for (var i = 0; i < imgOnline.Length; i++)
                imgOnline[i] = GameCanvas.loadImage("/GuiNaruto/" + (i == 0 ? "offline" : "online") + ".png");
            for (var i = 0; i < imgBGMenuIcon.Length; i++)
                imgBGMenuIcon[i] = GameCanvas.loadImage("/GuiNaruto/MapMenu/map_icon_" + (i + 1) + ".png");


            //load image icon chat
            for (var i = 0; i < imgEmo.Length; i++)
                imgEmo[i] = GameCanvas.loadImage("/iconChat/emo" + i + ".png");

            //quest
            imgShortQuest = GameCanvas.loadImage("/GuiNaruto/show.png");
            imgShortQuest_Close = GameCanvas.loadImage("/GuiNaruto/hide.png");

            //tab inventory main
            imgConerGui[0] = GameCanvas.loadImage("/GuiNaruto/frame_conner.png");
            imgConerGui[1] = GameCanvas.loadImage("/GuiNaruto/frame.png");
            imgConerGui[2] = GameCanvas.loadImage("/GuiNaruto/frame_01.png");
            imgBoxName = GameCanvas.loadImage("/GuiNaruto/box_name.png");
            imgBoxName_1 = GameCanvas.loadImage("/GuiNaruto/box_name_1.png");
            closeTab = GameCanvas.loadImage("/GuiNaruto/close.png");
            btnTab = GameCanvas.loadImage("/GuiNaruto/button_tab.png");
            btnTabFocus = GameCanvas.loadImage("/GuiNaruto/button_TabFocus.png");
            ImgItem = GameCanvas.loadImage("/GuiNaruto/item_box.png");

            //tab info sub
            imgsub_frame_info = GameCanvas.loadImage("/GuiNaruto/sub_frame_info.png");
            imgsub_frame_info_2 = GameCanvas.loadImage("/GuiNaruto/sub_frame_info_2.png");
            imgsub_info_conner = GameCanvas.loadImage("/GuiNaruto/sub_info_conner.png");
            imgsub_stone_01 = GameCanvas.loadImage("/GuiNaruto/sub_stone_01.png");

            //use
            img_use = GameCanvas.loadImage("/GuiNaruto/button2.png");
            img_use_focus = GameCanvas.loadImage("/GuiNaruto/button1.png");

            //main gui
            imgCharacter_info = GameCanvas.loadImage("/GuiNaruto/charBoard/character_info.png");
            imgBlood = GameCanvas.loadImage("/GuiNaruto/charBoard/blood.png");
            imgMp = GameCanvas.loadImage("/GuiNaruto/charBoard/mana.png");
            imgExp = GameCanvas.loadImage("/GuiNaruto/charBoard/exp.png");
            ;

            imgAttack = GameCanvas.loadImage("/GuiNaruto/attack.png");
            imgAttack_1 = GameCanvas.loadImage("/GuiNaruto/attack_1.png");

            imgFocusSelectItem = GameCanvas.loadImage("/GuiNaruto/focus.png");
            imgFocusSelectItem0 = GameCanvas.loadImage("/GuiNaruto/focusitem.png"); //focus0
            imgFocusSelectItem1 = GameCanvas.loadImage("/GuiNaruto/focus1.png");

            imgChatConner = GameCanvas.loadImage("/GuiNaruto/frameChat/chat_frame.png");
            imgChatRec = GameCanvas.loadImage("/GuiNaruto/frameChat/chat_frame_1.png");
            imgChatConner_1 = GameCanvas.loadImage("/GuiNaruto/frameChat/chat_frame_2.png");
            imgChatRec_1 = GameCanvas.loadImage("/GuiNaruto/frameChat/chat_frame3.png");
            imgChatButton = GameCanvas.loadImage("/GuiNaruto/frameChat/chat_button2.png");
            imgChatButtonFocus = GameCanvas.loadImage("/GuiNaruto/frameChat/chat_button1.png");
//		img_bg_char_wearing=GameCanvas.loadImage("/GuiNaruto/frameChat/screentab6.png");
            btnSendChat = GameCanvas.loadImage("/GuiNaruto/frameChat/btnChat.png");
            btnSendChatFocus = GameCanvas.loadImage("/GuiNaruto/frameChat/btnChatFocus.png");
            // move
            imgMoveCenter = GameCanvas.loadImage("/GuiNaruto/movecenter.png");
            imgMoveNormal = GameCanvas.loadImage("/GuiNaruto/move.png");
            imgMoveFocus = GameCanvas.loadImage("/GuiNaruto/movefocus.png");

            // hunmen eight gate 
            imgXinCho = GameCanvas.loadImage("/GuiNaruto/imgXinCho.png");
            imgMap = GameCanvas.loadImage("/GuiNaruto/imgMap.png");
            chardie = GameCanvas.loadImage("/GuiNaruto/chardie.png");
            iconpk = GameCanvas.loadImage("/GuiNaruto/iconpk.png");
            icn_Mail = GameCanvas.loadImage("/GuiNaruto/icn_Mail.png");
            btnChangeFocus0 = GameCanvas.loadImage("/GuiNaruto/btnChangeFocus0.png"); //
            btnChangeFocus1 = GameCanvas.loadImage("/GuiNaruto/btnChangeFocus1.png"); //

            imgIconFb0 = GameCanvas.loadImage("/GuiNaruto/iconFb0.png"); //
            imgIconFb1 = GameCanvas.loadImage("/GuiNaruto/iconFb1.png"); //
            imgIconGoogle0 = GameCanvas.loadImage("/GuiNaruto/iconGoogle0.png"); //
            imgIconGoogle1 = GameCanvas.loadImage("/GuiNaruto/iconGoogle1.png"); //
            btnLogin0 = GameCanvas.loadImage("/GuiNaruto/btnLogin0.png"); //
            btnLogin1 = GameCanvas.loadImage("/GuiNaruto/btnLogin1.png"); //
            imgCheckSetting = GameCanvas.loadImage("/GuiNaruto/check.png"); //

            for (var i = 0; i < imgHotKey.Length; i++)
                imgHotKey[i] = GameCanvas.loadImage("/GuiNaruto/Pad_" + i + ".png"); //
            for (var i = 0; i < changeFocus.Length; i++)
                changeFocus[i] = GameCanvas.loadImage("/GuiNaruto/changeFocus_" + i + ".png"); //
            for (var i = 0; i < btnUseHP.Length; i++)
                btnUseHP[i] = GameCanvas.loadImage("/GuiNaruto/btnHp_" + i + ".png"); //
            for (var i = 0; i < btnUseMP.Length; i++)
                btnUseMP[i] = GameCanvas.loadImage("/GuiNaruto/btnMp_" + i + ".png"); //
            for (var i = 0; i < btnChatMap.Length; i++)
                btnChatMap[i] = GameCanvas.loadImage("/GuiNaruto/IcnChat_" + i + ".png"); //
            imgGoc = GameCanvas.loadImage("/GuiNaruto/imgGoc.png");
            for (var i = 0; i < imgStar.Length; i++)
                imgStar[i] = GameCanvas.loadImage("/GuiNaruto/star" + i + ".png");
            bt_Plus = GameCanvas.loadImage("/GuiNaruto/human/bt_Plus_0.png");
            for (var i = 0; i < imgStoneMove.Length; i++)
                imgStoneMove[i] = GameCanvas.loadImage("/GuiNaruto/da" + (i + 1) + ".png");

            //Load effect thunder
            for (var i = 0; i < list_thunder4.Length; i++)
                list_thunder4[i] = GameCanvas.loadImage("/eff/thunder/cap4_" + (i + 1) + ".png");
            for (var i = 0; i < list_thunder5.Length; i++)
                list_thunder5[i] = GameCanvas.loadImage("/eff/thunder/cap5_" + (i + 1) + ".png");
            for (var i = 0; i < list_thunder6.Length; i++)
                list_thunder6[i] = GameCanvas.loadImage("/eff/thunder/cap6_" + (i + 1) + ".png");
            for (var i = 0; i < list_thunder7.Length; i++)
                list_thunder7[i] = GameCanvas.loadImage("/eff/thunder/cap7_" + (i + 1) + ".png");
            for (var i = 0; i < list_thunder8.Length; i++)
                list_thunder8[i] = GameCanvas.loadImage("/eff/thunder/cap8_" + (i + 1) + ".png");
        }
    }
}