using System;
using System.Threading;
using UnityEngine;

namespace src.lib
{
    public static class Rms
    {
        private const int INTERVAL = 5;
        private const int MAXTIME = 500;
        
        public static string autoSetting = "autosetting";

        private static int status; // 0: nothing, 1: working, 2: waitingSave, 3: waitingLoad
        private static sbyte[] data;
        private static string filename;

        public static void saveRMS(string filename, sbyte[] data)
        {
            if (Thread.CurrentThread.Name == Main.MainThreadName)
                __saveRMS(filename, data);
            else
                _saveRMS(filename, data);
        }

        public static sbyte[] loadRMS(string filename)
        {
            return Thread.CurrentThread.Name == Main.MainThreadName ? __loadRMS(filename) : _loadRMS(filename);
        }

        public static string loadRMSString(string fileName)
        {
            sbyte[] data = Rms.loadRMS(fileName);
            if (data == null)
                return null;
            DataInputStream dis = new DataInputStream(data);
            try
            {
                string t = dis.ReadUtf();
                dis.Close();
                return t;
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
            return null;
        }

        public static void saveRMSString(string filename, string data)
        {
            var dos = new MyWriter();
            try
            {
                dos.writeUTF(data);
                saveRMS(filename, dos.getData());
                dos.Close();
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        private static void _saveRMS(string filename, sbyte[] data)
        {
            if (status != 0)
            {
                Debug.LogError("Cannot save RMS " + filename + " because current is saving " + Rms.filename);
                return;
            }
            Rms.filename = filename;
            Rms.data = data;
            status = 2; // waiting save
            int i = 0;
            while (i < MAXTIME)
            {
                Thread.Sleep(INTERVAL);
                if (status == 0) break; //save done
                i++;
            }
            if (i == MAXTIME) Debug.LogError("TOO LONG TO SAVE RMS " + filename);
        }

        private static sbyte[] _loadRMS(string filename)
        {
            if (status != 0)
            {
                Debug.LogError("Cannot load RMS " + filename + " because current is loading " + Rms.filename);
                return null;
            }
            Rms.filename = filename;
            Rms.data = null;
            status = 3; // waiting load
            int i = 0;
            while (i < MAXTIME)
            {
                Thread.Sleep(INTERVAL);
                if (status == 0) break; //load done
                i++;
            }
            if (i == MAXTIME) Debug.LogError("TOO LONG TO LOAD RMS " + filename);
            return Rms.data;
        }

        public static void update()
        {
            if (Rms.status == 2)
            {
                Rms.status = 1;
                __saveRMS(Rms.filename, Rms.data);
                Rms.status = 0;
            }
            else if (Rms.status == 3)
            {
                Rms.status = 1;
                Rms.data = __loadRMS(Rms.filename);
                Rms.status = 0;
            }
        }

        public static int loadRMSInt(string file)
        {
            sbyte[] b = loadRMS(file);
            return b == null ? -1 : (int) b[0];
        }

        public static void saveRMSInt(string file, int x)
        {
            try
            {
                saveRMS(file, new sbyte[] {(sbyte) x});
            }
            catch (Exception e)
            {
            }
        }

        public static string GetiPhoneDocumentsPath()
        {
            if (Main.isPC)
                return Application.persistentDataPath;
            string path = Application.dataPath.Substring(0, Application.dataPath.Length - 5);
            path = path.Substring(0, path.LastIndexOf('/'));
            return path + "/Documents";
        }

        private static void __saveRMS(string filename, sbyte[] data)
        {
            var f = new System.IO.FileStream(GetiPhoneDocumentsPath() + "/" + filename, System.IO.FileMode.Create);
            f.Write(ArrayCast.Cast(data), 0, data.Length);
            f.Flush();
            f.Close();
        }

        private static sbyte[] __loadRMS(string filename)
        {
            try
            {
                var f = new System.IO.FileStream(GetiPhoneDocumentsPath() + "/" + filename, System.IO.FileMode.Open);
                byte[] data = new byte[f.Length];
                f.Read(data, 0, data.Length);
                f.Close();
                return ArrayCast.Cast(data);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static void saveRMSData(string filename, sbyte[] data)
        {
            if (Thread.CurrentThread.Name == Main.MainThreadName)
                __saveRMS(filename, data);
            else
                _saveRMS(filename, data);
        }
    }
}