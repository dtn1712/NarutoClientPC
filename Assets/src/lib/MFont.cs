using System;

namespace src.lib
{
    public class MFont
    {
        public const int LEFT = 0;
        public const int RIGHT = 1;
        public const int CENTER = 2;
        public const int RED = 0;

        public const int YELLOW = 1;
        public const int GREEN = 2;
        public const int FATAL = 3;
        public const int MISS = 4;
        public const int ORANGE = 5;
        public const int ADDMONEY = 6;
        public const int MISS_ME = 7;
        public const int FATAL_ME = 8;
        public const int HP = 9;
        public const int MP = 10;
        public const int XP = 11;
        public const int WHITE = 12;
        public const int NAMEITEMXANH = 13;
        public const int NAMEITEMTRANG = 14;

        private int space;
        private int height;
        private Image imgFont;
        private string strFont;
        private int[][] fImages;

        private string pathImage;
        private FontSys fontSys;

        public MFont(string namefont, sbyte id, int size, int color)
        {
            size = (size == 10 ? 11 : size);
            fontSys = new FontSys(namefont, id, color, size);
        }

        public int GetHeight()
        {
            return fontSys.GetHeight();
        }

        public int GetWidth(string st)
        {
            if (st == null) return 1;
            st = st.Replace(" ", ".");
            return fontSys?.GetWidth(st) ?? 5;
        }

        public void DrawString(MGraphics g, string st, int x, int y, int align, MFont font)
        {
            if (font != null)
            {
                font.DrawString(g, st, x + 1, y, align);
                font.DrawString(g, st, x, y + 1, align);
            }
            DrawString(g, st, x, y, align);
        }

        public void DrawString(MGraphics g, string st, int x, int y, int align)
        {
            if (st == null) return;
            fontSys.DrawString(g, st, x * MGraphics.zoomLevel, y * MGraphics.zoomLevel, align);
        }

        private void _drawStringShadown(MGraphics g, string st, int x, int y, int align)
        {
            if (st == null) return;
            fontSys.DrawString(g, st, x, y, align);
        }

        public Vector SplitFontVector(string src, int lineWidth)
        {
            var lines = new Vector();
            var line = "";
            for (var i = 0; i < src.Length; i++)
            {
                if (src[i] == '\n' || src[i] == '\b')
                {
                    lines.addElement(line);
                    line = "";
                }
                else
                {
                    line += src[i];
                    if (GetWidth(line) > lineWidth)
                    {
                        // System.out.println(line);
                        int j = 0;
                        for (j = line.Length - 1; j >= 0; j--)
                        {
                            if (line[j] == ' ')
                            {
                                break;
                            }
                        }
                        if (j < 0)
                            j = line.Length - 1;
                        lines.addElement(MSystem.substring(line, 0, j));
                        i = i - (line.Length - j) + 1;
                        line = "";
                    }
                    if (i == src.Length - 1 && !line.Trim().Equals(""))
                        lines.addElement(line);
                }
            }
            return lines;
        }

        public string[] SplitFontArray(string src, int lineWidth)
        {
            var lines = SplitFontVector(src, lineWidth);
            var arr = new string[lines.size()];
            for (var i = 0; i < lines.size(); i++)
            {
                arr[i] = lines.elementAt(i).ToString();
            }
            return arr;
        }

        public void DrawStringBorder(MGraphics g, string st, int x, int y, int align)
        {
            if (fontSys != null)
            {
                x *= MGraphics.zoomLevel;
                y *= MGraphics.zoomLevel;
                var hsv = new float[3];
                hsv[2] *= 0.4f;
                var colorDark = 0x1C1C1C; //Color.(hsv);
                var colorr = fontSys.cl1;
                fontSys.cl1 = colorDark;
                fontSys.color1 = FontSys.SetColor(colorDark);


                _drawStringShadown(g, st, x - 1, y - 1, align);
                _drawStringShadown(g, st, x, y - 1, align);
                _drawStringShadown(g, st, x + 1, y - 1, align);
                _drawStringShadown(g, st, x + 1, y, align);
                _drawStringShadown(g, st, x + 1, y + 1, align);
                _drawStringShadown(g, st, x, y + 1, align);
                _drawStringShadown(g, st, x - 1, y + 1, align);
                _drawStringShadown(g, st, x - 1, y, align);
                fontSys.cl1 = colorr;
                fontSys.color1 = FontSys.SetColor(colorr);

                DrawString(g, st, x / MGraphics.zoomLevel, y / MGraphics.zoomLevel, align);
            }
            else DrawString(g, st, x, y, align);
        }

        public void DrawStringShadown(MGraphics g, string st, int x, int y, int align)
        {
            if (fontSys != null)
            {
                var hsv = new float[3];
                hsv[2] *= 0.4f;
                const int colorDark = 0x1C1C1C; //Color.(hsv);
                var colorr = fontSys.cl1;
                fontSys.cl1 = colorDark;
                fontSys.color1 = FontSys.SetColor(colorDark);
                _drawStringShadown(g, st, x * MGraphics.zoomLevel + 1, y * MGraphics.zoomLevel + 1, align);


                fontSys.cl1 = colorr;
                fontSys.color1 = FontSys.SetColor(colorr);

                DrawString(g, st, x, y, align);
            }
            else DrawString(g, st, x, y, align);
        }
    }
}