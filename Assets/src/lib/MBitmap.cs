﻿using System;

namespace src.lib
{
    public class MBitmap
    {
        public Image image;
        public int w, h, width, height;
        private string key;
        
        public int GetWidth()
        {
            if (image == null) return 1;
            return image==null?2:image.getWidth();
        }
        public int GetHeight()
        {
            if (image == null) return 1;
            return image == null ? 2 : image.getHeight();
        }
        public MBitmap(Image imgg) {
            image = imgg;
        }

        public MBitmap() {}

        public void cleanImg()
        {
            try
            {
                if (image != null)
                    image.texture = null;
                MBitmap imgg = (MBitmap)Image.listImgLoad.get(key);
                if (imgg != null)
                {
                    Image.listImgLoad.Remove(key);
                    Image.listImgLoad.remove(imgg);
                }
            }
            catch (Exception e)
            {
            }
        }
    }
}

