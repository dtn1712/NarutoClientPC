using UnityEngine;

namespace src.lib
{
    public abstract class TCanvas
    {
        public static int realWidth, realHeight;
        public static int TileZoom = 1;

        private void checkZoomLevel(int w, int h)
        {
            w = Screen.width;
            h = Screen.height;
            

            if (w >= 1280 && h >= 960)
            {
                w = w / 3;
                h = h / 3;
                MGraphics.zoomLevel = 4;
            }
            else if (w >= 1280 && h >= 960)
            {
                w = w / 3;
                h = h / 3;
                MGraphics.zoomLevel = 4;
            }
            if (w >= 720 && h >= 960)
            {
                w = w / 3;
                h = h / 3;
                MGraphics.zoomLevel = 3;
            }
            else if (w >= 960 && h >= 720)
            {
                w = w / 3;
                h = h / 3;
                MGraphics.zoomLevel = 3;
            }
            if (w > 400 && h > 600)
            {
                w = w / 2;
                h = h / 2;
                MGraphics.zoomLevel = 2;
            }
            else if (w > 600 && h > 400)
            {
                w = w / 2;
                h = h / 2;
                MGraphics.zoomLevel = 2;
            }
            Debug.Log(GetType().Name + "mGraphics.zoomLevel   " + MGraphics.zoomLevel + " w " + w + " h  " + h);
        }

        public TCanvas()
        {
//            realWidth = getWidthL();
//            realHeight = getHeightL();
            realWidth = getWidthL();
            realHeight = getHeightL();
            setFullScreenMode(true);
            checkZoomLevel(realWidth, realHeight);
        }

        public int getHeightL()
        {
            return Screen.height;
        }

        public int getWidthL()
        {
            return Screen.width;
        }

        public int getHeightz()
        {
            return (realHeight / MGraphics.zoomLevel) + (realHeight % MGraphics.zoomLevel == 0 ? 0 : 1);
        }

        public int getWidthz()
        {
            return (realWidth / MGraphics.zoomLevel) + (realWidth % MGraphics.zoomLevel == 0 ? 0 : 1);
        }

        public void setFullScreenMode(bool b)
        {
        }

        public bool hasPointerEvents()
        {
            return true;
        }

        public void pointerDragged(int x, int y)
        {
            x = x / MGraphics.zoomLevel;
            y = y / MGraphics.zoomLevel;
            onPointerDragged(x, y);
        }

        public void pointerPressed(int x, int y)
        {
            x = x / MGraphics.zoomLevel;
            y = y / MGraphics.zoomLevel;
            onPointerPressed(x, y);
        }

        public void pointerReleased(int x, int y)
        {
            x = x / MGraphics.zoomLevel;
            y = y / MGraphics.zoomLevel;
            onPointerReleased(x, y);
        }

        public virtual void onPointerDragged(int x, int y)
        {
        }

        public virtual void onPointerPressed(int x, int y)
        {
        }

        public virtual void onPointerReleased(int x, int y)
        {
        }
    }
}