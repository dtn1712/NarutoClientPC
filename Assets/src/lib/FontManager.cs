﻿using System;

namespace src.lib
{
    public class FontManager
    {
        private static readonly FontManager Instance = new FontManager();
        
        private FontManager() {}

        public static FontManager GetInstance()
        {
            return Instance;
        }
        
		public MFont tahoma_7b_redsmall;
		public MFont tahoma_7b_whitesmall; 
		public MFont tahoma_7b_whitesmall_size6; 
		public MFont tahoma_7b_red; // 1
		public MFont tahoma_7b_bluesmall;
		public MFont tahoma_7b_blue ; // 2
		public MFont tahoma_7b_white;// 3
		public MFont tahoma_7b_yellow ; // 4
		public MFont tahoma_7b_yellowMainTab ; // 4
		public MFont tahoma_7b_yellowsmall ; // 4
		public MFont tahoma_7b_dark ; // 5
		public MFont tahoma_7b_dark_small ; // 5
		public MFont tahoma_7b_popup ; // 5
		public MFont tahoma_7b_yellowsmall_size6 ; // 4
		public MFont tahoma_7b_violetsmall;
		public MFont tahoma_7b_black_small; // 5

		public MFont tahoma_7b_green2 ;// 6
		public MFont tahoma_7b_green ; // 7
		public MFont tahoma_7_button;
		public MFont tahoma_7b_greensmall;
		public MFont tahoma_7b_focus ;//8
		public MFont tahoma_7b_unfocus; //9
		public MFont tahoma_7; //10
		public MFont tahoma_7_blue1 ; //11
		public MFont tahoma_7_green2 ;//12
		public MFont tahoma_7_yellow;//13
		public MFont tahoma_7_yellow_xu;//13
		public MFont tahoma_7_grey;//14
		public MFont tahoma_7_red ;//15
		public MFont tahoma_7_blue ;//16
		public MFont tahoma_7_green ;//17
		public MFont tahoma_7_white ;//18
		public MFont tahoma_7_white_size6 ;//18
		public MFont tahoma_7_white_size7;//18
		//Font for charname show in Game by country Huy(18/08/2017)
		public MFont tahoma_7_brown_charname;
		public MFont tahoma_7_red_charname;
		public MFont tahoma_7_blue_charname;
		public MFont tahoma_7_green_charname;
		public MFont tahoma_7_yellow_charname;
		//End
		public MFont tahoma_7_yellow_tahoma;
		public MFont tahoma_8b;//19
		public MFont tahoma_8btf, tahoma_8btf_unfocus;
		public MFont number_yellow_7b_size6;
		public MFont number_red_7b_size6;
		public MFont number_gray;//23
		public MFont number_orange ;//24

		public MFont number_yellow, number_yellow_xp;
		public MFont number_red, number_red_hp;
		public MFont number_green, number_green_mp;
	
		public MFont bigNumber_red;//25
		public MFont bigNumber_While; // 26
		public MFont bigNumber_yellow ;//27	
		public MFont bigNumber_green ;//28
		public MFont bigNumber_orange;//29
		public MFont bigNumber_blue ; //30
	
		public MFont nameFontRed;
		public MFont nameFontYellow;
		public MFont nameFontGreen;
		public MFont tahoma_7_redsmall;
		public MFont tahoma_7_whitesmall;// 3
		public MFont tahoma_7_bluesmall;
		public MFont tahoma_7_yellowsmall;
		public MFont tahoma_7_greensmall;
		public MFont tahoma_7_xamsmall;
		public MFont tahoma_7_xam;
		public MFont tahoma_7_orange;
		public MFont tahoma_7b_cyansmall;
		public MFont tahoma_6_white;
		public MFont tahoma_10b;

	    private bool _isLoaded;
	    
		public void LoadBegin()
		{
			if (_isLoaded) return;
			
			tahoma_7b_red               = new MFont("tahoma_7b",1,9,0xff0000); // 1
			tahoma_7b_whitesmall        = new MFont("tahoma_7b",2,8,0xffffff); // 1
			tahoma_7b_whitesmall_size6  = new MFont("tahoma_7b",3,6,0xffffff); // 1
			tahoma_7b_blue             	= new MFont("tahoma_7b",3,9,0x637dff); // 2
			tahoma_7b_white             = new MFont("tahoma_7b",4,9,0xffffff); // 3
			tahoma_7b_yellow 			= new MFont("tahoma_7b", 5, 9, 0xf6e634); // 4
			tahoma_7b_yellowsmall       = new MFont("tahoma_7b",6,8,0xffe21e); // 4
			tahoma_7b_dark              = new MFont("tahoma_7b",7,9,0x532905); // 5
			tahoma_7b_dark_small        = new MFont("tahoma_7b",8,8,0x532905); // 5
			tahoma_7b_bluesmall 		= new MFont("tahoma_7b", 9, 8, 0x637dff); // 2
			tahoma_7b_redsmall          = new MFont("tahoma_7b",10,8,0xff0000); // 1
			tahoma_7b_yellowMainTab     = new MFont("tahoma_7b",11,9,0xffaa00); // 4
			tahoma_7b_popup 			= new MFont("UVNAiCap_R", 12, 9, 0x102B2F); // 5
			tahoma_7b_yellowsmall_size6 = new MFont("tahoma_7b",13,6,0xffe21e); // 4
		  
		  
			tahoma_7b_green2            = new MFont("tahoma_7b",14,9,0x005325); // 6
			tahoma_7b_green             = new MFont("tahoma_7b",15,9,0x00cc00); // 7
			tahoma_7b_focus             = new MFont("tahoma_7b",16,8,0x70b474); //8
			tahoma_7b_greensmall        = new MFont("tahoma_7b",17,8,0x00cc00); // 7
			tahoma_7_button             = new MFont("tahoma_7b",18,9,0x000000); //10
			  
			tahoma_7b_unfocus           = new MFont("tahoma_7b",19,8,0xffeacc); //9
			tahoma_7 					= new MFont("tahoma_7b", 20, 8, 0x000000); //10
			tahoma_7_blue1 				= new MFont("tahoma_7b", 21, 8, 0x00ffff); //11
			tahoma_7_green2 			= new MFont("tahoma_7b", 22, 8, 0x005325);//12
			tahoma_7_yellow 			= new MFont("tahoma_7b", 23, 8, 0xffff00);//13
			tahoma_7_yellow_xu 			= new MFont("tahoma_7b", 24, 8, 0xffff00);//13
			tahoma_7_grey 				= new MFont("tahoma_7b", 25, 8, 0x555555);//14
			tahoma_7_red				= new MFont("tahoma_7b", 26, 8, 0xff7777);//15
			tahoma_7_blue 				= new MFont("tahoma_7b", 27, 8, 0x0080ff);//16
			tahoma_7_green 				= new MFont("tahoma_7b", 28, 8, 0x20eb5f);//17 20eb5f
			tahoma_7_white 				= new MFont("tahoma_7b", 29, 8, 0xefebef);//18
			tahoma_7_white_size6 		= new MFont("tahoma_7b", 30, 6, 0xead69a);//18
			tahoma_7_white_size7 		= new MFont("tahoma_7b", 31, 7, 0xefebef);//18
			tahoma_8b                  	= new MFont("tahoma_7b",32,9,0xeecc66);//19
			tahoma_8btf                 = new MFont("tahoma_7b",33,8,0xeecc66);//19
			
			//Font for charname by country Huy(18/08/2017)
			tahoma_7_red_charname 		= new MFont("tahoma_7b", 29, 8, 0xFF6161);
			tahoma_7_blue_charname 		= new MFont("tahoma_7b", 29, 8, 0x84c2ff);
			tahoma_7_yellow_charname 	= new MFont("tahoma_7b", 29, 8, 0xf8ff8a);
			tahoma_7_green_charname 	= new MFont("tahoma_7b", 29, 8, 0x8affaf);
			tahoma_7_brown_charname 	= new MFont("tahoma_7b", 29, 8, 0xf5c000);
			//End
			tahoma_7_bluesmall          = new MFont("tahoma_7b",34,7,0x0080ff);//16
			tahoma_7_yellowsmall 		= new MFont("tahoma_7b", 35, 7, 0xffff00);//16

			tahoma_7_whitesmall 		= new MFont("tahoma_7b", 36, 7, 0xefebef);//18
			tahoma_7_redsmall 			= new MFont("tahoma_7b", 37, 7, 0xff0000);//15
			number_yellow_7b_size6 		= new MFont("UTMAZUKI", 38, 12, 0xffdf2f);//20
			 
		 
			number_yellow               = new MFont("tahoma_7b",39,12,0xffdf2f);//20
			number_red 					= new MFont("tahoma_7b", 40, 12, 0xff0000);//21
			number_green 				= new MFont("tahoma_7b", 42, 12, 0x3ef0e3);//22
			number_yellow_xp 			= new MFont("UTMAZUKI", 39, 12, 0xffdf2f);//20
			number_red_hp 				= new MFont("UTMAZUKI", 40, 12, 0xff0000);//21
			number_green_mp 			= new MFont("UTMAZUKI", 42, 12, 0x3ef0e3);//22

			number_red_7b_size6 		= new MFont("UTMAZUKI", 41, 12, 0xff0012);//21
			number_gray                 = new MFont("tahoma_7b",43,12,0x474747);//23
			number_orange               = new MFont("tahoma_7b",44,12,0xf59c38);//24
			tahoma_8btf_unfocus         = new MFont("tahoma_7b",45, 8, 0xffffff);//19

			tahoma_7b_violetsmall 		= new MFont("tahoma_7b", 46, 8, 0xd105e9); // 4
			tahoma_7b_black_small 		= new MFont("tahoma_7b", 47, 8, 0x000000); // 5
			tahoma_7_greensmall 		= new MFont("tahoma_7b", 48, 7, 0x20eb5f);//17 20eb5f

			tahoma_7_yellow_tahoma 		= new MFont("tahoma_7b", 49, 6, 0xffff00);//18
			tahoma_7_xamsmall 			= new MFont("tahoma_7b", 50, 7, 0x7c7b77);//17 20eb5f

			tahoma_7_xam 				= new MFont("tahoma_7b", 51, 8, 0x7c7b77);//18
			tahoma_7_orange 			= new MFont("tahoma_7b", 52, 8, 0xfa9168);//18
			tahoma_7b_cyansmall 		= new MFont("tahoma_7b", 55, 8, 0x00ffff); // 7
			tahoma_6_white 				= new MFont("tahoma_7b", 53, 6, 0xefebef);//18
			tahoma_10b 					= new MFont("tahoma_7b", 54, 10, 0xffff00);//18
		
			nameFontRed = tahoma_7b_red;
			nameFontYellow = tahoma_7_yellow;
			nameFontGreen = tahoma_7_green;

			_isLoaded = true;
		}
	    
	    public string[] Split(string original, string separator)
	    {
		    var nodes = new Vector();
		    var index = original.IndexOf(separator, StringComparison.Ordinal);
		    while (index >= 0)
		    {
			    nodes.addElement(MSystem.substring(original, 0, index));
			    original = MSystem.substring(original, index + separator.Length);
			    index = original.IndexOf(separator, StringComparison.Ordinal);
		    }
		    nodes.addElement(original);
		    var result = new string[nodes.size()];

		    if (nodes.size() <= 0) return result;

		    for (var loop = 0; loop < nodes.size(); loop++)
		    {
			    result[loop] = (string) nodes.elementAt(loop);
		    }
		    return result;
	    }
    }
}