using System.Threading;
using src.main;
using UnityEngine;

namespace src.lib
{
    public class Sound
    {
        private const int INTERVAL = 5;
        private const int MAXTIME = 100;
        public static int status, postem;
        private static string filenametemp;
        private static float volumetem;
        private static bool isNotPlay;

        public static AudioSource SoundBGLoop;

        public static AudioClip[] music;
        public static GameObject[] player;


        public static void init()
        {
            GameObject AudioObj = new GameObject();
            AudioObj.name = "Audio Player";
            AudioObj.transform.position = Vector3.zero;
            AudioObj.AddComponent<AudioListener>();
            SoundBGLoop = AudioObj.AddComponent<AudioSource>();
        }

        //----moi 
        public static int l1;

        public static void init(int[] musicID, int[] sID)
        {
            if (player != null || music != null) return;
            init();
            l1 = musicID.Length;
            player = new GameObject[musicID.Length + sID.Length];
            music = new AudioClip[musicID.Length + sID.Length];
            for (int i = 0; i < player.Length; i++)
            {
                string name = (i < l1 ? "/music/" + i : "/sound/" + (i - l1));
                getAssetSoundFile(name, i);
            }
            //  bMuzikDisable = false;
        }

        public static void playSound(int id, float volume)
        {
            play(id + l1, volume);
        }

        //----moi 
        private static void getAssetSoundFile(string fileName, int pos)
        {
            stop(pos);
            var t = Constants.RESOURCE_NAME + fileName;
            load(t, pos);
        }


        public static void stopMusic(int x)
        {
        }

        public static void play(int id, float volume)
        {
            if (isNotPlay) return;
            if (GameCanvas.isPlaySound)
                start(volume, id);
        }

        public static void pauseMusic()
        {
            SoundBGLoop.GetComponent<AudioSource>().Stop();
        }


        public static void playMus(int type, float vl, bool loop)
        {
            if (isNotPlay) return;
            vl -= 0.3f;
            if (vl <= 0)
                vl = 0.010f;
            playSoundBGLoop(type, vl);
        }

        private static void playSoundBGLoop(int id, float volume)
        {
            //	if (!bMuzikDisable) {
            if (!GameCanvas.isPlaySound) return;
            
            if (SoundBGLoop == null)
                return;
            if (isPlayingSoundBG(id))
            {
                SoundBGLoop.GetComponent<AudioSource>().clip = music[id];
                SoundBGLoop.GetComponent<AudioSource>().Play();
                return;
            }
            SoundBGLoop.GetComponent<AudioSource>().loop = true;
            SoundBGLoop.GetComponent<AudioSource>().clip = music[id];
            SoundBGLoop.GetComponent<AudioSource>().volume = volume;
            SoundBGLoop.GetComponent<AudioSource>().Play();
        }


        private static bool isPlayingSoundBG(int id)
        {
            return SoundBGLoop != null && SoundBGLoop.GetComponent<AudioSource>().isPlaying;
        }

        public static void load(string filename, int pos)
        {
            if (Thread.CurrentThread.Name == Main.MainThreadName)
            {
                music[pos] = (AudioClip) Resources.Load(filename, typeof(AudioClip));
                player[pos] = GameObject.Find("Camera");
                GameObject.Find("Camera").AddComponent<AudioSource>();
            }
            else
            {
                if (status != 0)
                {
                    Debug.LogError("CANNOT LOAD AUDIO " + filename + " WHEN LOADING " + filenametemp);
                    return;
                }
                filenametemp = filename;
                postem = pos;
                status = 2;
                int i = 0;
                while (i < MAXTIME)
                {
                    Thread.Sleep(INTERVAL);
                    if (status == 0)
                    {
                        break; // load done
                    }
                    i++;
                }
                if (i == MAXTIME)
                    Debug.LogError("TOO LONG FOR LOAD AUDIO " + filename);
                else
                    Debug.Log("Load Audio " + filename + " done in " + (i * INTERVAL) + "ms");
            }
        }


        public static void start(float volume, int pos)
        {
            if (Thread.CurrentThread.Name == Main.MainThreadName)
            {
                if (player[pos] == null)
                    return;
                
                player[pos].GetComponent<AudioSource>().PlayOneShot(music[pos], volume);
            }
            else
            {
                if (status != 0)
                {
                    Debug.LogError("CANNOT START AUDIO WHEN STARTING");
                    return;
                }
                volumetem = volume;
                postem = pos;
                status = 3;
                int i = 0;
                while (i < MAXTIME)
                {
                    Thread.Sleep(INTERVAL);
                    if (status == 0)
                        break; // start done
                    i++;
                }
                if (i == MAXTIME)
                    Debug.LogError("TOO LONG FOR START AUDIO");
                else
                    Debug.Log("Start Audio done in " + (i * INTERVAL) + "ms");
            }
        }

        public static void stop(int pos)
        {
            if (Thread.CurrentThread.Name == Main.MainThreadName)
            {
                if (player[pos] != null)
                    player[pos].GetComponent<AudioSource>().Stop();
            }
            else
            {
                if (status != 0)
                {
                    Debug.LogError("CANNOT STOP AUDIO WHEN STOPPING");
                    return;
                }
                postem = pos;
                status = 4;
                int i = 0;
                while (i < MAXTIME)
                {
                    Thread.Sleep(INTERVAL);
                    if (status == 0)
                        break; // start done
                    i++;
                }
                if (i == MAXTIME)
                    Debug.LogError("TOO LONG FOR STOP AUDIO");
                else
                    Debug.Log("Stop Audio done in " + (i * INTERVAL) + "ms");
            }
        }

    }
}