namespace src.lib
{
    public class MyReader
    {
        private sbyte[] buffer;
        private int posRead, posMark;

        private static string fileName;
        private static int status = 0;


        public MyReader() {}

        public MyReader(sbyte[] data)
        {
            buffer = data;
        }


        private sbyte ReadSByte()
        {
            if (posRead < buffer.Length)
                return buffer[posRead++];
            posRead = buffer.Length;
            return 0;
        }


        public sbyte ReadByte()
        {
            return ReadSByte();
        }

        public byte ReadUnsignedByte()
        {
            return ConvertSbyteToByte(ReadSByte());
        }

        public short ReadShort()
        {
            short res = 0;
            for (var i = 0; i < 2; i++)
            {
                res <<= 8;
                res |= (short) (0xff & buffer[posRead++]);
            }
            return res;
        }

        public int ReadInt()
        {
            var res = 0;
            for (var i = 0; i < 4; i++)
            {
                res <<= 8;
                res |= (0xff & buffer[posRead++]);
            }
            return res;
        }

        public long ReadLong()
        {
            long res = 0;
            for (var i = 0; i < 8; i++)
            {
                res <<= 8;
                res |= (0xff & buffer[posRead++]);
            }
            return res;
        }

        public bool ReadBool()
        {
            return ReadSByte() > 0;
        }


        public string ReadUtf()
        {
            var len = ReadShort();
            var temp = new byte[len];
            for (var i = 0; i < len; i++)
            {
                temp[i] = ConvertSbyteToByte(ReadSByte());
            }

            var utf = new System.Text.UTF8Encoding();
            return utf.GetString(temp);
        }

        public void Read(ref sbyte[] data)
        {
            if (data == null)
                return;

            for (var i = 0; i < data.Length; i++)
            {
                data[i] = ReadSByte();
                if (posRead > buffer.Length)
                    return;
            }
        }

        public int Available()
        {
            return buffer.Length - posRead;
        }

        private static byte ConvertSbyteToByte(sbyte var)
        {
            if (var > 0)
                return (byte) var;

            return (byte) (var + 256);
        }

        public static byte[] ConvertSbyteToByte(sbyte[] var)
        {
            var temp = new byte[var.Length];
            for (var i = 0; i < var.Length; i++)
            {
                if (var[i] > 0)
                    temp[i] = (byte) var[i];
                else 
                    temp[i] = (byte) (var[i] + 256);
            }

            return temp;
        }

        public void Close()
        {
            buffer = null;
        }
    }
}