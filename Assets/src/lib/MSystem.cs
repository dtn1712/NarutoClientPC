using System;
using UnityEngine;

namespace src.lib
{
    public class MSystem
    {
        public static long currentTimeMillis()
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long current = (DateTime.UtcNow.Ticks - epoch.Ticks) / TimeSpan.TicksPerMillisecond;
            return current;
        }

        public static long currentTimeMillis_()
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long current = (DateTime.UtcNow.Ticks - epoch.Ticks) / TimeSpan.TicksPerMillisecond;
            return current;
        }

        public static void gcc()
        {
            Resources.UnloadUnusedAssets();
            GC.Collect();
        }


        public static void my_Gc()
        {
            gcc();
        }


        public static string getPathRMS(string str)
        {
            string tempt = str;
            if (str.Contains("/"))
            {
                tempt = str.Replace("/", "");
            }
            return tempt;
        }

        public static string substring(string scr, int startIndex, int vitri)
        {
            try
            {
                if (scr == null)
                    return "";
                return scr.Substring(startIndex, vitri - startIndex);
            }
            catch (Exception e)
            {
                return scr.Substring(startIndex, 0);
            }
        }

        public static string substring(string scr, int vitri)
        {
            try
            {
                if (scr == null)
                    return "";
                return scr.Substring(vitri, scr.Length - (vitri));
            }
            catch (Exception e)
            {
                return scr.Substring(vitri, 0);
            }
        }

        public static string loadfile(string path)
        {
            var txt = (TextAsset) Resources.Load(path, typeof(TextAsset));
            var text = txt.text;
            return text;
        }
    }
}