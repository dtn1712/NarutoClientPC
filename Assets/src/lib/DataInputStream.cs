using System;
using System.Threading;
using UnityEngine;

namespace src.lib
{
    public class DataInputStream
    {
        private readonly MyReader _reader;

        public DataInputStream(string filename)
        {
            var ta = (TextAsset) Resources.Load("res" + filename, typeof(TextAsset));
            _reader = new MyReader(ArrayCast.Cast(ta.bytes));
        }


        public DataInputStream(sbyte[] data)
        {
            _reader = new MyReader(data);
        }

        public short ReadShort()
        {
            return _reader.ReadShort();
        }


        public int Read()
        {
            return _reader.ReadUnsignedByte();
        }


        public void Close()
        {
            _reader.Close();
        }

        public string ReadUtf()
        {
            return _reader.ReadUtf();
        }

        public sbyte ReadByte()
        {
            return _reader.ReadByte();
        }

        public int ReadUnsignedByte()
        {
            return ((byte) _reader.ReadByte());
        }

        public int Available()
        {
            return _reader.Available();
        }
    }
}