using System;
using UnityEngine;

namespace src.lib
{
    public class Image
    {

        public Texture2D texture = new Texture2D(1, 1);
        public static Image imgTemp;
        public static string filenametemp;
        public static byte[] datatemp;
        public static Image imgSrcTemp;
        public static int xtemp, ytemp, wtemp, htemp, transformtemp;
        public int w, h, width, height;
        public static int status;

        public static MyHashtable listImgLoad = new MyHashtable();
        // 0:free 
        // 1:working 
        // 2:waitingCreateEmptyImage 
        // 3:waitingCreateImageFromFileName
        // 4:waitingCreateImageFromArray
        // 5:waitingCreateImageFromPartImage

        public static Image createImage(string filename)
        {
            return _createImageByPNG(filename);
        }

        private static Image _createImageByPNG(string path)
        {
            try
            {
                Texture2D spr = Resources.Load(path) as Texture2D;
                if (spr == null)
                    throw new Exception("NULL POINTER EXCEPTION AT Image _createImageByPNG " + path);
                Image img = new Image();

                img.texture = spr;
                img.w = spr.width;
                img.h = spr.height;
                return img;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        //----Sprite-------
        private static Image createImage(byte[] imageData)
        {
            return __createImage(imageData);
        }

        public static Image createImage(Image src, int x, int y, int w, int h, int transform)
        {
            return __createImage(src, x, y, w, h, transform);
        }

        public static Image createImage(int w, int h)
        {
            return __createImage(w, h);
        }

        public static Image createImage(sbyte[] imageData, int offset, int lenght)
        {
            if (offset + lenght > imageData.Length)
                return null;
            byte[] temp = new byte[lenght];

            for (int i = 0; i < lenght; i++)
                temp[i] = convertSbyteToByte(imageData[i + offset]);

            return createImage(temp);
        }

        public static byte convertSbyteToByte(sbyte var)
        {
            if (var > 0)
                return (byte) var;

            return (byte) (var + 256);
        }

        public static void update()
        {
            if (status == 2)
            {
                status = 1;
                imgTemp = __createEmptyImage();
                status = 0;
            }
            else if (status == 3)
            {
                status = 1;
                imgTemp = __createImage(filenametemp);

                status = 0;
            }
            else if (status == 4)
            {
                status = 1;
                imgTemp = __createImage(datatemp);
                status = 0;
            }
            else if (status == 5)
            {
                status = 1;
                imgTemp = __createImage(imgSrcTemp, xtemp, ytemp, wtemp, htemp, transformtemp);
                status = 0;
            }
            else if (status == 6)
            {
                status = 1;
                imgTemp = __createImage(wtemp, htemp);
                status = 0;
            }
        }


        private static Image __createImage(string path)
        {
            try
            {
                Texture2D spr = Resources.Load(path) as Texture2D;
                if (spr == null)
                    throw new System.Exception("NULL POINTER EXCEPTION AT Image _createImageByPNG " + path);
                Image img = new Image();

                img.texture = spr;
                img.w = spr.width;
                img.h = spr.height;
                return img;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private static Image __createImage(byte[] imageData)
        {
            if (imageData == null || imageData.Length == 0)
            {
                Debug.LogError("Create Image from byte array fail");
                return null;
            }
            Image img = new Image();
            try
            {
                img.texture.LoadImage(imageData);
                img.w = img.texture.width;
                img.h = img.texture.height;
                setTextureQuality(img);
            }
            catch (Exception e)
            {
                Debug.LogError("CREAT IMAGE FROM ARRAY FAIL \n" + Environment.StackTrace);
            }
            return img;
        }

        private static Image __createImage(Image src, int x, int y, int w, int h, int transform)
        {
            Image img = new Image();
            img.texture = new Texture2D(w, h);

            y = src.texture.height - y - h;
            for (int i = 0; i < w; i++)
            for (int j = 0; j < h; j++)
            {
                int ii = i;
                if (transform == 2) ii = w - i;
                int jj = j;
                Color rgb = src.texture.GetPixel(x + ii, y + jj);
                img.texture.SetPixel(i, j, rgb);
            }
            img.texture.Apply();
            img.w = img.texture.width;
            img.h = img.texture.height;
            setTextureQuality(img);
            return img;
        }

        private static Image __createEmptyImage()
        {
            return new Image();
        }

        public static Image __createImage(int w, int h)
        {    
            Image img = new Image();
            img.texture = new Texture2D(w, h, TextureFormat.RGBA32, false);
            setTextureQuality(img);
            img.w = w;
            img.h = h;
            img.texture.Apply();
            return img;
        }


        public int getWidth()
        {
            return (w == 0 ? 2 : w) / MGraphics.zoomLevel;
        }

        public int getHeight()
        {
            return (h == 0 ? 2 : h) / MGraphics.zoomLevel;
        }

        public static int getWidth(MBitmap img)
        {
            if (img == null) return 1;
            return img.GetWidth();
        }

        public static int getHeight(MBitmap img)
        {
            if (img == null) return 1;
            return img.GetHeight();
        }

        private static void setTextureQuality(Image img)
        {
            setTextureQuality(img.texture);
        }

        private static void setTextureQuality(Texture2D texture)
        {
            texture.anisoLevel = 0;
            texture.filterMode = FilterMode.Point;
            texture.mipMapBias = 0;
            texture.wrapMode = TextureWrapMode.Clamp;
        }

        public int getRealImageWidth()
        {
            return w;
        }

        public int getRealImageHeight()
        {
            return h;
        }


    }
}