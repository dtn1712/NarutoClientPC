using src.lib;
using src.main;

namespace src.model
{
    public class ChatManager
    {
        private static ChatManager instance;
        public static bool blockGlobalChat, blockPrivateChat, isMessageClan, isMessagePt;
        public Vector chatTabs;
        public int currentTabIndex;
        public int isNewMessage;

        public Vector waitList = new Vector();

        public ChatManager()
        {
            chatTabs = new Vector();
            chatTabs.addElement(new ChatTab(MResources.PUBLICCHAT[0], 0));
            chatTabs.addElement(new ChatTab(MResources.PARTYCHAT[0], 1));
            chatTabs.addElement(new ChatTab(MResources.GLOBALCHAT[0], 3));
            chatTabs.addElement(new ChatTab(MResources.CLANCHAT[0], 4));
            var c = findTab(MResources.GLOBALCHAT[0]);

            c.addInfo("c8" + MResources.GLOBALCHAT[1]);
            c.addInfo("c8" + MResources.GLOBALCHAT[2]);
            c.addInfo("c8" + MResources.GLOBALCHAT[3]);

            var c1 = findTab(MResources.PARTYCHAT[0]);
            c1.addInfo("c8" + MResources.PARTYCHAT[1]);

            var c2 = findTab(MResources.CLANCHAT[0]);
            c2.addInfo("c8" + MResources.CLANCHAT[1]);

            var c3 = findTab(MResources.PUBLICCHAT[0]);
            c3.addInfo("c8" + MResources.PUBLICCHAT[1]);
        }

        public void switchToPreviousTab()
        {
            currentTabIndex--;
            if (currentTabIndex < 0) currentTabIndex = chatTabs.size() - 1;
        }

        public void switchToTab(int index)
        {
            currentTabIndex = index;
        }

        public void switchToTab(ChatTab t)
        {
            currentTabIndex = chatTabs.indexOf(t);
        }

        public void switchToLastTab()
        {
            currentTabIndex = chatTabs.size() - 1;
        }

        public static ChatManager gI()
        {
            return instance == null ? instance = new ChatManager() : instance;
        }

        public ChatTab findTab(string ownerName)
        {
            for (var i = 0; i < chatTabs.size(); i++)
            {
                var c = (ChatTab) chatTabs.elementAt(i);
                if (c.ownerName.Equals(ownerName)) return c;
            }
            return null;
        }

        public void addChat(string ownerName, string whoChat, string text)
        {
            var c = findTab(ownerName);

            if (c == null)
                c = addNewTab(ownerName);
            c.addChat(whoChat, text);
        }


        public ChatTab getCurrentChatTab()
        {
            return (ChatTab) chatTabs.elementAt(currentTabIndex);
        }

        public ChatTab addNewTab(string friendName)
        {
            var c = new ChatTab(friendName, 2);
            if (!GameCanvas.isTouch)
                c.addInfo("c2" + MResources.CLOSE_CURTAB);
            chatTabs.addElement(c);
            return c;
        }



        public bool findWaitPerson(string nick)
        {
            for (var i = 0; i < waitList.size(); i++)
                if (((string) waitList.elementAt(i)).Equals(nick))
                    return true;
            return false;
        }


        public void removeFromWaitList(string nick)
        {
            for (var i = 0; i < waitList.size(); i++)
                if (((string) waitList.elementAt(i)).Equals(nick))
                {
                    waitList.removeElementAt(i);
                    return;
                }
        }

    }
}