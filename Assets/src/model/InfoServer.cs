using src.lib;
using src.main;
using src.Objectgame;
using src.screen;

public class InfoServer
{
    public const sbyte CHATSERVER = 0;
    public const sbyte CHATRAO = 1;
    public const sbyte CHATWORLD = 2;
    
    public bool isStop;
    public NodeChat node;
    public string text;
    public int tgdung = 50;
    public int type, wString, hpaint;
    public int x;

    public InfoServer(sbyte type, string text)
    {
        this.text = text;
        hpaint = 10;
        this.type = type;
        wString = FontManager.GetInstance().tahoma_7_white.GetWidth(text);
        x = 3 * GameCanvas.w / 4 + wString / 2;
    }

    public InfoServer(sbyte type, NodeChat text)
    {
        node = text;
        hpaint = 10;
        this.type = type;
        wString = FontManager.GetInstance().tahoma_7_white.GetWidth(text.textdai);
        x = 3 * GameCanvas.w / 4 + wString / 2;
    }


    public void update()
    {
        switch (type)
        {
            case CHATSERVER:
                UpdateInfoServer();
                break;
            case CHATWORLD:
                UpdateChatWorld();
                break;
        }
    }

    public void paint(MGraphics g)
    {
        g.setColor(0, 60);

        g.setClip(GameCanvas.w / 4 - 4, hpaint, GameCanvas.w / 2 + 8, 20);
        g.fillRect(GameCanvas.w / 4 - 4, hpaint, GameCanvas.w / 2 + 8, 20, true);
        g.disableBlending();
        switch (type)
        {
            case CHATSERVER:
                FontManager.GetInstance().tahoma_7_yellow.DrawString(g, text, x, hpaint + 3, 2);
                break;
            case CHATWORLD:
                if (node == null)
                    FontManager.GetInstance().tahoma_7_white.DrawString(g, text, x, hpaint + 3, 2);
                else node.paint(g, x, hpaint + 3, true, true);
                break;
        }
        GameCanvas.ResetTrans(g);
    }

    private void UpdateChatWorld()
    {
        if (x > GameCanvas.w / 4 - wString && !isStop)
            if (x > GameCanvas.w / 4)
            {
                x -= 10;
            }
            else
            {
                if (tgdung <= 0)
                {
                    tgdung--;
                    if (wString < GameCanvas.w / 2)
                        x += tgdung / 2;
                    else x -= 2;
                }
                else
                {
                    x = GameCanvas.w / 4 /*+wString/2*/;
                    tgdung--;
                }
            }
        else
            GameScr.listWorld.removeElement(this);
    }

    private void UpdateInfoServer()
    {
        if (x > GameCanvas.w / 4 - wString)
            x--;
        else
            GameScr.listInfoServer.removeElement(this);
    }
}