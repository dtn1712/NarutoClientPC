using System.Collections.Generic;

namespace src.model
{
    public static class Cmd
    {
       
        public const sbyte HANDSHAKE = 0;
        public const sbyte LOGIN = 1;
        public const sbyte MAIN_CHAR_INFO = 3;
        public const sbyte MOVE = 4;
        public const sbyte BASIC_CHAR_INFO = 5;
        public const sbyte MONSTER_INFO = 6;
        public const sbyte PLAYER_OUT = 7;
        public const sbyte ATTACK = 8;
        public const sbyte USE_ITEM = 9;
        public const sbyte CHANGE_MAP = 10;
        public const sbyte SELECT_CHAR = 11;
        public const sbyte CREATE_CHAR = 12;
        public const sbyte GIVEUP_ITEM = 13; //vứt bỏ item
        public const sbyte PICK_UP_ITEM = 14;
        public const sbyte ADD_BASE_POINT = 16;
        public const sbyte TALK_NPC = 17;
        public const sbyte BUY_ITEM_FROM_SHOP = 18;
        public const sbyte CHAT = 19;
        public const sbyte COME_HOME = 20;
        public const sbyte REGISTER = 23;
        public const sbyte MENU_NPC = 24;
        public const sbyte CHAR_LIST = 27;
        public const sbyte GET_ITEM_INVENTORY = 30;
        public const sbyte DROP_ITEM = 31;
        public const sbyte PARTY = 32;
        public const sbyte CHAR_EXP = 33; 
        public const sbyte TRADE_ITEM = 34; //trade
        public const sbyte SKILL_CHAR = 35;
        public const sbyte TAKE_OFF = 36;
//        public const sbyte SERVER_MESSAGE = 37;
        public const sbyte CHAR_DEAD = 39;
        public const sbyte FRIEND = 40;
        public const sbyte QUEST = 43; //quest
        public const sbyte NPC = 44;
        public const sbyte CHANGE_REGION = 45;
        public const sbyte REGION_INFO = 46;
        public const sbyte SELL_ITEM = 51;
        public const sbyte SERVER_NOTIFICATION = 52;
        public const sbyte REMOVE_TARGET = 53;
        public const sbyte REQUEST_SHOP_INFO = 54; // yeu cau item
        public const sbyte REQUEST_MENU_SHOP = 55; // menu shop;
        public const sbyte NPC_TEMPLATE = 57;
        public const sbyte MAP_TEMPLATE = 58;
        public const sbyte CHAR_SKILL_STUDY = 60;
        public const sbyte COMPETED_ATTACK = 62;
        public const sbyte STATUS_ATTACK = 63;
        public const sbyte UPDATE_CHAR = 64;
        public const sbyte UPDATE_CLIENT = 65;
        public const sbyte UPGRADE_ITEM = 66;
        public const sbyte CLAN = 67;
        public const sbyte BOSS_APPEAR = 68;
        public const sbyte MAKE_PAYMENT = 70;
        public const sbyte GET_PRICING = 71;
        public const sbyte SHOW_PAYMENT = 72;
        public const sbyte SERVER_DIALOG = 73;
        public const sbyte CHAR_WEARING = 81;
        public const sbyte CHAR_REVIVE = 82;

        //For trade
        public const sbyte TRADE_INVITE = 83;
        public const sbyte TRADE_ACCEPT_INVITE = 84;
        public const sbyte TRADE_MOVE_ITEM = 85;
        public const sbyte TRADE_ACCEPT = 86;
        public const sbyte TRADE_TAKE_OFF = 87;
        public const sbyte TRADE_CANCEL = 88;
        public const sbyte TRADE_LOCK = 96;
        public const sbyte TRADE_END = 97;
    
        //For item template
        public const sbyte ITEM_TEMPLATE = 89;
        //For Skill Char
        public const sbyte SKILL_CHAR_LIST = 90;
        //Char Position
        public const sbyte CHAR_POSITION = 91;
        //Friend
        public const sbyte FRIEND_GET_LIST = 92;
        public const sbyte FRIEND_INVITE = 93;
        public const sbyte FRIEND_KICK = 94;
        public const sbyte FRIEND_ACCEPT = 95;
       
        // server message
        
        public const sbyte ALERT_TIME = 98;
        public const sbyte SERVER_TOP_RIGHT_MSG = 99;
        public const sbyte SERVER_MESSAGE_EFFECT = 100;
        public const sbyte ALERT_MESSAGE = 101;
        public const sbyte MONSTER_MOVE = 102;
        //Clan
        public const sbyte CLAN_INVITE_LOCAL = 103;
        public const sbyte CLAN_INVITE_GLOBAL = 104;
        public const sbyte CLAN_KICK_LOCAL = 105;
        public const sbyte CLAN_KICK_GLOBAL = 106;
        public const sbyte CLAN_LEAVE_GLOBAL = 107;
        public const sbyte CLAN_LEAVE_LOCAL = 108;
        public const sbyte CLAN_GET_LIST_LOCAL = 109;
        public const sbyte CLAN_GET_LIST_GLOBAL = 110;
        
        public const sbyte CIVIL_WAR_DROP_KAGE = 111;

        public const sbyte REQUEST_KEY = -27;
        public const sbyte REQUEST_IMAGE = -56;


        private static readonly HashSet<sbyte> RequireEncryptionCommands = new HashSet<sbyte>(new[]
        {
            LOGIN, REGISTER
//            CLAN_GET_LIST_GLOBAL
        });
    
        private static readonly HashSet<sbyte> FastMessageCommands = new HashSet<sbyte>(new[]
        {
            MOVE, CHAR_POSITION, HANDSHAKE
        });
    
        public static bool IsRequireEncryptionCommand(sbyte command)
        {
            return RequireEncryptionCommands.Contains(command);
        }
    
        public static bool IsFastMessageCommand(sbyte key) {
            return FastMessageCommands.Contains(key);
        }
    }
}