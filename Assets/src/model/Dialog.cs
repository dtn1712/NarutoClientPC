using src.lib;
using src.main;
using src.model;

public abstract class Dialog
{
    public Command left, center, right;
    public int lenCaption = 0;


    public virtual void paint(MGraphics g)
    {
        g.translate(-g.getTranslateX(), -g.getTranslateY());
        g.setClip(0, 0, GameCanvas.w, GameCanvas.h);
        GameCanvas.paintt.paintTabSoft(g);
    }

    public virtual void keyPress(int keyCode)
    {
        switch (keyCode)
        {
            case -1:
            case -38:
                GameCanvas.keyHold[2] = true;
                GameCanvas.keyPressed[2] = true;
                return; // UP
            case -2:
            case -39:
                GameCanvas.keyHold[8] = true;
                GameCanvas.keyPressed[8] = true;
                return; // DOWN
            case -6:
            case -21:
                GameCanvas.keyHold[12] = true;
                GameCanvas.keyPressed[12] = true;
                return; // Soft1
            case -7:
            case -22:
                GameCanvas.keyHold[13] = true;
                GameCanvas.keyPressed[13] = true;
                return; // Soft2
            case -5:
            case 10:
                GameCanvas.keyHold[5] = true;
                GameCanvas.keyPressed[5] = true;
                return; // [i]
        }
    }

    public virtual void update()
    {
        if (center != null)
            if (GameCanvas.keyPressed[5] || TScreen.getCmdPointerLast(center))
            {
                GameCanvas.keyPressed[5] = false;
                GameCanvas.isPointerClick = false;
                TScreen.keyTouch = -1;
                GameCanvas.isPointerJustRelease = false;
                if (center != null) center.performAction();
                TScreen.keyTouch = -1;
            }
        if (left != null)
            if (GameCanvas.keyPressed[12] || TScreen.getCmdPointerLast(left))
            {
                GameCanvas.keyPressed[12] = false;
                GameCanvas.isPointerClick = false;
                TScreen.keyTouch = -1;
                GameCanvas.isPointerJustRelease = false;
                if (left != null) left.performAction();
                TScreen.keyTouch = -1;
            }
        if (right != null)
            if (GameCanvas.keyPressed[13] || TScreen.getCmdPointerLast(right))
            {
                GameCanvas.keyPressed[13] = false;
                GameCanvas.isPointerClick = false;
                GameCanvas.isPointerJustRelease = false;
                TScreen.keyTouch = -1;
                if (right != null) right.performAction();
                TScreen.keyTouch = -1;
            }
        GameCanvas.clearKeyPressed();
        GameCanvas.clearKeyHold();
    }

}