//thông tin server

using src.lib;
using src.main;
using src.Objectgame;

public class Info
{
    public static Vector infoWaitToShow = new Vector();
    public static InfoItem info;
    public static int p1 = 5, p2, p3, x, strWidth;
    public static int limLeft = 2;
    public static int hI = 15;


    private static bool canMergeString(string s)
    {
        if (info != null && info.s != null && s.Equals(info.s)) return true;
        if (infoWaitToShow.size() > 0 && s.Equals(((InfoItem) infoWaitToShow.lastElement()).s)) return true;
        if (s.Length < 8) return false;
        if (info != null && info.s != null && p1 < 3 && info.s.Length >= 8)
        {
            var m1 = MSystem.substring(s, 0, 8);
            var m2 = MSystem.substring(info.s, 0, 8);
            if (m1.Equals(m2))
            {
                var i = 7;
                for (; i < s.Length; i++)
                {
                    if (i >= info.s.Length) break;
                    if (s[i] != info.s[i]) break;
                }
                var append = MSystem.substring(s, i, s.Length);
                info.s += ", " + append;
                p1 = 2;
                p2 = 0;
                return true;
            }
        }
        if (infoWaitToShow.size() > 0)
        {
            var s2 = ((InfoItem) infoWaitToShow.lastElement()).s;
            if (s2.Length >= 8)
            {
                var m1 = MSystem.substring(s, 0, 8);
                var m2 = MSystem.substring(s2, 0, 8);
                if (m1.Equals(m2))
                {
                    var i = 7;
                    for (; i < s.Length; i++)
                    {
                        if (i >= s2.Length) break;
                        if (s[i] != s2[i]) break;
                    }
                    var append = MSystem.substring(s, i, s.Length);
                    s2 += ", " + append;
                    return true;
                }
            }
        }
        return false;
    }

    public static void addInfo(string s, int speed, MFont f)
    {
        if (canMergeString(s)) return;
        if (GameCanvas.w == 128)
            limLeft = 1;
        if (infoWaitToShow.size() > 10)
            infoWaitToShow.removeElementAt(0);
        infoWaitToShow.addElement(new InfoItem(s, f, speed));
    }

}