using System;
using src.lib;
using src.main;
using src.Objectgame;
using UnityEngine;

public class SmallImage
{
    public static int[][] smallImg;
    public static SmallImage instance;
    public static MBitmap[] imgbig;
    public static MyHashtable imgNew = new MyHashtable();
    public static MyHashtable img_big = new MyHashtable();
    public static MBitmap imgEmpty;

    public static string pathBigImage = "x" + MGraphics.zoomLevel + "/img/big_";
    public static string pathObjectMap = "x" + MGraphics.zoomLevel + "/mapobject/";
    public static string pathAvataNPC = "x" + MGraphics.zoomLevel + "/npc/";
    public static string pathMob = "x" + MGraphics.zoomLevel + "/mob/";
    public static string keyOKDownloaded = "download_bigimg";
    public static int nBigImage = 400;

    public static int ID_ADD_MAPOJECT = 1000;
    public static int ID_ADD_AVATARNPC = 20000;
    public static int ID_ADD_MOB = 21000;

    public static int[] IdBigImage;


    public SmallImage()
    {
        readImage();
    }

    public static string getPathImage(int id)
    {
        if (id < 1000) return pathBigImage;
        if (id < 20000)
            return pathObjectMap;
        if (id < 21000)
            return pathAvataNPC;
        return pathMob;
    }


    public static void loadBigImage()
    {
        imgbig = null;
        MSystem.gcc();
        imgbig = new MBitmap[35];
        BgItem.imgobj = new MBitmap[400];
      
        imgEmpty = new MBitmap(Image.createImage(1, 1));
    }

    public static void init()
    {
        instance = null;
        instance = new SmallImage();
    }

    public static void readImage()
    {
        try
        {
            DataInputStream file;
            file = new DataInputStream(Rms.loadRMS("nj_image"));
            nBigImage = file.ReadShort();

            imgbig = new MBitmap[nBigImage];
            int sum = file.ReadShort();
            IdBigImage = new int[sum];
            smallImg = new int[sum][];
            for (var i = 0; i < smallImg.Length; i++)
                smallImg[i] = new int[7];
            for (var i = 0; i < sum; i++)
            {
                IdBigImage[i] = i;
                smallImg[i][0] = file.ReadShort(); // id hình lớn //
                smallImg[i][1] = file.ReadShort(); // x cắt
                smallImg[i][2] = file.ReadShort(); // y cắt
                smallImg[i][3] = file.ReadShort(); // w cắt
                smallImg[i][4] = file.ReadShort(); // h cắt
            }


        }
        catch (Exception ex)
        {
            Debug.LogError(ex);
        }
    }


    public static void DrawSmallImage(MGraphics g, int id, int x, int y, int transform, int anchor)
    {
        //loadFromServer(id);
        //		System.out.println(smallImg[id][0]+" smallImg[id][0] ----> "+imgbig.length);
        try
        {
            MBitmap img = null;
            if (imgbig[smallImg[id][0]] != null &&
                (id >= smallImg.Length || smallImg[id][1] >= imgbig[smallImg[id][0]].GetWidth()
                 || smallImg[id][3] >= imgbig[smallImg[id][0]].GetWidth()
                 || smallImg[id][2] >= imgbig[smallImg[id][0]].GetHeight()
                 || smallImg[id][4] >= imgbig[smallImg[id][0]].GetHeight()))
            { 
                img = (MBitmap) imgNew.get(id + "");
          
                if (img != null)
                    g.drawRegion(img, 0, 0, img.GetWidth(), img.GetHeight(), transform, x, y, anchor);
            }
            else
            {

                if (imgbig[smallImg[id][0]] != null)
                {
                    g.drawRegion(imgbig[smallImg[id][0]], smallImg[id][1], smallImg[id][2], smallImg[id][3],
                        smallImg[id][4], transform, x, y, anchor);
                }
                else
                {
                    var imgput = CreateBigImage(smallImg[id][0]);
                    if (imgput != null)
                        g.drawRegion(imgput, smallImg[id][1], smallImg[id][2], smallImg[id][3], smallImg[id][4],
                            transform, x, y, anchor);
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }

    private static MBitmap CreateBigImage(int id)
    {
        return imgbig[id] ?? (imgbig[id] = GameCanvas.loadImage("/img/big_" + id + ".png"));
    }

    public static void CleanImg()
    {
        try
        {
            for (var i = 0; i < imgbig.Length; i++)
            {
//                if (imgbig[i] != null && imgbig[i].image != null)
//                    imgbig[i].cleanImg();
                
                imgbig[i] = null;
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }

    public static void DrawSmallImage(MGraphics g, int id, int x, int y, int transform, int anchor, bool isUclip)
    {
        try
        {
            MBitmap img = null;
            if (imgbig[smallImg[id][0]] != null && (id >= smallImg.Length
                                                    || smallImg[id][1] >= imgbig[smallImg[id][0]].GetWidth()
                                                    || smallImg[id][3] >= imgbig[smallImg[id][0]].GetWidth()
                                                    || smallImg[id][2] >= imgbig[smallImg[id][0]].GetHeight()
                                                    || smallImg[id][4] >= imgbig[smallImg[id][0]].GetHeight()
                    ))
            {
              


                img = (MBitmap) imgNew.get(id + "");

                if (img != null)
                    g.drawRegion(img, 0, 0, img.GetWidth(), img.GetHeight(), transform, x, y, anchor, isUclip);
            }
            else
            {
         
                if (imgbig[smallImg[id][0]] != null)
                {
                    g.drawRegion(imgbig[smallImg[id][0]], smallImg[id][1], smallImg[id][2], smallImg[id][3],
                        smallImg[id][4], transform, x, y, anchor, isUclip);
                }
                else
                {
                    var imgput = CreateBigImage(smallImg[id][0]);
                    if (imgput != null)
                        g.drawRegion(imgput, smallImg[id][1], smallImg[id][2], smallImg[id][3], smallImg[id][4],
                            transform, x, y, anchor, isUclip);
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }

    public static void drawSmallImageScalse(MGraphics g, int id, int x, int y, int transform, int anchor,
        bool isUclip, int perScale)
    {
        try
        {
            MBitmap img = null;
            if (imgbig[smallImg[id][0]] != null && (id >= smallImg.Length
                                                    || smallImg[id][1] >= imgbig[smallImg[id][0]].GetWidth()
                                                    || smallImg[id][3] >= imgbig[smallImg[id][0]].GetWidth()
                                                    || smallImg[id][2] >= imgbig[smallImg[id][0]].GetHeight()
                                                    || smallImg[id][4] >= imgbig[smallImg[id][0]].GetHeight()
                 ))
            {
                img = (MBitmap) imgNew.get(id + "");
                if (img != null)
                    g.drawRegion(img, 0, 0, img.GetWidth(), img.GetHeight(), transform, x, y, anchor, isUclip);
            }
            else
            {
                if (id == 1029 || id == 1030 || id == 1028 || id == 1031 || id == 1038)
                {
                }
                if (imgbig[smallImg[id][0]] != null)
                {
                    g.drawRegion(imgbig[smallImg[id][0]], smallImg[id][1], smallImg[id][2], smallImg[id][3],
                        smallImg[id][4], transform, x, y, anchor, isUclip);
                }
                else
                {
                    var imgput = CreateBigImage(smallImg[id][0]);
                    if (imgput != null)
                        g.drawRegion(imgput, smallImg[id][1], smallImg[id][2], smallImg[id][3], smallImg[id][4],
                            transform, x, y, anchor, isUclip);
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }

    public static void DrawSmallImage(MGraphics g, int id, int x, int y, int transform, int anchor, bool isUclip, int opacity)
    {
        try
        {
            MBitmap img = null;
            if (imgbig[smallImg[id][0]] != null && (id >= smallImg.Length
                                                    || smallImg[id][1] >= imgbig[smallImg[id][0]].GetWidth()
                                                    || smallImg[id][3] >= imgbig[smallImg[id][0]].GetWidth()
                                                    || smallImg[id][2] >= imgbig[smallImg[id][0]].GetHeight()
                                                    || smallImg[id][4] >= imgbig[smallImg[id][0]].GetHeight()
                   ))
            {
               

                img = (MBitmap) imgNew.get(id + "");
         
                if (img != null)
                    g.drawRegion(img, 0, 0, img.GetWidth(), img.GetHeight(), transform, x, y, anchor, isUclip, opacity);
            }
            else
            {
               
                if (imgbig[smallImg[id][0]] != null)
                {
                    g.drawRegion(imgbig[smallImg[id][0]], smallImg[id][1], smallImg[id][2], smallImg[id][3],
                        smallImg[id][4], transform, x, y, anchor, isUclip, opacity);
                }
                else
                {
                    var imgput = CreateBigImage(smallImg[id][0]);
                    if (imgput != null)
                        g.drawRegion(imgput, smallImg[id][1], smallImg[id][2], smallImg[id][3], smallImg[id][4],
                            transform, x, y, anchor, isUclip, opacity);
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }

}