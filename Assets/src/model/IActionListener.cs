namespace src.model
{
    public interface IActionListener
    {
        void perform(int idAction, object p);

    }
}