using src.lib;
using src.Objectgame;
using src.screen;

public class Party
{
    public static Vector vCharinParty = new Vector();
    public static short idleader;

    protected static Party instance;
    public Char c;
    public short charId; // in sưa lai short
    public Char Charleader;
    public short iconId;
    public short idParty;
    public bool isLeader = false;
    public bool isLock;
    public int level;
    public string name;
    public int size;

    public Party()
    {
    }

    public Party(short partyid, short[] idmember, short idleader, short[] lv, short[] idhead)
    {
        vCharinParty.removeAllElements();
        for (var i = 0; i < idmember.Length; i++)
        {
            var cMem = GameScr.findCharInMap(idmember[i]);

            if (cMem != null)
            {
                cMem.idParty = partyid;
                if (cMem.charID == idleader)
                    cMem.isLeader = true;
                else
                    cMem.isLeader = false;
                vCharinParty.addElement(cMem);
            }
            else
            {
                cMem = Char.myChar();
                cMem.idParty = partyid;
                if (cMem.charID == idleader)
                    cMem.isLeader = true;
                else
                    cMem.isLeader = false;

                cMem.clevel = lv[i];
                cMem.head = idhead[i];
                vCharinParty.addElement(cMem);
            }
        }
    }


    public static Party gI()
    {
        if (instance == null)
            instance = new Party();
        return instance;
    }
}