using src.lib;

public abstract class Effect
{
    public static Vector vEffect2 = new Vector();
    public static Vector vRemoveEffect2 = new Vector();
    public static Vector vEffect2Outside = new Vector();
    public static Vector vAnimateEffect = new Vector();

    public virtual void update()
    {
    }

    public virtual void paint(MGraphics g)
    {
    }
}