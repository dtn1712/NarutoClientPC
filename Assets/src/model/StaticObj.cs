using src.lib;

public class StaticObj
{
    // ====================
    public const int NORMAL = 0;

    public const int UP_FALL = 1;
    public const int UP_RUN = 2;
    public const int FALL_RIGHT = 3;
    public const int FALL_LEFT = 4; // KO BI THUONG
    public const int MOD_ATTACK_ME = 100;

    // =============model obj

    public const int TYPE_PLAYER = 3;
    // ==============model obj

    // ========item
    public const int TYPE_NON = 0;

    public const int TYPE_VUKHI = 1;
    public const int TYPE_AO = 2;
    public const int TYPE_LIEN = 3;
    public const int TYPE_TAY = 4;
    public const int TYPE_NHAN = 5;
    public const int TYPE_QUAN = 6;
    public const int TYPE_BOI = 7;
    public const int TYPE_GIAY = 8;
    public const int TYPE_PHU = 9;
    public const int TYPE_OTHER = 11;

    public const int TYPE_CRYSTAL = 15;
    // ==============

    // ===============set focus
    public const int FOCUS_MOD = 1;

    public const int FOCUS_ITEM = 2;
    public const int FOCUS_PLAYER = 3;
    public const int FOCUS_ZONE = 4;
    public const int FOCUS_NPC = 5;
    public static int TOP_CENTER = MGraphics.TOP | MGraphics.HCENTER;
    public static int TOP_LEFT = MGraphics.TOP | MGraphics.LEFT;
    public static int TOP_RIGHT = MGraphics.TOP | MGraphics.RIGHT;
    public static int BOTTOM_HCENTER = MGraphics.BOTTOM | MGraphics.HCENTER;
    public static int BOTTOM_LEFT = MGraphics.BOTTOM | MGraphics.LEFT;
    public static int BOTTOM_RIGHT = MGraphics.BOTTOM | MGraphics.RIGHT;
    public static int VCENTER_HCENTER = MGraphics.VCENTER | MGraphics.HCENTER;
    public static int VCENTER_LEFT = MGraphics.VCENTER | MGraphics.LEFT;

    public static int[] SKYCOLOR =
    {
        0x55aaee, 0x4880f8, 0x101010,
        0x1fc3f5, 0x1fc3f5, 0, 0xa0d8f8, 0x268dc6, 0x171808, 0xFFBFBC, 0,
        0x139CAA, 0x139CAA
    };

    public static int[][] TYPEBG =
    {
        new[] {0, 0, 0, 0}, new[] {1, 1, 1, -1},
        new[] {2, 2, 2, 2}, new[] {2, 2, 2, -1}, new[] {3, 3, 3, 3}, new[] {4, -1, -1, 4},
        new[] {5, 5, 5, -1}, new[] {6, 6, 6, 5}, new[] {7, 7, -1, -1}, new[] {8, 8, 8, 7},
        new[] {9, -1, -1, 8}, new[] {10, -1, -1, 9}, new[] {11, -1, -1, -1}
    };

    // =================
  

    // ==============mang type bg
}