using src.lib;
using src.main;
using src.model;

public class Scroll
{
    private int cmRun;
    public int cmtoX, cmtoY, cmx, cmy, cmvx, cmvy, cmdx, cmdy;
    private int cmxLim, cmyLim;
    private bool iswh;
    public int ITEM_PER_LINE;
    public int ITEM_SIZE, nITEM, ITEM_SIZEW, ITEM_SIZEH;
    private int pointerDownFirstX;
    private readonly int[] pointerDownLastX = new int[3];

    private int pointerDownTime;
    private bool pointerIsDowning, isDownWhenRunning;
    public int selectedItem;
    public bool styleLeftRight;
    public bool styleUPDOWN = true;
    public int timedow;
    public int xPos, yPos, width, height;

    public void clear()
    {
        cmtoX = 0;
        cmtoY = 0;
        cmx = 0;
        cmy = 0;
        cmvx = 0;
        cmvy = 0;
        cmdx = 0;
        cmdy = 0;
        cmxLim = 0;
        cmyLim = 0;
        width = 0;
        height = 0;
    }

    public void setClip(MGraphics g, int x, int y, int w, int h)
    {
        g.setClip(x - 2, y, w + 2, h - 1);
        g.translate(-g.getTranslateX(), -g.getTranslateY());
        g.translate(-cmx, -cmy);
    }

    public void setClip(MGraphics g)
    {
        g.setClip(xPos, yPos, width, height - 1);
        g.translate(-g.getTranslateX(), -g.getTranslateY());
        g.translate(-cmx, -cmy);
    }

    /***
     * 
     * @return Kết quả cuộn: Có phải vừa mới chọn hay không, và vị trí selected.
     */

    public ScrollResult updateKey()
    {
        if (iswh)
            return updateKeyScrollUpDownWH();
        if (!styleLeftRight)
            if (styleUPDOWN)
                return updateKeyScrollUpDown2();
            else
                return updateKeyScrollLeftRight();
        return updateKeyScrollLeftRight();
    }

    private ScrollResult updateKeyScrollUpDownWH()
    {
        var xTT = xPos;
        var yTT = yPos;
        var wTT = width;
        var hTT = height;
        if (GameCanvas.isPointerDown)
            if (!pointerIsDowning && GameCanvas.isPointer(xTT, yTT, wTT, hTT))
            {
                for (var i = 0; i < pointerDownLastX.Length; i++)
                    pointerDownLastX[0] = GameCanvas.py;
                pointerDownFirstX = GameCanvas.py;
                pointerIsDowning = true;
                selectedItem = -1;
                isDownWhenRunning = cmRun != 0;
                cmRun = 0;
            }
            else if (pointerIsDowning)
            {
                pointerDownTime = (timedow + 1) % 100;
                if (pointerDownTime > 5 && pointerDownFirstX == GameCanvas.py && !isDownWhenRunning)
                {
                    pointerDownFirstX = -1000;
                    if (ITEM_PER_LINE > 1)
                    {
                        var selectedRow = (cmtoY + GameCanvas.py - yTT) / ITEM_SIZEH;
                        var selectedColumn = (cmtoX + GameCanvas.px - xTT) / ITEM_SIZEW;
                        selectedItem = selectedRow * ITEM_PER_LINE + selectedColumn;
                    }
                    else
                    {
                        selectedItem = (cmtoY + GameCanvas.py - yTT) / ITEM_SIZEH;
                    }
                }
                var dx = GameCanvas.py - pointerDownLastX[0];

                if (dx != 0 && selectedItem != -1)
                    selectedItem = -1;

                for (var i = pointerDownLastX.Length - 1; i > 0; i--)
                    pointerDownLastX[i] = pointerDownLastX[i - 1];
                pointerDownLastX[0] = GameCanvas.py;

                cmtoY -= dx;
                if (cmtoY < 0)
                    cmtoY = 0;
                if (cmtoY > cmyLim)
                    cmtoY = cmyLim;
                if (cmy < 0 || cmy > cmyLim)
                    dx /= 2;
                cmy -= dx;
            }
        var isFinish = false;
        if (GameCanvas.isPointerJustRelease && pointerIsDowning)
        {
            var dx = GameCanvas.py - pointerDownLastX[0];
            if (CRes.abs(dx) < 20 && CRes.abs(GameCanvas.py - pointerDownFirstX) < 20 && !isDownWhenRunning)
            {
                cmRun = 0;
                cmtoY = cmy;
                pointerDownFirstX = -1000;

                if (ITEM_PER_LINE > 1)
                {
                    var selectedRow = (cmtoY + GameCanvas.py - yTT) / ITEM_SIZEH;
                    var selectedColumn = (cmtoX + GameCanvas.px - xTT) / ITEM_SIZEW;
                    selectedItem = selectedRow * ITEM_PER_LINE + selectedColumn;
                }
                else
                {
                    selectedItem = (cmtoY + GameCanvas.py - yTT) / ITEM_SIZEH;
                }

                pointerDownTime = 0;
                isFinish = true;
            }
            else if (selectedItem != -1 && pointerDownTime > 5)
            {
                pointerDownTime = 0;
                isFinish = true;
            }
            else if (selectedItem == -1 && !isDownWhenRunning)
            {
                if (cmy < 0)
                {
                    cmtoY = 0;
                }
                else if (cmy > cmyLim)
                {
                    cmtoY = cmyLim;
                }
                else
                {
                    var s = GameCanvas.py - pointerDownLastX[0] + (pointerDownLastX[0] - pointerDownLastX[1]) +
                            (pointerDownLastX[1] - pointerDownLastX[2]);
                    if (s > 10)
                        s = 10;
                    else if (s < -10)
                        s = -10;
                    else
                        s = 0;
                    cmRun = -s * 100;
                }
            }
            pointerIsDowning = false;
            pointerDownTime = 0;
        }

        var r = new ScrollResult();
        r.selected = selectedItem;
        r.isFinish = isFinish;
        r.isDowning = pointerIsDowning;

        return r;
    }


    private ScrollResult updateKeyScrollUpDown2()
    {
        var xTT = xPos;
        var yTT = yPos;
        var wTT = width;
        var hTT = height;
        if (GameCanvas.isPointerDown)
            if (!pointerIsDowning && GameCanvas.isPointer(xTT, yTT, wTT, hTT))
            {
                for (var i = 0; i < pointerDownLastX.Length; i++)
                    pointerDownLastX[0] = GameCanvas.py;
                pointerDownFirstX = GameCanvas.py;
                pointerIsDowning = true;
                selectedItem = -1;
                isDownWhenRunning = cmRun != 0;

                cmRun = 0;
            }
            else if (pointerIsDowning)
            {
                pointerDownTime = (timedow + 1) % 100;
                if (pointerDownTime > 5 && pointerDownFirstX == GameCanvas.py && !isDownWhenRunning)
                {
                    pointerDownFirstX = -1000;

                    if (ITEM_PER_LINE > 1)
                    {
                        var selectedRow = (cmtoY + GameCanvas.py - yTT) / ITEM_SIZE;
                        var selectedColumn = (cmtoX + GameCanvas.px - xTT) / ITEM_SIZE;
                        selectedItem = selectedRow * ITEM_PER_LINE + selectedColumn;
                    }
                    else
                    {
                        selectedItem = (cmtoY + GameCanvas.py - yTT) / ITEM_SIZE;
                    }
                }
                var dx = GameCanvas.py - pointerDownLastX[0];

                if (dx != 0 && selectedItem != -1)
                    selectedItem = -1;

                for (var i = pointerDownLastX.Length - 1; i > 0; i--)
                    pointerDownLastX[i] = pointerDownLastX[i - 1];
                pointerDownLastX[0] = GameCanvas.py;

                cmtoY -= dx;
                if (cmtoY < 0)
                    cmtoY = 0;
                if (cmtoY > cmyLim)
                    cmtoY = cmyLim;
                if (cmy < 0 || cmy > cmyLim)
                    dx /= 2;
                cmy -= dx;
            }
        var isFinish = false;
        if (GameCanvas.isPointerJustRelease && pointerIsDowning)
        {
            var dx = GameCanvas.py - pointerDownLastX[0];
            if (CRes.abs(dx) < 20 && CRes.abs(GameCanvas.py - pointerDownFirstX) < 20 && !isDownWhenRunning)
            {
                cmRun = 0;
                cmtoY = cmy;
                pointerDownFirstX = -1000;

                if (ITEM_PER_LINE > 1)
                {
                    var selectedRow = (cmtoY + GameCanvas.py - yTT) / ITEM_SIZE;
                    var selectedColumn = (cmtoX + GameCanvas.px - xTT) / ITEM_SIZE;
                    selectedItem = selectedRow * ITEM_PER_LINE + selectedColumn;
                }
                else
                {
                    selectedItem = (cmtoY + GameCanvas.py - yTT) / ITEM_SIZE;
                }

                pointerDownTime = 0;
                isFinish = true;
            }
            else if (selectedItem != -1 && pointerDownTime > 5)
            {
                pointerDownTime = 0;
                isFinish = true;
            }
            else if (selectedItem == -1 && !isDownWhenRunning)
            {
                if (cmy < 0)
                {
                    cmtoY = 0;
                }
                else if (cmy > cmyLim)
                {
                    cmtoY = cmyLim;
                }
                else
                {
                    var s = GameCanvas.py - pointerDownLastX[0] + (pointerDownLastX[0] - pointerDownLastX[1]) +
                            (pointerDownLastX[1] - pointerDownLastX[2]);
                    if (s > 10)
                        s = 10;
                    else if (s < -10)
                        s = -10;
                    else
                        s = 0;
                    cmRun = -s * 100;
                }
            }
            pointerIsDowning = false;
            pointerDownTime = 0;
        }
        var r = new ScrollResult();
        r.selected = selectedItem;
        r.isFinish = isFinish;
        r.isDowning = pointerIsDowning;
        return r;
    }

    private ScrollResult updateKeyScrollLeftRight()
    {
        var xTT = xPos;
        var yTT = yPos;
        var wTT = width;
        var hTT = height;
        if (GameCanvas.isPointerDown)
            if (!pointerIsDowning && GameCanvas.isPointer(xTT, yTT, wTT, hTT))
            {
                for (var i = 0; i < pointerDownLastX.Length; i++)
                    pointerDownLastX[0] = GameCanvas.px;
                pointerDownFirstX = GameCanvas.px;
                pointerIsDowning = true;
                selectedItem = -1;
                isDownWhenRunning = cmRun != 0;
                cmRun = 0;
            }
            else if (pointerIsDowning)
            {
                pointerDownTime++;
                if (pointerDownTime > 5 && pointerDownFirstX == GameCanvas.px && !isDownWhenRunning)
                {
                    pointerDownFirstX = -1000;

                    selectedItem = (cmtoX + GameCanvas.px - xTT) / ITEM_SIZE;
                }
                var dx = GameCanvas.px - pointerDownLastX[0];
                if (dx != 0 && selectedItem != -1)
                    selectedItem = -1;
                for (var i = pointerDownLastX.Length - 1; i > 0; i--)
                    pointerDownLastX[i] = pointerDownLastX[i - 1];
                pointerDownLastX[0] = GameCanvas.px;
            }
        var isFinish = false;
        if (GameCanvas.isPointerJustRelease && pointerIsDowning)
        {
            var dx = GameCanvas.px - pointerDownLastX[0];
            GameCanvas.isPointerJustRelease = false;

            if (CRes.abs(dx) < 20 && CRes.abs(GameCanvas.px - pointerDownFirstX) < 20 && !isDownWhenRunning)
            {
                cmRun = 0;
                pointerDownFirstX = -1000;
                //them vao cho scroll theo chieu ngang
                var selectedRow = (cmtoY + GameCanvas.py - yTT) / ITEM_SIZE;
                var selectedColumn = (cmtoX + GameCanvas.px - xTT) / ITEM_SIZE;
                selectedItem = selectedRow * ITEM_PER_LINE + selectedColumn;

                pointerDownTime = 0;
                isFinish = true;
            }
            else if (selectedItem != -1 && pointerDownTime > 5)
            {
                pointerDownTime = 0;
                isFinish = true;
            }
            else if (selectedItem == -1 && !isDownWhenRunning)
            {
                if (cmx < 0)
                {
                    cmtoX = 0;
                }
                else
                {
                    var s = GameCanvas.px - pointerDownLastX[0] + (pointerDownLastX[0] - pointerDownLastX[1]) +
                            (pointerDownLastX[1] - pointerDownLastX[2]);
                    if (s > 10)
                        s = 10;
                    else if (s < -10)
                        s = -10;
                    else
                        s = 0;
                    cmRun = -s * 100;
                }
            }
            pointerIsDowning = false;
            pointerDownTime = 0;
            GameCanvas.isPointerJustRelease = false;
        }

        var r = new ScrollResult();
        r.selected = selectedItem;
        r.isFinish = isFinish;
        r.isDowning = pointerIsDowning;
        return r;
    }


    public void updatecm()
    {
        if (cmRun != 0 && !pointerIsDowning)
        {
            if (!styleLeftRight)
            {
                if (styleUPDOWN)
                {
                    cmtoY += cmRun / 100;
                    if (cmtoY < 0)
                        cmtoY = 0;
                    else if (cmtoY > cmyLim)
                        cmtoY = cmyLim;
                    else
                        cmy = cmtoY;
                }
                else
                {
                    cmtoX += cmRun / 100;
                    if (cmtoX < 0)
                        cmtoX = 0;
                    else if (cmtoX > cmxLim)
                        cmtoX = cmxLim;
                    else
                        cmx = cmtoX;
                }
            }
            else //xu ly cho dang luoi. scroll ngang
            {
                cmtoX += cmRun / 100;
                if (cmtoX < 0)
                    cmtoX = 0;
                else if (cmtoX > cmxLim)
                    cmtoX = cmxLim;
                else
                    cmx = cmtoX;
            }


            cmRun = cmRun * 9 / 10;
            if (cmRun < 100 && cmRun > -100)
                cmRun = 0;
        }
        if (cmx != cmtoX && !pointerIsDowning)
        {
            cmvx = (cmtoX - cmx) << 2;
            cmdx += cmvx;
            cmx += cmdx >> 4;
            cmdx = cmdx & 0xf;
        }
        if (cmy != cmtoY && !pointerIsDowning)
        {
            cmvy = (cmtoY - cmy) << 2;
            cmdy += cmvy;
            cmy += cmdy >> 4;
            cmdy = cmdy & 0xf;
        }
    }

    /**
     * 
     * @param nItem
     *            Số dòng có trong danh sách cuộn đứng, hoặc số cột nếu là danh
     *            sách cuộn ngang
     * 
     * @param ITEM_SIZE
     *            Kích thước tính theo Pixel của 1 dòng hoặc 1 cột
     * @param xPos
     *            Vị trí x (pixel) của Scroll so với màn hình
     * @param yPos
     *            Vị trí y (pixel) của Scroll so với màn hình
     * @param width
     *            Chiều rộng (pixel) của Scroll
     * @param height
     *            Chiều cao (pixel) của Scroll
     * @param styleUPDOWN
     *            =TRUE nếu là Scroll dạng đứng, =FALSE nếu là Scroll dạng ngang
     * @param ITEM_PER_LINE
     *            chỉ áp dụng nếu styleUPDOWN=true, dành cho danh sách dạng lưới
     *            (kiểu hành trang) Số cột của lưới. Nếu không phải dạng lưới mà
     *            là dạng dòng (kiểu friend list thì truyền số 1)
     */

    public void setStyle(int nItem, int ITEM_SIZE, int xPos, int yPos, int width, int height, bool styleUPDOWN,
        int ITEM_PER_LINE)
    {
        this.xPos = xPos;
        this.yPos = yPos;
        this.ITEM_SIZE = ITEM_SIZE;
        nITEM = nItem;
        this.width = width;
        this.height = height;
        this.styleUPDOWN = styleUPDOWN;
        this.ITEM_PER_LINE = ITEM_PER_LINE;

        if (styleUPDOWN)
            cmyLim = nItem * ITEM_SIZE - height;
        else
            cmxLim = nItem * ITEM_SIZE - width;

        if (cmyLim < 0) cmyLim = 0;
        if (cmxLim < 0) cmxLim = 0;
    }

    //LeftRight: xu ly cho dang luoi, scroll chieu ngang
    public void setStyle(int nItem, int ITEM_SIZE, int xPos, int yPos, int width, int height, bool styleUPDOWN,
        bool LeftRight, int ITEM_PER_LINE)
    {
        this.xPos = xPos;
        this.yPos = yPos;
        this.ITEM_SIZE = ITEM_SIZE;
        nITEM = nItem;
        this.width = width;
        this.height = height;
        this.styleUPDOWN = styleUPDOWN;
        this.ITEM_PER_LINE = ITEM_PER_LINE;
        styleLeftRight = LeftRight;


        cmyLim = nItem * ITEM_SIZE - width;

        if (cmyLim < 0) cmyLim = 0;
        if (cmxLim < 0) cmxLim = 0;
    }

    // Hàm này được gọi khi dùng PHÍM BẤM (không cảm ứng) thay đổi vị trí của
    // vệt chọn
    public void moveTo(int to)
    {
        if (!styleLeftRight)
        {
            if (styleUPDOWN)
            {
                to -= (height - ITEM_SIZE) / 2;
                cmtoY = to;
                if (cmtoY < 0)
                    cmtoY = 0;
                if (cmtoY > cmyLim)
                    cmtoY = cmyLim;
            }
            else
            {
                to -= (width - ITEM_SIZE) / 2;
                cmtoX = to;
                if (cmtoX < 0)
                    cmtoX = 0;
                if (cmtoX > cmxLim)
                    cmtoX = cmxLim;
            }
        }
        else //xu ly cho truong hop dang luoi va scroll ngang
        {
            to -= (width - ITEM_SIZE) / 2;
            cmtoX = to;
            if (cmtoX < 0)
                cmtoX = 0;
            if (cmtoX > cmxLim)
                cmtoX = cmxLim;
        }
    }

    public void setStyleWH(int nItem, int ITEM_SIZEW, int ITEM_SIZEH, int xPos, int yPos, int width, int height,
        bool styleUPDOWN, int ITEM_PER_LINE)
    {
        this.xPos = xPos;
        this.yPos = yPos;
        this.ITEM_SIZEW = ITEM_SIZEW;
        this.ITEM_SIZEH = ITEM_SIZEH;
        nITEM = nItem;
        this.width = width;
        this.height = height;
        this.styleUPDOWN = true;
        iswh = true;
        this.ITEM_PER_LINE = ITEM_PER_LINE;

        if (styleUPDOWN)
            cmyLim = nItem * ITEM_SIZEH - height;
        else
            cmxLim = nItem * ITEM_SIZEW - width;
        if (cmyLim < 0) cmyLim = 0;
        if (cmxLim < 0) cmxLim = 0;
    }
}