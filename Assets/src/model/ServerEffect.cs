using System;
using src.lib;
using src.main;
using src.screen;
using Char = src.Objectgame.Char;

public class ServerEffect : Effect
{
    private Char c;
    public EffectCharPaint eff;
    private long endTime;
    private int i0, x, y, dir = 1;

    public bool isloopForever;
    private short loopCount;

    public static void addServerEffect(int id, int cx, int cy, int loopCount)
    {
        var e = new ServerEffect();
        e.eff = GameScr.efs[id];
        e.x = cx;
        e.y = cy;
        e.loopCount = (short) loopCount;
        vEffect2.addElement(e);
    }

    public static void addServerEffect(int id, int cx, int cy, bool loopForever)
    {
        var e = new ServerEffect();
        e.eff = GameScr.efs[id];
        e.x = cx;
        e.y = cy;
        e.isloopForever = loopForever;
        vEffect2.addElement(e);
    }

    public static void addServerEffect(int id, int cx, int cy, int loopCount, sbyte dir)
    {
        try
        {
            var e = new ServerEffect();
            e.eff = GameScr.efs[id];
            e.x = cx;
            e.y = cy;
            e.loopCount = (short) loopCount;
            e.dir = dir;
            vEffect2.addElement(e);
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
    }

    public static void addServerEffect(int id, Char c, int loopCount)
    {
        var e = new ServerEffect();
        e.eff = GameScr.efs[id];
        e.c = c;
        e.loopCount = (short) loopCount;
        vEffect2.addElement(e);
    }

    public static void addServerEffect(int id, Char c, bool isForever)
    {
        var e = new ServerEffect();
        e.eff = GameScr.efs[id];
        e.c = c;
        e.isloopForever = isForever;
        vEffect2.addElement(e);
    }

    public static void addServerEffectWithTime(int id, int cx, int cy, int timeLengthInSecond)
    {
        var e = new ServerEffect();
        e.eff = GameScr.efs[id];
        e.x = cx;
        e.y = cy;
        e.endTime = MSystem.currentTimeMillis() + timeLengthInSecond * 1000;
        vEffect2.addElement(e);
    }

    public static void addServerEffectWithTime(int id, Char c, int timeLengthInSecond)
    {
        var e = new ServerEffect();
        e.eff = GameScr.efs[id];
        e.c = c;
        e.endTime = MSystem.currentTimeMillis() + timeLengthInSecond * 1000;
        vEffect2.addElement(e);
    }


    public override void paint(MGraphics g)
    {
        try
        {
            if (c != null)
            {
                x = c.cx;
                y = c.cy;
            }
            int xp, yp;
            xp = x + eff.arrEfInfo[i0].dx * dir;
            yp = y + eff.arrEfInfo[i0].dy;
            if (GameCanvas.isPaint(xp, yp))
                SmallImage.DrawSmallImage(g, eff.arrEfInfo[i0].idImg, xp, yp, dir == 1 ? 0 : 2,
                    MGraphics.VCENTER | MGraphics.HCENTER);
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
    }

    public override void update()
    {
        if (endTime != 0)
        {
            i0++;
            if (i0 >= eff.arrEfInfo.Length)
                i0 = 0;
            if (MSystem.currentTimeMillis() - endTime > 0)
                vEffect2.removeElement(this);
        }
        else if (!isloopForever)
        {
            i0++;
            if (i0 >= eff.arrEfInfo.Length)
            {
                loopCount--;
                if (loopCount <= 0)
                    vEffect2.removeElement(this);
                else
                    i0 = 0;
            }
        }
        else
        {
            i0++;
            if (i0 >= eff.arrEfInfo.Length)
                i0 = 0;
        }
        if (GameCanvas.gameTick % 11 == 0 && c != null && c != Char.myChar())
            if (!GameScr.vCharInMap.contains(c))
                vEffect2.removeElement(this);
    }

    public static void removeEffect(int id)
    {
        for (var i = 0; i < vEffect2.size(); i++)
            try
            {
                var l = (ServerEffect) vEffect2.elementAt(i);
                if (l.eff.indexEffect == id)
                {
                    vEffect2.removeElementAt(i);
                    i--;
                }
            }
            catch (Exception)
            {
            }
    }
}