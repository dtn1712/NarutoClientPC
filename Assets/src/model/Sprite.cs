using System;
using UnityEngine;

public class Sprite
{
    public const int TRANS_NONE = 0;
    public const int TRANS_ROT90 = 5;
    public const int TRANS_ROT180 = 3;
    public const int TRANS_ROT270 = 6;

    public const int TRANS_MIRROR = 2;
    public const int TRANS_MIRROR_ROT90 = 7;
    public const int TRANS_MIRROR_ROT180 = 1;

}