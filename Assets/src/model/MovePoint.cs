namespace src.model
{
    public class MovePoint
    {
        public int xEnd, yEnd, dir, status;

        public MovePoint(int xEnd, int yEnd, int status, int dir)
        {
            this.xEnd = xEnd;
            this.yEnd = yEnd;
            this.dir = dir;
            this.status = status;
        }

    }
}