using src.Gui;
using src.lib;
using src.main;
using src.screen;
using UnityEngine;

namespace src.model
{
    public abstract class TScreen
    {

        public static int ITEM_HEIGHT = 18 * MGraphics.zoomLevel;
        public static int cmdW = 70 * MGraphics.zoomLevel;
        public static int cmdH = 22;
        public static int keyTouch = -1;
        public static int height, width;
        public static TScreen lastScreen;
        public bool isConnect = false, connectCallBack = false, isCloseConnect = false;
        public Command left, center;
        public Command right, cmdClose;

        public virtual void switchToMe()
        {
            // chỉ vào chỗ này 
            GameCanvas.currentScreen = this;
            GameCanvas.clearKeyPressed();
            GameCanvas.clearPointerEvent();
            keyTouch = -1;
        }

        public virtual void keyPress(int keyCode)
        {
        }

        public virtual void update()
        {
        }

        public virtual void updateKey()
        {
            if (GameCanvas.keyPressed[5] || getCmdPointerLast(GameCanvas.currentScreen.center))
            {
                GameCanvas.keyPressed[5] = false;
                keyTouch = -1;
                GameCanvas.isPointerJustRelease = false;
                center?.performAction();
                
            }
            if (GameCanvas.keyPressed[12] || getCmdPointerLast(GameCanvas.currentScreen.left))
            {
                GameCanvas.keyPressed[12] = false;
                keyTouch = -1;
                GameCanvas.isPointerJustRelease = false;
                if (ChatTextField.GetInstance().isShow)
                {
                    if (ChatTextField.GetInstance().left != null) ChatTextField.GetInstance().left.performAction();
                }
                else
                {
                    left?.performAction();
                }
                
            }
            if (getCmdPointerLast(GameCanvas.currentScreen.right))
            {
                keyTouch = -1;
                GameCanvas.isPointerJustRelease = false;
                if (ChatTextField.GetInstance().isShow)
                {
                    if (ChatTextField.GetInstance().right != null) ChatTextField.GetInstance().right.performAction();
                }
                else
                {
                    right?.performAction();
                }

            }
        }


        public static bool getCmdPointerLast(Command cmd)
        {
            if (cmd == null)
                return false;

            if (GameScr.ispaintChat)
                if (cmd == GameScr.chat || cmd == GameScr.bntIconChat)
                    if (GameCanvas.isPointerHoldIn(cmd.x, cmd.y, cmd.w, cmd.h + 10))
                    {
                        keyTouch = 0;
                        if (GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease)
                            return cmd.input();
                    }

            if (GameCanvas.currentScreen == GameCanvas.languageScr)
                if (cmd == LanguageScr.cmdEng || cmd == LanguageScr.cmdVn)
                    if (GameCanvas.isPointerHoldIn(cmd.x, cmd.y, cmd.w, cmd.h + 10))
                    {
                        keyTouch = 0;
                        if (GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease)
                            return cmd.input();
                    }

            if (GameCanvas.isPointerHoldIn(cmd.x, cmd.y, cmd.w, cmd.h + 10))
            {
                keyTouch = 0;
                if (GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease)
                    return cmd.input();
            }
            return false;
        }

        public virtual void updatePointer()
        {
        }

        protected static void resetTranslate(MGraphics g)
        {
            g.translate(-g.getTranslateX(), -g.getTranslateY());
            g.setClip(0, 0, GameCanvas.w, GameCanvas.h);
        }

        public virtual void paint(MGraphics g)
        {
            g.translate(-g.getTranslateX(), -g.getTranslateY());
            g.setClip(0, 0, GameCanvas.w, GameCanvas.h + 1);
            GameCanvas.paintt.paintTabSoft(g);
        }
    }
}