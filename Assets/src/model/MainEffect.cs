using src.lib;

namespace src.model
{
    public class MainEffect : MainObject
    {
        public FrameImage fraImgEff;
        public int fRemove;
        private bool isPaint = true;
        private long timeBegin;
        public int typeEffect = 0;

        public virtual void paint(MGraphics g)
        {
            base.paint(g);
        }

        public virtual void update()
        {
            base.update();
        }

    }
}