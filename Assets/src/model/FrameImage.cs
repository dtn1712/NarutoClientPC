//quan ly cac frame anh duoc xep theo chieu doc

using src.lib;

namespace src.model
{
    public class FrameImage
    {
        public int frameHeight;
        public int frameWidth;

        public int Id;
        private MBitmap imgFrame;

        private MBitmap[] imgList;
        private bool isRotate;
        public int nFrame;
        private int[] pos;
        private readonly int totalHeight;


        public FrameImage(int ID, int width, int height)
        {
            // TODO Auto-generated constructor stub
            Id = ID;
            imgFrame = ImageEffect.setImage(ID);
            frameWidth = width;
            frameHeight = height;
            nFrame = Image.getHeight(imgFrame) / height;
            pos = new int[nFrame];
            for (var i = 0; i < nFrame; i++)
                pos[i] = i * height;
        }

        public void drawFrame(int idx, int x, int y, int trans, int anchor, MGraphics g)
        {
            if (imgFrame == null)
                imgFrame = ImageEffect.setImage(Id);
            else if (idx >= 0 && idx < nFrame)
                g.drawRegion(imgFrame, 0, pos[idx], frameWidth, frameHeight, trans, x, y, anchor);
        }

    }
}