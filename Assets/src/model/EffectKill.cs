using System;
using src.lib;
using src.Objectgame;
using src.screen;
using Char = src.Objectgame.Char;

namespace src.model
{
    public class EffectKill : MainEffect
    {
        public const sbyte EFF_NORMAL = 0;
        public const sbyte EFF_NORMAL_BAYNGANG = 1;
        public const sbyte EFF_CUONG_HOA = 2;

        private Char charbeAttack;

        private int colorBuff, indexColorStar;

        private int[] colorpaint;

        private int frameArrow = 0;

        private int gocArrow = 0;
        private bool isEff = false;
        public bool isSpeedUp = false;

        private int life, va;
        private int lT_Arc, gocT_Arc;

        private int[][] mTamgiac;

        private MainObject objKill, objBeKillMain;

        private long time;
        private byte timeAddNum;
        public int timeRemove, subType;

        private readonly Vector VecEff = new Vector();

        private readonly Vector VecSubEff = new Vector();

        private int vXTam, vYTam;

        private int x1000, y1000, vX1000, vY1000, xEff, yEff;

        // Crack_earth
        private int xline, yline;

        public int xold, yold;
        public int xold1, yold1;

        //
        private int yFly, ydArchor, yArchor, indexLan;

        public EffectKill(int typeKill, Mob mobAttack,
            Char charbeAttack,
            int subtype)
        {
            subType = subtype;
            SetNewEffect(typeKill, mobAttack, charbeAttack);
        }

        public EffectKill(int typeKill, int x, int y, int xTo, int yTo)
        {
            typeEffect = typeKill;
            switch (typeEffect)
            {
                case EFF_CUONG_HOA:
                    fraImgEff = new FrameImage(104, 11, 11);
                    this.x = x;
                    this.y = y;
                    toX = xTo;
                    toY = yTo;
                    vMax = 8 * 1000;
                    frame = CRes.random(0, 3);
                    break;
            }
        }

        private void SetNewEffect(int typeKill, Mob mobAttack, Char charbeAttack)
        {

            typeEffect = typeKill;
            switch (typeKill)
            {
                case EFF_NORMAL:
                case EFF_NORMAL_BAYNGANG:
                    fRemove = 60;
                    var mob = new MainObject();
                    mob.x = mobAttack.x;
                    mob.y = mobAttack.y;
                    mob.dir = mobAttack.dir;
                    mob.hOne = mobAttack.getH();

                    this.charbeAttack = charbeAttack;
                    var charr = new MainObject();
                    objBeKillMain = charr;
                    objBeKillMain.x = charbeAttack.cx;
                    objBeKillMain.y = charbeAttack.cy;
                    objBeKillMain.dir = charbeAttack.cdir;
                    objBeKillMain.hOne = 60;
                    objKill = mob;
                    x = mobAttack.x;
                    y = mobAttack.y - mobAttack.getH() / 2 - 5;
                    switch (subType)
                    {
                        case 0:
                            fraImgEff = new FrameImage(101, 14, 14);
                            break;
                        case 1:
                            fraImgEff = new FrameImage(98, 14, 14);
                            break;
                        case 2:
                            fraImgEff = new FrameImage(100, 14, 14);
                            break;
                        case 3:
                            fraImgEff = new FrameImage(99, 14, 14);
                            break;
                        case 4:
                            fraImgEff = new FrameImage(102, 14, 14);
                            break;
                        case 5:
                            fraImgEff = new FrameImage(32, 14, 14);
                            break;
                        default:
                            fraImgEff = new FrameImage(32, 14, 14);
                            break;
                    }

                    vMax = 8 * 1000;
                    CreateNormal();
                    y -= 10;
                    if (typeEffect == EFF_NORMAL_BAYNGANG)
                    {
                        gocT_Arc = objKill.x - objBeKillMain.x >= 0 ? 180 : 0;
                        xold = x;
                        yold = mobAttack.y - mobAttack.h / 2 - 5;
                        xold1 = objBeKillMain.x;
                        yold1 = objBeKillMain.y - objBeKillMain.hOne / 2;
                        x += CRes.xetVX(gocT_Arc, 6);
                        y += CRes.xetVY(gocT_Arc, 6);
                    }
                    break;
            }
        }

        public override void paint(MGraphics g)
        {
            try
            {
                if (isRemove)
                    return;
                switch (typeEffect)
                {
                    case EFF_NORMAL:
                    case EFF_NORMAL_BAYNGANG:
                        if (fraImgEff == null)
                            return;
                        fraImgEff.drawFrame(f / 2 % fraImgEff.nFrame, x, y, 0,
                            MGraphics.VCENTER | MGraphics.HCENTER, g);
                        break;
                    case EFF_CUONG_HOA:
                        if (fraImgEff == null)
                            return;
                        fraImgEff.drawFrame(frame % fraImgEff.nFrame, x, y, 0,
                            MGraphics.VCENTER | MGraphics.HCENTER, g);
                        break;
                }
            }
            catch (Exception e)
            {
                RemoveEff();
            }
        }

        public override void update()
        {
            if (isRemove)
                return;
            f++;
            if (objBeKillMain != null)
            {
                xEff = objBeKillMain.x;
                yEff = objBeKillMain.y - objBeKillMain.hOne / 2;
            }
            switch (typeEffect)
            {
                case EFF_NORMAL:
                    UpdateAngleNormal();
                    break;
                case EFF_NORMAL_BAYNGANG:

                    x += CRes.xetVX(gocT_Arc, 6);
                    y += CRes.xetVY(gocT_Arc, 6);
                    if (CRes.abs(objBeKillMain.x - x) < 8 || f > fRemove)
                    {
                        ServerEffect.addServerEffect(25, x, y, 1);
                        charbeAttack.DoInjure(1, 0, false, 1);
                        RemoveEff();
                    }
                    break;
                case EFF_CUONG_HOA:
                    int dx = toX - x, dy = toY - y;
                    if (CRes.abs(dx) < 16 && CRes.abs(dy) < 16)
                    {
                        RemoveEff();
                        return;
                    }
                    var a = CRes.angle(dx, dy);
                    if (CRes.abs(a - gocT_Arc) < 90 || dx * dx + dy * dy > 64 * 64)
                        if (CRes.abs(a - gocT_Arc) < 15)
                            gocT_Arc = a;
                        else if (a - gocT_Arc >= 0 && a - gocT_Arc < 180
                                 || a - gocT_Arc < -180)
                            gocT_Arc = CRes.fixangle(gocT_Arc + 15);
                        else
                            gocT_Arc = CRes.fixangle(gocT_Arc - 15);
                    if (!isSpeedUp)
                        if (va < 8 << 10)
                            va += 2048;
                    vX1000 = (va * CRes.cos(gocT_Arc)) >> 10;
                    vY1000 = (va * CRes.sin(gocT_Arc)) >> 10;

                    dx += vX1000;
                    var deltaX = dx >> 10;
                    x += deltaX;
                    dx = dx & 0x3ff;
                    dy += vY1000;
                    var deltaY = dy >> 10;
                    y += deltaY;
                    dy = dy & 0x3ff;
                    break;
            }
            base.update();
            if (f > 200)
                RemoveEff();
        }


        private void RemoveEff()
        {
            GameScr.veffClient.removeElement(this);
            if (VecEff.size() > 0)
                VecEff.removeAllElements();
            if (VecSubEff.size() > 0)
                VecSubEff.removeAllElements();
        }

        private void CreateNormal()
        {
            if (objKill == null)
            {
                gocT_Arc = 0;
            }
            else
            {
                switch (objKill.dir)
                {
                    case 0: // d
                        gocT_Arc = 90;
                        break;
                    case 1: // u
                        gocT_Arc = 270;
                        break;
                    case 2: // l
                        gocT_Arc = 180;
                        break;
                    case 3: // r
                        gocT_Arc = 0;
                        break;
                }
                if (CRes.abs(objBeKillMain.y - objKill.y) <= 28)
                    typeEffect = EFF_NORMAL_BAYNGANG;
            }
            va = 256 * 16;
            vx = 0;
            vy = 0;
            life = 0;
            vX1000 = (va * CRes.cos(gocT_Arc)) >> 10;
            vY1000 = (va * CRes.sin(gocT_Arc)) >> 10;
        }

        private void UpdateAngleNormal()
        {
            int dx, dy;
            int a;
            if (charbeAttack == null)
            {
                RemoveEff();
                return;
            }
            dx = charbeAttack.cx - x;
            dy = charbeAttack.cy - (objBeKillMain.hOne >> 1) - y + objKill.hOne / 2;
            life++;
            if (CRes.abs(dx) < 16 && CRes.abs(dy) < 16 || life > fRemove)
            {
                charbeAttack.DoInjure(1, 0, false, 1);
                ServerEffect.addServerEffect(25, x, y, 1);
                RemoveEff();
                return;
            }
            a = CRes.angle(dx, dy);
            if (CRes.abs(a - gocT_Arc) < 90 || dx * dx + dy * dy > 64 * 64)
                if (CRes.abs(a - gocT_Arc) < 15)
                    gocT_Arc = a;
                else if (a - gocT_Arc >= 0 && a - gocT_Arc < 180
                         || a - gocT_Arc < -180)
                    gocT_Arc = CRes.fixangle(gocT_Arc + 15);
                else
                    gocT_Arc = CRes.fixangle(gocT_Arc - 15);
            if (!isSpeedUp)
                if (va < 8 << 10)
                    va += 2048;
            vX1000 = (va * CRes.cos(gocT_Arc)) >> 10;
            vY1000 = (va * CRes.sin(gocT_Arc)) >> 10;
  
            dx += vX1000;
            var deltaX = dx >> 10;
            x += deltaX;
            dx = dx & 0x3ff;
            dy += vY1000;
            var deltaY = dy >> 10;
            y += deltaY;
            dy = dy & 0x3ff;
        }
    }
}