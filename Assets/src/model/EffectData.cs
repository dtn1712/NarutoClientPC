using System;
using src.lib;
using UnityEngine;

namespace src.model
{
    public class EffectData
    {
        public short[] arrFrame;
        public short[][] arrFrameMove = new short[6][];
        public Frame[] frame;
        public MBitmap img;
        public ImageInfo[] imgInfo;

        public int width, height;

        public ImageInfo getImageInfo(sbyte id)
        {
            for (var i = 0; i < imgInfo.Length; i++)
                if (imgInfo[i].ID == id)
                    return imgInfo[i];
            return null;
        }


        public void readhd(DataInputStream iss)
        {
            try
            {
                var nFramestand = iss.ReadByte(); // 0
                arrFrameMove[0] = new short[nFramestand];
                if (nFramestand > 0)
                    for (var i = 0; i < nFramestand; i++)
                        arrFrameMove[0][i] = iss.ReadByte();
                var nFramewalk = iss.ReadByte(); //1
                if (nFramewalk > 0)
                {
                    arrFrameMove[1] = new short[nFramewalk];
                    for (var i = 0; i < nFramewalk; i++)
                        arrFrameMove[1][i] = iss.ReadByte();
                }
                var nFrameAttack1 = iss.ReadByte(); //2
                if (nFrameAttack1 > 0)
                {
                    arrFrameMove[2] = new short[nFrameAttack1];
                    for (var i = 0; i < nFrameAttack1; i++)
                        arrFrameMove[2][i] = iss.ReadByte();
                }
                var nFrameAttack2 = iss.ReadByte(); //3
                if (nFrameAttack2 > 0)
                {
                    arrFrameMove[3] = new short[nFrameAttack2];
                    for (var i = 0; i < nFrameAttack2; i++)
                        arrFrameMove[3][i] = iss.ReadByte();
                }
                var nFramesit = iss.ReadByte(); //4
                if (nFramesit > 0)
                {
                    arrFrameMove[4] = new short[nFramesit];
                    for (var i = 0; i < nFramesit; i++)
                        arrFrameMove[4][i] = iss.ReadByte();
                }
                var nFrameDead = iss.ReadByte(); //5
                if (nFrameDead > 0)
                {
                    arrFrameMove[5] = new short[nFramesit];
                    for (var i = 0; i < nFrameDead; i++)
                        arrFrameMove[5][i] = iss.ReadByte();
                }
                var nFrameAttack = iss.ReadByte(); //6
                if (nFrameAttack > 0)
                    for (var i = 0; i < nFrameAttack; i++)
                    {
                        var frame = iss.ReadByte();
                    }
                var nFrameBullet1 = iss.ReadByte(); //7
                if (nFrameBullet1 > 0)
                    for (var i = 0; i < nFrameBullet1; i++)
                    {
                        var frame = iss.ReadByte();
                    }
                var nFrameBullet2 = iss.ReadByte(); //8
                if (nFrameBullet2 > 0)
                    for (var i = 0; i < nFrameBullet2; i++)
                    {
                        var frame = iss.ReadByte();
                    }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        public void readData222(DataInputStream iss)
        {
            try
            {
                int left = 0, top = 0, right = 0, bottom = 0;
                if (iss != null)
                {
                    var smallNumImg = iss.ReadByte();
                    imgInfo = new ImageInfo[smallNumImg];
                    for (var i = 0; i < smallNumImg; i++)
                    {
                        imgInfo[i] = new ImageInfo();
                        imgInfo[i].ID = iss.ReadByte();
                        imgInfo[i].x0 = iss.ReadUnsignedByte();
                        imgInfo[i].y0 = iss.ReadUnsignedByte();
                        imgInfo[i].w = iss.ReadUnsignedByte();
                        imgInfo[i].h = iss.ReadUnsignedByte();

                    }
                    var nFrame = iss.ReadByte();
                    frame = new Frame[nFrame];

                    for (var i = 0; i < nFrame; i++)
                    {
                        frame[i] = new Frame();
                        var nPart = iss.ReadByte();

                        frame[i].dx = new short[nPart];
                        frame[i].dy = new short[nPart];
                        frame[i].idImg = new sbyte[nPart];
                        for (var a = 0; a < nPart; a++)
                        {
                            frame[i].dx[a] = iss.ReadByte();
                            frame[i].dy[a] = iss.ReadByte();
                            frame[i].idImg[a] = iss.ReadByte();

                        }
                    }
                    short nFrameCount = iss.ReadByte();
                    arrFrame = new short[nFrameCount];
                    for (var i = 0; i < nFrameCount; i++)
                        arrFrame[i] = iss.ReadByte();
                }

                width = iss.ReadByte();
                height = iss.ReadByte();
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        public void paintFrame(MGraphics g, int f, int x, int y, int trans, int layer)
        {
            if (frame != null && frame.Length != 0)
            {
                var fr = frame[f];
                for (var i = 0; i < fr.dx.Length; i++)
                {
                    var im = getImageInfo(fr.idImg[i]);
                    try
                    {
                        if (trans == 0)
                            g.drawRegion(img, im.x0, im.y0, im.w, im.h, 0, x + fr.dx[i], y + fr.dy[i]
                                                                                         - (layer < 4 && layer > 0
                                                                                             ? trans
                                                                                             : 0), 0);
                        if (trans == 1)
                            g.drawRegion(img, im.x0, im.y0, im.w, im.h, Sprite.TRANS_MIRROR, x - fr.dx[i], y + fr.dy[i]
                                                                                                           - (layer < 4 &&
                                                                                                              layer > 0
                                                                                                               ? trans
                                                                                                               : 0),
                                StaticObj.TOP_RIGHT);
                    }
                    catch (Exception e)
                    {
                        Debug.LogError(e);
                    }
                }
            }
        }
    }
}