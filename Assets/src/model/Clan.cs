using System;
using src.lib;
using src.Objectgame;

namespace src.model
{
    public class Clan
    {
        public const int CREATE_CLAN = 0;
        public const int MOVE_OUT_MEM = 1;
        public const int MOVE_INPUT_MONEY = 2;
        public const int MOVE_OUT_MONEY = 3;
        public const int FREE_MONEY = 4;
        public const int UP_LEVEL = 5;

        public const int TYPE_NORMAL = 0;
        public const int TYPE_UUTU = 1;
        public const int TYPE_TRUONGLAO = 2;
        public const int TYPE_TOCPHO = 3;
        public const int TYPE_TOCTRUONG = 4;
        public const int INVITE = 1, KICKED = 2, LEAVE = 3;
        public const int kickGlobal = 1, kickLocal = 0, leaveGlobal = 1, leaveLocal = 0, inviteLocal = 4, inviteGlobal = 11;

        public static int typeKick, typeLeave;
        public static int idClan;
        public string alert = "";
        public string assist_name = "";
        public int coin, freeCoin, coinUp;
        public string elder1_name = "";
        public string elder2_name = "";
        public string elder3_name = "";
        public string elder4_name = "";
        public string elder5_name = "";
        public int exp, expNext;
        public int icon;
        public Item[] items;
        public int level, itemLevel;
        public string log = "";
        public string main_name = "";
        public Vector members = new Vector();


        public string name = "";
        public int openDun;
        public string reg_date = "";
        public int total, use_card;

        public void writeLog(string data)
        {
            var strs = Utils.Split(data, "\n");
            log = "";
            try
            {
                for (var i = 0; i < strs.Length; i++)
                {
                    var str = strs[i].Trim();
                    if (!str.Equals(""))
                        try
                        {
                            var datas = Utils.Split(str, ",");
                            var value = datas[0];
                            var type = int.Parse(datas[1]);
                            if (type == CREATE_CLAN)
                            {
                                value = "c0" + value;
                                value += MResources.CLAN_ACTIVITY[1] + " " + Utils.NumberToString(datas[2]) + " " +
                                         MResources.CLAN_ACTIVITY[0] + " " + datas[3];
                            }
                            else if (type == MOVE_OUT_MEM)
                            {
                                value = "c1" + value;
                                value += " " + MResources.CLAN_ACTIVITY[2] + " " + Utils.NumberToString(datas[2]) + " " +
                                         MResources.CLAN_ACTIVITY[0] + " " + datas[3];
                            }
                            else if (type == MOVE_INPUT_MONEY)
                            {
                                value = "c2" + value;
                                value += " " + MResources.CLAN_ACTIVITY[3] + " " + Utils.NumberToString(datas[2]) + " " +
                                         MResources.CLAN_ACTIVITY[0] + " " + datas[3];
                            }
                            else if (type == MOVE_OUT_MONEY)
                            {
                                value = "c1" + value;
                                value += " " + MResources.CLAN_ACTIVITY[4] + " " + Utils.NumberToString(datas[2]) + " " +
                                         MResources.CLAN_ACTIVITY[0] + " " + datas[3];
                            }
                            else if (type == FREE_MONEY)
                            {
                                value = "c1" + value;
                                value += MResources.CLAN_ACTIVITY[5] + " " + Utils.NumberToString(datas[2]) + " " +
                                         MResources.CLAN_ACTIVITY[0] + " " + datas[3];
                            }
                            else if (type == UP_LEVEL)
                            {
                                value = "c2" + value;
                                value += " " + MResources.CLAN_ACTIVITY[6] + " " + Utils.NumberToString(datas[2]) + " " +
                                         MResources.CLAN_ACTIVITY[0] + " " + datas[3];
                            }

                            log += value + "\n";
                        }
                        catch (Exception e)
                        {
                        }
                }
            }
            catch (Exception e)
            {
            }
        }
    }
}