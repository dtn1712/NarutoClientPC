using src.lib;
using src.main;
using src.model;
using src.Objectgame;
using src.screen;

public class ChatPopup : Effect, IActionListener
{
    public static ChatPopup currentMultilineChatPopup;
    public Char c;
    public Command cmdNextLine;
    private int currentLine;

    private int cx, cy, ch;

    private string[] lines;
    private bool outSide;
    public string[] says;
    public int sayWidth = 100, delay, sayRun;

    public void perform(int idAction, object p)
    {
        if (idAction == 8000)
        {
            var currLine = currentMultilineChatPopup.currentLine;
            currLine++;
            if (currLine >= currentMultilineChatPopup.lines.Length)
            {
                currentMultilineChatPopup.c.chatPopup = null;
                currentMultilineChatPopup = null;
                return; // END
            }
            var cp = addChatPopup(currentMultilineChatPopup.lines[currLine], currentMultilineChatPopup.delay,
                currentMultilineChatPopup.c);
            cp.currentLine = currLine;
            cp.lines = currentMultilineChatPopup.lines;
            cp.cmdNextLine = currentMultilineChatPopup.cmdNextLine;
            currentMultilineChatPopup = cp;
        }
    }



    public static ChatPopup addChatPopup(string chat, int howLong, Char c)
    {
        var cp = new ChatPopup();
        if (chat.Length < 10)
            cp.sayWidth = 64;
        if (GameCanvas.w == 128)
            cp.sayWidth = 128;
        cp.says = FontManager.GetInstance().tahoma_7_red.SplitFontArray(chat, cp.sayWidth - 10);
        cp.delay = howLong;
        cp.c = c;
        cp.cx = c.cx;
        cp.cy = c.cy;
        c.chatPopup = cp;
        cp.sayRun = 7;
        if (c != null)
        {
            cp.cx = c.cx;
            cp.cy = c.cy;
            cp.ch = c.ch - 15;
        }
        vEffect2.addElement(cp);
        return cp;
    }


    public override void update()
    {
        if (delay > 0)
            delay--;

        if (c != null)
        {
            cx = c.cx;
            cy = c.cy;
            ch = c.ch - 5;
        }
        if (sayRun > 1)
            sayRun--;

        if (c != null && c.chatPopup != null && c.chatPopup != this || c != null && c.chatPopup == null ||
            delay == 0)
        {
            vEffect2Outside.removeElement(this);
            vEffect2.removeElement(this);
        }
    }

    public override void paint(MGraphics g)
    {
        var cx = this.cx;
        var cy = this.cy;
        if (outSide)
        {
            cx -= GameScr.cmx;
            cy -= GameScr.cmy;
            cy += 35;
        }
        g.setColor(0x000000);
        g.fillRect(cx - sayWidth / 2 + 2, cy - ch - 15 + sayRun - says.Length * 12 - 10, sayWidth - 4,
            (says.Length + 1) * 12 - 1 + 2);
        g.fillRect(cx - sayWidth / 2 - 1, cy - ch - 15 + sayRun - says.Length * 12 - 7, sayWidth + 2,
            (says.Length + 1) * 12 - 5);

        g.setColor(0xFFFFFF);
        g.fillRect(cx - sayWidth / 2, cy - ch - 15 + sayRun - says.Length * 12 - 9, sayWidth,
            (says.Length + 1) * 12 - 1);


        g.drawImage(LoadImageInterface.imgGocChat, cx - sayWidth / 2 - 1, cy - ch - 15 + sayRun - says.Length * 12 - 10,
            MGraphics.TOP | MGraphics.LEFT, true);
        g.drawRegion(LoadImageInterface.imgGocChat, 0, 0, LoadImageInterface.imgGocChat.GetWidth(),
            LoadImageInterface.imgGocChat.GetHeight(), 2,
            cx - sayWidth / 2 - 1 + sayWidth + 2 - LoadImageInterface.imgGocChat.GetWidth(),
            cy - ch - 15 + sayRun - says.Length * 12 - 10, MGraphics.TOP | MGraphics.LEFT, true);
        g.drawRegion(LoadImageInterface.imgGocChat, 0, 0, LoadImageInterface.imgGocChat.GetWidth(),
            LoadImageInterface.imgGocChat.GetHeight(), 6,
            cx - sayWidth / 2 - 1,
            cy - ch - 15 + sayRun - says.Length * 12 - 10 + (says.Length + 1) * 12 -
            LoadImageInterface.imgGocChat.GetHeight(), MGraphics.TOP | MGraphics.LEFT, true);
        g.drawRegion(LoadImageInterface.imgGocChat, 0, 0, LoadImageInterface.imgGocChat.GetWidth(),
            LoadImageInterface.imgGocChat.GetHeight(), 3,
            cx - sayWidth / 2 - 1 + sayWidth + 2 - LoadImageInterface.imgGocChat.GetWidth(),
            cy - ch - 15 + sayRun - says.Length * 12 - 10 + (says.Length + 1) * 12 -
            LoadImageInterface.imgGocChat.GetHeight() + 1, MGraphics.TOP | MGraphics.LEFT, true);

        g.drawImage(LoadImageInterface.imgGoc, cx - sayWidth / 4,
            cy - ch - 15 + sayRun + LoadImageInterface.imgGoc.GetHeight() / 2 - 1, MGraphics.TOP | MGraphics.LEFT,
            true);

        for (var i = 0; i < says.Length; i++)
            FontManager.GetInstance().tahoma_7.DrawString(g, says[i], cx, cy - ch - 15 + sayRun + i * 12 - says.Length * 12 - 4, 2);
    }

}