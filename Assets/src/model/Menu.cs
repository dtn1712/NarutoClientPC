﻿using src.lib;
using src.main;
using src.model;
using src.Objectgame;
using src.real;
using src.screen;

public class Menu : IActionListener
{
    public Scroll scrMain = new Scroll(); // them scroll de keo 
    public const sbyte MENULIST = 0;
    public const sbyte NPC = 1;
    public static int[] menuTemY;
    public static int cmtoX, cmx, cmdy, cmvy, cmxLim, xc;

    public static int ww;
    public static MBitmap nameNpc, decor, npctest, imgRightArrow, imgRightArrowFocus;
    public static MBitmap[] icn_focus = new MBitmap[6];

    private readonly Command center = null;

    private int cmvx, cmdx;

    private bool disableClose;

    private bool isClose;
    public bool[] isNotClose;
    private readonly Command left = new Command(MResources.SELECT, 0);
    public Vector menuItems;
    public int menuSelectedItem, wTextPaintNpc;
    public int menuX, menuY, menuW, menuH /*, menuTemY*/;
    public int menuXNPC, menuYNPC, menuWNPC, menuHNPC /*, menuTemY*/;
    private Command next;

    public Npc npc;

    private int pa = 0;
    private int pointerDownFirstX;
    private readonly int[] pointerDownLastX = new int[3];

    private int pointerDownTime;
    private bool pointerIsDowning, isDownWhenRunning;

    private readonly Command right = GameCanvas.isTouch
        ? null
        : new Command(MResources.CLOSE, 0, GameCanvas.w - 71, GameCanvas.h - TScreen.cmdH + 1);

    public bool showMenu;

    public int tDelay;
    public string[] textNpc;
    public long timeCownDowrequestImgNPC = 5000;
    public long timeCurrent;
    private bool touch, close;
    private bool trans = false;
    public sbyte typeMenu;
    private int waitToPerform, cmRun;
    private bool wantUpdateList;

    public void perform(int idAction, object p)
    {
        if (idAction == 0) doCloseMenu();
        if (idAction == 1000)
        {
            var c = (ChatTab) p;
            menuItems.removeAllElements();
            ChatManager.gI().removeFromWaitList(c.ownerName);
            ChatManager.gI().chatTabs.removeElement(c);
            for (var i = 0; i < ChatManager.gI().chatTabs.size(); i++)
            {
                var tab = (ChatTab) ChatManager.gI().chatTabs.elementAt(i);
                menuItems.addElement(new Command(tab.ownerName, null, 12001, i + ""));
            }
            menuItems.addElement(new Command(MResources.BLOCK_MESSAGE, null, 12006, null));
            menuItems.addElement(new Command(MResources.CHAT_ADMIN, null, 12008, null));
            for (var i = 0; i < menuItems.size(); i++)
            {
                var c1 = (Command) menuItems.elementAt(i);
                var w = FontManager.GetInstance().tahoma_7_yellow.GetWidth(c1.caption);
                if (w > menuW - 8)
                    c1.subCaption = FontManager.GetInstance().tahoma_7_yellow.SplitFontArray(c1.caption, menuW - 8);
            }

            cmxLim = menuItems.size() * menuW - GameCanvas.w;
            cmtoX = menuSelectedItem * menuW + menuW - GameCanvas.w / 2;
            if (cmtoX > cmxLim)
                cmtoX = cmxLim;
            if (cmtoX < 0)
                cmtoX = 0;
            if (menuSelectedItem == menuItems.size() - 1
                || menuSelectedItem == 0)
                cmx = cmtoX;
        }
    }

    public void startAt(Vector menuItems)
    {
        if (showMenu)
            return;
        npc = null;
        npctest = null;
        typeMenu = MENULIST;
        isClose = false;
        touch = false;
        close = false;
        isNotClose = new bool[menuItems.size()];
        for (var i = 0; i < isNotClose.Length; i++)
            isNotClose[i] = false;
        disableClose = false;
        ChatPopup.currentMultilineChatPopup = null;
        InfoDlg.hide();

        tDelay = 0;
        if (menuItems.size() == 0)
            return;
        this.menuItems = menuItems;
        menuW = 64;

        menuH = 25;

        for (var i = 0; i < menuItems.size(); i++)
        {
            var c = (Command) menuItems.elementAt(i);
            var w = FontManager.GetInstance().tahoma_7_white.GetWidth(c.caption);
            if (w > menuW - 8)
                c.subCaption = FontManager.GetInstance().tahoma_7_yellow.SplitFontArray(c.caption, menuW - 8);
        }
        menuTemY = new int[menuItems.size()];
        menuX = (GameCanvas.w - menuItems.size() * menuW) / 2;
        if (menuX < 1)
            menuX = 1;
        menuY = GameCanvas.h - menuH - (Paint.hTab + 1);
        if (GameCanvas.isTouch)
            menuY -= 3;
        //menuTemY = menuY;// GameCanvas.h;
        menuY += 27;
        for (var i = 0; i < menuTemY.Length; i++)
            menuTemY[i] = GameCanvas.h;
        showMenu = true;
        menuSelectedItem = 0;

        cmxLim = this.menuItems.size() * menuW - GameCanvas.w;

        if (cmxLim < 0)
            cmxLim = 0;
        cmtoX = 0;
        cmx = 0;
        xc = 50;

        ww = menuItems.size() * menuW - 1;
        if (ww > GameCanvas.w - 2)
            ww = GameCanvas.w - 2;

        if (GameCanvas.isTouch)
            menuSelectedItem = -1;
        TScreen.keyTouch = -1;
    }


    public void startAtNPC(Vector menuItems, int pos, int idiconNpc, Npc npcc, string text)
    {
        if (showMenu)
            return;

        timeCurrent = 0;
        npc = null;
        npc = npcc;
        npctest = null;
        typeMenu = NPC;
        isClose = false;
        touch = false;
        close = false;
        isNotClose = new bool[menuItems.size()];
        for (var i = 0; i < isNotClose.Length; i++)
        {
            isNotClose[i] = false;
        }
            
        disableClose = false;
        ChatPopup.currentMultilineChatPopup = null;
        InfoDlg.hide();
        menuXNPC = 40;

        wTextPaintNpc = GameCanvas.w - menuXNPC * 2;
        tDelay = 0;
        if (next != null)
        {
            next.isNoPaintImage = false;
        }
        
        this.menuItems = menuItems;
        if (menuItems.size() < 4)
        {
            menuW = 60;
            menuH = 60;
            menuXNPC = 30;
            menuYNPC = GameCanvas.h - 50;
            menuWNPC = GameCanvas.w - 60;
            menuHNPC = 70;
        }
        else
        {
            menuW = 60;
            menuH = 60;
            menuXNPC = 30;
            menuYNPC = GameCanvas.h - 100;
            menuWNPC = GameCanvas.w - 60;
            menuHNPC = 120;
        }
 
        for (var i = 0; i < menuItems.size(); i++)
        {
        
            var c = (Command) menuItems.elementAt(i);
            c.x = menuXNPC + menuWNPC - 20;
            
            c.y = menuYNPC + menuHNPC / 2 - 20;
            if (c.caption.Length == 0)
            {
                c.img = imgRightArrow;
                c.imgFocus = imgRightArrowFocus;
                next = c;
            }
            else
            {
                if (menuItems.size() < 4)
                {
                    var wCmd = LoadImageInterface.img_use.GetWidth();
                    c.x = menuXNPC + menuWNPC / 2
                          - this.menuItems.size() * wCmd / 2 - (this.menuItems.size() - 1) * wCmd / 2 / 2
                          + i * (wCmd + wCmd / 2);
                    c.y = menuYNPC + menuHNPC / 2 - 20 + (c.caption.Equals("OK") ? 10 : 0);
                    c.img = LoadImageInterface.img_use;
                    c.imgFocus = LoadImageInterface.img_use_focus;
                    c.w = MGraphics.getImageWidth(LoadImageInterface.img_use);
                    next = c;
                    if (!next.caption.Equals("Nhiệm vụ") && !next.caption.Equals("OK"))
                        c.h = MGraphics.getImageHeight(LoadImageInterface.img_use);
                    else if (next.caption.Equals("OK"))
                        next = null;
                }
                else
                {
                    var wCmd = LoadImageInterface.img_use.GetWidth();
                    if (i < 3)
                    {
                        c.x = menuXNPC + menuWNPC / 2
                              - this.menuItems.size() * wCmd / 2 - (this.menuItems.size() - 1) * wCmd / 2 / 2
                              + i * (wCmd + wCmd / 2) + 100;
                        c.y = menuYNPC + menuHNPC / 2 - 20 + (c.caption.Equals("OK") ? 10 : 0) - 20;
                    }
                    else
                    {
                        c.x = menuXNPC + menuWNPC / 2
                              - this.menuItems.size() * wCmd / 2 - (this.menuItems.size() - 1) * wCmd / 2 / 2
                              + i * (wCmd + wCmd / 2) - 140;
                        c.y = menuYNPC + menuHNPC / 2 - 20 + (c.caption.Equals("OK") ? 10 : 0) + 20;
                    }
                    
                    c.img = LoadImageInterface.img_use;
                    c.imgFocus = LoadImageInterface.img_use_focus;
                    c.w = MGraphics.getImageWidth(LoadImageInterface.img_use);
                    next = c;
                    if (!next.caption.Equals("Nhiệm vụ") && !next.caption.Equals("OK"))
                        c.h = MGraphics.getImageHeight(LoadImageInterface.img_use);
                    else if (next.caption.Equals("OK"))
                        next = null;
                }
               
            }

        }
        if (menuItems.size() == 0 && pos == 0)
        {
            next = new Command("", this, 0, null)
            {
                x = menuXNPC + menuWNPC - 20,
                y = menuYNPC + menuHNPC / 2 - 20
            };
            if (next.caption.Length == 0)
            {
                next.img = imgRightArrow;
                next.imgFocus = imgRightArrowFocus;
            }
        }
        textNpc = FontManager.GetInstance().tahoma_7_white.SplitFontArray(text, wTextPaintNpc);

        menuTemY = new int[menuItems.size()];
        menuX = (GameCanvas.w - menuItems.size() * menuW) / 2;
        if (menuX < 1)
            menuX = 1;
        menuY = GameCanvas.h - menuH - (Paint.hTab + 1);
        if (GameCanvas.isTouch)
            menuY -= 3;
        //menuTemY = menuY;// GameCanvas.h;
        menuY += 27;
        for (var i = 0; i < menuTemY.Length; i++)
            menuTemY[i] = GameCanvas.h;
        showMenu = true;
        menuSelectedItem = 0;

        cmxLim = this.menuItems.size() * menuW - GameCanvas.w;

        if (cmxLim < 0)
            cmxLim = 0;
        cmtoX = 0;
        cmx = 0;
        xc = 50;

        ww = menuItems.size() * menuW - 1;
        if (ww > GameCanvas.w - 2)
            ww = GameCanvas.w - 2;

        if (GameCanvas.isTouch)
            menuSelectedItem = -1;
        TScreen.keyTouch = -1;
    }


    public void updateMenuKey()
    {
        if (!showMenu)
            return;
        if (GameCanvas.keyPressed[13])
        {
            doCloseMenu();
            GameCanvas.keyPressed[13] = false;
            return;
        }
        if (isScrolling())
            return;
        var changeFocus = false;
        if (GameCanvas.keyPressed[2] || GameCanvas.keyPressed[4])
        {
            changeFocus = true;
            menuSelectedItem--;
            if (menuSelectedItem < 0)
                menuSelectedItem = menuItems.size() - 1;
        }
        else if (GameCanvas.keyPressed[8] || GameCanvas.keyPressed[6])
        {
            changeFocus = true;
            menuSelectedItem++;
            if (menuSelectedItem > menuItems.size() - 1)
                menuSelectedItem = 0;
        }
        else if (GameCanvas.keyPressed[5])
        {
            if (center != null)
            {
                if (center.idAction > 0)
                    if (center.actionListener == GameScr.GetInstance())
                        GameScr.GetInstance().actionPerform(center.idAction, center.p);
                    else
                        perform(center.idAction, center.p);
            }
            else
            {
                waitToPerform = 2;
            }
            // fire = true;
            // isClose = true;
        }
        else if (GameCanvas.keyPressed[12])
        {
            if (isScrolling())
                return;
            if (left.idAction > 0)
                perform(left.idAction, left.p);
            else
                waitToPerform = 2;
        }
        else if (!disableClose && (GameCanvas.keyPressed[13] || TScreen.getCmdPointerLast(right)))
        {
            if (isScrolling())
                return;

            if (!close)
                close = true;
            
            isClose = true;
        }
        
        if (changeFocus)
        {
            cmtoX = menuSelectedItem * menuW + menuW - GameCanvas.w / 2;
            if (cmtoX > cmxLim)
                cmtoX = cmxLim;
            if (cmtoX < 0)
                cmtoX = 0;
            if (menuSelectedItem == menuItems.size() - 1 || menuSelectedItem == 0)
                cmx = cmtoX;
        }

        if (!disableClose && GameCanvas.isPointerJustRelease && !GameCanvas.isPointer(menuX, menuY, ww, menuH)
            && !pointerIsDowning)
        {
            if (isScrolling())
                return;
            pointerDownTime = pointerDownFirstX = 0;
            pointerIsDowning = false;
            isClose = true;
            close = true;
            return;
        }

        if (GameCanvas.isPointerDown)
            if (!pointerIsDowning && GameCanvas.isPointer(menuX, menuY, ww, menuH))
            {
                for (var i = 0; i < pointerDownLastX.Length; i++)
                    pointerDownLastX[0] = GameCanvas.px;
                pointerDownFirstX = GameCanvas.px;
                pointerIsDowning = true;
                isDownWhenRunning = cmRun != 0;
                cmRun = 0;
            }
            else if (pointerIsDowning)
            {
                pointerDownTime++;
                if (pointerDownTime > 5 && pointerDownFirstX == GameCanvas.px && !isDownWhenRunning)
                {
                    pointerDownFirstX = -1000;
                    menuSelectedItem = (cmtoX + GameCanvas.px - menuX) / menuW;
                }
                var dx = GameCanvas.px - pointerDownLastX[0];
                if (dx != 0 && menuSelectedItem != -1)
                    menuSelectedItem = -1;
                for (var i = pointerDownLastX.Length - 1; i > 0; i--)
                    pointerDownLastX[i] = pointerDownLastX[i - 1];
                pointerDownLastX[0] = GameCanvas.px;

                cmtoX -= dx;
                if (cmtoX < 0)
                    cmtoX = 0;
                if (cmtoX > cmxLim)
                    cmtoX = cmxLim;
                if (cmx < 0 || cmx > cmxLim)
                    dx /= 2;
                cmx -= dx;

                if (cmx < -(GameCanvas.h / 3))
                    wantUpdateList = true;
                else
                    wantUpdateList = false;
            }
        if (GameCanvas.isPointerJustRelease && pointerIsDowning)
        {
            var dx = GameCanvas.px - pointerDownLastX[0];
            GameCanvas.isPointerJustRelease = false;

            if (CRes.abs(dx) < 20 && CRes.abs(GameCanvas.px - pointerDownFirstX) < 20 && !isDownWhenRunning)
            {
                cmRun = 0;
                cmtoX = cmx;
                pointerDownFirstX = -1000;
                menuSelectedItem = (cmtoX + GameCanvas.px - menuX) / menuW;

                pointerDownTime = 0;
                waitToPerform = 10;
            }
            else if (menuSelectedItem != -1 && pointerDownTime > 5)
            {
                pointerDownTime = 0;
                waitToPerform = 1;
            }
            else if (menuSelectedItem == -1 && !isDownWhenRunning)
            {
                if (cmx < 0)
                {
                    cmtoX = 0;
                }
                else if (cmx > cmxLim)
                {
                    cmtoX = cmxLim;
                }
                else
                {
                    var s = GameCanvas.px - pointerDownLastX[0] + (pointerDownLastX[0] - pointerDownLastX[1]) +
                            (pointerDownLastX[1] - pointerDownLastX[2]);
                    if (s > 10)
                        s = 10;
                    else if (s < -10)
                        s = -10;
                    else
                        s = 0;
                    cmRun = -s * 100;
                }
            }
            pointerIsDowning = false;
            pointerDownTime = 0;
            GameCanvas.isPointerJustRelease = false;
        }

        GameCanvas.clearKeyPressed();
        GameCanvas.clearKeyHold();
    }

    private void moveCamera()
    {
        if (cmRun != 0 && !pointerIsDowning)
        {
            cmtoX += cmRun / 100;
            if (cmtoX < 0)
                cmtoX = 0;
            else if (cmtoX > cmxLim)
                cmtoX = cmxLim;
            else
                cmx = cmtoX;
            cmRun = cmRun * 9 / 10;
            if (cmRun < 100 && cmRun > -100)
                cmRun = 0;
        }

        if (cmx == cmtoX || pointerIsDowning) return;
        
        cmvx = (cmtoX - cmx) << 2;
        cmdx += cmvx;
        cmx += cmdx >> 4;
        cmdx = cmdx & 0xf;
    }

    public void paintMenu(MGraphics g)
    {
        g.translate(-g.getTranslateX(), -g.getTranslateY());
        if (typeMenu == NPC)
        {
            paintMenuNpc(g);
            return;
        }
        g.translate(-cmx, 0);
        if (GameCanvas.isTouch)
            for (var i = 0; i < menuItems.size(); i++)
            {
                g.drawImage(i == menuSelectedItem ? LoadImageInterface.img_use_focus : LoadImageInterface.img_use,
                    menuX + i * menuW + 1, menuTemY[i] + 1, 0, true);
                
                var sc = ((Command) menuItems.elementAt(i)).subCaption ?? new[] {((Command) menuItems.elementAt(i)).caption};
                var yCaptionStart = menuTemY[i] + (menuH - sc.Length * 14) / 2 + 1;
                for (var k = 0; k < sc.Length; k++)
                    //				//menuW = 65;
                    if (GameScr.isMessageMenu)
                        if (ChatManager.gI().findWaitPerson(sc[k]))
                            if (GameCanvas.gameTick % 10 > 5)
                                FontManager.GetInstance().tahoma_7_red.DrawString(g, sc[k], menuX + i * menuW + menuW / 2 - 2,
                                    yCaptionStart + k * 14, 2);
                            else
                                FontManager.GetInstance().tahoma_7_white.DrawString(g, sc[k], menuX + i * menuW + menuW / 2,
                                    yCaptionStart + k * 14 + 1, 2);
                        else
                            FontManager.GetInstance().tahoma_7_white.DrawString(g, sc[k], menuX + i * menuW + menuW / 2,
                                yCaptionStart + k * 14 + 1, 2);
                    else
                        FontManager.GetInstance().tahoma_7_white.DrawString(g, sc[k], menuX + i * menuW + menuW / 2,
                            yCaptionStart + k * 14 + 1, 2);
            }
        else
            for (var i = 0; i < menuItems.size(); i++)
            {
                g.drawImage(i == menuSelectedItem ? LoadImageInterface.img_use_focus : LoadImageInterface.img_use,
                    menuX + i * menuW + 1, menuTemY[i] + 1 - 23, 0);

                var sc = ((Command) menuItems.elementAt(i)).subCaption ?? new[] {((Command) menuItems.elementAt(i)).caption};

                var yCaptionStart = menuTemY[i] + (menuH - sc.Length * 14) / 2 + 1 - 23;
                for (var k = 0; k < sc.Length; k++)
                    if (GameScr.isMessageMenu)
                        if (ChatManager.gI().findWaitPerson(sc[k]))
                            if (GameCanvas.gameTick % 10 > 5)
                                FontManager.GetInstance().tahoma_7_red.DrawString(g, sc[k], menuX + i * menuW + menuW / 2 - 2,
                                    yCaptionStart + k * 14, 2);
                            else
                                FontManager.GetInstance().tahoma_7_yellow.DrawString(g, sc[k], menuX + i * menuW + menuW / 2 - 2,
                                    yCaptionStart + k * 14, 2);
                        else
                            FontManager.GetInstance().tahoma_7_yellow.DrawString(g, sc[k], menuX + i * menuW + menuW / 2 - 2,
                                yCaptionStart + k * 14, 2);
                    else
                        FontManager.GetInstance().tahoma_7_yellow.DrawString(g, sc[k], menuX + i * menuW + menuW / 2 - 2,
                            yCaptionStart + k * 14, 2);
            }
        g.translate(-g.getTranslateX(), -g.getTranslateY());
    }

    private void paintMenuNpc(MGraphics g)
    {

        g.setColor(0x000000, 70);
        g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
        g.disableBlending();
        
        if (npctest != null)
            g.drawRegion(npctest, 0, 0, npctest.GetWidth(), npctest.GetHeight(), 2,
                menuXNPC + 10 + nameNpc.GetWidth() / 2, menuYNPC - npctest.GetHeight() / 2,
                MGraphics.VCENTER | MGraphics.HCENTER); 
        
        if (nameNpc != null)
            g.drawImage(nameNpc, menuXNPC + 10, menuYNPC - nameNpc.GetHeight(), 0);
        
        Paint.SubFrame(menuXNPC, menuYNPC, menuWNPC, menuHNPC + 20, g, 0x037ddb, 80);
        
        
        for (var i = 0; i < textNpc.Length; i++)
            FontManager.GetInstance().tahoma_7_white.DrawString(g, textNpc[i], menuXNPC + 10, menuYNPC + 10 + i * 14, 0);
        
        if (npc != null && npc.template.name != null && npc.template.name != null && nameNpc != null)
            FontManager.GetInstance().tahoma_7.DrawString(g, npc.template.name, menuXNPC + 10 + nameNpc.GetWidth() / 2,
                menuYNPC - 4 - nameNpc.GetHeight() / 2, 2);
        
        if (decor != null)
        {
            g.drawImage(decor, menuXNPC - decor.GetWidth(), GameCanvas.h - decor.GetHeight(), 0);
            g.drawRegion(decor, 0, 0, decor.GetWidth(), decor.GetHeight(), 2, GameCanvas.w - 30,
                GameCanvas.h - decor.GetHeight(),
                MGraphics.TOP | MGraphics.LEFT); // GameCanvas.w-40, GameCanvas.h-decor.getHeight()
        }
        for (var i = 0; i < menuItems.size(); i++)
        {
            var cmd = (Command) menuItems.elementAt(i);
            cmd.paint(g);
        }
        
        if (next != null) next.paint(g);
        if (next != null && next.isNoPaintImage && icn_focus != null)
            g.drawImage(icn_focus[GameCanvas.gameTick / 4 % 5],
                next.x - FontManager.GetInstance().tahoma_7b_white.GetWidth(next.caption + "   ") / 2 + next.img.GetWidth() / 2,
                next.y + next.img.GetHeight() / 2, MGraphics.VCENTER | MGraphics.HCENTER);
    }

    public void doCloseMenu()
    {
        isClose = false;
        showMenu = false;
        InfoDlg.hide();
        if (close)
            GameScr.isMessageMenu = false;
        else if (touch)
            if (menuSelectedItem >= 0)
            {
                var c = (Command) menuItems.elementAt(menuSelectedItem);
                if (c != null)
                    c.performAction();
            }
    }

    private bool isScrolling()
    {
        return menuItems.size() > 0 && !isClose && menuTemY.Length - 1 >= 0 && menuTemY[menuTemY.Length - 1] > menuY
               || isClose && menuTemY.Length - 1 >= 0 && menuTemY[menuTemY.Length - 1] < GameCanvas.h;
    }


    public void updateMenu()
    {
        if (typeMenu == NPC && nameNpc == null)
        {
            if (nameNpc == null) nameNpc = GameCanvas.loadImage("/GuiNaruto/nameNPC.png");
            if (decor == null) decor = GameCanvas.loadImage("/GuiNaruto/decor.png"); //
            imgRightArrow = GameCanvas.loadImage("/GuiNaruto/Trade/right_arrow.png");
            imgRightArrowFocus = GameCanvas.loadImage("/GuiNaruto/Trade/right_arrow2.png");
            if (next != null && next.caption.Trim().Length == 0)
            {
                next.img = imgRightArrow;
                next.imgFocus = imgRightArrowFocus;
            }
        }
        if (next != null && typeMenu == NPC && icn_focus != null && icn_focus[0] == null && next.isNoPaintImage)
            for (var i = 0; i < icn_focus.Length; i++)
                icn_focus[i] = GameCanvas.loadImage("/GuiNaruto/icn_focus_" + i + ".png");
        if (typeMenu == NPC && npctest == null && npc != null && npc.template != null && npc.template.idavatar != -1)
        {
            npctest = GameCanvas.loadImage("/npc/" + (npc.template.idavatar + SmallImage.ID_ADD_AVATARNPC) + ".png");
            if (MSystem.currentTimeMillis() - timeCurrent > timeCownDowrequestImgNPC)
            {
                timeCurrent = MSystem.currentTimeMillis();
                if (npctest == null)
                    Service.GetInstance().RequestImage(npc.template.idavatar + SmallImage.ID_ADD_AVATARNPC);
            }
        }

        if (typeMenu == NPC)
        {
            for (var i = 0; i < menuItems.size(); i++)
            {
                var cmd = (Command) menuItems.elementAt(i);
                if (TScreen.getCmdPointerLast(cmd))
                {
                    doCloseMenu();
                    cmd.performAction();
                    return;
                }
            }
            if (GameCanvas.isPointerJustRelease && !GameCanvas.isPoint(menuXNPC + 10, menuYNPC, wTextPaintNpc, 100))
                doCloseMenu();
            return;
        }
        moveCamera();
        if (!isClose)
        {
            tDelay++;
            for (var i = 0; i < menuTemY.Length; i++)
                if (menuTemY[i] > menuY)
                {
                    var delta = (menuTemY[i] - menuY) >> 1;
                    if (delta < 1)
                        delta = 1;
                    if (tDelay > i)
                        menuTemY[i] -= delta;
                }
            if (menuTemY.Length > 0 && menuTemY[menuTemY.Length - 1] <= menuY)
                tDelay = 0;
        }
        else
        {
            tDelay++;
            for (var i = 0; i < menuTemY.Length; i++)
                if (menuTemY[i] < GameCanvas.h)
                {
                    int delta;
                    if (GameCanvas.isTouch)
                        delta = ((GameCanvas.h - menuTemY[i]) >> 1) + 2;
                    else
                        delta = ((GameCanvas.h - menuTemY[i]) >> 1) + 2;
                    if (delta < 1)
                        delta = 1;
                    if (tDelay > i)
                        menuTemY[i] += delta;
                }

            if (menuTemY.Length - 1 >= 0 && menuTemY[menuTemY.Length - 1] >= GameCanvas.h)
            {
                tDelay = 0;
                doCloseMenu();
            }
        }
        if (xc != 0)
        {
            xc >>= 1;
            if (xc < 0)
                xc = 0;
        }
        if (isScrolling())
            return;
        
        if (waitToPerform > 0)
        {
            waitToPerform--;

            if (waitToPerform == 0)
                if (menuSelectedItem != -1 && menuSelectedItem < isNotClose.Length && !isNotClose[menuSelectedItem])
                {
                    isClose = true;
                    touch = true;
                }
                else
                {
                    performSelect();
                }
        }
    }

    private void performSelect()
    {
        InfoDlg.hide();
        
        if (menuSelectedItem < 0 || menuSelectedItem >= menuItems.size()) return;
        
        var c = (Command) menuItems.elementAt(menuSelectedItem);
        if (c != null)
            c.performAction();
    }

}