public class ChatType
{
    public const sbyte CHAT_MAP = 0;
    public const sbyte CHAT_WORLD = 1;
    public const sbyte CHAT_FRIEND = 2;
}