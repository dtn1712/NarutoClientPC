using src.lib;
using src.main;
using src.Objectgame;
using src.screen;

public class Paint
{
    public static int COLORBACKGROUND = 0xf3ae58;
    public static int COLORLIGHT = 0x923200;
    public static int hTab = 24;
    public void paintTabSoft(MGraphics g)
    {
        if (!GameCanvas.isTouch)
        {
            g.setColor(0);
            g.fillRect(0, GameCanvas.h - hTab, GameCanvas.w, hTab + 1);
            g.setColor(0x888888);
            g.fillRect(0, GameCanvas.h - (hTab - 1), GameCanvas.w, 1);
        }
    }


    public void paintInputDlg(MGraphics g, int x, int y, int w, int h, string[] str)
    {
        paintFrame(x, y, w, h, g);
        var yStart = y + 20 - FontManager.GetInstance().tahoma_8b.GetHeight();
        for (int i = 0, a = yStart; i < str.Length; i++, a += FontManager.GetInstance().tahoma_8b.GetHeight())
            FontManager.GetInstance().tahoma_8b.DrawString(g, str[i], x + w / 2, a, 2);
    }
    public static void paintFrame(int x, int y, int w, int h, MGraphics g)
    {
        g.setColor(COLORBACKGROUND);
        g.fillRect(x, y, w, h);
        g.setColor(0); // viền nâu
        g.drawRect(x - 2, y - 2, w + 3, h + 3);
        g.setColor(0xd4d4d4); // viền nâu
        g.drawRect(x - 1, y - 1, w + 1, h + 1);
        g.setColor(0x574949); // viền nâu
        g.drawRect(x, y, w - 1, h - 1);
    }

    //Paint naruto

    //paint gui
    public static void paintFrameNaruto(int x, int y, int w, int h, MGraphics g)
    {
        g.setColor(COLORBACKGROUND);
        g.fillRect(x, y, w, h);
        if (GameCanvas.isTouch)
        {

            var ww = Image.getWidth(LoadImageInterface.imgConerGui[0]);
            var hh = Image.getHeight(LoadImageInterface.imgConerGui[0]);



            var nWidth = (w - ww * 2) / (Image.getWidth(LoadImageInterface.imgConerGui[1]) - 1) + 2;
            var nHeight = (h - hh * 2) / (Image.getWidth(LoadImageInterface.imgConerGui[1]) - 1) + 1;

            for (var i = 0; i < nWidth; i++)
                g.drawImage(LoadImageInterface.imgConerGui[1],
                    x - 3 + ww + i * (Image.getWidth(LoadImageInterface.imgConerGui[1]) - 1), y - 3,
                    MGraphics.TOP | MGraphics.LEFT);

            for (var i = 0; i < nHeight; i++)
                g.drawRegion(LoadImageInterface.imgConerGui[2], 0, 0, Image.getWidth(LoadImageInterface.imgConerGui[2]),
                    Image.getHeight(LoadImageInterface.imgConerGui[2]), Sprite.TRANS_ROT180, x + w + 3,
                    y + hh - 3 + i * (Image.getHeight(LoadImageInterface.imgConerGui[2]) - 1), StaticObj.TOP_RIGHT);

            for (var i = 0; i < nWidth; i++)
                g.drawRegion(LoadImageInterface.imgConerGui[1], 0, 0, Image.getWidth(LoadImageInterface.imgConerGui[1]),
                    Image.getHeight(LoadImageInterface.imgConerGui[1]), Sprite.TRANS_ROT180,
                    x - 3 + ww + i * (Image.getWidth(LoadImageInterface.imgConerGui[1]) - 1), y + h + 3,
                    StaticObj.BOTTOM_LEFT);

            for (var i = 0; i < nHeight; i++)
                g.drawRegion(LoadImageInterface.imgConerGui[2], 0, 0, Image.getWidth(LoadImageInterface.imgConerGui[2]),
                    Image.getHeight(LoadImageInterface.imgConerGui[2]), Sprite.TRANS_NONE, x - 3,
                    y + hh - 3 + i * (Image.getHeight(LoadImageInterface.imgConerGui[2]) - 1), StaticObj.TOP_LEFT);


            g.drawImage(LoadImageInterface.imgConerGui[0], x - 3, y - 3, MGraphics.TOP | MGraphics.LEFT, true);

            g.drawRegion(LoadImageInterface.imgConerGui[0], 0, 0, ww,
                hh, Sprite.TRANS_MIRROR, x + w + 3, y - 3, StaticObj.TOP_RIGHT, true);

            g.drawRegion(LoadImageInterface.imgConerGui[0], 0, 0, ww,
                hh, Sprite.TRANS_MIRROR_ROT180, x - 3, y + h + 3, StaticObj.BOTTOM_LEFT, true);

            g.drawRegion(LoadImageInterface.imgConerGui[0], 0, 0, ww,
                hh, Sprite.TRANS_ROT180, x + w + 3, y + h + 3, StaticObj.BOTTOM_RIGHT, true);
        }
    }

    public static void paintFrameNaruto(int x, int y, int w, int h, MGraphics g, bool isClip)
    {
        g.setColor(COLORBACKGROUND);
        g.fillRect(x, y, w, h);

        if (GameCanvas.isTouch)
        {

            var ww = Image.getWidth(LoadImageInterface.imgConerGui[0]);
            var hh = Image.getHeight(LoadImageInterface.imgConerGui[0]);


            var nWidth = (w - ww * 2) / (Image.getWidth(LoadImageInterface.imgConerGui[1]) - 1) + 2;
            var nHeight = (h - hh * 2) / (Image.getWidth(LoadImageInterface.imgConerGui[1]) - 1) + 1;


            for (var i = 0; i < nWidth; i++)
               
                g.drawImage(LoadImageInterface.imgConerGui[1],
                    x - 3 + ww + i * (Image.getWidth(LoadImageInterface.imgConerGui[1]) - 1), y - 3,
                    MGraphics.TOP | MGraphics.LEFT, isClip);

            for (var i = 0; i < nHeight; i++)
          
                g.drawRegion(LoadImageInterface.imgConerGui[2], 0, 0, Image.getWidth(LoadImageInterface.imgConerGui[2]),
                    Image.getHeight(LoadImageInterface.imgConerGui[2]), Sprite.TRANS_ROT180, x + w + 3,
                    y + hh - 3 + i * (Image.getHeight(LoadImageInterface.imgConerGui[2]) - 1), StaticObj.TOP_RIGHT,
                    isClip);

            for (var i = 0; i < nWidth; i++)
                g.drawRegion(LoadImageInterface.imgConerGui[1], 0, 0, Image.getWidth(LoadImageInterface.imgConerGui[1]),
                    Image.getHeight(LoadImageInterface.imgConerGui[1]), Sprite.TRANS_ROT180,
                    x - 3 + ww + i * (Image.getWidth(LoadImageInterface.imgConerGui[1]) - 1), y + h + 3,
                    StaticObj.BOTTOM_LEFT, isClip);

            for (var i = 0; i < nHeight; i++)
                g.drawRegion(LoadImageInterface.imgConerGui[2], 0, 0, Image.getWidth(LoadImageInterface.imgConerGui[2]),
                    Image.getHeight(LoadImageInterface.imgConerGui[2]), Sprite.TRANS_NONE, x - 3,
                    y + hh - 3 + i * (Image.getHeight(LoadImageInterface.imgConerGui[2]) - 1), StaticObj.TOP_LEFT,
                    isClip);

            g.drawImage(LoadImageInterface.imgConerGui[0], x - 3, y - 3, MGraphics.TOP | MGraphics.LEFT, isClip);

            g.drawRegion(LoadImageInterface.imgConerGui[0], 0, 0, ww,
                hh, Sprite.TRANS_MIRROR, x + w + 3, y - 3, StaticObj.TOP_RIGHT, isClip);

            g.drawRegion(LoadImageInterface.imgConerGui[0], 0, 0, ww,
                hh, Sprite.TRANS_MIRROR_ROT180, x - 3, y + h + 3, StaticObj.BOTTOM_LEFT, isClip);

            g.drawRegion(LoadImageInterface.imgConerGui[0], 0, 0, ww,
                hh, Sprite.TRANS_ROT180, x + w + 3, y + h + 3, StaticObj.BOTTOM_RIGHT, isClip);
        }
    }

    //paint name box
    public static void PaintBoxName(string strName, int x, int y, int w, MGraphics g)
    {
        var count = w / 10;

        g.drawImage(LoadImageInterface.imgBoxName, x - 25, y, MGraphics.LEFT | MGraphics.TOP);
        for (var i = 0; i < count; i++)
            g.drawImage(LoadImageInterface.imgBoxName_1, x + Image.getWidth(LoadImageInterface.imgBoxName_1) * i, y,
                MGraphics.LEFT | MGraphics.TOP);

        g.drawRegion(LoadImageInterface.imgBoxName, 0, 0, 25,
            24, Sprite.TRANS_MIRROR, x + 25 + Image.getWidth(LoadImageInterface.imgBoxName_1) * count - 1, y,
            StaticObj.TOP_RIGHT);

        FontManager.GetInstance().tahoma_7b_white.DrawString(g, strName, x + w / 2, y + 8, 2);
    }

    public static void PaintBGListQuest(int x, int y, int w, MGraphics g)
    {
        var count = w / 40;

        g.drawImage(LoadImageInterface.bgQuestConner, x - 15, y, MGraphics.LEFT | MGraphics.TOP, true);
        for (var i = 0; i < count; i++)
            g.drawImage(LoadImageInterface.bgQuestLine, x + Image.getWidth(LoadImageInterface.bgQuestLine) * i, y,
                MGraphics.LEFT | MGraphics.TOP, true);

        g.drawRegion(LoadImageInterface.bgQuestConner, 0, 0, 15,
            42, Sprite.TRANS_MIRROR, x + 15 + Image.getWidth(LoadImageInterface.bgQuestLine) * count - 1, y,
            StaticObj.TOP_RIGHT, true);
    }

    public static void PaintBGListQuestFocus(int x, int y, int w, MGraphics g)
    {
        var count = w / 40;

        g.drawImage(LoadImageInterface.bgQuestConnerFocus, x - 15, y, MGraphics.LEFT | MGraphics.TOP, true);
        for (var i = 0; i < count; i++)
            g.drawImage(LoadImageInterface.bgQuestLineFocus,
                x + Image.getWidth(LoadImageInterface.bgQuestLineFocus) * i, y, MGraphics.LEFT | MGraphics.TOP, true);

        g.drawRegion(LoadImageInterface.bgQuestConnerFocus, 0, 0, 15,
            42, Sprite.TRANS_MIRROR, x + 15 + Image.getWidth(LoadImageInterface.bgQuestLineFocus) * count - 1, y,
            StaticObj.TOP_RIGHT, true);
    }

    /*
     * Paint sub frame
     */
    public static void SubFrame(int x, int y, int w, int h, MGraphics g)
    {
        g.setColor(0x454545);
        g.fillRect(x, y, w, h);

        if (GameCanvas.isTouch)
        {
            var ww = Image.getWidth(LoadImageInterface.imgsub_info_conner);
            var hh = Image.getHeight(LoadImageInterface.imgsub_info_conner);


            var nWidth = (w - ww * 2) / (Image.getWidth(LoadImageInterface.imgsub_frame_info) - 1) + 2;
            var nHeight = (h - hh * 2) / (Image.getHeight(LoadImageInterface.imgsub_frame_info_2) - 1) + 2;

            g.drawImage(LoadImageInterface.imgsub_info_conner, x - 3, y - 3, MGraphics.TOP | MGraphics.LEFT);

            g.drawRegion(LoadImageInterface.imgsub_info_conner, 0, 0, ww,
                hh, Sprite.TRANS_MIRROR, x + w + 3, y - 3, StaticObj.TOP_RIGHT);

            g.drawRegion(LoadImageInterface.imgsub_info_conner, 0, 0, ww,
                hh, Sprite.TRANS_MIRROR_ROT180, x - 3, y + h + 3, StaticObj.BOTTOM_LEFT);

            g.drawRegion(LoadImageInterface.imgsub_info_conner, 0, 0, ww,
                hh, Sprite.TRANS_ROT180, x + w + 3, y + h + 3, StaticObj.BOTTOM_RIGHT);
            //width
            for (var i = 0; i < nWidth; i++)
                g.drawImage(LoadImageInterface.imgsub_frame_info,
                    x - 4 + ww + i * (Image.getWidth(LoadImageInterface.imgsub_frame_info) - 1), y - 3,
                    MGraphics.TOP | MGraphics.LEFT);

            //height
            for (var i = 0; i < nHeight; i++)
                g.drawRegion(LoadImageInterface.imgsub_frame_info_2, 0, 0,
                    Image.getWidth(LoadImageInterface.imgsub_frame_info_2),
                    Image.getHeight(LoadImageInterface.imgsub_frame_info_2), Sprite.TRANS_ROT180, x + w + 3,
                    y + hh - 3 + i * (Image.getHeight(LoadImageInterface.imgsub_frame_info_2) - 1),
                    StaticObj.TOP_RIGHT);
            //width
            for (var i = 0; i < nWidth; i++)
                g.drawRegion(LoadImageInterface.imgsub_frame_info, 0, 0,
                    Image.getWidth(LoadImageInterface.imgsub_frame_info),
                    Image.getHeight(LoadImageInterface.imgsub_frame_info), Sprite.TRANS_ROT180,
                    x - 4 + ww + i * (Image.getWidth(LoadImageInterface.imgsub_frame_info) - 1), y + h + 3,
                    StaticObj.BOTTOM_LEFT);

            //height
            for (var i = 0; i < nHeight; i++)
                g.drawRegion(LoadImageInterface.imgsub_frame_info_2, 0, 0,
                    Image.getWidth(LoadImageInterface.imgsub_frame_info_2),
                    Image.getHeight(LoadImageInterface.imgsub_frame_info_2), 0, x - 3,
                    y + hh - 3 + i * (Image.getHeight(LoadImageInterface.imgsub_frame_info_2) - 1), StaticObj.TOP_LEFT);

            //3 nut
            g.drawImage(LoadImageInterface.imgsub_stone_01, x + w / 2, y - 2, MGraphics.VCENTER | MGraphics.HCENTER);
            g.drawImage(LoadImageInterface.imgsub_stone_01, x + w / 2, y + 2 + h,
                MGraphics.VCENTER | MGraphics.HCENTER);

        }
    }

    public static void SubFrame(int x, int y, int w, int h, MGraphics g, int color, int percenOpacity)
    {
        g.setColor(0x454545, percenOpacity);
        g.fillRect(x, y, w, h);
        g.disableBlending();
        if (GameCanvas.isTouch)
        {
            var ww = Image.getWidth(LoadImageInterface.imgsub_info_conner);
            var hh = Image.getHeight(LoadImageInterface.imgsub_info_conner);

            var nWidth = (w - ww * 2) / (Image.getWidth(LoadImageInterface.imgsub_frame_info) - 1) + 2;
            var nHeight = (h - hh * 2) / (Image.getHeight(LoadImageInterface.imgsub_frame_info_2) - 1) + 2;

            g.drawImage(LoadImageInterface.imgsub_info_conner, x - 3, y - 3, MGraphics.TOP | MGraphics.LEFT);

            g.drawRegion(LoadImageInterface.imgsub_info_conner, 0, 0, ww,
                hh, Sprite.TRANS_MIRROR, x + w + 3, y - 3, StaticObj.TOP_RIGHT);

            g.drawRegion(LoadImageInterface.imgsub_info_conner, 0, 0, ww,
                hh, Sprite.TRANS_MIRROR_ROT180, x - 3, y + h + 3, StaticObj.BOTTOM_LEFT);

            g.drawRegion(LoadImageInterface.imgsub_info_conner, 0, 0, ww,
                hh, Sprite.TRANS_ROT180, x + w + 3, y + h + 3, StaticObj.BOTTOM_RIGHT);
            //width
            for (var i = 0; i < nWidth; i++)
                g.drawImage(LoadImageInterface.imgsub_frame_info,
                    x - 4 + ww + i * (Image.getWidth(LoadImageInterface.imgsub_frame_info) - 1), y - 3,
                    MGraphics.TOP | MGraphics.LEFT);

            //height
            for (var i = 0; i < nHeight; i++)
                g.drawRegion(LoadImageInterface.imgsub_frame_info_2, 0, 0,
                    Image.getWidth(LoadImageInterface.imgsub_frame_info_2),
                    Image.getHeight(LoadImageInterface.imgsub_frame_info_2), Sprite.TRANS_ROT180, x + w + 3,
                    y + hh - 3 + i * (Image.getHeight(LoadImageInterface.imgsub_frame_info_2) - 1),
                    StaticObj.TOP_RIGHT);
            //width
            for (var i = 0; i < nWidth; i++)
                g.drawRegion(LoadImageInterface.imgsub_frame_info, 0, 0,
                    Image.getWidth(LoadImageInterface.imgsub_frame_info),
                    Image.getHeight(LoadImageInterface.imgsub_frame_info), Sprite.TRANS_ROT180,
                    x - 4 + ww + i * (Image.getWidth(LoadImageInterface.imgsub_frame_info) - 1), y + h + 3,
                    StaticObj.BOTTOM_LEFT);

            //height
            for (var i = 0; i < nHeight; i++)
                g.drawRegion(LoadImageInterface.imgsub_frame_info_2, 0, 0,
                    Image.getWidth(LoadImageInterface.imgsub_frame_info_2),
                    Image.getHeight(LoadImageInterface.imgsub_frame_info_2), 0, x - 3,
                    y + hh - 3 + i * (Image.getHeight(LoadImageInterface.imgsub_frame_info_2) - 1), StaticObj.TOP_LEFT);

            //3 nut
            g.drawImage(LoadImageInterface.imgsub_stone_01, x + w / 2, y - 2, MGraphics.VCENTER | MGraphics.HCENTER,
                true);
            g.drawImage(LoadImageInterface.imgsub_stone_01, x + w / 2, y + 2 + h, MGraphics.VCENTER | MGraphics.HCENTER,
                true);
        }
    }

    public static void PaintLine(int x, int y, int width, MGraphics g)
    {
        var nCol = width / Image.getWidth(LoadImageInterface.imgLineTrade) + 6;

        for (var i = 0; i < nCol; i++)
            g.drawImage(LoadImageInterface.imgLineTrade,
                x + (Image.getWidth(LoadImageInterface.imgLineTrade) - 1) * i + 15, y, 0);
    }

    /*
     * Paint menu
     * n: n time : 1n= 1 image, ma= 10 image
     */
    public static void PaintBGMenuIcon(int x, int y, int n, MGraphics g)
    {
        for (var i = 0; i < 10; i++)
            g.drawImage(LoadImageInterface.imgBGMenuIcon[i],
                x + Image.getWidth(LoadImageInterface.imgBGMenuIcon[i]) * i, y, 0);
    }

    /*
     * Paint menu
     * n: n time : 1n= 1 image, ma= 10 image
     */
    public static void PaintBGSubIcon(int x, int y, int n, MGraphics g)
    {
        if (n <= 0)
            n = 1;

        var xx = x;
        var width = Image.getWidth(LoadImageInterface.imgSubMenu[0]);
        var height = Image.getHeight(LoadImageInterface.imgSubMenu[0]);

        g.drawImage(LoadImageInterface.imgSubMenu[0], xx, y, 0); //conner

        for (var i = 0; i < n; i++)
        {
            xx += width;
            g.drawImage(LoadImageInterface.imgSubMenu[1], xx, y, 0);
        }

        xx += width;
        g.drawImage(LoadImageInterface.imgSubMenu[2], xx, y, 0); //mid

        for (var i = 0; i < n; i++)
        {
            xx += width;
            g.drawImage(LoadImageInterface.imgSubMenu[1], xx, y, 0);
        }

        //conner
        xx += width;
        g.drawRegion(LoadImageInterface.imgSubMenu[0], 0, 0, width, height, Sprite.TRANS_MIRROR, xx, y, 0);
    }

    // public paint Item Info
    public static void paintItemInfo(MGraphics g, Item itemInfo, int x, int y)
    {
        // paint thông tin item
        //		loadImageInterface.ImgItem
        if (itemInfo == null) return;
        g.drawImage(LoadImageInterface.ImgItem, x + 12, y + 12, MGraphics.VCENTER | MGraphics.HCENTER);
        if (itemInfo != null && itemInfo.template != null)
        {
            SmallImage.DrawSmallImage(g, itemInfo.template.iconID, x + 12, y + 12, 0,
                MGraphics.VCENTER | MGraphics.HCENTER, true);

            if (itemInfo.template.id != -1 && itemInfo.template.name != null)
            {
                FontManager.GetInstance().tahoma_7_white.DrawString(g, itemInfo.template.name, x + 28, y - 4, 0);
                FontManager.GetInstance().tahoma_7_white.DrawString(g, "Lv: " + itemInfo.template.level, x + 28, y + 7, 0);
                int typeitem = itemInfo.template.type;
                switch (typeitem)
                {
                    case 4:
                        FontManager.GetInstance().tahoma_7_white.DrawString(g, "Cấp trang bị: " + itemInfo.template.lvItem, x + 70, y + 7,
                            0);
                        break;
                    case 12:
                        FontManager.GetInstance().tahoma_7_white.DrawString(g, "Cấp trang bị: " + itemInfo.template.lvItem, x + 70, y + 7,
                            0);
                        break;
                    case 13:
                        FontManager.GetInstance().tahoma_7_white.DrawString(g, "Cấp trang bị: " + itemInfo.template.lvItem, x + 70, y + 7,
                            0);
                        break;
                    case 14:
                        FontManager.GetInstance().tahoma_7_white.DrawString(g, "Cấp trang bị: " + itemInfo.template.lvItem, x + 70, y + 7,
                            0);
                        break;
                    case 15:
                        FontManager.GetInstance().tahoma_7_white.DrawString(g, "Cấp trang bị: " + itemInfo.template.lvItem, x + 70, y + 7,
                            0);
                        break;
                    case 16:
                        FontManager.GetInstance().tahoma_7_white.DrawString(g, "Cấp trang bị: " + itemInfo.template.lvItem, x + 70, y + 7,
                            0);
                        break;
                    case 17:
                        FontManager.GetInstance().tahoma_7_white.DrawString(g, "Cấp trang bị: " + itemInfo.template.lvItem, x + 70, y + 7,
                            0);
                        break;
                    case 18:
                        FontManager.GetInstance().tahoma_7_white.DrawString(g, "Cấp trang bị: " + itemInfo.template.lvItem, x + 70, y + 7,
                            0);
                        break;
                    case 19:
                        FontManager.GetInstance().tahoma_7_white.DrawString(g, "Cấp trang bị: " + itemInfo.template.lvItem, x + 100, y + 7,
                            0);
                        break;
                }
            }
            for (var i = 0; i < itemInfo.level_dapdo; i++)
                if (GameCanvas.gameTick / 4 % (itemInfo.level_dapdo + 5) == i)
                    g.drawImage(LoadImageInterface.imgStar[1],
                        x + 28 + i * LoadImageInterface.imgStar[0].GetWidth() +
                        LoadImageInterface.imgStar[0].GetWidth() / 2,
                        y + 20 + LoadImageInterface.imgStar[0].GetHeight() / 2, MGraphics.VCENTER | MGraphics.HCENTER);
                else
                    g.drawImage(LoadImageInterface.imgStar[0],
                        x + 28 + i * LoadImageInterface.imgStar[0].GetWidth(), y + 20, MGraphics.TOP | MGraphics.LEFT);
            var ydem = 0;
            if (!(Item.isMP(itemInfo.template.type) || Item.isBlood(itemInfo.template.type)))
                if (itemInfo.template.gender < 0)
                {
                }
                else
                {
                    ydem += 12;
                    FontManager.GetInstance().tahoma_7_white.DrawString(g, "Giới tính: " + (itemInfo.template.gender == 0 ? "Nam" : "Nữ"),
                        x, y + 28, 0);
                }
            if (!Item.isBlood(itemInfo.template.type) && !Item.isMP(itemInfo.template.type))
            {
                FontManager.GetInstance().tahoma_7_white.DrawString(g,
                    "Class: " + (itemInfo.template.clazz > -1
                        ? Text.nameClass[itemInfo.template.clazz]
                        : "Thể, Ảo, Nhẫn"), x, y + 28 + ydem, 0);
                FontManager.GetInstance().tahoma_7_white.DrawString(g,
                    "Quốc gia: " + (itemInfo.template.quocgia > -1
                        ? Text.typeCountry[itemInfo.template.quocgia]
                        : "Tất cả"), x, y + 40 + ydem, 0);
                ydem += 24;
            }

            if (itemInfo.template.despaint != null)
                for (var i = 0; i < itemInfo.template.despaint.Length; i++)
                    FontManager.GetInstance().tahoma_7_white.DrawString(g, itemInfo.template.despaint[i], x, y + 28 + ydem + 12 * i, 0);
        }


        if (itemInfo != null && itemInfo.options != null)
            for (var i = 0; i < itemInfo.options.size(); i++)
            {
                var option = (ItemOption) itemInfo.options.elementAt(i);
                paintMultiLine(g, FontManager.GetInstance().tahoma_7_blue,
                    itemInfo.isTypeUIShopView() ? option.getOptionShopString() : option.getOptionString(),
                    x + 8, y += 12, MFont.LEFT);
            }
    }

    public static void paintItemInfo(MGraphics g, Item itemInfo, int x, int y, bool paintGia)
    {
        // paint thông tin item
        //		loadImageInterface.ImgItem
        if (itemInfo == null) return;
        g.drawImage(LoadImageInterface.ImgItem, x + 12, y + 12, MGraphics.VCENTER | MGraphics.HCENTER);
        if (itemInfo != null && itemInfo.template != null)
        {
            if (itemInfo.template.id != -1 && itemInfo.template.name != null)
            {
                FontManager.GetInstance().tahoma_7_white.DrawString(g, itemInfo.template.name, x + 28, y, 0);
                FontManager.GetInstance().tahoma_7_white.DrawString(g, "Lv: " + itemInfo.template.level, x + 28, y + 12, 0);
            }

            SmallImage.DrawSmallImage(g, itemInfo.template.iconID, x + 12, y + 12, 0,
                MGraphics.VCENTER | MGraphics.HCENTER, true);

            var ydem = 0;
            if (!Item.isBlood(itemInfo.template.type) && !Item.isMP(itemInfo.template.type))
            {
                ydem = 28;
                if (itemInfo.template.gender < 0)
                    ydem = 16;
                else
                    FontManager.GetInstance().tahoma_7_white.DrawString(g, "Giới tính: " + (itemInfo.template.gender == 0 ? "Nam" : "Nữ"),
                        x, y + ydem, 0);
                ydem += 12;
                FontManager.GetInstance().tahoma_7_white.DrawString(g,
                    "Class: " + (itemInfo.template.clazz > -1 ? Text.nameClass[itemInfo.template.clazz] : "Tất cả"), x,
                    y + ydem, 0);
                ydem += 12;
                FontManager.GetInstance().tahoma_7_white.DrawString(g,
                    "Quốc gia: " + (itemInfo.template.quocgia > -1
                        ? Text.typeCountry[itemInfo.template.quocgia]
                        : "Tất cả"), x, y + ydem, 0);
            }
            else
            {
                ydem = 16;
            }
            if (paintGia)
            {
                ydem += 12;
                FontManager.GetInstance().tahoma_7_white.DrawString(g,
                    "Giá: " + itemInfo.template.gia + " " + (itemInfo.template.typeSell == 0 ? "xu" : "gold"), x,
                    y + ydem, 0);
            }

            if (itemInfo.template.despaint != null)

                ydem += 12;
            for (var i = 0; i < itemInfo.template.despaint.Length; i++)
                FontManager.GetInstance().tahoma_7_white.DrawString(g, itemInfo.template.despaint[i], x, y + ydem + 12 * i, 0);
        }


        if (itemInfo != null && itemInfo.options != null)
            for (var i = 0; i < itemInfo.options.size(); i++)
            {
                var option = (ItemOption) itemInfo.options.elementAt(i);
                paintMultiLine(g, FontManager.GetInstance().tahoma_7_blue,
                    itemInfo.isTypeUIShopView() ? option.getOptionShopString() : option.getOptionString(),
                    x + 8, y += 12, MFont.LEFT);
            }
    }

    public static void paintMultiLine(MGraphics g, MFont f, string str, int x, int y, int align)
    {
        var a = GameCanvas.isTouch && GameCanvas.w >= 320 ? 20 : 10;
        var yTemp = y;
        var arr = f.SplitFontArray(str, 20 - a);
        for (var i = 0; i < arr.Length; i++)
            if (i == 0)
            {
                f.DrawString(g, arr[i], x, y, align);
            }
            else
            {
                if (i * GameScr.scrMain.ITEM_SIZE + yTemp >= GameScr.scrMain.cmy - 12 &&
                    i * GameScr.scrMain.ITEM_SIZE < GameScr.scrMain.cmy + GameScr.popupH - 44)
                    f.DrawString(g, arr[i], x, y += 12, align);
                else
                    y += 12;
                GameScr.indexRowMax++;
            }
    }

    public static void paintFocus(MGraphics g, int x, int y, int w, int h, int coutFc)
    {
        g.drawImage(LoadImageInterface.imgFocusSelectItem0, x - 2 + coutFc, y - 2 + coutFc, 0, false);
        g.drawRegion(LoadImageInterface.imgFocusSelectItem0,
            0, 0, LoadImageInterface.imgFocusSelectItem0.GetWidth(), LoadImageInterface.imgFocusSelectItem0.GetHeight(),
            Sprite.TRANS_MIRROR, x + w + 3 - coutFc, y - 2 + coutFc, MGraphics.TOP | MGraphics.RIGHT, false);

        g.drawRegion(LoadImageInterface.imgFocusSelectItem0,
            0, 0, LoadImageInterface.imgFocusSelectItem0.GetWidth(), LoadImageInterface.imgFocusSelectItem0.GetHeight(),
            Sprite.TRANS_MIRROR_ROT90, x + w + 3 - coutFc, y + h + 3 - coutFc, MGraphics.BOTTOM | MGraphics.RIGHT,
            false);

        g.drawRegion(LoadImageInterface.imgFocusSelectItem0,
            0, 0, LoadImageInterface.imgFocusSelectItem0.GetWidth(), LoadImageInterface.imgFocusSelectItem0.GetHeight(),
            Sprite.TRANS_ROT270, x - 2 + coutFc, y + h + 3 - coutFc, MGraphics.BOTTOM | MGraphics.LEFT, false);
    }
}