using src.lib;

namespace src.main
{
    public class GameMidlet
    {
        public static string IP = "192.168.0.106";

        public static int TCP_PORT = 19158;
        public static int UDP_PORT = 19159;
    
        public static string VERSION = "0.1.2";
    
        private static readonly GameMidlet Instance = new GameMidlet();
    
        public readonly GameCanvas GameCanvas;

        public static GameMidlet GetInstance()
        {
            return Instance;
        }
    
        private GameMidlet()
        {
            GameCanvas = new GameCanvas();
            SplashScr.loadSplashScr();
            GameCanvas.currentScreen = new SplashScr();
        }


        public void Exit()
        {
            MSystem.gcc();
        }
    }
}