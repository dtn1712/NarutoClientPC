using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using src.Gui;
using src.lib;
using src.model;
using src.network;
using src.network.impl.fast;
using src.network.impl.reliable;
using src.Objectgame;
using src.Objectgame.quest;
using src.real;
using src.screen;
using UnityEngine;
using Char = src.Objectgame.Char;

namespace src.main
{
    public class GameCanvas : TCanvas, IActionListener
    {
        private const int ENDDLG = 8882;
        private const int DISCONNECT = 8883;

        public const int cEndDgl = 8882;
        public const int cLogout = 9990;
        public const int cHoiSinh = 9991;
        public const int cVeLang = 9992;
        public const int cDongYThachDau = 9993;
        public const int cTaiVersionMoi = 9994;
        public const int cTaoClan = 9995;
        public const int cMenuNpc = 10000;
        public const int cmdYesDialogServer = 9999;

        public const int cEndDglTField = 10004;
        public static bool lowGraphic = false;

        public static byte opacityTab = 70;
        public static int transY = 15;
        public static bool isMoveNumberPad = true;
        public static bool isLoading;
        public static bool isTouch; // ---> Màn hình cảm ứng
        public static bool isTouchControl; // ----> Điều khiển bằng cảm ứngo
        public static bool isTouchControlSmallScreen; // ----> Điều khiển bằng
        public static GameScr gameScr; // cảm ứng màn hình nhỏ
        public static bool isTouchControlLargeScreen; // ----> Điều khiển bằng
        public static TabScreenNew AllInfo /* = new TabScreenNew()*/;
        public static Paint paintt; // cảm ứng màn hình lớn
        public static GameCanvas instance;
        public static sbyte zoomLevel;
        public static LoginScr loginScrr;
        public static LanguageScr languageScr;
        public static bool paintBG;
        public static int[] imgBGWidth;
        public static int skyColor;
        public static bool isStore = false;

        public static int gsskyHeight;

        public static int gsgreenField1Y,
            gsgreenField2Y,
            gshouseY,
            gsmountainY,
            bgLayer0y,
            bgLayer1y;

        public static MBitmap imgCloud, imgSun;
        public static MBitmap[] imgBorder = new MBitmap[3];

        public static int borderConnerW,
            borderConnerH,
            borderLineW,
            borderLineH,
            borderCenterW,
            borderCenterH,
            sunX,
            sunY;

        public static int[] cloudX, cloudY;
        public static InputDlg inputDlg;

        public static bool isPointerSelect,
            isPointerMove,
            isPointerEnd,
            isPointerJustDown,
            isPointerDown,
            isPointerClick,
            isPointerJustRelease;

        // 0 1 2 3 4 5 6 7 8 9 * LSoft RSoft
        public static bool[] keyPressed = new bool[21];

        public static bool[] keyReleased = new bool[21];
        public static bool[] keyHold = new bool[21];
        
        public static int px, py, pxFirst, pyFirst, pxLast, pyLast;
        public static Position[] arrPos = new Position[4];
        public static int gameTick, taskTick, curPos;
        public static bool isEff1, isEff2;
        public static long timeTickEff1, timeTickEff2;

        public static int hCommand = 25;
        // Width Height Half-W, Half-H, width/3, height/3, 2*width/3, 2*height/2,
        // ...

        public static MBitmap[] imgBG;
        public static int w, h, hw, hh, wd3, hd3, w2d3, h2d3, w3d4, h3d4, wd6, hd6;
        public static TScreen currentScreen;

        public static Menu menu;

        public static Dialog currentDialog;
        public static MsgDlg msgdlg;
        public static Vector currentPopup;
        public static Paint paintz;
        private static long lastTimePress;
        public static int keyAsciiPress;

        public static bool isPlaySound = true;

        public static string thongbao = "";
        public static int indexThongBao;
        public static bool isPaintThongBao;

        public static Vector keybgLoginSelect = new Vector();

        public static Vector debugUpdate;
        public static Vector debugPaint;

        public static Vector listPoint;

        private static Image imgSignal;


        private static int[] bgSpeed;

        private static MBitmap[][] imgDust;

        private static int typeBg = -1;
        private Command cmdError;
        private int count = 1;
        public int[] dustX, dustY, dustState;

        private readonly MGraphics _graphic = new MGraphics();

        private static readonly ReliableConnection ReliableConnection = new ReliableConnection();
        private static readonly FastConnection FastConnection = new FastConnection();

        public GameCanvas()
        {
            InitGame();
            InitGameCanvas();
            InitScreen();
        }

        public static int WCommand { get; set; }

        public void perform(int idAction, object p)
        {
            currentDialog = null;
            switch (idAction)
            {
                case cLogout:
                    LoginScr.isLogout = true;
                    Service.GetInstance().PlayerOut();
                    ResetToLoginScr(true);
                    break;
                case cHoiSinh:
                    Service.GetInstance().ReviveChar();
                    break;
                case cVeLang:
                    Service.GetInstance().goHome();
                    break;
                case cDongYThachDau:
                    var id = (string) p;
                    Service.GetInstance().ThachDau(1, short.Parse(id));
                    break;
                case ENDDLG:
                    EndDlg();
                    break;
                case cTaiVersionMoi:
                    Application.OpenURL((string) p);
                    break;
                case cMenuNpc:
                    var text = (string) p;
                    var indexMenu = sbyte.Parse(text);
                    if (Char.myChar().npcFocus != null)
                    {
                        Service.GetInstance().MenuNpc(Char.myChar().npcFocus.template.npcTemplateId, indexMenu);
                    }
                    break;
                case cTaoClan:
                    var namenhap = msgdlg.getTextTF();
                    if (namenhap == null) return;
                    if (namenhap.Trim().Length == 0)
                    {
                        StartDglThongBao("Vui lòng nhập lại !!!");
                        StartDlgTField("Tạo Bang", new Command("Tạo", this, cTaoClan, null), null,
                            new Command("Cancel", this, cEndDglTField, null));
                    }
                    else if (namenhap.Trim().Length > 12 || namenhap.Trim().Length < 6)
                    {
                        StartDglThongBao("Tên bang nhiều hơn 6 ký tự và không quá 12 ký tự !!!");
                        StartDlgTField("Tạo Bang", new Command("Tạo", this, cTaoClan, null), null,
                            new Command("Cancel", this, cEndDglTField, null));
                    }
                    Service.GetInstance().Clan_createClanName(namenhap);

                    break;
                case cEndDglTField:
                    msgdlg.hide();
                    break;
//                case cmdYesDialogServer:
//                    Service.GetInstance().ReplyDialogFromServer(ReliableMessageExecutor.cmdTroupleShoot);
//                    break;
                case DISCONNECT:
                    Disconnect();
                    break;
            }
        }

        private void InitGame()
        {
            FontManager.GetInstance().LoadBegin();
            
            listPoint = new Vector();
            w = getWidthz();
            h = getHeightz();
            Debug.Log("SIZE IN GAME ------> "+w+" "+h);
            menu = new Menu();
            currentPopup = new Vector();

            hw = w / 2;
            hh = h / 2;

            if (hasPointerEvents())
            {
                isTouch = true;
                if (w >= 240)
                    isTouchControl = true;
                if (w < 320)
                    isTouchControlSmallScreen = true;
                if (w >= 320)
                    isTouchControlLargeScreen = true;
            }
            msgdlg = new MsgDlg();
            if (h <= 160)
            {
                Paint.hTab = 15;
            }
            instance = this;
            MSystem.gcc();
            paintz = new Paint();
            debugUpdate = new Vector();
            debugPaint = new Vector();
            LoadBegin();
            
            cmdError = new Command("", 999999);
        }
        
        public void InitGameCanvas()
        {
            w = getWidthz();
            h = getHeightz();
            hw = w / 2;
            hh = h / 2;
            wd3 = w / 3;
            hd3 = h / 3;
            w2d3 = 2 * w / 3;
            h2d3 = 2 * h / 3;
            w3d4 = 3 * w / 4;
            h3d4 = 3 * h / 4;
            wd6 = w / 6;
            hd6 = h / 6;

            CRes.loadbegin();
            SmallImage.loadBigImage();

            MsgDlg.loadbegin();
            MResources.loadLanguage();
            GameScr.loadImages();
            LoadImageInterface.loadImage();
            
            TField.setVendorTypeMode(TField.NOKIA);
            TScreen.ITEM_HEIGHT = 15;
        }

        private void InitScreen()
        {
            loginScrr = new LoginScr();
            inputDlg = new InputDlg();
            languageScr = new LanguageScr();
            currentScreen = new SplashScr();
            languageScr = new LanguageScr();
            AllInfo = new TabScreenNew();
        }


        /**
     * @param resetToLoginScr: hàm dùng để resetLogin;
     */
//        public static void ResetToLoginScr()
//        {
//            GuiMain.vecItemOther.removeAllElements();
//            Mob.isBossAppear = false;
//            Party.gI().isLeader = false;
//            GameScr.hParty.clear();
//            MainQuestManager.getInstance().NewQuest = null;
//            MainQuestManager.getInstance().WorkingQuest = null;
//            MainQuestManager.getInstance().FinishQuest = null;
//            ChatPrivate.vOtherchar.removeAllElements();
//            ChatPrivate.nTinChuaDoc = 0;
//            Char.myChar().typePk = -1;
//            if (currentScreen != LoginScr.GetInstance())
//            {
//                LoginScr.GetInstance().switchToMe();
//                gameScr = null;
//                GameScr.vCharInMap.removeAllElements();
//                Disconnect();
//                loadBG(0);
//            }
//        }

        /**
     * @param loadBegin: Hàm dùng để khởi tạo giá trị BAN ĐẦU của các biến STATIC cho giống Unity
     */
        private void LoadBegin()
        {
            paintt = new Paint();
            LoadDust();
            TabInfoChar.loadbegin();
            GameScr.loadBegin();
            LoadImages();
            TField.loadBegin();
        }

        public static GameCanvas GetInstance()
        {
            return instance;
        }


        public static bool Connect()
        {
            if (!ReliableConnection.IsServerAvailable(GameMidlet.IP, GameMidlet.TCP_PORT))
            {
                StartOkDlg(MResources.SERVER_MAINTENANCE);
                return false;
            }

            Task.Factory.StartNew(() => { ReliableConnection.Connect(GameMidlet.IP, GameMidlet.TCP_PORT); });

            Task.Factory.StartNew(() => { FastConnection.Connect(GameMidlet.IP, GameMidlet.UDP_PORT); });

            Session.GetInstance().ReliableConnection = ReliableConnection;
            Session.GetInstance().FastConnection = FastConnection;

            return true;
        }

        private static void Disconnect()
        {
            Session.GetInstance().Disconnect();
        }

//        private static void Reconnect()
//        {
//            Disconnect();
//            Connect();
//        }

        public static void ResetTrans(MGraphics g)
        {
            g.translate(-g.getTranslateX(), -g.getTranslateY());
            g.setClip(0, 0, w, h);
        }


        public void update()
        {
            debugUpdate.removeAllElements();
            var l1 = MSystem.currentTimeMillis_();
            try
            {
                if (l1 - timeTickEff1 >= 780 && !isEff1)
                {
                    timeTickEff1 = l1;
                    isEff1 = true;
                }
                else
                {
                    isEff1 = false;
                }
                if (l1 - timeTickEff2 >= 7800 && !isEff2)
                {
                    timeTickEff2 = l1;
                    isEff2 = true;
                }
                else
                {
                    isEff2 = false;
                }
                if (taskTick > 0)
                    taskTick--;
                gameTick++;
                if (gameTick > 10000)
                {
                    if (MSystem.currentTimeMillis_() - lastTimePress > 20000
                        && currentScreen == LoginScr.GetInstance())
                        GameMidlet.GetInstance().Exit();
                    gameTick = 0;
                }
                if (currentScreen != null)
                {
                    if (currentDialog != null)
                    {
                        currentDialog.update();
                    }
                    else if (menu.showMenu)
                    {
                        menu.updateMenu();
                        menu.updateMenuKey();
                    }
                    if (!isLoading)
                    {
                        currentScreen.update();
                        if (!menu.showMenu && currentDialog == null)
                        {
                            currentScreen.updateKey();
                            currentScreen.updatePointer();
                        }
                    }
                }
                if (isPaintThongBao)
                {
                    indexThongBao++;
                    if (indexThongBao > 80)
                    {
                        indexThongBao = 0;
                        isPaintThongBao = false;
                    }
                }

                if (currentScreen == GameScr.GetInstance())
                    if (GameScr.listInfoServer.size() > 0)
                    {
                        var info = (InfoServer) GameScr.listInfoServer.elementAt(0);
                        info?.update();
                    }
                    else if (GameScr.listWorld.size() > 0)
                    {
                        var info = (InfoServer) GameScr.listWorld.elementAt(0);
                        info?.update();
                    }
                InfoDlg.update();
            }
            catch (Exception e)
            {
                Thread.Sleep(1000);
            }

            if (isPointerJustRelease)
                isPointerJustRelease = false;
        }


        public void KeyPressed(int keyCode)
        {
            lastTimePress = MSystem.currentTimeMillis_();
            if (keyCode >= 48 && keyCode <= 57 || keyCode >= 65 && keyCode <= 122 || keyCode == 10 || keyCode == 8
                || keyCode == 13 || keyCode == 32 || keyCode == 31)
                keyAsciiPress = keyCode;
            MapKeyPress(keyCode);
        }


        private void MapKeyPress(int keyCode)
        {
            isPointerJustRelease = false;
            if (msgdlg.ispaintTField)
                msgdlg.keyPress(keyCode);
            if (menu.showMenu)
            {
                //menu.keyPress(keyCode);
            }
            else
            {
                if (currentDialog != null)
                {
                    currentDialog.keyPress(keyCode);
                    keyAsciiPress = 0;
                    return;
                }
                currentScreen.keyPress(keyCode);
            }
            switch (keyCode)
            {
                case 48:
                    keyHold[0] = true;
                    keyPressed[0] = true;
                    return;
                case 49: // 1
                    if (currentScreen == GameScr.GetInstance() && isMoveNumberPad && !ChatTextField.GetInstance().isShow)
                    {
                        keyHold[1] = true;
                        keyPressed[1] = true;
                    }
                    return;
                case 51: // 3
                    if (currentScreen == GameScr.GetInstance() && isMoveNumberPad && !ChatTextField.GetInstance().isShow)
                    {
                        keyHold[3] = true;
                        keyPressed[3] = true;
                    }
                    return;
                case 55: // 7
                    keyHold[7] = true;
                    keyPressed[7] = true;
                    return;
                case 57: // 9
                    keyHold[9] = true;
                    keyPressed[9] = true;
                    return;
                case 42:
                    keyHold[10] = true;
                    keyPressed[10] = true;
                    return; // Key [*]
                case 35:
                    keyHold[11] = true;
                    keyPressed[11] = true;
                    return; // Key [#]
                case -6:
                case -21:
                    keyHold[12] = true;
                    keyPressed[12] = true;
                    return; // Soft1
                case -7:
                case -22:
                    keyHold[13] = true;
                    keyPressed[13] = true;
                    return; // Soft2
                case -5:
                case 10:

                    keyHold[5] = true;
                    keyPressed[5] = true;
                    return; // [i]
                case -1:
                case -38:

                    keyHold[2] = true;
                    keyPressed[2] = true;
                    return; // UP
                case -2:
                case -39:

                    keyHold[8] = true;
                    keyPressed[8] = true;
                    return; // DOWN
                case -3:

                    keyHold[4] = true;
                    keyPressed[4] = true;
                    return; // LEFT
                case -4:
                    keyHold[6] = true;
                    keyPressed[6] = true;
                    return; // RIGHT

                case 50: // 2
                    if (currentScreen == GameScr.GetInstance() && isMoveNumberPad && !ChatTextField.GetInstance().isShow)
                    {
                        keyHold[2] = true;
                        keyPressed[2] = true;
                    }
                    return;
                case 52: // 4
                    if (currentScreen == GameScr.GetInstance() && isMoveNumberPad && !ChatTextField.GetInstance().isShow)
                    {
                        keyHold[4] = true;
                        keyPressed[4] = true;
                    }
                    return;
                case 54: // 6
                    if (currentScreen == GameScr.GetInstance() && isMoveNumberPad && !ChatTextField.GetInstance().isShow)
                    {
                        keyHold[6] = true;
                        keyPressed[6] = true;
                    }
                    return;
                case 56: // 8
                    if (currentScreen == GameScr.GetInstance() && isMoveNumberPad && !ChatTextField.GetInstance().isShow)
                    {
                        keyHold[8] = true;
                        keyPressed[8] = true;
                    }
                    return;
                case 53: // 5
                    if (currentScreen == GameScr.GetInstance() && isMoveNumberPad && !ChatTextField.GetInstance().isShow)
                    {
                        keyHold[5] = true;
                        keyPressed[5] = true;
                    }
                    return;
            }
        }

        // Catch key-up input and clear from Input Array
        public void keyReleasedd(int keyCode)
        {
            keyAsciiPress = 0;
            mapKeyReleasee(keyCode);
        }

        private void mapKeyReleasee(int keyCode)
        {
            switch (keyCode)
            {
                case 48:
                    keyHold[0] = false;
                    keyReleased[0] = true;
                    return;
                case 49: // 1
                    keyHold[1] = false;
                    keyReleased[1] = true;
                    return;
                case 51: // 3
                    keyHold[3] = false;
                    keyReleased[3] = true;
                    return;
                case 55: // 7
                    keyHold[7] = false;
                    keyReleased[7] = true;
                    return;
                case 57: // 9
                    keyHold[9] = false;
                    keyReleased[9] = true;
                    return;
                case 42:
                    keyHold[10] = false;
                    keyReleased[10] = true;
                    return; // Key [*]
                case 35:
                    keyHold[11] = false;
                    keyReleased[11] = true;
                    return; // Key [#]
                case -6:
                case -21:
                    keyHold[12] = false;
                    keyReleased[12] = true;
                    return; // Soft1
                case -7:
                case -22:
                    keyHold[13] = false;
                    keyReleased[13] = true;
                    return; // Soft2
                case -5:
                case 10:
                    keyHold[5] = false;
                    keyReleased[5] = true;
                    return; // [i]
                case -1:
                case -38:

                    keyHold[2] = false;

                    return; // UP
                case -2:
                case -39:

                    keyHold[8] = false;
                    return; // DOWN
                case -3:

                    keyHold[4] = false;
                    return; // LEFT
                case -4:

                    keyHold[6] = false;
                    return; // RIGHT

                case 50: // 2
                {
                    keyHold[2] = false;
                    keyReleased[2] = true;
                }
                    return;
                case 52: // 4
                {
                    keyHold[4] = false;
                    keyReleased[4] = true;
                }
                    return;
                case 54: // 6
                {
                    keyHold[6] = false;
                    keyReleased[6] = true;
                }
                    return;
                case 56: // 8
                {
                    keyHold[8] = false;
                    keyReleased[8] = true;
                }
                    return;
                case 53: // 5
                {
                    keyHold[5] = false;
                    keyReleased[5] = true;
                }
                    return;
            }
        }

        public override void onPointerDragged(int x, int y)
        {
            if (isPointerMove)
            {
                listPoint.addElement(new Position(x, y));
            }
            else if (CRes.abs(px - pxLast) >= 10 * MGraphics.zoomLevel ||
                     CRes.abs(py - pyLast) >= 10 * MGraphics.zoomLevel)
            {
                isPointerDown = isPointerMove = true;
                isPointerClick = isPointerJustRelease = false;
            }
            px = x;
            py = y;
            curPos++;
            if (curPos > 3)
                curPos = 0;
            arrPos[curPos] = new Position(x, y);
        }

        public override void onPointerPressed(int x, int y)
        {
            isPointerJustRelease = false;
            isPointerJustDown = true;
            isPointerSelect = false;
            isPointerClick = isPointerDown = true;
            isPointerMove = false;
            lastTimePress = MSystem.currentTimeMillis_();
            pxFirst = x;
            pyFirst = y;
            pxLast = x;
            pyLast = y;
            px = x;
            py = y;
        }

        public override void onPointerReleased(int x, int y)
        {
            if (!isPointerMove && !isPointerEnd)
                isPointerSelect = true;
            isPointerMove = false;
            isPointerEnd = false;
            isPointerDown = false;
            isPointerJustRelease = true;
            px = x;
            py = y;
            clearKeyHold();
            clearKeyPressed();
        }

        public static bool isPointerHoldIn(int x, int y, int w, int h)
        {
            if (!isPointerDown && !isPointerJustRelease)
                return false;
            if (px >= x && px <= x + w && py >= y && py <= y + h)
                return true;
            return false;
        }

        public static void clearKeyPressed()
        {
            for (var i = 0; i < keyPressed.Length; i++)
                keyPressed[i] = false;
        }

        public static void clearKeyHold()
        {
            for (var i = 0; i < keyHold.Length; i++)
                keyHold[i] = false;
        }

        public void paint(MGraphics gx)
        {
            ResetTrans(_graphic);
            _graphic.setColor(0);
            _graphic.fillRect(0, 0, w, h);
            debugPaint.removeAllElements();
            if (currentScreen != null)
                currentScreen.paint(_graphic);
            _graphic.translate(-_graphic.getTranslateX(), -_graphic.getTranslateY());
            _graphic.setClip(0, 0, w, h);
            if (isPaintThongBao)
            {
                _graphic.setColor(0x000000, opacityTab);
                _graphic.fillRect(w - FontManager.GetInstance().tahoma_7_white.GetWidth(thongbao) - 20,
                    10 - (indexThongBao > 10 ? 10 : indexThongBao),
                    FontManager.GetInstance().tahoma_7_white.GetWidth(thongbao) + 20, 20);
                FontManager.GetInstance().tahoma_7_white.DrawString(_graphic, thongbao,
                    w - FontManager.GetInstance().tahoma_7_white.GetWidth(thongbao) - 15,
                    12 - (indexThongBao > 10 ? 10 : indexThongBao), 0);
                _graphic.disableBlending();
            }
            InfoDlg.paint(_graphic);
            if (currentDialog != null)
            {
                _graphic.setColor(0x000000, 80);
                _graphic.fillRect(0, 0, w, h);
                currentDialog.paint(_graphic);
            }
            else if (menu.showMenu)
            {
                menu.paintMenu(_graphic);
            }
            if (currentScreen == GameScr.GetInstance())
            {
                if (GameScr.listInfoServer.size() > 0)
                {
                    var info = (InfoServer) GameScr.listInfoServer.elementAt(0);
                    info?.paint(_graphic);
                }
                else if (GameScr.listWorld.size() > 0)
                {
                    var info = (InfoServer) GameScr.listWorld.elementAt(0);
                    info?.paint(_graphic);
                }
            }
        }

        public static void EndDlg()
        {
            inputDlg.tfInput.setMaxTextLenght(500);
            currentDialog = null;
            InfoDlg.hide();
        }

        public static void StartOkDlg(string info)
        {
            currentDialog = null;
            msgdlg.setInfo(info, null, new Command("OK", instance, ENDDLG, null), null);
            currentDialog = msgdlg;
        }


        public static void StartWaitDlg(string info)
        {
            msgdlg.setInfo(info, null, new Command(MResources.CANCEL, instance, ENDDLG, null), null);
            currentDialog = msgdlg;
        }


        public static void StartWaitLoginDialog()
        {
            msgdlg.setInfo(MResources.LOGGING_IN, null, new Command(MResources.CANCEL, instance, DISCONNECT, null),
                null);
            currentDialog = msgdlg;
        }

        public static void StartDlgTField(string tieude, Command cmdleft, Command center, Command right)
        {
            msgdlg.setInfoTF(tieude, cmdleft, center, right);
            msgdlg.isPaintCham = false;
            msgdlg.show();
        }


        public static void startOK(string info, int actionID, object p)
        {
            // BB: move command from left to center
            msgdlg.setInfo(info, null, new Command("OK", instance, actionID, p), null);
            msgdlg.show();
            currentDialog = msgdlg;
        }

        public static void startYesNoDlg(string info, int iYes, object pYes, int iNo, object pNo)
        {
            msgdlg.setInfo(info, new Command("OK", instance, iYes, pYes), new Command("", instance, iYes, pYes),
                new Command("No", instance, iNo, pNo));
            msgdlg.show();
            currentDialog = msgdlg;
        }

        public static void startYesNoDlg(string info, Command cmdYes, Command cmdNo)
        {
            msgdlg.setInfo(info, cmdYes, null, cmdNo);
            msgdlg.show();
            currentDialog = msgdlg;
        }

        private static void LoadImages()
        {
            CreatCharScr.loadImage();
            borderConnerW = MGraphics.getImageWidth(imgBorder[0]);
            borderConnerH = MGraphics.getImageHeight(imgBorder[0]);
            borderLineW = MGraphics.getImageWidth(imgBorder[1]);
            borderLineH = MGraphics.getImageHeight(imgBorder[1]);
            borderCenterW = MGraphics.getImageWidth(imgBorder[2]);
            borderCenterH = MGraphics.getImageHeight(imgBorder[2]);

            SplashScr.imgLogo = loadImage("/logo.png");
        }

        public static MBitmap loadImage(string path)
        {
            var imgg = (MBitmap) Image.listImgLoad.get(path);
            if (imgg != null)
                return imgg;

            var img = new MBitmap();
            var data = Rms.loadRMS(MSystem.getPathRMS(CutPng("/x" + MGraphics.zoomLevel + path)));

            if (data != null)
            {
                img = new MBitmap {image = Image.createImage(data, 0, data.Length)};
                if (img.image.texture == null)
                {
                    return img;
                }
                img.w = img.image.width = img.image.texture.width;
                img.h = img.height = img.image.height = img.image.texture.height;
                img.width = img.w / MGraphics.zoomLevel;
                img.height = img.h / MGraphics.zoomLevel;
                return img;
            }
            try
            {
                path = Constants.RESOURCE_NAME + CutPng("/x" + MGraphics.zoomLevel + path);

                img.image = Image.createImage(path);
                if (img.image != null && img.image.texture != null)
                {
                    img.image.width = img.image.texture.width;
                    img.image.height = img.image.texture.height;
                    img.width = img.image.width / MGraphics.zoomLevel;
                    img.height = img.image.height / MGraphics.zoomLevel;
                }
                if (img.image == null || img.image.texture == null)
                    return null;
            }
            catch (IOException e)
            {
                return null;
            }
            return img;
        }

        private static string CutPng(string str)
        {
            var tam = str;
            if (str.Contains(".png"))
                tam = str.Replace(".png", "");
            if (str.Contains(".jpg"))
                tam = str.Replace(".jpg", "");
            if (str.Contains(".bytes"))
                tam = str.Replace(".bytes", "");
            return tam;
        }


        public static bool isPointer(int x, int y, int w, int h)
        {
            return px >= x && px <= x + w && py >= y && py <= y + h;
        }

        public static bool IsPointSelect(int x, int y, int w, int h)
        {
            return isPointerJustRelease && isPointer(x, y, w, h);
        }


        public static void ResetToLoginScr(bool isLogout)
        {
            Char.myChar().typePk = -1;

            Mob.isBossAppear = false;
            Party.gI().isLeader = false;
            GameScr.hParty.clear();
            GuiMain.vecItemOther.removeAllElements();
            Char.myChar().clearAllFocus();
            Char.myCharr = null;
            MainQuestManager.getInstance().NewQuest = null;
            MainQuestManager.getInstance().WorkingQuest = null;
            MainQuestManager.getInstance().FinishQuest = null;
            ChatPrivate.vOtherchar.removeAllElements();
            ChatPrivate.nTinChuaDoc = 0;
            if (currentScreen != LoginScr.GetInstance())
            {
                LoginScr.GetInstance().switchToMe();
                gameScr = null;
                loadBG(0);
                GameScr.vCharInMap.removeAllElements();
                Disconnect();
                if (!isLogout)
                    StartOkDlg("Mất kết nối với máy chủ. Vui lòng thử lại !");
            }
            else
            {
                StartOkDlg("Có lỗi đăng nhập xảy ra. Vui lòng thử lại !");
            }
            
//            Reconnect();
        }

        public static void clearAllPointerEvent()
        {
            isPointerClick = false;
            isPointerDown = false;

            isPointerMove = false;
            isPointerSelect = false;
            isPointerJustDown = false;
            isPointerJustRelease = false;
        }


        public static void clearPointerEvent()
        {
            isPointerMove = isPointerClick = isPointerDown = isPointerJustRelease = false;
        }


        private void LoadDust()
        {
            if (lowGraphic)
                return;
            if (imgDust == null)
            {
                imgDust = new MBitmap[2][];
                for (var i = 0; i < imgDust.Length; i++)
                    imgDust[i] = new MBitmap[5];
                for (var i = 0; i < 2; i++)
                for (var j = 0; j < 5; j++)
                    imgDust[i][j] = loadImage("/e/d" + i + (j + ".png"));
            }
            dustX = new int[2];
            dustY = new int[2];
            dustState = new int[2];
            dustState[0] = dustState[1] = -1;
        }


        public static bool isPaint(int x, int y)
        {
            if (x < GameScr.cmx)
                return false;
            if (x > GameScr.cmx + GameScr.gW)
                return false;
            if (y < GameScr.cmy)
                return false;
            if (y > GameScr.cmy + GameScr.gH + 30)
                return false;
            return true;
        }


        public static bool loadBG(int typeBG)
        {
            var deltaLayer1 = 0;
            var deltaLayer2 = 0;
            var deltaLayer3 = 0;
            typeBg = typeBG;
            switch (typeBG)
            {
                case 0:
                    // typeBg = 0;

                    break;
                case 2:
                    deltaLayer1 = 16;
                    deltaLayer2 = 10;
                    deltaLayer3 = 10;
                    // typeBg = 2;
                    bgSpeed = new[] {1, 2, 3, 4};
                    break;
                case 3:
                    // typeBg = 3;
                    bgSpeed = new[] {1, 2, 3, 4};
                    break;
                case 4:
                    // typeBg = 4;
                    deltaLayer1 = 9;
                    deltaLayer2 = 6;
                    bgSpeed = new[] {1, 2, 3, 4};
                    break;
                case 5:
                    // typeBg = 5;
                    bgSpeed = new[] {1, 1, 1, 1};
                    break;
                case 6:
                    // typeBg = 6;
                    deltaLayer1 = 12;
                    bgSpeed = new[] {1, 2, 3, 4};
                    break;
                case 12:
                    // typeBg = 12;
                    bgSpeed = new[] {1, 2, 3, 4};
                    break;
                case 11:
                    // typeBg = 11;
                    bgSpeed = new[] {1, 2, 3, 4};
                    break;
                case 10:
                    // typeBg = 10;
                    bgSpeed = new[] {1, 1, 1, 1};
                    break;
                case 9:
                    // typeBg = 9;
                    deltaLayer1 = 16;
                    deltaLayer2 = 10;
                    deltaLayer3 = 10;
                    bgSpeed = new[] {1, 2, 3, 4};
                    break;
                case 8:
                    // typeBg = 8;
                    bgSpeed = new[] {1, 2, 3, 4};
                    break;
                case 7:
                    // typeBg = 7;
                    bgSpeed = new[] {1, 2, 3, 4};
                    break;
            }

            skyColor = StaticObj.SKYCOLOR[typeBg];

            try
            {
                if (!lowGraphic)
                {
                    imgBG = new MBitmap[4];
                    imgBGWidth = new int[4];
                    for (var i = 0; i < 4; i++)
                    {
                        try
                        {
                            if (StaticObj.TYPEBG[typeBg][i] != -1)
                            {
                                var path = "/bg/bg" + i + StaticObj.TYPEBG[typeBg][i] + ".png";
                                if (currentScreen != GameScr.GetInstance())
                                    keybgLoginSelect.add(path);
                                imgBG[i] = loadImage("/bg/bg" + i + StaticObj.TYPEBG[typeBg][i] + ".png");
                                if (imgBG[i] == null)
                                    return false;
                            }
                        }
                        catch (Exception e)
                        {
                            //e.printStackTrace();
                            return false;
                        }
                        if (imgBG[i] != null)
                            imgBGWidth[i] = MGraphics.getImageWidth(imgBG[i]);
                    }

                    if (typeBg == 10)
                    {
                        imgBG[1] = loadImage("/bg/bg09.png");
                        imgBG[2] = loadImage("/bg/bg09.png");
                        imgBGWidth[1] = MGraphics.getImageWidth(imgBG[1]);
                        imgBGWidth[2] = MGraphics.getImageWidth(imgBG[2]);
                    }
                    if (typeBg == 12)
                    {
                        imgBG[3] = loadImage("/bg/bg39.png");
                        imgBGWidth[3] = MGraphics.getImageWidth(imgBG[3]);
                    }
                }

                if (typeBg >= 0 && typeBg <= 1)
                {
                    imgCloud = loadImage("/bg/cl0.png");
                    imgSun = loadImage("/bg/sun0.png");
                }
                else
                {
                    imgCloud = null;
                    imgSun = null;
                }
                if (typeBg == 7 || typeBg == 11 || typeBg == 12)
                    imgCloud = TileMap.mapID == 20 ? null : loadImage("/bg/cl0.png");
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                return false;
            }

            paintBG = false;
            if (!lowGraphic)
            {
                paintBG = true;
                if (imgBG[0] != null && imgBG[1] != null && imgBG[2] != null)
                    gsskyHeight = GameScr.gH - (MGraphics.getImageHeight(imgBG[0]) +
                                                MGraphics.getImageHeight(imgBG[1]) +
                                                MGraphics.getImageHeight(imgBG[2])) /*+ 11*/;
                if (imgBG[0] != null)
                    gsgreenField1Y = GameScr.gH - Image.getHeight(imgBG[0]);
                if (imgBG[1] != null)
                    gsgreenField2Y = gsgreenField1Y - Image.getHeight(imgBG[1]);
                if (imgBG[2] != null)
                    gshouseY = gsgreenField2Y - Image.getHeight(imgBG[2]);
                if (imgBG[3] != null)
                    gsmountainY = gsgreenField2Y - Image.getHeight(imgBG[3]) /*- 10*/;
                // ================
                if (typeBg >= 2 && typeBg <= 12)
                {
                    var tem = GameScr.gH - MGraphics.getImageHeight(imgBG[0]);
                    bgLayer0y = tem;
                    if (imgBG[1] != null)
                        tem = tem - MGraphics.getImageHeight(imgBG[1]) + deltaLayer1;
                    bgLayer1y = tem;
                    if (imgBG[3] != null)
                        tem = tem - MGraphics.getImageHeight(imgBG[3]) + deltaLayer3;
                    gsmountainY = tem;
                    //
                    gsskyHeight = tem;

                    if (imgBG[2] != null)
                        gshouseY = bgLayer1y - MGraphics.getImageHeight(imgBG[2]) + deltaLayer2;
                    if (typeBg == 2)
                        gsskyHeight = h;
                }

                if (typeBG == 3)
                {
                    var tem = GameScr.gH - MGraphics.getImageHeight(imgBG[0]);
                    bgLayer0y = tem;
                    if (imgBG[1] != null)
                        tem = tem - MGraphics.getImageHeight(imgBG[1]) + 30 + deltaLayer1;
                    bgLayer1y = tem;
                    if (imgBG[3] != null)
                        tem = tem - MGraphics.getImageHeight(imgBG[3]) + deltaLayer3;
                    gsmountainY = tem;
                    //
                    gsskyHeight = tem;

                    if (imgBG[2] != null)
                        gshouseY = bgLayer1y - MGraphics.getImageHeight(imgBG[2]) + 20 + deltaLayer2;
                    if (typeBg == 2)
                        gsskyHeight = h;
                }
            }
            var bgDelta = 0;
            if (typeBg >= 2 && typeBg <= 12)
                bgDelta = 2 * GameScr.gH / 3 - bgLayer1y;
            else
                bgDelta = 2 * GameScr.gH / 3 - gsgreenField2Y;
            if (bgDelta < 0)
                bgDelta = 0;
            if (TileMap.mapID == 48 && TileMap.mapID == 51)
                bgLayer0y += bgDelta;
            if (typeBg >= 2 && typeBg <= 6)
                bgLayer1y += bgDelta;

            gsskyHeight += bgDelta;
            gsgreenField1Y += bgDelta;
            gsgreenField2Y += bgDelta;
            gshouseY += bgDelta;
            gsmountainY += bgDelta;
            sunX = 3 * GameScr.gW / 4;
            sunY = gsskyHeight / 3;
            cloudX = new int[2];
            cloudY = new int[2];
            cloudX[0] = GameScr.gW / 3;
            cloudY[0] = gsskyHeight / 2 - 8;
            cloudX[1] = 2 * GameScr.gW / 3;
            cloudY[1] = gsskyHeight / 2 + 8;
            if (typeBg == 2)
            {
                sunY = gsskyHeight / 5;
                cloudX = new int[5];
                cloudY = new int[5];
                cloudX[0] = GameScr.gW / 3;
                cloudY[0] = gsskyHeight / 3 - 35;
                cloudX[1] = 3 * GameScr.gW / 4;
                cloudY[1] = gsskyHeight / 3 + 12;
                cloudX[2] = GameScr.gW / 3 - 15;
                cloudY[2] = gsskyHeight / 3 + 12;
                cloudX[3] = GameScr.gW / +15;
                cloudY[3] = gsskyHeight / 2 + 12;
                cloudX[4] = 2 * GameScr.gW / 3 + 25;
                cloudY[4] = gsskyHeight / 3 + 12;
            }

            if (!lowGraphic)
            {
                if (typeBg == 8)
                    bgLayer0y = bgLayer1y = GameScr.gH2 - 50;
                if (typeBg == 10)
                    if (imgBG[3] != null)
                        gsmountainY = gshouseY - MGraphics.getImageHeight(imgBG[3]);
                if (typeBg == 11 || typeBg == 12)
                    gsmountainY = 0;
            }
            return true;
        }


        public static bool isPoint(int x, int y, int w, int h)
        {
            if (px >= x && px <= x + w && py >= y && py <= y + h)
                return true;
            return false;
        }

        public static void paintBGGameScr(MGraphics g)
        {
            // Paint the Sky
            if (paintBG && !lowGraphic)
            {
                if (typeBg >= 0 && typeBg <= 1)
                {
                    if (currentScreen == SelectCharScr.GetInstance() || currentScreen == CreatCharScr.gI())
                    {
                        if (imgBG[3] != null)
                            for (var i = -((GameScr.cmx >> 4) % 64); i < GameScr.gW; i += 64)
                                g.drawImage(imgBG[3], i, gsmountainY, 0);
                        if (imgBG[2] != null)
                            for (var i = -((GameScr.cmx >> 3) % 192);
                                i < GameScr.gW;
                                i += Image.getWidth(imgBG[2]) /*192*/)
                                g.drawImage(imgBG[2], i, gshouseY, 0);

                        // // Paint the GreenField2
                        if (imgBG[1] != null)
                        {
                            //						for (int i = -((GameScr.cmx >> 2) % 24); i < GameScr.gW; i += Image.getWidth(imgBG[1])/*24*/)
                            for (var i = w / 2;
                                i < w + Image.getWidth(imgBG[1]);
                                i += Image.getWidth(imgBG[1]))
                                g.drawImage(imgBG[1], i, gsgreenField2Y + 100, MGraphics.BOTTOM | MGraphics.HCENTER);

                            for (var i = w / 2; i > -Image.getWidth(imgBG[1]); i -= Image.getWidth(imgBG[1]))
                                g.drawImage(imgBG[1], i, gsgreenField2Y + 100, MGraphics.BOTTOM | MGraphics.HCENTER);
                        }
                        if (imgBG[0] != null)
                            for (var i = -((GameScr.cmx >> 1) % Image.getWidth(imgBG[0]));
                                i < GameScr.gW;
                                i += Image.getWidth(imgBG[0]) /*24*/)
                                g.drawImage(imgBG[0], i, gsgreenField1Y - 150, 0);
                        if (imgSun != null)
                            g.drawImage(imgSun, sunX, sunY, 3);
                        if (imgCloud != null)
                            for (var i = 0; i < 2; i++)
                                g.drawImage(imgCloud, cloudX[i], cloudY[i], 3);
                        // Paint House
                    }
                    else if (currentScreen == loginScrr ||
                             currentScreen == languageScr)
                    {
                        if (imgBG[3] != null)
                            for (var i = -((GameScr.cmx >> 4) % Image.getWidth(imgBG[3]));
                                i < GameScr.gW;
                                i += Image.getWidth(imgBG[3]))
                                g.drawImage(imgBG[3], i, gsmountainY, 0);
                        if (imgBG[2] != null)
                            for (var i = -((GameScr.cmx >> 3) % Image.getWidth(imgBG[2]));
                                i < GameScr.gW;
                                i += Image.getWidth(imgBG[2]))
                                g.drawImage(imgBG[2], Image.getWidth(imgBG[1]) - 50, gshouseY, 0);
                        if (imgBG[1] != null)
                            for (var i = -((GameScr.cmx >> 2) % Image.getWidth(imgBG[1]));
                                i < GameScr.gW;
                                i += Image.getWidth(imgBG[1]))
                                g.drawImage(imgBG[1], 0, gshouseY + 3, 0);
                        // // Paint Mountain

                        // Paint sun and cloud
                        if (imgSun != null)
                            g.drawImage(imgSun, sunX, sunY, 3);
                        if (imgCloud != null)
                            for (var i = 0; i < 2; i++)
                                g.drawImage(imgCloud, cloudX[i], cloudY[i], 3);
                        // Paint House

                        if (imgBG[0] != null)
                            for (var i = -((GameScr.cmx >> 1) % Image.getWidth(imgBG[0]));
                                i < GameScr.gW;
                                i += Image.getWidth(imgBG[0]))
                                g.drawImage(imgBG[0], i, gsgreenField1Y - 100, 0);
                    }


                    // ==========================
                }
                else if (typeBg >= 2 && typeBg <= 6)
                {
                    if (imgBG[1] != null)
                        for (var i = -((GameScr.cmx >> bgSpeed[1]) % imgBGWidth[1]); i < GameScr.gW; i += imgBGWidth[1])
                            g.drawImage(imgBG[1], i, h - imgBG[1].GetHeight(), 0);
                }
                else if (typeBg >= 7 && typeBg <= 12)
                {
                    g.setColor(skyColor);
                    g.fillRect(0, 0, GameScr.gW, GameScr.gH);
                    if (typeBg != 8 && imgBG[3] != null)
                        for (var i = -((GameScr.cmx >> bgSpeed[3]) % imgBGWidth[3]); i < GameScr.gW; i += imgBGWidth[3])
                            if (typeBg == 11 || typeBg == 12)
                                g.drawImage(imgBG[3], i, GameScr.gH - MGraphics.getImageHeight(imgBG[3]), 0);
                            else
                                g.drawImage(imgBG[3], i, gsmountainY, 0);

                    if (typeBg != 8 && typeBg != 11 && typeBg != 12 && imgBG[2] != null)
                        if (TileMap.mapID == 45)
                            g.drawImage(imgBG[2], GameScr.gW, gshouseY, 0);
                        else
                            for (var i = -((GameScr.cmx >> bgSpeed[2]) % imgBGWidth[2]);
                                i < GameScr.gW;
                                i += imgBGWidth[2])
                                g.drawImage(imgBG[2], i, gshouseY, 0);
                    if (typeBg != 11 && typeBg != 12 && imgBG[1] != null)
                        if (TileMap.mapID != 52)
                            for (var i = -((GameScr.cmx >> bgSpeed[1]) % imgBGWidth[1]);
                                i < GameScr.gW;
                                i += imgBGWidth[1])
                                g.drawImage(imgBG[1], i, bgLayer1y, 0);
                    if (TileMap.mapID == 45 || TileMap.mapID == 55)
                    {
                        g.setColor(0x110000);
                        g.fillRect(0, bgLayer0y + 20, GameScr.gW, GameScr.gH);
                    }

                    if (imgBG[0] != null)
                        for (var i = -((GameScr.cmx >> bgSpeed[0]) % imgBGWidth[0]); i < GameScr.gW; i += imgBGWidth[0])
                            g.drawImage(imgBG[0], i, bgLayer0y, 0);

                    if (imgCloud != null)
                        for (var i = 0; i < 2; i++)
                            g.drawImage(imgCloud, cloudX[i], cloudY[i], 3);
                }
                // ==============================
            }
            else
            {
                g.setColor(skyColor);
                g.fillRect(0, 0, GameScr.gW, GameScr.gH);
            }
        }

        public static void startDlgTime(string info, Command cmdYes, int time)
        {
            cmdYes.caption = MResources.OK;
            msgdlg.setInfo(info, null, cmdYes, null);
            msgdlg.setTimeLive(time);
            msgdlg.show();
            currentDialog = msgdlg;
        }

        public static void startWaitDlgWithoutCancel()
        {
            msgdlg.timeShow = 500;
            msgdlg.setInfo(MResources.PLEASEWAIT, null, null, null);
            currentDialog = msgdlg;
            msgdlg.isWait = true;
        }


        public static void StartDglThongBao(string text)
        {
            isPaintThongBao = true;
            indexThongBao = 0;
            thongbao = text;
        }

        public static void startCommandDlg(string info, Command cmdYes, Command cmdNo)
        {
            msgdlg.setInfo(info, cmdYes, null, cmdNo);
            msgdlg.isPaintCham = false;
            msgdlg.show();
            currentDialog = msgdlg;
        }

        public static void cleanImgBg()
        {
    //        for (var i = 0; i < keybgLoginSelect.size(); i++)
    //        {
    //            var key = (string) keybgLoginSelect.elementAt(i);
    //            if (key != null)
    //            {
    //                var imgg = (MBitmap) Image.listImgLoad.get(key);
    //                if (imgg != null)
    //                    imgg.cleanImg();
    //            }
    //        }
            keybgLoginSelect.removeAllElements();
        }
    }
}