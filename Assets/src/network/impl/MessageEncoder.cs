﻿
using System;
using src.lib;
using src.model;
using UnityEngine;

namespace src.network.impl
{
    public static class MessageEncoder
    {
        public static byte[] Encode(Message m)
        {
            try
            {
                var sendByteCount = Session.GetInstance().SendByteCount;
                var finalDataSize = 0;
                byte[] encryptedData = null;
                var cmd = m.Command;
                if (Session.GetInstance().Key != null)
                {
                    var data = m.Data;
                    int size;

                    if (data != null)
                    {
                        size = data.Length;
                        encryptedData = Cmd.IsRequireEncryptionCommand(cmd) ? MessageCipher.Encrypt(Converter.ConvertSbyteToByte(Session.GetInstance().Key), data) : data;
                        sendByteCount += 5 + data.Length;
                    }
                    else
                    {
                        size = 0;
                        sendByteCount += 5;
                    }

                    finalDataSize = size;
                    if (encryptedData != null)
                    {
                        finalDataSize = encryptedData.Length;
                    }

                }

                Session.GetInstance().SendByteCount = sendByteCount;
                
                var finalData = new byte[5 + finalDataSize];
                finalData[0] = Converter.ConvertSbyteToByte(cmd);
                var sizeBytes = Converter.ConvertIntToBytes(finalDataSize);
                finalData[1] = sizeBytes[0];
                finalData[2] = sizeBytes[1];
                finalData[3] = sizeBytes[2];
                finalData[4] = sizeBytes[3];

                if (finalDataSize > 0 && encryptedData != null)
                {
                    Buffer.BlockCopy(encryptedData, 0, finalData, 5, finalDataSize);
                }

                return finalData;
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }

            return null;
        }
    }
}