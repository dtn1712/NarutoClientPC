﻿using System.Collections.Generic;

namespace src.network.impl
{
    public class MessageQueue
    {
        
        private readonly List<Message> _messageQueue = new List<Message>();
        
        public Message PopMessageFromQueue()
        {
            var message = _messageQueue[0];
            _messageQueue.RemoveAt(0);
            return message;
        }

        public int GetMessageQueueSize()
        {
            return _messageQueue.Count;
        }

        public void AddMessageToQueue(Message message)
        {
            _messageQueue.Add(message);
        }

        public void Close()
        {
            _messageQueue.Clear();
        }
    }
}