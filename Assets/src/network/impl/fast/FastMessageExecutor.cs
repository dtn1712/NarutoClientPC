﻿using System;
using System.Collections.Generic;
using src.model;
using src.real.controller;
using UnityEngine;

namespace src.network.impl.fast
{
    public class FastMessageExecutor : IMessageExecutor
    {
        public void Execute(Message msg)
        {
            try
            {
                var controllers = MessageController.GetInstance().GetControllers();
                if (controllers.ContainsKey(msg.Command))
                {
                    controllers[msg.Command].processMessage(msg);
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        public void OnDisconnected()
        {
            throw new System.NotImplementedException();
        }

        public void OnConnectionFail()
        {
            throw new System.NotImplementedException();
        }
    }
}