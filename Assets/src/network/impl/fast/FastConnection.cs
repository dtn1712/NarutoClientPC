﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using src.lib;
using src.main;
using src.model;
using src.utils;
using UnityEngine;

namespace src.network.impl.fast
{
    public class FastConnection : IConnection
    {

        private UdpClient _udpClient;
        private bool _connected;
        private int _timeConnected;
        private IMessageWriter _messageWriter;
        private IMessageReader _messageReader;
        private readonly MessageQueue _writerMessageQueue = new MessageQueue();
        private readonly MessageQueue _readerMessageQueue = new MessageQueue();
        private readonly IMessageExecutor _messageExecutor = new FastMessageExecutor();
        
        public void Connect(string host, int port)
        {
            try
            {
                var remoteEndPoint = new IPEndPoint(IPAddress.Parse(host), port);
            
                _udpClient = new UdpClient();
                _udpClient.Connect(remoteEndPoint);
            
                _messageWriter = new FastMessageWriter(_udpClient);
                _messageReader = new FastMessageReader(_udpClient, remoteEndPoint);
            
                _connected = true;
                _timeConnected = Environment.TickCount;
            } catch (Exception e)
            {
                Debug.LogError(e);
            }
            
            
            var writerTask = Task.Factory.StartNew( () =>
            {
                while (_connected)
                {
                    while (_writerMessageQueue.GetMessageQueueSize() > 0)
                    {
                        var message = _writerMessageQueue.PopMessageFromQueue();
                        _messageWriter.WriteMessage(message);
                    }
                        
                    Thread.Sleep(5);
                }
            });
        
            var readerTask = Task.Factory.StartNew( () =>
            {
                try
                {
                    while (_connected)
                    {
                        var message = _messageReader.ReadMessage();
                        if (message != null)
                        {
                            OnReceiveMsg(message);
                        }
                        Thread.Sleep(5);
                    }
                } 
                catch (Exception e)
                {
                    Debug.LogError(e);
                }

                if (!_connected) return;
                    
                Debug.LogError("error read message!");
                if (_messageExecutor != null)
                {
                    if (Environment.TickCount - _timeConnected > 500)
                        _messageExecutor.OnDisconnected();
                    else
                        _messageExecutor.OnConnectionFail();
                }
                Close();
            });
            
            writerTask.Wait();
            readerTask.Wait();
        }

        public void Close()
        {
            _udpClient.Close();
            _writerMessageQueue.Close();
            _connected = false;
            _timeConnected = -1;
        }

        public void SendMessage(Message message)
        {
            _writerMessageQueue.AddMessageToQueue(message);
        }

        public void Update()
        {

            if (!_connected) return;
            
            while (_readerMessageQueue.GetMessageQueueSize() > 0)
            {
                var message = _readerMessageQueue.PopMessageFromQueue();
                if (message != null)
                {
                    _messageExecutor.Execute(message);
                }
            }
        }

        public bool IsServerAvailable(string host, int port)
        {
            throw new NotImplementedException();
        }

        public bool IsConnected()
        {
            return _connected;
        }

        public int GetTimeConnected()
        {
            return _timeConnected;
        }
        
        private void OnReceiveMsg(Message msg)
        {
            if (Thread.CurrentThread.Name == Main.MainThreadName)
                _messageExecutor.Execute(msg);
            else
                _readerMessageQueue.AddMessageToQueue(msg);
        }
    }
}