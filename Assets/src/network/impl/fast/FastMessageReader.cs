﻿
using System;
using System.Net;
using System.Net.Sockets;
using src.lib;
using src.model;
using UnityEngine;

namespace src.network.impl.fast
{
    public class FastMessageReader : IMessageReader
    {
        
        private readonly UdpClient _udpClient;
        private IPEndPoint _endPoint;

        public FastMessageReader(UdpClient udpClient, IPEndPoint endPoint)
        {
            _udpClient = udpClient;
            _endPoint = endPoint;
        }
        
        public Message ReadMessage()
        {
            var receiveData = _udpClient.Receive(ref _endPoint);
            try
            {
                var convertReceiveData = Converter.ConvertByteToSbyte(receiveData);
                // read message command
                var cmd = convertReceiveData[0];
                var size = ((convertReceiveData[1] & 0xff) << 24) | ((convertReceiveData[2] & 0xff) << 16) |
                           ((convertReceiveData[3] & 0xff) << 8) | (convertReceiveData[4] & 0xff);

                if (Cmd.IsRequireEncryptionCommand(cmd))
                {
                    // Decrypt the message
                    var dataTemp = new byte[size];
                    Array.Copy(receiveData, 5, dataTemp, 0, size);
                    var data = MessageCipher.Decrypt(Converter.ConvertSbyteToByte(Session.GetInstance().Key),dataTemp);
                    return new Message(cmd, data);
                }
                else
                {
                    var data = new byte[size];
                    Array.Copy(receiveData, 5, data, 0, size);
                    return new Message(cmd, data);
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }

            return null;
        }
    }
}