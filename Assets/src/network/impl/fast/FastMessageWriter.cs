﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using src.lib;
using UnityEngine;

namespace src.network.impl.fast
{
    public class FastMessageWriter : IMessageWriter
    {
        private readonly UdpClient _udpClient;

        public FastMessageWriter(UdpClient udpClient)
        {
            _udpClient = udpClient;
        }
        
        public void WriteMessage(Message m)
        {
            try
            {
                var finalData = MessageEncoder.Encode(m);
                _udpClient.SendAsync(finalData, finalData.Length);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }
        
    }
}