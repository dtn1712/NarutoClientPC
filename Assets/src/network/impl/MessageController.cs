﻿using System.Collections.Generic;
using src.model;
using src.real.controller;

namespace src.network.impl
{
    public class MessageController
    {
     
        private static readonly Dictionary<sbyte, IController> Controllers = new Dictionary<sbyte, IController>();
        
        private static readonly MessageController Instance = new MessageController();
   
        public static MessageController GetInstance()
        {
            return Instance;
        }

        private MessageController()
        {
            Controllers.Add(Cmd.REQUEST_KEY, new RequestAuthKeyController());
            Controllers.Add(Cmd.QUEST, new QuestController());
            Controllers.Add(Cmd.TALK_NPC, new TalkNPCController());
            Controllers.Add(Cmd.SKILL_CHAR, new SkillCharController());
            Controllers.Add(Cmd.REQUEST_SHOP_INFO, new RequestShopInfoController());
            Controllers.Add(Cmd.REQUEST_MENU_SHOP, new RequestMenuShopController());
//            Controllers.Add(Cmd.TRADE_ITEM, new TradeItemController());
//            Controllers.Add(Cmd.FRIEND, new FriendController());
            Controllers.Add(Cmd.PARTY, new PartyController());        // TODO: FIX DTO
            Controllers.Add(Cmd.MENU_NPC, new MenuNpcController());
            Controllers.Add(Cmd.NPC, new NpcController());
            Controllers.Add(Cmd.PICK_UP_ITEM, new PickUpItemController());
            Controllers.Add(Cmd.DROP_ITEM, new DropItemController());
            Controllers.Add(Cmd.CHAR_DEAD, new CharDeadController());
            Controllers.Add(Cmd.ATTACK, new AttackController());
            Controllers.Add(Cmd.MONSTER_INFO, new MonsterInfoController());
            Controllers.Add(Cmd.CHAT, new ChatController());
            Controllers.Add(Cmd.GET_ITEM_INVENTORY, new GetInventoryController());
            Controllers.Add(Cmd.PLAYER_OUT, new PlayerOutController());
            Controllers.Add(Cmd.BASIC_CHAR_INFO, new RequestCharInfoController());
            Controllers.Add(Cmd.REQUEST_IMAGE, new RequestImageController());
//            Controllers.Add(Cmd.PLAYER_MOVE, new PlayerMoveController());
            Controllers.Add(Cmd.CHANGE_MAP, new ChangeMapController());
//            Controllers.Add(Cmd.USE_ITEM, new ItemTemplateController());
            Controllers.Add(Cmd.MAIN_CHAR_INFO, new CharInfoController());
            Controllers.Add(Cmd.CHAR_LIST, new CharListController());
            Controllers.Add(Cmd.LOGIN, new LoginController());
//            Controllers.Add(Cmd.SERVER_MESSAGE, new ServerMessageController());
            Controllers.Add(Cmd.NPC_TEMPLATE, new NpcTemplateController());
            Controllers.Add(Cmd.MAP_TEMPLATE, new MapTemplateController());
            Controllers.Add(Cmd.CHAR_SKILL_STUDY, new CharSkillStudyController());
            Controllers.Add(Cmd.UPDATE_CHAR, new UpdateCharController());
            Controllers.Add(Cmd.COME_HOME, new ComeHomeController());
            Controllers.Add(Cmd.REGISTER, new RegisterController());
            Controllers.Add(Cmd.CHAR_EXP, new CharExpController());
            Controllers.Add(Cmd.REGION_INFO, new RegionInfoController());
            Controllers.Add(Cmd.CHANGE_REGION, new ChangeRegionController());
            Controllers.Add(Cmd.COMPETED_ATTACK, new CompetedAttackController());
            Controllers.Add(Cmd.STATUS_ATTACK, new StatusAttackController());
            Controllers.Add(Cmd.SERVER_NOTIFICATION, new ServerNotificationController());
            Controllers.Add(Cmd.UPDATE_CLIENT, new UpdateClientController());
            Controllers.Add(Cmd.UPGRADE_ITEM, new UpgradeItemController());
            Controllers.Add(Cmd.CLAN, new ClanController());
            Controllers.Add(Cmd.MAKE_PAYMENT, new MakePaymentController());
            Controllers.Add(Cmd.GET_PRICING, new GetPricingController());
            Controllers.Add(Cmd.SHOW_PAYMENT, new ShowPaymentController());
            Controllers.Add(Cmd.BOSS_APPEAR, new BossAppearController());
//            Controllers.Add(Cmd.SERVER_DIALOG, new ServerDialogController());
            
            Controllers.Add(Cmd.CHAR_WEARING, new CharWearingController());
            Controllers.Add(Cmd.CHAR_REVIVE, new CharReviveController());
            Controllers.Add(Cmd.ITEM_TEMPLATE, new ItemTemplateController());
            Controllers.Add(Cmd.CHAR_POSITION, new CharPositionController());
            Controllers.Add(Cmd.TRADE_INVITE, new TradeInviteController());
            Controllers.Add(Cmd.TRADE_ACCEPT_INVITE, new TradeAcceptInviteController());
            Controllers.Add(Cmd.TRADE_CANCEL, new TradeCancleController());
            Controllers.Add(Cmd.TRADE_MOVE_ITEM, new MoveItemTradeController());
            Controllers.Add(Cmd.TRADE_TAKE_OFF, new TakeOffItemTradeController());
            Controllers.Add(Cmd.TRADE_END, new EndTradeController());
            Controllers.Add(Cmd.TRADE_LOCK, new TradeClockController());
            Controllers.Add(Cmd.FRIEND_INVITE, new FriendInviteController());
            Controllers.Add(Cmd.FRIEND_GET_LIST, new RequestFriendListController());
            Controllers.Add(Cmd.FRIEND_KICK, new UnfriendController());
            Controllers.Add(Cmd.ALERT_TIME, new AlertTimeController());
            Controllers.Add(Cmd.SERVER_TOP_RIGHT_MSG, new ServerTopRightMsgController());
            Controllers.Add(Cmd.SERVER_MESSAGE_EFFECT, new UnfriendController());
            Controllers.Add(Cmd.ALERT_MESSAGE, new AlertMessageCotroller());
            Controllers.Add(Cmd.MONSTER_MOVE, new MonsterMoveController());
            Controllers.Add(Cmd.CIVIL_WAR_DROP_KAGE, new CivilWarDropKageController());

        }

        public Dictionary<sbyte, IController> GetControllers()
        {
            return Controllers;
        }
    }
}