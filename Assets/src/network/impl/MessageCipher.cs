﻿using System.Security.Cryptography;

namespace src.network.impl
{
    public static class MessageCipher
    {
        private static RijndaelManaged GetRijndaelManaged(byte[] keyBytes)
        {
            return new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = 128,
                BlockSize = 128,
                Key = keyBytes,
                IV = keyBytes
            };
        }

        public static byte[] Encrypt(byte[] key, byte[] rawData)
        {
            var rijndaelManaged = GetRijndaelManaged(key);
            return rijndaelManaged.CreateEncryptor().TransformFinalBlock(rawData, 0, rawData.Length);
        }

        public static byte[] Decrypt(byte[] key, byte[] encryptedData)
        {
            var rijndaelManaged = GetRijndaelManaged(key);
            return rijndaelManaged.CreateDecryptor()
                .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
            
        }
    }
}