﻿using System;
using System.IO;
using src.lib;
using src.model;
using UnityEngine;

namespace src.network.impl.reliable
{
    public class ReliableMessageReader : IMessageReader
    {

        private readonly BinaryReader _reader;

        public ReliableMessageReader(BinaryReader reader)
        {
            _reader = reader;
        }
        
        public Message ReadMessage()
        {
            var recvByteCount = Session.GetInstance().ReceiveByteCount;
            try
            {
                // read message command
                var cmd = _reader.ReadSByte();
                sbyte[] b = {_reader.ReadSByte(), _reader.ReadSByte(), _reader.ReadSByte(), _reader.ReadSByte()};

                var size = ((b[0] & 0xff) << 24) | ((b[1] & 0xff) << 16) |
                           ((b[2] & 0xff) << 8) | (b[3] & 0xff);


                var dataTemp = _reader.ReadBytes(size);

                recvByteCount += (5 + size);
                Session.GetInstance().ReceiveByteCount = recvByteCount;
                
                if (Cmd.IsRequireEncryptionCommand(cmd))
                {
                    // Decrypt the message
                    var data = MessageCipher.Decrypt(Converter.ConvertSbyteToByte(Session.GetInstance().Key), dataTemp);
                    return new Message(cmd, data);
                }
                else
                {
                    return new Message(cmd, dataTemp);
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }

            return null;
        }
    }
}