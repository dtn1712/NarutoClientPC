﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using src.model;
using src.utils;
using UnityEngine;

namespace src.network.impl.reliable
{
    public class ReliableConnection : IConnection
    {

        private bool _connected;
        private int _timeConnected;
        
        private BinaryReader _reader;
        private BinaryWriter _writer;
        
        private IMessageWriter _messageWriter;
        private IMessageReader _messageReader;
        
        private TcpClient _tcpClient;
        private NetworkStream _stream;
        private readonly IMessageExecutor _messageExecutor = new ReliableMessageExecutor();
        private readonly MessageQueue _writerMessageQueue = new MessageQueue();
        private readonly MessageQueue _readerMessageQueue = new MessageQueue();
        
        public void Connect(string host, int port)
        {
            try
            {
                _tcpClient = new TcpClient();
                _tcpClient.Connect(host, port);
                _stream = _tcpClient.GetStream();
                _reader = new BinaryReader(_stream, new System.Text.UTF8Encoding());
                _writer = new BinaryWriter(_stream, new System.Text.UTF8Encoding());

                _messageReader = new ReliableMessageReader(_reader);
                _messageWriter = new ReliableMessageWriter(_writer);

                _connected = true;
                _timeConnected = Environment.TickCount;
                
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
            
            
            var writerTask = Task.Factory.StartNew( () =>
                {
                    while (_connected)
                    {
                        while (_writerMessageQueue.GetMessageQueueSize() > 0)
                        {
                            var message = _writerMessageQueue.PopMessageFromQueue();
                            _messageWriter.WriteMessage(message);
                        }
                        
                        Thread.Sleep(5);
                    }
                });
        
            var readerTask = Task.Factory.StartNew( () =>
                {
                    try
                    {
                        while (_connected)
                        {
                            var message = _messageReader.ReadMessage();
                            if (message != null)
                            {
                                OnReceiveMsg(message);
                            }
                            else
                            {
                                break;
                            }
                            Thread.Sleep(5);
                        }
                    } 
                    catch (Exception e)
                    {
                        Debug.LogError(e);
                    }

                    if (!_connected) return;
                    
                    Debug.LogError("error read message!");
                    if (_messageExecutor != null)
                    {
                        if (Environment.TickCount - _timeConnected > 500)
                            _messageExecutor.OnDisconnected();
                        else
                            _messageExecutor.OnConnectionFail();
                    }
                    Close();
                });
            
            IoUtils.WriteMessage(Cmd.REQUEST_KEY);

            writerTask.Wait();
            readerTask.Wait();
        }

        public void Close()
        {
            try
            {
                _reader?.Close();
                _writer?.Close();
                _stream?.Close();
                _messageReader = null;
                _messageWriter = null;
                _connected = false;
                _timeConnected = -1;
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        public void SendMessage(Message message)
        {
            _writerMessageQueue.AddMessageToQueue(message);
        }

        public void Update()
        {
            if (!_connected) return;
            
            while (_readerMessageQueue.GetMessageQueueSize() > 0)
            {
                var message = _readerMessageQueue.PopMessageFromQueue();
                if (message != null)
                {
                    _messageExecutor.Execute(message);
                }
            }
        }

        public bool IsServerAvailable(string host, int port)
        {
            try
            {
                using(var client = new TcpClient())
                {
                    var result = client.BeginConnect(host, port, null, null);
                    var success = result.AsyncWaitHandle.WaitOne(TimeSpan.FromMilliseconds(2000));
                    if (!success)
                    {
                        return false;
                    }

                    client.EndConnect(result);
                }

            }
            catch
            {
                return false;
            }
            return true;
        }

        public bool IsConnected()
        {
            return _connected;
        }

        public int GetTimeConnected()
        {
            return _timeConnected;
        }
        
        private void OnReceiveMsg(Message msg)
        {
            if (Thread.CurrentThread.Name == Main.MainThreadName || msg.Command == Cmd.REQUEST_KEY)
                _messageExecutor.Execute(msg);
            else
                _readerMessageQueue.AddMessageToQueue(msg);
        }
        
        
    }
}