using System;
using System.Collections.Generic;
using src.main;
using src.model;
using src.real.controller;
using UnityEngine;

namespace src.network.impl.reliable
{
    public class ReliableMessageExecutor : IMessageExecutor
    {

        public void OnConnectionFail()
        {
            GameCanvas.currentScreen.isConnect = false;
            GameCanvas.currentScreen.connectCallBack = true;
        }

        public void OnDisconnected()
        {
            GameCanvas.currentScreen.isConnect = false;
            GameCanvas.currentScreen.isCloseConnect = true;
            GameCanvas.currentScreen.connectCallBack = true;
            GameCanvas.ResetToLoginScr(false);
        }

        public void Execute(Message msg)
        {
            try
            {
                var controllers = MessageController.GetInstance().GetControllers();
                if (controllers.ContainsKey(msg.Command))
                {
                    controllers[msg.Command].processMessage(msg);
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }   
  
    }
}