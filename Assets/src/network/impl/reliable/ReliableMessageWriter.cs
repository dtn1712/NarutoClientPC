﻿using System;
using System.Collections.Generic;
using System.IO;
using src.lib;
using UnityEngine;

namespace src.network.impl.reliable
{
    public class ReliableMessageWriter : IMessageWriter
    {
        
        private readonly BinaryWriter _writer;
        
        public ReliableMessageWriter(BinaryWriter writer)
        {
            _writer = writer;
        }

        public void WriteMessage(Message m)
        {
            try
            {
                var finalData = MessageEncoder.Encode(m);
                _writer.Write(finalData);
                _writer.Flush();
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }
        
        
    }
}