﻿namespace src.network
{
    public interface IConnection
    {
        void Connect(string host, int port);

        void Close();

        void SendMessage(Message message);

        void Update();

        bool IsServerAvailable(string host, int port);

        bool IsConnected();

        int GetTimeConnected();
    }
}