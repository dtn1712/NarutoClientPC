﻿using System;
using System.Collections.Generic;

namespace src.network
{
    public interface IMessageWriter
    {
        void WriteMessage(Message message);
        
    }
}