﻿namespace src.network
{
    public interface IMessageReader
    {
        Message ReadMessage();
    }
}