using System;
using System.Threading.Tasks;
using src.lib;
using src.model;
using UnityEngine;

namespace src.network
{
    public class Session
    {
        private static readonly Session Instance = new Session();
        

        public static Session GetInstance()
        {
            return Instance;
        }
        
        public int SendByteCount { get; set; }
        public int ReceiveByteCount { get; set; }
        
        public string SessionId { get; set; }
        public sbyte[] Key { get; set; }
        public IConnection ReliableConnection { get; set; }
        public IConnection FastConnection { get; set; }

        public void SendMessage(Message message)
        {
            try
            {
                if (Cmd.IsFastMessageCommand(message.Command))
                {
                    if (FastConnection.IsConnected())
                    {
                        FastConnection.SendMessage(message);
                    }
                }
                else
                {
                    if (ReliableConnection.IsConnected())
                    {
                        ReliableConnection.SendMessage(message);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
            
        }

        public void Disconnect()
        {
            Key = null;
            ReliableConnection.Close();
            FastConnection.Close();
        }

        public string GetByteCount()
        {
            var kb = ReceiveByteCount + SendByteCount;
            return kb / 1024 + "." + kb % 1024 / 102 + "Kb";
        }
        
        public void Update()
        {
            FastConnection?.Update();
            ReliableConnection?.Update();
        }
        
    }
}