using System.IO;
using src.lib;
using UnityEngine;

namespace src.network
{
	public class Message
	{

		public readonly sbyte Command;
		
		public byte[] Data { get; }
		
		private readonly MyReader _dis; 

		public Message(sbyte command) {
			Command = command;
		}

		public Message(sbyte command, byte[] data) {
			Command = command;
			Data = data;
			_dis = new MyReader(Converter.ConvertByteToSbyte(data));
		}

		public MyReader Reader() {
			return _dis;
		}

	
	}
}


