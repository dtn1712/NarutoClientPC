namespace src.network
{
    public interface IMessageExecutor
    {
        void Execute(Message message);

        void OnDisconnected();

        void OnConnectionFail();
    }
}