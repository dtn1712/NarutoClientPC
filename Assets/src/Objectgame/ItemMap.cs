﻿using System;
using src.lib;
using src.main;
using src.Objectgame;
using src.screen;
using UnityEngine;
using Char = src.Objectgame.Char;

public class ItemMap
{
    private int dem;
    public bool isMe, isSendNhat;
    public int itemMapID;
    public sbyte status, type;
    public ItemTemplate template;
    public long timeStartAdd;
    public long timeTonTai;
    public int x, y, xEnd, yEnd, vx, vy;

    public ItemMap(int itemMapID, short itemTemplateID, int x, int y)
    {
        this.itemMapID = itemMapID;
        template = ItemTemplates.Get(itemTemplateID);
        this.x = xEnd = x;
        this.y = yEnd = y;
        status = 1;
        timeStartAdd = MSystem.currentTimeMillis();
        dem = 0;
    }

    public void setPoint(int xEnd, int yEnd)
    {
        this.xEnd = xEnd;
        this.yEnd = yEnd;
        vx = (xEnd - x) >> 2;
        vy = (yEnd - y) >> 2;
        status = 2;
    }

    public void update()
    {
        dem++;
        if (dem % 25 == 0)
            ServerEffect.addServerEffect(36, x, y - 20, 1);
        if (status == 2 && x == xEnd && y == yEnd)
        {
            GameScr.vItemMap.removeElement(this);
            if (Char.myChar().itemFocus != null && Char.myChar().itemFocus.Equals(this))
                Char.myChar().itemFocus = null;
            if (isMe)
                GameCanvas.StartDglThongBao("Đã nhặt được " + template.name.ToLower());
            return;
        }
        if (status == 1)
            UpdateItem();
        if (status == 2)
        {
            if (vx == 0)
                x = xEnd;
            if (vy == 0)
                y = yEnd;
            if (x != xEnd)
            {
                x += vx;
                if (vx > 0 && x > xEnd || vx < 0 && x < xEnd)
                    x = xEnd;
            }
            if (y != yEnd)
            {
                y += vy;
                if (vy > 0 && y > yEnd || vy < 0 && y < yEnd)
                    y = yEnd;
            }
        }
    }

    public void paint(MGraphics g)
    {
        try
        {
            if (status != 2)
                g.drawImage(LoadImageInterface.bongChar, x,
                    y - 10, MGraphics.VCENTER | MGraphics.HCENTER);
            SmallImage.DrawSmallImage(g, template.iconID, x, y - 7, 0, MGraphics.BOTTOM | MGraphics.HCENTER);
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }

    private void UpdateItem()
    {
//        var wCount = 0;
        if (TileMap.GetInstance().TileTypeAt(x, y - 7, TileMap.T_TOP))
            return;
        // do item kage rot tu tren cao xuong phai chan lai 
//        while (wCount < 30)
//        {
//            wCount++;
            y += 24;
//        }
    }
}