using src.lib;
using src.main;
using src.model;
using src.screen;

namespace src.Objectgame
{
    public class BgItem
    {
        public static MyHashtable imgNew = new MyHashtable();
        public static MyHashtable imgPathLoad = new MyHashtable();
        public static MBitmap[] imgobj;

        public int dx;
        public int dy;
        public int id;
        public short idImage;

        private bool isBlur = false;

        public sbyte layer;
        public int[] tileX;
        public int[] tileY;
        public int trans;
        public int transX = 0;
        public int transY = 0;
        public int x;
        public int y;


        public void paint(MGraphics g)
        {
            if (imgobj[idImage] == null &&
                imgPathLoad.get("/mapobject/" + (idImage + SmallImage.ID_ADD_MAPOJECT) + ".png") == null)
                createBgItem(idImage);
            if (imgobj[idImage] == null) return;
            if (CRes.checkCollider(x,
                x + imgobj[idImage].GetWidth(), GameScr.cmx, GameScr.cmx + GameCanvas.w, y,
                y + imgobj[idImage].GetHeight(), GameScr.cmy, GameScr.cmy + GameCanvas.h))
                g.drawRegion(imgobj[idImage] /*GameScr.imgBgItem[idImage]*/, 0, 0,
                    imgobj[idImage].GetWidth(), imgobj[idImage].GetHeight(), trans, x + dx + transX,
                    y + dy + transY, 0);
        }

        private static void createBgItem(int id)
        {
            imgPathLoad.put("/mapobject/" + (id + SmallImage.ID_ADD_MAPOJECT) + ".png", 0);
            var img = (MBitmap) imgNew.get(id + "");
            if (img == null)
            {
                img = GameCanvas.loadImage("/mapobject/" + (id + SmallImage.ID_ADD_MAPOJECT) + ".png");
                if (img != null)
                    imgobj[id] = img;
            }
        }

        public static void cleanImg()
        {
            for (var i = 0; i < imgobj.Length; i++)
            {
//                if (imgobj[i] != null && imgobj[i].image != null)
//                    imgobj[i].cleanImg();
                
                imgobj[i] = null;
            }
        }



    }
}