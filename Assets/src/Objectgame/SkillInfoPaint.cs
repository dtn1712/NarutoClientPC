public class SkillInfoPaint
{
    public int adx;
    public int ady;

    public int arrowId;
    public int e0dx;
    public int e0dy;
    public int e1dx;
    public int e1dy;
    public int e2dx;
    public int e2dy;

    public int effS0Id;

    public int effS1Id;

    public int effS2Id;
    public int status;
}