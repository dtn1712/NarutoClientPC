using src.lib;

namespace src.Objectgame
{
    public class SkillTemplate
    {
        public int  ideff;
        public int[] cooldown, timebuffSkill;
        public string[] description;
        public int iconId, iconTron;
        public sbyte id;
        public short[] levelChar, mphao, tExistSubEff, usehp, usemp, rangelv, dame;
        public string name;
        public sbyte nlevelSkill;
        public sbyte[] pcSubEffAppear, ntargetlv;
        public int range, level = -1;
        public int type;
        public sbyte typeskill, ntarget, typebuffSkill, subeff;

        public SkillTemplate(sbyte id, string name, int idIcon, sbyte typeskilll, sbyte ntargett,
            sbyte typebuffSkilll, sbyte subEfff, short rangee, string des)
        {
            this.id = id;
            this.name = name;
            iconId = idIcon;
            typeskill = typeskilll;
            ntarget = ntargett;
            typebuffSkill = typebuffSkilll;
            subeff = subEfff;
            range = rangee;
            description = FontManager.GetInstance().tahoma_7_white.SplitFontArray(des, 149);
        }

        public void KhoitaoLevel(sbyte nlevelSkill)
        {
            this.nlevelSkill = nlevelSkill;
            levelChar = new short[nlevelSkill];
            mphao = new short[nlevelSkill];
            tExistSubEff = new short[nlevelSkill];
            usehp = new short[nlevelSkill];
            usemp = new short[nlevelSkill];
            rangelv = new short[nlevelSkill];
            cooldown = new int[nlevelSkill];
            timebuffSkill = new int[nlevelSkill];
            pcSubEffAppear = new sbyte[nlevelSkill];
            ntargetlv = new sbyte[nlevelSkill];
            dame = new short[nlevelSkill];
        }

        public int GetCoolDown(sbyte level)
        {
            return level >= cooldown.Length ? cooldown[0] : cooldown[level];
        }
    }
}