/*
 * this class use for chat of char
 */

using System;
using src.lib;
using src.network;
using src.Objectgame;
using src.real.dto.response;
using Char = src.Objectgame.Char;

public class ChatPrivate
{
    public static Vector vOtherchar = new Vector(); //list chater
    public static bool isBlock; //khoa tin nhan lai khong cho truot
    public static int nTinChuaDoc;

    // add when clicking chat private

    public static void AddNewChater(long idplayerNhan, string namethangnhan)
    {
        var other = new OtherChar(idplayerNhan, namethangnhan);
        vOtherchar.addElement(other);
    }

    //addd chat 
    public static void ChatFriend(ChatResponseDTO chatResponseDTO)
    {
        try
        {
            var idplayerGui = chatResponseDTO.playerSendId;
            var namethanggui = chatResponseDTO.playerSendName;
            var idplayerNhan = chatResponseDTO.playerReceiveId;
            var namethangnhan = chatResponseDTO.playerReceiveName;
            var mess = chatResponseDTO.text;

            if (idplayerGui == Char.myChar().charID) //chinh no
            {
                var isAdded = false;
                for (var i = 0; i < vOtherchar.size(); i++)
                {
                    var other = (OtherChar) vOtherchar.elementAt(i);
                    //thang nguoi nhan
                    if (other.id == idplayerNhan || other.name.Equals(namethangnhan))
                    {
                        isAdded = true;
                        var note = new NodeChat(mess, true, namethanggui);
                        var dem2 = new Vector();
                        for (var j = other.listChat.size() - 1; j >= 0; j--)
                        {
                            var nodechat = (NodeChat) other.listChat.elementAt(j);
                            if (nodechat != null)
                                dem2.addElement(nodechat);
                        }
                        other.listChat.removeAllElements();
                        other.listChat = dem2;
                        other.listChat.addElement(note);
                        var dem = new Vector();
                        for (var j = other.listChat.size() - 1; j >= 0; j--)
                        {
                            var nodechat = (NodeChat) other.listChat.elementAt(j);
                            if (nodechat != null)
                                dem.addElement(nodechat);
                        }
                        other.listChat.removeAllElements();
                        other.listChat = dem;
                        var demt = 0;
                        for (var j = other.listChat.size() - 1; j >= 0; j--)
                        {
                            var node2 = (NodeChat) other.listChat.elementAt(j);
                            if (node2 != null)
                                demt += node2.hnode + NodeChat.HSTRING / 2;
                        }
                    }
                }

                if (!isAdded || vOtherchar.size() == 0) //nhan tin tu player khac lan dau tien
                {
                    var other = new OtherChar(idplayerNhan, namethangnhan);

                    var note = new NodeChat(mess, true, namethanggui);
                    var dem2 = new Vector();

                    for (var j = other.listChat.size() - 1; j >= 0; j--)
                    {
                        var nodechat = (NodeChat) other.listChat.elementAt(j);
                        if (nodechat != null)
                            dem2.addElement(nodechat);
                    }
                    other.listChat.removeAllElements();
                    other.listChat = dem2;

                    other.listChat.addElement(note);
                    var dem = new Vector();
                    for (var j = other.listChat.size() - 1; j >= 0; j--)
                    {
                        var nodechat = (NodeChat) other.listChat.elementAt(j);
                        if (nodechat != null)
                            dem.addElement(nodechat);
                    }
                    other.listChat.removeAllElements();
                    other.listChat = dem;
                    var demt = 0;
                    for (var j = other.listChat.size() - 1; j >= 0; j--)
                    {
                        var node2 = (NodeChat) other.listChat.elementAt(j);
                        if (node2 != null)
                            demt += node2.hnode + NodeChat.HSTRING / 2;
                    }
                    if (isBlock && idplayerGui != Char.myChar().charID)
                    {
                    }
                    vOtherchar.addElement(other);
                }
            }
            else if (idplayerNhan == Char.myChar().charID)
            {
                //nhan tin tu thang khac
                var isAdded = false;
                for (var i = 0; i < vOtherchar.size(); i++)
                {
                    var other = (OtherChar) vOtherchar.elementAt(i);
                    other.nTinChuaDoc++;
                    if (other.id == idplayerGui || other.name.Equals(namethanggui))
                    {
                        other.nTinChuaDoc++;
                        other.id = idplayerGui;
                        isAdded = true;
                        var note = new NodeChat(mess, false, namethanggui);
                        var dem2 = new Vector();
                        for (var j = other.listChat.size() - 1; j >= 0; j--)
                        {
                            var nodechat = (NodeChat) other.listChat.elementAt(j);
                            if (nodechat != null)
                                dem2.addElement(nodechat);
                        }
                        other.listChat.removeAllElements();
                        other.listChat = dem2;

                        other.listChat.addElement(note);
                        var dem = new Vector();
                        for (var j = other.listChat.size() - 1; j >= 0; j--)
                        {
                            var nodechat = (NodeChat) other.listChat.elementAt(j);
                            if (nodechat != null)
                                dem.addElement(nodechat);
                        }
                        other.listChat.removeAllElements();
                        other.listChat = dem;
                        var demt = 0;
                        for (var j = other.listChat.size() - 1; j >= 0; j--)
                        {
                            var node2 = (NodeChat) other.listChat.elementAt(j);
                            if (node2 != null)
                                demt += node2.hnode + NodeChat.HSTRING / 2;
                        }
                    }
                }
                if (!isAdded || vOtherchar.size() == 0) // neu chua luu lan nao
                {
                    var other = new OtherChar(idplayerGui, namethanggui);
                    other.nTinChuaDoc++;
                    var note = new NodeChat(mess, false, namethanggui);
                    var dem2 = new Vector();
                    for (var j = other.listChat.size() - 1; j >= 0; j--)
                    {
                        var nodechat = (NodeChat) other.listChat.elementAt(j);
                        if (nodechat != null)
                            dem2.addElement(nodechat);
                    }
                    other.listChat.removeAllElements();
                    other.listChat = dem2;

                    other.listChat.addElement(note);
                    var dem = new Vector();
                    for (var j = other.listChat.size() - 1; j >= 0; j--)
                    {
                        var nodechat = (NodeChat) other.listChat.elementAt(j);
                        if (nodechat != null)
                            dem.addElement(nodechat);
                    }
                    other.listChat.removeAllElements();
                    other.listChat = dem;
                    var demt = 0;
                    for (var j = other.listChat.size() - 1; j >= 0; j--)
                    {
                        var node2 = (NodeChat) other.listChat.elementAt(j);
                        if (node2 != null)
                            demt += node2.hnode + NodeChat.HSTRING / 2;
                    }


                    vOtherchar.addElement(other);
                }
                nTinChuaDoc = 0;
                for (var i = 0; i < vOtherchar.size(); i++)
                {
                    var acc = (OtherChar) vOtherchar.elementAt(i);
                    if (acc != null) nTinChuaDoc += acc.nTinChuaDoc;
                }
            }
        }
        catch (Exception e)
        {
        }
    }
}