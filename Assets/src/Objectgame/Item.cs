using src.lib;

namespace src.Objectgame
{
    public class Item
    {
        // những item có option
        public const int TYPE_BODY_MIN = 0;

        public const int TYPE_BODY_MAX = 15;

        public const sbyte UI_WEAPON = 2; //  giao diện binh khí
        public const sbyte UI_BAG = 3; //  giao diện túi
        public const sbyte UI_BODY = 5; //  giao diện nhân vật
        public const sbyte UI_STACK = 6; //  giao diện dược phẩm
        public const sbyte UI_GROCERY = 8; // giao diện tạp hóa
        public const sbyte UI_STORE = 14; // giao diện cửa hàng
        public const sbyte UI_BOOK = 15; // giao diện quầy sách
        public const sbyte UI_LIEN = 16;
        public const sbyte UI_NHAN = 17;
        public const sbyte UI_NGOCBOI = 18;
        public const sbyte UI_PHU = 19;
        public const sbyte UI_NONNAM = 20;
        public const sbyte UI_NONNU = 21;
        public const sbyte UI_AONAM = 22;
        public const sbyte UI_AONU = 23;
        public const sbyte UI_GANGTAYNAM = 24;
        public const sbyte UI_GANGTAYNU = 25;
        public const sbyte UI_QUANNAM = 26;
        public const sbyte UI_QUANNU = 27;
        public const sbyte UI_GIAYNAM = 28;
        public const sbyte UI_GIAYNU = 29;
        public const sbyte UI_FASHION = 32; // giao diện thời trang
        public const sbyte UI_CLANSHOP = 34; // giao diện đồ gia tộc

        public const sbyte TYPE_BUA_CHU = 0;
        public const sbyte TYPE_SACH = 1;
        public const sbyte TYPE_TRUONG = 2;
        public const sbyte TYPE_PHAT_TRAN = 3;
        public const sbyte TYPE_KIEM = 4;
        public const sbyte TYPE_CUNG = 5;
        public const sbyte TYPE_QUYEN_DAU = 6;
        public const sbyte TYPE_HOAN = 7;
        public const sbyte TYPE_THUONG = 8;
        public const sbyte TYPE_CHUY = 9;
        public const sbyte TYPE_DAO_NGAN = 10;
        public const sbyte TYPE_QUAT = 11;

        public const sbyte TYPE_AO = 12;
        public const sbyte TYPE_GANG = 13;
        public const sbyte TYPE_GIAY = 14;
        public const sbyte TYPE_DAY_CHUYEN = 15;
        public const sbyte TYPE_NHAN = 16;
        public const sbyte TYPE_DAI_LUNG = 17;
        public const sbyte TYPE_NGOC_BOI = 18;
        public const sbyte TYPE_NON = 19;
        public const sbyte TYPE_BLOOD = 22;
        public const sbyte TYPE_BLOOD_LOW = 24;
        public const sbyte TYPE_MP = 25;
        public const sbyte TYPE_LEG = 23;
        public const sbyte TYPE_OTHER = 24;
        public const sbyte TYPE_DA = 29;
        public const sbyte TYPE_BUA_BAOHIEM = 30;

        private static readonly sbyte[][] POS_WEARING =
        {
            new[] {TYPE_NON},
            new[] {TYPE_NGOC_BOI},
            new[] {TYPE_DAY_CHUYEN},
            new[] {TYPE_GANG},
            new[] {TYPE_AO},
            new[]
            {
                TYPE_PHAT_TRAN, TYPE_TRUONG, TYPE_HOAN, TYPE_QUYEN_DAU, TYPE_KIEM, TYPE_QUAT,
                TYPE_BUA_CHU, TYPE_SACH, TYPE_THUONG, TYPE_CUNG, TYPE_DAO_NGAN, TYPE_CHUY
            },
            new[] {TYPE_NHAN}, // nhan trai
            new[] {TYPE_DAI_LUNG},
            new[] {TYPE_NHAN}, // nhan phai
            new[] {TYPE_GIAY},
            new[] {TYPE_BLOOD}, // chua dùng 
            new[] {TYPE_LEG}, // chua dung
            new[] {TYPE_OTHER} // chua dung
        };

        public int indexUI;
        public bool isLock;
        public int itemId;
        public sbyte level_dapdo = 0;
        public Vector options;
        public int quantity;
        public int value;
        public bool isUseInventory;
        public ItemTemplate template;

        public int typeUI;

        public int upgrade; // cường hóa

        public static int getPosWearingItem(int type)
        {
            for (var i = 0; i < POS_WEARING.Length; i++)
            for (var j = 0; j < POS_WEARING[i].Length; j++)
                if (POS_WEARING[i][j] == type)
                    return i;
            return -1; // item ko hợp lệ
        }

        public bool isTypeBody()
        {
            return TYPE_BODY_MIN <= template.type && template.type <= TYPE_BODY_MAX;
        }

        public bool isTypeUIShopView()
        {
            if (isTypeUIShop())
                return true;
        
            return isTypeUIStore() || isTypeUIBook() || isTypeUIFashion() || isTypeUIClanShop();
        }

        private bool isTypeUIShop()
        {
            return typeUI == UI_NONNAM || typeUI == UI_NONNU
                   || typeUI == UI_AONAM || typeUI == UI_AONU
                   || typeUI == UI_GANGTAYNAM || typeUI == UI_GANGTAYNU
                   || typeUI == UI_QUANNAM || typeUI == UI_QUANNU
                   || typeUI == UI_GIAYNAM || typeUI == UI_GIAYNU
                   || typeUI == UI_LIEN || typeUI == UI_NHAN
                   || typeUI == UI_NGOCBOI || typeUI == UI_PHU
                   || typeUI == UI_WEAPON || typeUI == UI_STACK
                   || typeUI == UI_GROCERY
                   || typeUI == UI_CLANSHOP;
        }

        private bool isTypeUIStore()
        {
            return typeUI == UI_STORE;
        }

        private bool isTypeUIBook()
        {
            return typeUI == UI_BOOK;
        }

        private bool isTypeUIFashion()
        {
            return typeUI == UI_FASHION;
        }

        private bool isTypeUIClanShop()
        {
            return typeUI == UI_CLANSHOP;
        }

        public static bool isBlood(int type)
        {
            return type == TYPE_BLOOD || type == TYPE_BLOOD_LOW;
        }

        public static bool isMP(int type)
        {
            return type == TYPE_MP;
        }

        public static bool isHP(int type)
        {
            return type == TYPE_BLOOD;
        }

        public static bool isItemDapDo(int type)
        {
            return type == TYPE_DA || type == TYPE_BUA_BAOHIEM;
        }

        public void paintItem(MGraphics g, int x, int y)
        {
            if (template != null && template.id != -1)
                SmallImage.DrawSmallImage(g, template.iconID, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER, true);
            if (quantity > 1 && (isBlood(template.type)
                                 || isMP(template.type)))
                FontManager.GetInstance().tahoma_7_white.DrawString(g, quantity + "", x + 12, y + 2, 1);
        }

        public void paintItem(MGraphics g, int x, int y, bool ispaintQuality)
        {
            if (template != null)
                SmallImage.DrawSmallImage(g, template.iconID, x, y, 0, MGraphics.VCENTER | MGraphics.HCENTER, true);
            if (!ispaintQuality && quantity > 1 && (isBlood(template.type)
                                                    || isMP(template.type)))
                FontManager.GetInstance().tahoma_7_white.DrawString(g, quantity + "", x + 12, y + 2, 1);
        }
    }
}