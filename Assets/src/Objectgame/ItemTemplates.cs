using System.Collections;

namespace src.Objectgame
{
    public static class ItemTemplates
    {
        private static readonly Hashtable Templates = new Hashtable();


        public static void Add(ItemTemplate it)
        {
            Templates.Add(it.id, it);
        }

        public static ItemTemplate Get(short id)
        {
            return (ItemTemplate) Templates[id];
        }
    }
}