using System;
using src.lib;
using src.model;
using src.real.dto.response;
using src.screen;

namespace src.Objectgame
{
    public class ChatWorld
    {
        public static Vector listNodeChat = new Vector();

        //add chat world
        public static void ChatWorldd(ChatResponseDTO chatResponseDTO)
        {
            try
            {
                var idplayerGui = chatResponseDTO.playerSendId;
                var namethanggui = chatResponseDTO.playerSendName;
                var mess = chatResponseDTO.text;
                var note = new NodeChat(mess, false, namethanggui, 170);
                var noteserver = new NodeChat(mess, false, namethanggui, 600);

                GameScr.listWorld.add(new InfoServer(InfoServer.CHATWORLD, noteserver));

                //show message on screen
                ChatManager.gI().addChat(MResources.GLOBALCHAT[0], namethanggui, mess);
                if (!ChatManager.blockGlobalChat)
                    Info.addInfo(namethanggui + ": " + mess, 80, FontManager.GetInstance().tahoma_7b_yellow);

                var dem2 = new Vector();
                for (var j = listNodeChat.size() - 1; j >= 0; j--)
                {
                    var nodechat = (NodeChat) listNodeChat.elementAt(j);
                    if (nodechat != null)
                        dem2.addElement(nodechat);
                }
                listNodeChat.removeAllElements();
                listNodeChat = dem2;
                listNodeChat.addElement(note);
                var dem = new Vector();
                for (var j = listNodeChat.size() - 1; j >= 0; j--)
                {
                    var nodechat = (NodeChat) listNodeChat.elementAt(j);
                    if (nodechat != null)
                        dem.addElement(nodechat);
                }
                listNodeChat.removeAllElements();
                listNodeChat = dem;
                var demt = 0;
                for (var j = listNodeChat.size() - 1; j >= 0; j--)
                {
                    var node2 = (NodeChat) listNodeChat.elementAt(j);
                    if (node2 != null)
                        demt += node2.hnode + NodeChat.HSTRING / 2;
                }

                if (listNodeChat.size() >= 200) //limit 200 message
                    listNodeChat.removeElementAt(listNodeChat.size() - 1);
            }
            catch (Exception e)
            {
                // TODO: handle exception
                //	e.printStackTrace();
            }
        }

        public static void Paint(MGraphics g, int xTab, int yTab, int width, Scroll scrMain)
        {
            var notehdem = 1;
            for (var i = listNodeChat.size() - 1; i >= 0; i--)
            {
                var node = (NodeChat) listNodeChat.elementAt(i);
                if (node != null)
                {
                    if (!node.isMe)
                        node.paint(g, xTab + NodeChat.HSTRING, yTab + NodeChat.HSTRING / 2 + notehdem, true, true);
                    else
                        node.paint(g, xTab + width - NodeChat.HSTRING - NodeChat.wnode + 40,
                            yTab + NodeChat.HSTRING / 2 + notehdem, true, true);
                    notehdem += node.hnode + NodeChat.HSTRING / 2;
                }
            }
            if (notehdem > scrMain.height)
                scrMain.cmtoY = -scrMain.height + notehdem; //dung de scroll tu duoi len
        }

        public static int update()
        {
            var dem = 0;
            for (var i = listNodeChat.size() - 1; i >= 0; i--)
            {
                var node = (NodeChat) listNodeChat.elementAt(i);
                if (node != null)
                    dem += node.hnode + NodeChat.HSTRING / 2;
            }
            return dem;
        }
    }
}