﻿using System;
using System.Collections.Generic;
using src.lib;

namespace src.Objectgame.quest
{
    public class QuestTemplate
    {
        public int Id { get; set; }
        public short Lv { get; set; }
        public short IdNpcReceive { get; set; }
        public short IdNpcResolve { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public string SupportContent { get; set; }
        public string ResolveContent { get; set; }
        public string ShortContent { get; set; }
        public string InfoFinish { get; set; }
        public string RemindContent { get; set; }
        public int Exp { get; set; }
        public int Gold { get; set; }
        public int Xu { get; set; }
        public List<QuestRequirement> Requirements { get; set; }

        public string[] ShortContentPaint;
        public string[] SupportContentPaint;
        public string[] RemindContentPaint;
        public string[] ContentPaint;

        public void paint()
        {
            if (SupportContent != null)
            {
                SupportContentPaint = FontManager.GetInstance().tahoma_7_white.SplitFontArray(SupportContent, 130);
            }
            if (RemindContent != null)
            {
                RemindContentPaint = FontManager.GetInstance().tahoma_7_white.SplitFontArray(RemindContent, InfoItem.wcat);
            }
            if (Content != null)
            {
                ContentPaint = FontManager.GetInstance().tahoma_7_white.SplitFontArray(Content, 130);
            }
            if (ShortContent != null)
            {
                ShortContentPaint = FontManager.GetInstance().tahoma_7_white.SplitFontArray(ShortContent, 130);
            }
        }

    }
}