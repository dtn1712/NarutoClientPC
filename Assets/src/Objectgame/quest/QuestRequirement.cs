﻿namespace src.Objectgame.quest
{
    public class QuestRequirement
    {
        public int Id { get; set; }
        public int QuestTemplateId { get; set; }
        public string Type { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}