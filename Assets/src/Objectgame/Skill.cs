namespace src.Objectgame
{
    public class Skill
    {
        public const sbyte ATT_STAND = 0;
        public const sbyte SKILL_CLICK_USE_BUFF = 2;

        public const sbyte STUDY_SKILL = 0;
        public const sbyte IMPROVE_SKILL = 1;
        
        public int dx;
        public int dy;
    }
}