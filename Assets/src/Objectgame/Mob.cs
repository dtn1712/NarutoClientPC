using System;
using src.lib;
using src.main;
using src.model;
using src.real;
using src.screen;
using UnityEngine;

namespace src.Objectgame
{
    public class Mob
    {
        public const sbyte TYPE_DUNG = 0;
        public const sbyte TYPE_DI = 1;
        public const sbyte TYPE_NHAY = 2;
        public const sbyte TYPE_LET = 3;
        public const sbyte TYPE_BAY = 4;
        public const sbyte TYPE_BAY_DAU = 5;
        public const sbyte TYPE_MOB_FRAME = 0;
        public const sbyte TYPE_MOB_TOOL = 1;

        // ========================
        public const sbyte MA_INHELL = 0; // trạng thái lần đầu tiên

        public const sbyte MA_DEADFLY = 1; // chết
        public const sbyte MA_STANDWAIT = 2; // nghỉ
        public const sbyte MA_ATTACK = 3; // đánh trái
        public const sbyte MA_STANDFLY = 4; // đậu khi bay
        public const sbyte MA_WALK = 5; // bay
        public const sbyte MA_FALL = 6; // rớt
        public const sbyte MA_INJURE = 7; // bị thương

        public const sbyte type_MOCNHAN = 43;
        public static bool isBossAppear = false;

        public static MyHashtable imgMob = new MyHashtable();
        public static MobTemplate[] arrMobTemplate;
        public static MBitmap[][] imgMobTem;

        public static int[] framemove = {0, 0, 0, 1, 1, 1, 2, 2, 2};
        public static int[] frameBeAttack = {4, 4, 4, 4, 6, 6, 6, 6, 4, 4, 4, 4};
        public static int[] frameAttack = {3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5};
        public Char cFocus;

        public int dame, dameMp;

        public int f = 1;

        private int frameIndex;

        // mobP1: bien dem;
        // mobF: loai quai

        // mobCharIndex:
        public int hp,
            maxHp = 1,
            x,
            y,
            frame,
            dir = 1,
            dirV = 1,
            status,
            p1,
            p2,
            p3,
            xFirst,
            yFirst,
            w,
            h,
            timeStatus;

        public sbyte idEffectAttack;
        public int idloadimage;
        public int indexRequestImg;

        public Char injureBy;

        public bool injureThenDie;
        public bool isBossOld, isBoss;

        public bool isBusyAttackSomeOne = true;
        public bool isGetInfo = false;
        public bool isDisable, isDontMove, isFire, isIce, isWind;
        public sbyte levelBoss, level;
        public int minX, minY, maxX, maxY;

        public short mobId;
        public string mobName;

        public Mob mobToAttack;
        public int typeAtt;
        public int templateId;
        private long timeStartDie = 0;


        public int typeMob = 0;
        public int ybong;


        public Mob(sbyte mobId, int hp, sbyte level, int maxhp, short x, short y,
            short minX, short minY, short maxX, short maxY)
        {
            y -= 10;
            this.mobId = mobId;
            this.hp = hp;
            this.level = level;
            maxHp = maxhp;
            this.x = x;
            xFirst = x;
            this.y = y;
            yFirst = y;
            this.minX = minX;
            this.minY = minY;
            this.maxX = maxX;
            this.maxY = maxY;
            dir = 1;
            dir = CRes.random(2) % 2 == 0 ? -1 : 1;
            status = MA_WALK;
            idEffectAttack = (sbyte) CRes.random(5);
            if (arrMobTemplate[templateId].imgs == null)
                arrMobTemplate[templateId].imgs = new MBitmap[8];
        }

        public int getW()
        {
            return w;
        }

        public int getH()
        {
            return h;
        }

        private MBitmap createImage(int id, int sub)
        {
            if (typeMob == TYPE_MOB_TOOL)
            {
                var path = "/mob/" + (id + SmallImage.ID_ADD_MOB) + "/_" + 1 + ".png";
                var pathRequest = "/mob/" + (id + SmallImage.ID_ADD_MOB);
                arrMobTemplate[mobId].data.img = GameCanvas.loadImage(path);
                if (arrMobTemplate[mobId].data.img == null)
                {
                    var path2 = "/mob/" + id + "/" + 1 + ".png";
                    arrMobTemplate[mobId].data.img = GameCanvas.loadImage(path2);
                }
                if (arrMobTemplate[mobId].data.img == null && imgMob.get(pathRequest) == null)
                {
                    var data = Rms.loadRMS(MSystem.getPathRMS(SmallImage.getPathImage(id + SmallImage.ID_ADD_MOB)) +
                                           "" + (id + SmallImage.ID_ADD_MOB + "/_" + (sub + 1)));
                    if (data == null)
                    {
                        imgMob.put(pathRequest, "" + 0);
                        Service.GetInstance().RequestImage(id + SmallImage.ID_ADD_MOB);
                    }
                }

                return arrMobTemplate[mobId].data.img;
            }

            if (imgMobTem == null) imgMobTem = new MBitmap[100][];
            if (imgMobTem[id] == null)
                imgMobTem[id] = new MBitmap[7];
            if (sub < imgMobTem[id].Length && imgMobTem[id][sub] == null)
            {
                var path = "/mob/" + (id + SmallImage.ID_ADD_MOB) + "/_" + (sub + 1) + ".png";
                var pathRequest = "/mob/" + (id + SmallImage.ID_ADD_MOB);
                imgMobTem[id][sub] = GameCanvas.loadImage(path);
                if (imgMobTem[id][sub] == null)
                {
                    var path2 = "/mob/" + id + "/" + (sub + 1) + ".png";
                    imgMobTem[id][sub] = GameCanvas.loadImage(path2);
                }
                if (imgMobTem[id][sub] == null && imgMob.get(pathRequest) == null)
                {
                    var data = Rms.loadRMS(MSystem.getPathRMS(SmallImage.getPathImage(id + SmallImage.ID_ADD_MOB)) +
                                           "" + (id + SmallImage.ID_ADD_MOB + "/_" + (sub + 1)));
                    if (data == null)
                    {
                        imgMob.put(pathRequest, "" + 0);
                        Service.GetInstance().RequestImage(id + SmallImage.ID_ADD_MOB);
                    }
                }
            }
            else if (imgMobTem[id][sub] == null)
            {
                return null;
            }
            return imgMobTem[id][sub];
        }

        public static void cleanImg()
        {
            if (imgMobTem == null) return;

            for (var i = 0; i < imgMobTem.Length; i++)
            {
                if (imgMobTem[i] == null) continue;
                for (var j = 0; j < imgMobTem[i].Length; j++)
                {
//                    if (imgMobTem[i][j] != null)
//                        imgMobTem[i][j].cleanImg();
                    imgMobTem[i][j] = null;
                }
            }
        }

        public void update()
        {

            if (typeMob == TYPE_MOB_TOOL && !isBossAppear)
                return;
            if (arrMobTemplate[mobId] == null)
            {
                arrMobTemplate[mobId] = new MobTemplate();
            }
            else
            {
                if (typeMob == TYPE_MOB_FRAME && arrMobTemplate[mobId] != null && arrMobTemplate[mobId].imgs != null &&
                    arrMobTemplate[mobId].imgs[GameCanvas.gameTick / 2 % 7] == null)
                {
                    try
                    {
                        indexRequestImg = (indexRequestImg + 1) % 1000;
                        if (indexRequestImg % 4 == 0)
                            for (var i = 0; i < arrMobTemplate[mobId].imgs.Length; i++)
                            {
                                if (arrMobTemplate[mobId].imgs[i] == null)
                                    if (idloadimage == type_MOCNHAN && i > 0 && i < 3) //moc nhan
                                        arrMobTemplate[mobId].imgs[i] = arrMobTemplate[mobId].imgs[0];
                                    else if (idloadimage == type_MOCNHAN && i > 3) //moc nhan
                                        arrMobTemplate[mobId].imgs[i] = arrMobTemplate[mobId].imgs[3];
                                    else
                                        arrMobTemplate[mobId].imgs[i] =
                                            createImage(idloadimage,
                                                i); //GameCanvas.loadImage("/mob/"+idloadimage+"/"+(i+1)+".png"); 
                                if (arrMobTemplate != null && arrMobTemplate[mobId] != null &&
                                    arrMobTemplate[mobId].imgs[i] != null)
                                {
                                    h = MGraphics.getImageHeight(arrMobTemplate[mobId].imgs[i]);
                                    w = MGraphics.getImageWidth(arrMobTemplate[mobId].imgs[i]);
                                }
                                //							
                            }
                    }
                    catch (Exception e)
                    {
                    }
                }
                else if (typeMob == TYPE_MOB_TOOL && arrMobTemplate[mobId] != null &&
                         arrMobTemplate[mobId].data.img == null)
                {
                    arrMobTemplate[mobId].data.img =
                        createImage(idloadimage, 1); //GameCanvas.loadImage("/mob_boss/big0.png");
                    h = arrMobTemplate[mobId].data.width;
                    w = arrMobTemplate[mobId].data.height;
                }
            }
            if (status != MA_ATTACK && isBusyAttackSomeOne)
            {
                if (cFocus != null)
                {
                    cFocus.DoInjure(dame, dameMp, isBossOld, GetTemplate().mobTemplateId);
                    cFocus = null;
                }
                isBusyAttackSomeOne = false;
            }
            UpdateShadown();
            switch (status)
            {
                case MA_DEADFLY:
                    p1++;

                    y += p1;
                    if (GameCanvas.gameTick % 2 == 0)
                        if (p2 > 1)
                            p2--;
                        else if (p2 < -1)
                            p2++;
                    x += p2;
                    frame = isBossOld ? 10 : 2;
                    if (y > GameScr.gssye * 24 || x < GameScr.gssx * 24 || x > GameScr.gssxe * 24)
                    {
                        p1 = 0;
                        p2 = 0;

                        x = xFirst;
                        y = yFirst;
                        hp = GetTemplate().hp;
                        status = MA_INHELL;
                        frame = 0;
                        timeStatus = 0;
                        return;
                    }

                    if (p3 == 0 && (TileMap.GetInstance().TileTypeAtPixel(x, y) & TileMap.T_TOP) == TileMap.T_TOP)
                    {
                        p1 = p1 > 4 ? -4 : -p1;
                        p3 = 16;
                    }
                    if (p3 > 0)
                        p3--;


                    break;
                case MA_STANDWAIT:
                    timeStatus = 0;
                    updateMobStandWait();
                    break;
                case MA_STANDFLY:
                    timeStatus = 0;
                    frame = 0;
                    p1++;
                    if (p1 > 40 + mobId % 5)
                    {
                        y -= 2;
                        status = MA_WALK;
                        p1 = 0;
                    }
                    break;
                case MA_ATTACK:
                    UpdateMobAttack();
                    break;
                case MA_WALK:
                    try
                    {
                        if (GameCanvas.gameTick % 4 == 0 && isBossOld)
                        {
                            frameIndex++;
                            if (frameIndex > arrMobTemplate[templateId].frameBossMove.Length - 1)
                                frameIndex = 0;
                        }
                    }
                    catch (Exception e)
                    {
                    }
                    timeStatus = 0;
                    UpdateMobWalk();
                    break;
                case MA_FALL:
                    timeStatus = 0;
                    p1++;
                    y += p1;
                    if (y >= yFirst)
                    {
                        y = yFirst;
                        p1 = 0;
                        status = MA_WALK;
                    }
                    break;
                case MA_INJURE:
                    updateInjure();
                    break;
            }
            if (mobToAttack != null)
            {
                var dx = mobToAttack.x - x;
                var dy = mobToAttack.y - y;
                x += dx / 4;
                y += dy / 4;
                if (mobToAttack.status == MA_DEADFLY || mobToAttack.status == MA_INHELL ||
                    CRes.abs(dx) < 20 && CRes.abs(dy) < 20)
                {
                    ServerEffect.addServerEffect(59, mobToAttack.x, mobToAttack.y, 1);
                    mobToAttack = null;
                }
            }
        }

        public void setInjure()
        {
            if (hp > 0)
            {
                f = 0;
                timeStatus = 12;
                status = MA_INJURE;
            }
        }

        public void setAttack(Char cFocus)
        {
            isBusyAttackSomeOne = true;
            this.cFocus = cFocus;
            p1 = 0;
            p2 = 0;
            f = 0;
            status = MA_ATTACK;
            frameIndex = 0;
            typeAtt = 0;
        }


        private void updateInjure()
        {
            frame = isBossOld
                ? GetTemplate().mobTemplateId == 139 ? 4 :
                GetTemplate().mobTemplateId == 160 ? 12 : 10
                : 2;
            f++;
            if (f == frameBeAttack.Length / 2)
                ServerEffect.addServerEffect(25, x, y - h / 2 - 10, 1);
            frame = frameBeAttack[f % frameBeAttack.Length];
            if (GetTemplate().mobTemplateId == 141)
                frame = 13;
            timeStatus--;
            if (timeStatus <= 0)
            {
                if (injureBy != null && injureThenDie || hp == 0)
                {
                    status = MA_DEADFLY;
                    p2 = injureBy.cdir << 3;
                    p1 = -5;
                    p3 = 0;
                }
                else
                {
                    status = MA_WALK;
                    x = xFirst;
                    y = yFirst;
                    if (injureBy != null)
                    {
                        dir = -injureBy.cdir;
                        if (CRes.abs(x - injureBy.cx) < 24)
                            status = MA_STANDWAIT;
                    }
                    p1 = p2 = p3 = 0;
                    timeStatus = 0;
                }
                injureBy = null;
                return;
            }
            if (arrMobTemplate[templateId].type != 0)
            {
                var fallBack = -injureBy.cdir << 1;
                if (x > xFirst - arrMobTemplate[templateId].rangeMove && x < xFirst + arrMobTemplate[templateId].rangeMove)
                    x -= fallBack;
            }
        }

        private void updateMobStandWait()
        {
            switch (arrMobTemplate[templateId].type)
            {
                case TYPE_DUNG:
                case TYPE_DI:
                case TYPE_NHAY:
                case TYPE_LET:
                    frame = 0;
                    p1++;
                    if (p1 > 10 + mobId % 10)
                        status = MA_WALK;
                    if (isBossOld)
                        frame = GameCanvas.gameTick % 101 > 1 ? 0 : 1;
                    break;
                case TYPE_BAY:
                case TYPE_BAY_DAU:
                    frame = GameCanvas.gameTick % 4 > 1 ? 0 : 1;
                    p1++;
                    if (p1 > mobId % 3)
                        status = MA_WALK;
                    break;
            }
        }

        private void UpdateMobAttack()
        {
            f++;
            if (f < 2 && typeMob == TYPE_MOB_FRAME)
                GameScr.AddEffectKillMobAttack(this, cFocus, EffectKill.EFF_NORMAL, idEffectAttack);
            if (f >= frameAttack.Length)
                status = MA_WALK;
            frame = frameAttack[f % frameAttack.Length];
            if (p1 == 0)
            {
                int xx = 0, yy = 0;
                if (typeAtt == 0)
                {
                    xx = cFocus.cx;
                    yy = cFocus.cy;
                }
                if (CRes.abs(xx - x) < 24 || CRes.abs(xx - x) < 5 || arrMobTemplate[templateId].type == 0)
                    frame = arrMobTemplate[templateId].imgs.Length == 3 ? 0 : 3;

                if (isBossOld && (CRes.abs(xx - x) < 48 || CRes.abs(xx - x) < 10 || arrMobTemplate[templateId].type == 0)
                ) //level boss
                    frame = arrMobTemplate[templateId].imgs.Length == 3 ? 0 : 3;

                if (isBossOld)
                {
                    frameIndex++;
                    if (CRes.abs(xx - x) < 48 || CRes.abs(yy - y) < 10)
                    {
                        if (frameIndex >= arrMobTemplate[templateId].frameBossAttack[0].Length)
                            frameIndex = 0;
                        frame = arrMobTemplate[templateId].frameBossAttack[0][frameIndex];
                    }
                    else
                    {
                        if (frameIndex >= arrMobTemplate[templateId].frameBossAttack[1].Length)
                            frameIndex = 0;
                        frame = arrMobTemplate[templateId].frameBossAttack[1][frameIndex];
                    }
                }
                if (frame == 3)
                    p1 = 1;

                if (arrMobTemplate[templateId].type != 0 && !isDontMove && isIce && isWind)
                    x += (xx - x) / 3;

                if (x > xFirst + arrMobTemplate[templateId].rangeMove)
                    p1 = 1;

                if (x < xFirst - arrMobTemplate[templateId].rangeMove)
                    p1 = 1;

                if ((arrMobTemplate[templateId].type == 4 || arrMobTemplate[templateId].type == 5) && !isDontMove)
                    y += (yy - y) / 20;
                p2++;
                if (isBossOld && CRes.abs(xx - x) < 48 && CRes.abs(yy - y) < 15 ||
                    CRes.abs(xx - x) < 12 && CRes.abs(yy - y) < 12 || p2 > 12 || p1 == 1)
                {
                    p1 = 1;
                    if (typeAtt == 0)
                        if (isBossOld && CRes.abs(xx - x) < 48 && CRes.abs(yy - y) < 15)
                        {
                            //59, 52
                            cFocus.DoInjure(dame, dameMp, isBossOld, GetTemplate().mobTemplateId);
                            isBusyAttackSomeOne = false;
                            if (CRes.abs(xx - x) < 24 && CRes.abs(yy - y) < 15)
                            {
                                cFocus.DoInjure(dame, dameMp, isBossOld, GetTemplate().mobTemplateId);
                                isBusyAttackSomeOne = false;
                            }
                            else
                            {
                                if (isBossOld)
                                    if (GetTemplate().mobTemplateId == 116)
                                    {
                                        ServerEffect.addServerEffect(84, cFocus, 2);
                                        isBusyAttackSomeOne = false;
                                        cFocus.DoInjure(dame, dameMp, isBossOld, GetTemplate().mobTemplateId);
                                    }
                                    else if (GetTemplate().mobTemplateId == 138)
                                    {
                                        ServerEffect.addServerEffect(83, cFocus, 2);
                                        isBusyAttackSomeOne = false;
                                        cFocus.DoInjure(dame, dameMp, isBossOld, GetTemplate().mobTemplateId);
                                    }
                                    else if (GetTemplate().mobTemplateId == 140 || GetTemplate().mobTemplateId == 161)
                                    {
                                        ServerEffect.addServerEffect(110, cFocus, 2);
                                        ServerEffect.addServerEffect(104, cFocus.cx - 20, cFocus.cy, 2);
                                        ServerEffect.addServerEffect(104, cFocus.cx + 20, cFocus.cy, 2);
                                        isBusyAttackSomeOne = false;
                                        cFocus.DoInjure(dame, dameMp, isBossOld, GetTemplate().mobTemplateId);
                                    }
                                    else if (GetTemplate().mobTemplateId == 141 || GetTemplate().mobTemplateId == 162)
                                    {

                                        ServerEffect.addServerEffect(121, cFocus, 1);
                                        isBusyAttackSomeOne = false;
                                        cFocus.DoInjure(dame, dameMp, isBossOld, GetTemplate().mobTemplateId);
                                    }
                                    else if (GetTemplate().mobTemplateId == 160)
                                    {
                                        ServerEffect.addServerEffect(124, cFocus, 2);
                                        isBusyAttackSomeOne = false;
                                        cFocus.DoInjure(dame, dameMp, isBossOld, GetTemplate().mobTemplateId);
                                    }
                                    else if (GetTemplate().mobTemplateId == 164 || GetTemplate().mobTemplateId == 165)
                                    {
                                        ServerEffect.addServerEffect(126, cFocus, 1);
                                        isBusyAttackSomeOne = false;
                                        cFocus.DoInjure(dame, dameMp, isBossOld, GetTemplate().mobTemplateId);
                                    }
                                isBusyAttackSomeOne = false;
                            }
                            if (typeAtt == 1)
                                if (CRes.abs(xx - x) < 24 && CRes.abs(yy - y) < 15)
                                    isBusyAttackSomeOne = false;
                                else
                                    isBusyAttackSomeOne = false;
                        }
                    dir = x < xx ? 1 : -1;
                }
                else if (p1 == 1)
                {
                    if (arrMobTemplate[templateId].type != 0 && !isDontMove && !isIce && !isWind)
                    {
                        x += (xFirst - x) / 4;
                        y += (yFirst - y) / 4;
                    }
                    if (CRes.abs(xFirst - x) < 5 && CRes.abs(yFirst - y) < 5)
                    {
                        status = MA_STANDWAIT;
                        p1 = 0;
                        p2 = 0;
                    }
                }
            }
        }

        private void UpdateMobWalk()
        {
            if (templateId > arrMobTemplate.Length - 1) return;
            if (idloadimage != type_MOCNHAN)
            {
                arrMobTemplate[templateId].type = 3;
                arrMobTemplate[templateId].speed = 1;
                arrMobTemplate[templateId].rangeMove = 20;
            }
        
            try
            {
                if (injureThenDie)
                {
                    status = MA_DEADFLY;
                    p2 = injureBy.cdir << 3;
                    p1 = -5;
                    p3 = 0;
                }
                if (isIce)
                    return;
                if (isDontMove || isWind)
                {
                    frame = 0;
                    return;
                }
                switch (arrMobTemplate[templateId].type)
                {
                    case TYPE_DUNG: // dung yen
                        frame = 0;
                        break;
                    case TYPE_DI:
                    case TYPE_NHAY:
                    case TYPE_LET:
                        var speed = arrMobTemplate[templateId].speed;
                        if (speed == 1)
                        {
                            if (GameCanvas.gameTick % 2 == 1)
                                break;
                        }
                        else if (speed > 2)
                        {
                            speed += (sbyte) (mobId % 2);
                        }
                        else if (GameCanvas.gameTick % 2 == 1)
                        {
                            speed--;
                        }
                        x += speed * dir;
                        if (x > xFirst + arrMobTemplate[templateId].rangeMove)
                            dir = -1;
                        else if (x < xFirst - arrMobTemplate[templateId].rangeMove)
                            dir = 1;
                        if (!isBossOld)
                        {
                            UpdateChangeFrame();
                            frame = framemove[f];
                        }
                        else
                        {
                            frame = arrMobTemplate[templateId].frameBossMove[frameIndex];
                        }
                        break;
                    case TYPE_BAY:
                        var speed2 = arrMobTemplate[templateId].speed;
                        speed2 += (sbyte) (mobId % 2);
                        x += speed2 * dir;
                        if (GameCanvas.gameTick % 10 > 2)
                            y += speed2 * dirV;
                        speed2 += (sbyte) ((GameCanvas.gameTick + mobId) % 2);
                        if (x > xFirst + arrMobTemplate[templateId].rangeMove)
                        {
                            dir = -1;
                            status = MA_STANDWAIT;
                            p1 = 0;
                        }
                        else if (x < xFirst - arrMobTemplate[templateId].rangeMove)
                        {
                            dir = 1;
                            status = MA_STANDWAIT;
                            p1 = 0;
                        }
                        if (y > yFirst + 24)
                            dirV = -1;
                        else if (y < yFirst - (20 + GameCanvas.gameTick % 10))
                            dirV = 1;
                        if (!isBossOld)
                            frame = GameCanvas.gameTick % 4 > 1 ? 0 : 1;
                        else
                            frame = arrMobTemplate[templateId].frameBossMove[frameIndex];

                        break;
                    case TYPE_BAY_DAU:
                        var speed3 = arrMobTemplate[templateId].speed;
                        speed3 += (sbyte) (mobId % 2);
                        x += speed3 * dir;
                        speed3 += (sbyte) ((GameCanvas.gameTick + mobId) % 2);
                        if (GameCanvas.gameTick % 10 > 2)
                            y += speed3 * dirV;

                        if (x > xFirst + arrMobTemplate[templateId].rangeMove)
                        {
                            dir = -1;
                            status = MA_STANDWAIT;
                            p1 = 0;
                        }
                        else if (x < xFirst - arrMobTemplate[templateId].rangeMove)
                        {
                            dir = 1;
                            status = MA_STANDWAIT;
                            p1 = 0;
                        }
                        if (y > yFirst + 24)
                            dirV = -1;
                        else if (y < yFirst - (20 + GameCanvas.gameTick % 10))
                            dirV = 1;
                        if (TileMap.GetInstance().TileTypeAt(x, y, TileMap.T_TOP))
                            if (GameCanvas.gameTick % 10 > 5)
                            {
                                y = TileMap.GetInstance().TileYofPixel(y);
                                status = MA_STANDFLY;
                                p1 = 0;
                                dirV = -1;
                            }
                            else
                            {
                                dirV = -1;
                            }
                        if (!isBossOld)
                            frame = GameCanvas.gameTick % 4 > 1 ? 3 : 1;
                        else
                            frame = arrMobTemplate[templateId].frameBossMove[frameIndex];
                        break;
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        private MobTemplate GetTemplate()
        {
            return arrMobTemplate[templateId];
        }

        private bool IsPaint()
        {
            if (x < GameScr.cmx)
                return false;
            if (x > GameScr.cmx + GameScr.gW)
                return false;
            if (y < GameScr.cmy)
                return false;
            if (y > GameScr.cmy + GameScr.gH + 30)
                return false;
            if (arrMobTemplate[templateId] == null)
                return false;
            if (!isBossOld)
            {
                if (frame >= arrMobTemplate[templateId].imgs.Length)
                    return false;
                if (arrMobTemplate[templateId].imgs[frame] == null)
                    return false;
            }
            if (status == MA_INHELL)
                return false;
            return true;
        }


        public void paint(MGraphics g)
        {
            if (!IsPaint())
                return;
            if (typeMob == TYPE_MOB_TOOL && !isBossAppear)
                return;
            if (status != MA_INHELL)
            {
                PaintShadow(g);
                var mt = arrMobTemplate[mobId];
                if (typeMob == TYPE_MOB_FRAME && mt.imgs[frame] != null)
                {
                    if (CRes.checkCollider(x,
                        x + mt.imgs[frame].GetWidth(), GameScr.cmx, GameScr.cmx + GameCanvas.w, y,
                        y + mt.imgs[frame].GetHeight(), GameScr.cmy, GameScr.cmy + GameCanvas.h))
                        if (typeMob == TYPE_MOB_FRAME)
                            g.drawRegion(mt.imgs[frame], 0, 0, MGraphics.getImageWidth(mt.imgs[frame]),
                                MGraphics.getImageHeight(mt.imgs[frame]), dir > 0 ? 2 : 0, x,
                                y - MGraphics.getImageHeight(mt.imgs[frame]) / 2, MGraphics.VCENTER | MGraphics.HCENTER);
                }
                else if (typeMob == TYPE_MOB_TOOL && mt.data.img != null)
                {
                    mt.data.paintFrame(g, frame, x, y, dir > 0 ? 1 : 0, 0);
                }
            }
        }


        public void StartDie()
        {
            hp = 0;

            injureThenDie = true;
            hp = 0;
            status = MA_DEADFLY;
            p1 = -5;
            p2 = -dir << 3;
            p3 = 0;
        }

        private void UpdateChangeFrame()
        {
            f++;
            if (f > framemove.Length - 1)
                f = 0;
        }


        private void PaintShadow(MGraphics g)
        {
            int size = TileMap.GetInstance().size;
            if (TileMap.GetInstance().TileTypeAt(x + size / 2, ybong + 1, TileMap.T_LEFT))
                g.setClip(x / size * size, (ybong - 30) / size * size, size, 100);
            else if (TileMap.GetInstance().TileTypeAt((x - size / 2) / size, (ybong + 1) / size) == 0)
                g.setClip(x / size * size, (ybong - 30) / size * size, 100, 100);
            else if (TileMap.GetInstance().TileTypeAt((x + size / 2) / size, (ybong + 1) / size) == 0)
                g.setClip(x / size * size, (ybong - 30) / size * size, size, 100);
            else if (TileMap.GetInstance().TileTypeAt(x - size / 2, ybong + 1, TileMap.T_RIGHT))
                g.setClip(x / 24 * size, (ybong - 30) / size * size, size, 100);

            g.drawImage(LoadImageInterface.bongChar, x,
                ybong - 2, MGraphics.VCENTER | MGraphics.HCENTER);
            g.setClip(GameScr.cmx, GameScr.cmy - GameCanvas.transY, GameScr.gW,
                GameScr.gH + 2 * GameCanvas.transY);
        }

        private void UpdateShadown()
        {
            var wCount = 0;
            if (TileMap.GetInstance().TileTypeAt(x, y, TileMap.T_TOP))
            {
                ybong = y;
            }
            else
            {
                ybong = y;
                while (wCount < 30)
                {
                    wCount++;
                    ybong += 24;
                }
            }
        }

        public void ResetToDie()
        {
            injureThenDie = true;
            hp = 0;
            status = MA_INHELL;
            x = xFirst;
            y = yFirst;
        }
    }
}