using src.lib;

namespace src.Objectgame
{
    public class InfoItem
    {
        public static int wcat = -1;
        public MFont f;
        public string s;
        public int speed;

        public InfoItem(string s)
        {
            f = FontManager.GetInstance().tahoma_7_white;
            this.s = s;
            speed = 20;
        }

        public InfoItem(string s, MFont f, int speed)
        {
            this.f = f;
            this.s = s;
            this.speed = speed;
        }
    }
}