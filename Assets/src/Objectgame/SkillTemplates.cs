using src.lib;
using src.Objectgame;

public class SkillTemplates
{
    public static MyHashtable hSkilltemplate = new MyHashtable();


    public static void add(SkillTemplate it)
    {
        hSkilltemplate.put("" + it.id, it);
    }

    public static ItemTemplate get(short id)
    {
        return (ItemTemplate) hSkilltemplate.get("" + id);
    }
}