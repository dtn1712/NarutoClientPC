using System;
using src.lib;
using src.main;
using src.model;
using src.Objectgame.quest;
using src.screen;
using UnityEngine;

namespace src.Objectgame
{
    public class Npc : Char, IActionListener
    {
        private const int START_QUEST = 1;
        private const int COMPLETE_QUEST = 2;
        private const int DOING_QUEST = 3;
    
        public static int idXaPhu = 40;
        public static MyHashtable arrNpcTemplate = new MyHashtable();
        public int npcId;

        public NpcTemplate template;
        public sbyte typeNV = -1;


        public Npc(short cx, short cy, short templateId)
        {
            this.cx = cx;
            this.cy = cy - 20;

            npcId = templateId;
            template = (NpcTemplate) arrNpcTemplate.get(templateId + "");
        }

        public void perform(int idAction, object p)
        {
            switch (idAction)
            {
                case START_QUEST: // co the nhan
                    MainQuestManager.getInstance().startQuest();
                    break;
                case COMPLETE_QUEST: // hoan thanh
                    MainQuestManager.getInstance().completeQuest();
                    break;
                case DOING_QUEST: // dang lam main
                    MainQuestManager.getInstance().showWorkingQuestInfo();
                    break;
                case 5:
                    NhiemVu(false);
                    break;
            }
        }

        public override void paint(MGraphics g)
        {
            if (!isPaint()) return;
            if (statusMe == A_HIDE)
                return;
            if (cTypePk != PK_NORMAL)
            {
                base.paint(g);
                return;
            }
            if (template == null)
                return;
            if (template.typeKhu != -1)
                g.drawImage(LoadImageInterface.bongChar, cx,
                    cy + 8, MGraphics.VCENTER | MGraphics.HCENTER);
            try
            {
                if (template.typeKhu == -1)
                {
                    g.drawImage(LoadImageInterface.khu, cx, cy - LoadImageInterface.khu.GetHeight(),
                        MGraphics.VCENTER | MGraphics.HCENTER);
                    FontManager.GetInstance().tahoma_7.DrawString(g, TileMap.zoneID + 1 + "", cx, cy - LoadImageInterface.khu.GetHeight() - 6,
                        2);
                    return;
                }
                if (template.npcTemplateId == idXaPhu)
                {
                    if (GameCanvas.gameTick % 2 == 0)
                        g.drawImage(LoadImageInterface.imgStoneMove[0], cx, cy - 10, MGraphics.VCENTER | MGraphics.HCENTER);
                    else
                        g.drawImage(LoadImageInterface.imgStoneMove[1], cx, cy - 10, MGraphics.VCENTER | MGraphics.HCENTER);
                    return;
                }
                Part ph = GameScr.parts[template.headId],
                    pl = GameScr.parts[template.legId],
                    pb = GameScr.parts[template.bodyId],
                    pw = null;
                if (template.npcTemplateId == 34)
                    pw = GameScr.parts[167];
                if (cdir == 1)
                {
                    SmallImage.DrawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id,
                        cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx,
                        cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
                    SmallImage.DrawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id,
                        cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx,
                        cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
                    SmallImage.DrawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                        cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx,
                        cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
                    if (pw != null)
                        SmallImage.DrawSmallImage(g, pw.pi[CharInfo[cf][2][0]].id,
                            cx + CharInfo[cf][3][1] + pw.pi[CharInfo[cf][3][0]].dx,
                            cy - CharInfo[cf][3][2] + pw.pi[CharInfo[cf][3][0]].dy, 0, 0);
                }
                else
                {
                    SmallImage.DrawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id,
                        cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx,
                        cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
                    SmallImage.DrawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id,
                        cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx,
                        cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
                    SmallImage.DrawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                        cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx,
                        cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
                    if (pw != null)
                        SmallImage.DrawSmallImage(g, pw.pi[CharInfo[cf][2][0]].id,
                            cx - CharInfo[cf][3][1] - pw.pi[CharInfo[cf][3][0]].dx,
                            cy - CharInfo[cf][3][2] + pw.pi[CharInfo[cf][3][0]].dy, 2, 24);
                }
                if (typeNV > -1)
                    g.drawRegion(GameScr.imgQuest, 0,
                        (typeNV * 2 + GameCanvas.gameTick / 10 % 2) * GameScr.imgQuest.GetHeight() / 4,
                        GameScr.imgQuest.GetWidth(), GameScr.imgQuest.GetHeight() / 4, 0, cx,
                        cy - ch - 3 - FontManager.GetInstance().tahoma_7.GetHeight() - 10, MGraphics.VCENTER | MGraphics.HCENTER, false);
                if (myChar().npcFocus != null && myChar().npcFocus.Equals(this))
                    FontManager.GetInstance().tahoma_7_yellow.DrawStringShadown(g, template.name, cx, cy - ch - FontManager.GetInstance().tahoma_7.GetHeight() - 7,
                        MFont.CENTER);
                else
                    FontManager.GetInstance().tahoma_7_yellow.DrawStringShadown(g, template.name, cx, cy - ch - 3 - FontManager.GetInstance().tahoma_7.GetHeight(),
                        MFont.CENTER);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }

        }

        public void NhiemVu(bool ischeckNPC)
        {
            //true ko hien popup chi de check paint icon NV tren dau NPC
            var menu = new Vector();
            var idnpc = npcId;
        
            if (MainQuestManager.getInstance().NewQuest != null && MainQuestManager.getInstance().NewQuest.IdNpcReceive == idnpc)
            {
                var cmd = new Command(MainQuestManager.getInstance().NewQuest.Name, this, START_QUEST, new MenuObject(npcId), true);
                menu.addElement(cmd);
            }
        
            if (MainQuestManager.getInstance().WorkingQuest != null && MainQuestManager.getInstance().WorkingQuest.IdNpcResolve == idnpc)
            {
                var cmd = new Command(MainQuestManager.getInstance().WorkingQuest.Name, this, DOING_QUEST, new MenuObject(npcId), true);
                menu.addElement(cmd);
            }
        
            if (MainQuestManager.getInstance().FinishQuest != null && MainQuestManager.getInstance().FinishQuest.IdNpcResolve == idnpc)
            {
                var cmd = new Command(MainQuestManager.getInstance().FinishQuest.Name, this, COMPLETE_QUEST, new MenuObject(npcId), true);
                menu.addElement(cmd);
            }
        
 
            GameCanvas.clearKeyHold();
            GameCanvas.clearKeyPressed();
            if (!ischeckNPC && menu.size() != 0)
                GameCanvas.menu.startAtNPC(menu, 0, idnpc, this, "");
        
        }

    }
}