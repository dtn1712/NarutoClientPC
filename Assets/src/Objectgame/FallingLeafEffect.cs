using src.lib;
using src.main;
using src.model;

namespace src.Objectgame
{
    public class FallingLeafEffect
    {
        private static Vector leaf = new Vector();

        private static FallingLeafEffect _instance;

        private int idFrame;
        private int tickk;
        private int type;

        private int vx, vy;
        private int X, Y;

        private FallingLeafEffect(int x, int y)
        {
            X = x;
            Y = y;
            vx = vy = 0;
        }

        public static void Load()
        {
            leaf.removeAllElements();
            var kc = TileMap.pxw / 50;
            for (var i = 0; i < 50; i++)
            {
                _instance = new FallingLeafEffect(-20 + Random(-5, 45) * kc * kc, -10 - Random(1, 30) * 50);
                leaf.addElement(_instance);
            }
        }

        private void Update()
        {
            tickk = (tickk + 1) % 100;
            if (tickk % 4 == 0)
            {
                idFrame++;
                if (idFrame > 3)
                    idFrame = 0;
            }
            X += Random(1, 3);
            Y += Random(1, 3);
            if (Y > ((GameCanvas.h + TileMap.pxh) / TileMap.GetInstance().size + 2) * 24 + 240
                || X > ((GameCanvas.w + TileMap.pxw) / TileMap.GetInstance().size + 1) * 24 + 240
                || X < -600)
            {
                X = Random(-5, TileMap.pxh / 50 - 5) * 50;
                Y = -Random(0, 20) * 50;
            }
        }

        private static int Random(int imin, int imax)
        {
            var delta = CRes.abs(imax - imin);
            return CRes.abs(CRes.random(delta)) + imin;
        }

        public static void UpdateFallingLeaf()
        {
            for (var i = 0; i < leaf.size(); i++)
            {
                var obj = (FallingLeafEffect) leaf.elementAt(i);
                obj.Update();
            }
        }

        public static void PaintFallingLeaf(MGraphics g)
        {
            for (var i = 0; i < leaf.size(); i++)
            {
                var obj = (FallingLeafEffect) leaf.elementAt(i);
                obj.Paint(g);
            }
        }

        private void Paint(MGraphics g)
        {
            g.drawRegion(LoadImageInterface.imgLa, 0, idFrame * 14, 20, 14, Sprite.TRANS_NONE, X,
                Y, MGraphics.VCENTER | MGraphics.BOTTOM);
        }
    }
}