using src.lib;
using src.main;
using src.Objectgame;

public class ItemThucAn
{
    // item paint ngoài màn hình, scale nho lại so với hình item trong hành trang
    public static int tileScale = 75; //50%

    public int day, hour, minute, second, value;

    public short id, idIcon, iditem;
    public bool isUsed, isNullIdicon;
    public string sday = "";
    public long time, timeRemove, timeOld; // mili giấy server trả về là s;
    public sbyte type;

    public ItemThucAn(short idd, sbyte typee, short iditem, short timeR, int valuee)
    {
        var dem = ItemTemplates.Get(iditem);
        this.iditem = iditem;
        if (dem != null)
            idIcon = dem.iconID;
        else isNullIdicon = true;
        type = typee;
        id = idd;
        value = valuee;
        time = MSystem.currentTimeMillis();
        timeRemove = timeR * 1000; // đổi sang miligiay
        timeOld = timeR;
        isUsed = false;
    }


    public void update()
    {
        if (isNullIdicon && GameCanvas.gameTick % 4 == 0)
        {
            var dem = ItemTemplates.Get(iditem);
            if (dem != null)
                idIcon = dem.iconID;
        }
        var currtime = MSystem.currentTimeMillis();
        if (currtime - time >= timeRemove)
        {
            GuiMain.vecItemOther.removeElement(this);
        }
        else
        {
            if ((timeRemove - (currtime - time)) / 1000 <= 60)
            {
                second = (int) (timeRemove - (currtime - time)) / 1000;
                day = minute = hour = 0;
            }
            else if ((timeRemove - (currtime - time)) / 1000 <= 3600)
            {
                second = (int) (timeRemove - (currtime - time)) / 1000;
                minute = second / 60;
                second = second % 60;
                second = hour = day = 0;
            }
            else if ((timeRemove - (currtime - time)) / 1000 <= 86400)
            {
                second = (int) (timeRemove - (currtime - time)) / 1000;
                hour = second / 3600;
                second = minute = day = 0;
            }
            else
            {
                second = (int) (timeRemove - (currtime - time)) / 1000;
                day = second / 86400;
                second = minute = hour = 0;
            }
            sday = (day <= 0 ? "" : (day < 10 ? "0" + day : day + "") + "d") +
                   (hour <= 0 ? "" : (hour < 10 ? "0" + hour : hour + "") + "h") +
                   (minute <= 0 ? "" : (minute < 10 ? "0" + minute : minute + "") + "'") +
                   (second <= 0 ? "" : (second < 10 ? "0" + second : second + "") + "s") + "";
        }
    }

    public void paint(MGraphics g, int x, int y)
    {	
        SmallImage.drawSmallImageScalse(g, idIcon, x + 6, y + 8, 0, MGraphics.VCENTER | MGraphics.HCENTER, true,
            tileScale);
        FontManager.GetInstance().tahoma_7.DrawString(g, sday, x + 6, y + 20, 2);
    }
}