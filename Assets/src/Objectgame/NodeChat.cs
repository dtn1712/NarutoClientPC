using System;
using src.lib;

namespace src.Objectgame
{
    public class NodeChat
    {
        public const sbyte t_TEXT = 0; // text
        public const sbyte t_EMO = 1; // icon
        public const sbyte t_TEXTEMO = 2; //vua test vua icon
        public const sbyte HSTRING = 12; //chieu cao font chu
        public static string mtextConvertEmo = "zpzp";
        public static int wnode;

        public static string[] maEmo =
        {
            //14,
            "z($)z", "z:Oz", "(highfive)", "z8-)", "z:Sz", "(wait)", "(oliver)", "z(nod)", "(facepalm)", "z:^)",
            "(muscle)", "z:)z", "z|-)", "(rofl)", "(lala)", "z(ske)", "z:$z", "z(n)z", "z>o)", "z;(z",
            "(bandit)", "z(y)z", "z(ci)", "z:?z", "(giggle)", "z(a)z", "(makeup)", "z>2)", "(swear)", "(wave)",
            "zz(v)z", "z(bow)", "z(*)z", "(emo)", "z:]z", "z:@z", "(doh)", "zz(6)", "z:&z", "(happy)",
            "z;)z", "|-(", "z(t)z", "(whew)", "(rock)", "z:|z", "(yawn)", ":P", "(hande)", "(tmi)z",
            "(fingers)", "z:Dz", "(smirk)", "(punch)", "z:(z", "z:*z", "(wasntme)", "8-|z", "z::|", "z:xz",
            "(waiting)", "(clap)", "z(mm)", "(headbang)"
        };

        public int hnode, wname;
        public bool isMe;
        public Vector listEmoLine = new Vector();
        public string[] text;
        public string textdai, name;

        public int type;

        /*khong cho vao khung
     * cat chuoi vua add icon hinh va doan chat
     * isMee: true: la minh, false: thang khac
     */
        public NodeChat(string textt, bool isMee, string namee)
        {
            //isMee = false;
            textdai = textt;
            isMe = isMee;
            if (isMe)
                namee = Char.myChar().cName;
            text = FontManager.GetInstance().tahoma_7_yellow.SplitFontArray((isMee == false ? namee + ": " : namee + ": ") + textt, wnode);
            type = t_TEXT;
            var typetext = new int[text.Length];

            //update icon
            if (text.Length >= 1)
                for (var m = 0; m < text.Length; m++)
                {
                    var listEmo = new Vector();
                    typetext[m] = t_TEXT;
                    for (var i = 0; i < maEmo.Length; i++)
                        if (text[0].IndexOf(maEmo[i]) >= 0)
                        {
                            var nEmo = findAllEmoInText(text[m], maEmo[i], i);
                            for (var j = 0; j < nEmo.size(); j++)
                            {
                                var emo = (EmoText) nEmo.elementAt(j);
                                listEmo.addElement(emo);
                            }
                            typetext[m] = t_EMO;
                            if (type == t_TEXT)
                                type = t_EMO;
                        }
                    if (listEmo.size() > 1)
                        listEmo = XepThuTuEmo(listEmo);
                    if (typetext[m] == t_EMO)
                    {
                        var lengEmo = 0;
                        for (var i = 0; i < listEmo.size(); i++)
                        {
                            var emo = (EmoText) listEmo.elementAt(i);
                            lengEmo += maEmo[emo.loai].Length;
                        }
                        if (lengEmo < text[m].Length)
                        {
                            if (type == t_EMO)
                                type = t_TEXTEMO;
                            typetext[m] = t_TEXTEMO;
                        }
                        else if (lengEmo < text[m].Length)
                        {
                            if (type == t_TEXT)
                                type = t_EMO;
                            typetext[m] = t_EMO;
                        }
                    }
                    if (typetext[m] == t_TEXTEMO)
                    {
                        var listext = slitFontWithEmo(text[m], listEmo);
                        var emoline = new EmoLineText(t_TEXTEMO, listext, listEmo);
                        listEmoLine.addElement(emoline);
                    }
                    else if (typetext[m] == t_TEXT)
                    {
                        var textline = new string[1];
                        textline[0] = text[m];
                        var emoline = new EmoLineText(t_TEXT, textline, listEmo);
                        listEmoLine.addElement(emoline);
                    }
                    else if (typetext[m] == t_EMO)
                    {
                        var textline = new string[1];
                        textline[0] = "";
                        var emoline = new EmoLineText(t_EMO, textline, listEmo);
                        listEmoLine.addElement(emoline);
                    }
                }
            var isType = new bool[3];
            for (var i = 0; i < typetext.Length; i++)
            {
                if (typetext[i] == t_TEXT)
                    isType[t_TEXT] = true;
                if (typetext[i] == t_EMO)
                    isType[t_EMO] = true;
                if (typetext[i] == t_TEXTEMO)
                    isType[t_TEXTEMO] = true;
            }
            if (isType[t_TEXTEMO])
                type = t_TEXTEMO;
            else if (isType[t_TEXT] && !isType[t_TEXTEMO] && !isType[t_EMO])
                type = t_TEXT;
            else if (!isType[t_TEXT] && !isType[t_TEXTEMO] && isType[t_EMO])
                type = t_EMO;
            else if (isType[t_TEXT] && !isType[t_TEXTEMO] && isType[t_EMO])
                type = t_TEXTEMO;
            if (text != null)
                hnode = HSTRING * text.Length + HSTRING / 4;
            name = namee;
            wname = FontManager.GetInstance().tahoma_7_yellow.GetWidth(name);
        }

        /*cho vao khung
     * cat theo w,h de xuong dong
     * wnodePainInfoGameScr: do rong cua khung
     */
        public NodeChat(string textt, bool isMee, string namee, int wnodePainInfoGameScr)
        {
            textdai = textt;
            isMe = isMee;
            text = FontManager.GetInstance().tahoma_7_yellow.SplitFontArray((isMee == false ? namee + ": " : "") + textt,
                wnodePainInfoGameScr);
            type = t_TEXT;
            var typetext = new int[text.Length];

            //update icon
            if (text.Length >= 1)
                for (var m = 0; m < text.Length; m++)
                {
                    var listEmo = new Vector();
                    typetext[m] = t_TEXT;
                    for (var i = 0; i < maEmo.Length; i++)
                        if (text[m].IndexOf(maEmo[i]) >= 0)
                        {
                            var nEmo = findAllEmoInText(text[m], maEmo[i], i);
                            for (var j = 0; j < nEmo.size(); j++)
                            {
                                var emo = (EmoText) nEmo.elementAt(j);
                                listEmo.addElement(emo);
                            }
                            typetext[m] = t_EMO;
                            if (type == t_TEXT)
                                type = t_EMO;
                        }
                    if (listEmo.size() > 1)
                        listEmo = XepThuTuEmo(listEmo);
                    if (typetext[m] == t_EMO)
                    {
                        var lengEmo = 0;
                        for (var i = 0; i < listEmo.size(); i++)
                        {
                            var emo = (EmoText) listEmo.elementAt(i);
                            lengEmo += maEmo[emo.loai].Length;
                        }
                        if (lengEmo < text[m].Length)
                        {
                            if (type == t_EMO)
                                type = t_TEXTEMO;
                            typetext[m] = t_TEXTEMO;
                        }
                        else if (lengEmo < text[m].Length)
                        {
                            if (type == t_TEXT)
                                type = t_EMO;
                            typetext[m] = t_EMO;
                        }
                    }
                    if (typetext[m] == t_TEXTEMO)
                    {
                        var listext = slitFontWithEmo(text[m] + "   ", listEmo);
                        var emoline = new EmoLineText(t_TEXTEMO, listext, listEmo);
                        listEmoLine.addElement(emoline);
                    }
                    else if (typetext[m] == t_TEXT)
                    {
                        var textline = new string[1];
                        textline[0] = text[m];
                        var emoline = new EmoLineText(t_TEXT, textline, listEmo);
                        listEmoLine.addElement(emoline);
                    }
                    else if (typetext[m] == t_EMO)
                    {
                        var textline = new string[1];
                        textline[0] = "";
                        var emoline = new EmoLineText(t_EMO, textline, listEmo);
                        listEmoLine.addElement(emoline);
                    }
                }
            var isType = new bool[3];
            for (var i = 0; i < typetext.Length; i++)
            {
                if (typetext[i] == t_TEXT)
                    isType[t_TEXT] = true;
                if (typetext[i] == t_EMO)
                    isType[t_EMO] = true;
                if (typetext[i] == t_TEXTEMO)
                    isType[t_TEXTEMO] = true;
            }
            if (isType[t_TEXTEMO])
                type = t_TEXTEMO;
            else if (isType[t_TEXT] && !isType[t_TEXTEMO] && !isType[t_EMO])
                type = t_TEXT;
            else if (!isType[t_TEXT] && !isType[t_TEXTEMO] && isType[t_EMO])
                type = t_EMO;
            else if (isType[t_TEXT] && !isType[t_TEXTEMO] && isType[t_EMO])
                type = t_TEXTEMO;
            if (text != null)
                hnode = HSTRING * text.Length + HSTRING / 4;
            name = namee;
            wname = FontManager.GetInstance().tahoma_7_yellow.GetWidth(name);
        }

        public void paint(MGraphics g, int x, int y, bool isMe, bool isWorld)
        {
            switch (type)
            {
                case t_TEXT:
                    for (var j = 0; j < text.Length; j++)
                        if (!isMe)
                            FontManager.GetInstance().tahoma_7.DrawString(g, text[j], x, y + j * HSTRING + 1, 0);
                        else if (isWorld)
                            FontManager.GetInstance().tahoma_7_white.DrawString(g, text[j], x, y + j * HSTRING + 1, 0);
                        else FontManager.GetInstance().tahoma_7_blue.DrawString(g, text[j], x, y + j * HSTRING + 1, 0);
                    break;
                case t_TEXTEMO:
                    for (var i = 0; i < listEmoLine.size(); i++)
                    {
                        var emoline = (EmoLineText) listEmoLine.elementAt(i);
                        switch (emoline.loai)
                        {
                            case t_TEXTEMO:
                                var indexemo = 0;
                                for (var j = 0; j < emoline.listtext.Length; j++)
                                    if (emoline.listtext[j].Equals(mtextConvertEmo))
                                    {
                                        var emo = (EmoText) emoline.listEmo.elementAt(indexemo);
                                        try
                                        {
                                            indexemo++;
                                            g.drawImage(LoadImageInterface.imgEmo[emo.loai],
                                                emoline.wlist[j] + x,
                                                y + i * HSTRING + 1, 0);
                                        }
                                        catch (Exception e)
                                        {
                                            // TODO: handle exception
                                            //	e.printStackTrace();
                                        }
                                    }
                                    else
                                    {
                                        if (emoline.listtext[j] != null && !isMe)
                                            FontManager.GetInstance().tahoma_7.DrawString(g, emoline.listtext[j], emoline.wlist[j] + x,
                                                y + i * HSTRING + 1, 0);
                                        else if (emoline.listtext[j] != null && isWorld)
                                            FontManager.GetInstance().tahoma_7_white.DrawString(g, emoline.listtext[j], emoline.wlist[j] + x,
                                                y + i * HSTRING + 1, 0);
                                        else if (emoline.listtext[j] != null && isMe)
                                            FontManager.GetInstance().tahoma_7_blue.DrawString(g, emoline.listtext[j], emoline.wlist[j] + x,
                                                y + i * HSTRING + 1, 0);
                                    }
                                break;
                            case t_TEXT:
                                if (emoline.listtext[0] != null && !isMe)
                                    FontManager.GetInstance().tahoma_7.DrawString(g, emoline.listtext[0], x, y + i * HSTRING + 1, 0);
                                else if (emoline.listtext[0] != null && isWorld)
                                    FontManager.GetInstance().tahoma_7_white.DrawString(g, emoline.listtext[0], x, y + i * HSTRING + 1, 0);
                                else if (emoline.listtext[0] != null && isMe)
                                    FontManager.GetInstance().tahoma_7_blue.DrawString(g, emoline.listtext[0], x, y + i * HSTRING + 1, 0);
                                break;
                            case t_EMO:
                                for (var j = 0; j < emoline.listEmo.size(); j++)
                                {
                                    var emo = (EmoText) emoline.listEmo.elementAt(j);
                                    g.drawImage(LoadImageInterface.imgEmo[emo.loai], x +
                                                                                     j * MGraphics.getImageWidth(
                                                                                         LoadImageInterface.imgEmo[0]),
                                        y + i * HSTRING + 1, 0);
                                }
                                break;
                        }
                    }
                    break;
                case t_EMO:
                    for (var i = 0; i < listEmoLine.size(); i++)
                    {
                        var emoline = (EmoLineText) listEmoLine.elementAt(i);
                        for (var j = 0; j < emoline.listEmo.size(); j++)
                        {
                            var emo = (EmoText) emoline.listEmo.elementAt(j);
                            g.drawImage(LoadImageInterface.imgEmo[emo.loai],
                                x + wnode - emoline.listEmo.size() * MGraphics.getImageWidth(LoadImageInterface.imgEmo[0]) -
                                HSTRING / 2 + j * MGraphics.getImageWidth(LoadImageInterface.imgEmo[0]),
                                y + i * HSTRING + 1, 0);
                        }
                    }
                    break;
            }
        }

        /*
     *  kiem tra ma icon
     */
        public string[] slitFontWithEmo(string text, Vector nEmo)
        {
            var listtext = new Vector();
            for (var i = 0; i < nEmo.size(); i++)
            {
                var emo = (EmoText) nEmo.elementAt(i);

                text = Utils.Replace(text, maEmo[emo.loai], mtextConvertEmo + " ");
            }
            var mangcat = FontManager.GetInstance().Split(text, mtextConvertEmo);

            if (mangcat == null)
                return null;
            var nemo = nEmo.size();
            for (var i = 0; i < mangcat.Length; i++)
                if (!(i == 0 && mangcat[i].Length == 0))
                {
                    listtext.addElement(mangcat[i]);
                    if (nemo > 0)
                    {
                        nemo--;
                        listtext.addElement(mtextConvertEmo);
                    }
                }
                else
                {
                    if (nemo > 0)
                    {
                        nemo--;
                        listtext.addElement(mtextConvertEmo);
                    }
                }
            var chuoitextt = new string[listtext.size()];
            for (var i = 0; i < listtext.size(); i++)
                chuoitextt[i] = (string) listtext.elementAt(i);
            return chuoitextt;
        }

        /*
     * su dung de tim ma icon
     */
        public Vector findAllEmoInText(string text, string maemo, int loaiemo)
        {
            var index = 0;
            var nemo = new Vector();
            for (var i = 0; i < text.Length; i++)
                if (text[i] == maemo[0] && i + maemo.Length <= text.Length)
                {
                    var isMa = true;
                    try
                    {
                        for (var j = 0; j < maemo.Length; j++)
                            if (maemo[j] != text[i + j])
                                isMa = false;
                    }
                    catch (Exception e)
                    {
                        //e.printStackTrace();
                    }

                    if (isMa)
                    {
                        index = maemo.Length;
                        nemo.addElement(new EmoText(loaiemo, i));
                        i += index - 1;
                    }
                }
            return nemo;
        }

        /*
     * sort emo
     */
        public Vector XepThuTuEmo(Vector listEmo)
        {
            int vitri = 0, loai = 0;
            for (var i = 0; i < listEmo.size() - 1; i++)
            for (var j = i + 1; j < listEmo.size(); j++)
            {
                var emoI = (EmoText) listEmo.elementAt(i);
                var emoJ = (EmoText) listEmo.elementAt(j);
                if (emoI.vitri > emoJ.vitri)
                {
                    vitri = emoI.vitri;
                    loai = emoI.loai;
                    //
                    emoI.loai = emoJ.loai;
                    emoI.vitri = emoJ.vitri;

                    emoJ.loai = loai;
                    emoJ.vitri = vitri;
                }
            }
            return listEmo;
        }
    }
}