using src.lib;
using src.model;

namespace src.Objectgame
{
    public class MobTemplate
    {
        public EffectData data;
        public sbyte[][] frameBossAttack;
        public sbyte[] frameBossMove;
        public int hp;
        public MBitmap[] imgs = new MBitmap[8];
        public short mobTemplateId, idloadimage;
        public sbyte rangeMove, speed, type;

    }
}