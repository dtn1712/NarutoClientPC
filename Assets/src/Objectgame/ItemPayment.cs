using System;
using src.lib;

[Serializable]
public class ItemPayment
{
    public const string KEY_ID = "";
    public const string KEY_VALUE_MONEY_AMOUNT = "gameMoneyAmount";
    public const string KEY_NAME_MONEY_AMOUNT = "gameMoneyUnit";
    public const string KEY_VALUE_MONEY_REAL = "realMoneyAmount";
    public const string KEY_NAME_MONEY_REAL = "realMoneyUnit";

    public int gameMoneyAmount;
    public string gameMoneyUnit;
    private string iditem;
    private string money;
    private string name_money;
    public int realMoneyAmount;
    public string realMoneyUnit;
    private string value_luong;
    private string value_xu;

    public ItemPayment()
    {
    }


    public ItemPayment(int gamemoney, string gamemoneyUtil, int realmoney, string realmoneyUtil)
    {
        //this.iditem = id+"";
        setGameMoneyAmount(gamemoney);
        setGameMoneyUnit(gamemoneyUtil);
        setRealMoneyAmount(realmoney);
        setRealMoneyUnit(realmoneyUtil);
    }

    public int getGameMoneyAmount()
    {
        return gameMoneyAmount;
    }

    private void setGameMoneyAmount(int gameMoneyAmount)
    {
        this.gameMoneyAmount = gameMoneyAmount;
    }

    public string getGameMoneyUnit()
    {
        return gameMoneyUnit;
    }

    private void setGameMoneyUnit(string gameMoneyUnit)
    {
        this.gameMoneyUnit = gameMoneyUnit;
    }

    public int getRealMoneyAmount()
    {
        return realMoneyAmount;
    }

    private void setRealMoneyAmount(int realMoneyAmount)
    {
        this.realMoneyAmount = realMoneyAmount;
    }

    public string getRealMoneyUnit()
    {
        return realMoneyUnit;
    }

    private void setRealMoneyUnit(string realMoneyUnit)
    {
        this.realMoneyUnit = realMoneyUnit;
    }
}