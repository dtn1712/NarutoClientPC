using System;
using src.lib;
using src.main;
using src.model;
using src.real;
using UnityEngine;
using src.screen;

namespace src.Objectgame
{
    public class Char
    {
        public const sbyte A_STAND = 1,
            A_RUN = 2,
            A_JUMP = 3,
            A_FALL = 4,
            A_DEADFLY = 5,
            A_NOTHING = 6,
            A_ATTK = 7,
            A_INJURE = 8,
            A_AUTOJUMP = 9,
            A_WATERRUN = 10,
            A_WATERDOWN = 11,
            SKILL_STAND = 12,
            SKILL_FALL = 13,
            A_DEAD = 14,
            A_HIDE = 15;

        public int mapId;
        public const sbyte PK_NORMAL = 0;
        public const sbyte PK_NHOM = 1;
        public const sbyte PK_BANG = 2;
        public const sbyte PK_DOSAT = 3;
        public const sbyte PK_PHE1 = 4;
        public const sbyte PK_PHE2 = 5;
        public static Clan clan;
        public static bool isAPickYen;
        public static bool isAPickYHM;
        public static bool isAPickYHMS;

        public static int aHpValue = 20;
        public static int aMpValue = 20;

        public static Char toCharChat;
        public static OtherChar toCharChatSelected;


        public static int[][][] CharInfo =
        {
            // Head, Leg, Body, Weapon
            new[]
            {
                new[] {0, -10, 32}, new[] {1, -7, 7}, new[] {1, -11, 15}, new[] {1, -9, 45}
            }, // Stand0
            // -
            // [0]
            new[]
            {
                new[] {0, -10, 33}, new[] {1, -7, 7}, new[] {1, -11, 16}, new[] {1, -9, 46}
            }, // Stand1
            // -
            // [1]
            // ==
            new[]
            {
                new[] {1, -10, 33}, new[] {2, -10, 11}, new[] {2, -9, 16}, new[] {1, -12, 49}
            }, // Run0
            // -
            // [2]
            new[]
            {
                new[] {1, -10, 32}, new[] {3, -11, 9}, new[] {3, -11, 16}, new[] {1, -13, 47}
            }, // Run1
            // -
            // [3]
            new[]
            {
                new[] {1, -10, 34}, new[] {4, -9, 9}, new[] {4, -8, 16}, new[] {1, -12, 47}
            }, // Run2
            // -
            // [4]
            new[]
            {
                new[] {1, -10, 34}, new[] {5, -11, 11}, new[] {5, -10, 17}, new[] {1, -13, 49}
            }, // Run3
            // -
            // [5]
            new[]
            {
                new[] {1, -10, 33}, new[] {6, -9, 9}, new[] {6, -8, 16}, new[] {1, -12, 47}
            }, // Run4

//			{ { 1, -10, 32 }, { 6, -9, 9 }, { 6, -8, 16 }, { 1, -12, 47 } }, // Run them 1 frame
            // -
            // [6]
            // ==
            new[]
            {
                new[] {0, -9, 36}, new[] {7, -5, 15}, new[] {7, -10, 21}, new[] {1, -8, 49}
            }, // Jump0
            // -
            // [7]
            new[]
            {
                new[] {4, -13, 26}, new[] {0, 0, 0}, new[] {0, 0, 0}, new[] {0, 0, 0}
            }, // JumpRotate0
            // - [8]
            new[]
            {
                new[] {5, -13, 25}, new[] {0, 0, 0}, new[] {0, 0, 0}, new[] {0, 0, 0}
            }, // JumpRotate1
            // - [9]
            new[]
            {
                new[] {6, -12, 26}, new[] {0, 0, 0}, new[] {0, 0, 0}, new[] {0, 0, 0}
            }, // JumpRotate2
            // -
            // [10]
            new[]
            {
                new[] {7, -13, 25}, new[] {0, 0, 0}, new[] {0, 0, 0}, new[] {0, 0, 0}
            }, // JumpRotate3
            // -
            // [11]
            new[]
            {
                new[] {0, -9, 35}, new[] {8, -4, 13}, new[] {8, -14, 27}, new[] {1, -9, 49}
            }, // Fall0
            // -
            // [12]
            // ==
            new[]
            {
                new[] {0, -9, 31}, new[] {9, -11, 8}, new[] {10, -10, 17}, new[] {0, 0, 0}
            }, // Attak0
            // -
            // [13]
            new[]
            {
                new[] {2, -7, 33}, new[] {9, -11, 8}, new[] {11, -8, 15}, new[] {0, 0, 0}
            }, // Attak1
            // -
            // [14]
            new[]
            {
                new[] {2, -8, 32}, new[] {9, -11, 8}, new[] {12, -8, 14}, new[] {0, 0, 0}
            }, // Attak2
            // -
            // [15]
            new[]
            {
                new[] {2, -7, 32}, new[] {9, -11, 8}, new[] {13, -12, 15}, new[] {0, 0, 0}
            }, // Attak3
            // -
            // [16]
            new[]
            {
                new[] {0, -11, 31}, new[] {9, -11, 8}, new[] {14, -15, 18}, new[] {0, 0, 0}
            }, // Attak4
            // -
            // [17]
            new[]
            {
                new[] {2, -9, 32}, new[] {9, -11, 8}, new[] {15, -13, 19}, new[] {0, 0, 0}
            }, // Attak5
            // -
            // [18]
            new[]
            {
                new[] {2, -9, 31}, new[] {9, -11, 8}, new[] {16, -7, 22}, new[] {0, 0, 0}
            }, // Attak6
            // -
            // [19]
            new[]
            {
                new[] {2, -9, 32}, new[] {9, -11, 8}, new[] {17, -11, 18}, new[] {0, 0, 0}
            }, // Attak7
            // -
            // [20]
            // ==
            new[]
            {
                new[] {3, -12, 34}, new[] {8, -4, 13}, new[] {8, -15, 25}, new[] {1, -10, 46}
            }, // Injure
            // -
            // [21]
            // ==
            new[]
            {
                new[] {0, -9, 32}, new[] {8, -4, 9}, new[] {10, -10, 18}, new[] {0, 0, 0}
            }, // FallAttak0
            // -
            // 22
            new[]
            {
                new[] {2, -7, 34}, new[] {8, -4, 9}, new[] {11, -8, 16}, new[] {0, 0, 0}
            }, // FallAttak1
            // -
            // 23
            new[]
            {
                new[] {2, -8, 33}, new[] {8, -4, 9}, new[] {12, -8, 15}, new[] {0, 0, 0}
            }, // FallAttak2
            // -
            // 24
            new[]
            {
                new[] {2, -7, 33}, new[] {8, -4, 9}, new[] {13, -12, 16}, new[] {0, 0, 0}
            }, // FallAttak3
            // -
            // 25
            new[]
            {
                new[] {0, -11, 32}, new[] {7, -5, 9}, new[] {14, -15, 19}, new[] {0, 0, 0}
            }, // FallAttak4
            // -
            // 26
            new[]
            {
                new[] {2, -9, 33}, new[] {7, -5, 9}, new[] {15, -13, 20}, new[] {0, 0, 0}
            }, // FallAttak5
            // -
            // 27
            new[]
            {
                new[] {2, -9, 32}, new[] {7, -5, 9}, new[] {16, -7, 23}, new[] {0, 0, 0}
            }, // FallAttak6
            // -
            // 28
            new[]
            {
                new[] {2, -9, 33}, new[] {7, -5, 9}, new[] {17, -11, 19}, new[] {0, 0, 0}
            } // FallAttak7
        };

        public static Char myCharr;
        public static bool ischangingMap, isLockKey;
        public static long timedelayloadmap;

        public static bool isManualFocus;
        public Item[] arrItemBag;
        public Item[] arrItemBody;
        public bool canJumpHigh = true, cmtoChar, me, isStartSoundRun, isAttack, isAttFly;

        public int cBonusSpeed,
            cspeed,
            cdame,
            clevel,
            cClass,
            cCountry,
            cMP,
            cMaxMP = 100,
            cHP,
            cMaxHP = 100,
            cMaxEXP = 100,
            cyStartFall;

        public int cCountryHoa = 0,
            cCountryThuy = 1,
            cCountryLoi = 2,
            cCountryPhong = 3,
            cCountryTho = 4,
            cIdClanLocal,
            cIdClanGlobal;

        public long cEXP = 0;

        public int cf, tick; // Weapon
        public Char charFocus;
        public long CharidDB;

        public ChatPopup chatPopup;

        public long cIdDB;
        public string cName, cClanName = "";
        public byte cTypePk;

        public MovePoint currentMovePoint;
        public int cvyJump;
        public int cw = 22, ch = 50, chw = 11, chh = 16;
        public int cx = 24, cy = 24, cybong;

        public int cxSend, cySend, cxMoveLast, cyMoveLast;

        private int cxtemp;
        public int[] diemTN, subTn;
        private int dxmove1, dxmove2;
        public EffectCharPaint eff, effTask;
        public EffectCharPaint eff0, eff1, eff2;
        public EffectPaint[] effPaints;


        public short head, leg, body;
        public long idFriend = -1;

        public short idParty = -1;
        public int indexEff = -1, indexEffTask = -1;
        public int indexSkill, i0, i1, i2, dx0, dx1, dx2, dy0, dy1, dy2;
        private int indexUseSkill = -1;

        public byte isInjure;
        public bool isInvisible;

        public bool isLeader = false;
        public bool isLockMove, isBlinking;
        public bool isOnline;
        public ItemMap itemFocus;
        public Item[] ItemMyTrade = new Item[8];
        public Item[] ItemParnerTrade = new Item[8];
        public int killCharId = -9999;
        public long lastUpdateTime;
        public int cvx, cvy, cp1, cp2, cp3, statusMe = 5, cdir = 1, cgender;
        public long charID;

        public Mob mobFocus;
        private short[] moveFast;
        public QuickSlot[] mQuickslot;
        private Skill myskill;

        private int nInjure;
        public Npc npcFocus;
        public bool paintName = true;
        public Char partnerTrade;

        public SkillPaint skillPaint;

        private int sleepaddEffBui;
        private int sType;

        private int testCharId = -9999;

        private int tickviBody = 0;
        private long timeRequesChangeMap; // time cấm chat
        public long timedelay = 5000;
        public long timeLastchangeMap;
        public int totalTN = 0;
        public sbyte typePk = -1;

        public Vector vFriend = new Vector();

        public Vector vMovePoints = new Vector(); // luu lai nhung vi tri nguoi choi ko phai la minh phai di qua.

        public short wdx, wdy;

        private int xSd;

        public long xu, luong;

        public bool isHostRoom = false; 

        public Char()
        {
            statusMe = A_STAND;
        }

        public int getdxSkill()
        {
            if (myChar().mQuickslot[0] != null)
            {
                var ql = myChar().mQuickslot[0];
                if (ql.idSkill != -1)
                {
                    var skill = (SkillTemplate) SkillTemplates.hSkilltemplate.get("" + ql.idSkill);
                    if (skill != null && skill.level >= 0 && skill.level < skill.rangelv.Length)
                        return skill.rangelv[skill.level];
                    if (skill != null)
                        return skill.rangelv[0];
                }
            }
            else if (myskill != null)
            {
                return myskill.dx;
            }
            return 0;
        }

        private int GetdySkill()
        {
            return myskill?.dy ?? 0;
        }

        public int getSpeed()
        {
            return cspeed < 6 ? 6 : 5;
        }

        public static Char myChar()
        {
            if (myCharr != null) return myCharr;
            myCharr = new Char
            {
                me = true,
                cmtoChar = true,
                mQuickslot = new QuickSlot[5]
            };
            for (var i = 0; i < 5; i++)
                myCharr.mQuickslot[i] = new QuickSlot();
        
            return myCharr;
        }


        private bool IsInWaypoint()
        {
            var size = TileMap.GetInstance().vGo.size();
            for (byte i = 0; i < size; i++)
            {
                var wp = (Waypoint) TileMap.GetInstance().vGo.elementAt(i);
                if (cx < wp.minX || cx > wp.maxX || cy < wp.minY || cy > wp.maxY)
                    continue;
                return true;
            }
            return false;
        }

        public void update()
        {
            if (ischangingMap)
            {
                return;
            }
                
            if (statusMe == A_DEAD)
            {
                if ((TileMap.GetInstance().TileTypeAtPixel(cx, cy) & TileMap.T_TOP) != TileMap.T_TOP)
                {
                    cvy++;
                    cy += cvy;
                    Service.GetInstance().CharMove();
                    if ((TileMap.GetInstance().TileTypeAtPixel(cx, cy) & TileMap.T_TOP) == TileMap.T_TOP)
                    {
                        cvy = 0;
                        cy = TileMap.GetInstance().TileXofPixel(cy + 3);
                    }
                    if (cmtoChar)
                    {
                        GameScr.cmtoX = cx - GameScr.gW2;
                        GameScr.cmtoY = cy - GameScr.gH23;

                        if (!GameCanvas.isTouchControl)
                            GameScr.cmtoX += GameScr.gW6 * cdir;
                    }
                }
                return;
            }
            UpdateShadow();


            if (cmtoChar)
            {
                GameScr.cmtoX = cx - GameScr.gW2;
                GameScr.cmtoY = cy - GameScr.gH23;
                if (!GameCanvas.isTouchControl)
                    GameScr.cmtoX += GameScr.gW6 * cdir;
            }
            UpdateSkillPaint();
            tick = (tick + 1) % 100;
            
            if (me && statusMe != A_RUN && isStartSoundRun && GameCanvas.gameTick % 11 == 0)
            {
                isStartSoundRun = false;
                Music.stopSound(Music.RUN);
            }
            if (charFocus != null && !GameScr.vCharInMap.contains(charFocus))
            {
                charFocus = null;
            }

            if (cx < 40)
            {
                cvx = 0;
                cx = 40;
            }
            else if (cx > TileMap.pxw - 10)
            {
                cx = TileMap.pxw - 10;
                cvx = 0;
            }

            if (me)
            {
                if (!ischangingMap && IsInWaypoint())
                {
                    timeRequesChangeMap--;
                    if (timeRequesChangeMap <= 0)
                    {
                        timeRequesChangeMap = 20;
                        Service.GetInstance().CharMove();
                    }
                    if (MSystem.currentTimeMillis() - (timeLastchangeMap + timedelay) > 0)
                    {
                        timeLastchangeMap = MSystem.currentTimeMillis();
                        Service.GetInstance().RequestChangeMap();
                    }
                    timedelayloadmap = MSystem.currentTimeMillis();
                }
                else if (statusMe != A_FALL)
                {
                    if (CRes.abs(cx - cxSend) >= 40 || CRes.abs(cy - cySend) >= 90)
                        if (cy - cySend <= 0)
                            Service.GetInstance().CharMove();
                }
            }

            SearchFocus();
            
            if (GameCanvas.gameTick % 20 == 0 && charID >= 0) // Auto hide name
            {
                paintName = true;
                for (var i = 0; i < GameScr.vCharInMap.size(); i++)
                {
                    Char c = null;
                    try
                    {
                        c = (Char) GameScr.vCharInMap.elementAt(i);
                    }
                    catch (Exception e)
                    {
                        Debug.LogError(e);
                    }
                    if (c == null || c.Equals(this))
                        continue;
                    if (c.cy == cy && CRes.abs(c.cx - cx) < 35 ||
                        cy - c.cy < 32 && cy - c.cy > 0 && CRes.abs(c.cx - cx) < 24)
                        paintName = false;
                }
            }


            if (isInjure > 0)
            {
                cf = 21;
                isInjure--;
            }
            else
            {
                switch (statusMe)
                {
                    case A_STAND:
                        UpdateCharStand();
                        break;
                    case A_RUN:
                        if (me)
                            if (GameCanvas.gameTick % 33 == 0)
                            {
                                Music.play(Music.RUN, 12);
                                isStartSoundRun = true;
                            }
                        UpdateCharRun();
                        break;
                    case A_JUMP:
                        UpdateCharJump();
                        break;
                    case A_FALL:
                        UpdateCharFall();
                        break;
                    case A_DEADFLY:
                        UpdateCharDeadFly();
                        break;
                    case A_AUTOJUMP:
                        UpdateCharAutoJump();
                        break;
                    case SKILL_STAND:
                        SetAttk();
                        break;
                    case A_WATERRUN:
                    case SKILL_FALL:
                    case A_DEAD:
                        break;
                    case A_INJURE:
                    case A_NOTHING:
                        if (cf == 21 && isInjure <= 0)
                            cf = 0;
                        break;
                }
            }
            if (wdx != 0 || wdy != 0)
            {
                StartDie(wdx, wdy);
                wdx = 0;
                wdy = 0;
            }
            if (moveFast != null)
                if (moveFast[0] == 0)
                {
                    moveFast[0]++;
                }
                else if (moveFast[0] < 10)
                {
                    moveFast[0]++;
                }
                else
                {
                    cx = moveFast[1];
                    cy = moveFast[2];
                    moveFast = null;
                    if (me)
                        if ((TileMap.GetInstance().TileTypeAtPixel(cx, cy) & TileMap.T_TOP) != TileMap.T_TOP)
                            statusMe = A_FALL;
                        else
                            Service.GetInstance().CharMove();
                }
            if (!me && vMovePoints.size() == 0 && cxMoveLast != 0 && cyMoveLast != 0 && currentMovePoint == null)
            {
                cx = cxMoveLast;
                cy = cyMoveLast;
                if (cHP > 0)
                    statusMe = A_NOTHING;
            }
            
            
//            if (!me && vMovePoints.size() == 0 && currentMovePoint == null)
//            {
//                if (cHP > 0)
//                    statusMe = A_NOTHING;
//            }


            if (me && statusMe != A_DEAD)
            {
                Service.GetInstance().CharMove();
            }
            
        }

        private void UpdateShadow()
        {
            var wCount = 0;
            xSd = cx;
            if (TileMap.GetInstance().TileTypeAt(cx, cy, TileMap.T_TOP))
            {
                cybong = cy;
            }
            else
            {
                cybong = cy;
                while (wCount < 30)
                {
                    wCount++;
                    cybong += 24;
                    if (TileMap.GetInstance().TileTypeAt(xSd, cybong, TileMap.T_TOP))
                    {
                        if (cybong % 24 != 0)
                        {
                            cybong -= cybong % 24;
                        }
                        break;
                    }
                }
            }
        }

        private void UpdateSkillPaint()
        {
            if (GameCanvas.gameTick % 2 == 0 && skillPaint != null)
                indexSkill++;

            if (skillPaint != null && indexSkill >= skillInfoPaint().Length)
            {
                if (!me)
                    if ((TileMap.GetInstance().TileTypeAtPixel(cx, cy) & TileMap.T_TOP) == TileMap.T_TOP)
                        ChangeStatusStand();
                    else
                        statusMe = A_NOTHING;
                indexSkill = -1;
                skillPaint = null;
                eff0 = eff1 = eff2 = null;
                i0 = i1 = i2 = 0;
            }
            // Gui message tan cong hoac buff khi trinh dien nua chung
        }

        private void UpdateCharDeadFly()
        {
            cp1++;
            cx += (cp2 - cx) / 4;
            if (cp1 > 7)
                cy += (cp3 - cy) / 4;
            else
                cy += cp1 - 10;
            if (CRes.abs(cp2 - cx) < 4 && CRes.abs(cp3 - cy) < 10)
            {
                cx = cp2;
                cy = cp3;
                statusMe = A_DEAD;
                CallEff(60);
                if (me)
                    GameScr.GetInstance().resetButton();
            }
            cf = 21;
        }

        private void SetAttk()
        {
            cp1++;
            if (cdir == 1)
            {
                if ((TileMap.GetInstance().TileTypeAtPixel(cx + chw, cy - chh) & TileMap.T_LEFT) == TileMap.T_LEFT)
                    cvx = 0;
            }
            else
            {
                if ((TileMap.GetInstance().TileTypeAtPixel(cx - chw, cy - chh) & TileMap.T_RIGHT) == TileMap.T_RIGHT)
                    cvx = 0;
            }

            if (cy > ch && TileMap.GetInstance().TileTypeAt(cx, cy - ch, TileMap.T_BOTTOM))
                if (!TileMap.GetInstance().TileTypeAt(cx, cy, TileMap.T_TOP))
                {
                    statusMe = A_FALL;
                    cp1 = 0;
                    cp2 = 0;
                    cvy = 1;
                }
                else
                {
                    cy = TileMap.GetInstance().TileYofPixel(cy);
                }

            cx += cvx;
            cy += cvy;
            if (cy < 0)
                cy = cvy = 0;
            if (cvy == 0)
            {
                // Stand + Attack
                if ((TileMap.GetInstance().TileTypeAtPixel(cx, cy) & TileMap.T_TOP) != TileMap.T_TOP)
                {
                    statusMe = A_FALL;

                    cvx = (getSpeed() >> 1) * cdir;
                    cp1 = cp2 = 0;
                }
            }
            else if (cvy < 0)
            {
                // Jump + Attack
                cvy++;
                if (cvy == 0)
                    cvy = 1;
            }
            else
            {
                // Fall + Attack
                if (cvy < 20 && cp1 % 5 == 0)
                    cvy++;
                if (cvy > 3)
                    cvy = 3;
                if ((TileMap.GetInstance().TileTypeAtPixel(cx, cy + 3) & TileMap.T_TOP) == TileMap.T_TOP &&
                    cy <= TileMap.GetInstance().TileXofPixel(cy + 3))
                {
                    cvx = cvy = 0;
                    cy = TileMap.GetInstance().TileXofPixel(cy + 3);
                }
                if (TileMap.GetInstance().TileTypeAt(cx, cy, TileMap.T_WATERFLOW) && cy % TileMap.GetInstance().size > 8)
                {
                    statusMe = A_WATERRUN;
                    cvx = cdir << 1;
                    cvy = cvy >> 2;
                    cy = TileMap.GetInstance().TileYofPixel(cy) + 12;
                    statusMe = A_WATERDOWN;
                    return;
                }
                if (TileMap.GetInstance().TileTypeAt(cx, cy, TileMap.T_UNDERWATER))
                {
                    statusMe = A_WATERDOWN;
                    return;
                }
            }
            if (cvx > 0)
                cvx--;
            else if (cvx < 0)
                cvx++;
        }


        private void UpdateCharAutoJump()
        {
            cx += cvx * cdir;
            cy += cvyJump;
            cvyJump++;
            cf = cp1 == 0 ? 7 : 23;
            if (canJumpHigh)
                if (cvyJump == -3)
                    cf = 8;
                else if (cvyJump == -2)
                    cf = 9;
                else if (cvyJump == -1)
                    cf = 10;
                else if (cvyJump == 0)
                    cf = 11;

            if (cvyJump != 0) return;
            statusMe = A_NOTHING;
            ((MovePoint) vMovePoints.firstElement()).status = A_FALL;
            cp1 = 0;
            cvy = 1;
        }

        private void UpdateCharStand()
        {
            isAttack = false;
            isAttFly = false;
            cvx = 0;
            cvy = 0;
            cp1++;
            if (cp1 > 30)
                cp1 = 0;
            cf = cp1 % 15 < 5 ? 0 : 1;

            if (!me || ischangingMap || !IsInWaypoint()) return;
            
            if (MSystem.currentTimeMillis() - (timeLastchangeMap + timedelay) <= 0) return;
            
            timeLastchangeMap = MSystem.currentTimeMillis();
            Service.GetInstance().RequestChangeMap();
        }

        public void UpdateCharRun()
        {
            if (me && !ischangingMap && IsInWaypoint() && (cx - cxSend != 0 || cy - cySend != 0))
            {
                Service.GetInstance().CharMove();
            }
            
            sleepaddEffBui--;
            if (sleepaddEffBui <= 0)
            {
                ServerEffect.addServerEffect(26, cx, cy + 8, -1, (sbyte) cdir);
                sleepaddEffBui = 10;
            }
            cp1++;

            if (cp1 >= 5)
            {
                cp1 = 0;
                cBonusSpeed = 0;
            }

            cf = (cp1 >> 1) + 2;

            if ((TileMap.GetInstance().TileTypeAtPixel(cx, cy - 1) & TileMap.T_WATERFLOW) == TileMap.T_WATERFLOW)
                cx += cvx >> 1;
            else
                cx += cvx;

            if (cdir == 1)
            {
                if (TileMap.GetInstance().TileTypeAt(cx + chw, cy - chh, TileMap.T_LEFT))
                {
                    if (me)
                    {
                        cvx = 0;
                        cx = TileMap.GetInstance().TileXofPixel(cx + chw) - chw;
                    }
                    else
                    {
                        Stop();
                    }
                }

            }
            else
            {
                if (TileMap.GetInstance().TileTypeAt(cx - chw - 1, cy - chh, TileMap.T_RIGHT))
                {
                    if (me)
                    {
                        cvx = 0;
                        cx = TileMap.GetInstance().TileXofPixel(cx - chw - 1) + TileMap.GetInstance().size + chw;
                    }
                    else
                    {
                        Stop();
                    }
                }
                    
            }

            //auto move main char toi
//          if (cvx >= GetSpeed() && CRes.abs(cvx) % 2 == 0)
//          {
//              Service.GetInstance().CharMove();
//          }
            
            
            if (cvx > 0)
            {
                cvx--;
            }
            else if (cvx < 0)
            {
                cvx++;
            }
            else
            {
                ChangeStatusStand();
                cBonusSpeed = 0;
            }

            if ((TileMap.GetInstance().TileTypeAtPixel(cx, cy) & TileMap.T_TOP) != TileMap.T_TOP)
            {
                cf = 7;
                statusMe = A_FALL;
                cvx = 3 * cdir;
                cp2 = 0;
            }

            if (me && !ischangingMap && IsInWaypoint())
            {
                if (MSystem.currentTimeMillis() - (timeLastchangeMap + timedelay) > 0)
                {
                    timeLastchangeMap = MSystem.currentTimeMillis();
                    Service.GetInstance().RequestChangeMap();
                }
            }
        }

        private void Stop()
        {
            statusMe = A_NOTHING;
            cvx = 0;
            cvy = 0;
            cp1 = cp2 = 0;
        }

        public void UpdateCharJump()
        {
            cx += cvx;
            cy += cvy;
            if (cy < 0)
            {
                cy = 0;
                cvy = -1;
            }
            cvy++;
            
            cf = cp1 == 0 ? 8 : 24;
            if (canJumpHigh)
                if (cvy == -3)
                    cf = 9;
                else if (cvy == -2)
                    cf = 10;
                else if (cvy == -1)
                    cf = 11;
                else if (cvy == 0)
                    cf = 12;
            if (cdir == 1)
            {
                if ((TileMap.GetInstance().TileTypeAtPixel(cx + chw, cy - 1) & TileMap.T_LEFT) == TileMap.T_LEFT &&
                    cx <= TileMap.GetInstance().TileXofPixel(cx + chw) + 12)
                {
                    cx = TileMap.GetInstance().TileXofPixel(cx + chw) - chw;
                    cvx = 0;
                }
            }
            else
            {
                if ((TileMap.GetInstance().TileTypeAtPixel(cx - chw, cy - 1) & TileMap.T_RIGHT) == TileMap.T_RIGHT &&
                    cx >= TileMap.GetInstance().TileXofPixel(cx - chw) + 12)
                {
                    cx = TileMap.GetInstance().TileXofPixel(cx + 24 - chw) + chw;
                    cvx = 0;
                }
            }
            if (cvy == 0)
                if (!isAttFly)
                    if (me)
                        SetCharFallFromJump();
                    else
                        Stop();
                else
                    SetCharFallFromJump();
            
            if (me && !ischangingMap && IsInWaypoint())
            {
                Service.GetInstance().CharMove();
                Service.GetInstance().RequestChangeMap();
                return;
            }
            if (cp3 < 0)
            {
                cp3++;
            }
                
            if (cy > ch && TileMap.GetInstance().TileTypeAt(cx, cy - ch, TileMap.T_BOTTOM))
            {
                statusMe = A_FALL;
                cp1 = 0;
                cp2 = 0;
                cvy = 1;
            }
            
            if (!me && currentMovePoint == null)
            {
                Stop();
            }
        }


        private void SetCharFallFromJump()
        {
            cyStartFall = cy;
            statusMe = A_FALL;
            cp1 = 0;
            cp2 = canJumpHigh ? 1 : 0;
            cvy = 1;
            UpdateCharFall();
            if (me && (cx - cxSend != 0 || cy - cySend != 0))
            {
                Service.GetInstance().CharMove();
            }
        }

        private void UpdateCharFall()
        {
            if (cy + 4 >= TileMap.pxh)
            {
                ChangeStatusStand();
                cvx = cvy = 0;
                return;
            }
            if (cy % 24 == 0 && (TileMap.GetInstance().TileTypeAtPixel(cx, cy) & TileMap.T_TOP) == TileMap.T_TOP)
            {
                if (me)
                {
//                    if (cy - cySend > 0)
//                        Service.getInstance().charMove();
//                    else if (cx - cxSend != 0 || cy - cySend < 0)
                        Service.GetInstance().CharMove();
                    
                    cvx = cvy = 0;
                    cp1 = cp2 = 0;
                    ChangeStatusStand();
                    return;
                }
                Stop();
                cf = 0;
            }
        
            cf = 13;
            cx += cvx;
            cy += cvy;

            if (me)
                Service.GetInstance().CharMove();
        
            if (cvy < 20)
                cvy++;

            if (cdir == 1)
            {
                if ((TileMap.GetInstance().TileTypeAtPixel(cx + chw, cy - 1) & TileMap.T_LEFT) == TileMap.T_LEFT &&
                    cx <= TileMap.GetInstance().TileXofPixel(cx + chw) + 12)
                {
                    cx = TileMap.GetInstance().TileXofPixel(cx + chw) - chw;
                    cvx = 0;
                }
            }
            else
            {
                if ((TileMap.GetInstance().TileTypeAtPixel(cx - chw, cy - 1) & TileMap.T_RIGHT) == TileMap.T_RIGHT &&
                    cx >= TileMap.GetInstance().TileXofPixel(cx - chw) + 12)
                {
                    cx = TileMap.GetInstance().TileXofPixel(cx + 24 - chw) + chw; //
                    cvx = 0;
                }
            }

            if (cvy > 4)
                if ((cyStartFall == 0 || cyStartFall <= TileMap.GetInstance().TileYofPixel(cy + 3))
                    && (TileMap.GetInstance().TileTypeAtPixel(cx, cy + 3) & TileMap.T_TOP) == TileMap.T_TOP)
                {
                    if (me)
                    {
                        cyStartFall = 0;
                        cvx = cvy = 0;
                        cp1 = cp2 = 0;
                        cy = TileMap.GetInstance().TileXofPixel(cy + 3);
                        Service.GetInstance().CharMove();
                        ChangeStatusStand();
                        // =====================
//                        if (cy - cySend > 0)
//                        {
//                            if (me)
//                                Service.getInstance().charMove();
//                        }
//                        else if (cx - cxSend != 0 || cy - cySend < 0)
//                        {
//                            if (me)
//                                Service.getInstance().charMove();
//                        }

                        //					Cout.println("-----------------stand----22222222-----");
                    }
                    else
                    {
                        Stop();
                        cy = TileMap.GetInstance().TileXofPixel(cy + 3);
                        cf = 0;
                    }
                    return;
                }
            if (cp2 == 1) // That means fall rotate
            {
                if (cvy == 3)
                    cf = 12;
                else if (cvy == 2)
                    cf = 9;
                else if (cvy == 1)
                    cf = 10;
                else if (cvy == 0)
                    cf = 11;
            }
            else
            {
                cf = 13;
            }

            if (cvy > 6)
            {
                if (TileMap.GetInstance().TileTypeAt(cx, cy, TileMap.T_WATERFLOW) && cy % TileMap.GetInstance().size > 8)
                {
                    cy = TileMap.GetInstance().TileYofPixel(cy) + 8;
      
                    cvx = cdir << 1;
                    cvy = cvy >> 2;
                    cy = TileMap.GetInstance().TileYofPixel(cy) + 12;
//                    if (cx - cxSend != 0 || cy - cySend != 0)
//                    {
//                        if (me)
//                        {
                            Service.GetInstance().CharMove();
//                        }
//                    }
                }
            }


            if (me) return;
        
            if ((TileMap.GetInstance().TileTypeAtPixel(cx, cy + 1) & TileMap.T_TOP) == TileMap.T_TOP)
                cf = 0;
            
        }

        public void setSkillPaint(SkillPaint skillPaint, int sType)
        {
            setAutoSkillPaint(skillPaint, sType);
        }

        private void setAutoSkillPaint(SkillPaint skillPaint, int sType)
        {
            try
            {
                this.skillPaint = skillPaint;
                this.sType = sType;
                indexSkill = -1;
                i0 = i1 = i2 = dx0 = dx1 = dx2 = dy0 = dy1 = dy2 = 0;
                eff0 = null;
                eff1 = null;
                eff2 = null;
            }
            catch (Exception e)
            {
            }
        }

        private SkillInfoPaint[] skillInfoPaint()
        {
            if (skillPaint == null)
                return null;
            return sType == 0 ? skillPaint.skillStand : skillPaint.skillfly;
        }

        protected bool isPaint()
        {
            if (cx < GameScr.cmx)
                return false;
            if (cx > GameScr.cmx + GameScr.gW)
                return false;
            if (cy < GameScr.cmy)
                return false;
        
            return cy <= GameScr.cmy + GameScr.gH + 30;
        }

        private void paintShadow(MGraphics g)
        {
            int size = TileMap.GetInstance().size;
            if (TileMap.GetInstance().TileTypeAt(cx + size / 2, cybong + 1, TileMap.T_LEFT))
                g.setClip(xSd / size * size, (cybong - 30) / size * size, size,
                    100);
            else if (TileMap.GetInstance().TileTypeAt((cx - size / 2) / size, (cybong + 1) / size) == 0)
                g.setClip(cx / size * size, (cybong - 30) / size * size, 100, 100);
            else if (TileMap.GetInstance().TileTypeAt((cx + size / 2) / size, (cybong + 1) / size) == 0)
                g.setClip(cx / size * size, (cybong - 30) / size * size, size,100);
            else if (TileMap.GetInstance().TileTypeAt(xSd - size / 2, cybong + 1, TileMap.T_RIGHT))
                g.setClip(cx / 24 * size, (cybong - 30) / size * size, size, 100);

            g.drawImage(LoadImageInterface.bongChar, cx,
                cybong + 15, MGraphics.VCENTER | MGraphics.HCENTER);
            g.setClip(GameScr.cmx, GameScr.cmy - GameCanvas.transY, GameScr.gW,
                GameScr.gH + 2 * GameCanvas.transY);
        }

        public virtual void paint(MGraphics g)
        {
            paintCharName_HP_MP_Overhead(g);

            if (typePk >= 0)

                g.drawRegion(LoadImageInterface.iconpk, 0, 12 * (typePk * 3 + GameCanvas.gameTick / 3 % 3), 12, 12, 0,
                    cx,cy - 75, MGraphics.VCENTER | MGraphics.HCENTER);
            if (statusMe == A_DEAD || statusMe == A_DEADFLY)
            {
                g.drawImage(LoadImageInterface.chardie, cx - LoadImageInterface.chardie.GetWidth() / 2,
                    cy + 10 - LoadImageInterface.chardie.GetHeight() + 4, MGraphics.TOP | MGraphics.LEFT);

                return;
            }
            paintShadow(g);

            if (!isPaint())
            {
                if (skillPaint != null)
                {
                    indexSkill = skillInfoPaint().Length;
                    skillPaint = null;
                    effPaints = null;
                    eff = null;
                    effTask = null;
                    indexEff = -1;
                    indexEffTask = -1;
                }
                return;
            }

            if (skillPaint != null)
                paintCharWithSkill(g);
            else
                PaintCharWithoutSkill(g);

            if (effPaints != null)
                for (var i = 0; i < effPaints.Length; i++)
                    if (effPaints[i] != null)
                    {
                        if (effPaints[i].eMob != null)
                        {
                            if (!effPaints[i].isFly)
                            {
                                effPaints[i].eMob.setInjure();
                                effPaints[i].eMob.injureBy = this;

                                effPaints[i].isFly = true;
                            }
                            SmallImage.DrawSmallImage(g, effPaints[i].getImgId(), effPaints[i].eMob.x, effPaints[i].eMob.y,
                                0, MGraphics.BOTTOM | MGraphics.HCENTER);
                        }
                        else if (effPaints[i].eChar != null)
                        {
                            if (!effPaints[i].isFly)
                            {
                                if (effPaints[i].eChar.charID >= 0)
                                    effPaints[i].eChar.DoInjure();
                                effPaints[i].isFly = true;
                            }
                            SmallImage.DrawSmallImage(g, effPaints[i].getImgId(), effPaints[i].eChar.cx,
                                effPaints[i].eChar.cy + 8, 0, MGraphics.BOTTOM | MGraphics.HCENTER);
                        }
                        effPaints[i].index++;
                        if (effPaints[i].index >= effPaints[i].effCharPaint.arrEfInfo.Length)
                            effPaints[i] = null;
                    }
            if (indexEff >= 0 && eff != null)
            {
                SmallImage.DrawSmallImage(g, eff.arrEfInfo[indexEff].idImg, cx + eff.arrEfInfo[indexEff].dx,
                    cy + 8 + eff.arrEfInfo[indexEff].dy, 0, MGraphics.VCENTER | MGraphics.HCENTER);
                if (GameCanvas.gameTick % 2 == 0)
                {
                    indexEff++;
                    if (indexEff >= eff.arrEfInfo.Length)
                    {
                        indexEff = -1;
                        eff = null;
                    }
                }
            }


            if (indexEffTask >= 0 && effTask != null)
            {
                SmallImage.DrawSmallImage(g, effTask.arrEfInfo[indexEffTask].idImg, cx + effTask.arrEfInfo[indexEffTask].dx,
                    cy + 8 + effTask.arrEfInfo[indexEffTask].dy, 0, MGraphics.VCENTER | MGraphics.HCENTER);
                if (GameCanvas.gameTick % 2 == 0)
                {
                    indexEffTask++;
                    if (indexEffTask >= effTask.arrEfInfo.Length)
                    {
                        indexEffTask = -1;
                        effTask = null;
                    }
                }
            }


            if (typePk >= 0)

                g.drawRegion(LoadImageInterface.iconpk, 0, 12 * (typePk * 3 + GameCanvas.gameTick / 3 % 3), 12, 12, 0,
                    cx, cy - 75, MGraphics.VCENTER | MGraphics.HCENTER);
        }


        //Pain charname by country Huy (18/07/2017)
        private void paintCharName_HP_MP_Overhead(MGraphics g)
        {
            var height = ch + 5;
            if (myChar().cCountry == myChar().cCountryHoa)
                FontManager.GetInstance().tahoma_7_red_charname.DrawStringShadown(g, cName+" "+cx+" "+cy, cx, cy - height - 15, MFont.CENTER);
            else if (myChar().cCountry == myChar().cCountryThuy)
                FontManager.GetInstance().tahoma_7_blue_charname.DrawStringShadown(g, cName+" "+cx+" "+cy, cx, cy - height - 15, MFont.CENTER);
            else if (myChar().cCountry == myChar().cCountryLoi)
                FontManager.GetInstance().tahoma_7_yellow_charname.DrawStringShadown(g, cName+" "+cx+" "+cy, cx, cy - height - 15, MFont.CENTER);
            else if (myChar().cCountry == myChar().cCountryTho)
                FontManager.GetInstance().tahoma_7_brown_charname.DrawStringShadown(g, cName+" "+cx+" "+cy, cx, cy - height - 15, MFont.CENTER);
            else if (myChar().cCountry == myChar().cCountryPhong)
                FontManager.GetInstance().tahoma_7_green_charname.DrawStringShadown(g, cName+" "+cx+" "+cy, cx, cy - height - 15, MFont.CENTER);
        }

        private void PaintCharWithoutSkill(MGraphics g)
        {
            try
            {
                if (ischangingMap)
                    return;


                Part ph = GameScr.parts[head], pl = GameScr.parts[leg], pb = GameScr.parts[body];
                cxtemp = cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx;


                if (cdir == 1)
                {
                    if (statusMe != A_RUN)
                    {
                        SmallImage.DrawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id,
                            cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx,
                            cy + 8 - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
                        SmallImage.DrawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id,
                            cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx,
                            cy + 8 - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
                        SmallImage.DrawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                            cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx,
                            cy + 8 - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
                    }
                    else
                    {
                        if (cf != 7)
                        {
                            SmallImage.DrawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id,
                                cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx,
                                cy + 8 - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
                            SmallImage.DrawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id,
                                cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx,
                                cy + 8 - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
                            SmallImage.DrawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                                cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx,
                                cy + 8 - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
                        }
                        else
                        {
                            SmallImage.DrawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id + 1,
                                cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx,
                                cy + 8 - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
                            SmallImage.DrawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id + 1,
                                cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx,
                                cy + 8 - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
                            SmallImage.DrawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                                cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx,
                                cy + 8 - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
                        }
                    }
                }
                else
                {
                    if (statusMe != A_RUN)
                    {
                        SmallImage.DrawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id,
                            cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx,
                            cy + 8 - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
                        SmallImage.DrawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id,
                            cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx,
                            cy + 8 - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
                        SmallImage.DrawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                            cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx,
                            cy + 8 - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
                    }
                    else
                    {
                        if (cf != 7)
                        {
                            SmallImage.DrawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id,
                                cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx,
                                cy + 8 - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
                            SmallImage.DrawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id,
                                cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx,
                                cy + 8 - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
                            SmallImage.DrawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                                cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx,
                                cy + 8 - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
                        }
                        else
                        {
                            SmallImage.DrawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id + 1,
                                cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx,
                                cy + 8 - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
                            SmallImage.DrawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id + 1,
                                cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx,
                                cy + 8 - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
                            SmallImage.DrawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                                cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx,
                                cy + 8 - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
                        }
                    }
                }
                if (GameCanvas.gameTick % 3 != 0)
                    g.drawImage(
                        LoadImageInterface.list_thunder4[
                            GameCanvas.gameTick / 2 % LoadImageInterface.list_thunder4.Length], cx, cy,
                        MGraphics.VCENTER | MGraphics.HCENTER);
            }
            catch (Exception e)
            {
            }
        }

        private void PaintCharWithoutSkill(MGraphics g, int cx, int cy)
        {
            try
            {
                if (ischangingMap)
                    return;
                Part ph = GameScr.parts[head], pl = GameScr.parts[leg], pb = GameScr.parts[body];
                cxtemp = cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx;

                if (cdir == 1)
                {
                    if (statusMe != A_RUN)
                    {
                        SmallImage.DrawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id,
                            cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx,
                            cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
                        SmallImage.DrawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id,
                            cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx,
                            cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
                        SmallImage.DrawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                            cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx,
                            cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
                    }
                    else
                    {
                        if (cf != 7)
                        {
                            SmallImage.DrawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id,
                                cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx,
                                cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
                            SmallImage.DrawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id,
                                cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx,
                                cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
                            SmallImage.DrawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                                cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx,
                                cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
                        }
                        else
                        {
                            SmallImage.DrawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id + 1,
                                cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx,
                                cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
                            SmallImage.DrawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id + 1,
                                cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx,
                                cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
                            SmallImage.DrawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                                cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx,
                                cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
                        }
                    }
                }
                else
                {
                    if (statusMe != A_RUN)
                    {
                        SmallImage.DrawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id,
                            cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx,
                            cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
                        SmallImage.DrawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id,
                            cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx,
                            cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
                        SmallImage.DrawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                            cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx,
                            cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
                    }
                    else
                    {
                        if (cf != 7)
                        {
                            SmallImage.DrawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id,
                                cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx,
                                cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
                            SmallImage.DrawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id,
                                cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx,
                                cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
                            SmallImage.DrawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                                cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx,
                                cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
                        }
                        else
                        {
                            SmallImage.DrawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id + 1,
                                cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx,
                                cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
                            SmallImage.DrawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id + 1,
                                cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx,
                                cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
                            SmallImage.DrawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                                cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx,
                                cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        public void paintCharWithSkill(MGraphics g)
        {
            var line = 0;
            try
            {
                var skillInfoPaintt = skillInfoPaint();
                cf = skillInfoPaintt[indexSkill].status;
                if (skillInfoPaintt[indexSkill].effS0Id != 0)
                {
                    eff0 = GameScr.efs[skillInfoPaintt[indexSkill].effS0Id - 1];
                    i0 = dx0 = dy0 = 0;
                }
                line = 1;
                if (skillInfoPaintt[indexSkill].effS1Id != 0)
                {
                    eff1 = GameScr.efs[skillInfoPaintt[indexSkill].effS1Id - 1];
                    i1 = dx1 = dy1 = 0;
                }
                if (skillInfoPaintt[indexSkill].effS2Id != 0)
                {
                    eff2 = GameScr.efs[skillInfoPaintt[indexSkill].effS2Id - 1];
                    i2 = dx2 = dy2 = 0;
                }
                var sp = skillInfoPaintt;


                PaintCharWithoutSkill(g);
                line = 1;
                if (cdir == 1)
                {
                    if (eff0 != null)
                    {
                        if (dx0 == 0)
                            dx0 = skillInfoPaintt[indexSkill].e0dx;
                        if (dy0 == 0)
                            dy0 = skillInfoPaintt[indexSkill].e0dy;
                        SmallImage.DrawSmallImage(g, eff0.arrEfInfo[i0].idImg, cx + dx0 + eff0.arrEfInfo[i0].dx,
                            cy + 8 + dy0 + eff0.arrEfInfo[i0].dy, 0,
                            MGraphics.VCENTER | MGraphics.HCENTER);
                        i0++;
                        if (i0 >= eff0.arrEfInfo.Length)
                        {
                            eff0 = null;
                            i0 = dx0 = dy0 = 0;
                        }
                    }
                    if (eff1 != null)
                    {
                        if (dx1 == 0)
                            dx1 = skillInfoPaintt[indexSkill].e1dx;
                        if (dy1 == 0)
                            dy1 = skillInfoPaintt[indexSkill].e1dy;
                        SmallImage.DrawSmallImage(g, eff1.arrEfInfo[i1].idImg, cx + dx1 + eff1.arrEfInfo[i1].dx,
                            cy + 8 + dy1 + eff1.arrEfInfo[i1].dy, 0,
                            MGraphics.VCENTER | MGraphics.HCENTER);
                        i1++;
                        if (i1 >= eff1.arrEfInfo.Length)
                        {
                            eff1 = null;
                            i1 = dx1 = dy1 = 0;
                        }
                    }
                    if (eff2 != null)
                    {
                        if (dx2 == 0)
                            dx2 = skillInfoPaintt[indexSkill].e2dx;
                        if (dy2 == 0)
                            dy2 = skillInfoPaintt[indexSkill].e2dy;
                        SmallImage.DrawSmallImage(g, eff2.arrEfInfo[i2].idImg, cx + dx2 + eff2.arrEfInfo[i2].dx,
                            cy + 8 + dy2 + eff2.arrEfInfo[i2].dy, 0,
                            MGraphics.VCENTER | MGraphics.HCENTER);
                        i2++;
                        if (eff2.arrEfInfo != null)
                            if (i2 >= eff2.arrEfInfo.Length)
                            {
                                eff2 = null;
                                i2 = dx2 = dy2 = 0;
                            }
                    }
                }
                else
                {
                    if (eff0 != null)
                    {
                        if (dx0 == 0)
                            dx0 = skillInfoPaintt[indexSkill].e0dx;
                        if (dy0 == 0)
                            dy0 = skillInfoPaintt[indexSkill].e0dy;
                        SmallImage.DrawSmallImage(g, eff0.arrEfInfo[i0].idImg, cx - dx0 - eff0.arrEfInfo[i0].dx,
                            cy + 8 + dy0 + eff0.arrEfInfo[i0].dy, 2,
                            MGraphics.VCENTER | MGraphics.HCENTER);
                        i0++;
                        if (i0 >= eff0.arrEfInfo.Length)
                        {
                            eff0 = null;
                            i0 = 0;
                            dx0 = 0;
                            dy0 = 0;
                        }
                    }
                    if (eff1 != null)
                    {
                        if (dx1 == 0)
                            dx1 = skillInfoPaintt[indexSkill].e1dx;
                        if (dy1 == 0)
                            dy1 = skillInfoPaintt[indexSkill].e1dy;
                        SmallImage.DrawSmallImage(g, eff1.arrEfInfo[i1].idImg, cx - dx1 - eff1.arrEfInfo[i1].dx,
                            cy + 8 + dy1 + eff1.arrEfInfo[i1].dy, 2,
                            MGraphics.VCENTER | MGraphics.HCENTER);
                        i1++;
                        if (i1 >= eff1.arrEfInfo.Length)
                        {
                            eff1 = null;
                            i1 = 0;
                            dx1 = 0;
                            dy1 = 0;
                        }
                    }
                    if (eff2 != null)
                    {
                        if (dx2 == 0)
                            dx2 = skillInfoPaintt[indexSkill].e2dx;
                        if (dy2 == 0)
                            dy2 = skillInfoPaintt[indexSkill].e2dy;
                        SmallImage.DrawSmallImage(g, eff2.arrEfInfo[i2].idImg, cx - dx2 - eff2.arrEfInfo[i2].dx,
                            cy + 8 + dy2 + eff2.arrEfInfo[i2].dy, 2,
                            MGraphics.VCENTER | MGraphics.HCENTER);
                        i2++;
                        if (i2 >= eff2.arrEfInfo?.Length)
                        {
                            eff2 = null;
                            i2 = 0;
                            dx2 = 0;
                            dy2 = 0;
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        private void CallEff(int effId)
        {
            indexEff = 0;
            eff = GameScr.efs[effId];
        }

        public static int getIndexChar(int ID)
        {
            for (var i = 0; i < GameScr.vCharInMap.size(); i++)
            {
                var c = (Char) GameScr.vCharInMap.elementAt(i);
                if (c.charID == ID)
                    return i;
            }
            return -1;
        }

//        public void moveTo(int toX, int toY)
//        {
//            int dirX = 0, cact = 0;
//            var dx = toX - cx;
//            var dy = toY - cy;
//            if (dx == 0 && dy == 0)
//            {
//                cact = A_STAND;
//            }
//            else if (dy == 0)
//            {
//                cact = A_RUN;
//                if (vMovePoints.size() > 0)
//                {
//                    var mp = (MovePoint) vMovePoints.lastElement();;
//                    if (mp != null && TileMap.GetInstance().TileTypeAt(mp.xEnd, mp.yEnd, TileMap.T_WATERFLOW) &&
//                        mp.yEnd % TileMap.GetInstance().size > 8)
//                    {
//                        cact = A_WATERRUN;
//                    }
//                }
//                if (dx > 0)
//                {
//                    dirX = 1;
//                }
//                else if (dx < 0)
//                {
//                    dirX = -1;
//                }
//                   
//            }
//            else if (dy != 0)
//            {
//                if (dy < 0)
//                {
//                    cact = A_JUMP;
//                } else if (dy > 0)
//                {
//                    cact = A_FALL;
//                }
//
//                if (dx < 0)
//                {
//                    dirX = -1;
//                } else if (dx > 0)
//                {
//                    dirX = 1;
//                }
//            }
//            var x = cx + dx;
//            var y = cy + dy;
//            
//            vMovePoints.addElement(new MovePoint(x, y, cact, dirX));
//        }

        private void SearchFocus()
        {
            int[] d = {-1, -1, -1, -1};
            var minx = myChar().cx - myChar().getdxSkill();
            var maxx = myChar().cx + myChar().getdxSkill();
            var miny = GameScr.cmy - 15;
            var maxy = GameScr.cmy + GameCanvas.h - GameScr.cmdBarH + 100 - 15;
            if (isManualFocus)
                if (mobFocus != null &&
                    minx <= mobFocus.x
                    && mobFocus.x <= maxx && miny <= mobFocus.y && mobFocus.y <= maxy
                )
                    return;
                else
                    isManualFocus = false;
            if (itemFocus == null)
            {
                for (var i = 0; i < GameScr.vItemMap.size(); i++)
                {
                    var itemMap = (ItemMap) GameScr.vItemMap.elementAt(i);
                    var dxx = CRes.abs(myChar().cx - itemMap.x);
                    var dyy = CRes.abs(myChar().cy - itemMap.y);
                    var dd = dxx > dyy ? dxx : dyy;
                    if (dxx <= 50 && dyy <= 80)
                        if (itemFocus == null || dd < d[3])
                        {
                            itemFocus = itemMap;
                            d[3] = dd;
                        }
                }
            }
            else if (minx > itemFocus.x || itemFocus.x > maxx || miny > itemFocus.y || itemFocus.y > maxy)
            {
                itemFocus = null;
                for (var i = 0; i < GameScr.vItemMap.size(); i++)
                {
                    var itemMap = (ItemMap) GameScr.vItemMap.elementAt(i);
                    var dxx = CRes.abs(myChar().cx - itemMap.x);
                    var dyy = CRes.abs(myChar().cy - itemMap.y);
                    var dd = dxx > dyy ? dxx : dyy;
                    if (minx <= itemMap.x && itemMap.x <= maxx && miny <= itemMap.y && itemMap.y <= maxy)
                        if (itemFocus == null || dd < d[3])
                        {
                            itemFocus = itemMap;
                            d[3] = dd;
                        }
                }
            }
            else
            {
                clearFocus(3);
                return;
            }

            // ------mob auto focus
            minx = myChar().cx - myChar().getdxSkill();
            maxx = myChar().cx + myChar().getdxSkill();
            miny = myChar().cy - myChar().GetdySkill() - 80;
            maxy = myChar().cy + myChar().GetdySkill() + 60;

            if (maxy > myChar().cy + 30)
                maxy = myChar().cy + 30;

            if (mobFocus == null)
            {
                for (var i = 0; i < GameScr.vMob.size(); i++)
                {
                    var mob = (Mob) GameScr.vMob.elementAt(i);
                    var dxx = CRes.abs(myChar().cx - mob.x);
                    var dyy = CRes.abs(myChar().cy - mob.y);
                    var dd = dxx > dyy ? dxx : dyy;

                    if (minx <= mob.x && mob.x <= maxx && miny <= mob.y && mob.y <= maxy && mob.status != Mob.MA_INHELL
                        && mob.status != Mob.MA_DEADFLY)
                        if (mobFocus == null || dd < d[0])
                        {
                            if (mob.isBoss && !Mob.isBossAppear) continue;
                            mobFocus = mob;
                            if (!mob.isGetInfo)
                            {
                                mob.isGetInfo = true;
                                Service.GetInstance().RequestMonsterInfo(mob.mobId);
                            }
                            d[0] = dd;
                        }
                }
            }
            else if (minx > mobFocus.x || mobFocus.x > maxx
                     || miny > mobFocus.y || mobFocus.y > maxy)
            {
                mobFocus = null;
                for (var i = 0; i < GameScr.vMob.size(); i++)
                {
                    var mob = (Mob) GameScr.vMob.elementAt(i);
                    var dxx = CRes.abs(myChar().cx - mob.x);
                    var dyy = CRes.abs(myChar().cy - mob.y);
                    var dd = dxx > dyy ? dxx : dyy;

                    if (minx <= mob.x && mob.x <= maxx && miny <= mob.y && mob.y <= maxy && mob.status != Mob.MA_INHELL
                        && mob.status != Mob.MA_DEADFLY)
                        if (mobFocus == null || dd < d[0])
                        {
                            if (mob.isBoss && !Mob.isBossAppear) continue;
                            if (!mob.isGetInfo)
                            {
                                mob.isGetInfo = true;
                                Service.GetInstance().RequestMonsterInfo(mob.mobId);
                            }
                            mobFocus = mob;
                            d[0] = dd;
                        }
                }
            }
            else
            {
                clearFocus(0);
                return;
            }
            minx = myChar().cx - 50;
            maxx = myChar().cx + 50;
            miny = myChar().cy - 30;
            maxy = myChar().cy + 30;
            if (npcFocus != null && npcFocus.template.npcTemplateId == 13)
            {
                minx = myChar().cx - 20;
                maxx = myChar().cx + 20;
                miny = myChar().cy - 10;
                maxy = myChar().cy + 10;
            }
            if (npcFocus == null)
            {
                for (var i = 0; i < GameScr.vNpc.size(); i++)
                {
                    var npc = (Npc) GameScr.vNpc.elementAt(i);
                    if (npc.statusMe == A_HIDE)
                        continue;
                    var dxx = CRes.abs(myChar().cx - npc.cx);
                    var dyy = CRes.abs(myChar().cy - npc.cy);
                    var dd = dxx > dyy ? dxx : dyy;
                    minx = myChar().cx - 80;
                    maxx = myChar().cx + 80;
                    miny = myChar().cy - 30;
                    maxy = myChar().cy + 30;
                    if (npc.template != null && npc.template.npcTemplateId == 13)
                    {
                        minx = myChar().cx - 20;
                        maxx = myChar().cx + 20;
                        miny = myChar().cy - 10;
                        maxy = myChar().cy + 10;
                    }
                    if (minx <= npc.cx && npc.cx <= maxx && miny <= npc.cy && npc.cy <= maxy)
                        if (npcFocus == null || dd < d[1])
                        {
                            npcFocus = npc;
                            d[1] = dd;
                        }
                }
            }
            else if (minx > npcFocus.cx || npcFocus.cx > maxx || miny > npcFocus.cy || npcFocus.cy > maxy)
            {
                DoFocusNPC();
                for (var i = 0; i < GameScr.vNpc.size(); i++)
                {
                    var npc = (Npc) GameScr.vNpc.elementAt(i);
                    if (npc.statusMe == A_HIDE)
                        continue;
                    var dxx = CRes.abs(myChar().cx - npc.cx);
                    var dyy = CRes.abs(myChar().cy - npc.cy);
                    var dd = dxx > dyy ? dxx : dyy;
                    minx = myChar().cx - 80;
                    maxx = myChar().cx + 80;
                    miny = myChar().cy - 30;
                    maxy = myChar().cy + 30;
                    if (npc.template.npcTemplateId == 13)
                    {
                        minx = myChar().cx - 20;
                        maxx = myChar().cx + 20;
                        miny = myChar().cy - 10;
                        maxy = myChar().cy + 10;
                    }
                    if (minx <= npc.cx && npc.cx <= maxx && miny <= npc.cy && npc.cy <= maxy)
                        if (npcFocus == null || dd < d[1])
                        {
                            npcFocus = npc;
                            d[1] = dd;
                        }
                }
            }
            else
            {
                clearFocus(1);
                return;
            }
          
            if (charFocus == null)
            {
                for (var i = 0; i < GameScr.vCharInMap.size(); i++)
                {
                    var c = (Char) GameScr.vCharInMap.elementAt(i);
                    if (c.Equals(myChar())) continue;
                    // if (TileMap.typeMap != TileMap.MAP_DAUTRUONG) {
                    if (c.statusMe == A_HIDE || c.isInvisible)
                        continue;
                    if (c.charID >= -1)
                        continue;
                    if (wdx != 0 || wdy != 0 || c.statusMe == A_DEAD || c.statusMe == A_DEADFLY)
                        continue;
                    var dxx = CRes.abs(myChar().cx - c.cx);
                    var dyy = CRes.abs(myChar().cy - c.cy);
                    var dd = dxx > dyy ? dxx : dyy;
                    if (minx <= c.cx && c.cx <= maxx && miny <= c.cy && c.cy <= maxy && !c.Equals(myChar()))
                    {
                        if (charFocus == null)
                            Service.GetInstance().RequestPlayerInfo((short) c.charID);
                        if (charFocus == null || dd < d[2])
                        {
                            charFocus = c;
                            d[2] = dd;
                        }
                    }
                }
            }
            else if (minx > charFocus.cx || charFocus.cx > maxx || miny > charFocus.cy || charFocus.cy > maxy ||
                     charFocus.statusMe == A_HIDE
                     || charFocus.isInvisible)
            {
                charFocus = null;
                for (var i = 0; i < GameScr.vCharInMap.size(); i++)
                {
                    var c = (Char) GameScr.vCharInMap.elementAt(i);
                    if (c.Equals(myChar())) continue;
                    if (c.statusMe == A_HIDE || c.isInvisible)
                        continue;
                    if (c.charID >= 0)
                        continue;
                    if (wdx != 0 || wdy != 0 || c.statusMe == A_DEAD || c.statusMe == A_DEADFLY)
                        continue;
                    var dxx = CRes.abs(myChar().cx - c.cx);
                    var dyy = CRes.abs(myChar().cy - c.cy);
                    var dd = dxx > dyy ? dxx : dyy;
                    if (minx <= c.cx && c.cx <= maxx && miny <= c.cy && c.cy <= maxy)
                    {
                        if (charFocus == null)
                            Service.GetInstance().RequestPlayerInfo((short) c.charID);
                        if (charFocus == null || dd < d[2])
                        {
                            charFocus = c;
                            d[2] = dd;
                        }
                    }
                }
            }
            else
            {
                clearFocus(2);
                return;
            }
            var index = -1;

            for (var i = 0; i < d.Length; i++)
                if (index == -1)
                {
                    if (d[i] != -1)
                        index = i;
                }
                else if (d[i] < d[index] && d[i] != -1)
                {
                    index = i;
                }
            clearFocus(index);
            //		}
        }

        public void clearAllFocus()
        {
            charFocus = null;
            itemFocus = null;
            mobFocus = null;
            npcFocus = null;
        }

        public void clearFocus(int index)
        {
            if (index == 0)
            {
                DoFocusNPC();
                charFocus = null;
                itemFocus = null;
            }
            else if (index == 1)
            {
                mobFocus = null;
                charFocus = null;
                itemFocus = null;
            }
            else if (index == 2)
            {
                mobFocus = null;
                DoFocusNPC();
                itemFocus = null;
            }
            else if (index == 3)
            {
                mobFocus = null;
                DoFocusNPC();
                charFocus = null;
            }
        }

        public static bool isCharInScreen(Char c)
        {
            var minx = GameScr.cmx;
            var maxx = GameScr.cmx + GameCanvas.w;
            var miny = GameScr.cmy + 10;
            var maxy = GameScr.cmy + GameScr.gH;
            if (c.statusMe != A_HIDE && !c.isInvisible && minx <= c.cx
                && c.cx <= maxx && miny <= c.cy && c.cy <= maxy)
                return true;
            return false;
        }

        public void DoFocusNPC()
        {
            if (me && npcFocus != null)
            {
                npcFocus.chatPopup = null;
                npcFocus = null;
            }
        }


        public void DoInjure(int hpShow, int mpShow, bool isBoss, int idBoss)
        {
            isInjure = 4;
            cf = 21;
        }

        private void DoInjure()
        {
            isInjure = 4;
            CallEff(49);
        }

        private void StartDie(short toX, short toY)
        {
            if (me)
            {
                isLockKey = true;
                for (var i = 0; i < GameScr.vCharInMap.size(); i++)
                {
                    var c = (Char) GameScr.vCharInMap.elementAt(i);
                    c.killCharId = -9999;
                }
            }
            statusMe = A_DEADFLY;
            cp2 = toX;
            cp3 = toY;
            cp1 = 0;
            cHP = 0;
            testCharId = -9999;
            killCharId = -9999;
        }

        private void ChangeStatusStand()
        {
            statusMe = A_STAND;
        }

        public void paintChar(MGraphics g, int x, int y)
        {
            try
            {
                var a = GameCanvas.isTouchControlLargeScreen ? -25 : 16;
                Part ph = GameScr.parts[GameScr.currentCharViewInfo.head],
                    pl = GameScr.parts[GameScr.currentCharViewInfo.leg],
                    pb = GameScr.parts[GameScr.currentCharViewInfo.body];

                SmallImage.DrawSmallImage(g,
                    pl.pi[CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][0]].id,
                    x + CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][1] +
                    pl.pi[CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][0]].dx,
                    y + a - CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][2] +
                    pl.pi[CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][0]].dy, 0, 0);
                SmallImage.DrawSmallImage(g,
                    pb.pi[CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][0]].id,
                    x + CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][1] +
                    pb.pi[CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][0]].dx,
                    y + a - CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][2] +
                    pb.pi[CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][0]].dy, 0, 0);
                SmallImage.DrawSmallImage(g,
                    ph.pi[CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][0][0]].id,
                    x + CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][0][1] +
                    ph.pi[CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][0][0]].dx,
                    y + a - CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][0][2] +
                    ph.pi[CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][0][0]].dy, 0, 0);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        public void paintCharWithSkill(MGraphics g, int index, int xchar, int ychar)
        {
            try
            {
                var skillInfoPaintt = skillInfoPaint();
                if (skillInfoPaintt == null)
                    return;
                cf = skillInfoPaintt[index].status;
                if (skillInfoPaintt[index].effS0Id != 0)
                {
                    eff0 = GameScr.efs[skillInfoPaintt[index].effS0Id - 1];
                    i0 = dx0 = dy0 = 0;
                }
                if (skillInfoPaintt[index].effS1Id != 0)
                {
                    eff1 = GameScr.efs[skillInfoPaintt[index].effS1Id - 1];
                    i1 = dx1 = dy1 = 0;
                }
                if (skillInfoPaintt[index].effS2Id != 0)
                {
                    eff2 = GameScr.efs[skillInfoPaintt[index].effS2Id - 1];
                    i2 = dx2 = dy2 = 0;
                }

                PaintCharWithoutSkill(g, xchar, ychar);
                if (cdir == 1)
                {
                    if (eff0 != null)
                    {
                        if (dx0 == 0)
                            dx0 = skillInfoPaintt[index].e0dx;
                        if (dy0 == 0)
                            dy0 = skillInfoPaintt[index].e0dy;
                        SmallImage.DrawSmallImage(g, eff0.arrEfInfo[i0].idImg, xchar + dx0 + eff0.arrEfInfo[i0].dx,
                            ychar + dy0 + eff0.arrEfInfo[i0].dy, 0,
                            MGraphics.VCENTER | MGraphics.HCENTER);
                        i0++;
                        if (i0 >= eff0.arrEfInfo.Length)
                        {
                            eff0 = null;
                            i0 = dx0 = dy0 = 0;
                        }
                    }
                    if (eff1 != null)
                    {
                        if (dx1 == 0)
                            dx1 = skillInfoPaintt[index].e1dx;
                        if (dy1 == 0)
                            dy1 = skillInfoPaintt[index].e1dy;
                        SmallImage.DrawSmallImage(g, eff1.arrEfInfo[i1].idImg, xchar + dx1 + eff1.arrEfInfo[i1].dx,
                            ychar + dy1 + eff1.arrEfInfo[i1].dy, 0,
                            MGraphics.VCENTER | MGraphics.HCENTER);
                        i1++;
                        if (i1 >= eff1.arrEfInfo.Length)
                        {
                            eff1 = null;
                            i1 = dx1 = dy1 = 0;
                        }
                    }
                    if (eff2 != null)
                    {
                        if (dx2 == 0)
                            dx2 = skillInfoPaintt[index].e2dx;
                        if (dy2 == 0)
                            dy2 = skillInfoPaintt[index].e2dy;
                        SmallImage.DrawSmallImage(g, eff2.arrEfInfo[i2].idImg, xchar + dx2 + eff2.arrEfInfo[i2].dx,
                            ychar + dy2 + eff2.arrEfInfo[i2].dy, 0,
                            MGraphics.VCENTER | MGraphics.HCENTER);
                        i2++;
                        if (eff2.arrEfInfo != null)
                            if (i2 >= eff2.arrEfInfo.Length)
                            {
                                eff2 = null;
                                i2 = dx2 = dy2 = 0;
                            }
                    }
                }
                else
                {
                    if (eff0 != null)
                    {
                        if (dx0 == 0)
                            dx0 = skillInfoPaintt[index].e0dx;
                        if (dy0 == 0)
                            dy0 = skillInfoPaintt[index].e0dy;
                        SmallImage.DrawSmallImage(g, eff0.arrEfInfo[i0].idImg, xchar - dx0 - eff0.arrEfInfo[i0].dx,
                            ychar + dy0 + eff0.arrEfInfo[i0].dy, 2,
                            MGraphics.VCENTER | MGraphics.HCENTER);
                        i0++;
                        if (i0 >= eff0.arrEfInfo.Length)
                        {
                            eff0 = null;
                            i0 = 0;
                            dx0 = 0;
                            dy0 = 0;
                        }
                    }
                    if (eff1 != null)
                    {
                        if (dx1 == 0)
                            dx1 = skillInfoPaintt[index].e1dx;
                        if (dy1 == 0)
                            dy1 = skillInfoPaintt[index].e1dy;
                        SmallImage.DrawSmallImage(g, eff1.arrEfInfo[i1].idImg, xchar - dx1 - eff1.arrEfInfo[i1].dx,
                            ychar + dy1 + eff1.arrEfInfo[i1].dy, 2,
                            MGraphics.VCENTER | MGraphics.HCENTER);
                        i1++;
                        if (i1 >= eff1.arrEfInfo.Length)
                        {
                            eff1 = null;
                            i1 = 0;
                            dx1 = 0;
                            dy1 = 0;
                        }
                    }
                    if (eff2 != null)
                    {
                        if (dx2 == 0)
                            dx2 = skillInfoPaintt[index].e2dx;
                        if (dy2 == 0)
                            dy2 = skillInfoPaintt[index].e2dy;
                        SmallImage.DrawSmallImage(g, eff2.arrEfInfo[i2].idImg, xchar - dx2 - eff2.arrEfInfo[i2].dx,
                            ychar + dy2 + eff2.arrEfInfo[i2].dy, 2,
                            MGraphics.VCENTER | MGraphics.HCENTER);
                        i2++;
                        if (eff2.arrEfInfo != null)
                            if (i2 >= eff2.arrEfInfo.Length)
                            {
                                eff2 = null;
                                i2 = 0;
                                dx2 = 0;
                                dy2 = 0;
                            }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }
    }
}