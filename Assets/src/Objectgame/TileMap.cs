using System;
using src.lib;
using src.main;
using src.screen;
using UnityEngine;

namespace src.Objectgame
{
    public class TileMap
    {
        public const int MAP_SURVIVAL_GAME = 31;
        public const int T_TOP = 2;
        public const int T_LEFT = 2 << 1;
        public const int T_RIGHT = 2 << 2;
        public const int T_WATERFLOW = 2 << 5;
        public const int T_UNDERWATER = 2 << 10;
        public const int T_BOTTOM = 2 << 12;
      
        private static int[] maps;
        private static int[] types;
        private static int bx, dbx, fx, dfx;

        // Tile
        public static MBitmap imgMaptile;
        public static int tmw, tmh, pxw, pxh, tileID;
        public byte size = 24;

        public string mapName = "";
        public static short mapID;
        public static sbyte zoneID, bgID;
        public Vector vGo = new Vector();
        public static Vector vItemBg = new Vector();

        public static Vector vCurrItem = new Vector();

        public static MyHashtable listNameAllMap = new MyHashtable();

        public int[][] tileType;
        public int[][][] tileIndex;

        public static int gssx, gssxe, gssy, gssye, countx, county;
    
    
        private static readonly TileMap Instance = new TileMap();
    
        private TileMap(){}

        public static TileMap GetInstance()
        {
            return Instance;
        }

        public static BgItem GetBiById(int id)
        {
            for (var i = 0; i < vItemBg.size(); i++)
            {
                var bi = (BgItem) vItemBg.elementAt(i);
                if (bi.id == id)
                    return bi;
            }
            return null;
        }

        private static void SetTile(int index, int[] mapsArr, int type)
        {
            for (var i = 0; i < mapsArr.Length; i++)
                if (maps[index] == mapsArr[i])
                {
                    types[index] |= type;
                    return;
                }
        }

        public void LoadMap()
        {
            try
            {
                pxh = tmh * size;
                pxw = tmw * size;

                for (var i = 0; i < tmw * tmh; i++)
                {
                    for (var a = 0; a < tileType[0].Length; a++)
                    {
                        SetTile(i, tileIndex[0][a], tileType[0][a]);
                    }
                }
            }
            catch (Exception e)
            {
                GameMidlet.GetInstance().Exit();
            }
        }

        private void LoadMapFromResource()
        {
            DataInputStream isss = new DataInputStream("/map/" + TileMap.mapID);
            tmw = (char) isss.Read();
            tmh = (char) isss.Read();
            maps = new int[isss.Available()];
            for (var i = 0; i < tmw * tmh; i++)
                maps[i] = (char) isss.Read();
            types = new int[maps.Length];
        }


        public int TileTypeAt(int x, int y)
        {
            try
            {
                return types[y * tmw + x];
            }
            catch (Exception ex)
            {
                return 1000;
            }
        }

        public int TileTypeAtPixel(int px, int py)
        {
            try
            {
                return types[py / size * tmw + px / size];
            }
            catch (Exception ex)
            {
                return 1000;
            }
        }

        public bool TileTypeAt(int px, int py, int t)
        {
            try
            {
                return (types[py / size * tmw + px / size] & t) == t;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public int TileYofPixel(int py)
        {
            return py / size * size;
        }

        public int TileXofPixel(int px)
        {
            return px / size * size;
        }


        public void loadMapfile()
        {
            try
            {
                vCurrItem.removeAllElements();
                vItemBg.removeAllElements();
                LoadMapFromResource();
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        public void loadimgTile(int tileId)
        {
            tileId = tileId - 1;
//            if (imgMaptile != null && imgMaptile.image != null)
//                imgMaptile.cleanImg();
            imgMaptile = null;
            imgMaptile = GameCanvas.loadImage("/tile" + tileId + ".png");
        }

        public void paintMap(MGraphics g)
        {
            GameScr.paintBgItem(g, 1);
            GameScr.gssw = GameCanvas.w / size + 2;
            GameScr.gssh = GameCanvas.h / size + 2;
            if (GameCanvas.w % 24 != 0)
                GameScr.gssw += 1;

            GameScr.gssx = GameScr.cmx / size - 1;
            if (GameScr.gssx < 0)
                GameScr.gssx = 0;
            GameScr.gssy = GameScr.cmy / size;
            GameScr.gssxe = GameScr.gssx + GameScr.gssw;
            GameScr.gssye = GameScr.gssy + GameScr.gssh;
            if (GameScr.gssy < 0)
            {
                GameScr.gssy = 0;
            }
            
            if (GameScr.gssye > tmh - 1)
            {
                GameScr.gssye = tmh - 1;
            }
        
            if (imgMaptile == null) return;
        
            for (var a = GameScr.gssx; a < GameScr.gssxe; a++)
            {
                for (var b = GameScr.gssy; b < GameScr.gssye; b++)
                {
                    try
                    {
                        var t = maps[b * tmw + a] - 1;
                        if ((t + 1) * size > imgMaptile.GetHeight())
                            return;
                        if (t >= 0)
                            g.drawRegion(imgMaptile, 0, t * size, size, size, 0, a * size, b * size, 0);
                    }
                    catch (Exception e)
                    {
                        Debug.LogError(e);
                    }
                }
            }
        }
    }
}