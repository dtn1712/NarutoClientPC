using src;
using src.lib;
using src.main;
using src.model;
using src.Objectgame;
using src.screen;
using src.Gui;

public class QShortQuest : IActionListener
{
    private readonly Command bntOpen;
    private bool moveClose, moveOpen;
    private int v; //gia toc tang toc do move
    private int width = 100;
    private readonly int height = 100;
    private readonly int x = GameCanvas.w - 100; //x,y
    private readonly int y = GameCanvas.h / 2 - 85; //x,y

    private int xMove;
    private readonly int limMove = 70;

    public QShortQuest()
    {
        bntOpen = new Command("", this, Constants.BUTTON_OPEN, null, 0, 0);
        bntOpen.setPos(x, y + height / 2 - Image.getHeight(LoadImageInterface.imgShortQuest) / 2,
            LoadImageInterface.imgShortQuest_Close, LoadImageInterface.imgShortQuest_Close);
    }

    public void perform(int idAction, object p)
    {
        switch (idAction)
        {
            case Constants.BUTTON_OPEN:
                if (!moveClose)
                {
                    moveClose = true;
                    bntOpen.setPos(x, y + height / 2 - Image.getHeight(LoadImageInterface.imgShortQuest) / 2,
                        LoadImageInterface.imgShortQuest, LoadImageInterface.imgShortQuest_Close);
                }
                else
                {
                    moveClose = false;
                    bntOpen.setPos(x, y + height / 2 - Image.getHeight(LoadImageInterface.imgShortQuest) / 2,
                        LoadImageInterface.imgShortQuest_Close, LoadImageInterface.imgShortQuest_Close);
                }
                break;
        }
    }

    public void Update()
    {
        if (moveClose)
        {
            if (xMove > limMove)
            {
                v = 0;
                xMove += GameCanvas.w - (bntOpen.x + Image.getWidth(LoadImageInterface.imgShortQuest));
            }
            else
            {
                xMove += v;
                v++;
            }
        }
        else
        {
            if (xMove <= 0)
            {
                v = 0;
            }
            else
            {
                xMove += v;
                v--;
            }
        }
    }

    public void UpdateKey()
    {
        if (!TScreen.getCmdPointerLast(bntOpen)) return;
        if (bntOpen != null)
        {
            GameCanvas.isPointerJustRelease = false;
            TScreen.keyTouch = -1;
            if (bntOpen != null)
                bntOpen.performAction();
        }
    }

    public void Paint(MGraphics g)
    {
        GameScr.resetTranslate(g);
        PaintFrameChat(g, GuiQuest.listNhacNv.size() * 2 - 1);

        for (var i = 0; i < GuiQuest.listNhacNv.size(); i++)
        {
            var infonv = (InfoItem) GuiQuest.listNhacNv.elementAt(i);
            if (infonv != null)
                FontManager.GetInstance().tahoma_7_white.DrawString(g, infonv.s,
                    x + 4 + xMove + Image.getWidth(LoadImageInterface.imgShortQuest),
                    y + 4 + i * (TScreen.ITEM_HEIGHT + 2), 0);
        }
        bntOpen.x = x + xMove;
        bntOpen.y = bntOpen.y;
        bntOpen.paint(g, 2); //paint button open-close
    }

    private void PaintFrameChat(MGraphics g, int h)
    {
        var xpaint = x + Image.getWidth(LoadImageInterface.imgShortQuest) - 1;
        var w = 10;
        h = h < 8 ? 8 : h;
        //first row
        g.drawImage(LoadImageInterface.imgChatConner, xpaint + xMove, y, MGraphics.TOP | MGraphics.LEFT);
        for (var i = 1; i < w; i++)
            g.drawImage(LoadImageInterface.imgChatRec,
                xpaint + i * Image.getWidth(LoadImageInterface.imgChatRec) + xMove, y, 0, true);
        g.drawRegion(LoadImageInterface.imgChatConner, 0, 0, Image.getWidth(LoadImageInterface.imgChatConner),
            Image.getHeight(LoadImageInterface.imgChatConner), Sprite.TRANS_MIRROR,
            xpaint + w * Image.getWidth(LoadImageInterface.imgChatConner) + xMove, y, MGraphics.TOP | MGraphics.LEFT);

        //paint all rac of frame
        for (var i = 1; i < h; i++)
        {
            for (var j = 0; j < w; j++)
            {
                g.drawImage(LoadImageInterface.imgChatRec,
                    xpaint + j * Image.getWidth(LoadImageInterface.imgChatRec) + xMove,
                    y + Image.getHeight(LoadImageInterface.imgChatRec) * i, 0, true);
            }
        }

        g.drawRegion(LoadImageInterface.imgChatConner, 0, 0, Image.getHeight(LoadImageInterface.imgChatRec),
            Image.getHeight(LoadImageInterface.imgChatRec),
            1, xpaint + xMove, y + Image.getHeight(LoadImageInterface.imgChatRec) * h,
            MGraphics.TOP | MGraphics.LEFT);

        for (var i = 1; i < w; i++)
        {
            g.drawImage(LoadImageInterface.imgChatRec,
                xpaint + i * Image.getWidth(LoadImageInterface.imgChatRec) + xMove,
                y + Image.getHeight(LoadImageInterface.imgChatRec) * h, 0, true);
        }

        g.drawRegion(LoadImageInterface.imgChatRec, 0, 0, Image.getWidth(LoadImageInterface.imgChatRec),
            Image.getHeight(LoadImageInterface.imgChatRec), Sprite.TRANS_MIRROR,
            xpaint + w * Image.getWidth(LoadImageInterface.imgChatRec) + xMove,
            y + Image.getHeight(LoadImageInterface.imgChatRec) * h, MGraphics.TOP | MGraphics.LEFT);
    }
}