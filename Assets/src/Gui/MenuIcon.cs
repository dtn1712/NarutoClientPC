using System;
using src;
using src.Gui;
using src.lib;
using src.main;
using src.model;
using src.Objectgame;
using src.real;
using src.screen;
using UnityEngine;
using Char = src.Objectgame.Char;

public class MenuIcon : TScreen, IActionListener
{
    public static bool isCloseSub, isShowTab;

    public static Vector lastTab = new Vector();
    public GuiContact contact;

    public GuiFriend friend;

    //public RoomMiniGameScr roomMiniGameScr;

    public Command iconChar, iconMission, iconShop, iconContact, iconImprove, iconTrade, iconPK, iconLogout, iconClan
        , iconMinigame; //main

    public int indexpICon;

    public bool paintButtonClose;
    public TabParty party;
    private GuiQuest quest; //quest
    private QuestMain questMain;
    private ShopMain shop; //shop

    public MenuIcon subMenu;
    public TradeGui trade; //trade
    private readonly int width = 430;
    public int x, y;

    public MenuIcon()
    {
    }

    public MenuIcon(int x, int y)
    {
        // TODO Auto-generated constructor stub
        this.x = x;
        this.y = y;
        

        InitComand();
    }

    public virtual void perform(int idAction, object p)
    {
        // TODO Auto-generated method stub
        GameScr.GetInstance().guiMain.moveClose = false;
        switch (idAction)
        {
            case Constants.ICON_CHAR:
                GameScr.GetInstance().guiMain.moveClose = false;
                indexpICon = Constants.ICON_CHAR;
                GameScr.isBag = true;
                var bg = (TabBag) GameCanvas.AllInfo.VecTabScreen.elementAt(0);
                bg.idSelect = -1;
                Service.GetInstance().RequestInventory();
                lastTab.add("" + Constants.ICON_CHAR);
                break;

            case Constants.ICON_MISSION:
                GameScr.GetInstance().guiMain.moveClose = false;
                indexpICon = Constants.ICON_MISSION;
                if (quest == null)
                    quest = new GuiQuest(GameCanvas.wd6 - 20, 20);
                lastTab.add("" + Constants.ICON_MISSION);
                paintButtonClose = true;
                quest.SetPosClose(cmdClose);
                break;
            case Constants.ICON_CLAN:
                Service.GetInstance().Clan_GetlistUser_local();
                Service.GetInstance().Clan_GetlistUser_gobal();
                Service.GetInstance().Clan_GetlistClan_local();
                Service.GetInstance().Clan_GetlistClan_gobal();
                ClanScreen.gI().switchToMe();
                break;
            case Constants.ICON_SETTING:
                SettingScreen.gI().switchToMe();
                break;
            case Constants.ICON_TEAM:
                indexpICon = Constants.ICON_TEAM;
                //if (roomMiniGameScr == null)
                  //  roomMiniGameScr = new RoomMiniGameScr();

//                lastTab.add("" + Constants.ICON_TEAM);
                //Cout.println("adÄ‘ ICON_TEAM " + MenuIcon.lastTab.size());
//                paintButtonClose = true;
//                party.SetPosClose(cmdClose);

                indexpICon = Constants.ICON_TEAM;
                if (party == null)
                    party = new TabParty(GameCanvas.hw, 20);

                lastTab.add("" + Constants.ICON_TEAM);
                //Cout.println("adÄ‘ ICON_TEAM " + MenuIcon.lastTab.size());
                paintButtonClose = true;
                party.SetPosClose(cmdClose);

                break;
            case Constants.ICON_SHOP:
                if (ShopMain.nameMenu == null)
                {
                    GameCanvas.StartOkDlg("Cửa hàng chưa mở cửa. Vui lòng quay lại sau !");
                    return;
                }
                else
                {
                    GameScr.GetInstance().guiMain.moveClose = false;
                    indexpICon = Constants.ICON_SHOP;
                    if (shop == null)
                        shop = new ShopMain(GameCanvas.wd6 - 20, 20);
                    ShopMain.indexidmenu = 0;
                    paintButtonClose = true;
                    shop.SetPosClose(cmdClose);

                    lastTab.add("" + Constants.ICON_SHOP);
                }
                break;

            case Constants.ICON_TRADE:
                if (Char.myChar().partnerTrade != null)
                {
                    GameScr.GetInstance().guiMain.moveClose = false;
                    indexpICon = Constants.ICON_TRADE;
                    trade = new TradeGui(GameCanvas.wd6 - 20, 20);
                    trade.SetPosClose(cmdClose);
                    paintButtonClose = true;
                    lastTab.add("" + Constants.ICON_TRADE);
                }
                break;

            case Constants.ICON_CONTACT:

                indexpICon = Constants.ICON_CONTACT;
                GameScr.GetInstance().guiMain.moveClose = false;
                paintButtonClose = true;
                if (subMenu == null)
                    subMenu = new SubMenuContact(iconContact.x - SubMenuContact.width2, iconContact.y - 50);
                subMenu.SetPosClose(cmdClose);
                lastTab.add("" + Constants.ICON_CONTACT);

                //Cout.println("adÄ‘ contact " + MenuIcon.lastTab.size());
                break;
            case Constants.ICON_DEO_CO:
                GameScr.GetInstance().guiMain.moveClose = false;
                FlagScreen.gI().switchToMe();
                break;
            case Constants.CMD_TAB_CLOSE:
                indexpICon = 0;

                paintButtonClose = false;
                if (lastTab.size() > 0)
                {
                    var indextabx = (string) lastTab.elementAt(lastTab.size() - 1);

                    try
                    {
                        var indextab = int.Parse(indextabx);
                        if (lastTab.size() > 1)
                        {
                            indexpICon = int.Parse((string) lastTab.elementAt(lastTab.size() - 2));
                            setPostCloseTab(indexpICon);
                        }
                        lastTab.removeElementAt(lastTab.size() - 1);
                        switch (indextab)
                        {
                            case Constants.ICON_GIAOTIEP:
                                contact = null;
                                break;
                            case Constants.ICON_CONTACT:
                                subMenu = null;
                                break;
                            case Constants.ICON_TRADE:
                                if (trade != null)
                                {
                                    Service.GetInstance().CancelTrade(Char.myChar().partnerTrade.charID);
                                    trade = null;
                                }
                                break;
                            case Constants.ICON_SHOP:
                                shop = null;
                                break;
                            case Constants.ICON_TEAM:
//                                if (party != null)
//                                    party = null;
//                                if (roomMiniGameScr != null)
//                                    roomMiniGameScr = null;
                                break;
                            case Constants.ICON_MISSION:
                                quest = null;
                                break;
                            case Constants.ICON_SUB_FRIEND:
                                friend = null;
                                break;
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogError(e);
                    }
                }
                lastTab.removeAllElements();
                if (GameScr.GetInstance().guiChatClanWorld.moveClose) //TRUONG HOP DANG MO CHAT WORLD
                {
                    GameScr.GetInstance().guiMain.menuIcon.indexpICon = 0; //truong hop dong chat ko upda menuicon

                    GameScr.GetInstance().guiChatClanWorld.moveClose = false;
                    GameScr.GetInstance().guiChatClanWorld.bntOpen.setPos(
                        GameScr.GetInstance().guiChatClanWorld.x + GameScr.GetInstance().guiChatClanWorld.xBtnMove -
                        Image.getWidth(LoadImageInterface.imgShortQuest) + 35,
                        GameScr.GetInstance().guiChatClanWorld.y + 130 / 2 -
                        Image.getHeight(LoadImageInterface.imgShortQuest) / 2 + 25, LoadImageInterface.imgShortQuest,
                        LoadImageInterface.imgShortQuest);
                }
                break;
            case Constants.ICON_LOGOUT:
                GameCanvas.startYesNoDlg("Bạn có muốn thoát game?",
                    new Command(MResources.YES, GameCanvas.instance, GameCanvas.cLogout, null),
                    new Command(MResources.CANCEL, GameCanvas.instance, 8882, null));
                break;
        }
        isShowTab = lastTab.size() > 0;
        if (!isShowTab && lastTab.size() == 0) indexpICon = 0;
    }

    public void InitComand()
    {
        int x;

        var indexcong = 20;
        x = this.x + width / 2 - 156 - 20 ;

        iconLogout = new Command("", this, Constants.ICON_LOGOUT, null, 0, 0);
        iconLogout.setPos(x, y, LoadImageInterface.imgLogout, LoadImageInterface.imgLogout);

        x = this.x + width / 2 - 125 - 20 ;

        iconTrade = new Command("", this, Constants.ICON_TRADE, null, 0, 0);
        iconTrade.setPos(x, y + 1, LoadImageInterface.imgIconDeoCo, LoadImageInterface.imgIconDeoCo);

        iconPK = new Command("", this, Constants.ICON_DEO_CO, null, 0, 0);
        iconPK.setPos(x, y + 1, LoadImageInterface.imgIconDeoCo, LoadImageInterface.imgIconDeoCo);


        //improve//settings
        x = this.x + width / 2 - 84 - 20 ;
        iconImprove = new Command("", this, Constants.ICON_SETTING, null, 0, 0);
        iconImprove.setPos(x, y + 1, LoadImageInterface.imgSettingIcon, LoadImageInterface.imgSettingIcon);


        //quest
        x = this.x + width / 2 - 47 - 16 ;
        iconMission = new Command("", this, Constants.ICON_MISSION, null, 0, 0);
        iconMission.setPos(x, y + 4, LoadImageInterface.imgMissionIcon, LoadImageInterface.imgMissionIcon);

        //char
        x = this.x + width / 2 - 10 - 16 ;
        iconChar = new Command("", this, Constants.ICON_CHAR, null, 0, 0);
        iconChar.setPos(x, y, LoadImageInterface.imgCharIcon, LoadImageInterface.imgCharIcon);

        //shop
        x = this.x + width / 2 + 27 - 16 ;
        iconShop = new Command("", this, Constants.ICON_SHOP, null, 0, 0);
        iconShop.setPos(x, y + 2, LoadImageInterface.imgShopIcon, LoadImageInterface.imgShopIcon);

        //contact
        x = this.x + width / 2 + 64 - 16 ;
        iconContact = new Command("", this, Constants.ICON_CONTACT, null, 0, 0);
        iconContact.setPos(x, y + 2, LoadImageInterface.imgContactIcon, LoadImageInterface.imgContactIcon);


        //close gui
        cmdClose = new Command(" ", this, Constants.CMD_TAB_CLOSE, null, 0, 0);
        cmdClose.setPos(GameCanvas.hw + 180, y, LoadImageInterface.closeTab, LoadImageInterface.closeTab);

        //Clan
        x = this.x + width / 2 + 100 ;
        iconClan = new Command("", this, Constants.ICON_CLAN, null, 0, 0);
        iconClan.setPos(x, y, LoadImageInterface.imgClanIcon, LoadImageInterface.imgClanIcon);
        // minigame
        x = this.x + width / 2 + 120 + indexcong + 10;
        iconMinigame = new Command("", this, Constants.ICON_CLAN, null, 0, 0);
        iconMinigame.setPos(x, y, LoadImageInterface.imgMiniGame, LoadImageInterface.imgMiniGame);
        
        
    }

    public virtual void update()
    {
        if (!isShowTab && lastTab.size() == 0 && indexpICon != 0)
            indexpICon = 0;
        switch (indexpICon)
        {
            case Constants.ICON_CONTACT:
                if (contact != null)
                    contact.Update();
                break;
            case Constants.ICON_SHOP:
                if (shop != null) //shop
                    shop.update();
                break;
            case Constants.ICON_TEAM:
                if (party != null) //shop
                    party.update();
//                if (roomMiniGameScr != null)
//                {
//                    roomMiniGameScr.update();
//                }
                break;
            case Constants.ICON_SUB_FRIEND:
                if (friend != null)
                    friend.update();
//                if (roomMiniGameScr != null)
//                    roomMiniGameScr.update();
                break;
            case Constants.ICON_GIAOTIEP:
                if (contact != null)
                    contact.Update();
                break;
            case Constants.ICON_MISSION:
                break;
        }
    }

    public virtual void updateKey()
    {
        //click close
        if (getCmdPointerLast(cmdClose) && paintButtonClose)
            if (iconShop != null)
            {
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                keyTouch = -1;
                if (cmdClose != null)
                    cmdClose.performAction();
            }

        switch (indexpICon)
        {
            //chi dc update 1 trong các man hinh
            case Constants.ICON_SHOP:
                if (Char.myChar().mapId == TileMap.MAP_SURVIVAL_GAME)
                    return;
                if (shop != null) //shop
                {
                    shop.updateKey();
                    return;
                }
                break;

            case Constants.ICON_TRADE:
                if (trade != null) //trade
                {
                    trade.updateKey();
                    return;
                }
                break;
            case Constants.ICON_CHAR:

                break;
            case Constants.ICON_MISSION:
                if (quest != null) //shop
                {
                    quest.updateKey();
                    return;
                }
                break;
            case Constants.ICON_CONTACT:
                if (contact != null) //
                {
                    contact.UpdateKey();
                    return;
                }
                break;

            case Constants.ICON_GIAOTIEP:
                if (contact != null)
                    contact.UpdateKey();
                break;
            case Constants.ICON_TEAM:
                if (party != null)
                {
                    party.updateKey();
                    return;
                }
//                if (roomMiniGameScr != null)
//                {
//                    roomMiniGameScr.updateKey();
//                    return;
//                }

                break;
            case Constants.ICON_SUB_FRIEND:
                if (friend != null)
                    friend.updateKey();
//                if (roomMiniGameScr != null)
//                    roomMiniGameScr.updateKey();
                
                break;
            case 0:
                //click char
                if (getCmdPointerLast(iconChar))
                    if (iconChar != null)
                    {
                        GameCanvas.isPointerJustRelease = false;
                        GameCanvas.keyPressed[5] = false;
                        keyTouch = -1;
                        if (iconChar != null)
                            iconChar.performAction();
                    }

                //click shop
                if (getCmdPointerLast(iconShop))
                    if (iconShop != null)
                    {
                        if (Char.myChar().mapId == TileMap.MAP_SURVIVAL_GAME)
                            return;
                        GameCanvas.isPointerJustRelease = false;
                        GameCanvas.keyPressed[5] = false;
                        keyTouch = -1;
                        if (iconShop != null)
                            iconShop.performAction();
                    }
                //logout
                if (getCmdPointerLast(iconLogout))
                    if (iconLogout != null)
                    {
                        
                        GameCanvas.isPointerJustRelease = false;
                        GameCanvas.keyPressed[5] = false;
                        keyTouch = -1;
                        if (iconLogout != null)
                            iconLogout.performAction();
                    }

                //click trade
                if ( getCmdPointerLast(iconPK))
                    if (iconPK != null)
                    {
                        GameCanvas.isPointerJustRelease = false;
                        GameCanvas.keyPressed[5] = false;
                        keyTouch = -1;
                        if (iconPK != null)
                            iconPK.performAction();
                    }

                //click contact
                if (getCmdPointerLast(iconContact))
                    if (iconContact != null)
                    {
                        GameCanvas.isPointerJustRelease = false;
                        GameCanvas.keyPressed[5] = false;
                        keyTouch = -1;
                        if (iconContact != null)
                            iconContact.performAction();
                    }

                //click quest
                if (getCmdPointerLast(iconMission))
                    if (iconMission != null)
                    {
                        GameCanvas.isPointerJustRelease = false;
                        GameCanvas.keyPressed[5] = false;
                        keyTouch = -1;
                        if (iconMission != null)
                            iconMission.performAction();
                    }
                if (getCmdPointerLast(iconImprove))
                    if (iconImprove != null)
                    {
                        GameCanvas.isPointerJustRelease = false;
                        GameCanvas.keyPressed[5] = false;
                        keyTouch = -1;
                        if (iconImprove != null)
                            iconImprove.performAction();
                    }
                //Clan
                if (getCmdPointerLast(iconClan))
                    if (iconClan != null)
                    {
                        GameCanvas.isPointerJustRelease = false;
                        GameCanvas.keyPressed[5] = false;
                        keyTouch = -1;
                        if (iconClan != null)
                            iconClan.performAction();
                    }
                if (getCmdPointerLast(iconMinigame))
                    if (iconMinigame != null)
                    {
                        GameCanvas.isPointerJustRelease = false;
                        GameCanvas.keyPressed[5] = false;
                        keyTouch = -1;
                        if (iconMinigame != null)
                            iconMinigame.performAction();
                    }
                break;
        }


        if (subMenu != null) //sub contact
            subMenu.updateKey();
        if (!GameCanvas.isPointerDown && isCloseSub)
        {
            isCloseSub = false;
            subMenu = null;
        }
    }

    public void setPostCloseTab(int indexTab)
    {
        switch (indexTab)
        {
            case Constants.ICON_CHAR:
                break;

            case Constants.ICON_MISSION:
                GameScr.GetInstance().guiMain.moveClose = false;
                if (quest != null)
                    quest.SetPosClose(cmdClose);
                paintButtonClose = true;
                break;
            case Constants.ICON_TEAM:
                if (party != null)
                    party.SetPosClose(cmdClose);
                paintButtonClose = true;
//                if (roomMiniGameScr != null)
//                    roomMiniGameScr.SetPosClose(cmdClose);
//                paintButtonClose = true;
                break;
            case Constants.ICON_SHOP:
                GameScr.GetInstance().guiMain.moveClose = false;
                if (shop != null)
                    shop.SetPosClose(cmdClose);
                paintButtonClose = true;
                break;

            case Constants.ICON_TRADE:
                GameScr.GetInstance().guiMain.moveClose = false;
                indexpICon = Constants.ICON_TRADE;
                if (trade != null)
                    trade.SetPosClose(cmdClose);
                paintButtonClose = true;
                break;

            case Constants.ICON_CONTACT:
                GameScr.GetInstance().guiMain.moveClose = false;
                paintButtonClose = true;
                if (contact != null)
                    contact.SetPosClose(cmdClose);
                break;
        }
    }

    public virtual void SetPosClose(Command cmd)
    {
        // TODO Auto-generated method stub
        //	Cout.println(getClass(), "menuicon setposclose");
        cmd.setPos(x + width - Image.getWidth(LoadImageInterface.closeTab), y, LoadImageInterface.closeTab,
            LoadImageInterface.closeTab);
    }

    public virtual void paint(MGraphics g)
    {
       
        Paint.PaintBGMenuIcon(x, y, 10, g);
        iconMinigame.y = y + 8;
        iconPK.y = y + 1;
        iconImprove.y = y + 7;
        iconClan.y = y + 3;
        iconMission.y = y + 4;
        iconChar.y = y;
        iconShop.y = y + 2;
        iconContact.y = y + 2;
        iconLogout.y = y + 1;
        iconLogout.paint(g);
        iconPK.paint(g);
        iconMission.paint(g);
        iconChar.paint(g);
        iconShop.paint(g);
        iconImprove.paint(g);
        iconClan.paint(g);
        iconContact.paint(g);
        iconMinigame.paint(g);

        if (shop != null) //shop
            shop.paint(g);
//        if (party != null)
//            party.paint(g);
        if (quest != null) //quest
            quest.paint(g);
        if (trade != null) //trade
            trade.paint(g);
        if (contact != null) 
            contact.Paintt(g);
        if (subMenu != null)
            subMenu.paint(g);
        if (friend != null)
            friend.paint(g);
//        if (roomMiniGameScr != null)
//        {
//            LogDebug.println("Paitn RoomminiGame ----> "+roomMiniGameScr);
//            roomMiniGameScr.paint(g);
//        }
            
        if (paintButtonClose)
            cmdClose.paint(g);

        GameScr.resetTranslate(g);
    }
}