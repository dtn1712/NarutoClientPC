using src;
using src.Gui;
using src.lib;
using src.main;
using src.Objectgame;
using src.real;
using src.screen;

public class SubMenuContact : MenuIcon
{
    public static int width2 = 72;
    private readonly int height = 50;
    private Command iconSubFriend, iconSubParty; //sun contact


    public SubMenuContact(int x, int y)
    {
        //super(x, y);
        // TODO Auto-generated constructor stub

        this.x = x;
        this.y = y;
        InitComand();
    }


    public override void SetPosClose(Command cmd)
    {
        // TODO Auto-generated method stub
        cmd.setPos(
            x + Image.getWidth(LoadImageInterface.imgSubMenu[0]) * 7 - Image.getWidth(LoadImageInterface.closeTab), y,
            LoadImageInterface.closeTab, LoadImageInterface.closeTab);
    }

    public void InitComand()
    {
        // TODO Auto-generated method stub
        var xx = 0;
        //trade
        xx = width2 + x - 37;
        iconSubFriend = new Command("", this, Constants.ICON_SUB_FRIEND, null, 0, 0);
        iconSubFriend.setPos(xx, y, LoadImageInterface.imgFriendIcon, LoadImageInterface.imgFriendIcon);

        xx = width2 + x + 3;
        iconSubParty = new Command("", this, Constants.ICON_SUB_TEAM, null, 0, 0);
        iconSubParty.setPos(xx, y, LoadImageInterface.imgTeamIcon, LoadImageInterface.imgTeamIcon);
    }


    public override void updateKey()
    {
        // TODO Auto-generated method stub
        //click sun friend
        if (GameCanvas.gameTick % 13 == 0)
        if (GameCanvas.keyPressed[5] || getCmdPointerLast(iconSubFriend))
            if (iconSubFriend != null)
            {
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                keyTouch = -1;
                if (iconSubFriend != null)
                    iconSubFriend.performAction();
            }

        //click sub party
        if (GameCanvas.keyPressed[5] || getCmdPointerLast(iconSubParty))
        {
            if (iconSubParty != null)
            {
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                keyTouch = -1;
                if (iconSubParty != null)
                    iconSubParty.performAction();
            }
        }

        //click sub party
        if (GameCanvas.keyPressed[5] || getCmdPointerLast(cmdClose))
        {
            if (cmdClose != null)
            {
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                keyTouch = -1;
                if (cmdClose != null)
                    cmdClose.performAction();
            }
        }
        GameCanvas.clearKeyPressed();
    }

    public override void updatePointer()
    {
        // TODO Auto-generated method stub
        base.updatePointer();
        if (GameCanvas.isPointerDown)
            if (!GameCanvas.IsPointSelect(x, y, width2 * 2, height))
                isCloseSub = true;
    }

    public override void perform(int idAction, object p)
    {
        // TODO Auto-generated method stub
//		super.perform(idAction, p);
        //		super.perform(idAction, p);
        switch (idAction)
        {
            case Constants.ICON_SUB_FRIEND:

                GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                lastTab.add("" + Constants.ICON_SUB_FRIEND);

                Service.GetInstance().RequestFriendList(Char.myChar().charID);
                GameCanvas.gameScr.guiMain.menuIcon.friend = new GuiFriend();
                GameCanvas.gameScr.guiMain.menuIcon.indexpICon = Constants.ICON_SUB_FRIEND;
                GameCanvas.gameScr.guiMain.menuIcon.friend.SetPosClose(GameCanvas.gameScr.guiMain.menuIcon.cmdClose);
                GameCanvas.gameScr.guiMain.menuIcon.paintButtonClose = true;
                isShowTab = true;
             
                break;
            case Constants.ICON_SUB_TEAM:

                GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                GameCanvas.gameScr.guiMain.menuIcon.indexpICon = Constants.ICON_TEAM;
                if (GameCanvas.gameScr.guiMain.menuIcon.party == null)
                {
                    GameCanvas.gameScr.guiMain.menuIcon.party = new TabParty(GameCanvas.hw, 20);
                }
                isShowTab = true;
                lastTab.add("" + Constants.ICON_TEAM);
                GameCanvas.gameScr.guiMain.menuIcon.paintButtonClose = true;
                GameCanvas.gameScr.guiMain.menuIcon.party.SetPosClose(GameCanvas.gameScr.guiMain.menuIcon.cmdClose);
                break;

            case Constants.CMD_TAB_CLOSE:

                GameScr.GetInstance().guiMain.menuIcon.indexpICon = 0;

                break;
        }
    }

    public override void paint(MGraphics g)
    {
        // TODO Auto-generated method stub
        GameScr.resetTranslate(g);
        Paint.PaintBGSubIcon(x, y, 2, g);

        iconSubFriend.paint(g);
        iconSubParty.paint(g);
    }
}