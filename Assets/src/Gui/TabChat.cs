using src.lib;
using src.main;
using src.model;
using src.Objectgame;
using src.screen;

namespace src.Gui
{
    public class TabChat : TScreen, IActionListener
    {
        private static TabChat _instance;

        //icon chat
        public static IconChat iconChat;

        public static Command cmdchat, cmdClose;
        public static Command cmdIconChat;
        public static TField tfCharFriend;
        public int heightGui = 220, popupY, popw = 100, popupW = 220, popx;
        public int indexRow;
        public int indexSize = 28;
        public bool isPaintListIcon;

        public Scroll scrListActor;

        public TabChat()
        {
            cmdchat = new Command("Gửi", Constants.BUTTON_SEND);
            cmdIconChat = new Command("", Constants.BUTTON_ICON_CHAT);
            tfCharFriend = new TField();
            scrListActor = new Scroll();
        }

        public void perform(int idAction, object p)
        {
            // TODO Auto-generated method stub
            switch (idAction)
            {
                case 20:
                    GameScr.GetInstance().switchToMe();
                    break;

                default:
                    break;
            }
        }

        public static TabChat GetInstance()
        {
            return _instance ?? (_instance = new TabChat());
        }

        private void InitTField()
        {
            tfCharFriend.width = popupW - 64;
            tfCharFriend.height = ITEM_HEIGHT + 2;
            popx = GameCanvas.w / 2 - (popw + popupW) / 2 - 5;
            tfCharFriend.x = popx + popw + 8;
            popupY = GameCanvas.h / 2 - heightGui / 2;
            tfCharFriend.y = popupY + heightGui - 30;
            tfCharFriend.isFocus = false;
            tfCharFriend.setIputType(TField.INPUT_TYPE_ANY);
            cmdIconChat.setPos(popx + GameScr.GetInstance().popw + 5 + tfCharFriend.width + 5, tfCharFriend.y,
                LoadImageInterface.imgEmo[7], LoadImageInterface.imgEmo[7]);
            cmdIconChat.w = 15;
            cmdIconChat.h = 20;
            cmdchat.setPos(cmdIconChat.x + cmdIconChat.w + 2, tfCharFriend.y - 5, LoadImageInterface.img_use,
                LoadImageInterface.img_use_focus);
            cmdchat.w = 30;
            cmdchat.h = 20;

            tfCharFriend.y = popupY + heightGui - 32;
        }

        public override void switchToMe()
        {
            InitTField();
            cmdClose = new Command("", this, 20, null);
//		GameScr.xGui+5, GameScr.yGui+5,popupW-20
            cmdClose.setPos(popx + popw + popupW - LoadImageInterface.closeTab.height / 2, popupY,
                LoadImageInterface.closeTab, LoadImageInterface.closeTab);
            scrListActor.selectedItem = -1;
            base.switchToMe();
        }

        public override void update()
        {
            // TODO Auto-generated method stub
            if (isPaintListIcon)
            {
                iconChat.Updatecm();
                iconChat.updateKeySelectIconChat();
                if (iconChat.indexSelect >= 0)
                {
                    tfCharFriend.setText(tfCharFriend.getText() + NodeChat.maEmo[iconChat.indexSelect]);
                    iconChat.indexSelect = -1;
                    isPaintListIcon = false;
                }
            }
            else
            {
                scrListActor.updatecm();
                var s1 = scrListActor.updateKey();
                if (scrListActor.selectedItem > -1)
                    indexRow = scrListActor.selectedItem;
                if (indexRow >= 0 && indexRow < ChatPrivate.vOtherchar.size() &&
                    (OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow) != null)
                {
                    var c = GameScr.findCharInMap(((OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow)).id);
                    if (c != null)
                    {
                        Char.toCharChat = c;

                        ChatPrivate.nTinChuaDoc -= ((OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow)).nTinChuaDoc;
                        ((OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow)).nTinChuaDoc = 0;
                    }
                    ((OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow)).update();
                }
            }
            GameScr.GetInstance().update();
            base.update();
        }

        public override void keyPress(int keyCode)
        {
            tfCharFriend.keyPressed(keyCode);
            base.keyPress(keyCode);
        }

        public override void updateKey()
        {
            // TODO Auto-generated method stub
            updatePointer();
            scrListActor.updateKey();
            if ( /*GameCanvas.keyPressed[5] ||*/ getCmdPointerLast(cmdClose))
                if (cmdClose != null)
                {
                    GameCanvas.isPointerJustRelease = false;
                    GameCanvas.keyPressed[5] = false;
                    keyTouch = -1;
                    if (cmdClose != null)
                        cmdClose.performAction();
                }
            if (indexRow < ChatPrivate.vOtherchar.size() && indexRow >= 0 &&
                (OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow) != null && Char.toCharChat != null)
                Char.toCharChat.CharidDB = ((OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow)).id;
            base.updateKey();
        }

        public override void paint(MGraphics g)
        {
            GameScr.GetInstance().paint(g);
            PaintListFiendChat(g);
            tfCharFriend.paint(g);
            Paint.PaintBoxName("Chat Riêng", popx + popw + popupW / 2 - 55, popupY, popupW - 110, g);
            //paint content of chat friend
            if (indexRow < ChatPrivate.vOtherchar.size() && indexRow >= 0 &&
                (OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow) != null)
            {
                ((OtherChar) ChatPrivate.vOtherchar.elementAt(indexRow)).Paint(g, GameScr.xGui + 5 + popw / 2, GameScr.yGui + 5, popupW - 20, tfCharFriend.y - GameScr.yGui - 7);
            }
            resetTranslate(g);
            cmdchat.paintW(g);
            cmdIconChat.paint(g);
            if (isPaintListIcon)
            {
                iconChat?.paint(g);
            }

            cmdClose.paint(g, true);
            base.paint(g);
        }

        public override void updatePointer()
        {
            if (isPaintListIcon)
            {
                if (GameCanvas.isPointerClick && !GameCanvas.isPoint(iconChat.scrMain.xPos,
                        iconChat.scrMain.yPos, iconChat.scrMain.width, iconChat.scrMain.height))
                {
                    isPaintListIcon = false;
                    GameCanvas.isPointerClick = false;
                }
            }
            else
            {
                if (getCmdPointerLast(cmdchat))
                    if (cmdchat != null)
                    {
                        GameCanvas.isPointerJustRelease = false;
                        keyTouch = -1;
                        if (cmdchat != null)
                            cmdchat.performAction();
                    }
                if ( getCmdPointerLast(cmdIconChat))
                    if (cmdIconChat != null)
                    {
                        GameCanvas.isPointerJustRelease = false;
                        keyTouch = -1;
                        if (cmdIconChat != null)
                            cmdIconChat.performAction();
                    }
                tfCharFriend.update();
            }
            base.updatePointer();
        }

        private void PaintListFiendChat(MGraphics g)
        {
            Paint.paintFrameNaruto(popx - 8, popupY, popw, heightGui, g); //paint list chater
            Paint.paintFrameNaruto(popx + popw, popupY, popupW, heightGui, g); //paint frame chat
            if (ChatPrivate.vOtherchar.size() > 0)
            {
                GameScr.xstart = popx - 3;
                GameScr.ystart = popupY + 5;
                resetTranslate(g);
                scrListActor.setStyle(ChatPrivate.vOtherchar.size(), indexSize, popx - 8, popupY, popw, heightGui, true, 1);
                scrListActor.setClip(g, popx - 8, popupY, popw, heightGui);

                for (var i = 0; i < ChatPrivate.vOtherchar.size(); i++)
                {
                    var c = (OtherChar) ChatPrivate.vOtherchar.elementAt(i);
                    	
                    if (indexRow == i)
                    {
                        g.setColor(Paint.COLORLIGHT);
                        g.fillRect(GameScr.xstart + 4, GameScr.ystart + indexRow * GameScr.indexSize + 4, popw - 19,
                            indexSize - 8);
                        g.setColor(0xffffff);
                        g.drawRect(GameScr.xstart + 4, GameScr.ystart + indexRow * GameScr.indexSize + 4, popw - 19,
                            indexSize - 8);
                    }
              
                    FontManager.GetInstance().tahoma_7_red.DrawString(g, c.name, GameScr.xstart + 8,
                        GameScr.ystart + i * indexSize + indexSize / 2 - 6, 0);

                }
            }
        }
    }
}