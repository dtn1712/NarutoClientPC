using System;
using src.lib;
using src.main;
using src.model;
using src.real;
using src.screen;
using Char = src.Objectgame.Char;

namespace src.Gui
{
    public class TradeGui : TScreen, IActionListener
    {
        public const sbyte TRADE_XU = 7;
        public const short REQUEST_MOVE_MONEY = 1;
        public const sbyte REQUEST_CHUYEN_TIEN = 2;
        public const sbyte REQUEST_SUCCESS = 3;
        public const sbyte REQUEST_FAIL = 4;

        public static Char parner = new Char();
        public static Scroll scrMain = new Scroll();
        public static byte MAX_PAGE_INVETORY = 3;

        public static Command cmdTradeEnd;

        public bool block1, block2;


        public Command cmdTrade, cmdClose, cmdUpItem, cmdBlock1, cmdBlock2, cmdA, cmdLeftArrow, cmdRightArrow, cmdXu;
        private readonly int colsTrade = 2;
        public int columns = 8, rows;
        public byte currentPage = 1;
        private readonly int heightInvent = 232; //height of frame


        //trade
        public MBitmap imgBar,
            imgButtontrade,
            imgButtontradeFocus,
            imgLeftArrow,
            imgLeftArrowFocus,
            imgLineTrade,
            imgLocked,
            imgUnClocked,
            imgName,
            imgRightArrow,
            imgRightArrowFocus;

        public int indexSize = 30,
            indexTitle = 0,
            indexSelect = -1,
            indexRow = -1,
            indexRowMax,
            indexMenu = 0,
            indexCard = -1,
            indexSelect1 = -1,
            indexSelect2 = -1;

        private int numHInvent = 2; //col of inventory

        private int numWInvent = 8; //row of inventory
        public int popupY, popupX, isborderIndex, isselectedRow;

        private readonly int rowsTrade = 4;

        public int typeTrade = 1, typeTradeOrder = 1;


        private readonly int widthInvent = 345; //width of frame
        private int widthTrade = 170;
        public int xstart, ystart, popupW = 163, popupH = 160, cmySK, cmtoYSK, cmdySK, cmvySK, cmyLimSK;
        public int zoneCol = 6, coutFc;

        // idselect đưa lên, idselect1 đưa xuống 

        public TradeGui(int x, int y)
        {
            popupX = GameCanvas.w / 2 - widthInvent / 2;
            popupY = y + heightInvent + 10 > GameCanvas.h ? GameCanvas.h - heightInvent - 10 : y;

            cmdTrade = new Command("Giao dịch", this, Constants.BTN_TRADE, null);

            cmdUpItem = new Command("Chọn", this, Constants.BTN_SELECT, null);

            cmdLeftArrow = new Command("", this, Constants.BTN_lEFT_ARROW, null);

            cmdRightArrow = new Command("", this, Constants.BTN_RIGHT_ARROW, null);
            cmdTradeEnd = new Command("Đồng ý", this, Constants.END_TRADE, null);

            cmdXu = new Command("Xu", this, Constants.BTN_XU, null);
        }

        public void perform(int idAction, object p)
        {
            switch (idAction)
            {
                case Constants.BTN_SELECT:
                    if (Char.myChar().arrItemBag == null || indexSelect == -1 ||
                        indexSelect >= Char.myChar().arrItemBag.Length)
                    {
                        return;
                    }

                    if (indexSelect != -1 && Char.myChar().partnerTrade != null &&
                        indexSelect < Char.myChar().arrItemBag.Length)
                    {
                        Service.GetInstance().MoveItemTrade((short) Char.myChar().partnerTrade.charID, 
                            Constants.MOVE_ITEM_TO_TRADE, 
                            (short) Char.myChar().arrItemBag[indexSelect].itemId);
                    }
                    else if (indexSelect1 != -1 && Char.myChar().partnerTrade != null && indexSelect < Char.myChar().ItemMyTrade.Length && Char.myChar().ItemMyTrade[indexSelect1] != null)
                    {
                        Service.GetInstance().MoveItemTrade((short) Char.myChar().partnerTrade.charID,
                            Constants.MOVE_ITEM_TO_INVENTORY,
                            (short) Char.myChar().ItemMyTrade[indexSelect1].itemId); 
                    }
                        
                    break;
                case Constants.BTN_lEFT_ARROW:
                    if (currentPage > 1)
                    {
                        currentPage -= 1;
                        indexSelect = -1;
                        scrMain.cmtoX -= 8 * indexSize;
                    }
                    break;
                case Constants.BTN_RIGHT_ARROW:
                    if (currentPage < 3)
                    {
                        currentPage += 1;
                        indexSelect = -1;
                        scrMain.cmtoX += 8 * indexSize;
                    }
                    break;
                case Constants.BTN_TRADE:
                    if (!block1)
                        GameCanvas.StartOkDlg("Bạn chưa khóa giao dịch !");
                    else if (!block2 && Char.myChar().partnerTrade != null)
                        GameCanvas.StartOkDlg(Char.myChar().partnerTrade.cName + " chưa khóa giao dịch !");
                    else if (Char.myChar().partnerTrade != null)
                        Service.GetInstance().DoTrade(Char.myChar().partnerTrade.charID);
                    break;
                case Constants.BTN_XU:
                    GameCanvas.StartDlgTField("Giao dịch xu", new Command("OK", this, Constants.BTN_OK_XU, null), null,
                        new Command("Cancel", GameCanvas.instance, GameCanvas.cEndDglTField, null));
                    break;
                case Constants.BTN_OK_XU:
                    var amountStr = GameCanvas.msgdlg.getTextTF();
                    if (amountStr != null && amountStr.Trim().Length == 0)
                    {
                        var amount = Convert.ToInt64(amountStr);
                        if (amount < 100000 && amount > 0)
                            Service.GetInstance().MoveMoney(amount, Char.myChar().partnerTrade.charID);
                        else if (amount > 100000)
                            GameCanvas.StartOkDlg("Số xu tối đa cho 1 lần giao dịch là 100000 xu");
                        else if (amount <= 0)
                            GameCanvas.StartOkDlg("Số xu tối thiểu cho 1 lần giao dịch phải lớn hơn 0 xu");
                        else
                            GameCanvas.StartOkDlg("Chỉ nhập số ");
                    }
                    
                    break;
                case Constants.END_TRADE:
                    if (Char.myChar().partnerTrade != null)
                        Service.GetInstance().EndTrade(Char.myChar().partnerTrade.charID);
                    GameCanvas.EndDlg();
                    break;
            }
        }

        //load all images of trade
        public void LoadImage()
        {
            //load image
            imgBar = GameCanvas.loadImage("/GuiNaruto/Trade/bar.png");
            imgButtontrade = GameCanvas.loadImage("/GuiNaruto/Trade/button_trade.png");
            imgButtontradeFocus = GameCanvas.loadImage("/GuiNaruto/Trade/button_trade2.png");

            imgLineTrade = GameCanvas.loadImage("/GuiNaruto/Trade/line.png");
            imgLocked = GameCanvas.loadImage("/GuiNaruto/Trade/locked.png");
            imgUnClocked = GameCanvas.loadImage("/GuiNaruto/Trade/unlocked.png");
            imgName = GameCanvas.loadImage("/GuiNaruto/Trade/name.png");

            imgLeftArrow = GameCanvas.loadImage("/GuiNaruto/Trade/left_arrow.png");
            imgLeftArrowFocus = GameCanvas.loadImage("/GuiNaruto/Trade/left_arrow2.png");
            imgRightArrow = GameCanvas.loadImage("/GuiNaruto/Trade/right_arrow.png");
            imgRightArrowFocus = GameCanvas.loadImage("/GuiNaruto/Trade/right_arrow2.png");

            cmdTrade.setPos(popupX + widthInvent / 2 - Image.getWidth(imgButtontrade) / 2, popupY + heightInvent / 3,
                imgButtontrade, imgButtontradeFocus);
            cmdUpItem.setPos(popupX + widthInvent - Image.getWidth(imgButtontrade) - 15,
                popupY + heightInvent - Image.getHeight(imgButtontrade) - 5, imgButtontrade, imgButtontradeFocus);
            cmdLeftArrow.setPos(popupX + widthInvent / 3 - 15, popupY + heightInvent - Image.getHeight(imgButtontrade) + 7,
                imgLeftArrow, imgLeftArrowFocus);
            cmdRightArrow.setPos(popupX + widthInvent / 3 - 15 + 35,
                popupY + heightInvent - Image.getHeight(imgButtontrade) + 7, imgRightArrow, imgRightArrowFocus);
            cmdXu.setPos(popupX + widthInvent / 2 - Image.getWidth(imgButtontrade) / 2 + 125,
                popupY + heightInvent / 2 + 40, imgButtontrade, imgButtontradeFocus);
        }

        public override void update()
        {
            if (imgBar == null || imgUnClocked == null)
                LoadImage();
            if (scrMain != null)
                scrMain.updatecm();
        }

        public override void updateKey()
        {
            // xử lý rool select item
            if (imgBar == null && GameCanvas.gameTick % 4 == 0) LoadImage();
            //		    select inventory
            if (GameCanvas.isTouch)
            {
                if (indexSelect != -1)
                {
                    indexSelect1 = -1;
                    indexSelect2 = -1;
                }
                var r = scrMain.updateKey();
                if (r.isDowning || r.isFinish)
                {
                    indexSelect = r.selected;
                    indexSelect -= (currentPage - 1) * 8;
                    if (Char.myChar().arrItemBag != null && indexSelect >= Char.myChar().arrItemBag.Length)
                        indexSelect = -1;
                }
            }
            if (GameCanvas.gameTick % 4 == 0)
            {
                coutFc++;
                if (coutFc > 2)
                    coutFc = 0;
            }
            //up item
            if (getCmdPointerLast(cmdUpItem))
                if (cmdUpItem != null)
                {
                    GameCanvas.isPointerJustRelease = false;
                    GameCanvas.keyPressed[5] = false;
                    keyTouch = -1;
                    if (cmdUpItem != null)
                        cmdUpItem.performAction();
                }

            //trade
            if (getCmdPointerLast(cmdTrade))
                if (cmdTrade != null)
                {
                    GameCanvas.isPointerJustRelease = false;
                    keyTouch = -1;
                    if (cmdTrade != null)
                        cmdTrade.performAction();
                }
            // Trade xu Huy code (21/8/2017)
            if (getCmdPointerLast(cmdXu))
                if (cmdXu != null)
                {
                    GameCanvas.isPointerJustRelease = false;
                    keyTouch = -1;
                    if (cmdXu != null)
                        cmdXu.performAction();
                }
            //arrow left
            if ( getCmdPointerLast(cmdLeftArrow))
                if (cmdLeftArrow != null)
                {
                    GameCanvas.isPointerJustRelease = false;
                    keyTouch = -1;
                    if (cmdLeftArrow != null)
                        cmdLeftArrow.performAction();
                }

            //arrow right
            if (getCmdPointerLast(cmdRightArrow))
                if (cmdRightArrow != null)
                {
                    GameCanvas.isPointerJustRelease = false;
                    keyTouch = -1;
                    if (cmdRightArrow != null)
                        cmdRightArrow.performAction();
                }

            UpdatePointer();
        }

        public void PaintLine(MGraphics g)
        {
            ystart = popupY + 10 + heightInvent - 100;
            var nCol = widthInvent / Image.getWidth(imgLineTrade) + 6;

            for (var i = 0; i < nCol; i++)
                g.drawImage(imgLineTrade, popupX + (Image.getWidth(imgLineTrade) - 1) * i + 15, ystart, 0);

            var yMoney = ystart + 10;
        }

        public void UpdatePointer()
        {
            xstart = popupX + 20;
            ystart = popupY + 55 + 10;

            //focus trade 1
            if (GameCanvas.IsPointSelect(xstart, ystart,
                rowsTrade * (Image.getWidth(LoadImageInterface.ImgItem) + colsTrade),
                colsTrade * (Image.getHeight(LoadImageInterface.ImgItem) + 2) + 12))
            {
                indexSelect = -1; //not focus inventory
                var col = (GameCanvas.px - xstart) / Image.getWidth(LoadImageInterface.ImgItem);
                var row = (GameCanvas.py - ystart) / Image.getWidth(LoadImageInterface.ImgItem);

                if (col > rowsTrade)
                    col = 4;
                if (row >= colsTrade)
                    row = 1;
                var dem = col + row * rowsTrade;
                if (dem >= 0 && Char.myChar().ItemMyTrade != null && Char.myChar().ItemMyTrade[dem] != null)
                    indexSelect1 = dem;
            }

            xstart = popupX + widthInvent - 20 - 112;
            ystart = popupY + 55 + 10;
            //focus trade 2
            if (GameCanvas.IsPointSelect(xstart, ystart, rowsTrade * (Image.getWidth(LoadImageInterface.ImgItem) + 2),
                colsTrade * (Image.getHeight(LoadImageInterface.ImgItem) + 2) + 12))
            {
                indexSelect = -1; //not focus inventory
                var col = (GameCanvas.px - xstart) / Image.getWidth(LoadImageInterface.ImgItem);
                var row = (GameCanvas.py - ystart) / Image.getWidth(LoadImageInterface.ImgItem);

                if (col > rowsTrade)
                    col = 4;
                if (row >= colsTrade)
                    row = 1;

                var dem = col + row * rowsTrade;
                if (dem >= 0 && Char.myChar().partnerTrade != null && Char.myChar().partnerTrade.ItemMyTrade != null
                    && dem < Char.myChar().partnerTrade.ItemMyTrade.Length &&
                    Char.myChar().partnerTrade.ItemMyTrade[dem] != null)
                    indexSelect2 = dem;
            }

            //touch block and unblock
            var x = popupX + 20 + 12 + Image.getWidth(imgName);
            var y = ystart - 12 - Image.getHeight(imgUnClocked);
            if (GameCanvas.IsPointSelect(x, y, Image.getWidth(imgUnClocked), Image.getHeight(imgUnClocked)))
            {
                if (Char.myChar().partnerTrade != null)
                {
                    Service.GetInstance().ConfirmTrade(Char.myChar().partnerTrade.charID);
                }
                GameCanvas.isPointerClick = false;
            }


        }

        public override void paint(MGraphics g)
        {
            GameScr.resetTranslate(g);
            Paint.paintFrameNaruto(popupX, popupY + 10, widthInvent, heightInvent, g);
            paintTrade(g);
            paintParnerTrade(g);
            GameScr.resetTranslate(g);

            cmdUpItem.paint(g, true);
            cmdTrade.paint(g, true);
            cmdXu.paint(g, true);
            cmdLeftArrow.paint(g, true);
            cmdRightArrow.paint(g, true);


            //paint page
            FontManager.GetInstance().tahoma_7b_white.DrawString(g, currentPage + "/" + MAX_PAGE_INVETORY,
                cmdLeftArrow.x + cmdLeftArrow.w + (cmdRightArrow.x - (cmdLeftArrow.x + cmdLeftArrow.w)) / 2,
                cmdLeftArrow.y + 2, MGraphics.VCENTER);


            Paint.PaintBoxName("Giao Dịch", popupX + widthInvent / 4, popupY + 10, widthInvent / 2, g);
            PaintLine(g);
        }

        //paint trade zone
        public void paintTrade(MGraphics g)
        {
            try
            {
                GameScr.resetTranslate(g);
                xstart = popupX + 20;
                ystart = popupY + 55;

                rows = 4;
                columns = 2;
                //draw name char
                g.drawImage(imgName, xstart + 10, ystart - 10, 0);
                FontManager.GetInstance().tahoma_7_yellow.DrawString(g, Char.myChar().cName, xstart + 10 + Image.getWidth(imgName) / 2,
                    ystart - 10, MFont.CENTER);
                //My xu
                FontManager.GetInstance().tahoma_7_yellow.DrawString(g, "Xu: ", xstart + 10 + Image.getWidth(imgName) / 3, ystart - 10,MFont.CENTER);
                //Friend xu
                FontManager.GetInstance().tahoma_7_yellow.DrawString(g, "Xu: ", xstart + 10 + Image.getWidth(imgName) / 3, ystart - 10,MFont.CENTER);

                if (!block1)
                    //draw block
                    g.drawImage(imgUnClocked, xstart + 12 + Image.getWidth(imgName), ystart - 12, 0, true);
                else
                    g.drawImage(imgLocked, xstart + 12 + Image.getWidth(imgName), ystart - 12, 0, true);

                //trade 1
                for (var i = 0; i < rows; i++)
                for (var j = 0; j < columns; j++)
                    g.drawImage(LoadImageInterface.ImgItem, xstart + (Image.getWidth(LoadImageInterface.ImgItem) + 2) * i,
                        10 + ystart + (Image.getHeight(LoadImageInterface.ImgItem) + 2) * j, 0);
                for (var k = 0; k < Char.myChar().ItemMyTrade.Length; k++)
                    if (Char.myChar().ItemMyTrade[k] != null)
                        if (Char.myChar().ItemMyTrade[k].template != null)
                        {
                            var r = k % 4;
                            var c = k / 4;
                            SmallImage.DrawSmallImage(g, Char.myChar().ItemMyTrade[k].template.iconID,
                                xstart + r * indexSize + 14, 10 + ystart + c * indexSize + 14, 0,
                                MGraphics.VCENTER | MGraphics.HCENTER);
                        }
                //focus item
                if (indexSelect1 >= 0)
                    Paint.paintFocus(g,
                        xstart + indexSelect1 % rows * (Image.getWidth(LoadImageInterface.ImgItem) + 2) + 4, ystart
                                                                                                             + indexSelect1 /
                                                                                                             rows *
                                                                                                             (Image
                                                                                                                  .getHeight(
                                                                                                                      LoadImageInterface
                                                                                                                          .ImgItem) +
                                                                                                              2) + 14
                        , LoadImageInterface.ImgItem.GetWidth() - 9, LoadImageInterface.ImgItem.GetWidth() - 9, coutFc);

                // draw bg money
           

                xstart = popupX + widthInvent - 20 - 112;
                ystart = popupY + 55;

                //draw name char
                g.drawImage(imgName, xstart + 10, ystart - 10, 0);
                if (Char.myChar().partnerTrade != null)
                    FontManager.GetInstance().tahoma_7_yellow.DrawString(g, Char.myChar().partnerTrade.cName,
                        xstart + 10 + Image.getWidth(imgName) / 2, ystart - 10, MFont.CENTER);

                //draw block
                if (!block2)
                    g.drawImage(imgUnClocked, xstart + 12 + Image.getWidth(imgName), ystart - 12, 0);
                else
                    g.drawImage(imgLocked, xstart + 12 + Image.getWidth(imgName), ystart - 12, 0);
                for (var i = 0; i < rows; i++)
                for (var j = 0; j < columns; j++)
                    g.drawImage(LoadImageInterface.ImgItem,
                        xstart + (Image.getWidth(LoadImageInterface.ImgItem) + 2) * i,
                        10 + ystart + (Image.getHeight(LoadImageInterface.ImgItem) + 2) * j, 0, true);
                if (Char.myChar().partnerTrade != null && Char.myChar().partnerTrade.ItemParnerTrade.Length > 0)
                    for (var k = 0; k < Char.myChar().partnerTrade.ItemParnerTrade.Length; k++)
                        if (Char.myChar().partnerTrade.ItemParnerTrade[k] != null)
                            if (Char.myChar().partnerTrade.ItemParnerTrade[k].template != null &&
                                Char.myChar().arrItemBag != null)
                            {
                                var r = k % 4;
                                var c = k / 4;
                                SmallImage.DrawSmallImage(g, Char.myChar().partnerTrade.ItemParnerTrade[k].template.iconID,
                                    xstart + r * indexSize + 14, 10 + ystart + c * indexSize + 14, 0,
                                    MGraphics.VCENTER | MGraphics.HCENTER, true);
                           
                            }

                //focus item
                if (indexSelect2 >= 0)
                    g.drawImage(LoadImageInterface.imgFocusSelectItem,
                        xstart + indexSelect2 % rows * (Image.getWidth(LoadImageInterface.ImgItem) + 2) + 2, ystart
                                                                                                             + indexSelect2 /
                                                                                                             rows *
                                                                                                             (Image
                                                                                                                  .getHeight(
                                                                                                                      LoadImageInterface
                                                                                                                          .ImgItem) +
                                                                                                              2) + 12,
                        0);

                // draw bg money
          
            }
            catch (Exception e)
            {
                // TODO: handle exception
                // e.printStackTrace();
            }
        }


        public void paintParnerTrade(MGraphics g)
        {
            try
            {
                GameScr.resetTranslate(g);
                xstart = popupX + 20;
                ystart = popupY + 10 + heightInvent - 90;
                rows = 2;
                columns = 8;
                if (currentPage == 3)
                {
                    columns = 4;
                    scrMain.setStyle(1, indexSize, xstart, ystart, columns * indexSize, rows * indexSize, true, true, 4);
                    scrMain.setClip(g, xstart, ystart, columns * indexSize, rows * indexSize + 2);
                }
                else
                {
                    scrMain.setStyle(1, indexSize, xstart, ystart, columns * indexSize, rows * indexSize, true, true, 8);
                    scrMain.setClip(g, xstart, ystart, columns * indexSize, rows * indexSize + 2);
                }

                for (var i = 0; i < 21; i++)
                for (var j = 0; j < rows; j++)
                    g.drawImage(LoadImageInterface.ImgItem, xstart + (Image.getWidth(LoadImageInterface.ImgItem) + 2) * i,
                        ystart + (Image.getHeight(LoadImageInterface.ImgItem) + 2) * j, 0, true);
                if (Char.myChar().arrItemBag != null)
                    for (var k = 0; k < Char.myChar().arrItemBag.Length; k++)
                    {
                        var r = k % 16 / columns;
                        var c = k % 16 % columns;
                        if (Char.myChar().arrItemBag[k] != null
                            && k < Char.myChar().arrItemBag.Length &&
                            Char.myChar().arrItemBag[k].template != null && k >= (currentPage - 1) * 16 &&
                            k < currentPage * 16)
                            Char.myChar().arrItemBag[k].paintItem(g,
                                xstart + c * indexSize + 14,
                                ystart + r * indexSize + 14);
                    }
           

                GameScr.resetTranslate(g);
                if (currentPage == 3)
                {
                    numWInvent = 4;
                    if (indexSelect >= 0)
                        Paint.paintFocus(g,
                            xstart + indexSelect % numWInvent * (Image.getWidth(LoadImageInterface.ImgItem) + 2) +
                            4, ystart
                               + indexSelect / numWInvent * (Image.getHeight(LoadImageInterface.ImgItem) + 2) + 4
                            , LoadImageInterface.ImgItem.GetWidth() - 9, LoadImageInterface.ImgItem.GetWidth() - 9, coutFc);
                }
                else
                {
                    numWInvent = 8;
                    if (indexSelect >= 0)
                        Paint.paintFocus(g,
                            xstart + indexSelect % numWInvent * (Image.getWidth(LoadImageInterface.ImgItem) + 2) +
                            4, ystart
                               + indexSelect / numWInvent * (Image.getHeight(LoadImageInterface.ImgItem) + 2) + 4
                            , LoadImageInterface.ImgItem.GetWidth() - 9, LoadImageInterface.ImgItem.GetWidth() - 9, coutFc);
                }
            }
            catch (Exception e)
            {
                // TODO: handle exception
            }
        }


        public void SetPosClose(Command cmdClose)
        {
            // TODO Auto-generated method stub
            cmdClose.setPos(popupX + widthInvent - Image.getWidth(LoadImageInterface.closeTab), popupY + 10,
                LoadImageInterface.closeTab, LoadImageInterface.closeTab);
        }
    }
}