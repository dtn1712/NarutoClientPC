using src.lib;
using src.main;
using src.model;
using src.Objectgame.quest;
using src.screen;


namespace src.Gui
{
    public class GuiQuest : TScreen, IActionListener
    {
        private bool isMainQuest, isExtraQuest, isPhoBan; // cho biet dang o tag nao 
    
        private Command cmdMainQuest, cmdExtraQuest, cmdPhoBan; // nut bam chuyen tag
    
    //    private const int actionMainQuest = 0, actionExtraQuest = 1, actionPhoBan = 2; // action perform 
        private Command cmdReciveExtraQuest, cmdNextExtraQuest, cmdCancleExtraQuest; // nut bam nhan huy next quest 
    
        private const int actionReciveExtraQuest = 0, actionNextExtraQuest = 1, actionCancleExtraQuest = 2;
        private bool isWorkingExtraQuest = false;
    
        public static readonly Vector listNhacNv = new Vector();
    
        private int indexRow = -1, indexRowUnRecive;
        private int indexselectNV; //0 nv moi, 1 nv da nhan
        private int indexSize;
        private readonly Scroll info = new Scroll();
        private readonly string[] nameMenu = {"Chính", "Phụ", "Phó bản"};
        private int selectTab; //chinh,phu,phoban
        private readonly Vector VecTabScreen = new Vector();
        private readonly int wMenu;
        private readonly int hMenu;
        private int wScollReceived = 100, hScollReceived = 60;
    
        private readonly int x;
        private readonly int y;
        private const int width = 180;
        private const int height = 232;
        private const int widthSub = 160;
        private const int heightSub = 230;
        private readonly int xSub;
    
        private readonly int xM; //diem nam giua frame
        private readonly int xMenu;
        private readonly int yMenu;
        private readonly int xsub;
        private readonly int wKhoangCach = 4;
    
    
    
        public GuiQuest(int x, int yy)
        {
            var wAll = width + widthSub + wKhoangCach + Image.getWidth(LoadImageInterface.btnTab);
            xsub = GameCanvas.w / 2 - wAll / 2 < 2 ? 2 : GameCanvas.w / 2 - wAll / 2;
            indexselectNV = 0;
            var wall = width + 30 + widthSub;
            this.x = GameCanvas.w / 2 - wall / 2;
            y = GameCanvas.h / 2 - heightSub / 2 < 0 ? 0 : GameCanvas.h / 2 - heightSub / 2;
    
            var xdem = this.x + widthSub;
            xSub = this.x;
            this.x = xdem + 10;
            xMenu = this.x + width - 15;
            yMenu = y + 40;
            xM = this.x + width / 2;
    
            var qMain = new QMain();
            VecTabScreen.addElement(qMain);
    
            var qMain1 = new QMain();
            VecTabScreen.addElement(qMain1);
    
            var qMain2 = new QMain();
            VecTabScreen.addElement(qMain2);
    
            isMainQuest = true;
            isExtraQuest = false;
            isPhoBan = false;
    
            cmdReciveExtraQuest = new Command("Nhận", this, actionReciveExtraQuest, null);
            cmdNextExtraQuest = new Command("Kế ", this, actionNextExtraQuest, null);
            cmdCancleExtraQuest = new Command("Huỷ", this, actionCancleExtraQuest, null);
    
    
            cmdReciveExtraQuest.setPos((xsub + widthSub / 2 - LoadImageInterface.img_use.GetWidth() / 2) - 27,
                (y + heightSub - 2 * LoadImageInterface.img_use.GetHeight() - 2) + 15, LoadImageInterface.img_use,
                LoadImageInterface.img_use_focus);
            cmdNextExtraQuest.setPos((xsub + widthSub / 2 - LoadImageInterface.img_use.GetWidth() / 2) + 52,
                (y + heightSub - 2 * LoadImageInterface.img_use.GetHeight() - 2) + 15, LoadImageInterface.img_use,
                LoadImageInterface.img_use_focus);
            cmdCancleExtraQuest.setPos(xsub + widthSub / 2 - LoadImageInterface.img_use.GetWidth() / 2 + 7,
                (y + heightSub - 2 * LoadImageInterface.img_use.GetHeight() - 2) + 15, LoadImageInterface.img_use,
                LoadImageInterface.img_use_focus);
    
    
        }
    
    
        public void perform(int idAction, object p) // xu ly nut bam
        {
            switch (idAction)
            {
                case actionReciveExtraQuest: // gui thong tin nhan nhiem vu daily cho server
    
                    break;
                case actionNextExtraQuest: // gui next qua nhiem vu tiep theo cho server 
                    break;
                case actionCancleExtraQuest: // gui huy quest cho server;
                    break;
            }
        }
    
        public void update()
        {
            if (selectTab == 0)
            {
                isMainQuest = true;
                isExtraQuest = false;
                isPhoBan = false;
    
            }
            else if (selectTab == 1)
            {
                isMainQuest = false;
                isExtraQuest = true;
                isPhoBan = false;
    
            }
            else if (selectTab == 2)
            {
                isMainQuest = false;
                isExtraQuest = false;
                isPhoBan = true;
    
            }
        }
    
        public void SetPosClose(Command cmd)

        {
            cmd.setPos(x + width - Image.getWidth(LoadImageInterface.closeTab), y, LoadImageInterface.closeTab,
                LoadImageInterface.closeTab);
        }
    
        public void updateKey()
        {
            info.updatecm();
            info.updateKey();
    
            if (GameCanvas.isPointer(x + 40, y + 70, 160, Image.getHeight(LoadImageInterface.bgQuestLineFocus)) &&
                selectTab == 0)
            {
                indexselectNV = 0;
                GameCanvas.isPointerClick = false;
            }
            else if (GameCanvas.isPointer(x + 40, y + 165, 160, Image.getHeight(LoadImageInterface.bgQuestLineFocus)) &&
                     selectTab == 0)
            {
                indexselectNV = 1;
                GameCanvas.isPointerClick = false;
            }
            for (var i = 0; i < VecTabScreen.size(); i++)
            {
                if (!GameCanvas.isPointer(xMenu, yMenu + (5 + Image.getHeight(LoadImageInterface.btnTab)) * i,
                    Image.getWidth(LoadImageInterface.btnTab), Image.getHeight(LoadImageInterface.btnTab)))
                {
                    continue;
                }
    
                selectTab = i;
                indexselectNV = -1;
                GameCanvas.isPointerClick = false;
            }
            update();
        }
    
    
    
        public void paint(MGraphics g)
        {
            GameScr.resetTranslate(g);
            g.setColor(0x000000, GameCanvas.opacityTab);
            g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
            g.disableBlending();
    
    
            for (var i = 0; i < VecTabScreen.size(); i++)
            {
                g.drawImage(LoadImageInterface.btnTab, xMenu, yMenu + (5 + Image.getHeight(LoadImageInterface.btnTab)) * i,
                    0);
            }
    
            for (var i = 0; i < VecTabScreen.size(); i++)
            {
                if (i != selectTab)
                {
                    FontManager.GetInstance().tahoma_7b_white.DrawString(g, nameMenu[i], xMenu + 32,
                        yMenu + Image.getHeight(LoadImageInterface.btnTab) / 4 +
                        (5 + Image.getHeight(LoadImageInterface.btnTab)) * i, 2);
    
                }
    
            }
    
            Paint.paintFrameNaruto(x, y, width, height, g);
            Paint.PaintBoxName("Nhiệm vụ" + nameMenu[selectTab].ToLower(), x + 45, y, width - 90, g);
            Paint.SubFrame(xSub, y, widthSub, heightSub, g); //sub
    
            paintNewQuest(g);
            Paint.PaintLine(x + 5, y + 125, width - 30, g); //line
            paintWorkingQuest(g);
    
            g.drawImage(LoadImageInterface.btnTabFocus, xMenu,
                yMenu + (5 + Image.getHeight(LoadImageInterface.btnTabFocus)) * selectTab, 0);
    
            for (var i = 0; i < VecTabScreen.size(); i++)
            {
                if (i == selectTab)
                {
                    FontManager.GetInstance().tahoma_7b_white.DrawString(g, nameMenu[i], xMenu + 32,
                        yMenu + Image.getHeight(LoadImageInterface.btnTab) / 4 +
                        (5 + Image.getHeight(LoadImageInterface.btnTab)) * i, 2);
                }
            }
            if (isExtraQuest)
            {
                if (isWorkingExtraQuest)
                {
                    cmdCancleExtraQuest.paint(g);
                }
                else
                {
                    cmdReciveExtraQuest.paint(g);
                    cmdNextExtraQuest.paint(g);
                }
    
            }
    
        }
    
        private void paintInfoQuestFocus(MGraphics g, int x, int y, QuestTemplate q, byte typequest)
        {
            var lenghtContentDis = q.ContentPaint.Length;
            var lenghtContentRemind = q.RemindContent.Length;
            var size = 3 + (q.SupportContentPaint != null ? +q.SupportContentPaint.Length : 0);
            size += typequest == 1 ? 3 + q.SupportContentPaint.Length : 0; //yeu cau
            if (q.Gold > 0) size += 2;
            if (q.Xu > 0) size += 2;
            info.setStyle(size, 12, x - 5, y, widthSub, heightSub - 20, true, 1);
            info.setClip(g, x - 5, y, widthSub, heightSub - 20);
    
            //name
            g.drawImage(LoadImageInterface.imgName, x + 30, y + 10, MGraphics.VCENTER | MGraphics.HCENTER);
            FontManager.GetInstance().tahoma_7b_white.DrawString(g, "Nhiệm vụ", x + 30, y + 5, 2);
            FontManager.GetInstance().tahoma_7b_yellow.DrawString(g, q.Name, x + 15, y + 20, 0);
    
            // mo ta
            g.drawImage(LoadImageInterface.imgName, x + 30, y + 40, MGraphics.VCENTER | MGraphics.HCENTER);
            FontManager.GetInstance().tahoma_7b_white.DrawString(g, "Mổ tả", x + 30, y + 35, 2);
            if (q.ContentPaint != null)
            {
                for (var i = 0; i < q.ContentPaint.Length; i++)
                {
                    FontManager.GetInstance().tahoma_7_white.DrawString(g, q.ContentPaint[i], x + 15, y + 50 + 12 * i, 0);
                }
            }
    
            switch (typequest)
            {
                case 0:
                    // qua
                    g.drawImage(LoadImageInterface.imgName, x + 30, y + 65 + 12 * lenghtContentDis,
                        MGraphics.VCENTER | MGraphics.HCENTER);
                    FontManager.GetInstance().tahoma_7b_white
                        .DrawString(g, "Thưởng", x + 30, y + 60 + 12 * lenghtContentDis, 2);
                    if (q.Gold > 0)
                    {
                        g.drawImage(LoadImageInterface.coins[1], x + 15, y + 75 + 12 * lenghtContentDis,
                            MGraphics.TOP | MGraphics.LEFT);
                        FontManager.GetInstance().tahoma_7_white.DrawString(g, q.Gold + "", x + 40,
                            (y + 80 + 12 * lenghtContentDis) + 5,
                            0);
                    }
                    if (q.Xu > 0)
                    {
                        g.drawImage(LoadImageInterface.coins[0], x + 15,
                            y + 75 + (q.Gold > 0 ? 20 : 0) + 12 * lenghtContentDis,
                            MGraphics.TOP | MGraphics.LEFT);
                        FontManager.GetInstance().tahoma_7_white.DrawString(g, q.Xu + "", x + 40,
                            (y + 75 + (q.Gold > 0 ? 28 : 0) + 12 * lenghtContentDis) + 5, 0);
                    }
    
                    break;
                case 1: //dag lam
                    g.drawImage(LoadImageInterface.imgName, x + 30, y + 65 + 12 * lenghtContentDis,
                        MGraphics.VCENTER | MGraphics.HCENTER);
                    FontManager.GetInstance().tahoma_7b_white
                        .DrawString(g, "Thưởng", x + 30, y + 60 + 12 * lenghtContentDis, 2);
                    if (q.Gold > 0)
                    {
                        g.drawImage(LoadImageInterface.coins[1], x + 15, y + 75 + 12 * lenghtContentDis,
                            MGraphics.TOP | MGraphics.LEFT);
                        FontManager.GetInstance().tahoma_7_white.DrawString(g, q.Gold + "", x + 40,
                            (y + 80 + 12 * lenghtContentDis) + 5,
                            0);
                    }
                    if (q.Xu > 0)
                    {
                        g.drawImage(LoadImageInterface.coins[0], x + 15,
                            y + 75 + (q.Gold > 0 ? 20 : 0) + 12 * lenghtContentDis,
                            MGraphics.TOP | MGraphics.LEFT);
                        FontManager.GetInstance().tahoma_7_white.DrawString(g, q.Xu + "", x + 40,
                            (y + 75 + (q.Gold > 0 ? 28 : 0) + 12 * lenghtContentDis) + 5, 0);
                    }
                    break;
            }
            GameCanvas.ResetTrans(g);
        }
    
        private void paintNewQuest(MGraphics g)
        {
            //new quest
            if (isMainQuest) // nhiem vu chinh
            {
                g.drawImage(LoadImageInterface.imgName, xM, y + 50, MGraphics.VCENTER | MGraphics.HCENTER);
                FontManager.GetInstance().tahoma_7b_white.DrawString(g, "Mới", xM, y + 45, 2);
                Paint.PaintBGListQuest(x + 30, y + 70, width - 50, g); //new quest
    
                if (indexselectNV == 0 && MainQuestManager.getInstance().NewQuest != null &&
                    GameCanvas.gameTick / 10 % 2 == 0)
                {
                    Paint.PaintBGListQuestFocus(x + 30, y + 70, width - 50, g); //new focus quest
                }
    
                //paint ds nv moi
                var questName = MainQuestManager.getInstance().NewQuest != null && selectTab == 0
                    ? MainQuestManager.getInstance().NewQuest.Name
                    : "Không có nhiệm vụ";
    
                FontManager.GetInstance().tahoma_7b_yellow.DrawString(g, questName, x + 30, y + 75, 0);
    
                // thong tin quest focus 
                if (indexselectNV == 0 && MainQuestManager.getInstance().NewQuest != null)
                {
                    paintInfoQuestFocus(g, xSub + 15, y + 5, MainQuestManager.getInstance().NewQuest, 0);
                }
            }
            else if (isExtraQuest) // nhiem vu phu 
            {
                g.drawImage(LoadImageInterface.imgName, xM, y + 50, MGraphics.VCENTER | MGraphics.HCENTER);
                FontManager.GetInstance().tahoma_7b_white.DrawString(g, "Mới", xM, y + 45, 2);
                Paint.PaintBGListQuest(x + 30, y + 70, width - 50, g); //new quest
    
                if (indexselectNV == 0 && MainQuestManager.getInstance().NewQuest != null &&
                    GameCanvas.gameTick / 10 % 2 == 0)
                {
                    Paint.PaintBGListQuestFocus(x + 30, y + 70, width - 50, g); //new focus quest
                }
    
                //paint ds nv moi
                var questName = MainQuestManager.getInstance().NewQuest != null && selectTab == 0
                    ? MainQuestManager.getInstance().NewQuest.Name
                    : "Không có nhiệm vụ";
    
                FontManager.GetInstance().tahoma_7b_yellow.DrawString(g, questName, x + 30, y + 75, 0);
    
                // thong tin quest focus 
                if (indexselectNV == 0 && MainQuestManager.getInstance().NewQuest != null)
                {
                    paintInfoQuestFocus(g, xSub + 15, y + 5, MainQuestManager.getInstance().NewQuest, 0);
                }
            }
            else if (isPhoBan) // nhiem vu pho ban
            {
                g.drawImage(LoadImageInterface.imgName, xM, y + 50, MGraphics.VCENTER | MGraphics.HCENTER);
                FontManager.GetInstance().tahoma_7b_white.DrawString(g, "Mới", xM, y + 45, 2);
                Paint.PaintBGListQuest(x + 30, y + 70, width - 50, g); //new quest
    
                if (indexselectNV == 0 && MainQuestManager.getInstance().NewQuest != null &&
                    GameCanvas.gameTick / 10 % 2 == 0)
                {
                    Paint.PaintBGListQuestFocus(x + 30, y + 70, width - 50, g); //new focus quest
                }
    
                //paint ds nv moi
                var questName = MainQuestManager.getInstance().NewQuest != null && selectTab == 0
                    ? MainQuestManager.getInstance().NewQuest.Name
                    : "Không có nhiệm vụ";
    
                FontManager.GetInstance().tahoma_7b_yellow.DrawString(g, questName, x + 30, y + 75, 0);
    
                // thong tin quest focus 
                if (indexselectNV == 0 && MainQuestManager.getInstance().NewQuest != null)
                {
                    paintInfoQuestFocus(g, xSub + 15, y + 5, MainQuestManager.getInstance().NewQuest, 0);
                }
            }
    
        }

        private void paintWorkingQuest(MGraphics g)
        {
            // Received
            if (isMainQuest)
            {
                g.drawImage(LoadImageInterface.imgName, xM, y + 145, MGraphics.VCENTER | MGraphics.HCENTER);
                FontManager.GetInstance().tahoma_7b_white.DrawString(g, "Đã nhận", xM, y + 140, 2);
                Paint.PaintBGListQuest(x + 30, y + 165, width - 50, g); //dang lam or hoan thanh

                if (indexselectNV == 1 &&
                    (MainQuestManager.getInstance().WorkingQuest != null ||
                     MainQuestManager.getInstance().FinishQuest != null) && GameCanvas.gameTick / 10 % 2 == 0)
                {
                    Paint.PaintBGListQuestFocus(x + 30, y + 165, width - 50, g); //new focus quest
                }

                var questName = selectTab == 0 && MainQuestManager.getInstance().WorkingQuest != null
                    ? MainQuestManager.getInstance().WorkingQuest.Name
                    : "Không có nhiệm vụ";

                FontManager.GetInstance().tahoma_7b_yellow.DrawString(g, questName, x + 30, y + 170, 0);

                if (indexselectNV == 1 && MainQuestManager.getInstance().WorkingQuest != null)
                {
                    paintInfoQuestFocus(g, xSub + 15, y + 5, MainQuestManager.getInstance().WorkingQuest, 1);
                }
            }
            else if (isExtraQuest)
            {
                g.drawImage(LoadImageInterface.imgName, xM, y + 145, MGraphics.VCENTER | MGraphics.HCENTER);
                FontManager.GetInstance().tahoma_7b_white.DrawString(g, "Đã nhận", xM, y + 140, 2);
                Paint.PaintBGListQuest(x + 30, y + 165, width - 50, g); //dang lam or hoan thanh

                if (indexselectNV == 1 &&
                    (MainQuestManager.getInstance().WorkingQuest != null ||
                     MainQuestManager.getInstance().FinishQuest != null) && GameCanvas.gameTick / 10 % 2 == 0)
                {
                    Paint.PaintBGListQuestFocus(x + 30, y + 165, width - 50, g); //new focus quest
                }

                var questName = selectTab == 0 && MainQuestManager.getInstance().WorkingQuest != null
                    ? MainQuestManager.getInstance().WorkingQuest.Name
                    : "Không có nhiệm vụ";

                FontManager.GetInstance().tahoma_7b_yellow.DrawString(g, questName, x + 30, y + 170, 0);

                if (indexselectNV == 1 && MainQuestManager.getInstance().WorkingQuest != null)
                {
                    paintInfoQuestFocus(g, xSub + 15, y + 5, MainQuestManager.getInstance().WorkingQuest, 1);
                }
            }
            else if (isPhoBan)
            {
                g.drawImage(LoadImageInterface.imgName, xM, y + 145, MGraphics.VCENTER | MGraphics.HCENTER);
                FontManager.GetInstance().tahoma_7b_white.DrawString(g, "Đã nhận", xM, y + 140, 2);
                Paint.PaintBGListQuest(x + 30, y + 165, width - 50, g); //dang lam or hoan thanh

                if (indexselectNV == 1 &&
                    (MainQuestManager.getInstance().WorkingQuest != null ||
                     MainQuestManager.getInstance().FinishQuest != null) && GameCanvas.gameTick / 10 % 2 == 0)
                {
                    Paint.PaintBGListQuestFocus(x + 30, y + 165, width - 50, g); //new focus quest
                }

                var questName = selectTab == 0 && MainQuestManager.getInstance().WorkingQuest != null
                    ? MainQuestManager.getInstance().WorkingQuest.Name
                    : "Không có nhiệm vụ";

                FontManager.GetInstance().tahoma_7b_yellow.DrawString(g, questName, x + 30, y + 170, 0);

                if (indexselectNV == 1 && MainQuestManager.getInstance().WorkingQuest != null)
                {
                    paintInfoQuestFocus(g, xSub + 15, y + 5, MainQuestManager.getInstance().WorkingQuest, 1);
                }
            }
        }
    }
}

