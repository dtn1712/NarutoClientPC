using src.lib;
using src.main;
using src.model;
using src.Objectgame;
using src.real;
using src.screen;

namespace src.Gui
{
    public class TabNangCap : MainTabNew
    {
        public const sbyte SUB_UPGRADE = 0;
        public const sbyte SUB_TAKE_OFF = 1;
        public const sbyte SUB_TAKE_ON = 2;
        public const sbyte SUB_UPDATE_INFO_UPGRADE = 3;
        public const sbyte SUB_UPGRADE_SUCCESS = 4;
        public const sbyte SUB_UPGRADE_FAIL = 5;

        public static int maxSize = 12;
        public static int delta = 0;
        public static int numberItemInBag;

        // list item
        public static string[] meffskill = new string[5];

        public static bool isSuccess;
        public static bool isFail;
        public int[] arrItemNangCap = new int[9];
        private readonly Command dropAllItem;
        private readonly Command shop;
        private readonly Command cmdGo;
        private readonly Command cmdNangCap;
        private readonly Command cmdDuaLen;
        private Command cmdTestUp, cmdTestDown;
        private int h12, w5;
        private readonly int heightBGChar = 100;

        private int idSelect;

        private MBitmap imgButton_dressed,
            imgButton_dressed2,
            imgBgInfo,
            imgEff_dapdo,
            imgChien_luc,
            imgCoins1,
            imgCoins2,
            imgExp_tube,
            imgExp,
            imgLevel;

        private int indexFocusListItemNangCap = -1;
        private bool isDuaLen, isShowCmd = true;
        public bool isLevelUp;
        private bool isList = false;
        private bool isShowInfo = false;
        public bool isSpecialEffect;
        public bool isThunderEffect;

        private Item itemFocus;
        private readonly MBitmap[] list_eff = new MBitmap[12];

        private readonly MBitmap[] list_fail = new MBitmap[18];

        private readonly MBitmap[] list_lv1 = new MBitmap[10];

        private readonly MBitmap[] list_success = new MBitmap[18];

        private readonly MBitmap[] list_upgrade = new MBitmap[16];
        public Item[] listItemNangCap = new Item[9];

        private int lvItemCurrent = 0;

        private int mamau = 0xefec14;

        //paint bg human
        public int[] mang_eff = {0, 1, 2, 3, 4, 3, 2, 1};

        private int maxList, selectList, xList, yList, timeUpdateInfo;

        private int[] mColorInfo;

        //trang bi
        private int numW, numH, coutFc;

        private readonly Scroll scroll_listItem = new Scroll();
        public int tick, tick2, tick3;
        private readonly int widthBGChar = 130;

        private int wsize;

        private readonly int xBGHuman = gI().xTab + gI().widthFrame / 6 - 10;

        // info
        private readonly int xStart;

        private readonly int yStart;

        public int[][] xyItemNangCap = new int[14][];
        private readonly int yBGHuman = gI().yTab + GameCanvas.h / 6 - 10;

        public int z;

        public TabNangCap(string nametab)
        {
            xBGHuman = xTab + 16;
            if (GameCanvas.isTouch)
                idSelect = -1;
            else
                idSelect = 0;

            for (var i = 0; i < xyItemNangCap.Length; i++)
                xyItemNangCap[i] = new int[2];
            xStart = xBegin + wblack / 2 - numW * wOneItem / 2 + numW / 2;
            yStart = yBegin + 10;

            xBegin = xTab + wOneItem / 2 + 4;
            yBegin = yTab + 30 + wOneItem - 5;
            wsize = wOneItem;
            typeTab = EQUIP;
            yBGHuman = yTab + 15;
            nameTab = nametab;
            var xCmd = xBGHuman + widthBGChar + 32;
            var yCmd = yBGHuman + 27;

            dropAllItem = new Command("", this, Constants.BTN_USE_ITEM, null, 0, 0);
            dropAllItem.setPos(xCmd, yCmd, imgButton_dressed, imgButton_dressed2);

            yCmd = yCmd + 30;
            shop = new Command("", this, Constants.BTN_USE_ITEM, null, 0, 0);
            shop.setPos(xCmd, yCmd, imgButton_dressed, imgButton_dressed2);

            yCmd = yCmd + 30;
            cmdGo = new Command("Gỡ", this, 9, null, 0, 0);
            cmdGo.setPos(xBegin + Image.getWidth(LoadImageInterface.img_use) - widthSubFrame - 13,
                yBegin + heightSubFrame * 2 / 3 - 10, LoadImageInterface.img_use, LoadImageInterface.img_use_focus);
            cmdNangCap = new Command("Nâng cấp", this, 10, null, 0, 0);
            cmdNangCap.setPos(xBegin - widthSubFrame - 21, yBegin + heightSubFrame * 2 / 3 - 10, LoadImageInterface.img_use,
                LoadImageInterface.img_use_focus);

            cmdDuaLen = new Command("Đưa lên", this, 11, null, 0, 0);
            cmdDuaLen.setPos(xBegin - widthSubFrame / 2 - 26 - Image.getWidth(LoadImageInterface.img_use_focus) / 2,
                yBegin + heightSubFrame * 2 / 3 - 10, LoadImageInterface.img_use, LoadImageInterface.img_use_focus);

            xyItemNangCap[0][0] = xBGHuman - 16 + 65 + 25; //xBGHuman-16,yBGHuman
            xyItemNangCap[0][1] = yBGHuman + 46 + 21;

            xyItemNangCap[1][0] = xBGHuman + 10; //top left
            xyItemNangCap[1][1] = yBGHuman + 25;
            xyItemNangCap[2][0] = xBGHuman - 16 + 65 + 25; //xBGHuman-16,yBGHuman
            xyItemNangCap[2][1] = yBGHuman + 25;
            xyItemNangCap[3][0] = xBGHuman + 138; //top right
            xyItemNangCap[3][1] = yBGHuman + 25;
            xyItemNangCap[4][0] = xBGHuman + 138; //xBGHuman-16,yBGHuman
            xyItemNangCap[4][1] = yBGHuman + 68;
            xyItemNangCap[5][0] = xBGHuman + 138; //bottom right
            xyItemNangCap[5][1] = yBGHuman + 114;
            xyItemNangCap[6][0] = xBGHuman + 74; //
            xyItemNangCap[6][1] = yBGHuman + 114;
            xyItemNangCap[7][0] = xBGHuman + 10; //bottom left
            xyItemNangCap[7][1] = yBGHuman + 114;
            xyItemNangCap[8][0] = xBGHuman + 10; //
            xyItemNangCap[8][1] = yBGHuman + 68;
        }

        public void LoadImage()
        {
            for (var i = 0; i < list_eff.Length; i++)
                list_eff[i] = GameCanvas.loadImage("/eff/eff_dapdo_" + (i + 1) + ".png");
            for (var i = 0; i < list_upgrade.Length; i++)
                list_upgrade[i] = GameCanvas.loadImage("/eff/upgrade/upgrade_" + (i + 1) + ".png");
            for (var i = 0; i < list_success.Length; i++)
                list_success[i] = GameCanvas.loadImage("/eff/upgrade/success_" + (i + 1) + ".png");
            for (var i = 0; i < list_fail.Length; i++)
                list_fail[i] = GameCanvas.loadImage("/eff/upgrade/fail_" + (i + 1) + ".png");
            imgEff_dapdo = GameCanvas.loadImage("/eff/nen_eff.png");
            imgBgInfo = GameCanvas.loadImage("/GuiNaruto/imageBGChar/bg_info.png");
            imgButton_dressed = GameCanvas.loadImage("/GuiNaruto/myseft/button_dressed.png");
            imgButton_dressed2 = GameCanvas.loadImage("/GuiNaruto/myseft/button_dressed2.png");
            ;
            imgChien_luc = GameCanvas.loadImage("/GuiNaruto/myseft/chien_luc.png");
            imgCoins1 = GameCanvas.loadImage("/GuiNaruto/myseft/coins1.png");
            imgCoins2 = GameCanvas.loadImage("/GuiNaruto/myseft/coins2.png");
            imgExp_tube = GameCanvas.loadImage("/GuiNaruto/myseft/exp_tube.png");
            imgExp = GameCanvas.loadImage("/GuiNaruto/myseft/exp.png");
            imgLevel = GameCanvas.loadImage("/GuiNaruto/myseft/level.png");
        }

        //Test hieu ung
        public void loadeffect(string type)
        {
            isLevelUp = true;
            if (type == "up")
            {
                if (z < 8)
                    z++;
                for (var i = 0; i < list_lv1.Length; i++)
                    list_lv1[i] = GameCanvas.loadImage("/eff/upgrade/lv" + z + "_" + (i + 1) + ".png");
                if (z >= 6)
                    isThunderEffect = true;
                else
                    isThunderEffect = false;
                isSpecialEffect = true;
                isSuccess = true;
            }
            else
            {
                if (z > 0)
                    z--;
                for (var i = 0; i < list_lv1.Length; i++)
                    list_lv1[i] = GameCanvas.loadImage("/eff/upgrade/lv" + z + "_" + (i + 1) + ".png");
                if (z >= 6)
                    isThunderEffect = true;
                else
                    isThunderEffect = false;
                isFail = true;
            }
        }

        private void PaintBGHuman(int x, int y, MGraphics g)
        {
            g.drawImage(imgBgInfo, x + 13 * 7, y + 70, MGraphics.VCENTER | MGraphics.HCENTER);
        }


        private void PaintItem(int x, int y, MGraphics g)
        {
            if (Char.myChar().arrItemBag == null)
                return;
            scroll_listItem.setStyle(
                Char.myChar().arrItemBag.Length / 5 + (Char.myChar().arrItemBag.Length % 5 != 0 ? 1 : 0), 28, x, y, 140, 56,
                true, 5);
            scroll_listItem.setClip(g, x, y, 140, 56);
            // paint item


            if (Char.myChar().arrItemBag != null)
                for (var i = 0; i < 30; i++)
                {
                    var r = i / 5;
                    var c = i - r * 5;
                    g.drawImage(LoadImageInterface.ImgItem,
                        x + Image.getWidth(LoadImageInterface.ImgItem) * c + 14,
                        y + Image.getHeight(LoadImageInterface.ImgItem) * r + 14 + 2,
                        MGraphics.VCENTER | MGraphics.HCENTER, true);

                    if (i > 29 || i >= Char.myChar().arrItemBag.Length || Char.myChar().arrItemBag[i] == null)
                        break;
                    var it = Char.myChar().arrItemBag[i];
                    r = it.indexUI / 5;
                    c = it.indexUI - r * 5;
                    if (it != null)
                        it.paintItem(g, x + Image.getWidth(LoadImageInterface.ImgItem) * c + 14,
                            y + Image.getHeight(LoadImageInterface.ImgItem) * r + 14 + 2);
                }
            if (scroll_listItem.selectedItem >= 0 && scroll_listItem.selectedItem < Char.myChar().arrItemBag.Length)
            {
                var it = Char.myChar().arrItemBag[scroll_listItem.selectedItem];
                var r = it.indexUI / 5;
                var c = it.indexUI - r * 5;
                Paint.paintFocus(g,
                    x + Image.getWidth(LoadImageInterface.ImgItem) * c + 4,
                    y + Image.getHeight(LoadImageInterface.ImgItem) * r + 4 + 2
                    , LoadImageInterface.ImgItem.GetWidth() - 9, LoadImageInterface.ImgItem.GetWidth() - 9, coutFc);
            }
            GameCanvas.ResetTrans(g);
        }

        public override void paint(MGraphics g)
        {
            if (numberItemInBag > 0)
            {
                PaintBGHuman(xBGHuman - 16, yBGHuman, g);
                if (idSelect > -1 && Focus == INFO)
                    Paint.paintItemInfo(g, itemFocus, xTab - widthSubFrame - 5, yTab + 10);

                if (itemFocus != null && isShowCmd)
                    if (isDuaLen)
                    {
                        cmdDuaLen.paint(g);
                    }
                    else
                    {
                        cmdNangCap.paint(g);
                        if (indexFocusListItemNangCap >= 0)
                            cmdGo.paint(g);
                    }
                paintListItemNangCap(g);
                dropAllItem.paint(g);
                shop.paint(g);
                PaintItem(xBGHuman, yBGHuman + heightBGChar + Image.getHeight(imgChien_luc) + 10, g);
            }
            else
            {
                FontManager.GetInstance().tahoma_7b_white.DrawString(g, "Bạn chưa có trang bị để nâng cấp!", xStart + 4, yStart + 10, 2);
            }
        }

        public override void updatePointer()
        {
            var ismove = false;
            if (GameCanvas.gameTick % 4 == 0)
            {
                coutFc++;
                if (coutFc > 2)
                    coutFc = 0;
            }
            if (imgButton_dressed == null)
                LoadImage();
            for (var i = 0; i < 9; i++)
                if (GameCanvas.isPointerClick || GameCanvas.isPointerJustRelease)
                    if (GameCanvas.isPoint(xyItemNangCap[i][0] - 14, xyItemNangCap[i][1] - 14, 28, 28))
                    {
                        if (i < listItemNangCap.Length && listItemNangCap[i] != null)
                            itemFocus = listItemNangCap[i];
                        isShowCmd = true;
                        indexFocusListItemNangCap = i;

                        scroll_listItem.selectedItem = -1;
                        GameCanvas.clearPointerEvent();
                        break;
                    }

            if (itemFocus != null && !isDuaLen && isShowCmd)
                if (GameCanvas.keyPressed[5] || getCmdPointerLast(cmdNangCap))
                    if (cmdNangCap != null)
                    {
                        GameCanvas.isPointerJustRelease = false;
                        GameCanvas.keyPressed[5] = false;
                        keyTouch = -1;
                        if (cmdNangCap != null)
                            cmdNangCap.performAction();
                    }
            if (cmdDuaLen != null && isDuaLen && isShowCmd)
                if (GameCanvas.keyPressed[5] || getCmdPointerLast(cmdDuaLen))
                    if (cmdDuaLen != null && isDuaLen)
                    {
                        GameCanvas.isPointerJustRelease = false;
                        GameCanvas.keyPressed[5] = false;
                        keyTouch = -1;
                        if (cmdDuaLen != null)
                            cmdDuaLen.performAction();
                    }
            if (cmdGo != null && !isDuaLen && isShowCmd)
                if (GameCanvas.keyPressed[5] || getCmdPointerLast(cmdGo))
                    if (cmdDuaLen != null)
                    {
                        GameCanvas.isPointerJustRelease = false;
                        GameCanvas.keyPressed[5] = false;
                        keyTouch = -1;
                        if (cmdGo != null)
                            cmdGo.performAction();
                    }
        }

        public void setPaintInfo()
        {
            if (itemFocus != Char.myChar().arrItemBag[idSelect])
            {
                itemFocus = Char.myChar().arrItemBag[idSelect];
                isShowCmd = isDuaLen = true;
                if (Item.isHP(itemFocus.template.type) || Item.isBlood(itemFocus.template.type)
                    || Item.isMP(itemFocus.template.type))
                {
                    isShowCmd = isDuaLen = false;
                }
                else if (Item.isItemDapDo(itemFocus.template.type))
                {
                }
                else
                {
                    if (listItemNangCap[0] != null)
                    {
                        isDuaLen = false;
                        if (itemFocus == listItemNangCap[0])
                            isShowCmd = true;
                        else
                            isShowCmd = false;
                    }
                    else
                    {
                        isShowCmd = isDuaLen = true;
                    }
                }
            }
        }

        public override void perform(int idAction, object p)
        {
            switch (idAction)
            {
                case 9: //go xuong

                    if (indexFocusListItemNangCap >= 0)
                    {
                        Service.GetInstance().NangCap_ThaoXuong((sbyte) indexFocusListItemNangCap);
                        indexFocusListItemNangCap = -1;
                    }
                    break;
                case 10:
                    if (itemFocus != null && idSelect > -1 && idSelect < Char.myChar().arrItemBag.Length)
                    {
                        isLevelUp = true;
                        Service.GetInstance().NangCap_NangCap();
                        for (var i = 1; i < listItemNangCap.Length; i++)
                            listItemNangCap[i] = null;
                    }
                    break;
                case 99:
                    loadeffect("up");
                    break;
                case 100:
                    loadeffect("down");
                    break;
                case 11:

                    var indexNull = 9;
                    var isExit = false;
                    if (listItemNangCap[0] == null && Item.isItemDapDo(itemFocus.template.type))
                    {
                        GameCanvas.StartOkDlg("Bạn cần bỏ đồ cần nâng cấp lên trước !!");
                        return;
                    }
                    var itemNangCap = listItemNangCap[0] == null ? 0 : 1;
                    for (var i = itemNangCap; i < listItemNangCap.Length; i++)
                    {
                        if (listItemNangCap[i] == null && i < indexNull)
                            indexNull = i;
                        if (itemFocus == listItemNangCap[i])
                        {
                            listItemNangCap[i] = null;
                            isExit = true;
                            break;
                        }
                    }
                    if (!isExit && indexNull < listItemNangCap.Length)
                        for (var i = 0; i < Char.myChar().arrItemBag.Length; i++)
                            if (Char.myChar().arrItemBag[i] != null && Char.myChar().arrItemBag[i] == itemFocus)
                            {
                                Service.GetInstance().NangCap_Dualen((sbyte) i);
                                isDuaLen = false;
                                break;
                            }
                    break;
            }
            base.perform(idAction, p);
        }

        public void paintListItemNangCap(MGraphics g)
        {
            for (var i = 0; i < 9; i++)
            {
                g.drawImage(LoadImageInterface.ImgItem, xyItemNangCap[i][0], xyItemNangCap[i][1],
                    MGraphics.VCENTER | MGraphics.HCENTER, true);
                //Bi Null
                if (i < listItemNangCap.Length && listItemNangCap[i] != null)
                    listItemNangCap[i].paintItem(g, xyItemNangCap[i][0], xyItemNangCap[i][1], false);
                if (indexFocusListItemNangCap == i)
                    Paint.paintFocus(g,
                        xyItemNangCap[i][0] - 10, xyItemNangCap[i][1] - 10
                        , LoadImageInterface.ImgItem.GetWidth() - 9, LoadImageInterface.ImgItem.GetWidth() - 9, coutFc);
            }
            var x = xBGHuman - 16;
            var y = yBGHuman;
            if (listItemNangCap[0] != null && idSelect > -1 && idSelect < Char.myChar().arrItemBag.Length)
            {
                g.drawRegionRotate(imgEff_dapdo, 0, 0, imgEff_dapdo.GetWidth(), imgEff_dapdo.GetHeight(), 0,
                    x + 13 * 7, y + 70, MGraphics.VCENTER | MGraphics.HCENTER, GameCanvas.gameTick * 3 % 360);
                if (indexFocusListItemNangCap == 0)
                    Paint.paintFocus(g,
                        xyItemNangCap[0][0] - 10, xyItemNangCap[0][1] - 10
                        , LoadImageInterface.ImgItem.GetWidth() - 9, LoadImageInterface.ImgItem.GetWidth() - 9, coutFc);

                if (GameCanvas.gameTick % 3 != 0 && isThunderEffect)
                    g.drawImage(list_eff[GameCanvas.gameTick / 2 % list_eff.Length], x + 13 * 7, y + 65,
                        MGraphics.VCENTER | MGraphics.HCENTER);
                if (GameCanvas.gameTick % 4 != 0 && isSpecialEffect)
                    g.drawImage(list_lv1[GameCanvas.gameTick / 2 % list_lv1.Length], x + 13 * 7, y + 65,
                        MGraphics.VCENTER | MGraphics.HCENTER);

                if (GameCanvas.gameTick % 4 == 0 && isSpecialEffect)
                    g.drawImage(list_lv1[GameCanvas.gameTick / 2 % list_lv1.Length], x + 13 * 7, y + 65,
                        MGraphics.VCENTER | MGraphics.HCENTER);


                if (listItemNangCap[0] != null)
                    listItemNangCap[0].paintItem(g, xyItemNangCap[0][0], xyItemNangCap[0][1], false);

                if (GameCanvas.gameTick % 3 == 0 && isThunderEffect)
                    g.drawImage(list_eff[GameCanvas.gameTick / 2 % list_eff.Length], x + 13 * 7, y + 65,
                        MGraphics.VCENTER | MGraphics.HCENTER);
            }
            if (GameCanvas.gameTick % 2 != 0 && isLevelUp)
                g.drawImage(list_upgrade[tick], x + 13 * 7, y + 50, MGraphics.VCENTER | MGraphics.HCENTER);
            if (GameCanvas.gameTick % 2 == 0 && isLevelUp)
            {
                g.drawImage(list_upgrade[tick], x + 13 * 7, y + 50, MGraphics.VCENTER | MGraphics.HCENTER);
                tick++;
                if (tick >= 15)
                {
                    isLevelUp = false;
                    tick = 0;
                }
            }
            //Hieu ung thanh cong
            if (GameCanvas.gameTick % 2 != 0 && isSuccess)
                g.drawImage(list_success[tick2], x + 13 * 7, y + 10, MGraphics.VCENTER | MGraphics.HCENTER);
            if (GameCanvas.gameTick % 2 == 0 && isSuccess)
            {
                g.drawImage(list_success[tick2], x + 13 * 7, y + 10, MGraphics.VCENTER | MGraphics.HCENTER);
                tick2++;
                if (tick2 >= 18)
                {
                    isSuccess = false;
                    tick2 = 0;
                }
            }
            //Hieu ung thất bại
            if (GameCanvas.gameTick % 2 != 0 && isFail)
                g.drawImage(list_fail[tick3], x + 13 * 7, y + 10, MGraphics.VCENTER | MGraphics.HCENTER);
            if (GameCanvas.gameTick % 2 == 0 && isFail)
            {
                g.drawImage(list_fail[tick3], x + 13 * 7, y + 10, MGraphics.VCENTER | MGraphics.HCENTER);
                tick3++;
                if (tick3 >= 18)
                {
                    isFail = false;
                    tick3 = 0;
                }
            }
        }

        //@Override
        public override void updateKey()
        {
            // TODO Auto-generated method stub
            base.updateKey();

            var s1 = scroll_listItem.updateKey();
            scroll_listItem.updatecm();

            if (Char.myChar().arrItemBag != null && scroll_listItem.selectedItem > -1 &&
                scroll_listItem.selectedItem < Char.myChar().arrItemBag.Length)
            {
                indexFocusListItemNangCap = -1;
                idSelect = scroll_listItem.selectedItem;
                setPaintInfo();
            }
            for (var i = 1; i < listItemNangCap.Length; i++)
                if (listItemNangCap[i] != null && GameCanvas.gameTick / 2 % CRes.random(4, 8) == 0)
                    GameScr.AddEffectEnd(EffectKill.EFF_CUONG_HOA,
                        xyItemNangCap[i][0], xyItemNangCap[i][1],
                        xyItemNangCap[xyItemNangCap.Length - 1][0], xyItemNangCap[xyItemNangCap.Length - 1][1]);
        }

        public void updateListItemNangCap(short[] listId)
        {
            for (var i = 0; i < listItemNangCap.Length; i++)
                listItemNangCap[i] = null;
            for (var i = 0; i < listId.Length; i++)
                if (listId[i] != -1)
                {
                    var itUpgradeList = new Item();
                    itUpgradeList.template = ItemTemplates.Get(listId[i]);
                    listItemNangCap[i] = itUpgradeList;
                }
            changeListEffByLv(listItemNangCap[0].template.lvItem);
        }

        //Hàm xử lý lại mảng eff level item
        public void changeListEffByLv(int lv)
        {
            switch (lv)
            {
                case 1:
                    lv = 0;
                    isThunderEffect = false;
                    isSpecialEffect = false;
                    break;
                case 2:
                    lv = 1;
                    isThunderEffect = false;
                    isSpecialEffect = true;
                    break;
                case 3:
                    lv = 2;
                    isThunderEffect = false;
                    isSpecialEffect = true;
                    break;
                case 4:
                    lv = 3;
                    isThunderEffect = false;
                    isSpecialEffect = true;
                    break;
                case 5:
                    lv = 4;
                    isThunderEffect = false;
                    isSpecialEffect = true;
                    break;
                case 6:
                    lv = 5;
                    isThunderEffect = true;
                    isSpecialEffect = true;
                    break;
                case 7:
                    lv = 6;
                    isThunderEffect = true;
                    isSpecialEffect = true;
                    break;
                case 8:
                    lv = 7;
                    isThunderEffect = true;
                    isSpecialEffect = true;
                    break;
            }
            if (lv > 0)
                for (var i = 0; i < list_lv1.Length; i++)
                    list_lv1[i] = GameCanvas.loadImage("/eff/upgrade/lv" + lv + "_" + (i + 1) + ".png");
        }
    }
}