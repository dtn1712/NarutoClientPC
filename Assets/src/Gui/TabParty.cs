using System;
using src.lib;
using src.main;
using src.model;
using src.real;
using src.screen;
using UnityEngine;
using Char = src.Objectgame.Char;

namespace src.Gui
{
    public class TabParty : TScreen, IActionListener
    {
        public static Scroll scrMain = new Scroll();

        public Command kich, giaitan, roi;

        public int popupY,
            popupX,
            indexRow = -1,
            widthGui = 235,
            heightGui = 240,
            indexRowMax;

        private int x, y;
        public int xstart, ystart, popupW = 163;

        public TabParty(int x, int y)
        {
            this.x = GameCanvas.w / 2 - widthGui / 2; //GameCanvas.w/2-GameScr.widthGui/2;
            this.y = y;
            popupX = x - widthGui / 2;
            popupY = GameCanvas.h / 2 - heightGui / 2;
        }

        public void perform(int idAction, object p)
        {
            // TODO Auto-generated method stub
            switch (idAction)
            {
                case 111111: // giai tan
                    Service.GetInstance().removeParty(PartyType.DISBAND_PARTY);
                    break;
                case 270192: // kich ra khoi nhom
                    var party = (Party) GameScr.hParty.get(Char.myChar().idParty + "");
                    if (party == null)
                        return;
                    if (((Char) Party.vCharinParty.elementAt(0)).charID == Char.myChar().charID)
                        if (indexRow < Party.vCharinParty.size())
                        {
                            var c = (Char) Party.vCharinParty.elementAt(indexRow);
                            if (c.charID != Char.myChar().charID)
                                Service.GetInstance().kickPlayeLeaveParty(PartyType.KICK_OUT_PARTY, (short) c.charID);
                        }
                    break;
                case 231291: // tu roi nhom
                    Service.GetInstance().leaveParty(PartyType.OUT_PARTY, (short) Char.myChar().charID);
                    break;
            }
        }

        public override void updateKey()
        {
            // TODO Auto-generated method stub
            if (kich != null && Party.gI().isLeader && Party.vCharinParty.size() >= 2)
                if (getCmdPointerLast(kich))
                    kich.performAction();
            if (giaitan != null && Party.gI().isLeader && Party.vCharinParty.size() >= 2)
                if (getCmdPointerLast(giaitan))
                    giaitan.performAction();
            if (roi != null && Party.vCharinParty.size() >= 2)
                if (getCmdPointerLast(roi))
                    roi.performAction();

            var s1 = scrMain.updateKey();
            base.updateKey();
        }

        public override void update()
        {
            if (GameCanvas.keyPressed[Key.NUM8])
            {
                indexRow++;
                if (indexRow >= GameScr.hParty.size())
                    indexRow = GameScr.hParty.size() - 1;
                scrMain.moveTo(indexRow * scrMain.ITEM_SIZE);
            }
            else if (GameCanvas.keyPressed[Key.NUM2])
            {
                indexRow--;
                if (indexRow < 0)
                    indexRow = 0;
                scrMain.moveTo(indexRow * scrMain.ITEM_SIZE);
            }
            scrMain.updatecm();

            if (GameScr.hParty.size() == 0 && scrMain.selectedItem != -1)
            {
                indexRow = scrMain.selectedItem;
                scrMain.selectedItem = -1;
            }
            else if (GameScr.hParty.size() != 0 && scrMain.selectedItem != -1)
            {
                indexRow = scrMain.selectedItem;
                scrMain.selectedItem = -1;
                setPartyCommand();
            }
            base.update();
        }

        private void setPartyCommand()
        {
            var party = (Party) GameScr.hParty.get(Char.myChar().idParty + "");
            if (party == null)
                return;
            if (indexRow == -1)
                return;

            if (Char.myChar().charID == Party.idleader)
            {
                var c = (Char) Party.vCharinParty.elementAt(indexRow);
                if (c != null)
                {
                    kich = new Command("Kích", this, 270192, null, 0, 0);
                    kich.setPos(popupX + popupW + LoadImageInterface.btnTab.GetWidth() - 10,
                        popupY + LoadImageInterface.btnTab.GetHeight() * 2, LoadImageInterface.btnTab,
                        LoadImageInterface.btnTab);
                    giaitan = new Command("Giải tán", this, 111111, null, 0, 0);
                    giaitan.setPos(popupX + popupW + LoadImageInterface.btnTab.GetWidth() - 10,
                        popupY + LoadImageInterface.btnTab.GetHeight() * 2 + cmdH * 4,
                        LoadImageInterface.btnTab, LoadImageInterface.btnTab);
                    roi = new Command("Rời", this, 231291, null, 0, 0);
                    roi.setPos(popupX + popupW + LoadImageInterface.btnTab.GetWidth() - 10,
                        popupY + LoadImageInterface.btnTab.GetHeight() * 2 + cmdH * 2,
                        LoadImageInterface.btnTab, LoadImageInterface.btnTab);
                    giaitan.img = roi.img = kich.img = LoadImageInterface.btnTab;
                    giaitan.imgFocus = roi.imgFocus = kich.imgFocus = LoadImageInterface.btnTabFocus;
                    giaitan.w = roi.w = kich.w = Image.getWidth(LoadImageInterface.btnTab);
                    giaitan.h = roi.h = kich.h = Image.getHeight(LoadImageInterface.btnTabFocus);
                }
            }
            else
            {
                kich = new Command("Kích", this, 270192, null, 0, 0);
                kich.setPos(popupX + popupW + LoadImageInterface.btnTab.GetWidth() - 10,
                    popupY + LoadImageInterface.btnTab.GetHeight() * 2, LoadImageInterface.btnTab,
                    LoadImageInterface.btnTab);
                giaitan = new Command("Giải tán", this, 111111, null, 0, 0);
                giaitan.setPos(popupX + popupW + LoadImageInterface.btnTab.GetWidth() - 10,
                    popupY + LoadImageInterface.btnTab.GetHeight() * 2 + cmdH * 4, LoadImageInterface.btnTab,
                    LoadImageInterface.btnTab);
                roi = new Command("Rời", this, 231291, null, 0, 0);
                roi.setPos(popupX + popupW + LoadImageInterface.btnTab.GetWidth() - 10,
                    popupY + LoadImageInterface.btnTab.GetHeight() * 2 + cmdH * 2, LoadImageInterface.btnTab,
                    LoadImageInterface.btnTab);
                giaitan.img = roi.img = kich.img = LoadImageInterface.btnTab;
                giaitan.imgFocus = roi.imgFocus = kich.imgFocus = LoadImageInterface.btnTabFocus;
                giaitan.w = roi.w = kich.w = Image.getWidth(LoadImageInterface.btnTab);
                giaitan.h = roi.h = kich.h = Image.getHeight(LoadImageInterface.btnTabFocus);
            }
        }

        public override void paint(MGraphics g)
        {

            g.setColor(0x000000, GameCanvas.opacityTab);
            g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
            g.disableBlending();
            Paint.paintFrameNaruto(popupX, popupY, widthGui, heightGui, g, true);

            xstart = popupX + 5;
            ystart = popupY + 40;

            if (GameScr.hParty.size() == 0)
            {
                FontManager.GetInstance().tahoma_7_white.DrawString(g, MResources.NOT_TEAM, popupX + widthGui / 2, popupY + 40, MFont.CENTER);
            }
            else
            {
                resetTranslate(g);
                scrMain.setStyle(Party.vCharinParty.size(), 50, xstart, ystart, widthGui - 3, heightGui - 50, true, 1);
                scrMain.setClip(g, xstart, ystart, widthGui - 3, heightGui - 50);
                var yBgFriend = 0;
                indexRowMax = Party.vCharinParty.size();
                for (var i = 0; i < Party.vCharinParty.size(); i++)
                {
                    var c = (Char) Party.vCharinParty.elementAt(i);

                    Paint.PaintBGListQuest(xstart + 35, ystart + yBgFriend, 160, g); //new quest	
                    if (indexRow == i)
                        Paint.PaintBGListQuestFocus(xstart + 35, ystart + yBgFriend, 160, g);
                    g.drawImage(LoadImageInterface.charPic, xstart + 45, ystart + yBgFriend + 20,
                        MGraphics.VCENTER | MGraphics.HCENTER);
                    FontManager.GetInstance().tahoma_7_white.DrawString(g, "Level: " + c.clevel, xstart + 73, ystart + yBgFriend + 24, 0);
                    if (c.isOnline)
                    {
                        g.drawImage(LoadImageInterface.imgName, xstart + 100, ystart + yBgFriend + 15,
                            MGraphics.VCENTER | MGraphics.HCENTER, true);
                        FontManager.GetInstance().tahoma_7_white.DrawString(g, c.cName, xstart + 73, ystart + yBgFriend + 8, 0);
                    }

                    else
                    {
                        g.drawImage(LoadImageInterface.imgName, xstart + 100, ystart + yBgFriend + 15,
                            MGraphics.VCENTER | MGraphics.HCENTER, true);
                        FontManager.GetInstance().tahoma_7_white.DrawString(g, c.cName, xstart + 73, ystart + yBgFriend + 8, 0);
                    }

                    if (c != null)
                    {
                        if (c.isLeader)
                            FontManager.GetInstance().tahoma_7_yellow.DrawString(g, c.cName, xstart + 73, ystart + yBgFriend + 8, 0);
                        try
                        {
                            var ph2 = GameScr.parts[c.head];
                            SmallImage.DrawSmallImage(g, ph2.pi[Char.CharInfo[0][0][0]].id,
                                xstart + 44, ystart + yBgFriend + 20, 0, MGraphics.VCENTER | MGraphics.HCENTER);
                        }
                        catch (Exception e)
                        {
                            Debug.LogError(e);
                        }
                    }

                    yBgFriend += 50;
                }
            }

            GameCanvas.ResetTrans(g);
            if (indexRow != -1 && Party.vCharinParty.size() >= 2)
            {
                if (Party.gI().isLeader)
                {
                    kich?.paint(g);
                    giaitan?.paint(g);
                }
                roi?.paint(g);
            }


            //paint name box 
            Paint.PaintBoxName("DANH SÁCH NHÓM", popupX + widthGui / 2 - 50, popupY + 10, 100, g);
            base.paint(g);
        }

        public void SetPosClose(Command cmdClose)
        {
            scrMain.selectedItem = -1;
            cmdClose.setPos(popupX + widthGui - Image.getWidth(LoadImageInterface.closeTab), popupY,
                LoadImageInterface.closeTab, LoadImageInterface.closeTab);
        }
    }
}