using src.lib;
using src.model;

public class FatherChat : IActionListener
{
    public static int popw = 158, poph = 230;
    public Command btnChat, btnIconChat;
    public IconChat iconChat;
    public int indexSize = 28;
    public int popx = 75, popy = 54, indexRow = -1;
    public Scroll scrMain = new Scroll();
    public TField tfChar;

    public virtual void perform(int idAction, object p)
    {
        // TODO Auto-generated method stub
    }

    public virtual void ActionPerformSend()
    {
    }

    public virtual void ActionPerformIconChat()
    {
    }

    public virtual void Update()
    {
    }

    public virtual void UpdateKey()
    {
    }

    public virtual void KeyPress(int keyCode)
    {
    }


    public virtual void paintContentChatWorld(MGraphics g)
    {
    }
}