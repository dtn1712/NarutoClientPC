/*
 * this equip
 */

using System;
using src.lib;
using src.main;
using src.Objectgame;
using src.real;
using src.screen;
using UnityEngine;
using Char = src.Objectgame.Char;

namespace src.Gui
{
    public class TabMySeftNew : MainTabNew
    {
        public static int maxSize = 12;

        private readonly MBitmap[] arrayBGHuman = new MBitmap[25];

        private readonly Command dropAllItem;
        private readonly Command shop;
        private readonly Command improve;
        private readonly Command cmdGo;
        private int h12, w5;
        private readonly int heightBGChar = 100;

        private sbyte idSelect;

        private MBitmap imgButton_dressed,
            imgButton_dressed2,
            imgChien_luc,
            imgCoins1,
            imgCoins2,
            imgExp_tube,
            imgExp,
            imgLevel;

        private readonly bool isList = false;
        private bool isShowInfo = false;

        private Item itemFocus;
        private int maxList, selectList, xList, yList, timeUpdateInfo;

        private int[] mColorInfo;
        private int numW, numH, coutFc;


        private readonly int widthBGChar = 130;

        private readonly int wsize;

        private readonly int xBGHuman = gI().xTab + gI().widthFrame / 6 - 10;

        // info
        private readonly int xStart;

        private readonly int yStart;

        private readonly int yBGHuman = gI().yTab + GameCanvas.h / 6 - 10;

        public TabMySeftNew(string nametab)
        {
            xBGHuman = xTab + 16;
//		    LoadImage();
            if (GameCanvas.isTouch)
                idSelect = -1;
            else
                idSelect = 0;
            xStart = xBegin + wblack / 2 - numW * wOneItem / 2 + numW / 2;
            yStart = yBegin + 10;
            wsize = wOneItem;
            typeTab = EQUIP;
            yBGHuman = yTab + 15;
            nameTab = nametab;
            var xCmd = xBGHuman + widthBGChar + 32;
            var yCmd = yBGHuman + 27;

            xBegin = xTab + wOneItem / 2 + 4;
            yBegin = yTab + 30 + wOneItem - 5;
            dropAllItem = new Command("", this, Constants.BTN_USE_ITEM, null, 0, 0);
            dropAllItem.setPos(xCmd, yCmd, imgButton_dressed, imgButton_dressed2);

            yCmd = yCmd + 30;
            shop = new Command("", this, Constants.BTN_USE_ITEM, null, 0, 0);
            shop.setPos(xCmd, yCmd, imgButton_dressed, imgButton_dressed2);

            yCmd = yCmd + 30;
            improve = new Command("", this, Constants.BTN_USE_ITEM, null, 0, 0);
            improve.setPos(xCmd, yCmd, imgButton_dressed, imgButton_dressed2);
            cmdGo = new Command("Th�o", this, 10, null, 0, 0);
            cmdGo.setPos(xBegin - widthSubFrame / 2 - 26 - Image.getWidth(LoadImageInterface.img_use_focus) / 2,
                yBegin + heightSubFrame * 2 / 3 - 14, LoadImageInterface.img_use, LoadImageInterface.img_use_focus);
        }

        private void LoadImage()
        {
            for (var i = 0; i < 25; i++)
                arrayBGHuman[i] = GameCanvas.loadImage("/GuiNaruto/imageBGChar/bgCharacter_01_" + (i + 1) + ".png");
            imgButton_dressed = GameCanvas.loadImage("/GuiNaruto/myseft/button_dressed.png");
            imgButton_dressed2 = GameCanvas.loadImage("/GuiNaruto/myseft/button_dressed2.png");
            ;
            imgChien_luc = GameCanvas.loadImage("/GuiNaruto/myseft/chien_luc.png");
            imgCoins1 = GameCanvas.loadImage("/GuiNaruto/myseft/coins1.png");
            imgCoins2 = GameCanvas.loadImage("/GuiNaruto/myseft/coins2.png");
            imgExp_tube = GameCanvas.loadImage("/GuiNaruto/myseft/exp_tube.png");
            imgExp = GameCanvas.loadImage("/GuiNaruto/myseft/exp.png");
            imgLevel = GameCanvas.loadImage("/GuiNaruto/myseft/level.png");
        }

        //paint bg human
        private void PaintBgHuman(int x, int y, MGraphics g)
        {
            var wImage = Image.getWidth(arrayBGHuman[0]);
            var hImage = Image.getHeight(arrayBGHuman[0]);
            var index = 0;
            for (var i = 1; i < 6; i++)
            for (var j = 1; j < 6; j++)
            {
                g.drawImage(arrayBGHuman[index], x + j * wImage, y + i * hImage, 0);
                index++;
            }
        }

        //paint exp
        private void PaintExp(int x, int y, MGraphics g)
        {
            var Exppaint = (int) (Char.myChar().cEXP * MGraphics.getImageHeight(imgExp) / Char.myChar().cMaxEXP);
            Exppaint = Exppaint < 7 && Exppaint != 0 ? 7 : Exppaint;
            g.drawRegion(imgExp, MGraphics.getImageHeight(imgExp) - Exppaint, 0, Image.getWidth(imgExp),
                Exppaint,
                0, x + 3, y + 2 + (MGraphics.getImageHeight(imgExp) - Exppaint), MGraphics.TOP | MGraphics.LEFT);
            g.drawImage(imgExp_tube, x, y, 0);
            g.drawImage(imgLevel, x + Image.getWidth(imgExp) / 2 + 2, y + Image.getHeight(imgExp) + 10,
                MGraphics.VCENTER | MGraphics.HCENTER);
            FontManager.GetInstance().tahoma_7b_white.DrawString(g, Char.myChar().clevel + "",
                x + Image.getWidth(imgExp) / 2 - 1,
                y + Image.getHeight(imgExp) + 5, MGraphics.VCENTER | MGraphics.HCENTER);
        }

        private void PaintItem(int x, int y, MGraphics g)
        {
            for (var i = 0; i < 2; i++)
            for (var j = 0; j < 5; j++)
                g.drawImage(LoadImageInterface.ImgItem, x + Image.getWidth(LoadImageInterface.ImgItem) * j,
                    y + Image.getHeight(LoadImageInterface.ImgItem) * i, 0);
        }

        private void PaintStrong(MGraphics g, int x, int y)
        {
            g.drawImage(imgChien_luc, x, y, MGraphics.VCENTER | MGraphics.HCENTER);
            FontManager.GetInstance().tahoma_7b_white.DrawString(g,
                "" + (Char.myChar().diemTN[1] + Char.myChar().diemTN[2]) / 2, x, y - 2,
                2);
        }

        public override void paint(MGraphics g)
        {
            PaintBgHuman(xBGHuman - 16, yBGHuman, g);
            PaintExp(xBGHuman - 6, yBGHuman + 22, g); //paint exp
            PaintStrong(g, xBGHuman + 10 + widthBGChar / 2, yBGHuman + 20 + heightBGChar + 3);
            PaintItem(xBGHuman, yBGHuman + heightBGChar + Image.getHeight(imgChien_luc) + 10, g);

            try
            {
                for (byte i = 0; i < 10; i++)
                {
                    // paint 12 item trang bi
                    int xp = xBGHuman + i % 5 * 24 + i % 5 * 4 + 1,
                        yp = yBGHuman + heightBGChar + Image.getHeight(imgChien_luc) + 10 + i / 5
                             * 28 + 1;

                    if (Char.myChar().arrItemBody != null && i < Char.myChar().arrItemBody.Length)
                    {
                        var item = Char.myChar().arrItemBody[i];
                        if (item?.template.id > -1)
                            item.paintItem(g, xp + wOneItem / 2 + 1, yp + wOneItem / 2 + 1);
                    }
                    if (idSelect <= -1 || Focus != INFO || i != idSelect) continue;

                    SetPaintInfo();
                    if (itemFocus != null)
                        Paint.paintFocus(g,
                            xBGHuman + idSelect % 5 * Image.getWidth(LoadImageInterface.ImgItem) + 4,
                            yBGHuman + heightBGChar + Image.getHeight(imgChien_luc) + 12
                            + idSelect / 5 * Image.getHeight(LoadImageInterface.ImgItem) + 2
                            , LoadImageInterface.ImgItem.GetWidth() - 9, LoadImageInterface.ImgItem.GetWidth() - 9,
                            coutFc);
                    Paint.paintItemInfo(g, itemFocus, xTab - widthSubFrame - 5, yTab + 10);
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }

            if (itemFocus != null && itemFocus.template.type != Item.TYPE_AO)
                cmdGo.paint(g);
            GameScr.currentCharViewInfo.paintChar(g, xBGHuman + 74, yBGHuman + 120);

            dropAllItem.paint(g);
            shop.paint(g);
            improve.paint(g);
        }

        public override void updatePointer()
        {
            var ismove = false;
            if (GameCanvas.gameTick % 4 == 0)
            {
                coutFc++;
                if (coutFc > 2)
                    coutFc = 0;
            }

            if (imgButton_dressed == null)
                LoadImage();
            if (isList)
            {
                if (listContent != null)
                    if (GameCanvas.isPoint(listContent.x, listContent.y,
                        listContent.maxW, listContent.maxH))
                    {
                        listContent.update_Pos_UP_DOWN();
                        ismove = true;
                    }
                if (GameCanvas.isPointerClick /*&& !ismove*/)
                    if (GameCanvas.isPoint(xList, yList, wsize * maxList,
                        wOneItem))
                    {
                        var select = (sbyte) ((
                                                  +GameCanvas.px - xList) / wsize);

                        GameCanvas.isPointerClick = false;
                    }
                    else if (!GameCanvas.isPoint(0, GameCanvas.h
                                                    - GameCanvas.hCommand, GameCanvas.w,
                        GameCanvas.hCommand))
                    {
                        GameCanvas.isPointerClick = false;
                    }
            }
            else
            {
                if (listContent != null)
                    if (GameCanvas.isPoint(listContent.x, listContent.y,
                        listContent.maxW, listContent.maxH))
                    {
                        listContent.update_Pos_UP_DOWN();
                        ismove = true;
                    }
                if (GameCanvas.IsPointSelect(xStart, yStart, wsize * numW,
                        wsize * numH)
                    && !ismove)
                {
                    GameCanvas.isPointerClick = false;
                    var select = (sbyte) ((GameCanvas.px - xStart) / wsize + (GameCanvas.py - yStart) / wsize
                                          * numW);
                    if (select >= 0 && select < maxSize)
                    {
                        if (select == idSelect)
                        {
                            SetPaintInfo();
                        }
                        else
                        {
                            idSelect = select;
                            timePaintInfo = 0;
                        }
                        listContent = null;
                        Focus = INFO;
                    }
                }
            }
            var x = xBGHuman;
            var y = yBGHuman + heightBGChar + Image.getHeight(imgChien_luc) + 10;
            var w = Image.getWidth(LoadImageInterface.ImgItem) * 7;
            var h = Image.getHeight(LoadImageInterface.ImgItem) * 2;

            if (GameCanvas.IsPointSelect(x, y, w, h))
            {
                //khoang cach 2 pixel giua cac o
                var row = (GameCanvas.px - x) / Image.getWidth(LoadImageInterface.ImgItem);
                var col = (GameCanvas.py - y) / Image.getHeight(LoadImageInterface.ImgItem);

                row = (GameCanvas.px - x - (row - 1) * 2) / Image.getWidth(LoadImageInterface.ImgItem);
                col = (GameCanvas.py - y - (col - 1) * 2) / Image.getHeight(LoadImageInterface.ImgItem);

                if (row > 6)
                    row = 6;
                if (col > 1)
                    col = 1;

                idSelect = (sbyte) (row + col * 5);
                GameCanvas.clearKeyPressed();
                GameCanvas.clearKeyHold();
            }
            if (itemFocus != null && itemFocus.template.type != Item.TYPE_AO)
                if (getCmdPointerLast(cmdGo))
                    if (cmdGo != null)
                    {
                        GameCanvas.isPointerJustRelease = false;
                        GameCanvas.keyPressed[5] = false;
                        keyTouch = -1;
                        if (cmdGo != null)
                            cmdGo.performAction();
                    }
        }

        private void SetPaintInfo()
        {
            itemFocus = Char.myChar().arrItemBody[idSelect];
        }

        public override void perform(int idAction, object p)
        {
            switch (idAction)
            {
                case 10:
                    if (itemFocus != null && idSelect > -1 && idSelect < Char.myChar().arrItemBody.Length)
                        Service.GetInstance().TakeOffItem((sbyte) Char.myChar().arrItemBody[idSelect].itemId);
                    break;
            }
        }
    }
}