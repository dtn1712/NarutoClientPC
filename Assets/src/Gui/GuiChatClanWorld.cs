using src;
using src.Gui;
using src.lib;
using src.main;
using src.model;
using src.screen;

public class GuiChatClanWorld : IActionListener
{
    public const sbyte CHAT_WORLD = 0;
    public const sbyte CHAT_CLAN = 1;
    public static int xMove;
    public Command bntOpen;
    private readonly int distanceTab = 5; //distance between 2 tab

    private readonly FatherChat[] fatherChat;
    private readonly int heightTab = 25;
    private readonly int limMove = FatherChat.popw - 10;
    private readonly string[] listNameTab;
    public bool moveClose, moveOpen;
    private readonly int nTab;
    private int selectTab;
    private int v; //gia toc tang toc do move
    private readonly int widthTab = 30;

    public int x, y;

    public int xBtnMove = FatherChat.popw + 10;
    private readonly int yButtonTab = GameCanvas.h - 50; //y of button tab

    public GuiChatClanWorld(int x, int y, string[]arrayName)
    {
        this.x = x;
        this.y = y;
        nTab = arrayName.Length;
        listNameTab = arrayName;
        fatherChat = new FatherChat[arrayName.Length];
        fatherChat[0] = new GuiChatWorld(this.x, this.y);
        fatherChat[1] = new GuiChatClan(this.x, this.y);

        bntOpen = new Command("", this, Constants.BUTTON_OPEN, null, 0, 0);
        bntOpen.setPos(0, y + 130 / 2 - Image.getHeight(LoadImageInterface.imgShortQuest) / 2 + 25,
            LoadImageInterface.imgShortQuest, LoadImageInterface.imgShortQuest);
    }

    public void perform(int idAction, object p)
    {
        // TODO Auto-generated method stub
        fatherChat[selectTab].perform(idAction, p);
        switch (idAction)
        {
            case Constants.BUTTON_OPEN:
                if (!moveClose)
                {
                    GameScr.GetInstance().guiMain.menuIcon.indexpICon =
                        Constants.BUTTON_OPEN; //truong hop mo chat ko upda menuicon

                    moveClose = true;
                    bntOpen.setPos(x + xBtnMove - Image.getWidth(LoadImageInterface.imgShortQuest) + 35,
                        y + 130 / 2 - Image.getHeight(LoadImageInterface.imgShortQuest) / 2 + 25,
                        LoadImageInterface.imgShortQuest_Close, LoadImageInterface.imgShortQuest_Close);
                }
                else
                {
                    if (fatherChat[selectTab].iconChat != null) return;
                    GameScr.GetInstance().guiMain.menuIcon.indexpICon = 0; //truong hop dong chat ko upda menuicon

                    moveClose = false;
                    bntOpen.setPos(x + xBtnMove - Image.getWidth(LoadImageInterface.imgShortQuest) + 35,
                        y + 130 / 2 - Image.getHeight(LoadImageInterface.imgShortQuest) / 2 + 25,
                        LoadImageInterface.imgShortQuest, LoadImageInterface.imgShortQuest);
                }
                break;
        }
    }

    public void Update()
    {
        fatherChat[selectTab].Update();

        if (moveClose) //move to right
        {
            if (xMove > limMove)
            {
//				xMove=limMove;
                v = 0;
            }
            else
            {
                xMove += v;
                v++;
            }
        }
        else //move to left
        {
            if (xMove <= -30)
            {
                v = 0;
                xMove = Image.getWidth(LoadImageInterface.imgShortQuest) - x - xBtnMove - 35;
            }
            else
            {
                xMove += v;
                v--;
            }
        }
        if (v != 0)
            Move();
    }

    public void UpdateKey()
    {
        if (moveClose)
            fatherChat[selectTab].UpdateKey();
        if ( /*GameCanvas.keyPressed[5] ||*/ TScreen.getCmdPointerLast(bntOpen))
            if (bntOpen != null)
            {
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                TScreen.keyTouch = -1;
                if (bntOpen != null)
                    bntOpen.performAction();
            }
    }

    public void KeyPress(int keyCode)
    {
        fatherChat[selectTab].KeyPress(keyCode);
    }

    public void Close()
    {
    }

    /*
     * Paint background tab  working and done
     */
    public void paintTabBig(MGraphics g, int x, int y, int w, int h)
    {
//		g.setColor(0x584848);
        g.drawImage(LoadImageInterface.imgChatButton, x, y, 0);
    }

    /*
     * Paint background tab is selected
     */
    public void paintTabFocus(MGraphics g, int x, int y, int w, int h)
    {
        g.drawImage(LoadImageInterface.imgChatButtonFocus, x, y, 0);
    }

    /*
     * execute touch
     */
    public void updatePointer()
    {
        if (GameCanvas.isPointerClick)
            for (var i = 0; i < nTab; i++)
                if (GameCanvas.isPointer(x + (widthTab + distanceTab) * i + xMove, yButtonTab,
                    widthTab, heightTab))
                {
                    selectTab = i;
                    GameCanvas.isPointerClick = false;
                    break;
                }
    }

    public void resetTab(bool isResetCmy)
    {
        TabScreenNew.timeRepaint = 10;
        var t = 0;
    }

    private void Move()
    {
        bntOpen.x = x + xBtnMove - Image.getWidth(LoadImageInterface.imgShortQuest) + xMove + 35;
    }

    public void Paint(MGraphics g)
    {
        PaintFrameChat(g);
        //paint background tab
        for (var i = 0; i < nTab; i++)
        {
            paintTabBig(g, x + (widthTab + distanceTab) * i + xMove, yButtonTab,
                widthTab + distanceTab, heightTab);
            FontManager.GetInstance().tahoma_7_white.DrawString(g, listNameTab[i],
                x + (widthTab + distanceTab) * i + widthTab / 2 + xMove - 7,
                heightTab / 4 + yButtonTab - 4, 0);
        }
        //paint background tab focus
        paintTabFocus(g, x + (widthTab + distanceTab) * selectTab + xMove, yButtonTab,
            widthTab + distanceTab, heightTab);
        FontManager.GetInstance().tahoma_7_white.DrawString(g, listNameTab[selectTab],
            x + (widthTab + distanceTab) * selectTab + widthTab / 2 + xMove - 7,
            heightTab / 4 + yButtonTab - 4, 0);

        bntOpen.paint(g);
        fatherChat[selectTab].paintContentChatWorld(g);
        GameScr.resetTranslate(g);
    }

    private void PaintFrameChat(MGraphics g)
    {
        //first row
        g.drawImage(LoadImageInterface.imgChatConner, x + xMove, y, MGraphics.TOP | MGraphics.LEFT);
        for (var i = 1; i < 18; i++)
            g.drawImage(LoadImageInterface.imgChatRec, x + i * Image.getWidth(LoadImageInterface.imgChatRec) + xMove,
                y, 0);
        g.drawRegion(LoadImageInterface.imgChatConner, 0, 0, Image.getWidth(LoadImageInterface.imgChatConner),
            Image.getHeight(LoadImageInterface.imgChatConner), Sprite.TRANS_MIRROR,
            x + 18 * Image.getWidth(LoadImageInterface.imgChatConner) + xMove, y, MGraphics.TOP | MGraphics.LEFT);

        //paint all rac of frame
        for (var i = 1; i < 20; i++)
        for (var j = 0; j < 19; j++)
            g.drawImage(LoadImageInterface.imgChatRec, x + j * Image.getWidth(LoadImageInterface.imgChatRec) + xMove,
                y + Image.getHeight(LoadImageInterface.imgChatRec) * i, 0);


        g.drawImage(LoadImageInterface.imgChatConner_1, x + xMove,
            y + Image.getHeight(LoadImageInterface.imgChatRec) * 20, MGraphics.TOP | MGraphics.LEFT);
        for (var i = 1; i < 18; i++)
            g.drawImage(LoadImageInterface.imgChatRec_1,
                x + i * Image.getWidth(LoadImageInterface.imgChatRec_1) + xMove,
                y + Image.getHeight(LoadImageInterface.imgChatRec) * 20, 0);

        g.drawRegion(LoadImageInterface.imgChatConner_1, 0, 0, Image.getWidth(LoadImageInterface.imgChatConner_1),
            Image.getHeight(LoadImageInterface.imgChatConner_1), Sprite.TRANS_MIRROR,
            x + 18 * Image.getWidth(LoadImageInterface.imgChatConner_1) + xMove,
            y + Image.getHeight(LoadImageInterface.imgChatRec) * 20, MGraphics.TOP | MGraphics.LEFT);
    }
}