using System;
using src.lib;
using src.main;
using src.Objectgame;
using src.real;
using src.screen;
using Char = src.Objectgame.Char;

namespace src.Gui
{
    public class TabSkill : MainTabNew
    {
        public static bool isDaHoc = true;
        public static SkillTemplate skillIndex;
        public Command cmdGan, cmdNangCap, cmdKhaiMo;

        public Char demo;
        public MBitmap imgBg, imgBgText, imgskill;
        public int indexKill = -1, coutFc;

        public Scroll scrInfo = new Scroll();
        public int[][] xyListItem = new int[15][];
        public int ypaintText;

        public TabSkill(string text)
        {
            // TODO Auto-generated constructor stub
            for (var i = 0; i < xyListItem.Length; i++)
            {
                xyListItem[i] = new int[2];
                xyListItem[i][0] =
                    xTab + 14 + i % 5 * ((widthFrame - 80 - Image.getWidth(LoadImageInterface.ImgItem) * 5) / 6)
                    + i % 5 * Image.getWidth(LoadImageInterface.ImgItem);
                xyListItem[i][1] = yTab + i / 5 * (Image.getWidth(LoadImageInterface.ImgItem) +
                                                   (widthFrame - 50 - Image.getWidth(LoadImageInterface.ImgItem) * 6) /
                                                   7) + 35;
            }
            ypaintText = xyListItem[xyListItem.Length - 1][1]
                         + Image.getWidth(LoadImageInterface.ImgItem)
                         + 5;

            cmdGan = new Command("Gán", this, 2000, null);
            cmdGan.setPos(xTab + wtab2 / 2 - 5 * LoadImageInterface.img_use.GetWidth() / 4,
                yTab + heightFrame - LoadImageInterface.img_use.GetHeight() - 8,
                LoadImageInterface.img_use, LoadImageInterface.img_use_focus);

            cmdNangCap = new Command("Nâng cấp", this, 2001, null);
            cmdNangCap.setPos(xTab + wtab2 / 2 + LoadImageInterface.img_use.GetWidth() / 4,
                yTab + heightFrame - LoadImageInterface.img_use.GetHeight() - 8,
                LoadImageInterface.img_use, LoadImageInterface.img_use_focus);


            cmdKhaiMo = new Command("Khai mở", this, 2002, null);
            cmdKhaiMo.setPos(xTab + wtab2 / 2 - LoadImageInterface.img_use.GetWidth() / 2,
                yTab + heightFrame - LoadImageInterface.img_use.GetHeight() - 8,
                LoadImageInterface.img_use, LoadImageInterface.img_use_focus);

            skillIndex = (SkillTemplate) SkillTemplates.hSkilltemplate.get(0 + "");
            demo = new Char();
            demo.head = Char.myChar().head;
            demo.body = Char.myChar().body;
            demo.leg = Char.myChar().leg;
        }

        public override void perform(int idAction, object p)
        {
            // TODO Auto-generated method stub
            switch (idAction)
            {
                case 2000:
                    var listKey = new Vector();
                    for (var i = 0; i < 5; i++)
                    {
                        var cmd = new Command("Key " + (i + 1), this, 2010 + i, null);
                        listKey.add(cmd);
                    }
                    GameCanvas.menu.startAt(listKey);
                    break;
                case 2010:
                    Char.myChar().mQuickslot[0].setIsSkill(skillIndex.id,
                        skillIndex.typebuffSkill == Skill.SKILL_CLICK_USE_BUFF);
                    QuickSlot.SaveRmsQuickSlot();
                    break;
                case 2011:
                    Char.myChar().mQuickslot[1].setIsSkill(skillIndex.id,
                        skillIndex.typebuffSkill == Skill.SKILL_CLICK_USE_BUFF);
                    QuickSlot.SaveRmsQuickSlot();
                    break;
                case 2012:
                    Char.myChar().mQuickslot[2].setIsSkill(skillIndex.id,
                        skillIndex.typebuffSkill == Skill.SKILL_CLICK_USE_BUFF);
                    QuickSlot.SaveRmsQuickSlot();
                    break;
                case 2013:
                    Char.myChar().mQuickslot[3].setIsSkill(skillIndex.id,
                        skillIndex.typebuffSkill == Skill.SKILL_CLICK_USE_BUFF);
                    QuickSlot.SaveRmsQuickSlot();
                    break;
                case 2014:
                    Char.myChar().mQuickslot[4].setIsSkill(skillIndex.id,
                        skillIndex.typebuffSkill == Skill.SKILL_CLICK_USE_BUFF);
                    QuickSlot.SaveRmsQuickSlot();
                    break;
            }
            base.perform(idAction, p);
        }


        public override void update()
        {
            // TODO Auto-generated method stub
            if (imgBg == null)
            {
                imgBg = GameCanvas.loadImage("/GuiNaruto/skill/bg.png");
                imgBgText = GameCanvas.loadImage("/GuiNaruto/skill/bgtext.png");
                imgskill = GameCanvas.loadImage("/GuiNaruto/createChar/itemCreateC.png");
            }
            if (GameCanvas.gameTick % 4 == 0)
            {
                coutFc++;
                if (coutFc > 2)
                    coutFc = 0;
            }
            //		Char.myChar().update();
            if (skillIndex != null && GameCanvas.gameTick % 53 == 0 && demo.indexSkill <= 0)
            {
                demo.statusMe = Char.A_ATTK;
                demo.setSkillPaint(GameScr.sks[skillIndex.ideff], Skill.ATT_STAND);
            }

            demo.update();
        }

        public override void updateKey()
        {
            // TODO Auto-generated method stub
            var s1 = scrInfo.updateKey();
            scrInfo.updatecm();
            for (var i = 0; i < xyListItem.Length; i++)
                if (i < SkillTemplates.hSkilltemplate.size() && GameCanvas.isPointerClick && GameCanvas.isPoint(
                        xyListItem[i][0], xyListItem[i][1],
                        LoadImageInterface.ImgItem.GetWidth(), LoadImageInterface.ImgItem.GetHeight()))
                {
                    GameCanvas.clearPointerEvent();
                    indexKill = i;

                    skillIndex = (SkillTemplate) SkillTemplates.hSkilltemplate.get("" + indexKill);
                    demo.setSkillPaint(GameScr.sks[skillIndex.ideff], Skill.ATT_STAND);
                    if (skillIndex != null) isDaHoc = skillIndex.level >= 0;
                    if (isDaHoc)
                        if (skillIndex.level >= skillIndex.nlevelSkill - 1)
                            cmdGan.x = cmdKhaiMo.x;
                        else
                            cmdGan.x = xTab + wtab2 / 2 - 5 * LoadImageInterface.img_use.GetWidth() / 4;
                }
            if (skillIndex == null) return;
            if (skillIndex.level == -1)
            {
                if (getCmdPointerLast(cmdKhaiMo))
                {
                    cmdKhaiMo.performAction();
                    GameCanvas.clearPointerEvent();
                    Service.GetInstance().StudySkill(skillIndex.id);
                }
            }
            else
            {
                if (getCmdPointerLast(cmdGan))
                {
                    cmdGan.performAction();
                    GameCanvas.clearPointerEvent();
                }

                if (skillIndex.level < skillIndex.nlevelSkill - 1 && getCmdPointerLast(cmdNangCap))
                {
                    cmdNangCap.performAction();
                    GameCanvas.clearPointerEvent();
                    Service.GetInstance().ImproveSkill(skillIndex.id);
                }
            }
        }

        public override void paint(MGraphics g)
        {
            GameCanvas.ResetTrans(g);
            g.drawImage(imgBg, xTab - widthSubFrame / 2 - 10, yTab + heightFrame / 2,
                MGraphics.VCENTER | MGraphics.HCENTER);

            foreach (int[] t in xyListItem)
                g.drawImage(LoadImageInterface.ImgItem, t[0], t[1], MGraphics.TOP | MGraphics.LEFT);

            for (var i = 0; i < SkillTemplates.hSkilltemplate.size(); i++)
            {
                var skillIndexx = (SkillTemplate) SkillTemplates.hSkilltemplate.get("" + i);
                if (skillIndexx != null)
                {
                    SmallImage.DrawSmallImage(g, skillIndexx.iconId,
                        xyListItem[i][0] + LoadImageInterface.ImgItem.GetWidth() / 2,
                        xyListItem[i][1] + LoadImageInterface.ImgItem.GetHeight() / 2, 0,
                        MGraphics.VCENTER | MGraphics.HCENTER, true);
              
                    if (skillIndexx.level == -1)
                    {
                        g.setColor(0x000000, 50);
                        g.fillRect(xyListItem[i][0] + 2,
                            xyListItem[i][1] + 2,
                            LoadImageInterface.ImgItem.GetWidth() - 4,
                            LoadImageInterface.ImgItem.GetHeight() - 4);
                        g.disableBlending();
                    }
                }
            }
            if (indexKill >= 0)
                Paint.paintFocus(g,
                    xyListItem[indexKill][0] + 4,
                    xyListItem[indexKill][1] + 4
                    , LoadImageInterface.ImgItem.GetWidth() - 9, LoadImageInterface.ImgItem.GetWidth() - 9, coutFc);
            if (imgBgText != null)
                g.drawImage(imgBgText, xTab + wtab2 / 2, ypaintText + imgBgText.GetHeight() / 2,
                    MGraphics.VCENTER | MGraphics.HCENTER);
            if (imgskill != null)
                g.drawImage(imgskill,
                    xTab + wtab2 / 2 - imgBgText.GetWidth() / 2 + 4 + imgskill.GetWidth() / 2,
                    ypaintText + 4 + imgskill.GetHeight() / 2, MGraphics.VCENTER | MGraphics.HCENTER);
   
            if (skillIndex != null)
            {
                if (isDaHoc)
                {
                    cmdGan.paint(g);
                    if (skillIndex.level < skillIndex.nlevelSkill - 1)
                        cmdNangCap.paint(g);
                }
                else
                {
                    cmdKhaiMo.paint(g);
                }
                SmallImage.DrawSmallImage(g, skillIndex.iconId,
                    xTab + wtab2 / 2 - imgBgText.GetWidth() / 2 + 4 + imgskill.GetWidth() / 2,
                    ypaintText + 4 + imgskill.GetHeight() / 2, 0, MGraphics.VCENTER | MGraphics.HCENTER, true);
                FontManager.GetInstance().tahoma_7_yellow.DrawString(g, skillIndex.name,
                    xTab + wtab2 / 2 - imgBgText.GetWidth() / 2 + 6 + imgskill.GetWidth(),
                    ypaintText + 4, 0);
                FontManager.GetInstance().tahoma_7_blue1.DrawString(g, "Cấp: " + ((skillIndex.level < 0 ? 0 : skillIndex.level) + 1),
                    xTab + wtab2 / 2 - imgBgText.GetWidth() / 2 + 6 + imgskill.GetWidth(),
                    ypaintText + 16, 0);
                scrInfo.setStyle(
                    skillIndex.description.Length + 2 +
                    (skillIndex.level + 1 < skillIndex.nlevelSkill && skillIndex.id != 0 ? 3 : 0), 10,
                    xTab + wtab2 / 2 - imgBgText.GetWidth() / 2 + 4 + imgskill.GetWidth() / 2,
                    ypaintText + 30, imgBgText.GetWidth() - 8, imgBgText.GetHeight() - 32, true, 1);
                scrInfo.setClip(g, xTab + wtab2 / 2 - imgBgText.GetWidth() / 2 + 4,
                    ypaintText + 30, imgBgText.GetWidth() - 8, imgBgText.GetHeight() - 32);
                try
                {
                    FontManager.GetInstance().tahoma_7_white.DrawString(g,
                        "Sát thương: " + skillIndex.dame[skillIndex.level < 0 ? 0 : skillIndex.level],
                        xTab + wtab2 / 2 - imgBgText.GetWidth() / 2 + 6,
                        ypaintText + 30, 0);
                    FontManager.GetInstance().tahoma_7_white.DrawString(g,
                        "Phạm vi: " + skillIndex.rangelv[skillIndex.level < 0 ? 1 : skillIndex.level],
                        xTab + wtab2 / 2 - imgBgText.GetWidth() / 2 + 6,
                        ypaintText + 40, 0);
                }
                catch (Exception e)
                {
                    // TODO: handle exception
               
                }


                for (var i = 0; i < skillIndex.description.Length; i++)
                    FontManager.GetInstance().tahoma_7_white.DrawString(g, skillIndex.description[i],
                        xTab + wtab2 / 2 - imgBgText.GetWidth() / 2 + 6,
                        ypaintText + 50 + i * 10, 0);
                if (skillIndex.level + 1 < skillIndex.nlevelSkill)
                {
                    FontManager.GetInstance().tahoma_7_yellow.DrawString(g, "Cấp:" + ((skillIndex.level < 0 ? 0 : skillIndex.level) + 2),
                        xTab + wtab2 / 2 - imgBgText.GetWidth() / 2 + 6,
                        ypaintText + 50 + 10 * skillIndex.description.Length, 0);
                    try
                    {
                        FontManager.GetInstance().tahoma_7_white.DrawString(g,
                            "Sát thương: " + skillIndex.dame[(skillIndex.level < 0 ? 0 : skillIndex.level) + 1],
                            xTab + wtab2 / 2 - imgBgText.GetWidth() / 2 + 6,
                            ypaintText + 60 + 10 * skillIndex.description.Length, 0);
                        FontManager.GetInstance().tahoma_7_white.DrawString(g,
                            "Phạm vi: " + skillIndex.rangelv[(skillIndex.level < 0 ? 0 : skillIndex.level) + 1],
                            xTab + wtab2 / 2 - imgBgText.GetWidth() / 2 + 6,
                            ypaintText + 70 + 10 * skillIndex.description.Length, 0);
                    }
                    catch (Exception e)
                    {
                        // TODO: handle exception
                   
                    }
                }
            }
            GameCanvas.ResetTrans(g);
            if (demo.skillPaint != null)
            {
                var dir = demo.cdir;
                demo.cdir = 1;
                demo.paintCharWithSkill(g, demo.indexSkill, xTab - widthSubFrame / 2 - 5, yTab + 3 * heightFrame / 4 + 5);
                demo.cdir = dir;
            }
            else
            {
                demo.paintChar(g, xTab - widthSubFrame / 2 - 5, yTab + 3 * heightFrame / 4 + 30);
            }
            base.paint(g);
        }


        public override void updatePointer()
        {
            // TODO Auto-generated method stub
        }
    }
}