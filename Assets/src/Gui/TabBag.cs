/*
 * this class inventory tab
 */

using System.Collections.Generic;
using src.lib;
using src.main;
using src.Objectgame;
using src.real;

namespace src.Gui
{
    public class TabBag : MainTabNew
    {
        public static bool isPayment, isShowPayment = true;
        public static List<ItemPayment> ListItem = new List<ItemPayment>();

        private int cItem = 0, rItem = 0;

        private readonly Command cmdopenPayment; //use
        private readonly Command cmdSendPayment; //use
        private readonly Command cmdClosePayment; //use
        private readonly Command cmdSelect; //use
        private readonly Command cmdSale; //use
        private readonly Command cmdXoaItem; //drop item
        public int coutFc;
        private int hcmd = 0;
        public int idSelect;
        private MBitmap imgCoins1, imgCoins2;
        private readonly bool[] isTypeTheCao;

        private Item itemFocus;
        private ListNew list;
        public ListNew listContent;
        private readonly Scroll listItemPayment = new Scroll();
        private int maxw, maxh, indexPaint = 12, winfo = 140;
        private readonly int numW = 5;
        private readonly int numH = 6;
        private int numHPaint, maxSize = 60;
        public string[] text_info;
        private readonly TField tf_mathecao = new TField();
        private readonly TField tf_soseri = new TField();
        public int x_payment, y_payment, w_payment, h_payment;
        public int[][] xypaint_text = new int[10][];

        public TabBag(string name)
        {
            idSelect = -1;
            isTypeTheCao = new[] {true, false, false, false, false, false, false, false, false};
            typeTab = INVENTORY;
            nameTab = name;
            xBegin = xTab + wOneItem / 2 + 4;
            yBegin = yTab + 30 + wOneItem - 5;
            maxw = (wblack - 8) / 32;
            maxh = (hblack - 8) / 32;
            cmdSelect = new Command("Sử dụng", this, Constants.BTN_USE_ITEM, null, 0, 0);
            cmdSelect.setPos(xBegin - widthSubFrame - 21, yBegin + heightSubFrame * 2 / 3 - 10,
                LoadImageInterface.img_use,
                LoadImageInterface.img_use_focus);
            cmdSale = new Command("Bán ", this, Constants.BTN_SALE_ITEM, null, 0, 0);
            cmdSale.setPos(xBegin - widthSubFrame / 2 - 26 - Image.getWidth(LoadImageInterface.img_use_focus) / 2,
                yBegin + heightSubFrame * 2 / 3 - 14 - Image.getHeight(LoadImageInterface.img_use_focus),
                LoadImageInterface.img_use, LoadImageInterface.img_use_focus);

            cmdXoaItem = new Command("Vứt bỏ", this, 36, null, 0, 0);
            cmdXoaItem.setPos(xBegin + Image.getWidth(LoadImageInterface.img_use) - widthSubFrame - 13,
                yBegin + heightSubFrame * 2 / 3 - 10, LoadImageInterface.img_use, LoadImageInterface.img_use_focus);


            cmdopenPayment = new Command("", this, 40, null, 0, 0);
            cmdopenPayment.setPos(xBegin + widthSubFrame - 10,
                yBegin - 18, LoadImageInterface.bt_Plus, LoadImageInterface.bt_Plus);

            init();

            text_info = new[]
            {
                "Mã thẻ:", "Số serial:", "Viettel", "Mobile", "Vinaphone", "Vn mobile", "Zing", "Vcoin", "Bit", "Gate"
            };
            w_payment = 240;
            h_payment = 200;

            x_payment = GameCanvas.w / 2 - w_payment / 2;
            y_payment = GameCanvas.h / 2 - h_payment / 2 - 10;

            cmdSendPayment = new Command("Send ", this, 41, null, 0, 0);
            cmdSendPayment.setPos(x_payment + w_payment / 2 - LoadImageInterface.img_use.GetWidth() / 2,
                y_payment + h_payment - LoadImageInterface.img_use.GetHeight() / 2, LoadImageInterface.img_use,
                LoadImageInterface.img_use_focus);

            cmdClosePayment = new Command("", this, 42, null, 0, 0);
            cmdClosePayment.setPos(x_payment + w_payment - LoadImageInterface.closeTab.GetWidth() / 2,
                y_payment - LoadImageInterface.closeTab.GetHeight() / 2,
                LoadImageInterface.closeTab, LoadImageInterface.closeTab);


            tf_mathecao.x = x_payment + w_payment / 2 - 60;
            tf_mathecao.y = y_payment + 40;
            tf_mathecao.width = 120;
            tf_mathecao.height = ITEM_HEIGHT + 2;
            tf_mathecao.isFocus = false;
            tf_mathecao.setIputType(TField.INPUT_ALPHA_NUMBER_ONLY);


            tf_soseri.x = x_payment + w_payment / 2 - 60;
            tf_soseri.y = y_payment + 60;
            tf_soseri.width = 120;
            tf_soseri.height = ITEM_HEIGHT + 2;
            tf_soseri.isFocus = false;
            tf_soseri.setIputType(TField.INPUT_ALPHA_NUMBER_ONLY);

            for (var i = 0; i < xypaint_text.Length; i++)
                xypaint_text[i] = new int[2];
            xypaint_text[0][0] = x_payment + 5;
            xypaint_text[0][1] = tf_mathecao.y + 5;
            xypaint_text[1][0] = x_payment + 5;
            xypaint_text[1][1] = tf_soseri.y + 5;

            for (var i = 2; i < 6; i++)
            {
                xypaint_text[i][0] = x_payment + (i - 1) * (w_payment - 20) / 5 - (w_payment - 20) / 10 + 4 * (i - 1) +
                                     (i == 5 ? 10 : 0);
                xypaint_text[i][1] = tf_soseri.y + 30;
            }
            for (var i = 6; i < xypaint_text.Length; i++)
            {
                xypaint_text[i][0] = x_payment + (i - 5) * (w_payment - 20) / 5 - (w_payment - 20) / 10 + 4 * (i - 5) +
                                     (i == 9 ? 10 : 0);
                xypaint_text[i][1] = tf_soseri.y + 50;
            }
//        Service.getInstance().Payment_GetInfo();
        }

        private void LoadImage()
        {
            imgCoins1 = GameCanvas.loadImage("/GuiNaruto/myseft/coins1.png");
            imgCoins2 = GameCanvas.loadImage("/GuiNaruto/myseft/coins2.png");
        }

        public override void paint(MGraphics g)
        {
            g.setColor(color[1]);

            //money
            g.drawImage(imgCoins1, xBegin - 7, yBegin - Image.getHeight(imgCoins1) - 1, 0);
            FontManager.GetInstance().tahoma_7b_white.DrawString(g, Char.myChar().xu + "",
                xBegin + 18, yBegin - Image.getHeight(imgCoins1) / 2 - 4, 0);

            //gold
            g.drawImage(imgCoins2, xBegin + Image.getWidth(LoadImageInterface.ImgItem) * 5 + 9,
                yBegin - Image.getHeight(imgCoins2) - 1, MGraphics.TOP | MGraphics.RIGHT);
            FontManager.GetInstance().tahoma_7b_white.DrawString(g, Char.myChar().luong + "",
                xBegin + (Image.getWidth(LoadImageInterface.ImgItem) + 2) * 5 - Image.getWidth(imgCoins1) + 34,
                yBegin - Image.getHeight(imgCoins1) / 2 - 4, 0);
            //paint image item
            for (var i = 0; i < numW; i++)
            for (var j = 0; j < numH; j++)
                g.drawImage(LoadImageInterface.ImgItem, xBegin + Image.getWidth(LoadImageInterface.ImgItem) * i,
                    yBegin + Image.getHeight(LoadImageInterface.ImgItem) * j + 2, 0);

            // paint item
            if (Char.myChar().arrItemBag != null)
                for (var i = 0; i < Char.myChar().arrItemBag.Length; i++)
                {
                    if (i > 29) return;
                    var it = Char.myChar().arrItemBag[i];
                    if (it == null) continue;
                    var r = it.indexUI / numW;
                    var c = it.indexUI - r * numW;

                    it.paintItem(g, xBegin + Image.getWidth(LoadImageInterface.ImgItem) * c + 14,
                        yBegin + Image.getHeight(LoadImageInterface.ImgItem) * r + 14 + 2);
                }
            if (idSelect > -1 && Focus == INFO)
            {
                //focus
                SetPaintInfo();
                Paint.paintFocus(g,
                    xBegin + idSelect % numW * Image.getWidth(LoadImageInterface.ImgItem) + 11 -
                    LoadImageInterface.ImgItem.GetWidth() / 4,
                    yBegin + idSelect / numW * Image.getHeight(LoadImageInterface.ImgItem) + 13 -
                    LoadImageInterface.ImgItem.GetWidth() / 4
                    , LoadImageInterface.ImgItem.GetWidth() - 9, LoadImageInterface.ImgItem.GetWidth() - 9, coutFc);
                Paint.paintItemInfo(g, itemFocus, xTab - widthSubFrame - 5, yTab + 10);
                cmdSelect.paint(g);
                cmdXoaItem.paint(g);
                cmdSale.paint(g);
            }
            if (isPayment)
                PaintPayment(g);
            else
                cmdopenPayment.paint(g);
        }

        public override void perform(int idAction, object p)
        {
            switch (idAction)
            {
                case Constants.BTN_USE_ITEM:
                    if (Char.myChar().arrItemBag != null && idSelect > -1 && idSelect < Char.myChar().arrItemBag.Length)
                    {
                        Service.GetInstance().useItem((sbyte) idSelect);
                    }
                    idSelect = -1;
                    break;
                case Constants.BTN_SALE_ITEM:
                    if (Char.myChar().arrItemBag != null && idSelect > -1 && idSelect < Char.myChar().arrItemBag.Length
                        && Char.myChar().arrItemBag[idSelect].template != null)
                        GameCanvas.startYesNoDlg(
                            "Bạn có muốn bán " + Char.myChar().arrItemBag[idSelect].template.name + " không?",
                            new Command("Có", this, 37, null, 0, 0),
                            new Command("Không", GameCanvas.instance, GameCanvas.cEndDgl, null, 0, 0));

                    break;
                case 36:
                    if (Char.myChar().arrItemBag != null && idSelect > -1 && idSelect < Char.myChar().arrItemBag.Length
                        && Char.myChar().arrItemBag[idSelect].template != null)
                        GameCanvas.startYesNoDlg(
                            "Bạn có muốn vứt bỏ " + Char.myChar().arrItemBag[idSelect].template.name + " không?",
                            new Command("Có", this, Constants.BTN_DROP_ITEM, null, 0, 0),
                            new Command("Không", GameCanvas.instance, GameCanvas.cEndDgl, null, 0, 0));
                    break;
                case Constants.BTN_DROP_ITEM:
                    if (Char.myChar().arrItemBag != null && idSelect > -1 && idSelect < Char.myChar().arrItemBag.Length)
                        Service.GetInstance().giveUpItem((sbyte) idSelect);
                    GameCanvas.EndDlg();
                    idSelect = -1;
                    break;
                case 37: //ok ban item
                    if (Char.myChar().arrItemBag != null && idSelect > -1 && idSelect < Char.myChar().arrItemBag.Length
                    )
                        Service.GetInstance().SellItem((sbyte) Char.myChar().arrItemBag[idSelect].itemId);
                    GameCanvas.EndDlg();
                    idSelect = -1;
                    break;
                case 40:
                    if (GameCanvas.isStore) return;
                    if (isShowPayment)
                    {
                        isPayment = !isPayment;
                        isPaintCmdClose = !isPaintCmdClose;
                    }
                    else
                    {
                        isPayment = false;
                        isPaintCmdClose = true;
                    }
                    break;
                case 41:
                    byte indexthecao = 0;
                    for (var i = 0; i < isTypeTheCao.Length; i++)
                        if (isTypeTheCao[i])
                        {
                            indexthecao = (byte) i;
                            break;
                        }
                    if (tf_mathecao.getText().Trim().Length > 0 && tf_soseri.getText().Trim().Length > 0)
                    {
                        Service.GetInstance().MakePayment(tf_mathecao.getText(), tf_soseri.getText(),
                            (sbyte) indexthecao);
                        tf_mathecao.clear();
                        tf_soseri.clear();
                        GameCanvas.StartDglThongBao("Đã gửi mã thẻ vui lòng chờ giây lát...");
                    }
                    else
                    {
                        GameCanvas.StartDglThongBao("Vui lòng nhập đầy đủ thông tin !!!");
                    }
                    break;
                case 42:
                    isPayment = false;
                    isPaintCmdClose = true;
                    break;
            }
        }

        public override void updatePointer()
        {
            if (imgCoins1 == null)
                LoadImage();

            listItemPayment.updateKey();
            listItemPayment.updatecm();
            if (GameCanvas.gameTick % 4 == 0)
            {
                coutFc++;
                if (coutFc > 2)
                    coutFc = 0;
            }
            if (!isPayment && GameCanvas.isPointerJustRelease && GameCanvas.IsPointSelect(xBegin, yBegin,
                    numW * (Image.getWidth(LoadImageInterface.ImgItem) + 2),
                    numH * (Image.getHeight(LoadImageInterface.ImgItem) + 4)))
            {
                GameCanvas.isPointerJustRelease = false;
                var row = (GameCanvas.px - xBegin) / Image.getWidth(LoadImageInterface.ImgItem);
                var col = (GameCanvas.py - yBegin) / Image.getWidth(LoadImageInterface.ImgItem);

                row = (GameCanvas.px - xBegin - (row - 1) * 2) / Image.getWidth(LoadImageInterface.ImgItem);
                col = (GameCanvas.py - yBegin - (col - 1) * 2) / Image.getWidth(LoadImageInterface.ImgItem);


                if (row > 6)
                    row = 6;
                if (col > 5)
                    col = 5;

                var select = row + col * numW;

                var size = 0;
                if (typeTab == INVENTORY && Char.myChar().arrItemBag != null)
                    size = Char.myChar().arrItemBag.Length;
                if (select >= 0 && select < size)
                {
                    GameCanvas.isPointerClick = false;
                    if (select != idSelect)
                    {
                        timePaintInfo = 0;
                        idSelect = select;
                        listContent = null;
                    }
                    Focus = INFO;
                }
                else
                {
                    idSelect = -1;
                    timePaintInfo = 0;
                    listContent = null;
                }
            }
        }


        public override void updateKey()
        {
            if (getCmdPointerLast(cmdSelect))
                if (cmdSelect != null)
                {
                    GameCanvas.isPointerJustRelease = false;
                    GameCanvas.keyPressed[5] = false;
                    keyTouch = -1;
                    if (cmdSelect != null)
                        cmdSelect.performAction();
                }

            if (getCmdPointerLast(cmdXoaItem))
                if (cmdXoaItem != null)
                {
                    GameCanvas.isPointerJustRelease = false;
                    GameCanvas.keyPressed[5] = false;
                    keyTouch = -1;
                    if (cmdXoaItem != null)
                        cmdXoaItem.performAction();
                }
            if ( getCmdPointerLast(cmdSale))
                if (cmdSale != null)
                {
                    GameCanvas.isPointerJustRelease = false;
                    GameCanvas.keyPressed[5] = false;
                    keyTouch = -1;
                    if (cmdSale != null)
                        cmdSale.performAction();
                }
            if (!isPayment && getCmdPointerLast(cmdopenPayment))
                if (cmdopenPayment != null)
                {
                    GameCanvas.isPointerJustRelease = false;
                    GameCanvas.keyPressed[5] = false;
                    keyTouch = -1;
                    if (cmdopenPayment != null)
                        cmdopenPayment.performAction();
                }

            if (isPayment)
            {
                if (tf_mathecao != null)
                    tf_mathecao.update();
                if (tf_soseri != null)
                    tf_soseri.update();
                UpdateKeyPayment();
                if (getCmdPointerLast(cmdClosePayment))
                    if (cmdClosePayment != null)
                    {
                        GameCanvas.isPointerJustRelease = false;
                        GameCanvas.keyPressed[5] = false;
                        keyTouch = -1;
                        if (cmdClosePayment != null)
                            cmdClosePayment.performAction();
                    }
                if (getCmdPointerLast(cmdSendPayment))
                    if (cmdSendPayment != null)
                    {
                        GameCanvas.isPointerJustRelease = false;
                        GameCanvas.keyPressed[5] = false;
                        keyTouch = -1;
                        if (cmdSendPayment != null)
                            cmdSendPayment.performAction();
                    }
            }
        }

        private void SetPaintInfo()
        {
            if (idSelect >= Char.myChar().arrItemBag.Length) return;
            var item = Char.myChar().arrItemBag[idSelect];
            itemFocus = Char.myChar().arrItemBag[idSelect];
            if (item.template != null)
                name = item.template.name;
        }

        private void PaintPayment(MGraphics g)
        {
            g.setColor(0x000000, GameCanvas.opacityTab);
            g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
            g.disableBlending();
            Paint.paintFrameNaruto(x_payment, y_payment, w_payment, h_payment + 2, g);
            Paint.PaintBoxName("Nạp gold", x_payment + w_payment / 2 - 40, y_payment, 80, g);
            tf_mathecao.paint(g);
            tf_soseri.paint(g);
            cmdSendPayment.paint(g);
            cmdClosePayment.paint(g);
            for (var i = 0; i < text_info.Length; i++)
            {
                FontManager.GetInstance().tahoma_7.DrawString(g, text_info[i],
                    xypaint_text[i][0] + LoadImageInterface.imgCheckSetting.GetWidth() / 2 + 2, xypaint_text[i][1], 0);
                if (i > 1 && i - 2 < isTypeTheCao.Length)
                    g.drawRegion(LoadImageInterface.imgCheckSetting, 0,
                        (isTypeTheCao[i - 2] ? 1 : 0) * LoadImageInterface.imgCheckSetting.GetHeight() / 2,
                        LoadImageInterface.imgCheckSetting.GetWidth(),
                        LoadImageInterface.imgCheckSetting.GetHeight() / 2,
                        0, xypaint_text[i][0], xypaint_text[i][1] + 4, MGraphics.VCENTER | MGraphics.HCENTER);
            }
            if (ListItem != null && ListItem.Count > 0)
            {
                listItemPayment.setStyle(ListItem.Count, 15, x_payment + 5,
                    xypaint_text[xypaint_text.Length - 1][1] + 20,
                    w_payment - 10, h_payment - (xypaint_text[xypaint_text.Length - 1][1] + 20), true, 1);
                listItemPayment.setClip(g, x_payment + 5, xypaint_text[xypaint_text.Length - 1][1] + 20, w_payment - 10,
                    h_payment - (xypaint_text[xypaint_text.Length - 1][1] + 20));

                for (var i = 0; i < ListItem.Count; i++)
                {
                    var item = ListItem[i];
                    if (item != null)
                    {
                        FontManager.GetInstance().tahoma_7.DrawString(g,
                            "Mệnh giá " + item.getRealMoneyAmount() + " (" + item.getRealMoneyUnit() + ")",
                            x_payment + 15, xypaint_text[xypaint_text.Length - 1][1] + 15 * i + 25, 0);
                        FontManager.GetInstance().tahoma_7.DrawString(g,
                            item.getGameMoneyAmount() + " (" + item.getGameMoneyUnit() + ")",
                            x_payment + w_payment - 10, xypaint_text[xypaint_text.Length - 1][1] + 15 * i + 25, 1);
                    }
                }
                GameCanvas.ResetTrans(g);
            }
        }

        private void UpdateKeyPayment()
        {
            var indexTypethe = -1;
            for (var i = 2; i < text_info.Length; i++)
                if (GameCanvas.isPointerJustRelease && GameCanvas.isPointer(
                        xypaint_text[i][0] - LoadImageInterface.imgCheckSetting.GetWidth() / 2,
                        xypaint_text[i][1] - LoadImageInterface.imgCheckSetting.GetWidth() / 2,
                        LoadImageInterface.imgCheckSetting.GetWidth(), LoadImageInterface.imgCheckSetting.GetWidth()))
                {
                    indexTypethe = i - 2;
                    break;
                }

            if (indexTypethe > -1 && indexTypethe < isTypeTheCao.Length)
            {
                for (var i = 0; i < isTypeTheCao.Length; i++)
                    isTypeTheCao[i] = false;
                isTypeTheCao[indexTypethe] = true;
            }
        }

        public override void keypress(int keyCode)
        {
            if (tf_mathecao.isFocus)
                tf_mathecao.keyPressed(keyCode);
            if (tf_soseri.isFocus)
                tf_soseri.keyPressed(keyCode);
            base.keypress(keyCode);
        }
    }
}