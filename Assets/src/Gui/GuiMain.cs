using System;
using src;
using src.Gui;
using src.lib;
using src.main;
using src.model;
using src.Objectgame;
using src.real;
using src.screen;
using UnityEngine;
using Char = src.Objectgame.Char;

public class GuiMain : TScreen, IActionListener
{
    public static int cmdBarX,
        cmdBarY,
        cmdBarW,
        cmdBarLeftW,
        cmdBarRightW,
        cmdBarCenterW,
        hpBarX,
        hpBarY,
        hpBarW,
        mpBarW,
        expBarW,
        lvPosX,
        moneyPosX,
        hpBarH,
        girlHPBarY;

    public static int xL, yL; // left
    public static int xC, yC;
    public static int xR, yR; // right
    public static int xF, yF; // fire
    public static int xU, yU; // up
    public static int xCenter, yCenter;
    public static GamePad gamePad;
    public static bool isAnalog = false;
    public static bool isTestSkill, isPaintOjectMap = true;
    public static int indexSkillTest;
    public static Vector vecItemOther = new Vector();

    public static TField tfchatMap;

    public static int indexshow;
    public Command bntAttack;
    public Command bntAttack_1;
    public Command bntAttack_2;
    public Command bntAttack_3;
    public Command bntAttack_4;
    private readonly Command bntCharBoard;
    private readonly Command btnBatTestSkill;
    private readonly Command btnChangeFocus;
    private readonly Command btnchangeSkill;
    private readonly Command btnChatMap;
    private readonly Command btnUseHp;
    private readonly Command btnUseMp;

    public int indexNext, indexLoopFind;

    //menu icon
    public MenuIcon menuIcon = new MenuIcon(GameCanvas.hw - 215, GameCanvas.h - 50);

    public bool moveClose, moveOpen;
    private readonly int timeoff = 10; //30s
    private long timestart;

    public int typeNextFocus; //char = 0 , mob = 1, item = 2, npc = 3;
    private int v; //gia toc tang toc do move

    private int xMove;
    private readonly int limMove = 45;
    public int[][] xySkill = new int[5][];
    private readonly int y = GameCanvas.h - 43;

    public GuiMain()
    {
        var xAttack = GameCanvas.w - 50 - 10;
        var yAttack = GameCanvas.h - 50 - 10;

        for (var i = 0; i < xySkill.Length; i++)
            xySkill[i] = new int[2];
        bntCharBoard = new Command("", this, Constants.CHAR_BROAD, null, 0, 0);
        bntCharBoard.setPos(1, 1, LoadImageInterface.imgCharacter_info, LoadImageInterface.imgCharacter_info);

        btnchangeSkill = new Command("ChangeKill", this, 20, null, 0, 0);
        btnchangeSkill.setPos(GameCanvas.w - 60, 1, LoadImageInterface.img_use, LoadImageInterface.img_use_focus);

        btnBatTestSkill = new Command("BatTest", this, 21, null, 0, 0);
        btnBatTestSkill.setPos(GameCanvas.w - 120, 1, LoadImageInterface.img_use, LoadImageInterface.img_use_focus);


        btnUseHp = new Command("", this, 31, null, 0, 0);
        btnUseHp.setPos(GameCanvas.w - LoadImageInterface.btnUseHP[0].GetHeight() * 2 - 10, GameCanvas.h - 115,
            LoadImageInterface.btnUseHP[0], LoadImageInterface.btnUseHP[1]);

        btnUseMp = new Command("", this, 32, null, 0, 0);
        btnUseMp.setPos(GameCanvas.w - LoadImageInterface.btnUseMP[0].GetHeight() - 5, GameCanvas.h - 115,
            LoadImageInterface.btnUseMP[0], LoadImageInterface.btnUseMP[1]);

        //0, 68, 30, 30
        btnChatMap = new Command("", this, 33, null, 0, 0);
        btnChatMap.setPos(35,
            73 - LoadImageInterface.btnChatMap[0].GetHeight() / 2, LoadImageInterface.btnChatMap[0],
            LoadImageInterface.btnChatMap[1]);


        btnChangeFocus = new Command("", this, 30, null, 0, 0);
        btnChangeFocus.setPos(
            GameCanvas.w - LoadImageInterface.btnUseHP[0].GetHeight() * 2 - 15 -
            LoadImageInterface.changeFocus[0].GetHeight(), GameCanvas.h - 115, LoadImageInterface.changeFocus[0],
            LoadImageInterface.changeFocus[1]);


        xySkill[0][0] = xAttack + LoadImageInterface.imgAttack_1.GetWidth() / 2;
        xySkill[0][1] = yAttack + LoadImageInterface.imgAttack_1.GetHeight() / 2;
        bntAttack = new Command("", this, Constants.CHAR_BROAD, null, 0, 0);
        bntAttack.setPos(xAttack, yAttack, LoadImageInterface.imgAttack_1, LoadImageInterface.imgAttack_1);

        xAttack = xAttack - Image.getWidth(LoadImageInterface.imgAttack);
        yAttack = yAttack + Image.getHeight(LoadImageInterface.imgAttack_1) / 2;
        xySkill[1][0] = xAttack + LoadImageInterface.imgAttack.GetWidth() / 2;
        xySkill[1][1] = yAttack + LoadImageInterface.imgAttack.GetHeight() / 2;
        bntAttack_1 = new Command("", this, Constants.CHAR_BROAD, null, 0, 0);
        bntAttack_1.setPos(xAttack, yAttack, LoadImageInterface.imgAttack, LoadImageInterface.imgAttack);

        xAttack = xAttack + 5;
        yAttack = yAttack - Image.getHeight(LoadImageInterface.imgAttack) - 5;
        xySkill[2][0] = xAttack + LoadImageInterface.imgAttack.GetWidth() / 2;
        xySkill[2][1] = yAttack + LoadImageInterface.imgAttack.GetHeight() / 2;
        bntAttack_2 = new Command("", this, Constants.CHAR_BROAD, null, 0, 0);
        bntAttack_2.setPos(xAttack, yAttack, LoadImageInterface.imgAttack, LoadImageInterface.imgAttack);

        xAttack = xAttack + Image.getWidth(LoadImageInterface.imgAttack) / 3 + 10;
        yAttack = yAttack - Image.getHeight(LoadImageInterface.imgAttack) + 5;
        xySkill[3][0] = xAttack + LoadImageInterface.imgAttack.GetWidth() / 2;
        xySkill[3][1] = yAttack + LoadImageInterface.imgAttack.GetHeight() / 2;
        bntAttack_3 = new Command("", this, Constants.CHAR_BROAD, null, 0, 0);
        bntAttack_3.setPos(xAttack, yAttack, LoadImageInterface.imgAttack, LoadImageInterface.imgAttack);

        xAttack = xAttack + Image.getWidth(LoadImageInterface.imgAttack) + 5;
        yAttack = yAttack;
        xySkill[4][0] = xAttack + LoadImageInterface.imgAttack.GetWidth() / 2;
        xySkill[4][1] = yAttack + LoadImageInterface.imgAttack.GetHeight() / 2;
        bntAttack_4 = new Command("", this, Constants.CHAR_BROAD, null, 0, 0);
        bntAttack_4.setPos(xAttack, yAttack, LoadImageInterface.imgAttack, LoadImageInterface.imgAttack);

        tfchatMap = new TField();
        tfchatMap.x = GameCanvas.w / 2 - GameCanvas.w / 8;
        tfchatMap.y = GameCanvas.h - 25;
        tfchatMap.isFocus = false;

        tfchatMap.width = GameCanvas.w / 4;
        tfchatMap.height = ITEM_HEIGHT + 2;
        tfchatMap.setMaxTextLenght(40);
    }

    public void perform(int idAction, object p)
    {
        // TODO Auto-generated method stub
        // TODO Auto-generated method stub
        switch (idAction)
        {
            case Constants.CHAR_BROAD:

                if (!moveClose)
                    moveClose = true;
                else
                    moveClose = false;
                if (!moveClose)
                    timestart = MSystem.currentTimeMillis();
                break;
            case 20:
                indexSkillTest = (indexSkillTest + 1) % GameScr.sks.Length;
                isPaintOjectMap = !isPaintOjectMap;
                break;
            case 21:
                isTestSkill = !isTestSkill;
                break;

            case 30:
                int minx, miny, maxx, maxy;
                var rangeMax = Char.myChar().getdxSkill();
                minx = Char.myChar().cx - 3 * rangeMax / 2;
                maxx = Char.myChar().cx + 3 * rangeMax / 2;
                miny = Char.myChar().cy - rangeMax - 20;
                maxy = Char.myChar().cy + rangeMax + 40;
                switch (typeNextFocus)
                {
                    case 0:
                        var dem = indexNext < GameScr.vCharInMap.size() ? indexNext : 0;
                        var isFinded = false;
                        for (var i = dem; i < GameScr.vCharInMap.size(); i++)
                        {
                            var c = (Char) GameScr.vCharInMap.elementAt(i);
                            if (c.Equals(Char.myChar()) || c.Equals(Char.myChar().charFocus))
                                continue;
                            if (c.statusMe == Char.A_HIDE || c.isInvisible)
                                continue;
                            if (c.charID >= -1)
                                continue;
                            if (minx <= c.cx && c.cx <= maxx && miny <= c.cy && c.cy <= maxy &&
                                !c.Equals(Char.myChar()))
                            {
                                Char.myChar().clearAllFocus();
                                Char.myChar().charFocus = c;
                                indexNext++;
                                if (indexNext >= GameScr.vCharInMap.size())
                                    typeNextFocus = (typeNextFocus + 1) % 4;
                                isFinded = true;
                                break;
                            }
                        }
                        if (!isFinded)
                        {
                            typeNextFocus = (typeNextFocus + 1) % 4;
                            if (typeNextFocus == indexLoopFind)
                                return;
                            indexNext = 0;
                            if (btnChangeFocus != null)
                                btnChangeFocus.performAction();
                        }
                        break;
                    case 1:
                        var demMob = indexNext < GameScr.vMob.size() ? indexNext : 0;
                        var isFindedMob = false;
                        for (var i = demMob; i < GameScr.vMob.size(); i++)
                        {
                            var c = (Mob) GameScr.vMob.elementAt(i);
                            if (c.status == Mob.MA_INJURE || c.status == Mob.MA_DEADFLY || c.hp <= 0 ||
                                c.Equals(Char.myChar().mobFocus))
                                continue;
                            if (minx <= c.x && c.x <= maxx && miny <= c.y && c.y <= maxy)
                            {
                                if (c.isBoss && !Mob.isBossAppear) continue;
                                Char.myChar().clearAllFocus();
                                Char.myChar().mobFocus = c;
                                indexNext++;
                                if (indexNext >= GameScr.vMob.size())
                                    typeNextFocus = (typeNextFocus + 1) % 4;
                                isFindedMob = true;
                                break;
                            }
                        }
                        if (!isFindedMob)
                        {
                            typeNextFocus = (typeNextFocus + 1) % 4;
                            if (typeNextFocus == indexLoopFind)
                                return;
                            indexNext = 0;
                            if (btnChangeFocus != null)
                                btnChangeFocus.performAction();
                        }
                        break;
                    case 2:
                        var demItem = indexNext < GameScr.vItemMap.size() ? indexNext : 0;
                        var isFindedItem = false;
                        for (var i = demItem; i < GameScr.vItemMap.size(); i++)
                        {
                            var c = (ItemMap) GameScr.vItemMap.elementAt(i);
                            if (Char.myChar().itemFocus != null && c.Equals(Char.myChar().itemFocus)) continue;
                            if (minx <= c.x && c.x <= maxx && miny <= c.y && c.y <= maxy)
                            {
                                Char.myChar().clearAllFocus();
                                Char.myChar().itemFocus = c;
                                indexNext++;
                                if (indexNext >= GameScr.vItemMap.size())
                                    typeNextFocus = (typeNextFocus + 1) % 4;
                                isFindedItem = true;
                                break;
                            }
                        }
                        if (!isFindedItem)
                        {
                            typeNextFocus = (typeNextFocus + 1) % 4;
                            if (typeNextFocus == indexLoopFind)
                                return;
                            indexNext = 0;
                            if (btnChangeFocus != null)
                                btnChangeFocus.performAction();
                        }
                        break;
                    case 3:
                        var demNpc = indexNext < GameScr.vNpc.size() ? indexNext : 0;
                        var isFindedNpc = false;
                        for (var i = demNpc; i < GameScr.vNpc.size(); i++)
                        {
                            var c = (Npc) GameScr.vNpc.elementAt(i);
                            if (Char.myChar().mobFocus != null && c.Equals(Char.myChar().npcFocus)) continue;
                            if (minx <= c.cx && c.cx <= maxx && miny <= c.cy && c.cy <= maxy)
                            {
                                Char.myChar().clearAllFocus();
                                Char.myChar().npcFocus = c;
                                indexNext++;
                                if (indexNext >= GameScr.vNpc.size())
                                    typeNextFocus = (typeNextFocus + 1) % 4;
                                isFindedNpc = true;
                                break;
                            }
                        }
                        if (!isFindedNpc)
                        {
                            typeNextFocus = (typeNextFocus + 1) % 4;
                            if (typeNextFocus == indexLoopFind)
                                return;
                            indexNext = 0;
                        }
                        break;
                }
                break;
            case 31:
                if (Char.myChar().cHP < Char.myChar().cMaxHP)
                {
                    if (Char.myChar().arrItemBag != null)
                    {
                        var isNoExitHp = true;
                        for (var i = 0; i < Char.myChar().arrItemBag.Length; i++)
                        {
                            var it = Char.myChar().arrItemBag[i];
                            if (it != null && Item.isHP(it.template.type))
                            {
                                isNoExitHp = false;
                                if (Char.myChar().arrItemBag != null)
                                    Service.GetInstance().useItem((sbyte)i);
                                break;
                            }
                        }
                        if (isNoExitHp)
                            GameCanvas.StartDglThongBao("Bạn đã hết bình Hp");
                    }
                }
                else
                {
                    GameCanvas.StartDglThongBao("Hp đang đầy");
                }
                break;
            case 32:
                if (Char.myChar().cMP < Char.myChar().cMaxMP)
                {
                    if (Char.myChar().arrItemBag != null)
                    {
                        var isNoExitMp = true;
                        for (var i = 0; i < Char.myChar().arrItemBag.Length; i++)
                        {
                            var it = Char.myChar().arrItemBag[i];
                            if (it != null && Item.isMP(it.template.type))
                            {
                                if (Char.myChar().arrItemBag != null)
                                    Service.GetInstance().useItem((sbyte) i);
                                isNoExitMp = false;
                                break;
                            }
                        }
                        if (isNoExitMp)
                            GameCanvas.StartDglThongBao("Bạn đã hết bình Charka");
                    }
                }
                else
                {
                    GameCanvas.StartDglThongBao("Charka đang đầy");
                }
                break;
            case 33:
                tfchatMap.setTextBox();
                GameCanvas.isPointerClick = false;
                break;
        }
    }

    public void update()
    {
        for (var i = 0; i < vecItemOther.size(); i++)
        {
            var item = (ItemThucAn) vecItemOther.elementAt(i);
            item.update();
        }
        if (moveClose)
        {
            if (xMove > limMove)
            {
                v = 0;
            }
            else
            {
                xMove += v;
                v++;
            }
        }
        else
        {
            if (xMove <= 0)
            {
                v = 0;
            }
            else
            {
                xMove += v;
                v--;
            }
            if ((MSystem.currentTimeMillis() - timestart) / 1000 > timeoff)
            {
                moveClose = true;
                if (menuIcon.subMenu != null) menuIcon.cmdClose.performAction();
            }
        }
        if (tfchatMap.isFocus)
            tfchatMap.update();
        if (tfchatMap.isOKReturn)
        {
            tfchatMap.isOKReturn = false;
            try
            {
                Service.GetInstance().ChatMap(tfchatMap.getText());
                tfchatMap.setText("");
            }
            catch (Exception e)
            {
                // TODO: handle exception
            }
        }
        if (Char.myChar().statusMe == Char.A_DEAD || Char.myChar().statusMe == Char.A_DEADFLY)
            return;
        if (GameCanvas.gameScr != null && GameCanvas.gameScr.guiChatClanWorld != null &&
            !GameCanvas.gameScr.guiChatClanWorld.moveClose)
            menuIcon.update();
    }


    public void UpdateKey()
    {
        if (tfchatMap.isFocus)
            tfchatMap.update();
        if (Char.myChar().statusMe == Char.A_DEAD || Char.myChar().statusMe == Char.A_DEADFLY)
            return;
        var isPress = false;
        if (!MenuIcon.isShowTab && !GameScr.GetInstance().guiChatClanWorld.moveClose && moveClose)
            isPress = updateKeyTouchControl();
        if (!GameCanvas.gameScr.guiChatClanWorld.moveClose &&
            menuIcon.indexpICon == 0 && /*GameCanvas.keyPressed[5] ||*/ getCmdPointerLast(bntCharBoard))
            if (bntCharBoard != null)
            {
                if (Char.myChar().statusMe == Char.A_DEAD || Char.myChar().statusMe == Char.A_DEADFLY)
                {
                    GameCanvas.startCommandDlg("Bạn có muốn hồi sinh tại chỗ (1 gold)?",
                        new Command("Hồi sinh", GameCanvas.instance, GameCanvas.cHoiSinh, null),
                        new Command("Về làng", GameCanvas.instance, GameCanvas.cVeLang, null));
                    return;
                }
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                keyTouch = -1;
                isPress = true;
                if (bntCharBoard != null)
                    bntCharBoard.performAction();
            }
        if (!MenuIcon.isShowTab & getCmdPointerLast(btnChangeFocus))
            if (btnChangeFocus != null)
            {
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                keyTouch = -1;
                isPress = true;
                if (btnChangeFocus != null)
                    btnChangeFocus.performAction();
            }
        if (!MenuIcon.isShowTab & getCmdPointerLast(btnUseHp))
            if (btnUseHp != null)
            {
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                keyTouch = -1;
                isPress = true;
                if (btnUseHp != null)
                    btnUseHp.performAction();
            }
        if (!MenuIcon.isShowTab & getCmdPointerLast(btnUseMp))
            if (btnUseMp != null)
            {
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                keyTouch = -1;
                isPress = true;
                if (btnUseMp != null)
                    btnUseMp.performAction();
            }
        if (LoginScr.isTest)
        {
            if (menuIcon.indexpICon == 0 && /*GameCanvas.keyPressed[5] ||*/ getCmdPointerLast(btnchangeSkill))
                if (bntCharBoard != null)
                {
                    Debug.Log("btnchangeSkill  " + GameCanvas.isPointerJustRelease);
                    GameCanvas.isPointerJustRelease = false;
                    GameCanvas.keyPressed[5] = false;
                    keyTouch = -1;
                    isPress = true;
                    if (btnchangeSkill != null)
                        btnchangeSkill.performAction();
                }
            if (menuIcon.indexpICon == 0 && /*GameCanvas.keyPressed[5] ||*/ getCmdPointerLast(btnBatTestSkill))
                if (bntCharBoard != null)
                {
                    GameCanvas.isPointerJustRelease = false;
                    GameCanvas.keyPressed[5] = false;
                    keyTouch = -1;
                    isPress = true;
                    if (btnBatTestSkill != null)
                        btnBatTestSkill.performAction();
                }
        }
        if (!MenuIcon.isShowTab & getCmdPointerLast(btnChatMap))
            if (btnChatMap != null)
            {
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                keyTouch = -1;
                isPress = true;
                if (btnChatMap != null)
                    btnChatMap.performAction();
            }

        if (!GameCanvas.gameScr.guiChatClanWorld.moveClose && (!moveClose || MenuIcon.lastTab.size() > 0))
            menuIcon.updateKey();
        if (!isPress && GameCanvas.isPointerJustRelease && GameCanvas.isPointerClick && !moveClose)
        {
            moveClose = true;
            if (menuIcon.subMenu != null) menuIcon.cmdClose.performAction();
        }
    }

    public void Paint(MGraphics g)
    {
        var iMppaint =
            Char.myChar().cMP * (MGraphics.getImageWidth(LoadImageInterface.imgMp) - 19) //pháº§n dÆ° ko paint
            / Char.myChar().cMaxMP;
        g.drawRegion(LoadImageInterface.imgMp, 0, 0,
            iMppaint + 19,
            MGraphics.getImageHeight(LoadImageInterface.imgMp),
            0, bntCharBoard.x + 36, bntCharBoard.y + 12, MGraphics.TOP | MGraphics.LEFT, false);

        var hppaint =
            (Char.myChar().cHP > Char.myChar().cMaxHP ? Char.myChar().cMaxHP : Char.myChar().cHP) *
            MGraphics.getImageWidth(LoadImageInterface.imgBlood) / Char.myChar().cMaxHP;
        g.drawRegion(LoadImageInterface.imgBlood, 0, 0,
            hppaint,
            MGraphics.getImageHeight(LoadImageInterface.imgBlood),
            0, bntCharBoard.x + 56, bntCharBoard.y + 21, MGraphics.TOP | MGraphics.LEFT, false);
        var Exppaint = (int) (Char.myChar().cEXP * MGraphics.getImageWidth(LoadImageInterface.imgExp) /
                              Char.myChar().cMaxEXP);
        g.drawRegion(LoadImageInterface.imgExp, 0, 0,
            Exppaint,
            MGraphics.getImageHeight(LoadImageInterface.imgExp),
            0, bntCharBoard.x + 54, bntCharBoard.y + 35, MGraphics.TOP | MGraphics.LEFT, false);
        g.drawRegion(LoadImageInterface.icn_Mail, 0, 0,
            21,
            21,
            0, 10, 65, MGraphics.TOP | MGraphics.LEFT, false);
        if (ChatPrivate.nTinChuaDoc > 0)
            FontManager.GetInstance().tahoma_7_red.DrawStringBorder(g, ChatPrivate.nTinChuaDoc + "", 28,
                68, 1);

        if (TileMap.GetInstance().mapName != null)
        {
            var www = FontManager.GetInstance().tahoma_7_yellow.GetWidth(TileMap.GetInstance().mapName) + 8;
            g.drawImage(MsgDlg.imgpopup[0], GameCanvas.w - (www < 40 ? 40 : www), 0, MGraphics.TOP | MGraphics.LEFT);
            FontManager.GetInstance().tahoma_7_yellow.DrawString(g, TileMap.GetInstance().mapName, GameCanvas.w - 4, 10, 1);
        }

        if (LoginScr.isTest)
        {
            btnchangeSkill.paint(g);
            btnBatTestSkill.paint(g);
        }
        bntCharBoard.paint(g);

        var ph = GameScr.parts[Char.myChar().head];
        SmallImage.DrawSmallImage(g, ph.pi[Char.CharInfo[0][0][0]].id,
            30, 30, 0, MGraphics.VCENTER | MGraphics.HCENTER);

        if (Char.myChar() != null && Char.myChar().cName != null)
            FontManager.GetInstance().tahoma_7b_white.DrawString(g, Char.myChar().cName, bntCharBoard.x + 60, bntCharBoard.y + 45, 0);
        FontManager.GetInstance().tahoma_7_white.DrawString(g, Char.myChar().cHP + "/" + Char.myChar().cMaxHP,
            bntCharBoard.x + 95, bntCharBoard.y + 20, 2);
        FontManager.GetInstance().tahoma_7_white.DrawString(g, Char.myChar().cMP + "/" + Char.myChar().cMaxMP,
            bntCharBoard.x + 95, bntCharBoard.y + 9, 2);
        FontManager.GetInstance().tahoma_7_white.DrawString(g, Char.myChar().cEXP + "%",
            bntCharBoard.x + 95, bntCharBoard.y + 31, 2);
        FontManager.GetInstance().tahoma_7_white.DrawString(g, Char.myChar().clevel + "",
            bntCharBoard.x + 49, bntCharBoard.y + 44, 2);

        if (moveClose && !MenuIcon.isShowTab) //khi menu hien thi
        {
            if (isAnalog)
            {
                GameScr.resetTranslate(g);
                gamePad.paint(g);
            }
            else
            {
                paintTouchControl(g); //3 mui ten
            }
            bntAttack.paint(g);
            bntAttack_1.paint(g);
            bntAttack_2.paint(g);
            bntAttack_3.paint(g);
            bntAttack_4.paint(g);
            btnChangeFocus.paint(g);
            btnUseHp.paint(g);
            btnUseMp.paint(g);
            btnChatMap.paint(g);
            //--------------------------------------------cho pc
            if (tfchatMap.isFocus)
                tfchatMap.paint(g);
            for (var i = 0; i < Char.myChar().mQuickslot.Length; i++)
            {
                var ql = Char.myChar().mQuickslot[i];
                if (ql.quickslotType != -1)
                {
                    var skillIndexx = (SkillTemplate) SkillTemplates.hSkilltemplate.get("" + ql.idSkill);
                    if (skillIndexx != null)
                        if (ql.canUse())
                        {
                            SmallImage.DrawSmallImage(g, skillIndexx.iconTron,
                                xySkill[i][0],
                                xySkill[i][1], 0, MGraphics.VCENTER | MGraphics.HCENTER, true);
                        }
                        else
                        {
                            SmallImage.DrawSmallImage(g, skillIndexx.iconTron,
                                xySkill[i][0],
                                xySkill[i][1], 0, MGraphics.VCENTER | MGraphics.HCENTER, true, 50);
                            FontManager.GetInstance().tahoma_7_white.DrawString(g, ql.gettimeCooldown(), xySkill[i][0],
                                xySkill[i][1] - 6, 2);
                        }
                }
            }
        }

        GameScr.resetTranslate(g);
        if (Char.myChar().npcFocus != null && Char.myChar().npcFocus.template.typeKhu != -1 ||
            Char.myChar().mobFocus != null || Char.myChar().charFocus != null
            || Char.myChar().itemFocus != null)
        {
            var hppaintFocus = 0;
            if (Char.myChar().npcFocus != null)
            {
                hppaintFocus = MGraphics.getImageWidth(LoadImageInterface.imgBlood);
            }
            else if (Char.myChar().charFocus != null)
            {
                hppaintFocus = (Char.myChar().charFocus.cHP > Char.myChar().charFocus.cMaxHP
                                   ? Char.myChar().charFocus.cMaxHP
                                   : Char.myChar().charFocus.cHP)
                               * MGraphics.getImageWidth(LoadImageInterface.imgBlood) /
                               Char.myChar().charFocus.cMaxHP;
                
            }
            else if (Char.myChar().mobFocus != null)
            {
                Char.myChar().mobFocus.hp = Char.myChar().mobFocus.hp < 0 ? 0 : Char.myChar().mobFocus.hp;
                hppaintFocus = (Char.myChar().mobFocus.hp > Char.myChar().mobFocus.maxHp
                                   ? Char.myChar().mobFocus.maxHp
                                   : Char.myChar().mobFocus.hp)
                               * MGraphics.getImageWidth(LoadImageInterface.imgBlood) /
                               Char.myChar().mobFocus.maxHp;
                
            }
            g.drawRegion(LoadImageInterface.imgBlood, 0, 0,
                hppaintFocus,
                MGraphics.getImageHeight(LoadImageInterface.imgBlood),
                0, GameCanvas.w / 2 + 24, LoadImageInterface.imgfocusActor.GetHeight() / 2 - 6,
                MGraphics.TOP | MGraphics.LEFT, false);

            g.drawImage(LoadImageInterface.imgfocusActor, GameCanvas.w / 2 + 43,
                LoadImageInterface.imgfocusActor.GetHeight() / 2,
                MGraphics.VCENTER | MGraphics.HCENTER);
            if (Char.myChar().npcFocus != null && Char.myChar().npcFocus.template.name != null &&
                Char.myChar().npcFocus.template.typeKhu != -1)
            {
                FontManager.GetInstance().tahoma_7b_white.DrawString(g, Char.myChar().npcFocus.template.name, GameCanvas.w / 2 + 28,
                    LoadImageInterface.imgfocusActor.GetHeight() / 2 + 10 , 0);

                try
                {
                    var ph2 = GameScr.parts[Char.myChar().npcFocus.template.headId];
                    SmallImage.DrawSmallImage(g, ph2.pi[Char.CharInfo[0][0][0]].id,
                        GameCanvas.w / 2,
                        LoadImageInterface.imgfocusActor.GetHeight() / 2 , 0,
                        MGraphics.VCENTER | MGraphics.HCENTER);
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                }
            }
            else if (Char.myChar().charFocus != null && Char.myChar().charFocus.cName != null)
            {
                FontManager.GetInstance().tahoma_7b_white.DrawString(g, Char.myChar().charFocus.cName, GameCanvas.w / 2 + 28,
                    LoadImageInterface.imgfocusActor.GetHeight() / 2 + 10, 0);
                FontManager.GetInstance().tahoma_7_white.DrawString(g, Char.myChar().charFocus.cHP + "/" + Char.myChar().charFocus.cMaxHP,
                    GameCanvas.w / 2 + 68,
                    LoadImageInterface.imgfocusActor.GetHeight() / 2 - 7, 2);
                FontManager.GetInstance().tahoma_7b_white.DrawString(g, Char.myChar().charFocus.clevel + "", GameCanvas.w / 2 + 17,
                    LoadImageInterface.imgfocusActor.GetHeight() / 2 + 11, 2);
                try
                {
                    var ph2 = GameScr.parts[Char.myChar().charFocus.head];
                    SmallImage.DrawSmallImage(g, ph2.pi[Char.CharInfo[0][0][0]].id,
                        GameCanvas.w / 2,
                        LoadImageInterface.imgfocusActor.GetHeight() / 2, 0,
                        MGraphics.VCENTER | MGraphics.HCENTER);
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                }
            }
            else if (Char.myChar().mobFocus != null && Char.myChar().mobFocus.mobName != null)
            {
                FontManager.GetInstance().tahoma_7_white.DrawString(g, Char.myChar().mobFocus.hp + "/" + Char.myChar().mobFocus.maxHp,
                    GameCanvas.w / 2 + 68,
                    LoadImageInterface.imgfocusActor.GetHeight() / 2 - 7, 2);
                try
                {
                    FontManager.GetInstance().tahoma_7b_white.DrawString(g, Char.myChar().mobFocus.mobName, GameCanvas.w / 2 + 28,
                        LoadImageInterface.imgfocusActor.GetHeight() / 2 + 10, 0);
                    FontManager.GetInstance().tahoma_7b_white.DrawString(g, Char.myChar().mobFocus.level + "", GameCanvas.w / 2 + 17,
                        LoadImageInterface.imgfocusActor.GetHeight() / 2 + 11, 2);
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                }
            }
            else if (Char.myChar().itemFocus != null && Char.myChar().itemFocus.template != null)
            {
                FontManager.GetInstance().tahoma_7b_white.DrawString(g, Char.myChar().itemFocus.template.name, GameCanvas.w / 2 + 28,
                    LoadImageInterface.imgfocusActor.GetHeight() / 2 + 10, 0);
            }
        }
        for (var i = 0; i < vecItemOther.size(); i++)
        {
            var item = (ItemThucAn) vecItemOther.elementAt(i);
            item.paint(g, 20 * i + 14, bntCharBoard.y + 85);
        }
        menuIcon.y = y + xMove;
        menuIcon.paint(g);
    }


    public static void loadCmdBar()
    {
        hpBarX = 78 - 15 + 10;
        hpBarY = cmdBarY;
        hpBarW = GameScr.gW - 84 - 30;
        expBarW = GameScr.gW - 44 - 4;
        hpBarH = 20;
        loadInforBar();
    }

    public static void loadInforBar()
    {
        if (!GameCanvas.isTouch)
            return;
        hpBarW = 83;
        mpBarW = 57;
        hpBarX = 52;
        hpBarY = 10 + Info.hI;
        expBarW = GameScr.gW - 61;
    }

    public static void paintTouchControl(MGraphics g)
    {
        try
        {
            GameScr.resetTranslate(g);
         
            g.drawRegion(LoadImageInterface.imgMoveNormal, 0, 0, Image.getWidth(LoadImageInterface.imgMoveNormal),
                Image.getHeight(LoadImageInterface.imgMoveNormal), Sprite.TRANS_MIRROR, xL, yL,
                MGraphics.HCENTER | MGraphics.VCENTER);
            if (keyTouch == 4)
                g.drawRegion(LoadImageInterface.imgMoveFocus, 0, 0, Image.getWidth(LoadImageInterface.imgMoveNormal),
                    Image.getHeight(LoadImageInterface.imgMoveNormal), Sprite.TRANS_ROT180, xL, yL,
                    MGraphics.HCENTER | MGraphics.VCENTER);

            // ----RIGHT
          
            g.drawRegion(LoadImageInterface.imgMoveNormal, 0, 0, Image.getWidth(LoadImageInterface.imgMoveNormal),
                Image.getHeight(LoadImageInterface.imgMoveNormal), 0, xR, yR, MGraphics.HCENTER
                                                                              | MGraphics.VCENTER);
            if (keyTouch == 6)
                g.drawRegion(LoadImageInterface.imgMoveFocus, 0, 0, Image.getWidth(LoadImageInterface.imgMoveNormal),
                    Image.getHeight(LoadImageInterface.imgMoveNormal), 0, xR, yR,
                    MGraphics.HCENTER | MGraphics.VCENTER);

            // ----UP
           
            g.drawRegion(LoadImageInterface.imgMoveNormal, 0, 0, Image.getWidth(LoadImageInterface.imgMoveNormal),
                Image.getHeight(LoadImageInterface.imgMoveNormal), Sprite.TRANS_MIRROR_ROT90, xU,
                yU, MGraphics.HCENTER | MGraphics.VCENTER);
            if (keyTouch == 3)
                g.drawRegion(LoadImageInterface.imgMoveFocus, 0, 0, Image.getWidth(LoadImageInterface.imgMoveFocus),
                    Image.getHeight(LoadImageInterface.imgMoveFocus), Sprite.TRANS_MIRROR_ROT90, xU,
                    yU, MGraphics.HCENTER | MGraphics.VCENTER);
            // CENTER
            g.drawImage(LoadImageInterface.imgMoveCenter, xCenter, yCenter, MGraphics.HCENTER | MGraphics.VCENTER);
        }
        catch (Exception e)
        {
        }
    }

    public static bool updateKeyTouchControl()
    {
        var isPress = false;
        keyTouch = -1;
        if (isAnalog)
        {
            gamePad.update();
        }
        else
        {
            if (GameCanvas.isPointerHoldIn(xU - Image.getHeight(LoadImageInterface.imgMoveNormal) / 2,
                yU - Image.getWidth(LoadImageInterface.imgMoveNormal) / 2,
                Image.getHeight(LoadImageInterface.imgMoveNormal),
                Image.getWidth(LoadImageInterface.imgMoveNormal) / 2))
            {
                keyTouch = 3;
                GameCanvas.keyHold[2] = true;
                isPress = true;
            }
            else if (GameCanvas.isPointerDown)
            {
                GameCanvas.keyHold[2] = false;
            }

            if (GameCanvas.isPointerHoldIn(
                xU - Image.getHeight(LoadImageInterface.imgMoveNormal) / 2 -
                Image.getWidth(LoadImageInterface.imgMoveNormal) / 2,
                yU - Image.getWidth(LoadImageInterface.imgMoveNormal) / 2,
                Image.getWidth(LoadImageInterface.imgMoveNormal) -
                Image.getHeight(LoadImageInterface.imgMoveNormal) / 2,
                Image.getWidth(LoadImageInterface.imgMoveNormal) -
                Image.getHeight(LoadImageInterface.imgMoveNormal) / 2))
            {
                GameCanvas.keyHold[1] = true;
                isPress = true;
            }
            else if (GameCanvas.isPointerDown)
            {
                GameCanvas.keyHold[1] = false;
            }

            if (GameCanvas.isPointerHoldIn(xU + Image.getHeight(LoadImageInterface.imgMoveNormal) / 2,
                yU - Image.getWidth(LoadImageInterface.imgMoveNormal) / 2,
                Image.getWidth(LoadImageInterface.imgMoveNormal) -
                Image.getHeight(LoadImageInterface.imgMoveNormal) / 2,
                Image.getWidth(LoadImageInterface.imgMoveNormal) -
                Image.getHeight(LoadImageInterface.imgMoveNormal) / 2))
            {
                GameCanvas.keyHold[3] = true;
                isPress = true;
            }
            else if (GameCanvas.isPointerDown)
            {
                GameCanvas.keyHold[3] = false;
            }

            if (GameCanvas.isPointerHoldIn(xL - Image.getWidth(LoadImageInterface.imgMoveNormal) / 2,
                yL - Image.getHeight(LoadImageInterface.imgMoveNormal) / 2,
                Image.getWidth(LoadImageInterface.imgMoveNormal), Image.getHeight(LoadImageInterface.imgMoveNormal)))
            {
                keyTouch = 4;
                GameCanvas.keyHold[4] = true;
                isPress = true;
            }
            else if (GameCanvas.isPointerDown)
            {
                GameCanvas.keyHold[4] = false;
            }

            if (GameCanvas.isPointerHoldIn(xR - Image.getWidth(LoadImageInterface.imgMoveNormal) / 2,
                yR - Image.getHeight(LoadImageInterface.imgMoveNormal) / 2,
                Image.getWidth(LoadImageInterface.imgMoveNormal), Image.getHeight(LoadImageInterface.imgMoveNormal)))
            {
                keyTouch = 6;
                GameCanvas.keyHold[6] = true;
                isPress = true;
            }
            else if (GameCanvas.isPointerDown)
            {
                GameCanvas.keyHold[6] = false;
            }
        }

        if (GameCanvas.isPointerHoldIn(xF, yF, 54, 54))
        {
            keyTouch = 5;
            if (GameCanvas.isPointerJustRelease)
            {
                GameCanvas.keyPressed[5] = true;
                isPress = true;
            }
        }

        if (GameCanvas.isPointerClick && GameCanvas.isPoint(0, 68, 30, 30))
        {
            GameCanvas.isPointerClick = false;
            TabChat.GetInstance().switchToMe();
            if (ChatPrivate.nTinChuaDoc > 0)
                for (var i = 0; i < ChatPrivate.vOtherchar.size(); i++)
                {
                    var other = (OtherChar) ChatPrivate.vOtherchar.elementAt(i);
                    if (other.nTinChuaDoc > 0)
                    {
                        TabChat.GetInstance().indexRow = i;
                        ChatPrivate.nTinChuaDoc -= other.nTinChuaDoc;
                        ChatPrivate.nTinChuaDoc = ChatPrivate.nTinChuaDoc < 0 ? 0 : ChatPrivate.nTinChuaDoc;
                        other.nTinChuaDoc = 0;
                        break;
                    }
                }
        }


        if (GameCanvas.isPointerJustRelease && !isAnalog)
        {
            GameCanvas.keyHold[1] = false;
            GameCanvas.keyHold[2] = false;
            GameCanvas.keyHold[3] = false;
            GameCanvas.keyHold[4] = false;
            GameCanvas.keyHold[6] = false;
        }
        return isPress;
    }


    public void keyPress(int keyCode)
    {
        if (tfchatMap.isFocus)
            tfchatMap.keyPressed(keyCode);
        base.keyPress(keyCode);
    }
}