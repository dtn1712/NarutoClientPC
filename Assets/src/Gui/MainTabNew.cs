﻿using src.lib;
using src.main;
using src.model;
using src.screen;

namespace src.Gui
{
    public class MainTabNew : TScreen, IActionListener
    {
        public const sbyte INVENTORY = 0;
        public const sbyte EQUIP = 1;
        public const sbyte MY_INFO = 2;
        public const sbyte SKILLS = 3;
        public const sbyte QUEST = 4;
        public const sbyte CHAT = 5;
        public const sbyte GOLD = 6;
        public const sbyte CONFIG = 7;
        public const sbyte SHOP = 8;
        public const sbyte CHEST = 9;
        public const sbyte REBUILD = 10;
        public const sbyte FUNCTION = 11;

        public const sbyte CLAN_INVENTORY = 12;

        // 
        public const sbyte COLOR_WHITE = 0;

        public const sbyte COLOR_BLUE = 1;
        public const sbyte COLOR_YELLOW = 2;
        public const sbyte COLOR_VIOLET = 3;
        public const sbyte COLOR_ORANGE = 4;
        public const sbyte COLOR_GREEN = 5;
        public const sbyte COLOR_RED = 6;
        public const sbyte COLOR_BLACK = 7;
        public const sbyte TAB = 0;
        public const sbyte INFO = 1;
        public static sbyte maxTypeTab = 12;

        public static int wbackground, hbackground;
        public static int longwidth, xlongwidth, ylongwidth;
        public static int timeRequest = 15;
        public static int wblack, hblack, hMaxContent;
        public static sbyte wOneItem = 20, wOne5; //width of 1 item,width 1 item device 5
        public static bool is320;
        public static sbyte Focus = 0;
        public static int timePaintInfo;
        public static MainTabNew instance;

        public static MBitmap[] imgTab = new MBitmap[14];

        public static int[] color =
        {
            0xa89982, 0xbaaa92, 0xf8b848,
            0xebebeb, 0xa69780, 0xc7b59c, 0xb6a58e, 0x706354,
            0xe3d5be, 0xaf9f89, 0xf8f8a8
        };

        private static readonly int[] colorLow =
        {
            0xd6c7ae, 0x9c8c77, 0x706354, 0x887a67,
            0x4b4339
        };

        private static string nameCur = "";
        private readonly string[] cNameMenu = {"HÀNH TRANG", "TRANG BỊ", "NHÂN VẬT", "KỸ NĂNG", "NÂNG CẤP"};
        private MBitmap imgStarRebuild;

        public bool isPaintCmdClose = true;

        public ListNew listContent = null;
        public string name;

        public string nameTab = "";
        private Paint paint;
        public sbyte typeTab;
        public int wContent, colorName;

        private int width = 256, height = 256;

        public int widthFrame = 237, heightFrame = 230, widthSubFrame = 145, heightSubFrame = 230;

        public int xBegin, yBegin; //vi tri bat bau de paint thong tin 1 tab duoc select

        //xTab: toa do x menu, yTab+GameCanvas.h/5: toa do y menu
        public int xTab,
            yTab,
            wSmall,
            hSmall,
            numWSmall,
            numHSmall,
            xMoney,
            wtab2 = 176,
            xSub,
            yMoney,
            xChar,
            yChar,
            sizeFocus,
            numWBlack,
            numHBlack;


        public MainTabNew()
        {
            paint = new Paint();
            //xet do rong cua item
            if (GameCanvas.isTouch)
                wOneItem = 24;
            else if (GameCanvas.w >= 240)
                wOneItem = 24;
            if (GameCanvas.h < 240 && wOneItem > 24)
                wOneItem = 24;


            hMaxContent = GameCanvas.h - GameCanvas.hCommand * 2;
            wOne5 = (sbyte) (wOneItem / 5);
            wbackground = GameCanvas.w / 32 + 1;
            hbackground = GameCanvas.h / 32 + 1;
            var tw = GameCanvas.w / wOneItem;
            if (tw > 9)
                tw = 9;
            var htam = GameCanvas.h / 5 * 4 - GameCanvas.hCommand / 2;
            if (GameCanvas.isTouch)
                htam += GameCanvas.hCommand / 2;
            var th = htam / wOneItem;
            if (th > 8)
                th = 8;
            wSmall = (tw - 1) * wOneItem - wOne5 * 3 + wOne5;
            hSmall = th * wOneItem + wOne5;
            if (hSmall % 2 != 0)
                hSmall -= 1;
            if (GameCanvas.isTouch)
                if (GameCanvas.w >= 380)
                {
                    longwidth = 170;
                    timeRequest = 5;
                    hMaxContent = hSmall - wOneItem - wOne5;
                    xTab = (GameCanvas.w - tw * wOneItem - longwidth) / 2;
                    xlongwidth = GameCanvas.w - xTab - longwidth;
                    ylongwidth = yTab + GameCanvas.h / 5;
                }
                else if (GameCanvas.w > 315)
                {
                    is320 = true;
                    tw = 8;
                    wSmall = (tw - 1) * wOneItem - wOne5 * 3;
                    longwidth = 130;
                    timeRequest = 5;
                    hMaxContent = hSmall - wOneItem - wOne5;
                    xlongwidth = GameCanvas.w - xTab - longwidth + 5;
                    ylongwidth = yTab + GameCanvas.h / 5;
                }

            if (is320)
            {
                xTab = -5;
                xlongwidth = xTab + wSmall + wOneItem + wOne5 * 2;
                longwidth = GameCanvas.w - xlongwidth;
            }
            else
            {
                xTab = (GameCanvas.w - tw * wOneItem - longwidth) / 2;
            }

            yTab = 0;
            numWSmall = wSmall / 32;
            numHSmall = hSmall / 32;
            wblack = wSmall / wOneItem * wOneItem;
            hblack = (hSmall / wOneItem - 1) * wOneItem;
            numWBlack = wblack / 32;
            numHBlack = hblack / 32;
            xMoney = GameCanvas.w - (xTab - 9) - 72;
            if (GameCanvas.isTouch)
                if (xMoney > GameCanvas.w - 112)
                    xMoney = GameCanvas.w - 112;
            yMoney = 5;
            xChar = 0; // xTab - 9;
            yChar = GameCanvas.h / 10 - 21;
            sizeFocus = wOne5 + wOneItem;
            if (sizeFocus > 32)
                sizeFocus = 32;
            var xpaint2khung = GameCanvas.w / 2 - wtab2 / 2 - widthSubFrame / 2 - 20;
            xTab = (xpaint2khung < 0 ? widthSubFrame : xpaint2khung + widthSubFrame) + 5;
            xSub = xTab - widthSubFrame - 10;
            yTab = GameCanvas.h / 2 - heightSubFrame / 2 < 0 ? 0 : GameCanvas.h / 2 - heightSubFrame / 2;
        }


        public virtual void perform(int idAction, object p)
        {
            // TODO Auto-generated method stub
        }

        public static MainTabNew gI()
        {
            return instance ?? (instance = new MainTabNew());
        }

        /*
     * Paint tab main manu
     */
        public void paintTab(MGraphics g, string nameTab, int idSelect,
            Vector vec, bool isClan)
        {
            var size = vec.size();
            if (GameCanvas.lowGraphic)
            {
                paintRectLowGraphic(g, 0, 0, GameCanvas.w, GameCanvas.h, 0);
                paintRectLowGraphic(g, xMoney, yMoney, 22 * 3, 32, 1);
                paintRectLowGraphic(g, xTab, yTab, wSmall, hSmall, 1);
            }
            else
            {
                Paint.paintFrameNaruto(xTab, yTab, wtab2, heightSubFrame + 2, g);
                Paint.PaintBoxName(cNameMenu[idSelect], xTab + wtab2 / 2 - 100 / 2, yTab, 100, g);
                Paint.SubFrame(xSub, yTab, widthSubFrame, heightSubFrame, g);

                return;
            }


            g.drawRegion(imgTab[4], 0, 0, 14, 14, 0, xMoney + 4, yMoney + 2, 0); //money
            g.drawRegion(imgTab[4], 0, 14, 14, 14, 0, xMoney + 4, yMoney + 17, 0); //gold
            if (isClan)
            {
            }

            //background focus tab
            if (GameCanvas.lowGraphic)
            {
//low graphic
                paintRectLowGraphic(g, xTab + wOne5, yTab + GameCanvas.h / 5
                                                     + wOneItem * idSelect, wOne5 + wOneItem, wOneItem, 1);
            }
            else
            {
                if (wOne5 + wOneItem > 32)
                {
// nen focus trai tab
                    g.drawRegion(imgTab[1], 0, 0, wOneItem, wOneItem, 0, xTab
                                                                         + wOne5, yTab + GameCanvas.h / 5
                                                                                  + wOneItem * idSelect, 0);
                    g.drawRegion(imgTab[1], 0, 0, wOne5, wOneItem, 0, xTab + wOne5
                                                                      + wOneItem, yTab + GameCanvas.h / 5
                                                                                  + wOneItem * idSelect, 0);
                }
                else
                {
                    g.drawRegion(imgTab[1], 0, 0, wOne5 + wOneItem, wOneItem, 0,
                        xTab + wOne5, yTab + GameCanvas.h / 5
                                      + wOneItem * idSelect, 0);
                }
            }

            // khung focus tab left
            if (!GameCanvas.isTouch && Focus == TAB)
            {
                g.setColor(color[3]);
                g.drawRect(xTab + wOne5 + 1, yTab + GameCanvas.h / 5
                                             + wOneItem * idSelect + 1, wOne5 + wOneItem - 3,
                    wOneItem - 5);
            }
            g.setColor(color[0]);

            //draw icon tab
            for (var i = 0; i < size; i++)
            {
                var tab = (MainTabNew) vec.elementAt(i);
                var t = 0;
                int indexp = tab.typeTab; //type tab

                if (indexp > maxTypeTab)
                    indexp = typeTab;
                g.drawRegion(imgTab[3], 0, indexp * 16, 16, 16, 0, xTab + wOne5
                                                                   + wOne5 / 2 + wOneItem / 2 + t, yTab + GameCanvas.h / 5
                                                                                                   + wOneItem / 2 +
                                                                                                   wOneItem * i,
                    MGraphics.VCENTER
                    | MGraphics.HCENTER);
            }

            for (var i = 0; i < size; i++)
            {
//background icon tab trai
                var tab = (MainTabNew) vec.elementAt(i);
                var t = 0; //nhung tab duoc select thi icon se dich sang trai 1 pixex
                if (i != idSelect)
                    g.drawRect(xTab + wOne5, yTab + GameCanvas.h / 5
                                             + wOneItem * i, wOne5 + wOneItem, wOneItem);
                else if (Focus == TAB || GameCanvas.isTouch)
                    t = -1 + GameCanvas.gameTick / 2 % 3;
                //xu ly rung icon tab
                int indexp = tab.typeTab;
                if (indexp > maxTypeTab)
                    indexp = typeTab;
                g.drawRegion(imgTab[3], 0, indexp * 16, 16, 16, 0, xTab + wOne5
                                                                   + wOne5 / 2 + wOneItem / 2 + t, yTab + GameCanvas.h / 5
                                                                                                   + wOneItem / 2 +
                                                                                                   wOneItem * i,
                    MGraphics.VCENTER
                    | MGraphics.HCENTER);
            }

            if (GameCanvas.lowGraphic)
                paintRectLowGraphic(g, xTab + wOneItem + wOne5 * 3, yTab
                                                                    + GameCanvas.h / 5 + wOneItem, wblack, hblack, 2);
            else
                for (var i = 0; i <= numWBlack; i++)
                    // nen den thong tin
                for (var j = 0; j <= numHBlack; j++)
                    if (i == numWBlack)
                    {
                        if (j == numHBlack)
                            g.drawImage(imgTab[2], xTab + wOneItem + wOne5 * 3
                                                   + wblack - 32, yTab + GameCanvas.h / 5
                                                                  + wOneItem + hblack - 32, 0);
                        else
                            g.drawImage(imgTab[2], xTab + wOneItem + wOne5 * 3
                                                   + wblack - 32, yTab + GameCanvas.h / 5
                                                                  + wOneItem + j * 32, 0);
                    }
                    else
                    {
                        if (j == numHBlack)
                            g.drawImage(imgTab[2], xTab + wOneItem + wOne5 * 3
                                                   + i * 32, yTab + GameCanvas.h / 5
                                                             + wOneItem + hblack - 32, 0);
                        else
                            g.drawImage(imgTab[2], xTab + wOneItem + wOne5 * 3
                                                   + i * 32, yTab + GameCanvas.h / 5
                                                             + wOneItem + j * 32, 0);
                    }
            if (longwidth > 0)
            {
                GameScr.resetTranslate(g);
                var indexPaint = 12;
                if (GameCanvas.lowGraphic)
                {
                    paintRectLowGraphic(g, xlongwidth, ylongwidth, longwidth,
                        hSmall, indexPaint);
                }
                else
                {
// man hinh menu ben phai
                    int maxw = longwidth / 32, maxh = hSmall / 32;
                    for (var i = 0; i <= maxw; i++)
                    for (var j = 0; j <= maxh; j++)
                    {
                        indexPaint = 12;
                        if (j == 0)
                            indexPaint = 12;
                        if (i == maxw)
                        {
                            if (j == maxh)
                                g.drawImage(imgTab[indexPaint], xlongwidth
                                                                + longwidth - 32, ylongwidth + hSmall
                                                                                  - 32, 0);
                            else
                                g.drawImage(imgTab[indexPaint], xlongwidth
                                                                + longwidth - 32,
                                    ylongwidth + j * 32, 0);
                        }
                        else
                        {
                            if (j == maxh)
                                g.drawImage(imgTab[indexPaint], xlongwidth + i
                                                                * 32, ylongwidth + hSmall - 32, 0);
                            else
                                g.drawImage(imgTab[indexPaint], xlongwidth + i
                                                                * 32, ylongwidth + j * 32, 0);
                        }
                    }
                }
                for (var i = xlongwidth; i < xlongwidth + longwidth; i += 6)
                    g.fillRect(i, ylongwidth + wOneItem, 4, 1);
            }
        }

        public static void paintRectLowGraphic(MGraphics g, int x, int y, int w,
            int h, int indexColor)
        {
            g.setColor(colorLow[indexColor]);
            g.fillRect(x, y, w, h);
        }

        public void init()
        {
        }

        public virtual void keypress(int keyCode)
        {
        }


        public override void keyPress(int keyCode)
        {
            keypress(keyCode);
            base.keyPress(keyCode);
        }


    }
}