﻿using System;
using src.lib;
using src.main;
using src.model;
using src.real;
using UnityEngine;
using Char = src.Objectgame.Char;

namespace src.screen
{
    public class RoomMiniGameScr : TScreen, IActionListener
    {
        public static RoomMiniGameScr InstanRoomMiniGameScr;
       
        public static bool isRoomMiniGameScr;

        public bool isReady = false;
        public static Scroll scrMain = new Scroll();

        public GuiFriend guiFriendInvite;


        /////  Action tuong tac
        private Command btnStartGame;

        private Command btnKickPlayerOutRoom;

        private Command btnOutRoom;

        private Command btnReady;

        private Command btnCancleReady;

        ///// Player in room 
        public Char[] charInRoom;

        /// action btn
        private const int START_GAME = 0;

        private const int KICKPLAYER_GAME = 1;
        private const int OUT_GAME = 2;
        private const int READY = 5;
        private const int CANCEL_READY = 6;

        public int indexRow;

        public int popupX, popupY, popupW, popupH = 220;
        public long timestartUpdate;

//        public static RoomMiniGameScr getInstanRoomMiniGameScr()
//        {
//            return InstanRoomMiniGameScr ?? (InstanRoomMiniGameScr = new RoomMiniGameScr());
//        }

        public RoomMiniGameScr()
        {
            isRoomMiniGameScr = true;
            guiFriendInvite = new GuiFriend();
            charInRoom = new Char[4];
            charInRoom[0] = Char.myChar();
            popupX = GameCanvas.w / 2 - GameScr.widthGui / 2 - 50;
            popupY = GameScr.popupY;
            
            popupW = GameScr.widthGui - 70;
            
            btnStartGame = new Command("Play game", this, START_GAME, null, 0, 0);
            btnKickPlayerOutRoom = new Command("Kích", this, KICKPLAYER_GAME, null, 0, 0);
            btnOutRoom = new Command("Rời phòng", this, OUT_GAME, null, 0, 0);
            btnReady = new Command("Sẵn sàn", this, READY, null, 0, 0);
            btnCancleReady = new Command("Huỷ", this, CANCEL_READY, null, 0, 0);
            btnStartGame.setPos(popupX + popupW - Image.getWidth(LoadImageInterface.btnTab) / 2,
                popupY + 40, LoadImageInterface.btnTab, LoadImageInterface.btnTabFocus);
            btnKickPlayerOutRoom.setPos(popupX + popupW - Image.getWidth(LoadImageInterface.btnTab) / 2,
                popupY + 3 * Image.getHeight(LoadImageInterface.btnTab), LoadImageInterface.btnTab,
                LoadImageInterface.btnTabFocus);
            btnOutRoom.setPos(popupX + popupW - Image.getWidth(LoadImageInterface.btnTab) / 2,
                popupY + 3 * Image.getHeight(LoadImageInterface.btnTab) + 10, LoadImageInterface.btnTab,
                LoadImageInterface.btnTabFocus);
            btnReady.setPos(popupX + popupW - Image.getWidth(LoadImageInterface.btnTab) / 2,
                popupY + 3 * Image.getHeight(LoadImageInterface.btnTab) + 10, LoadImageInterface.btnTab,
                LoadImageInterface.btnTabFocus);
            btnCancleReady.setPos(popupX + popupW - Image.getWidth(LoadImageInterface.btnTab) / 2,
                popupY + 3 * Image.getHeight(LoadImageInterface.btnTab) + 10, LoadImageInterface.btnTab,
                LoadImageInterface.btnTabFocus);
        }

        public void perform(int idAction, object p)
        {
            // TODO Auto-generated method stub
            switch (idAction)
            {
                case START_GAME:
                    break;
                case KICKPLAYER_GAME:
                    break;
                case OUT_GAME:
                    break;
                case READY:
                    isReady = true;
                    break;
                case CANCEL_READY:
                    isReady = false;
                    break;
            }
        }

        public override void updateKey()
        {
            // TODO Auto-generated method stub
            scrMain.updatecm();
            var s1 = scrMain.updateKey();
            if (getCmdPointerLast(btnStartGame))
                if (btnStartGame != null)
                {
                    GameCanvas.isPointerJustRelease = false;
                    keyTouch = -1;
                    if (btnStartGame != null)
                        btnStartGame.performAction();
                }
            //unfriend
            if (getCmdPointerLast(btnOutRoom))
                if (btnOutRoom != null)
                {
                    GameCanvas.isPointerJustRelease = false;
                    keyTouch = -1;
                    if (btnOutRoom != null)
                        btnOutRoom.performAction();
                }

            if (getCmdPointerLast(btnKickPlayerOutRoom))
                if (btnKickPlayerOutRoom != null)
                {
                    GameCanvas.isPointerJustRelease = false;
                    keyTouch = -1;
                    if (btnKickPlayerOutRoom != null)
                        btnKickPlayerOutRoom.performAction();
                }
        }

        public void update()
        {
            // TODO Auto-generated method stub
            if ((MSystem.currentTimeMillis() - timestartUpdate) / 1000 > 30)
            {
                timestartUpdate = MSystem.currentTimeMillis();
                Service.GetInstance().RequestFriendList(Char.myChar().charID);
            }
            if (scrMain.selectedItem == -1)
                return;
            indexRow = scrMain.selectedItem;
            if (indexRow < Char.myChar().vFriend.size() && Char.myChar().vFriend.size() > 0)
            {
                var c = (Char) Char.myChar().vFriend.elementAt(indexRow);
                Char.toCharChat = c;
            }
        }

        public void paint(MGraphics g)
        {
            g.setColor(0x000000, GameCanvas.opacityTab);
            g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
            g.disableBlending();

            if (guiFriendInvite != null)
            {
                guiFriendInvite.paint(g); 
            }
            paintMemberRoomMiniGame(g);
           
        }

        public void SetPosClose(Command cmd)
        {
            // TODO Auto-generated method stub
            cmd.setPos(popupX + popupW - Image.getWidth(LoadImageInterface.closeTab), popupY,
                LoadImageInterface.closeTab,
                LoadImageInterface.closeTab);
        }


        public void paintMemberRoomMiniGame(MGraphics g)
        {
            
            var str = "ROOM";

            Paint.paintFrameNaruto(popupX, popupY, popupW, popupH, g);
//            if (charInRoom != null)
//            {
                 if (charInRoom.Length > 0)
                {
                GameScr.xstart = popupX + 5;
                GameScr.ystart = popupY + 40;

                scrMain.setStyle(charInRoom.Length, 50, GameScr.xstart, GameScr.ystart, popupW - 3, 180, true,
                    1);
                scrMain.setClip(g, GameScr.xstart, GameScr.ystart - 10, popupW - 3, 180);
                var friendCount = 0;
                var yBGFriend = 0;
                for (var i = 0; i < charInRoom.Length; i++)
                {
                    var c =  charInRoom[i];
                    if (c != null)

                    {
                         Paint.PaintBGListQuest(GameScr.xstart + 20, GameScr.ystart + yBGFriend, 120, g); //new quest	
                        if (indexRow == i)
                            Paint.PaintBGListQuestFocus(GameScr.xstart + 20, GameScr.ystart + yBGFriend, 120, g);
        
        
                        FontManager.GetInstance().tahoma_7_white.DrawString(g, "Level: " + c.clevel, GameScr.xstart + 73,
                            GameScr.ystart + yBGFriend + 24, 0);
        
                        g.drawImage(LoadImageInterface.charPic, GameScr.xstart + 35, GameScr.ystart + yBGFriend + 20,
                            MGraphics.VCENTER | MGraphics.HCENTER);
                        g.drawImage(LoadImageInterface.imgName, GameScr.xstart + 90, GameScr.ystart + yBGFriend + 15,
                            MGraphics.VCENTER | MGraphics.HCENTER, true);
                        FontManager.GetInstance().tahoma_7_white.DrawString(g, c.cName, GameScr.xstart + 63, 
                            GameScr.ystart + yBGFriend + 8, 0);
                       
        
                        try
                        {
                            var ph2 = GameScr.parts[c.head];
                        }
                        catch (Exception e)
                        {
                            Debug.LogError(e);
                        }
                        friendCount++;
                        yBGFriend += 50;
                    }
                   
                }
                resetTranslate(g);
                if (Char.myChar().isHostRoom)
                {
                    btnStartGame.paint(g);
                    btnKickPlayerOutRoom.paint(g);
                }
                else
                {
                    btnReady.paint(g);
                    btnCancleReady.paint(g);
                }
                btnOutRoom.paint(g);

//                if (btnChat != null)
//                {
//                    btnChat.paint(g);
//                    btnUnfriend.paint(g);
//                }
                }
                else
                {
                    FontManager.GetInstance().tahoma_7_white.DrawString(g, MResources.NO_FRIEND, popupX + popupW / 2, popupY + 40,
                        MFont.CENTER);
                }
                GameScr.resetTranslate(g);
                //paint name box 
                Paint.PaintBoxName("PHÒNG CHỜ", popupX + 35, popupY, 100, g);
//            }
           
        }
    }
}