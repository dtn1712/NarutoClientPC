using System;
using System.Globalization;
using System.IO;
using src.Gui;
using src.lib;
using src.main;
using src.model;
using src.Objectgame;
using src.Objectgame.quest;
using src.real;
using UnityEngine;
using Char = src.Objectgame.Char;

namespace src.screen
{
    public class GameScr : TScreen, IActionListener, IChatable
    {
        public static long delayPressAtt = 500, lastpress;
        public static bool isSendMove;
        public static Vector veffClient = new Vector();
        public static GameScr instance;

        // FLYING TEXT
        public static string[] flyTextstring;

        public static int[] flyTextX, flyTextY, flyTextDx, flyTextDy, flyTextState, flyTextColor;

        public static int gW, gH, gW2, gssw, gssh, gH34, gW3, gH3, gH23, gW23, gH2, csPadMaxH, cmdBarH, gW34, gW6, gH6;

        // camera
        public static int cmx, cmy, cmdx, cmdy, cmvx, cmvy, cmtoX, cmtoY, cmxLim, cmyLim, gssx, gssy, gssxe, gssye;

        public static SkillPaint[] sks;
        public static Part[] parts;
        public static EffectCharPaint[] efs;
        private static readonly Scroll scrInfo = new Scroll();
        public static Scroll scrMain = new Scroll();

        public static Vector vCharInMap = new Vector(),
            vItemMap = new Vector(),
            vNhatItemMap = new Vector(),
            vMob = new Vector(),
            vNpc = new Vector(),
            listInfoServer = new Vector(),
            listWorld = new Vector();

        public static MyHashtable hParty = new MyHashtable();
        public static int indexSize = 28,
            indexTitle,
            indexSelect,
            indexRow = -1,
            indexRowMax,
            indexMenu,
            indexCard = -1;

        public static ItemStands[] arrItemStands;
        public static short[] arrItemSprin;

        public static bool isViewClanInvite = true;
        public static bool isMessageMenu;

        public static bool isAutoDanh, isAutoNhatItem;

        public static Char currentCharViewInfo;

        public static sbyte vcData, vcMap, vcSkill, vcItem;

        public static MBitmap imgMenu,
            imgSkill,
            imgFocusActor;

        public static MBitmap imgQuest;
        public static Vector charnearByme = new Vector();

        public static MBitmap[] imgCloudy = new MBitmap[3];
        public static int wcloudy1, ncloudy1, wcloudy0, ncloudy0, kcwcloudy0, wcloudy2, ncloudy2;
        public static int[] xpaintcloudy1, xpaintcloudy0, ypaintcloudy1, ypaintcloudy0, xpaintcloudy2, ypaintcloudy2;


        //chat friend
        public static Command chat;

        public static Command bntIconChat;
        public static bool ispaintChat = false;
        public static bool isPaintQuest;

        //friend list gui
        public static Command btnUnfriend; //display quest info


        public static ShopMain guiQuest; //gui main quest

        public static bool isBag;

        // trade command
        public static Command cmdAcceptTrade;

        // quest main 
        public static QuestMain questMain;

        public static TradeGui tradeGui; //test thu


        private static readonly Skill[] keySkill = {null, null, null};
        private static readonly Skill[] onScreenSkill = {null, null, null, null, null};

        private static int shaking, count = 0;
        private static int deltaY = 20;

        public static bool isloadimgfocus;


        private static int yTouchBar;
        private static int xL, yL; // left
        private static int xC, yC;
        private static int xR, yR; // right
        private static int xF, yF; // fire
        private static int xU, yU; // up
        private static int xHP, yHP; // HP
        private static int xMP, yMP; // MP
        private static int xTG, yTG; // target
        private static int[] xS;
        private static int[] yS;
        private static int xSkill, ySkill, padSkill;


        public static int cmdBarY,
            hpBarX,
            hpBarY,
            hpBarW,
            girlHPBarY;

        public static int popupY, popupX;
        private static MBitmap imgNolearn;


        public static int widthGui = 237, heightGui = 232, xGui, yGui;

        public static int xstart, ystart, popupW = 140, popupH = 160;

        public static TField tfCharFriend;

        private string alertURL, fnick;
        private string[] arrClanInfo = null, arrClanDongGop = null;

        public int auto;


        public OtherChar c;
        public int changeKillID = 1, idEff;
        public Command cmdAcceptParty, cmdNo, cmdComfirmFriend;

        private Command cmdAddFriend;
        // char

        public Command cmdback, cmdBag, cmdSkill, cmdTiemnang, cmdtrangbi, cmdInfo, cmdFocus;
        private Command cmdBagSelectItem;
        protected Command cmdBagSortItem;
        protected Command cmdBagSplitItem;
        protected Command cmdBagThrowItem;
        protected Command cmdBagUseItem;
        private Command cmdBagViewItemInfo;
        public Command cmdCloseAll;
        protected Command cmdConvertMoveOut;
        private Command cmdItemInfoClose;
        private Command cmdPotentialAdd;
        private Command cmdSkillUp;
        protected Command cmdSplitMoveOut;
        private Command cmdTradeAccept;
        private Command cmdTradeLock;
        private Command cmdTradeMoveOut;
        private Command cmdTradeSelectItem;
        private Command cmdTradeSendMoney;
        protected Command cmdUpgradeMoveOut;

        public int cmxp, cmvxp, cmdxp, cmxLimp, cmyLimp, cmyp, cmvyp, cmdyp;
        private int cmY_Old, cmX_Old;


        //chat clan,world
        public GuiChatClanWorld guiChatClanWorld = new GuiChatClanWorld(-FatherChat.popw + 5,
            GameCanvas.h - FatherChat.poph + 7, new[] {"World", "Clan"});

        //gui Contact
        private readonly GuiContact guiContact = new GuiContact(GameCanvas.w / 2, GameCanvas.h / 2);

        //main guis
        public GuiMain guiMain;

        private int Hitem = 30, maxSizeRow = 5, isTranKyNang = 0;

        public int indexItemUse = -1, cLastFocusID = -1;

        public int indexKeyTouchAuto, timeDow, xStartAuto, yStartAuto, rangeAuto = 240;


        private int indexMember = 0;
        private int indexTiemNang = 0;
        public bool isLockKey;
        private bool isstarOpen, isChangeSkill = false, isShortcut = false;
        private bool isTran = false;

        private long lastFire;

        private long lastMove;

        private long lastSendUpdatePostion;
        private bool lockAutoMove;

        private long longPress = 0;

        public Command menu;
        private int moveUp, moveDow, idTypeTask;


        private int nSkill = 0;
        public int popx, popy, popw, poph;

        //quest screen
        public QShortQuest qShortQuest = new QShortQuest();

        private int selectedIndexSkill = -1;


        public Vector texts = new Vector();
        public string textsTitle;
        public TField tfText = null;

        private int yPaint;
        private int ypaintKill = 0, ylimUp = 0, ylimDow;

        //public RoomMiniGameScr roomMinigame; // test

        public GameScr()
        {
            //roomMinigame = new RoomMiniGameScr();
            if (GameCanvas.w == 128 || GameCanvas.h <= 208)
                indexSize = 20;
            
            Init();
            InitCanvas();
            InitTField();
            InitCommand();
        }


        public void perform(int idAction, object p)
        {
            switch (idAction)
            {
                case 10:
                    var menu3 = (MenuObject) p;
                    Npc npc = null;
                    for (var i = 0; i < vNpc.size(); i++)
                    {
                        var dem = (Npc) vNpc.elementAt(i);
                        if (dem != null && menu3 != null && dem.npcId == menu3.idActor)
                            npc = dem;
                    }
                    npc?.NhiemVu(false);
                    break;
                case 9999:
                    GameCanvas.ResetToLoginScr(true);
                    break;
            }
        }

        public void onCancelChat()
        {
        }

        //chat world
        public void onChatFromMe(string text, string to)
        {
            if (GameCanvas.isTouch)
                ChatTextField.GetInstance().isShow = false;
            if (text.Equals(""))
                return;
            if (to.Equals(MResources.PUBLICCHAT[0]))
                Service.GetInstance().ChatMap(text);

            else if (to.Equals(MResources.GLOBALCHAT[0]))
                Service.GetInstance().ChatGlobal(text);
        }


        public static void loadBegin()
        {
            sbyte[] d1, d2, d3, d4;
            d1 = Rms.loadRMS("dataVersion");
            d2 = Rms.loadRMS("mapVersion");
            d3 = Rms.loadRMS("skillVersion");
            d4 = Rms.loadRMS("itemVersion");
            if (d1 != null)
                vcData = d1[0];
            if (d2 != null)
                vcMap = d2[0];
            if (d3 != null)
                vcSkill = d3[0];
            if (d4 != null)
                vcItem = d4[0];
            flyTextX = new int[5];
            flyTextY = new int[5];
            flyTextDx = new int[5];
            flyTextDy = new int[5];
            flyTextState = new int[5];
            flyTextstring = new string[5];
            flyTextColor = new int[8];
            for (var i = 0; i < 5; i++)
                flyTextState[i] = -1;
        }


        public override void switchToMe()
        {
            base.switchToMe();
            isBag = false;
        }

        public static void readPart()
        {
            DataInputStream file = null;
            try
            {
                var data = Rms.loadRMS("nj_part");
                file = new DataInputStream(data);
                int sum = file.ReadShort();
                parts = new Part[sum];
                for (var i = 0; i < sum; i++)
                {
                    int type = file.ReadByte();
                    parts[i] = new Part(type);
                    for (var j = 0; j < parts[i].pi.Length; j++)
                    {
                        parts[i].pi[j] = new PartImage
                        {
                            id = file.ReadShort(),
                            dx = file.ReadByte(),
                            dy = file.ReadByte()
                        };
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
            finally
            {
                try
                {
                    file?.Close();
                }
                catch (IOException e)
                {
                    Debug.LogError(e);
                }
            }
        }

        public static void readEfect()
        {
            DataInputStream file = null;
            try
            {
                file = new DataInputStream(Rms.loadRMS("nj_effect"));
                int sum = file.ReadShort();
                efs = new EffectCharPaint[sum];
                for (var i = 0; i < sum; i++)
                {
                    efs[i] = new EffectCharPaint();
                    efs[i].idEf = file.ReadShort();
                    efs[i].arrEfInfo = new EffectInfoPaint[file.ReadByte()];
                    for (var j = 0; j < efs[i].arrEfInfo.Length; j++)
                    {
                        efs[i].arrEfInfo[j] = new EffectInfoPaint();
                        efs[i].arrEfInfo[j].idImg = file.ReadShort();
                        efs[i].arrEfInfo[j].dx = file.ReadByte();
                        efs[i].arrEfInfo[j].dy = file.ReadByte();
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
            finally
            {
                try
                {
                    file?.Close();
                }
                catch (IOException e)
                {
                    Debug.LogError(e);
                }
            }
        }


        public static void readSkill()
        {
            DataInputStream file = null;
            try
            {
                file = new DataInputStream(Rms.loadRMS("nj_skill"));
                int sum = file.ReadShort();

                sks = new SkillPaint[sum + 1];
                for (var i = 1; i < sum; i++)
                {
                    var skillId = i;
                    sks[skillId] = new SkillPaint();
                    sks[skillId].id = file.ReadShort();
                    sks[skillId].effId = file.ReadShort();
                    sks[skillId].numEff = file.ReadByte();
                    sks[skillId].skillStand = new SkillInfoPaint[file.ReadByte()];
                    for (var j = 0; j < sks[skillId].skillStand.Length; j++)
                    {
                        sks[skillId].skillStand[j] = new SkillInfoPaint();
                        sks[skillId].skillStand[j].status = file.ReadByte();
                        sks[skillId].skillStand[j].effS0Id = file.ReadShort();
                        sks[skillId].skillStand[j].e0dx = file.ReadShort();
                        sks[skillId].skillStand[j].e0dy = file.ReadShort();

                        sks[skillId].skillStand[j].effS1Id = file.ReadShort();
                        sks[skillId].skillStand[j].e1dx = file.ReadShort();
                        sks[skillId].skillStand[j].e1dy = file.ReadShort();

                        sks[skillId].skillStand[j].effS2Id = file.ReadShort();
                        sks[skillId].skillStand[j].e2dx = file.ReadShort();
                        sks[skillId].skillStand[j].e2dy = file.ReadShort();

                        sks[skillId].skillStand[j].arrowId = file.ReadShort();
                        sks[skillId].skillStand[j].adx = file.ReadShort();
                        sks[skillId].skillStand[j].ady = file.ReadShort();
                    }

                    sks[skillId].skillfly = new SkillInfoPaint[file.ReadByte()];
                    for (var j = 0; j < sks[skillId].skillfly.Length; j++)
                    {
                        sks[skillId].skillfly[j] = new SkillInfoPaint();
                        sks[skillId].skillfly[j].status = file.ReadByte();
                        sks[skillId].skillfly[j].effS0Id = file.ReadShort();
                        sks[skillId].skillfly[j].e0dx = file.ReadShort();
                        sks[skillId].skillfly[j].e0dy = file.ReadShort();

                        sks[skillId].skillfly[j].effS1Id = file.ReadShort();
                        sks[skillId].skillfly[j].e1dx = file.ReadShort();
                        sks[skillId].skillfly[j].e1dy = file.ReadShort();

                        sks[skillId].skillfly[j].effS2Id = file.ReadShort();
                        sks[skillId].skillfly[j].e2dx = file.ReadShort();
                        sks[skillId].skillfly[j].e2dy = file.ReadShort();

                        sks[skillId].skillfly[j].arrowId = file.ReadShort();
                        sks[skillId].skillfly[j].adx = file.ReadShort();
                        sks[skillId].skillfly[j].ady = file.ReadShort();
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
            finally
            {
                try
                {
                    file?.Close();
                }
                catch (IOException e)
                {
                    Debug.LogError(e);
                }
            }
        }


        public static GameScr GetInstance()
        {
            return GameCanvas.gameScr ?? (GameCanvas.gameScr = new GameScr());
        }

        private void InitCanvas()
        {
            popy = 50;
            popw = 100;
            poph = GameCanvas.h - 50 - 40;
            widthGui = 240;

            xGui = GameCanvas.w / 2 - widthGui / 2;

            popx = xGui - popw;
            popx = popx < 0 ? 0 : popx;
            yGui = 50;
            NodeChat.wnode = widthGui - 10;
            heightGui = GameCanvas.h - 50 - 40;
        }
        
        public void LoadData()
        {
            Service.GetInstance().RequestFriendList(Char.myChar().charID);
            Service.GetInstance().RequestMenuShop();
            Service.GetInstance().RequestShopInfo(0);
            Service.GetInstance().GetPricingInfo();
        }

        private void Init()
        {
            GuiMain.loadCmdBar();
            CRes.init();
            
            guiMain = new GuiMain();
            questMain = new QuestMain();
            
            var v = new Vector();
            v.addElement(new TabBag("Hành Trang"));
            v.addElement(new TabMySeftNew("Trang Bị"));
            v.addElement(new TabInfoChar("Thông tin"));
            v.addElement(new TabSkill("Kỹ năng"));
            v.addElement(new TabNangCap("Nâng cấp"));
            
            GameCanvas.AllInfo.addMoreTab(v);
        }

        private void InitCommand()
        {
            bntIconChat = new Command("", Constants.BUTTON_ICON_CHAT);
            bntIconChat.setPos(xGui + 5 + tfCharFriend.width + 5, tfCharFriend.y, LoadImageInterface.imgEmo[7],
                LoadImageInterface.imgEmo[7]);
            bntIconChat.w = 40;
            bntIconChat.h = 20;
            //create button sen chat
            chat = new Command("Gửi", Constants.BUTTON_SEND);
            chat.setPos(bntIconChat.x + bntIconChat.w + 5, tfCharFriend.y, imgSkill, imgSkill);

            chat.w = 60;
            chat.h = 20;

            cmdAddFriend = new Command(MResources.ACCEPT, 11002);
            cmdSkillUp = new Command(MResources.ADD, 11003);
            cmdPotentialAdd = new Command(MResources.ADD, 11006);

            cmdCloseAll = new Command(MResources.CLOSE, 11021);
            cmdBagSelectItem = new Command(MResources.SELECT, 11022);
            cmdBagViewItemInfo = new Command(GameCanvas.isTouch ? MResources.VIEW : "", 11023);
            cmdBagThrowItem = new Command(MResources.THROW, 11024);
            cmdBagSplitItem = new Command(MResources.SPLIT, 110244);
            cmdItemInfoClose = new Command(MResources.CLOSE, 11025);
            cmdBagUseItem = new Command(MResources.USE, 11026);
            cmdBagSortItem = new Command(MResources.SORT, 110221);
            cmdTradeSelectItem = new Command(MResources.SELECT, 11029);
            cmdback = new Command(MResources.BACK, 11021);
            menu = new Command(MResources.MENU, 11000);
            cmdFocus = new Command("Focus", 11001);

            cmdTradeLock = new Command(MResources.LOCK1, 11032);
            cmdTradeAccept = new Command(MResources.ACCEPT, 11033);
            cmdUpgradeMoveOut = new Command(MResources.MOVEOUT, 11034);
            cmdConvertMoveOut = new Command(MResources.MOVEOUT, 14014);
            cmdSplitMoveOut = new Command(MResources.MOVEOUT, 11035);
            cmdTradeSendMoney = new Command(MResources.SENDMONEY, 11036);
            cmdTradeMoveOut = new Command(MResources.MOVEOUT, 11037);
            cmdAcceptParty = new Command("Đồng ý", 111037);
            cmdComfirmFriend = new Command("Đồng ý", 111038);

            cmdAcceptTrade = new Command("Đồng ý", 111039);


            // ----- INIT COMMAND FOR TOUCHSCREEN
            if (!GameCanvas.isTouch || !GameCanvas.isTouchControl) return;
            menu.x = gW - 135;
            menu.y = 6;
            menu.img = imgMenu;

            cmdFocus.x = gW;
            cmdFocus.y = gH;

            if (!GameCanvas.isTouchControlSmallScreen) return;
            menu.x = gW / 2 - 38;
            menu.y = gH - 34;
        }


        public void doUpPotential()
        {
            if ((indexTitle <= 0 || indexTitle > 4) && !GameCanvas.isTouch)
                return;
            GameCanvas.inputDlg.show(MResources.INPUT_POINT, cmdPotentialAdd, TField.INPUT_TYPE_NUMERIC);
        }

        public void doUpSkill()
        {
            if (indexTitle <= 0 || indexTitle > 4)
                return;
            GameCanvas.inputDlg.show(MResources.INPUT_POINT, cmdSkillUp, TField.INPUT_TYPE_NUMERIC);
        }

        public void doAddFriend()
        {
            GameCanvas.inputDlg.show(MResources.BE_FRIEND_WITH, cmdAddFriend, TField.INPUT_TYPE_ANY);
        }


        protected void doMenuInforMe()
        {
            scrMain.clear();
            scrInfo.clear();
            cmdBag = new Command(MResources.MENUME[0], 1100011);
            cmdSkill = new Command(MResources.MENUME[1], 1100012);
            cmdTiemnang = new Command(MResources.MENUME[2], 1100013);
            cmdInfo = new Command(MResources.MENUME[3], 1100014);
            cmdtrangbi = new Command(MResources.MENUME[4], 1100015);

            var menu = new Vector();
            menu.addElement(cmdBag);
            menu.addElement(cmdSkill);
            menu.addElement(cmdTiemnang);
            menu.addElement(cmdInfo);
            menu.addElement(cmdtrangbi);
            menu.addElement(new Command(MResources.MENUME[5], 1100016));
            GameCanvas.menu.startAt(menu);
        }

        protected void doMenusynthesis()
        {
            var sub = new Vector();
            sub.addElement(new Command(MResources.SYNTHESIS[0], 110002));
            sub.addElement(new Command(MResources.SYNTHESIS[1], 1100032));
            sub.addElement(new Command(MResources.SYNTHESIS[2], 1100033));
            sub.addElement(new Command(MResources.CONFIG, LoginScr.GetInstance(), 1004, null));
            GameCanvas.menu.startAt(sub);
        }

        private void DoShowTeamUi()
        {
            resetButton();
            isLockKey = true;
            setPopupSize(175, 200);
            right = cmdCloseAll;
        }

        private void DoShowFriendUI()
        {
            resetButton();
            isLockKey = true;
            setPopupSize(175, 200);
            center = null;
            indexRow = -1;
        }


        public static void loadCamera(bool fullScreen, int cx, int cy)
        {
            gW = GameCanvas.w;

            cmdBarH = 39;

            gH = GameCanvas.h;
            girlHPBarY = 0;

            csPadMaxH = GameCanvas.h / 6;
            if (csPadMaxH < 48)
                csPadMaxH = 48;
            gW2 = gW >> 1;
            gH2 = gH >> 1;
            gW3 = gW / 3;
            gH3 = gH / 3;
            gW23 = gH - 120;
            gH23 = gH * 2 / 3;
            gW34 = 3 * gW / 4;
            gH34 = 3 * gH / 4;
            gW6 = gW / 6;
            gH6 = gH / 6;
            // Map --------------------
            gssw = gW / TileMap.GetInstance().size + 2;
            gssh = gH / TileMap.GetInstance().size + 2;
            if (gW % 24 != 0)
                gssw += 1;
            cmxLim = TileMap.tmw * TileMap.GetInstance().size - gW;
            cmyLim = TileMap.tmh * TileMap.GetInstance().size - gH;
            if (cx == -1 && cy == -1)
            {
                cmx = cmtoX = Char.myChar().cx - gW2 + gW6
                              * Char.myChar().cdir;
                cmy = cmtoY = Char.myChar().cy - gH23;
            }

            else
            {
                cmx = cmtoX = Char.myChar().cx - gW23 + gW6 * Char.myChar().cdir;
                cmy = cmtoY = Char.myChar().cy - gH23;
            }
            if (cmx < 24)
                cmx = cmtoX = 24;
            if (cmx > cmxLim)
                cmx = cmtoX = cmxLim;
            if (cmy < 0)
                cmy = cmtoY = 0;
            if (cmy > cmyLim)
                cmy = cmtoY = cmyLim;

            gssx = cmx / TileMap.GetInstance().size - 1;
            if (gssx < 0)
                gssx = 0;
            gssy = cmy / TileMap.GetInstance().size;
            gssxe = gssx + gssw;
            gssye = gssy + gssh;
            if (gssy < 0)
                gssy = 0;
            if (gssye > TileMap.tmh - 1)
                gssye = TileMap.tmh - 1;

            TileMap.countx = (gssxe - gssx) * 4;
            if (TileMap.countx > TileMap.tmw)
                TileMap.countx = TileMap.tmw;
            TileMap.county = (gssye - gssy) * 4;
            if (TileMap.county > TileMap.tmh)
                TileMap.county = TileMap.tmh;

            TileMap.gssx = (Char.myChar().cx - 2 * gW) / TileMap.GetInstance().size;
            if (TileMap.gssx < 0)
                TileMap.gssx = 0;
            TileMap.gssxe = TileMap.gssx + TileMap.countx;
            if (TileMap.gssxe > TileMap.tmw)
                TileMap.gssxe = TileMap.tmw;

            TileMap.gssy = (Char.myChar().cy - 2 * gH) / TileMap.GetInstance().size;
            if (TileMap.gssy < 0)
                TileMap.gssy = 0;

            TileMap.gssye = TileMap.gssy + TileMap.county;

            if (TileMap.gssye > TileMap.tmh)
                TileMap.gssye = TileMap.tmh;
            ChatTextField.GetInstance().parentScreen = instance;

            ChatTextField.GetInstance().tfChat.y = GameCanvas.h - 35 - ChatTextField.GetInstance().tfChat.height;

            if (GameCanvas.isTouch)
            {
                // INIT TOUCH CONTROL POSITION
                yTouchBar = gH - 88;

                xC = gW - 100;
                yC = 2;

                if (GameCanvas.isTouchControlSmallScreen)
                {
                    xC = gW / 2 - 2;
                    yC = yTouchBar + 50;
                }
                var xtrungtam = 3 * Image.getWidth(LoadImageInterface.imgMoveNormal) / 2;

                GuiMain.xCenter = xtrungtam;
                GuiMain.yCenter = yTouchBar + 60;

                GuiMain.xL = 3 * Image.getWidth(LoadImageInterface.imgMoveNormal) / 2 -
                             Image.getWidth(LoadImageInterface.imgMoveNormal) / 2;
                GuiMain.yL = GuiMain.yCenter; // left
                GuiMain.xR = xtrungtam + Image.getWidth(LoadImageInterface.imgMoveNormal) / 2;
                GuiMain.yR = GuiMain.yCenter; // right

                xF = gW - 50;
                yF = yTouchBar + 35; // fire

                GuiMain.xU = xtrungtam;
                GuiMain.yU = GuiMain.yCenter - Image.getWidth(LoadImageInterface.imgMoveNormal) / 2; // up


                GuiMain.gamePad = new GamePad();

                xC = (GuiMain.xL + GuiMain.xR) / 2;
                yC = (GuiMain.yU + GuiMain.yU) / 2;
                xHP = gW - 74;
                yHP = yTouchBar + 13; // hp

                xMP = gW - 85;
                yMP = yTouchBar + 50; // mp

                xTG = gW - 37;
                yTG = yTouchBar - 1;

                yU -= 15;

                yTG -= 12;
                yHP -= 7;
                xF -= 18;
                xTG -= 10;
                xHP -= 17;
                xMP -= 24;
            }


            xS = new int[onScreenSkill.Length];
            yS = new int[onScreenSkill.Length];
            if (GameCanvas.isTouch)
            {
                if (GameCanvas.isTouchControlSmallScreen)
                {
                    xSkill = 2;
                    ySkill = 55;
                    padSkill = 5;
                    for (var i = 0; i < xS.Length; i++)
                    {
                        xS[i] = i * (25 + padSkill);
                        yS[i] = ySkill;
                    }
                }
                else
                {
                    if (GameCanvas.w <= 320)
                        xSkill = gW2 - onScreenSkill.Length * 25 / 2 - 15;
                    else
                        xSkill = gW2 - onScreenSkill.Length * 25 / 2;
                    ySkill = yTouchBar + 58;
                    padSkill = 5;

                    for (var i = 0; i < xS.Length; i++)
                    {
                        xS[i] = i * (25 + padSkill);
                        yS[i] = ySkill;
                    }
                }
            }
            else
            {
                xSkill = 0;
                for (var i = 0; i < yS.Length; i++)
                {
                    xS[i] = 2;
                    yS[i] = 2 + 25 * i;
                }
            }
        }

        private static void UpdateCamera()
        {
            if (cmx != cmtoX || cmy != cmtoY)
            {
                cmvx = (cmtoX - cmx) << 2;
                cmvy = (cmtoY - cmy) << 2;

                cmdx += cmvx;

                cmx += cmdx >> 4;

                cmdx = cmdx & 0xf;

                cmdy += cmvy;
                cmy += cmdy >> 4;
                cmdy = cmdy & 0xf;

                if (cmx < 24)
                    cmx = 24;
                if (cmx > cmxLim)
                    cmx = cmxLim;
                if (cmy < 0)
                    cmy = 0;
                if (cmy > cmyLim)
                    cmy = cmyLim;
            }

            gssx = cmx / TileMap.GetInstance().size - 1;
            if (gssx < 0)
                gssx = 0;
            gssy = cmy / TileMap.GetInstance().size;
            gssxe = gssx + gssw;
            gssye = gssy + gssh;
            if (gssy < 0)
                gssy = 0;
            if (gssye > TileMap.tmh - 1)
                gssye = TileMap.tmh - 1;

            TileMap.gssx = (Char.myChar().cx - 2 * gW) / TileMap.GetInstance().size;
            if (TileMap.gssx < 0)
                TileMap.gssx = 0;
            TileMap.gssxe = TileMap.gssx + TileMap.countx;
            if (TileMap.gssxe > TileMap.tmw)
            {
                TileMap.gssxe = TileMap.tmw;
                TileMap.gssx = TileMap.gssxe - TileMap.countx;
            }

            TileMap.gssy = (Char.myChar().cy - 2 * gH) / TileMap.GetInstance().size;
            if (TileMap.gssy < 0)
                TileMap.gssy = 0;

            TileMap.gssye = TileMap.gssy + TileMap.county;

            if (TileMap.gssye > TileMap.tmh)
            {
                TileMap.gssye = TileMap.tmh;
                TileMap.gssy = TileMap.gssye - TileMap.county;
            }
            scrMain.updatecm();
        }

        public void resetButton()
        {
            if (Char.myChar().arrItemBag != null)
            {
            }

            GameCanvas.menu.showMenu = false;
            ChatTextField.GetInstance().close();
            ChatTextField.GetInstance().center = null;

            isMessageMenu = false;

            isLockKey = false;

            indexMenu = 0;
            indexSelect = 0;
            indexItemUse = -1;
            indexRow = -1;
            indexRowMax = 0;
            indexTitle = 0;
            left = menu;
            right = cmdFocus;
            center = null;

            scrMain.clear();
        }

        //execute event key press
        public override void keyPress(int keyCode)
        {
            if (tfText != null && tfText.isFocus)
                tfText.keyPressed(keyCode);
            if (tfCharFriend != null && tfCharFriend.isFocus)
                tfCharFriend.keyPressed(keyCode);
            if (isBag)
                GameCanvas.AllInfo.keyPress(keyCode);

            guiChatClanWorld?.KeyPress(keyCode);
            
            guiMain.keyPress(keyCode);
            base.keyPress(keyCode);
        }

        public override void updateKey()
        {
            //roomMinigame.update();
            if (isAutoDanh && (GameCanvas.keyPressed[2] || GameCanvas.keyPressed[6] || GameCanvas.keyPressed[4]))
                isAutoDanh = false;
            if (!GameCanvas.gameScr.guiChatClanWorld.moveClose && !GameCanvas.menu.showMenu)
                guiMain.UpdateKey();
            if (MenuIcon.isShowTab) return;

            guiChatClanWorld.updatePointer();

            guiChatClanWorld.UpdateKey();

            var isUpdatePhim = false;
            qShortQuest?.UpdateKey();


            if (!MenuIcon.isShowTab && (getCmdPointerLast(guiMain.bntAttack) || GameCanvas.keyPressed[5]))
            {
                GameCanvas.keyPressed[5] = false;

                DoTouchQuickSlot(0);
                isUpdatePhim = true;
            }
            if (!MenuIcon.isShowTab && getCmdPointerLast(guiMain.bntAttack_1))
            {
                DoTouchQuickSlot(1);
                isUpdatePhim = true;
            }
            if (!MenuIcon.isShowTab && getCmdPointerLast(guiMain.bntAttack_2))
            {
                DoTouchQuickSlot(2);
                isUpdatePhim = true;
            }
            if (!MenuIcon.isShowTab && getCmdPointerLast(guiMain.bntAttack_3))
            {
                DoTouchQuickSlot(3);
                isUpdatePhim = true;
            }
            if (!MenuIcon.isShowTab && getCmdPointerLast(guiMain.bntAttack_4))
            {
                DoTouchQuickSlot(4);
                isUpdatePhim = true;
            }
            if (ispaintChat)
            {
                if (getCmdPointerLast(chat))
                    if (chat != null)
                    {
                        GameCanvas.isPointerJustRelease = false;
                        keyTouch = -1;
                        chat?.performAction();
                    }
                if (getCmdPointerLast(bntIconChat))
                    if (bntIconChat != null)
                    {
                        GameCanvas.isPointerJustRelease = false;
                        keyTouch = -1;
                        bntIconChat?.performAction();
                    }
                if (Char.toCharChatSelected != null)
                    Char.toCharChatSelected.update();
            }

            //chat private
            if (GameCanvas.menu.showMenu)
                return;
            if (InfoDlg.isLock)
                return;

            if (getCmdPointerLast(btnUnfriend))
                if (btnUnfriend != null)
                {
                    GameCanvas.isPointerJustRelease = false;
                    GameCanvas.keyPressed[5] = false;
                    keyTouch = -1;
                    if (btnUnfriend != null)
                        btnUnfriend.performAction();
                }

            //chat private

            if (Char.myChar().currentMovePoint != null)
                for (var i = 0; i < GameCanvas.keyPressed.Length; i++)
                    if (GameCanvas.keyPressed[i])
                    {
                        Char.myChar().currentMovePoint = null;
                        break;
                    }

            if (ChatTextField.GetInstance().isShow && GameCanvas.keyAsciiPress != 0)
            {
                ChatTextField.GetInstance().keyPressed(GameCanvas.keyAsciiPress);
                GameCanvas.keyAsciiPress = 0;
            }

            if (GameCanvas.menu.showMenu || Char.isLockKey)
                return;

            if (GameCanvas.keyPressed[10])
            {
                GameCanvas.keyPressed[10] = false;
                GameCanvas.clearKeyPressed();
            }
            if (GameCanvas.keyPressed[11])
            {
                GameCanvas.keyPressed[11] = false;
                GameCanvas.clearKeyPressed();
            }
            if (GameCanvas.keyAsciiPress != 0)
                if (TField.isQwerty)
                    if (GameCanvas.keyAsciiPress == ' ')
                    {
                        GameCanvas.keyAsciiPress = 0;
                        GameCanvas.clearKeyPressed();
                    }
                    else if (GameCanvas.keyAsciiPress == '@')
                    {
                        GameCanvas.keyAsciiPress = 0;
                        GameCanvas.clearKeyPressed();
                    }
                    else if (GameCanvas.keyAsciiPress == '0')
                    {
                        GameCanvas.keyAsciiPress = 0;
                        GameCanvas.clearKeyPressed();
                    }
                    else if (GameCanvas.keyAsciiPress == '?')
                    {
                        GameCanvas.keyAsciiPress = 0;
                        GameCanvas.clearKeyPressed();
                    }

            if (Char.myChar().skillPaint != null)
                return;
            
            if (Char.myChar().statusMe == Char.A_STAND)
            {
                if (!MenuIcon.isShowTab && getCmdPointerLast(guiMain.bntAttack))
                {
                    GameCanvas.keyPressed[5] = false;
                    isUpdatePhim = true;
                    DoTouchQuickSlot(0);

                }
                else if (GameCanvas.keyHold[2])
                {
                    Char.myChar().UpdateCharJump();
                    isUpdatePhim = true;
                    if (!Char.myChar().isLockMove && !Char.myChar().isBlinking)
                        setCharJump(0);
                    Char.myChar().UpdateCharJump();
                    isAutoDanh = false;
                }
                else if (GameCanvas.keyHold[1])
                {
                    Char.myChar().cdir = -1;
                    isUpdatePhim = true;
                    if (!Char.myChar().isLockMove && !Char.myChar().isBlinking)
                        setCharJump(-4);
                    isAutoDanh = false;
                }
                else if (GameCanvas.keyHold[3])
                {
                    Char.myChar().cdir = 1;
                    isUpdatePhim = true;
                    if (!Char.myChar().isLockMove && !Char.myChar().isBlinking)
                        setCharJump(4);
                    isAutoDanh = false;
                }
                else if (GameCanvas.keyHold[4])
                {
                    Char.myChar().isAttack = false;
                    isUpdatePhim = true;
                    Char.myChar().UpdateCharRun();
                    isAutoDanh = false;
                    if (Char.myChar().cdir == 1)
                    {
                        Char.myChar().cdir = -1;
                    }
                    else if (!Char.myChar().isLockMove && !Char.myChar().isBlinking)
                    {
//                        if (Char.myChar().cx - Char.myChar().cxSend != 0)
//                            Service.GetInstance().CharMove();
//                        Char.myChar().updateCharRun();
                        Char.myChar().statusMe = Char.A_RUN;
                        Char.myChar().isStartSoundRun = true;
                        Music.play(Music.RUN, 12);
                        Char.myChar().cvx = -Char.myChar().getSpeed();
                    }
                }
                else if (GameCanvas.keyHold[6])
                {
                    Char.myChar().isAttack = false;
                    isUpdatePhim = true;
                    Char.myChar().UpdateCharRun();
                    isAutoDanh = false;
                    if (Char.myChar().cdir == -1)
                    {
                        Char.myChar().cdir = 1;
                    }
                    else if (!Char.myChar().isLockMove && !Char.myChar().isBlinking)
                    {
//                        if (Char.myChar().cx - Char.myChar().cxSend != 0)
//                            Service.GetInstance().CharMove();
//                        Char.myChar().updateCharRun();
                        Char.myChar().statusMe = Char.A_RUN;
                        Char.myChar().isStartSoundRun = true;
                        Music.play(Music.RUN, 12);
                        isAutoDanh = false;
                        isAutoDanh = false;
                        Char.myChar().cvx = Char.myChar().getSpeed();
                    }
                }
            }
            else if (Char.myChar().statusMe == Char.A_RUN)
            {
                if (!MenuIcon.isShowTab && GameCanvas.keyPressed[5])
                {
                    GameCanvas.keyPressed[5] = false;
                    isUpdatePhim = true;
                    DoTouchQuickSlot(0);
                }
                else if (GameCanvas.keyHold[2])
                {
                    Service.GetInstance().CharMove();
//                    if (Char.myChar().cx - Char.myChar().cxSend != 0 || Char.myChar().cy - Char.myChar().cySend != 0)
//                        Service.GetInstance().CharMove();

                    isUpdatePhim = true;
                    Char.myChar().cvy = Char.myChar().canJumpHigh ? -10 : -8;
                    Char.myChar().statusMe = Char.A_JUMP;
                    isAutoDanh = false;
                    Char.myChar().cp1 = 0;
                }
                else if (GameCanvas.keyHold[1])
                {
                    Service.GetInstance().CharMove();
//                    if (Char.myChar().cx - Char.myChar().cxSend != 0 || Char.myChar().cy - Char.myChar().cySend != 0)
//                        Service.GetInstance().CharMove();
                    Char.myChar().cdir = -1;
                    Char.myChar().cvy = Char.myChar().canJumpHigh ? -10 : -8;
                    Char.myChar().cvx = -4;
                    Char.myChar().statusMe = Char.A_JUMP;
                    isAutoDanh = false;
                    Char.myChar().cp1 = 0;
                    isUpdatePhim = true;
                }
                else if (GameCanvas.keyHold[3])
                {
                    Service.GetInstance().CharMove();
//                    if (Char.myChar().cx - Char.myChar().cxSend != 0 || Char.myChar().cy - Char.myChar().cySend != 0)
//                        Service.GetInstance().CharMove();
                    Char.myChar().cdir = 1;
                    Char.myChar().cvy = Char.myChar().canJumpHigh ? -10 : -8;
                    Char.myChar().cvx = 4;
                    Char.myChar().statusMe = Char.A_JUMP;
                    isAutoDanh = false;
                    Char.myChar().cp1 = 0;
                    isUpdatePhim = true;
                }
                else if (GameCanvas.keyHold[4])
                {
                    if (Char.myChar().cdir == 1)
                        Char.myChar().cdir = -1;
                    else
                        Char.myChar().cvx = -Char.myChar().getSpeed() + Char.myChar().cBonusSpeed;
                    isUpdatePhim = true;
                }
                else if (GameCanvas.keyHold[6])
                {
                    if (Char.myChar().cdir == -1)
                        Char.myChar().cdir = 1;
                    else
                        Char.myChar().cvx = Char.myChar().getSpeed() + Char.myChar().cBonusSpeed;

                    isUpdatePhim = true;
                }
            }
            else if (Char.myChar().statusMe == Char.A_JUMP)
            {
                if (!MenuIcon.isShowTab && GameCanvas.keyPressed[5])
                {
                    GameCanvas.keyPressed[5] = false;
                    isUpdatePhim = true;
                    DoTouchQuickSlot(0);
                }
                if (GameCanvas.keyHold[4] || GameCanvas.keyHold[1])
                {
                    if (Char.myChar().cdir == 1)
                        Char.myChar().cdir = -1;
                    else
                        Char.myChar().cvx = -Char.myChar().getSpeed();		
                    isAutoDanh = false;
                }
                else if (GameCanvas.keyHold[6] || GameCanvas.keyHold[3])
                {
                    if (Char.myChar().cdir == -1)
                        Char.myChar().cdir = 1;
                    else
                        Char.myChar().cvx = Char.myChar().getSpeed();
                    isAutoDanh = false;
                }
                if (GameCanvas.keyHold[2] || GameCanvas.keyHold[1] || GameCanvas.keyHold[3])
                {
                    if (Char.myChar().canJumpHigh && Char.myChar().cp1 == 0 && Char.myChar().cvy > -4)
                    {
                        Char.myChar().cp1++;
                        Char.myChar().cvy = -7;
                    }

                    isAutoDanh = false;
                }
            }
            else if (Char.myChar().statusMe == Char.A_FALL)
            {
                if (!MenuIcon.isShowTab && GameCanvas.keyPressed[5])
                {
                    GameCanvas.keyPressed[5] = false;

                    DoTouchQuickSlot(0);
                }
                if (GameCanvas.keyPressed[2])
                    GameCanvas.clearKeyPressed();
                if (GameCanvas.keyHold[4])
                    if (Char.myChar().cdir == 1)
                        Char.myChar().cdir = -1;
                    else
                        Char.myChar().cvx = -Char.myChar().getSpeed();
                else if (GameCanvas.keyHold[6])
                    if (Char.myChar().cdir == -1)
                        Char.myChar().cdir = 1;
                    else
                        Char.myChar().cvx = Char.myChar().getSpeed();
            }
            else if (Char.myChar().statusMe == Char.A_WATERRUN)
            {
                if (!MenuIcon.isShowTab && GameCanvas.keyPressed[5])
                {
                    GameCanvas.keyPressed[5] = false;

                    DoTouchQuickSlot(0);
                }
                if (GameCanvas.keyHold[2])
                {
                    Service.GetInstance().CharMove();
//                    if (Char.myChar().cx - Char.myChar().cxSend != 0 || Char.myChar().cy - Char.myChar().cySend != 0)
//                        Service.GetInstance().CharMove();
                    Char.myChar().cvy = -10;
                    Char.myChar().statusMe = Char.A_JUMP;
                    Char.myChar().cp1 = 0;
                }
                else if (GameCanvas.keyHold[4])
                {
                    if (Char.myChar().cdir == 1)
                        Char.myChar().cdir = -1;
                    else
                        Char.myChar().cvx = -5;
                }
                else if (GameCanvas.keyHold[6])
                {
                    if (Char.myChar().cdir == -1)
                        Char.myChar().cdir = 1;
                    else
                        Char.myChar().cvx = 5; // Char.cwspeed + Char.cBonusSpeed;
                }
            }
            else if (Char.myChar().statusMe == Char.A_ATTK)
            {
                if (!MenuIcon.isShowTab && GameCanvas.keyPressed[5])
                {
                    GameCanvas.keyPressed[5] = false;

                    DoTouchQuickSlot(0);
                }
                if (GameCanvas.keyHold[4])
                {
                    if (Char.myChar().cdir == 1)
                        Char.myChar().cdir = -1;
                    else
                        Char.myChar().cvx = -Char.myChar().getSpeed() + 2;

                    isAutoDanh = false;
                }
                else if (GameCanvas.keyHold[6])
                {
                    if (Char.myChar().cdir == -1)
                        Char.myChar().cdir = 1;
                    else
                        Char.myChar().cvx = Char.myChar().getSpeed() - 2;
                    isAutoDanh = false;
                }
            }
            else if (Char.myChar().statusMe == Char.A_WATERDOWN)
            {
                if (!MenuIcon.isShowTab && GameCanvas.keyPressed[5])
                {
                    GameCanvas.keyPressed[5] = false;

                    DoTouchQuickSlot(0);
                }
                if (GameCanvas.keyHold[2])
                {
                    Char.myChar().cvy = -10;
                    Char.myChar().statusMe = Char.A_JUMP;
                    Char.myChar().cp1 = 0;
                }
            }
            
            if (GameCanvas.keyPressed[Key.NUM8] && GameCanvas.keyAsciiPress != '8')
                GameCanvas.keyPressed[Key.NUM8] = false;

            guiChatClanWorld.updatePointer();
            guiContact.UpdateKey();

            if (!isUpdatePhim && GameCanvas.isPointerJustRelease)
            {
                for (var i = 0; i < vNpc.size(); i++)
                {
                    var c = (Npc) vNpc.elementAt(i);
                    if (c != null && GameCanvas.isPointer(c.cx - 20 - cmx, c.cy - 60 - cmy, 40, 60))
                    {
                        Char.myChar().clearAllFocus();
                        Char.myChar().npcFocus = c;
                        GameCanvas.isPointerJustRelease = false;
                        return;
                    }
                }
                for (var i = 0; i < vCharInMap.size(); i++)
                {
                    var c = (Char) vCharInMap.elementAt(i);
                    if (c != null && GameCanvas.isPointer(c.cx - 20 - cmx, c.cy - 60 - cmy, 40, 60))
                        if (c.charID == Char.myChar().charID &&
                            (Char.myChar().statusMe == Char.A_DEAD || Char.myChar().statusMe == Char.A_DEADFLY))
                        {
                            GameCanvas.startCommandDlg("Bạn muốn hồi sinh tại chỗ (1 gold)?",
                                new Command("Hồi sinh", GameCanvas.instance, GameCanvas.cHoiSinh, null),
                                new Command("Về làng", GameCanvas.instance, GameCanvas.cVeLang, null));
                            return;
                        }
                        else if (c.charID != Char.myChar().charID)
                        {
                            Char.myChar().clearAllFocus();
                            Char.myChar().charFocus = c;
                            Service.GetInstance().RequestPlayerInfo((short) c.charID);
                            GameCanvas.isPointerJustRelease = false;
                            return;
                        }
                }
                for (var i = 0; i < vMob.size(); i++)
                {
                    var c = (Mob) vMob.elementAt(i);
                    if (c != null &&
                        GameCanvas.isPointer(c.x - c.getW() / 2 - cmx, c.y - c.getH() - cmy, c.getW(), c.getH()))
                    {
                        if (c.mobName == null || c.mobName.Equals(""))
                            Service.GetInstance().RequestMonsterInfo(c.mobId);
                        if (Char.myChar().mobFocus != null && c.mobId == Char.myChar().mobFocus.mobId)
                        {
                            DoTouchQuickSlot(0);
                            if (!isAutoDanh)
                            {
                                xStartAuto = Char.myChar().cx;
                                yStartAuto = Char.myChar().cy;
                            }
                            var listauto = Rms.loadRMS(Rms.autoSetting);
                            if (listauto?[SettingScreen.AUTO_DANH] == 1)
                                isAutoDanh = true;

                            return;
                        }
                        if (c.isBoss && !Mob.isBossAppear) continue;
                        Char.myChar().clearAllFocus();
                        Char.myChar().mobFocus = c;
                        GameCanvas.isPointerJustRelease = false;
                        if (Char.myChar().statusMe == Char.A_DEAD || Char.myChar().statusMe == Char.A_DEADFLY)
                            return;
                        var maxkcX = CRes.abs(Char.myChar().cx - c.x);
                        int kc = 1, leng = 0;
                        if (maxkcX > 20 || CRes.abs(Char.myChar().cy - c.y) > 10)
                        {
                            for (var j = 0; j < maxkcX; j++)
                            {
                                leng += j;
                                if (leng >= maxkcX)
                                {
                                    kc = j - 1;
                                    break;
                                }
                            }
                            if (Char.myChar().cx > c.x)
                            {
                                Char.myChar().UpdateCharRun();
                                if (Char.myChar().cdir == 1)
                                    Char.myChar().cdir = -1;
                                if (!Char.myChar().isLockMove && !Char.myChar().isBlinking)
                                {
//                                    if (Char.myChar().cx - Char.myChar().cxSend != 0)
//                                        Service.GetInstance().CharMove();
                                    Char.myChar().UpdateCharRun();
                                    Char.myChar().statusMe = Char.A_RUN;

                                    Char.myChar().cvx = -kc;
                                }
                            }
                            else
                            {
                                Char.myChar().UpdateCharRun();
                                if (Char.myChar().cdir == -1)
                                    Char.myChar().cdir = 1;
                                if (!Char.myChar().isLockMove && !Char.myChar().isBlinking)
                                {
//                                    if (Char.myChar().cx - Char.myChar().cxSend != 0)
//                                        Service.GetInstance().CharMove();
                                    Char.myChar().UpdateCharRun();
                                    Char.myChar().statusMe = Char.A_RUN;
                                    Char.myChar().cvx = kc;
                                }
                            }
                        }
                        return;
                    }
                }
                for (var i = 0; i < vItemMap.size(); i++)
                {
                    var c = (ItemMap) vItemMap.elementAt(i);
                    if (c != null && GameCanvas.isPointer(c.x - 14 - cmx, c.y - 28 - cmy, 14, 28))
                    {
                        Char.myChar().clearAllFocus();
                        Char.myChar().itemFocus = c;
                        GameCanvas.isPointerJustRelease = false;
                        return;
                    }
                }
            }
            GameCanvas.clearKeyPressed();
        }


        private void setCharJump(int cvx)
        {
            if (Char.myChar().cx - Char.myChar().cxSend != 0 || Char.myChar().cy - Char.myChar().cySend != 0)
                Service.GetInstance().CharMove();
            Char.myChar().cvy = Char.myChar().canJumpHigh ? -10 : -8;
            Char.myChar().cvx = cvx;
            Char.myChar().statusMe = Char.A_JUMP;
            Char.myChar().cp1 = 0;
        }

        private int countDown = 0;
        public override void update()
        {
            countDown++;
            if (countDown >= 2)
            {
                countDown = 0;
                return;
            }
            FallingLeafEffect.UpdateFallingLeaf();
            UpdateCloudy();
            if (GameCanvas.imgBG != null)
                for (var i = 0; i < GameCanvas.imgBG.Length; i++)
                    if (GameCanvas.imgBG[i] == null)
                    {
                        GameCanvas.loadBG(2);
                        break;
                    }

            scrMain.updatecm();

            guiChatClanWorld?.Update();
            if (isBag)
            {
                GameCanvas.AllInfo.updatekey();
                GameCanvas.AllInfo.update();
            }

	
            for (var i = 0; i < veffClient.size(); i++)
            {
                var eff = (MainEffect) veffClient.elementAt(i);
                eff.update();
            }
            guiMain.update();	
            UpdateCamera();

            ChatTextField.GetInstance().update();
            for (var i = 0; i < vCharInMap.size(); i++)
                ((Char) vCharInMap.elementAt(i)).update();
            for (var i = 0; i < vItemMap.size(); i++)
                // paint item
                ((ItemMap) vItemMap.elementAt(i)).update();
            for (var i = 0; i < vMob.size(); i++)
                ((Mob) vMob.elementAt(i)).update();
       
            for (var i = 0; i < vNpc.size(); i++)
                ((Npc) vNpc.elementAt(i)).update();

            updateFlyText();
            for (var i = 0; i < Effect.vEffect2.size(); i++)
            {
                var l = (Effect) Effect.vEffect2.elementAt(i);
                l.update();
            }	
            AutoFocus();
            if (isBag)
                GameCanvas.AllInfo.updatePointer();
            qShortQuest?.Update();
            guiContact.Update();
            if (isAutoDanh)
                AutoDanh();
            if (isAutoNhatItem)
                AutoNhat();
        }


        public static void loadImages()
        {
            imgFocusActor = GameCanvas.loadImage("/GuiNaruto/imgFocus.png");
            isloadimgfocus = true;
            imgQuest = GameCanvas.loadImage("/imgNpcQuest.png");
            for (var i = 0; i < imgCloudy.Length; i++)
            {
                imgCloudy[i] = GameCanvas.loadImage("/bg/may" + i + ".png");
                ;
            }
            wcloudy0 = MGraphics.getImageWidth(imgCloudy[0]);
            ncloudy0 = GameCanvas.w / wcloudy0 + 1;
            xpaintcloudy0 = new int[ncloudy0 * 3];
            ypaintcloudy0 = new int[ncloudy0 * 3];
            kcwcloudy0 = GameCanvas.w % wcloudy0;
            for (var i = 0; i < xpaintcloudy0.Length; i++)
            {
                xpaintcloudy0[i] = (wcloudy0 + kcwcloudy0) * i;
                ypaintcloudy0[i] = CRes.random(GameCanvas.h / 2, GameCanvas.h);
            }
            wcloudy1 = MGraphics.getImageWidth(imgCloudy[1]);
            ncloudy1 = GameCanvas.w / wcloudy1 + 2;
            xpaintcloudy1 = new int[ncloudy1 * 3];
            ypaintcloudy1 = new int[ncloudy1 * 3];
            for (var i = 0; i < xpaintcloudy1.Length; i++)
            {
                xpaintcloudy1[i] = wcloudy1 * i;
                ypaintcloudy1[i] = CRes.random(GameCanvas.h / 2, GameCanvas.h);
            }

            wcloudy1 = MGraphics.getImageWidth(imgCloudy[2]);
            ncloudy1 = GameCanvas.w / wcloudy1 + 2;
            xpaintcloudy2 = new int[ncloudy1 * 3];
            ypaintcloudy2 = new int[ncloudy1 * 3];
            for (var i = 0; i < xpaintcloudy2.Length; i++)
            {
                xpaintcloudy2[i] = wcloudy1 * i;
                ypaintcloudy2[i] = CRes.random(GameCanvas.h / 2, GameCanvas.h);
            }
        }


        public override void paint(MGraphics g)
        {
            g.setColor(0x3aadfe);
            g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
            GameCanvas.paintBGGameScr(g);
            g.translate(-cmx, -cmy);
            PaintCloudy0(g);
            PaintCloudy1(g);
            PaintCloudy2(g);
            TileMap.GetInstance().paintMap(g);
            FallingLeafEffect.PaintFallingLeaf(g);
            for (var i = 0; i < vNpc.size(); i++)
                ((Npc) vNpc.elementAt(i)).paint(g);
            for (var i = 0; i < vMob.size(); i++)
                ((Mob) vMob.elementAt(i)).paint(g);
            if (Char.myChar().mobFocus != null)
                paintInfoMod(g);
            if (Char.myChar().npcFocus != null)
                g.drawImage(imgFocusActor, Char.myChar().npcFocus.cx,
                    Char.myChar().npcFocus.cy - Char.myChar().npcFocus.ch - 20 + GameCanvas.gameTick % 20 / 4,
                    MGraphics.VCENTER | MGraphics.HCENTER, true);


            if (Char.myChar().charFocus != null)
                g.drawImage(imgFocusActor, Char.myChar().charFocus.cx,
                    Char.myChar().charFocus.cy - Char.myChar().charFocus.ch - 20 + GameCanvas.gameTick % 20 / 4,
                    MGraphics.VCENTER | MGraphics.HCENTER, true);

            if (Char.myChar().mobFocus != null)
                g.drawImage(imgFocusActor, Char.myChar().mobFocus.x,
                    Char.myChar().mobFocus.y - Char.myChar().mobFocus.h - 35 + GameCanvas.gameTick % 20 / 4,
                    MGraphics.VCENTER | MGraphics.HCENTER, true);
            if (Char.myChar().itemFocus != null)
                g.drawImage(imgFocusActor, Char.myChar().itemFocus.x,
                    Char.myChar().itemFocus.y - 65 + GameCanvas.gameTick % 20 / 4, MGraphics.VCENTER | MGraphics.HCENTER,
                    true);

            for (var i = 0; i < vCharInMap.size(); i++)
            {
                // paint char trong map
                Char c = null;
                try
                {
                    c = (Char) vCharInMap.elementAt(i);
                }
                catch (Exception e)
                {
                }

                c.paint(g);
            }
            for (var i = 0; i < vCharInMap.size(); i++)
            {
                var c = (Char) vCharInMap.elementAt(i);
                if (c != null && c.skillPaint != null && c.statusMe != Char.A_DEAD && c.statusMe != Char.A_DEADFLY)
                    c.paintCharWithSkill(g);
            }

            paintFlyText(g);
            for (var i = 0; i < veffClient.size(); i++)
            {
                var eff = (MainEffect) veffClient.elementAt(i);
                eff.paint(g);
            }
            paintWaypointArrow(g);
            ChatTextField.GetInstance().paint(g);

            for (var i = 0; i < vItemMap.size(); i++)
            {
                // paint item
                ((ItemMap) vItemMap.elementAt(i)).paint(g);
            }
               

            for (var i = 0; i < Effect.vEffect2.size(); i++)
            {
                var l = (Effect) Effect.vEffect2.elementAt(i);
                l.paint(g);
            }

            for (var i = 0; i < Effect.vEffect2Outside.size(); i++)
            {
                var l = (Effect) Effect.vEffect2Outside.elementAt(i);
                l.paint(g);
            }
            paintBgItem(g, 2);
            paintBgItem(g, 3);
            for (var a = 0; a < TileMap.vCurrItem.size(); a++)
            {
                var bi = (BgItem) TileMap.vCurrItem.elementAt(a);
                if (bi.idImage != -1)
                    if (bi.layer > 3 && GuiMain.isPaintOjectMap)
                        bi.paint(g);
            }


            base.paint(g);


            ChatTextField.GetInstance().paint(g);


            //paint short quest
            if (qShortQuest != null)
                qShortQuest.Paint(g);


            paintCmdBar(g);
            guiMain.Paint(g);
            //roomMinigame.paint(g); // test 

            if (isBag)
            {
                g.setColor(0x000000, GameCanvas.opacityTab);
                g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
                g.disableBlending();
                GameCanvas.AllInfo.paint(g);
            }

            if (guiChatClanWorld != null)
                if (!MenuIcon.isShowTab)
                    guiChatClanWorld.Paint(g);
        }


        public static void resetTranslate(MGraphics g)
        {
            g.translate(-g.getTranslateX(), -g.getTranslateY());
            g.setClip(0, -200, GameCanvas.w, 200 + GameCanvas.h);
        }


        public static void startFlyText(string flyString, int x, int y, int dx, int dy, int color)
        {
            var n = -1;
            for (var i = 0; i < 5; i++)
                if (flyTextState[i] == -1)
                {
                    n = i;
                    break;
                }
            if (n == -1)
                return;
            flyTextColor[n] = color;
            flyTextstring[n] = flyString;
            flyTextX[n] = x;
            flyTextY[n] = y;
            flyTextDx[n] = dx;
            flyTextDy[n] = dy;
            flyTextState[n] = 0;
        }

        public static void updateFlyText()
        {
            for (var i = 0; i < 5; i++)
                if (flyTextState[i] != -1)
                {
                    flyTextState[i] += CRes.abs(flyTextDy[i]);
                    if (flyTextState[i] > 30)
                        flyTextState[i] = -1;
                    flyTextX[i] += flyTextDx[i];
                    flyTextY[i] += flyTextDy[i];
                }
        }


        public static void paintFlyText(MGraphics g)
        {
            for (var i = 0; i < 5; i++)
                if (flyTextState[i] != -1)
                {
                    if (!GameCanvas.isPaint(flyTextX[i], flyTextY[i]))
                        continue;
                
                    if (flyTextColor[i] == MFont.RED)
                        FontManager.GetInstance().number_red_hp.DrawStringBorder(g, flyTextstring[i], flyTextX[i], flyTextY[i], MFont.CENTER);
                    else if (flyTextColor[i] == MFont.YELLOW)
                        FontManager.GetInstance().number_yellow_xp.DrawStringBorder(g, flyTextstring[i], flyTextX[i], flyTextY[i], MFont.CENTER);
                    else if (flyTextColor[i] == MFont.GREEN)
                        FontManager.GetInstance().number_green_mp.DrawStringBorder(g, flyTextstring[i], flyTextX[i], flyTextY[i], MFont.CENTER);
                    else if (flyTextColor[i] == MFont.FATAL)
                        FontManager.GetInstance().tahoma_7b_yellow.DrawString(g, flyTextstring[i], flyTextX[i], flyTextY[i], MFont.CENTER,FontManager.GetInstance().tahoma_7b_blue);
                    else if (flyTextColor[i] == MFont.FATAL_ME)
                        FontManager.GetInstance().tahoma_7b_white.DrawString(g, flyTextstring[i], flyTextX[i], flyTextY[i], MFont.CENTER,FontManager.GetInstance().tahoma_7b_blue);
                    else if (flyTextColor[i] == MFont.MISS)
                        SmallImage.DrawSmallImage(g, 1062, flyTextX[i], flyTextY[i], 0, MGraphics.VCENTER | MGraphics.HCENTER);
                    else if (flyTextColor[i] == MFont.ORANGE)
                        FontManager.GetInstance().number_orange.DrawString(g, flyTextstring[i], flyTextX[i], flyTextY[i], MFont.CENTER);
                    else if (flyTextColor[i] == MFont.ADDMONEY)
                        FontManager.GetInstance().tahoma_7_yellow.DrawString(g, flyTextstring[i], flyTextX[i], flyTextY[i], MFont.CENTER, FontManager.GetInstance().tahoma_7_red);
                    else if (flyTextColor[i] == MFont.MISS_ME)
                        SmallImage.DrawSmallImage(g, 655, flyTextX[i], flyTextY[i], 0, MGraphics.VCENTER | MGraphics.HCENTER);
                }
        }

        private static void paintCmdBar(MGraphics g)
        {
            g.setClip(0, cmdBarY - 4, GameCanvas.w, 100);

            // // PAINT HP
            var hpWidth = Char.myChar().cHP * hpBarW / Char.myChar().cMaxHP;
            if (hpWidth > hpBarW)
                hpWidth = 0;
            g.setColor(0x770000);
            g.fillRect(hpBarX + 4, hpBarY - 2, hpWidth, 10);

            g.setColor(0xCC0000);
            g.fillRect(hpBarX + 4, hpBarY - 2 + 1, hpWidth, 10);
            // paint MP
            hpWidth = Char.myChar().cMP * hpBarW / Char.myChar().cMaxMP;
            if (hpWidth > hpBarW)
                hpWidth = 0;
            g.setColor(0x001188);
            g.fillRect(hpBarX + 4, hpBarY - 12, hpWidth, 10);
        }



        public void actionBuy(Item item)
        {
            var ok = new Command(MResources.ACCEPT, 11055, item);
            GameCanvas.inputDlg.show(MResources.INPUT_QUANTITY, ok, TField.INPUT_TYPE_NUMERIC);
        }

        public void actionSale(Item item)
        {
            if (item.upgrade > 0 && item.isTypeBody())
            {
                GameCanvas.msgdlg.setInfo(MResources.NOT_SALE_UPGRADE, null, new Command(MResources.CLOSE, 110561), null);
                GameCanvas.msgdlg.show();
                return;
            }
            GameCanvas.inputDlg.tfInput.getText();
            if (item.quantity > 1)
            {
                var ok = new Command(MResources.ACCEPT, 110562, item);
                GameCanvas.inputDlg.show(MResources.INPUT_QUANTITY, ok, TField.INPUT_TYPE_NUMERIC);
            }
            else
            {
                GameCanvas.startYesNoDlg(MResources.CONFIRMSALEITEM, new Command(MResources.YES, 11061, item),
                    new Command(MResources.NO, 0001));
            }
        }

        private void ActionCoinOut()
        {
            var ok = new Command(MResources.ACCEPT, 11042);
            GameCanvas.inputDlg.show(MResources.INPUT_COIN, ok, TField.INPUT_TYPE_NUMERIC);
        }

        private void ActionCoinTrade()
        {
            var ok = new Command(MResources.ACCEPT, 110361);
            GameCanvas.inputDlg.show(MResources.INPUT_COIN, ok, TField.INPUT_TYPE_NUMERIC);
        }

        private void ActionCoinIn()
        {
            var ok = new Command(MResources.ACCEPT, 11043);
            GameCanvas.inputDlg.show(MResources.INPUT_COIN, ok, TField.INPUT_TYPE_NUMERIC);
        }

        public static void setPopupSize(int w, int h)
        {
            popupW = w;
            popupH = h;
            popupX = gW2 - w / 2;
            popupY = gH2 - h / 2;

            if (GameCanvas.h <= 250)
                popupY -= 10;
            if (GameCanvas.isTouchControlLargeScreen && GameCanvas.currentScreen is GameScr)
            {
                popupW = 310;
                popupX = gW / 2 - popupW / 2;
            }
            if (popupY < -10)
                popupY = -10;
            if (GameCanvas.h > 208)
                if (popupY < 0)
                    popupY = 0;
            if (GameCanvas.h == 208)
                if (popupY < 10)
                    popupY = 10;
        }


        private void DoCloseAlert()
        {
            textsTitle = null;
            texts = null;
            center = null;
            resetButton();
        }

        public static Char findCharInMap(long charId)
        {
            for (var i = 0; i < vCharInMap.size(); i++)
            {
                var c = (Char) vCharInMap.elementAt(i);
                if (c.charID == charId)
                    return c;
            }
            return null;
        }

        public static Mob findMobInMap(short charId)
        {
            for (var i = 0; i < vMob.size(); i++)
            {
                var c = (Mob) vMob.elementAt(i);
                if (c.mobId == charId)
                    return c;
            }
            return null;
        }

        public void actionPerform(int idAction, object p)
        {
            Item item;
            string strTemp;
            Vector menu;
            switch (idAction)
            {
                case 111039: // dong y trao doi 
                    Service.GetInstance().AcceptTrade((short) Char.myChar().partnerTrade.charID);
                    tradeGui = new TradeGui(45, 0);
                    Service.GetInstance().RequestInventory();
                    GameCanvas.EndDlg();
                    break;
                case Constants.BUTTON_SEND:
                    if (TabChat.tfCharFriend.getText().ToLower().Trim().Length > 0 && Char.toCharChat != null)
                        Service.GetInstance().ChatPrivate(Char.toCharChat.CharidDB, TabChat.tfCharFriend.getText());
                    TabChat.tfCharFriend.setText("");
                    TabChat.tfCharFriend.isFocus = false;
                    break;

                case Constants.BUTTON_ICON_CHAT:
                    // xy ly button icon o cho nay
                    TabChat.iconChat = new IconChat(popupX, popupY);
                    TabChat.GetInstance().isPaintListIcon = true;
                    break;
                case Constants.CHAT_PRIVATE: // mo giao dien chat rieng
                    if (indexRow >= 0)
                    {
                        var charF = (Char) Char.myChar().vFriend.elementAt(indexRow);
                        ChatPrivate.AddNewChater((short) Char.myChar().charID, charF.cName);
                    }
                    indexRow = -1;
                    left = null;
                    center = null;
                    OpenUiChatTab();
                    break;
                case Constants.UN_FRIEND:
                    var player = (Char) Char.myChar().vFriend.elementAt(indexRow);
                    Service.GetInstance().DeleteFriend(player.CharidDB);
                    break;
                case Constants.MENU_BAG: //info invetory
                    isBag = true;
                    break;
                case Constants.MENU_CHAT_PRIVATE: //info party
                    OpenUiChatTab();
                    break;
                case Constants.MENU_CHAT_WORLD: //chat world
                    break;
                case Constants.MENU_LIST_FRIEND: //chat world
                    //show list friend //tam thoi commment lai
                    InitTField();
                    Service.GetInstance().RequestFriendList(Char.myChar().charID);
                    break;
                case Constants.MENU_QUEST: //chat world
                    //show list friend //tam thoi commment lai
                    guiQuest = new ShopMain(GameCanvas.wd6 - 20, 20);
                    isPaintQuest = true;
                    break;
                case 155555555: // xin vao nhom
                    var ch = (Char) charnearByme.elementAt(indexRow);
                    Service.GetInstance().requestJoinParty(PartyType.REQUEST_JOIN_PARTY, (short) ch.charID);
                    break;
                case 166666666:
                    ch = (Char) charnearByme.elementAt(indexRow);
                    Service.GetInstance().inviteParty((short) ch.charID, PartyType.INVITE_PARTY);
                    break;
                case 191210: // giai tan
                    Service.GetInstance().removeParty(PartyType.DISBAND_PARTY);
                    break;
                case 270192: // kich ra khoi nhom
                    if (((Char) Party.vCharinParty.elementAt(0)).charID == Char.myChar().charID)
                    {
                        var c = (Char) Party.vCharinParty.elementAt(indexRow);
                        if (c.charID != Char.myChar().charID)
                            Service.GetInstance().kickPlayeLeaveParty(PartyType.KICK_OUT_PARTY, (short) c.charID);
                    }
                    break;
                case 231291: // tu roi nhom
                    Service.GetInstance().leaveParty(PartyType.OUT_PARTY, (short) Char.myChar().charID);
                    break;
                case 111037:
                    Service.GetInstance().AcceptParty(PartyType.ACCEPT_INVITE_PARTY, Party.gI().charId);
                    GameCanvas.EndDlg();
                    break;
                case 111038:
                    Service.GetInstance().AcceptFriend(Char.myChar().idFriend);
                    Char.myChar().idFriend = -1;
                    GameCanvas.EndDlg();
                    break;
                case 0001:
                case 0002: 
                case 110771:
                case 1107921:
                case 150421:
                    GameCanvas.EndDlg();
                    break;
                case 0003: // CLOSE ARLET
                    DoCloseAlert();
                    break;
                case 1000:
                case 2000:
                case 11021:
                case 1108041:
                    resetButton();
                    break;
                case 110001:
                    doMenuInforMe();
                    break;
                case 110003:
                    doMenusynthesis();
                    break;
                case Constants.MENU_LIST_PARTY: //info party
                case 1100062:
                    DoShowTeamUi();
                    break;
                case 1100063:
                case 12003:
                    DoShowFriendUI();
                    break;
                case 11000671:
                    GameCanvas.startYesNoDlg(MResources.ACTIVE_PROTECT_ACC, 88836, null, 8882, null);
                    break;
                case 11000665:
                    isViewClanInvite = !isViewClanInvite;
                    Rms.saveRMSInt(Char.myChar().cName + "vci", isViewClanInvite ? 1 : 0);
                    break;
                case 110018:
                    doShowListChatTab();
                    break;
                case 110019:
                    ChangeTaskInfo();
                    break;
                case 11036:
                    ActionCoinTrade();
                    break;
                case 110441:
                    doAddFriend();
                    break;
                case 11049:
                case 11050:
                    ActionCoinIn();
                    break;
                case 110562:
                    var itemSaleOk = (Item) p;
                    doActionSaleOk(itemSaleOk);
                    break;
                case 11060:
                    ActdoMiniInfo();
                    break;
                case 11065:
                    doUpPotential();
                    break;
                case 11066:
                    DoCloseAlert();
                    isMessageMenu = false;
                    ChatTextField.GetInstance().center = null;
                    break;
                case 11067:
                    if (TileMap.zoneID != indexSelect)
                        InfoDlg.showWait();
                    break;
                case 11068:
                case 11069:
                    ActOpenWebCancel();
                    break;
                case 110722:
                case 11073:
                    actionSale(Char.myChar().arrItemBag[indexSelect]);
                    break;
                case 110792:
                    strTemp = (string) p;
                    ActSetDeleteFriend(strTemp);
                    break;
                
                case 11080: // set command giao diện party
                    strTemp = (string) p;
                    actSetPartyCommand(strTemp);
                    break;
                case 11081:
                    actdoGan();
                    break;
                // TRANG SỨC
                case 11092:
                    var itemTrangSuc = (Item) p;
                    actBuyLeft(itemTrangSuc);
                    break;
                case 110922:
                    var itemTsBuys = (Item) p;
                    actionBuy(itemTsBuys);
                    break;
                case 11120:
                    var objOnScreen = (object[]) p;
                    var skOnScreen = (Skill) objOnScreen[0];
                    var indexOnScreen = int.Parse((string) objOnScreen[1]);
                    onScreenSkill[indexOnScreen] = skOnScreen;
                    break;
                case 11121:
                    var objOnKey = (object[]) p;
                    var skOnKey = (Skill) objOnKey[0];
                    var indexOnKey = int.Parse((string) objOnKey[1]);
                    keySkill[indexOnKey] = skOnKey;
                    break;

                case 12001:
                    ChatManager.gI().switchToTab(int.Parse((string) p, NumberStyles.HexNumber));
                    OpenUiChatTab();
                    break;
                case 12004:
                    strTemp = (string) p;
                    var t = ChatManager.gI().findTab(strTemp);
                    if (t == null)
                    {
                        ChatManager.gI().addNewTab(strTemp);
                        ChatManager.gI().switchToLastTab();
                    }
                    else
                    {
                        ChatManager.gI().switchToTab(t);
                    }

                    OpenUiChatTab();
                    ChatTextField.GetInstance().center = null;
                    break;
                case 12005:
                    DoShowChatTextFieldInMessage();
                    break;
                case 120051:
                    var curChattab = (ChatTab) p;
                    ChatManager.gI().chatTabs.removeElement(curChattab);
                    if (ChatManager.gI().currentTabIndex > ChatManager.gI().chatTabs.size() - 1)
                        ChatManager.gI().switchToPreviousTab();
                    curChattab = ChatManager.gI().getCurrentChatTab();
                    if (curChattab != null)
                    {
                        OpenUiChatTab();
                    }
                    else
                    {
                        ChatTextField.GetInstance().isShow = false;
                        resetButton();
                    }
                    break;
                case 12009:
                    var subPermission = new Vector();
                    subPermission.addElement(new Command("Xin vào nhóm", 155555555));
                    subPermission.addElement(new Command("Mời vào nhóm", 166666666));
                    GameCanvas.menu.startAt(subPermission);
                    break;
                case 140012:
                    doUpSkill();
                    break;
                case 14002:
                    DoUnfocusChar();
                    break;
                case 14003:
                    DoCharFocusList();
                    break;
                case 140041:
                    GameCanvas.inputDlg.tfInput.setMaxTextLenght(180);
                    GameCanvas.inputDlg.show(MResources.INPUT_CLAN_TEXT,
                        new Command(MResources.ACCEPT, GameCanvas.instance, 88832, null),
                        TField.INPUT_TYPE_ANY);
                    break;
                case 140043:
                    GameCanvas.inputDlg.show(MResources.INPUT_CLAN_MONEY,
                        new Command(MResources.ACCEPT, GameCanvas.instance, 88834, null),
                        TField.INPUT_TYPE_ANY);
                    break;
                case 14008:
                    GameCanvas.startYesNoDlg(MResources.MOVE_OUT_CLAN1, new Command(MResources.YES, 140081),
                        new Command(MResources.NO, 0001));
                    break;

                case 140093:
                    GameCanvas.startYesNoDlg(MResources.CLEAR_CLAN_TYPE, new Command(MResources.YES, 1400931),
                        new Command(MResources.NO, 0001));
                    break;
                case 140094: // Trục xuất
                    GameCanvas.startYesNoDlg(MResources.MOVE_OUT_CLAN, new Command(MResources.YES, 1400941),
                        new Command(MResources.NO, 0001));
                    break;
                case 140096:
                    menu = new Vector();
                    menu.addElement(new Command(MResources.INVITE_THIS_PERSON, 1400961));
                    menu.addElement(new Command(MResources.INVITE_ALL, 1400962));
                    GameCanvas.menu.startAt(menu);
                    break;
                case 14010:
                    GameCanvas.inputDlg.show(MResources.INPUT_CLAN_CONTRIBUTE,
                        new Command(MResources.ACCEPT, GameCanvas.instance, 88833, null),
                        TField.INPUT_TYPE_NUMERIC);
                    break;
                case 14011:
                    isMessageMenu = false;
                    break;
                case 140131:
                    GameCanvas.startYesNoDlg(MResources.CONFIRMCONVERT, new Command(MResources.YES, 140132),
                        new Command(MResources.NO, 0001));
                    break;
                case 1500:
                    menu = new Vector();
                    menu.addElement(new Command(MResources.MOVEOUT, 15001));
                    if (Char.myChar().xu >= 5000)
                        menu.addElement(new Command(MResources.SALE, 15002));
                    GameCanvas.menu.startAt(menu);
                    break;
                case 15042:
                    GameCanvas.startYesNoDlg(
                        MResources.replace(MResources.BUY_ASK, Utils.NumberToString(arrItemStands[indexSelect].price + "")),
                        new Command(MResources.YES, 150421), new Command(MResources.NO, 0001));
                    break;
                case 1506:
                    if (arrItemSprin != null)
                    {

                        indexCard = -1;
                        arrItemSprin = null;
                        GetInstance().left = new Command(MResources.SELECT, 1506);
                    }
                    else
                    {
                        indexCard = indexSelect;
                        GameCanvas.startWaitDlgWithoutCancel();
                    }
                    break;
                case 1508:
                    menu = new Vector();
                    item = Char.clan.items[indexSelect];
                    if (item != null)
                    {
                        if (item.template.id == 281)
                            menu.addElement(new Command(MResources.USE, 15081));
                        else
                            menu.addElement(new Command(MResources.CLAN_BOX, 15082));
                        GameCanvas.menu.startAt(menu);
                    }
                    break;
                case 15082:
                    GameCanvas.inputDlg.show(MResources.INPUT_MEM_NAME,
                        new Command(MResources.OK, GameCanvas.instance, 88843, "" + indexSelect),
                        TField.INPUT_TYPE_ANY);
                    break;
                case 1511:
                    strTemp = GameCanvas.inputDlg.tfInput.getText();
                    GameCanvas.EndDlg();
                    try
                    {
                        if (strTemp.Equals(""))
                        {
                            GameCanvas.StartOkDlg(MResources.INVALID_NUM);
                        }
                        else
                        {
                            var value = int.Parse(strTemp,
                                NumberStyles.HexNumber); // int.valueOf(strTemp).intValue();

                            if (value < 10 || value > 90)
                                GameCanvas.StartOkDlg(MResources.INVALID_NUM);
                            else
                                Char.aHpValue = value;
                        }
                    }
                    catch (Exception e)
                    {
                        GameCanvas.StartOkDlg(MResources.INVALID_NUM);
                    }
                    break;
                case 1512:
                    strTemp = GameCanvas.inputDlg.tfInput.getText();
                    GameCanvas.EndDlg();
                    try
                    {
                        if (strTemp.Equals(""))
                        {
                            GameCanvas.StartOkDlg(MResources.INVALID_NUM);
                        }
                        else
                        {
                            var value = int.Parse(strTemp,
                                NumberStyles.HexNumber); // Integer.valueOf(strTemp).intValue();

                            if (value < 10 || value > 90)
                                GameCanvas.StartOkDlg(MResources.INVALID_NUM);
                            else
                                Char.aMpValue = value;
                        }
                    }
                    catch (Exception e)
                    {
                        GameCanvas.StartOkDlg(MResources.INVALID_NUM);
                    }
                    break;
            }
        }


        private void DoUnfocusChar()
        {
            Char.myChar().charFocus = null;
            Char.isManualFocus = false;
            cLastFocusID = -1;

            resetButton();
        }

        private void AutoFocus()
        {
            if (cLastFocusID >= 0 && vCharInMap.size() > 0)
            {
                var cIndex = Char.getIndexChar(cLastFocusID);
                if (cIndex >= 0 && cIndex < vCharInMap.size())
                {
                    var cFocus = (Char) vCharInMap.elementAt(cIndex);
                    if (cFocus != null)
                        if (Char.isCharInScreen(cFocus) /*&&!cFocus.isNhanban()*/)
                        {
                            Char.myChar().mobFocus = null;
                            Char.myChar().DoFocusNPC();
                            Char.myChar().itemFocus = null;
                            Char.isManualFocus = true;
                            Char.myChar().charFocus = cFocus;
                        }
                }
                else
                {
                    cLastFocusID = -1;
                    Char.myChar().charFocus = null;
                }
            }
            else
            {
                cLastFocusID = -1;
            }
        }

        private void DoCharFocusList()
        {
            resetButton();
        }

        private void ChangeTaskInfo()
        {
            indexMenu = indexMenu == 0 ? 1 : 0;
            indexRow = 0;
            idTypeTask = indexMenu;
        }


        private void actBuyLeft(Item itemBuy)
        {
            var menu = new Vector();
            menu.addElement(new Command(MResources.BUY, 110921, itemBuy));
            menu.addElement(new Command(MResources.BUYS, 110922, itemBuy));
            GameCanvas.menu.startAt(menu);
        }


        private void actdoGan()
        {
            var menu = new Vector();
            menu.addElement(new Command(MResources.ASSIGN_KEY[0], 110811));
            menu.addElement(new Command(MResources.ASSIGN_KEY[1], 110812));
            GameCanvas.menu.startAt(menu);
        }

        private void actSetPartyCommand(string partyName)
        {
            var subPermission = new Vector();
            subPermission.addElement(new Command("Xin vào nhóm", 155555555));
            subPermission.addElement(new Command("Mời vào nhóm", 166666666));

            GameCanvas.menu.startAt(subPermission);
        }


        private void ActSetDeleteFriend(string friendName)
        {
            GameCanvas
                .startYesNoDlg(MResources.CONFIRM_REMOVE_FRIEND, new Command(MResources.YES, 1107921, friendName),
                    new Command(MResources.NO, 0001));
        }

        private void ActOpenWebCancel()
        {
            textsTitle = null;
            texts = null;
            center = null;
            resetButton();
        }


        private void ActdoMiniInfo()
        {
            indexMenu = 0;
            resetButton();
            if (currentCharViewInfo == Char.myChar())
                doMenuInforMe();
        }


        private void doActionSaleOk(Item item)
        {
            var text = GameCanvas.inputDlg.tfInput.getText();
            if (text.Trim().Equals(""))
                return;
            var quantity = 0;
            try
            {
                quantity = int.Parse(text);
            }
            catch (Exception e)
            {
                GameCanvas.inputDlg.hide();
                return;
            }
            if (quantity <= 0)
            {
                GameCanvas.inputDlg.hide();
                return;
            }
            if (quantity > item.quantity)
            {
                GameCanvas.StartOkDlg(MResources.NOT_ENOUGH_QUANTITY);
                return;
            }
            GameCanvas.inputDlg.hide();
            GameCanvas.startYesNoDlg(MResources.CONFIRMSALEITEM, new Command(MResources.YES, 11058, item),
                new Command(MResources.NO, 0001));
        }


        private void paintWaypointArrow(MGraphics g)
        {
            int x, y, a = 10;
            for (var i = 0; i < TileMap.GetInstance().vGo.size(); i++)
            {
                var way = (Waypoint) TileMap.GetInstance().vGo.elementAt(i);

                if (way.name != null)
                    FontManager.GetInstance().tahoma_7.DrawString(g, way.name, way.minX + (way.maxX - way.minX) / 2,
                        way.minY + 2 + LoadImageInterface.imgXinCho.GetHeight() / 3, 2);
            }
        }


        private void OpenUiChatTab()
        {
            TabChat.GetInstance().switchToMe();
            isLockKey = true;
            setPopupSize(175, 200);
            left = center = null;
        }

        private void DoShowChatTextFieldInMessage()
        {
            var currentTab = ChatManager.gI().getCurrentChatTab();
            if (currentTab.type == 0) // public
                ChatTextField.GetInstance().startChat(this, MResources.PUBLICCHAT[0]);
            if (currentTab.type == 1) // party
                ChatTextField.GetInstance().startChat(this, MResources.PARTYCHAT[0]);
            if (currentTab.type == 2) // private
                ChatTextField.GetInstance().startChat(this, currentTab.ownerName);
            if (currentTab.type == 3) // global
                ChatTextField.GetInstance().startChat(this, MResources.GLOBALCHAT[0]);
            if (currentTab.type == 4) // clan
                ChatTextField.GetInstance().startChat(this, MResources.CLANCHAT[0]);
        }


        private void doShowListChatTab()
        {
            var v = new Vector();
            for (var i = 0; i < ChatManager.gI().chatTabs.size(); i++)
            {
                var tab = (ChatTab) ChatManager.gI().chatTabs.elementAt(i);
                v.addElement(new Command(tab.ownerName, 12001, i + ""));
            }
            v.addElement(new Command(MResources.BLOCK_MESSAGE, 12006));
            v.addElement(new Command(MResources.CHAT_ADMIN, 12008));
            GameCanvas.menu.startAt(v);
            isMessageMenu = true;
        }

        public void paintInfoMod(MGraphics g)
        {
            if (Char.myChar().mobFocus.mobName != null)
                FontManager.GetInstance().tahoma_7_white.DrawStringShadown(g, Char.myChar().mobFocus.mobName, Char.myChar().mobFocus.x,
                    Char.myChar().mobFocus.y
                    - Char.myChar().mobFocus.h - 20, 2);
        }

        private void DoFire()
        {
            if (Char.myChar().statusMe != Char.A_DEAD && Char.myChar().mobFocus != null)
            {
                if (MSystem.currentTimeMillis() - (lastpress + delayPressAtt) > 0)
                {
                    Char.myChar().cdir = Char.myChar().cx - Char.myChar().mobFocus.x > 0 ? -1 : 1;
                    if (GuiMain.isTestSkill)
                        idEff = changeKillID;
                    if (changeKillID == 0)
                        Music.play(Music.ATTACK_0, 0.5f);
                    else Music.play(Music.SKILL2, 0.5f);
                    Char.myChar().setSkillPaint(sks[idEff], Skill.ATT_STAND);
                    var ds = new Vector();
                    ds.add(Char.myChar().mobFocus);
                    Char.myChar().mobFocus.setInjure();
                    Char.myChar().mobFocus.injureBy = Char.myChar();
                    Char.myChar().mobFocus.status = Mob.MA_INJURE;

                    lastpress = MSystem.currentTimeMillis();
                    if (!isSendMove)
                        Service.GetInstance().CharMove();
                    Service.GetInstance().sendPlayerAttack(ds, vCharInMap, 1, changeKillID, true);
                }
            }
            else if (Char.myChar().statusMe != Char.A_DEADFLY && Char.myChar().statusMe != Char.A_DEAD &&
                     Char.myChar().charFocus != null
                     && Char.myChar().charFocus.statusMe != Char.A_DEADFLY &&
                     Char.myChar().charFocus.statusMe != Char.A_DEAD
            )
            {
                var dSs = new Vector();
                dSs.add(Char.myChar().charFocus);
                Char.myChar().cdir = Char.myChar().cx - Char.myChar().charFocus.cx > 0 ? -1 : 1;
                if (Char.myChar().typePk == 0 || Char.myChar().charFocus.typePk == 0
                    || Char.myChar().typePk > 0 && Char.myChar().charFocus.typePk > 0
                    && Char.myChar().typePk != Char.myChar().charFocus.typePk)
                {
                    if (!isSendMove)
                        Service.GetInstance().CharMove();
                    Char.myChar().charFocus.DoInjure(1, 0, false, 1);
                    Service.GetInstance().sendPlayerAttack(dSs, dSs, 0, changeKillID, false);
                    ServerEffect.addServerEffect(25, Char.myChar().charFocus.cx, Char.myChar().charFocus.cy - 20, 1);
                    Char.myChar().setSkillPaint(sks[idEff], Skill.ATT_STAND);
                }
                else
                {
                    Char.myChar().cdir = Char.myChar().cx - Char.myChar().charFocus.cx > 0 ? -1 : 1;
                    guiMain.menuIcon.indexpICon = Constants.ICON_GIAOTIEP;
                    guiMain.menuIcon.paintButtonClose = true;
                    guiMain.menuIcon.contact = new GuiContact(GameCanvas.hw, 20);
                    guiMain.menuIcon.contact.SetPosClose(guiMain.menuIcon.cmdClose);
                    MenuIcon.lastTab.add("" + Constants.ICON_GIAOTIEP);
                    MenuIcon.isShowTab = true;
                }
            }
            else if (Char.myChar().statusMe != Char.A_DEAD && Char.myChar().npcFocus != null)
            {
                Char.myChar().cdir = Char.myChar().cx - Char.myChar().npcFocus.cx > 0 ? -1 : 1;
                if (Char.myChar().npcFocus.template.typeKhu == -1)
                {
                    //focus khu
                }
                else if (Char.myChar().npcFocus.typeNV > -1)
                {
                    var menuList = new Vector();
                    menuList.addElement(new Command("Nhiệm vụ", this, 10, new MenuObject(Char.myChar().npcFocus.npcId)));
                    menuList.addElement(new Command("Menu", GameCanvas.instance, GameCanvas.cMenuNpc, -1 + ""));

                    GameCanvas.menu.startAtNPC(menuList, 0, Char.myChar().npcFocus.npcId, Char.myChar().npcFocus, "");
                }
                else
                {
//                for (var j = 0; j < MainQuestManager.listUnReceiveQuest.size(); j++)
//                {
//                    var npc = (MainQuestManager) MainQuestManager.listUnReceiveQuest.elementAt(j);
//                    if (npc != null && npc.idNPC_From == Char.myChar().npcFocus.npcId)
//                    {
//                        Char.myChar().npcFocus.typeNV = 0;
//                        var menu = new Vector();
//                        menu.addElement(new Command("Nhiệm vụ", this, 10,
//                            new MenuObject(Char.myChar().npcFocus.npcId)));
//
//                        GameCanvas.menu.startAtNPC(menu, 0, Char.myChar().npcFocus.npcId, Char.myChar().npcFocus, "");
//                        break;
//                    }
//                }

                    if (MainQuestManager.getInstance().NewQuest != null)
                    {
                        var quest = MainQuestManager.getInstance().NewQuest;
                        if (quest.IdNpcReceive == Char.myChar().npcFocus.npcId)
                        {
                            Char.myChar().npcFocus.typeNV = 0;
                            var menuList = new Vector();
                            menuList.addElement(new Command("Nhiệm vụ", this, 10, new MenuObject(Char.myChar().npcFocus.npcId)));

                            GameCanvas.menu.startAtNPC(menuList, 0, Char.myChar().npcFocus.npcId, Char.myChar().npcFocus, "");

                        }
                    }
                
                    if (Char.myChar().npcFocus.typeNV == -1 && MainQuestManager.getInstance().WorkingQuest != null)
                    {
//                    for (var j = 0; j < MainQuestManager.vecQuestDoing_Main.size(); j++)
//                    {
//                        var npc = (MainQuestManager) MainQuestManager.vecQuestDoing_Main.elementAt(j);
//                        if (npc != null && npc.idNPC_From == Char.myChar().npcFocus.npcId)
//                        {
//                            Char.myChar().npcFocus.typeNV = 1;
//                            var menu = new Vector();
//                            menu.addElement(new Command("Nhiệm vụ", this, 10,
//                                new MenuObject(Char.myChar().npcFocus.npcId)));
//                            GameCanvas.menu.startAtNPC(menu, 0, Char.myChar().npcFocus.npcId, Char.myChar().npcFocus,
//                                "");
//                            break;
//                        }
//                    }
                        var quest = MainQuestManager.getInstance().WorkingQuest;
                        if (quest.IdNpcReceive == Char.myChar().npcFocus.npcId)
                        {
                            Char.myChar().npcFocus.typeNV = 1;
                            var menuList = new Vector();
                            menuList.addElement(new Command("Nhiệm vụ", this, 10, new MenuObject(Char.myChar().npcFocus.npcId)));
                        
                            GameCanvas.menu.startAtNPC(menuList, 0, Char.myChar().npcFocus.npcId, Char.myChar().npcFocus, "");
                        }
                    }
                    if (Char.myChar().npcFocus.typeNV == -1 && MainQuestManager.getInstance().FinishQuest != null)
                    {
//                    for (var j = 0; j < MainQuestManager.vecQuestFinish.size(); j++)
//                    {
//                        var npc = (MainQuestManager) MainQuestManager.vecQuestFinish.elementAt(j);
//                        if (npc != null && npc.idNPC_From == Char.myChar().npcFocus.npcId)
//                        {
//                            Char.myChar().npcFocus.typeNV = 1;
//                            var menu = new Vector();
//                            menu.addElement(new Command("Nhiệm vụ", this, 10,
//                                new MenuObject(Char.myChar().npcFocus.npcId)));
//                            GameCanvas.menu.startAtNPC(menu, 0, Char.myChar().npcFocus.npcId, Char.myChar().npcFocus,
//                                "");
//                            break;
//                        }
//                    }
                    
                        var quest = MainQuestManager.getInstance().FinishQuest;
                        if (quest != null && quest.IdNpcReceive == Char.myChar().npcFocus.npcId)
                        {
                            Char.myChar().npcFocus.typeNV = 1;
                            var menuList = new Vector();
                            menuList.addElement(new Command("Nhiệm vụ", this, 10, new MenuObject(Char.myChar().npcFocus.npcId)));
                            GameCanvas.menu.startAtNPC(menuList, 0, Char.myChar().npcFocus.npcId, Char.myChar().npcFocus, "");
                        }
                    }

                    Service.GetInstance().MenuNpc(Char.myChar().npcFocus.template.npcTemplateId, -1);
                }
            }
            else if (Char.myChar().statusMe != Char.A_DEAD && Char.myChar().itemFocus != null)
            {
                Service.GetInstance().itemPick(Char.myChar().itemFocus.type, (short) Char.myChar().itemFocus.itemMapID);
            }
        }

        private static void InitTField()
        {
            tfCharFriend = new TField
            {
                width = popupW - 30,
                height = ITEM_HEIGHT + 2,
                x = xGui + 5,
                y = yGui + heightGui - 25,
                isFocus = false
            };
            tfCharFriend.setIputType(TField.INPUT_TYPE_ANY);
        }

        public static void loadMapItem()
        {
            // đọc dữ liệu data Map item
            DataInputStream dis;
            try
            {
                dis = new DataInputStream("/mapitem/mapItem");
                var nMapItem = dis.ReadShort();
                for (var i = 0; i < nMapItem; i++)
                {
                    var biSe = new BgItem();
                    biSe.id = i;
                    biSe.idImage = dis.ReadShort(); // id hinh 
                    biSe.layer = dis.ReadByte(); // layer truoc sau so voi tile
                    biSe.dx = dis.ReadShort(); // tam item trong map
                    biSe.dy = dis.ReadShort();
                    var nTileNotMove = dis.ReadByte();
                    if (nTileNotMove < 0) return;
                    biSe.tileX = new int[nTileNotMove];
                    biSe.tileY = new int[nTileNotMove];
                    for (var j = 0; j < nTileNotMove; j++)
                    {
                        biSe.tileX[j] = dis.ReadByte();
                        biSe.tileY[j] = dis.ReadByte();
                    }
                    TileMap.vItemBg.addElement(biSe);
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        public static void loadMapTable(int mapId)
        {
            try
            {
                var dis = new DataInputStream("/mapitem/mapTable" + mapId);
                var count = dis.ReadShort();
                for (var i = 0; i < count; i++)
                {
                    var biMap = new BgItem();
                    var id = dis.ReadShort();
                    var currBi = TileMap.GetBiById(id);
                    biMap.id = currBi.id;
                    biMap.x = dis.ReadShort() * TileMap.GetInstance().size;
                    biMap.y = dis.ReadShort() * TileMap.GetInstance().size;
                    biMap.idImage = currBi.idImage;
                    biMap.dx = currBi.dx;
                    biMap.dy = currBi.dy;
                    biMap.layer = currBi.layer;
                    TileMap.vCurrItem.addElement(biMap);
                }
                Service.GetInstance().RequestInventory();
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }


        public static void paintBgItem(MGraphics g, int layer)
        {
            for (var a = 0; a < TileMap.vCurrItem.size(); a++)
            {
                var bi = (BgItem) TileMap.vCurrItem.elementAt(a);
                if (bi.idImage == -1) continue;
                if (bi.layer == layer && GuiMain.isPaintOjectMap)
                    bi.paint(g);
            }
        }


        public static void AddEffectKillMobAttack(Mob m, Char c, int idEffect, sbyte ideffsub)
        {
            var eff = new EffectKill(idEffect, m, c, ideffsub);
            veffClient.add(eff);
        }

        public static void AddEffectEnd(sbyte type, int x, int y, int xTo, int toY)
        {
            var end = new EffectKill(type, x, y, xTo, toY);
            veffClient.add(end);
        }

        private void DoTouchQuickSlot(int id)
        {
            if (Char.myChar().statusMe == Char.A_DEAD || Char.myChar().statusMe == Char.A_DEADFLY)
            {
                GameCanvas.startCommandDlg("Bạn có muốn hồi sinh tại chỗ (1 gold)?",
                    new Command("Hồi sinh", GameCanvas.instance, GameCanvas.cHoiSinh, null),
                    new Command("Về làng", GameCanvas.instance, GameCanvas.cVeLang, null));
                return;
            }
            if (Char.myChar().npcFocus != null && Char.myChar().npcFocus.template.typeKhu == -1)
            {
                Service.GetInstance().RequestRegionInfo();
                return;
            }
            if (Char.myChar().npcFocus != null) DoFire();
            if (GuiMain.isTestSkill)
            {
                changeKillID = GuiMain.indexSkillTest;
                DoFire();
                return;
            }

            if (GameCanvas.isTouch)
            {
                var ql = Char.myChar().mQuickslot[id];
                if (ql.canfight())
                {
                    //timeDow = 40;
                    var isNgoaiRange = false;
                    if (Char.myChar().mobFocus != null)
                    {
                        var skill = (SkillTemplate) SkillTemplates.hSkilltemplate.get("" + ql.idSkill);
                        var range = CRes.getDistance(Char.myChar().cx, Char.myChar().cy, Char.myChar().mobFocus.x,
                            Char.myChar().mobFocus.y);
                        isNgoaiRange = range > skill.rangelv[skill.level >= skill.rangelv.Length ? 0 : skill.level];
                    }
                    changeKillID = ql.idSkill;
                    idEff = ql.ideff;
                    if (Char.myChar().mobFocus != null && isNgoaiRange)
                    {
                        GameCanvas.StartDglThongBao("Mục tiêu ở quá xa");
                        var maxkcX = CRes.abs(Char.myChar().cx - Char.myChar().mobFocus.x);
                        int kc = 1, leng = 0;
                        if (maxkcX > 20 || CRes.abs(Char.myChar().cy - Char.myChar().mobFocus.y) > 10)
                        {
                            for (var j = 0; j < maxkcX; j++)
                            {
                                leng += j;
                                if (leng >= maxkcX)
                                {
                                    kc = j - 1;
                                    break;
                                }
                            }
                            if (Char.myChar().cx > Char.myChar().mobFocus.x)
                            {
                                Char.myChar().UpdateCharRun();
                                if (Char.myChar().cdir == 1)
                                    Char.myChar().cdir = -1;
                                if (!Char.myChar().isLockMove && !Char.myChar().isBlinking)
                                {
//                                    if (Char.myChar().cx - Char.myChar().cxSend != 0)
//                                        Service.GetInstance().CharMove();
                                    Char.myChar().UpdateCharRun();
                                    Char.myChar().statusMe = Char.A_RUN;
                                    Char.myChar().cvx = -kc;
                                }
                            }
                            else
                            {
                                Char.myChar().UpdateCharRun();
                                if (Char.myChar().cdir == -1)
                                    Char.myChar().cdir = 1;
                                if (!Char.myChar().isLockMove && !Char.myChar().isBlinking)
                                {
//                                    if (Char.myChar().cx - Char.myChar().cxSend != 0)
//                                        Service.GetInstance().CharMove();
                                    Char.myChar().UpdateCharRun();
                                    Char.myChar().statusMe = Char.A_RUN;
                                    Char.myChar().cvx = kc;
                                }
                            }
                        }
                        return;
                    }
                    if (Char.myChar().mobFocus != null)
                        if (ql.isEnoughtMp())
                        {
                            for (var i = 0; i < Char.myChar().mQuickslot.Length; i++)
                            {
                                var qll = Char.myChar().mQuickslot[i];
                                if (qll.idSkill == ql.idSkill && !isNgoaiRange)
                                    Char.myChar().mQuickslot[i].startCoolDown();
                            }
                            if (!isAutoDanh)
                            {
                                xStartAuto = Char.myChar().cx;
                                yStartAuto = Char.myChar().cy;
                            }
                            var listauto = Rms.loadRMS(Rms.autoSetting);
                            if (listauto?[SettingScreen.AUTO_DANH] == 1)
                                isAutoDanh = true;
                            DoFire();
                        }
                        else
                        {
                            GameCanvas.StartDglThongBao("Không đủ charka");
                        }
                    else if (Char.myChar().npcFocus != null || Char.myChar().itemFocus != null ||
                             Char.myChar().charFocus != null)
                        if (Char.myChar().charFocus != null)
                        {
                            if (Char.myChar().typePk == Char.myChar().charFocus.typePk)
                                DoFire();
                        }
                        else
                        {
                            DoFire();
                        }
                    if (Char.myChar().npcFocus == null && Char.myChar().charFocus != null &&
                        Char.myChar().charFocus.statusMe != Char.A_DEAD &&
                        Char.myChar().typePk != Char.myChar().charFocus.typePk) // ko start cooldow
                        if (ql.isEnoughtMp())
                        {
                            for (var i = 0; i < Char.myChar().mQuickslot.Length; i++)
                            {
                                var qll = Char.myChar().mQuickslot[i];
                                if (qll.idSkill == ql.idSkill && !isNgoaiRange)
                                    Char.myChar().mQuickslot[i].startCoolDown();
                            }
                            DoFire();
                        }
                        else
                        {
                            GameCanvas.StartDglThongBao("Không đủ charka");
                        }
                }
            }
        }

        private static void UpdateCloudy()
        {
            for (var i = 0; i < ncloudy1; i++)
                if (xpaintcloudy1[i] > TileMap.pxw)
                    ypaintcloudy1[i] = CRes.random(80, TileMap.pxh + GameCanvas.h);
                else xpaintcloudy1[i] += 1;
            for (var i = 0; i < ncloudy1; i++)
                if (xpaintcloudy1[i] > TileMap.pxw)
                    if (i == ncloudy1 - 1)
                    {
                        xpaintcloudy1[i] = 0 - wcloudy1;
                        ypaintcloudy1[i] = CRes.random(80, TileMap.pxh + GameCanvas.h);
                    }
                    else
                    {
                        xpaintcloudy1[i] = 0 - wcloudy1;
                        ypaintcloudy1[i] = CRes.random(80, TileMap.pxh + GameCanvas.h);
                    }
            for (var i = 0; i < ncloudy2; i++)
                if (xpaintcloudy2[i] > TileMap.pxw)
                    ypaintcloudy2[i] = CRes.random(80, TileMap.pxh + GameCanvas.h);
                else xpaintcloudy2[i] += 2;
            for (var i = 0; i < ncloudy2; i++)
                if (xpaintcloudy2[i] > TileMap.pxw)
                    if (i == ncloudy2 - 1)
                    {
                        xpaintcloudy2[i] = 0 - wcloudy2;
                        ypaintcloudy2[i] = CRes.random(80, TileMap.pxh + GameCanvas.h);
                    }
                    else
                    {
                        xpaintcloudy2[i] = 0 - wcloudy2;
                        ypaintcloudy2[i] = CRes.random(80, TileMap.pxh + GameCanvas.h);
                    }
            for (var i = 0; i < ncloudy0; i++)
                if (xpaintcloudy0[i] > TileMap.pxw)
                    ypaintcloudy0[i] = CRes.random(80, TileMap.pxh);
                else xpaintcloudy0[i] += 3;
            for (var i = 0; i < ncloudy0; i++)
                if (xpaintcloudy0[i] > TileMap.pxw)
                    if (i == ncloudy0 - 1)
                    {
                        xpaintcloudy0[i] = 0 - wcloudy0 - kcwcloudy0;
                        ypaintcloudy0[i] = CRes.random(80, TileMap.pxh + GameCanvas.h);
                    }
                    else
                    {
                        xpaintcloudy0[i] = 0 - wcloudy0 - kcwcloudy0;
                        ypaintcloudy0[i] = CRes.random(80, TileMap.pxh + GameCanvas.h);
                    }
        }

        private static void PaintCloudy1(MGraphics g)
        {
            if (imgCloudy[1] == null) return;
            for (var i = 0; i < ncloudy1; i++)
            {
                g.drawImage(imgCloudy[1], xpaintcloudy1[i], ypaintcloudy1[i], MGraphics.BOTTOM | MGraphics.LEFT);
            }
        }

        private static void PaintCloudy0(MGraphics g)
        {
            if (imgCloudy[0] == null) return;
            for (var i = 0; i < ncloudy0; i++)
            {
                g.drawImage(imgCloudy[0], xpaintcloudy0[i], ypaintcloudy0[i], MGraphics.BOTTOM | MGraphics.LEFT);
            }
        }

        private static void PaintCloudy2(MGraphics g)
        {
            if (imgCloudy[2] == null) return;
            for (var i = 0; i < ncloudy2; i++)
            {
                g.drawImage(imgCloudy[2], xpaintcloudy2[i], ypaintcloudy2[i], MGraphics.BOTTOM | MGraphics.LEFT);
            }
            
        }

        private void AutoDanh()
        {
            if (Char.myChar().statusMe == Char.A_DEAD || Char.myChar().statusMe == Char.A_ATTK
                || Char.myChar().statusMe == Char.A_RUN
                || Char.myChar().skillPaint != null)
                return;

            if (Char.myChar().statusMe == Char.A_ATTK || Char.myChar().statusMe == Char.A_RUN) return;
        
            timeDow--;
            if (timeDow <= 0 && Char.myChar().mobFocus != null && !Char.myChar().mobFocus.injureThenDie)
            {
                int[] key = {0, 1, 2, 3, 4, 5};
                indexKeyTouchAuto = (indexKeyTouchAuto + 1) % 5;
                var ql = Char.myChar().mQuickslot[indexKeyTouchAuto];
                if (ql.idSkill == -1) return;
                var skill = (SkillTemplate) SkillTemplates.hSkilltemplate.get("" + ql.idSkill);
                var range = skill.rangelv[skill.level];
                if (Utils.Distance(Char.myChar().cx, Char.myChar().cy, Char.myChar().mobFocus.x,
                        Char.myChar().mobFocus.y) <= range)
                {
                    if (ql.quickslotType == QuickSlot.TYPE_SKILL)
                        DoTouchQuickSlot(GameCanvas.isTouch ? key[indexKeyTouchAuto] : indexKeyTouchAuto);
                }
                else
                {
                    timeDow = 100;
                }
            }
            else if (timeDow <= 0 && (Char.myChar().mobFocus == null || Char.myChar().mobFocus.injureThenDie))
            {
                // tim con quai trong range auTo
                Char.myChar().clearAllFocus();
                if (Char.myChar().statusMe == Char.A_STAND)
                    for (var i = 0; i < vMob.size(); i++)
                    {
                        var ac = (Mob) vMob.elementAt(i);
                        if (ac != null && !ac.injureThenDie && CRes.abs(ac.y - Char.myChar().cy) < 30 &&
                            Utils.Distance(xStartAuto, yStartAuto, ac.x, ac.y) <= rangeAuto)
                        {
                            var dis = Utils.Distance(Char.myChar().cx, Char.myChar().cy, ac.x, ac.y);
                            if (dis < 160)
                            {
                                var maxkcX = CRes.abs(Char.myChar().cx - ac.x);
                                int kc = 1, leng = 0;
                                if (maxkcX > 20 || CRes.abs(Char.myChar().cy - ac.y) > 10)
                                {
                                    for (var j = 0; j < maxkcX; j++)
                                    {
                                        leng += j;
                                        if (leng >= maxkcX)
                                        {
                                            kc = j - 1;
                                            break;
                                        }
                                    }
                                    if (Char.myChar().cx > ac.x)
                                    {
                                        Char.myChar().UpdateCharRun();
                                        if (Char.myChar().cdir == 1)
                                            Char.myChar().cdir = -1;
                                        if (!Char.myChar().isLockMove && !Char.myChar().isBlinking)
                                        {
                                            if (ac.isBoss && !Mob.isBossAppear) continue;
//                                            if (Char.myChar().cx - Char.myChar().cxSend != 0)
//                                                Service.GetInstance().CharMove();
                                            Char.myChar().UpdateCharRun();
                                            Char.myChar().statusMe = Char.A_RUN;
                                            Char.myChar().mobFocus = ac;
                                            Char.myChar().cvx = -kc;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        Char.myChar().UpdateCharRun();
                                        if (Char.myChar().cdir == -1)
                                            Char.myChar().cdir = 1;
                                        if (!Char.myChar().isLockMove && !Char.myChar().isBlinking)
                                        {
                                            if (ac.isBoss && !Mob.isBossAppear) continue;
//                                            if (Char.myChar().cx - Char.myChar().cxSend != 0)
//                                                Service.GetInstance().CharMove();
                                            Char.myChar().UpdateCharRun();
                                            Char.myChar().statusMe = Char.A_RUN;
                                            Char.myChar().mobFocus = ac;
                                            Char.myChar().cvx = kc;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        if (i == vMob.size() - 1)
                            timeDow = 120; // sleep thoi gian tim quai
                    }
            }
        }

        private static void AutoNhat()
        {
            for (var i = 0; i < vNhatItemMap.size(); i++)
            {
                var item = (ItemMap) vNhatItemMap.elementAt(i);
                if (item.timeTonTai < 3 || item.isSendNhat) continue;
                item.isSendNhat = true;
                Service.GetInstance().itemPick(item.type, (short) item.itemMapID); //.itemMapID

                vNhatItemMap.removeElementAt(i);
                i--;
            }
        }
    }
}