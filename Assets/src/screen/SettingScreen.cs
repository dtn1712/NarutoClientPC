using src.lib;
using src.main;
using src.model;
using src.screen;

public class SettingScreen : TScreen, IActionListener
{
    public const int MUSIC = 0;
    public const int SOUND = 1;
    public const int TYPE_MOVE = 2;
    public const int AUTO_DANH = 3;
    public const int AUTO_NHAT = 4;
    public static SettingScreen instance;

    public static sbyte[] defautAuto = {1, 1, 0, 1, 0};
    public Command cmdClose;
    public bool[] isListAuto = new bool[5];

    public string[] listText =
        {"Âm nhạc nền", "Âm thanh game", "Analog di chuyển", "Tự động đánh" /*"Tự động nhặt vật phẩm"*/};

    public int wkhung, hKhung, xpaint, ypaint;

    public SettingScreen()
    {
        wkhung = 200;
        hKhung = 150;

        xpaint = GameCanvas.w / 2 - wkhung / 2;
        ypaint = GameCanvas.h / 2 - hKhung / 2 - 10;
        cmdClose = new Command("", this, 2, null);
        cmdClose.setPos(xpaint + wkhung - LoadImageInterface.closeTab.width / 2,
            ypaint, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
    }

    public void perform(int idAction, object p)
    {
        switch (idAction)
        {
            case 2:
                GameScr.GetInstance().switchToMe();
                break;
        }
    }

    public static SettingScreen gI()
    {
        if (instance == null) instance = new SettingScreen();
        return instance;
    }

    public override void switchToMe()
    {
        base.switchToMe();
        var listauto = Rms.loadRMS(Rms.autoSetting);
        if (listauto == null || listauto.Length < defautAuto.Length)
            Rms.saveRMS(Rms.autoSetting, defautAuto);
        else
            for (var i = 0; i < listauto.Length; i++)
                if (listauto[i] == 1)
                    isListAuto[i] = true;
                else isListAuto[i] = false;
    }

    public override void updateKey()
    {
        base.updateKey();
        if (GameCanvas.keyPressed[13] || getCmdPointerLast(cmdClose))
            if (cmdClose != null)
            {
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                keyTouch = -1;
                if (cmdClose != null)
                    cmdClose.performAction();
            }
        for (var i = 0; i < listText.Length; i++)
            if (GameCanvas.isPointerJustRelease &&
                GameCanvas.isPoint(xpaint + 20, ypaint + 31 + 20 * i, wkhung - 40, 18))
            {
                GameCanvas.isPointerJustRelease = false;
                if (i == MUSIC)
                {
                    isListAuto[MUSIC] = !isListAuto[MUSIC];
                    Music.isplayMusic = isListAuto[MUSIC];
                    if (isListAuto[MUSIC])
                        Music.resumeMusic();
                    else Music.pauseMusic();
                }
                else if (i == SOUND)
                {
                    isListAuto[SOUND] = !isListAuto[SOUND];
                    Music.isplaySound = isListAuto[SOUND];
                    if (isListAuto[SOUND])
                    {
                    }
                    else
                    {
                        Music.stopSound(Music.currentSound);
                    }
                    ;
                }
                else if (i == AUTO_DANH)
                {
                    isListAuto[AUTO_DANH] = !isListAuto[AUTO_DANH];
                    GameScr.isAutoDanh = isListAuto[AUTO_DANH];
                }
                else if (i == AUTO_NHAT)
                {
                    isListAuto[AUTO_NHAT] = !isListAuto[AUTO_NHAT];
                    GameScr.isAutoNhatItem = isListAuto[AUTO_NHAT];
                }
                else if (i == TYPE_MOVE)
                {
                    isListAuto[TYPE_MOVE] = !isListAuto[TYPE_MOVE];
                    GuiMain.isAnalog = isListAuto[TYPE_MOVE];
                }
                sbyte[] listauto = {0, 0, 0, 0, 0};
                for (var j = 0; j < listauto.Length; j++)
                    if (isListAuto[j])
                        listauto[j] = 1;
                Rms.saveRMS(Rms.autoSetting, listauto);
                break;
            }
    }

    public override void update()
    {
        base.update();
        GameScr.GetInstance().update();
    }

    public override void paint(MGraphics g)
    {
        base.paint(g);
        GameScr.GetInstance().paint(g);
        g.setColor(0x000000, GameCanvas.opacityTab);
        g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
        g.disableBlending();
        Paint.paintFrameNaruto(xpaint, ypaint, wkhung, hKhung + 2, g);
        Paint.PaintBoxName("Cài Đặt", xpaint + wkhung / 2 - 40, ypaint, 80, g);
        for (var i = 0; i < listText.Length; i++)
        {
            FontManager.GetInstance().tahoma_7.DrawString(g, listText[i], xpaint + 20, ypaint + 40 + 20 * i, 0);

            g.drawRegion(LoadImageInterface.imgCheckSetting, 0,
                (isListAuto[i] ? 1 : 0) * LoadImageInterface.imgCheckSetting.GetHeight() / 2,
                LoadImageInterface.imgCheckSetting.GetWidth(), LoadImageInterface.imgCheckSetting.GetHeight() / 2,
                0, xpaint + wkhung - 30, ypaint + 45 + 20 * i, MGraphics.VCENTER | MGraphics.HCENTER);
        }
        cmdClose.paint(g);
    }
}