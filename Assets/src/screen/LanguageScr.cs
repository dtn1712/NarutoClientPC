using src.lib;
using src.main;
using src.model;
using src.Objectgame;
using src.screen;

public class LanguageScr : TScreen, IActionListener
{
    private static LanguageScr gi;
    public static Command cmdEng, cmdVn;
    private int indexRow = -1;
    private int popupW, popupH, popupX, popupY;

    public LanguageScr()
    {
        cmdEng = new Command("English", 1);
        cmdEng.setPos(GameCanvas.w / 2 - 120 + 5, GameCanvas.h / 2 + 20, LoadImageInterface.img_use,
            LoadImageInterface.img_use_focus);
        cmdVn = new Command("Tiếng Việt", 2);
        cmdVn.setPos(GameCanvas.w / 2 + 5, GameCanvas.h / 2 + 20, LoadImageInterface.img_use,
            LoadImageInterface.img_use_focus);
    }

    public void perform(int idAction, object p)
    {
        switch (idAction)
        {
            case 1:
                break;
            case 2:
                break;
            case 1000:
                GameCanvas.currentDialog = null;
                MResources.languageID = indexRow == 0 ? MResources.Lang_VI : MResources.Lang_EN;
                saveLanguageID(MResources.languageID);
                MResources.loadLanguage();
                GameCanvas.GetInstance().InitGameCanvas();
                GameCanvas.loginScrr.switchToMe();
                break;
        }
    }

    public override void switchToMe()
    {
        GameScr.gH = GameCanvas.h;
        GameCanvas.loadBG(0);

        base.switchToMe();
            
        GameScr.instance = null;

        // ==============================
        TileMap.bgID = (sbyte) (MSystem.currentTimeMillis() % 9);

        GameScr.loadCamera(true, Char.myChar().cx, Char.myChar().cy);
        GameScr.cmx = 100;
        // ==============================
        popupW = 170;
        popupH = 175;
        if (GameCanvas.w == 128 || GameCanvas.h <= 208)
        {
            popupW = 126;
            popupH = 160;
        }
        popupX = GameCanvas.w / 2 - popupW / 2;
        popupY = GameCanvas.h / 2 - popupH / 2;
        if (GameCanvas.h <= 250)
            popupY -= 10;

        indexRow = -1;
        if (!GameCanvas.isTouch)
            indexRow = 0;
    }


    private void saveLanguageID(int languageID)
    {
        Rms.saveRMSInt("indLanguage", languageID);
    }

    public override void update()
    {
        GameScr.cmx++;
        if (GameScr.cmx > GameCanvas.w * 3 + 100)
            GameScr.cmx = 100;

        base.update();
    }

    public override void updateKey()
    {
        if (GameCanvas.keyPressed[2] || GameCanvas.keyPressed[4] || GameCanvas.keyPressed[6] ||
            GameCanvas.keyPressed[8])
            indexRow = indexRow == 0 ? 1 : 0;

        if (GameCanvas.isPointerJustRelease)
            if (GameCanvas.isPointerHoldIn(popupX + 10, popupY + 45, popupW - 10, 70))
            {
                if (GameCanvas.isPointerClick)
                    indexRow = (GameCanvas.py - (popupY + 45)) / 35;
                perform(1000, null);
            }
        if (getCmdPointerLast(cmdEng))
        {
        }
        if (getCmdPointerLast(cmdVn))
        {
        }
        base.updateKey();
        GameCanvas.clearKeyPressed();
    }
}