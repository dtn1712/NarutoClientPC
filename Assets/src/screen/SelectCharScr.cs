using src.lib;
using src.main;
using src.model;
using src.Objectgame;
using src.real;

namespace src.screen
{
    public class SelectCharScr : TScreen, IActionListener
    {
        private static SelectCharScr _instance;

        public static int w1char, h1char, padchar, x, y, indexSelect;
        public long[] charIDDB;

        public int dem;
        public sbyte[] gender, type;

        private int gsgreenField1Y;
        public bool isLoadImg;
        public int[] lv;
        public string[] name;
        public int[] parthead, partleg, partbody, partWp, level;
        public string[] phai;

        private int waitToPerform;

        private SelectCharScr()
        {
            w1char = 48;
            h1char = 85;
            if (GameCanvas.w < 160)
            {
                w1char = 32;
            }

            padchar = 7;
            x = ((GameCanvas.w - 3 * w1char) >> 1) - 5;
            y = GameCanvas.hh - (h1char >> 1) + 10;
            if (GameCanvas.isTouch && GameCanvas.w > 200)
            {
                w1char = 74;
                padchar = 25;
                h1char = 110;
                x = ((GameCanvas.w - 3 * w1char) >> 1) - 20;
                y = GameCanvas.hh - (h1char >> 1);

                if (GameCanvas.w < 320)
                {
                    padchar = 6;
                    x = ((GameCanvas.w - 3 * w1char) >> 1) - 6;
                }
            }
            y = GameCanvas.h - MGraphics.getImageHeight(LoadImageInterface.imgTatus) - 20;
            y = y < 0 ? 0 : y;
            left = null;
            center = new Command("", this, 1000, null);
            right = new Command(MResources.EXIT, this, 1001, null);
            right.setPos(GameCanvas.w - LoadImageInterface.img_use.GetWidth() - 2,
                GameCanvas.h - LoadImageInterface.img_use.GetHeight() - 2, LoadImageInterface.img_use,
                LoadImageInterface.img_use_focus);
        }

        public void perform(int idAction, object p)
        {
            switch (idAction)
            {
                case 1000:
                    DoSelect();
                    break;
                case 1001:
                    GameCanvas.ResetToLoginScr(true);
                    break;
            }
        }

        public static SelectCharScr GetInstance()
        {
            return _instance ?? (_instance = new SelectCharScr());
        }

        public void InitSelectChar()
        {
            charIDDB = new long[3];
            name = new string[3];
            parthead = new int[3];
            partleg = new int[3];
            partbody = new int[3];
            partWp = new int[3];
            level = new int[3];
            lv = new int[3];
            phai = new string[3];
            gender = new sbyte[3];
            type = new sbyte[3];
            indexSelect = (GameCanvas.isTouch) ? indexSelect = -1 : indexSelect = 0;             
        
            GameScr.readPart(); // đọc part
            SmallImage.init(); // đọc dữ liệu hình ảnh
        }

        private void DoSelect()
        {
            if (name[indexSelect] != null)
            {
                Service.GetInstance().SelectChar(charIDDB[indexSelect]);
                GameCanvas.StartWaitDlg(MResources.PLEASEWAIT);
            }
            else
            {
                CreatCharScr.gI().switchToMe();
            }
            indexSelect = -1;
        }

        private int countDown = 0;
        public override void updateKey()
        {
            countDown++;
            if (countDown >= 2)
            {
                countDown = 0;
                return;
            }
            base.updateKey();

            if (GameCanvas.keyPressed[6])
            {
                indexSelect++;
                if (indexSelect >= 3)
                    indexSelect = 0;
            }
            if (GameCanvas.keyPressed[4])
            {
                indexSelect--;
                if (indexSelect < 0)
                    indexSelect = 2;
            }

            if (GameCanvas.isPointerDown)
                if (GameCanvas.isPointerHoldIn(x, y + 140, 3 * (w1char + padchar), h1char))
                {
                    var index = (GameCanvas.px - x) / (w1char + padchar);
                    if (index > 2)
                        index = 2;
                    if (index < 0)
                        index = 0;
                    indexSelect = index;
                }
            if (GameCanvas.isPointerJustRelease)
            {
                if (GameCanvas.isPointer(x, y + 140, 3 * (w1char + padchar), h1char))
                    waitToPerform = 5;
                else
                    indexSelect = -1;
                GameCanvas.isPointerJustRelease = false;
            }
            GameCanvas.clearKeyHold();
            GameCanvas.clearKeyPressed();
        }

        public override void update()
        {
            if (GameCanvas.imgCloud == null || !isLoadImg)
            {
                isLoadImg = GameCanvas.loadBG(1);
            }
            GameScr.cmx++;
            dem++;
        
            if (dem >= 1000)
                dem = 0;
            if (GameScr.cmx > GameCanvas.w * 3 + 100)
                GameScr.cmx = 100;
            if (waitToPerform > 0)
            {
                waitToPerform--;
                if (waitToPerform == 0 && indexSelect >= 0)
                {
                    DoSelect();
                }      
            }
        }

        public override void switchToMe()
        {
            isLoadImg = GameCanvas.loadBG(1);
            gsgreenField1Y = GameScr.gH - Image.getHeight(LoadImageInterface.imgTrangtri) + 170;
            MSystem.gcc();
            base.switchToMe();
        }


        public override void paint(MGraphics g)
        {
            GameCanvas.paintBGGameScr(g);
            g.drawImage(LoadImageInterface.imgTatus, GameCanvas.w / 2, y + 200, MGraphics.HCENTER | MGraphics.BOTTOM);

            for (var i = -((GameScr.cmx >> 1) % Image.getWidth(LoadImageInterface.imgTrangtri));
                i < GameScr.gW;
                i += Image.getWidth(LoadImageInterface.imgTrangtri))
                g.drawImage(LoadImageInterface.imgTrangtri, i, gsgreenField1Y - 150, 0);
            for (var i = 0; i < 3; i++)
                g.drawImage(LoadImageInterface.imgRock, x + i * (w1char + padchar), y + 175, 0);

            for (var i = 0; i < 3; i++)
            {
                if (name[i] == null)
                    continue;


                Part ph = GameScr.parts[parthead[i]],
                    pl = GameScr.parts[partleg[i]],
                    pb = GameScr.parts[partbody[i]];

                var cx = x + i * (w1char + padchar + 2) + w1char / 2;
                if (!GameCanvas.isTouch)
                {
                    if (indexSelect == i)
                    {
                        FontManager.GetInstance().tahoma_8b.DrawStringShadown(g, MResources.CHARINGFO[0] + ": " + name[i], GameCanvas.hw,
                            y - 45, MFont.CENTER);
                        FontManager.GetInstance().tahoma_7b_white.DrawStringBorder(g, MResources.CHARINGFO[1] + ": " + lv[i], GameCanvas.hw,
                            y - 28, 2);
                    }
                }
                else
                {
                    var cy = y + h1char / 2 - 15;
                    g.drawImage(LoadImageInterface.bongChar,
                        cx + Char.CharInfo[dem % 15 < 5 ? 0 : 1][1][1] +
                        pl.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][1][0]].dx - 5,
                        cy - Char.CharInfo[dem % 15 < 5 ? 0 : 1][1][2] +
                        pl.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][1][0]].dy + 150, 0);

                    SmallImage.DrawSmallImage(g, pl.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][1][0]].id,
                        cx + Char.CharInfo[dem % 15 < 5 ? 0 : 1][1][1] +
                        pl.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][1][0]].dx,
                        cy - Char.CharInfo[dem % 15 < 5 ? 0 : 1][1][2] +
                        pl.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][1][0]].dy + 140, 0, 0);
                    SmallImage.DrawSmallImage(g, pb.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][2][0]].id,
                        cx + Char.CharInfo[dem % 15 < 5 ? 0 : 1][2][1] +
                        pb.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][2][0]].dx,
                        cy - Char.CharInfo[dem % 15 < 5 ? 0 : 1][2][2] +
                        pb.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][2][0]].dy + 140, 0, 0);
                    SmallImage.DrawSmallImage(g, ph.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][0][0]].id,
                        cx + Char.CharInfo[dem % 15 < 5 ? 0 : 1][0][1] +
                        ph.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][0][0]].dx,
                        cy - Char.CharInfo[dem % 15 < 5 ? 0 : 1][0][2] +
                        ph.pi[Char.CharInfo[dem % 15 < 5 ? 0 : 1][0][0]].dy + 140, 0, 0);

                    FontManager.GetInstance().tahoma_8b.DrawStringShadown(g, name[i], cx, y + h1char / 2 + 55, MFont.CENTER);
                    FontManager.GetInstance().tahoma_7b_white.DrawStringBorder(g, MResources.CHARINGFO[1] + ": " + lv[i], cx,
                        y + h1char / 2 + 72, 2);
                }
            }
            base.paint(g);
        }


    }
}