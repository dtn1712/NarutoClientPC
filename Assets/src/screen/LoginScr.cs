using System;
using System.Threading;
using src.lib;
using src.main;
using src.model;
using src.network;
using src.Objectgame;
using src.real;
using UnityEngine;
using Char = src.Objectgame.Char;

namespace src.screen
{
    public class LoginScr : TScreen, IActionListener
    {

        public const string LOGIN_ACTION = "LOGIN";
        public const string REGISTER_ACTION = "REGISTER";
        
        public static bool isLogout;
        public static bool isTest = false;
        private static MBitmap imgTitle;
        private static LoginScr _instance;

        public static bool isLoggingIn;
        private readonly Command cmdLogin;
        private Command cmdCheck;
        private readonly Command cmdFogetPass;
        private readonly Command cmdRes;
        private readonly Command cmdMenu;
        private readonly Command cmdLoginFB;
        private readonly Command cmdLoginGoogle;


        private string[] _currentTip;
        private int dir = 1;
        private int focus;
        private int g;
        private bool isRes = false;

        private string strNick = "";
        private string _user, _pass;
        
        private TField tfIp;
        private TField tfPass;
        private TField tfRegPass;
        private TField tfUser;
        
        private int tipid = -1;
        
        private readonly int v = 2;
        private readonly int wC;
        private int yL;
        private readonly int defYL;
        private int ylogo = -40;

        private readonly int yt;

        private string _connectAction;

        public LoginScr()
        {
            imgTitle = GameCanvas.loadImage("/screen/title.png");
            _instance = this;
            var ip = Rms.loadRMSString("ipgame");
            if (ip != null) GameMidlet.IP = ip;
            TileMap.bgID = (sbyte) (MSystem.currentTimeMillis() % 9);
            if (TileMap.bgID == 5 || TileMap.bgID == 6)
            {
                TileMap.bgID = 4;
            }
            GameScr.loadCamera(true, Char.myChar().cx, Char.myChar().cy);
            GameScr.cmx = 100;
            if (GameCanvas.h > 200)
            {
                defYL = GameCanvas.hh - 80;
            }
            else
            {
                defYL = GameCanvas.hh - 65;
            }
            
            ResetLogo();
            wC = GameCanvas.w - 30;
            if (wC < 70)
            {
                wC = 70;
            }

            if (wC > 99)
            {
                wC = 99;
            }

            yt = GameCanvas.h - 150;
            yt = GameCanvas.h / 2 - 100 + 60 > 0 ? GameCanvas.h / 2 - 100 + 60 : 0;

            if (GameCanvas.h <= 160)
                yt = 20;

            tfUser = new TField
            {
                x = GameCanvas.hw - 75,
                y = yt + 20,
                width = wC + 50,
                height = ITEM_HEIGHT + 2,
                isFocus = false
            };
            tfUser.setIputType(TField.INPUT_ALPHA_NUMBER_ONLY);
            tfPass = new TField
            {
                x = GameCanvas.hw - 75,
                y = yt + 60,
                width = wC + 50,
                height = ITEM_HEIGHT + 2,
                isFocus = false
            };
            tfPass.setIputType(TField.INPUT_TYPE_PASSWORD);

            tfIp = new TField
            {
                x = GameCanvas.hw - 75,
                y = yt + 100,
                width = wC + 50,
                height = ITEM_HEIGHT + 2,
                isFocus = false
            };
            tfIp.setIputType(TField.INPUT_ALPHA_NUMBER_ONLY);

            tfRegPass = new TField
            {
                x = GameCanvas.hw - 20,
                y = yt += 35,
                width = wC,
                height = ITEM_HEIGHT + 2,
                isFocus = false
            };
            tfRegPass.setIputType(TField.INPUT_TYPE_PASSWORD);

            var listauto = Rms.loadRMS(Rms.autoSetting);
            if (listauto == null || listauto.Length < SettingScreen.defautAuto.Length)
            {
                Rms.saveRMS(Rms.autoSetting, SettingScreen.defautAuto);
            }
            else
            {
                GameScr.isAutoDanh = listauto[SettingScreen.AUTO_DANH] == 1;
                GameScr.isAutoNhatItem = listauto[SettingScreen.AUTO_NHAT] == 1;
                Music.isplayMusic = listauto[SettingScreen.MUSIC] == 1;
                Music.isplaySound = listauto[SettingScreen.SOUND] == 1;
                GuiMain.isAnalog = listauto[SettingScreen.TYPE_MOVE] == 1;
            }
        
            tfUser.setText(Rms.loadRMSString("acc"));
            tfPass.setText(Rms.loadRMSString("pass"));
            tfPass.setText(Rms.loadRMSString("pass"));

            focus = 0;
            cmdLogin = new Command(GameCanvas.w > 200 ? MResources.LOGIN1 : MResources.LOGIN2, this, 2000, null);
            cmdLogin.setPos(tfPass.x - 2 + 80, tfPass.y + 30 + (isTest ? 35 : 0), LoadImageInterface.btnLogin0,
                LoadImageInterface.btnLogin1);
            cmdCheck = new Command(MResources.REMEMBER, this, 2001, null);
            cmdRes = new Command(MResources.REGISTER, this, 2002, null);

            cmdRes.setPos(tfPass.x - 2 + 38, tfPass.y + 30 + (isTest ? 35 : 0), LoadImageInterface.btnLogin0,
                LoadImageInterface.btnLogin1);
            cmdFogetPass = new Command(MResources.FORGETPASS, this, 2004, null);
            cmdMenu = new Command(MResources.MENU, this, 2003, null);
            cmdMenu.setPos(tfPass.x - 2 + 40, tfPass.y + 30 + (isTest ? 35 : 0), LoadImageInterface.btnLogin0,
                LoadImageInterface.btnLogin1);

            cmdLoginFB = new Command("", this, 2010, null);
            cmdLoginFB.setPos(tfPass.x - 2, tfPass.y + 30 + (isTest ? 35 : 0), LoadImageInterface.imgIconFb0,
                LoadImageInterface.imgIconFb1);

            cmdLoginGoogle = new Command("", this, 2011, null);
            cmdLoginGoogle.setPos(tfPass.x + 120, tfPass.y + 30 + (isTest ? 35 : 0), LoadImageInterface.imgIconGoogle0,
                LoadImageInterface.imgIconGoogle1);
        }

        public void perform(int idAction, object p)
        {
            switch (idAction)
            {
                case 2010:
                case 2011:
                    GameCanvas.StartDglThongBao("Chức năng đang phát triển");
                    break;
                case 2000:
                case 20001:
                case 20002:
                case 20003:
                case 20004:
                case 20005:
                case 200041:
                case 200042:
                    GameCanvas.menu.showMenu = false;
                    DoLogin();
                    break;
                case 2002:
                    ConfirmRegister();
                    break;
                case 2003:
                    DoMenu();
                    break;
                case 2004:
                    GameCanvas.inputDlg.show(MResources.INPUT_NICK, new Command(MResources.OK, this, 20041, null), TField.INPUT_TYPE_ANY);
                    break;
                case 20041:
                    strNick = GameCanvas.inputDlg.tfInput.getText();
                    GameCanvas.EndDlg();
                    if (strNick.Equals(""))
                    {
                        GameCanvas.StartOkDlg(MResources.NOT_INPUT_USERNAME);
                    }
                    else
                    {
                        GameCanvas.startYesNoDlg(MResources.ASK_REG_NUM,
                            new Command(MResources.YES, this, 200421, null),
                            new Command(MResources.NO, this, 200422, null));
                    }
                    break;
//            case 200421:
//                GameCanvas.EndDlg();
//                DoGetForgetPass(strNick);
//                break;
                case 200422:
                    GameCanvas.StartOkDlg(MResources.replace(MResources.GETPASS_BY_NUMPHONE, strNick));
                    break;
                case 4000:
                    DoRegister();
                    break;
                case 5001:
                    GameMidlet.GetInstance().Exit();
                    break;
                case 5002:
                    GameCanvas.currentDialog = null;
                    break;
            }
        }

        public override void switchToMe()
        {
            ResetLogo();
            GameScr.gH = GameCanvas.h;
            GameCanvas.loadBG(0);
            base.switchToMe();

            if (GameCanvas.menu != null)
            {
                GameCanvas.menu = new Menu();
            }
            
            GameCanvas.menu = new Menu();
            var isSoftKey = Rms.loadRMSInt("isSoftKey");
            if (isSoftKey <= 0)
            {
                Rms.saveRMSInt("isSoftKey", 1);
            }

            var sound = Rms.loadRMSInt("isSound");
            if (sound < 0)
            {
                Rms.saveRMSInt("isSound", 1);
            }
            
            Music.init();
            Music.play(Music.MLogin, 5);
            if (!isLogout) return;
            isLogout = false;
            GameCanvas.gameScr = null;
        }

        public static LoginScr GetInstance()
        {
            return _instance ?? (_instance = new LoginScr());
        }

        private void DoMenu()
        {
            var menu = new Vector();

            menu.addElement(new Command(MResources.NEWREGISTER, this, 1002, null));
            menu.addElement(cmdFogetPass);
            menu.addElement(new Command(MResources.FORUM, this, 1003, null));
            menu.addElement(new Command(MResources.CONFIG, this, 1006, null));

            menu.addElement(new Command(MResources.EXIT, GameCanvas.instance, 8885, null));
            GameCanvas.menu.startAt(menu);
        }

        private void ConfirmRegister()
        {
            if (tfUser.getText().Equals(""))
            {
                GameCanvas.StartOkDlg(MResources.NOT_INPUT_USERNAME);
            } else if (tfPass.getText().Equals(""))
            {
                GameCanvas.StartOkDlg(MResources.NOT_INPUT_PASS1);
            } else if (tfUser.getText().Length < 8)
            {
                GameCanvas.StartOkDlg(MResources.USERNAME_LENGHT);
            }
            else
            {
                var ch = tfUser.getText().ToCharArray();
                for (var i = 0; i < ch.Length; i++)
                {
                    if (TField.setNormal(ch[i])) continue;
                    GameCanvas.StartOkDlg(MResources.NOT_SPEC_CHARACTER);
                    return;
                }

                GameCanvas.msgdlg.setInfo(
                    MResources.REGISTER_TEXT[0] + " " + tfUser.getText() + " " +
                    MResources.REGISTER_TEXT[1], new Command(MResources.ACCEPT, this, 4000, null), null,
                    new Command(MResources.NO, GameCanvas.instance, 8882, null));
            
                GameCanvas.currentDialog = GameCanvas.msgdlg;
            }
        }

        private void DoRegister()
        {
            HandleIpForTest();

            _user = tfUser.getText().Trim(); 
            _pass = tfPass.getText().Trim();
            _connectAction = REGISTER_ACTION;
            
            var isConnectSuccess = GameCanvas.Connect();
            if (!isConnectSuccess)
            {
                _connectAction = null;
            }
            else
            {
                GameCanvas.StartWaitDlg(MResources.REGISTERING);
            }

        }

//    private void DoGetForgetPass(string user)
//    {
//        isFAQ = false;
//        GameCanvas.StartWaitDlg(mResources.CONNECTING);
//        
//        var isConnectSuccess = GameCanvas.Connect();
//        if (!isConnectSuccess) return;
//        
//        Thread.Sleep(1000);
//        
//        GameCanvas.StartWaitDlg(mResources.PLEASEWAIT);
//    }

        private void HandleIpForTest()
        {
            if (tfIp.getText() != null && tfIp.getText().Trim().Length > 0)
            {
                var ip = tfIp.getText();
                if (ip.Contains("@"))
                {
                    try
                    {
                        var textIp = ip.Split('@');
                        if (textIp.Length > 1)
                        {
                            Rms.saveRMSString("ipgame", textIp[0]);
                            GameMidlet.IP = textIp[0].ToLower().Trim();
                            GameMidlet.VERSION = textIp[1];
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogError(e);
                    }
                }
                else
                {
                    Rms.saveRMSString("ipgame", tfIp.getText());
                    GameMidlet.IP = tfIp.getText().ToLower().Trim();
                }
            }
        }

        private void DoLogin()
        {
            _connectAction = LOGIN_ACTION;
            _user = tfUser.getText().Trim(); 
            _pass = tfPass.getText().Trim();

            HandleIpForTest();

            SavePass(_user, _pass);
                
            if (GameMidlet.VERSION == null || _user.Equals(""))
            {
                return;
            }
            if (_pass.Equals(""))
            {
                focus = 1;
                tfUser.isFocus = false;
                tfPass.isFocus = true;
                return;
            }
            var listauto = Rms.loadRMS(Rms.autoSetting);
            if (listauto == null || listauto.Length < SettingScreen.defautAuto.Length)
            {
                Rms.saveRMS(Rms.autoSetting, SettingScreen.defautAuto);
            }
            else
            {
                GameScr.isAutoDanh = listauto[SettingScreen.AUTO_DANH] == 1;
                GameScr.isAutoNhatItem = listauto[SettingScreen.AUTO_NHAT] == 1;
            }

            
            isLoggingIn = true;
            focus = 0;
            
            var isConnectSuccess = GameCanvas.Connect();
            if (!isConnectSuccess)
            {
                _connectAction = null;
            }
            else
            {
                GameCanvas.StartWaitLoginDialog();
            }
        }

        private static void SavePass(string user, string pass)
        {
            Rms.saveRMSInt("check", 1);
            Rms.saveRMSString("acc", user);
            Rms.saveRMSString("pass", pass);
        }

        private int countDown = 0;
        public override void update()
        {
            countDown++;
            if (countDown >= 2)
            {
                countDown = 0;
                return;
            }
            updateKey();
            if (connectCallBack && !isConnect)
            {
                isConnect = connectCallBack = false;
                GameCanvas.StartOkDlg(!isCloseConnect ? MResources.SERVER_MAINTENANCE : "Mất kết nối với máy chủ. Vui lòng thử lại !");
                isCloseConnect = false;
            }
            
            if (GameCanvas.imgCloud == null && GameCanvas.gameTick % 2 == 0 && GameCanvas.imgBG != null &&
                GameCanvas.imgBG[0] == null)
            {
                GameCanvas.loadBG(0);
            }

            GameScr.cmx++;
            if (GameScr.cmx > GameCanvas.w * 3 + 100)
            {
                GameScr.cmx = 100;
            }

            tfUser.update();
            tfPass.update();
            if (isTest)
            {
                tfIp.update();
                if (isRes)
                    tfRegPass.update();
            }
            
            UpdateLogo();
            if (g >= 0)
            {
                ylogo += dir * g;
                g += dir * v;
                if (g <= 0)
                    dir *= -1;
                if (ylogo > 0)
                {
                    dir *= -1;
                    g -= 2 * v;
                }
            }
            if (tipid >= 0 && GameCanvas.gameTick % 100 == 0)
            {
                tipid++;
                if (tipid >= MResources.tips.Length) tipid = 0;
                _currentTip = FontManager.GetInstance().tahoma_7_white.SplitFontArray(MResources.tips[tipid], GameCanvas.w - 40);
            }
        }

        private void UpdateLogo()
        {
            if (defYL != yL)
            {
                yL += (defYL - yL) >> 1;
            }
        }

        public override void keyPress(int keyCode)
        {
            if (tfUser.isFocus)
            {
                tfUser.keyPressed(keyCode);
            }
            else if (tfPass.isFocus)
            {
                tfPass.keyPressed(keyCode);
            }
            else if (isRes && tfRegPass.isFocus)
            {
                tfRegPass.keyPressed(keyCode);
            }
            else if (tfIp.isFocus)
            {
                tfIp.keyPressed(keyCode);
            }
            
            base.keyPress(keyCode);
        }

        public override void paint(MGraphics g)
        {
            try
            {
                g.setColor(0x3aadfe);
                g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);

                GameCanvas.paintBGGameScr(g);
                GameCanvas.ResetTrans(g);
                if (GameCanvas.currentDialog == null)
                {
                    Paint.SubFrame(GameCanvas.hw - 85, yt - 60, 170, 160 + (isTest ? 40 : 0), g);

                    if (imgTitle != null)
                    {
                        g.drawImage(imgTitle, GameCanvas.hw, yt - 66, 3);
                    }
                    FontManager.GetInstance().tahoma_7_white.DrawString(g, "Email or Phone", tfUser.x, tfUser.y - 12, 0);
                    FontManager.GetInstance().tahoma_7_white.DrawString(g, "Password", tfPass.x, tfPass.y - 12, 0);
                    tfUser.paint(g);
                    tfPass.paint(g);
                    if (isTest)
                    {
                        tfIp.paint(g);
                        if (isRes)
                            tfRegPass.paint(g);
                    }
                    g.setClip(0, 0, GameCanvas.w, GameCanvas.h);
                }
                else
                {
                    if (_currentTip != null)
                    {
                        for (var i = 0; i < _currentTip.Length; i++)
                        {
                            FontManager.GetInstance().tahoma_7_white.DrawString(g, _currentTip[i], GameCanvas.w / 2,
                                tfUser.y - 10 + 10 * i, 2);
                        }
                    }
                }

                var s = GameMidlet.VERSION;
                if (isLoggingIn)
                {
                    s = Session.GetInstance().GetByteCount();
                }
                FontManager.GetInstance().tahoma_7_grey.DrawString(g, s, GameCanvas.w - 5, 5, 1);
                if (GameCanvas.currentDialog == null)
                {
                    cmdLoginFB.paint(g);
                    cmdLoginGoogle.paint(g);
                    cmdLogin.paint(g);
                    cmdRes.paint(g);
                }

                base.paint(g);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
       
        }

        public override void updateKey()
        {
            if (getCmdPointerLast(cmdLoginFB))
            {
                cmdLoginFB.performAction();
                GameCanvas.clearPointerEvent();
            }
            if (getCmdPointerLast(cmdLoginGoogle))
            {
                cmdLoginGoogle.performAction();
                GameCanvas.clearPointerEvent();
            }
            if (getCmdPointerLast(cmdLogin))
            {
                cmdLogin.performAction();
                GameCanvas.clearPointerEvent();
            }
            if (getCmdPointerLast(cmdRes))
            {
                cmdRes.performAction();
                GameCanvas.clearPointerEvent();
            }

            base.updateKey();
        }

        private void ResetLogo()
        {
            yL = -50;
        }

        public string GetAction()
        {
            return _connectAction;
        }

        public string GetUser()
        {
            return _user;
        }

        public string GetPass()
        {
            return _pass;
        }

        public void Clear()
        {
            _connectAction = null;
            _user = null;
            _pass = null;
        }
    }
}