using src.lib;
using src.main;
using src.model;
using src.Objectgame;

namespace src.screen
{
    public class XinChoScreen : TScreen
    {
        private static XinChoScreen _instance;

        public static bool isChangeKhu;
        public bool isRemoveImg;
        public int timeSleepLoadTile;
        public long timeStartOff;

        public override void switchToMe()
        {
            timeStartOff = MSystem.currentTimeMillis();
            timeSleepLoadTile = 20;
            isRemoveImg = true;
            base.switchToMe();
        }

        public static XinChoScreen GetInstance()
        {
            return _instance ?? (_instance = new XinChoScreen());
        }

        public override void update()
        {
            if (isRemoveImg)
            {
                if (!isChangeKhu)
                {
                    Mob.cleanImg();
                    BgItem.cleanImg();
                }
                isChangeKhu = false;
                SmallImage.CleanImg();
                MSystem.my_Gc();
                isRemoveImg = false;
            }
            if (TileMap.imgMaptile == null)
            {
                timeSleepLoadTile++;
                if (timeSleepLoadTile > 20)
                {
                    timeSleepLoadTile = 0;
                    TileMap.GetInstance().loadimgTile(TileMap.tileID);
                }
            }
            if ((MSystem.currentTimeMillis() - timeStartOff) / 1000 > 3)

                if (GameCanvas.gameScr != null)
                {
                    Char.myChar().timeLastchangeMap = MSystem.currentTimeMillis();
                    Char.myChar().timedelay = 5000;
                    GameCanvas.gameScr.switchToMe();
                    GameCanvas.cleanImgBg();
                }
            base.update();
        }

        public override void paint(MGraphics g)
        {
            g.setColor(0x000000);
            g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);

            g.drawImage(LoadImageInterface.imgMap, GameCanvas.w / 2, GameCanvas.h / 2,
                MGraphics.VCENTER | MGraphics.HCENTER);

            var nCham = GameCanvas.gameTick / 10 % 4;
            var text = "";
            for (var i = 0; i < nCham; i++)
                text += ".";
            for (var i = 0; i < 3 - nCham; i++)
                text += " ";
            FontManager.GetInstance().tahoma_7.DrawString(g, TileMap.GetInstance().mapName + text, GameCanvas.w / 2, GameCanvas.h / 2 - 4, 2);
            g.drawRegion(LoadImageInterface.imgXinCho, 0, 16 * (GameCanvas.gameTick / 2 % 3), 16, 16, 0,
                GameCanvas.w / 2, 3 * GameCanvas.h / 4 + 20, MGraphics.VCENTER | MGraphics.HCENTER, false);

            base.paint(g);
        }
    }
}