using src.lib;
using src.main;
using src.model;

public class SplashScr : TScreen
{
    public static MBitmap imgLogo;
    public static int splashScrStat;

    private static int time;

    public bool isSwitch;

    public static void loadSplashScr()
    {
        splashScrStat = 0;
        time = 80;
    }

    public override void update()
    {
        if (splashScrStat >= time && !isSwitch)
        {
            if (Rms.loadRMSInt("indLanguage") >= 0)
                GameCanvas.loginScrr.switchToMe();
            else
                GameCanvas.loginScrr.switchToMe();
            isSwitch = true;
        }
        splashScrStat++;
    }

    public override void paint(MGraphics g)
    {
        g.setColor(0xffffff);
        g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);

        if (imgLogo != null)
            g.drawImage(imgLogo, GameCanvas.w >> 1, GameCanvas.h >> 1, MGraphics.HCENTER | MGraphics.VCENTER);
    }
}