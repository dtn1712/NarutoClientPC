using src.lib;
using src.main;
using src.model;
using src.real;
using src.screen;

public class DownloadImageScreen : TScreen
{
    public static bool isOKNext, isDownLoadedSuccess;

    public static DownloadImageScreen intansce;
    public int idAdd;
    public int idNext, idTo;
    public MBitmap imgBgLoading, frameLoading, ongLoading;

    public MBitmap[] imgFrameChaLoadingr = new MBitmap[6];
    public int indexPhanTram, indexpaintload;
    public bool isTheoList;
    public int[] listId;
    public long timeCoolDownNextImage = 15000, timeStart;

    public void switchToMe()
    {
        isDownLoadedSuccess = isTheoList = false;
        idNext = 0;
        idTo = 0;
        indexPhanTram = indexpaintload = 0;
        base.switchToMe();
    }

    public void switchToMe(int idScreenTo)
    {
        isDownLoadedSuccess = isTheoList = false;
        idNext = 0;
        indexPhanTram = indexpaintload = 0;
        idTo = idScreenTo;
        base.switchToMe();
    }

    public void switchToMe(int idAdd, int[] listIdd)
    {
        this.idAdd = idAdd;
        listId = listIdd;
        isDownLoadedSuccess = false;
        isTheoList = true;
        idNext = 0;
        indexPhanTram = indexpaintload = 0;
        base.switchToMe();
    }

    public void loadingImg()
    {
        if (imgBgLoading == null)
            imgBgLoading = GameCanvas.loadImage("/GuiNaruto/loading/imgBgLoading.png");

        if (frameLoading == null)
            frameLoading = GameCanvas.loadImage("/GuiNaruto/loading/ongLoading.png");

        if (ongLoading == null)
            ongLoading = GameCanvas.loadImage("/GuiNaruto/loading/frameLoading.png");

        if (imgFrameChaLoadingr[0] == null)
            for (var i = 0; i < imgFrameChaLoadingr.Length; i++)
                if (imgFrameChaLoadingr[i] == null)
                    imgFrameChaLoadingr[i] = GameCanvas.loadImage("/GuiNaruto/loading/image-" + (i + 1) + ".png");
    }

    public override void update()
    {
        loadingImg();
        if (idNext < (isTheoList == false ? SmallImage.nBigImage : listId.Length))
        {
            indexPhanTram = idNext * frameLoading.GetWidth() /
                            (isTheoList == false ? SmallImage.nBigImage : listId.Length);
            indexpaintload += (indexPhanTram - indexpaintload) / 2;
            if (indexpaintload >= frameLoading.GetWidth()) indexpaintload = frameLoading.GetWidth();
        }
        else
        {
            indexpaintload = frameLoading.GetWidth();
        }
        if (!isTheoList && idNext < SmallImage.nBigImage)
        {
            var data = Rms.loadRMS(MSystem.getPathRMS(SmallImage.getPathImage(idNext)) + "" + idNext);
            if (data != null)
            {
                idNext++;
                if (idNext >= SmallImage.nBigImage)
                {
                    isDownLoadedSuccess = true;
                    Rms.saveRMSData(SmallImage.keyOKDownloaded, new sbyte[] {1});
                }
                data = null;
            }
            else if (isOKNext || MSystem.currentTimeMillis() - timeStart > timeCoolDownNextImage)
            {
                timeStart = MSystem.currentTimeMillis();
                Service.GetInstance().RequestImage(idNext);
                isOKNext = false;
                idNext++;
                if (idNext >= SmallImage.nBigImage)
                {
                    isDownLoadedSuccess = true;
                    Rms.saveRMSData(SmallImage.keyOKDownloaded, new sbyte[] {1});
                }
            }
        }
        else if (isTheoList && idNext < listId.Length)
        {
            var data =
                Rms.loadRMS(MSystem.getPathRMS(SmallImage.getPathImage(listId[idNext] + idAdd) + "" +
                                               (listId[idNext] + idAdd)));
            if (data != null)
            {
                idNext++;
                if (idNext >= listId.Length)
                {
                    isDownLoadedSuccess = true;
                    Rms.saveRMSData(SmallImage.keyOKDownloaded, new sbyte[] {1});
                    //XinChoScreen.gI().switchToMe();
                }
                data = null;
            }
            else if (isOKNext || MSystem.currentTimeMillis() - timeStart > timeCoolDownNextImage)
            {
                timeStart = MSystem.currentTimeMillis();
                Service.GetInstance().RequestImage(listId[idNext] + idAdd);
                isOKNext = false;
                idNext++;
                Rms.saveRMSData(SmallImage.keyOKDownloaded, new sbyte[] {0});
                if (idNext >= listId.Length)
                    isDownLoadedSuccess = true;
            }
        }
        if (isDownLoadedSuccess && GameCanvas.gameTick % 7 == 0)
            if (!isTheoList)
            {
                if (idTo == 0)
                    SelectCharScr.GetInstance().switchToMe();
                else if (idTo == 1)
                {
                    CreatCharScr.gI().switchToMe();
                }
            }
            else
            {
                XinChoScreen.GetInstance().switchToMe();
            }
        base.update();
    }

    public override void paint(MGraphics g)
    {
        g.setColor(0x000000);
        g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
        g.drawRegionScalse(imgBgLoading, 0, 0, imgBgLoading.GetWidth(), imgBgLoading.GetHeight(), 0,
            GameCanvas.w / 2, GameCanvas.h / 2, MGraphics.VCENTER | MGraphics.HCENTER, false,
            GameCanvas.w * 100 / imgBgLoading.GetWidth());
        g.setColor(0x3c3a39);
        g.fillRect(GameCanvas.w / 2 - frameLoading.GetWidth() / 2, 7 * GameCanvas.h / 8 - frameLoading.GetHeight() / 2,
            frameLoading.GetWidth(), frameLoading.GetHeight());
        g.drawRegion(frameLoading, 0, 0, indexpaintload, frameLoading.GetHeight(),
            0, GameCanvas.w / 2 - frameLoading.GetWidth() / 2, 7 * GameCanvas.h / 8 - frameLoading.GetHeight() / 2,
            MGraphics.TOP | MGraphics.LEFT);
        g.drawImage(ongLoading, GameCanvas.w / 2, 7 * GameCanvas.h / 8, MGraphics.VCENTER | MGraphics.HCENTER);
        if (imgFrameChaLoadingr != null && imgFrameChaLoadingr[GameCanvas.gameTick / 2 % 6] != null)
            g.drawImage(imgFrameChaLoadingr[GameCanvas.gameTick / 2 % 6],
                GameCanvas.w / 2 - frameLoading.GetWidth() / 2 + indexpaintload,
                7 * GameCanvas.h / 8 - 18, MGraphics.VCENTER | MGraphics.HCENTER);

        base.paint(g);
    }

    public static DownloadImageScreen gI()
    {
        if (intansce == null) intansce = new DownloadImageScreen();
        intansce.loadingImg();
        return intansce;
    }
}