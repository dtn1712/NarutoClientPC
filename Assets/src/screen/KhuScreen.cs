using src.lib;
using src.main;
using src.model;
using src.real;

namespace src.screen
{
    public class KhuScreen : TScreen, IActionListener
    {
        public static KhuScreen instance;
        private readonly Command cmdClose;
        public int coutFc;

        public sbyte[][] listKhu;
        public int minKhu = 15;
        public Scroll srclist = new Scroll();
        public int wKhung = 170, hKhung = 150;
        public int xpaint, ypaint;

        public KhuScreen()
        {
            xpaint = GameCanvas.w / 2 - wKhung / 2;
            ypaint = GameCanvas.h / 2 - hKhung / 2;
            cmdClose = new Command("", this, 2, null);
            cmdClose.setPos(xpaint + wKhung - LoadImageInterface.closeTab.width / 2,
                ypaint, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
        }

        public void perform(int idAction, object p)
        {
            switch (idAction)
            {
                case 2:
                    GameScr.GetInstance().switchToMe();
                    break;
            }
        }

        public static KhuScreen GetInstance()
        {
            return instance ?? (instance = new KhuScreen());
        }

        public override void updateKey()
        {
            if (GameCanvas.keyPressed[5] || getCmdPointerLast(cmdClose))
                if (cmdClose != null)
                {
                    GameCanvas.isPointerJustRelease = false;
                    GameCanvas.keyPressed[5] = false;
                    keyTouch = -1;
                    cmdClose?.performAction();
                }
            srclist.updateKey();
            srclist.updatecm();
            if (GameCanvas.isPointerJustRelease && srclist.selectedItem != -1 && srclist.selectedItem < listKhu.Length)
            {
                Service.GetInstance().RequestChangeRegion((sbyte) srclist.selectedItem);
                GameCanvas.isPointerJustRelease = false;
            }

            base.updateKey();
        }

        public override void update()
        {
            // TODO Auto-generated method stub
            base.update();
            GameScr.GetInstance().update();
            if (GameCanvas.gameTick % 4 == 0)
            {
                coutFc++;
                if (coutFc > 2)
                    coutFc = 0;
            }
        }

        public override void paint(MGraphics g)
        {
            // TODO Auto-generated method stub
            base.paint(g);
            GameScr.GetInstance().paint(g);
            Paint.paintFrameNaruto(xpaint, ypaint, wKhung, hKhung + 2, g);
            Paint.PaintBoxName("Khu", xpaint + wKhung / 2 - 40, ypaint, 80, g);
            cmdClose.paint(g);
            if (listKhu != null)
            {
                srclist.setStyle((listKhu.Length > minKhu ? listKhu.Length : minKhu) / 5,
                    Image.getWidth(LoadImageInterface.ImgItem),
                    xpaint + 15, ypaint + 22 + Image.getHeight(LoadImageInterface.ImgItem) / 4,
                    wKhung - 30, hKhung - 32, true, 5);
                srclist.setClip(g, xpaint + 15, ypaint + 22 + Image.getHeight(LoadImageInterface.ImgItem) / 4,
                    wKhung - 30, hKhung - 32);
                for (var i = 0; i < (listKhu.Length > minKhu ? listKhu.Length : minKhu) / 5; i++)
                for (var j = 0; j < 5; j++)
                {
                    g.drawImage(LoadImageInterface.ImgItem,
                        xpaint + 15 + Image.getWidth(LoadImageInterface.ImgItem) * j,
                        ypaint + 30 + Image.getHeight(LoadImageInterface.ImgItem) * i + 2, 0, true);
                    if (i * 5 + j < listKhu.Length)
                        if (listKhu[i * 5 + j][0] == 0)
                            FontManager.GetInstance().tahoma_7_green.DrawString(g, i * 5 + j + 1 + "",
                                xpaint + 15 + Image.getWidth(LoadImageInterface.ImgItem) * j +
                                Image.getWidth(LoadImageInterface.ImgItem) / 2,
                                ypaint + 23 + Image.getHeight(LoadImageInterface.ImgItem) * i + 2 +
                                Image.getHeight(LoadImageInterface.ImgItem) / 2, 2);
                        else if (listKhu[i * 5 + j][0] == 1)
                            FontManager.GetInstance().tahoma_7_yellow.DrawString(g, i * 5 + j + 1 + "",
                                xpaint + 15 + Image.getWidth(LoadImageInterface.ImgItem) * j +
                                Image.getWidth(LoadImageInterface.ImgItem) / 2,
                                ypaint + 23 + Image.getHeight(LoadImageInterface.ImgItem) * i + 2 +
                                Image.getHeight(LoadImageInterface.ImgItem) / 2, 2);
                        else
                            FontManager.GetInstance().tahoma_7_red.DrawString(g, i * 5 + j + 1 + "",
                                xpaint + 15 + Image.getWidth(LoadImageInterface.ImgItem) * j +
                                Image.getWidth(LoadImageInterface.ImgItem) / 2,
                                ypaint + 23 + Image.getHeight(LoadImageInterface.ImgItem) * i + 2 +
                                Image.getHeight(LoadImageInterface.ImgItem) / 2, 2);
                }
                if (srclist.selectedItem > 0 && srclist.selectedItem < listKhu.Length)
                    Paint.paintFocus(g,
                        xpaint + 15 + srclist.selectedItem % 5 * Image.getWidth(LoadImageInterface.ImgItem) + 11 -
                        LoadImageInterface.ImgItem.GetWidth() / 4,
                        ypaint + 30 + srclist.selectedItem / 5 * Image.getHeight(LoadImageInterface.ImgItem) + 13 -
                        LoadImageInterface.ImgItem.GetWidth() / 4
                        , LoadImageInterface.ImgItem.GetWidth() - 9, LoadImageInterface.ImgItem.GetWidth() - 9, coutFc);
                GameCanvas.ResetTrans(g);
            }
        }
    }
}