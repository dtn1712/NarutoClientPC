﻿using src.lib;
using src.network;
using System;
using MessagePack;
using src.real.dto.response;
using src.screen;
using src.utils;
using UnityEngine;

namespace src.real.controller
{
    public class LoginController : IController
    {
        public void processMessage(Message msg)
        {
            var loginResponeData = IoUtils.ReadMessage(msg);
            var loginResponeDto = MessagePackSerializer.Deserialize<LoginResponseDTO>(loginResponeData);
            
            createData(loginResponeDto);
            
            GameScr.readEfect(); // doc ef
            SmallImage.readImage();
            GameScr.readPart();
            GameScr.readSkill();
        }
        
        
        private void createData(LoginResponseDTO loginResponeDto)
        {
            try
            {
                Rms.saveRMS("nj_arrow", Converter.ConvertByteToSbyte(loginResponeDto.njArrow));
                Rms.saveRMS("nj_effect", Converter.ConvertByteToSbyte(loginResponeDto.njEffect));
                Rms.saveRMS("nj_image", Converter.ConvertByteToSbyte(loginResponeDto.njImages));
                Rms.saveRMS("nj_part", Converter.ConvertByteToSbyte(loginResponeDto.njPart));
                Rms.saveRMS("nj_skill", Converter.ConvertByteToSbyte(loginResponeDto.njSkill));
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
        }
    }
}