﻿//using System;
//using src.lib;
//using src.network;
//using src.Objectgame;
//using src.screen;
//using UnityEngine;
//using Char = src.Objectgame.Char;
//
//namespace src.real.controller
//{
//    public class PlayerMoveController : IController
//    {
//        public void processMessage(Message msg)
//        {
//            var c = new Char();
//            var typemove = msg.Reader().ReadByte();
//            try
//            {
//                if (typemove == Constants.CAT_MONSTER)
//                {
//                    var idmobb = msg.Reader().ReadShort();
//                    var idmobx = msg.Reader().ReadShort();
//                    var idmoby = msg.Reader().ReadShort();
//                    var namemobb = msg.Reader().ReadUtf();
//                    var hpmob = msg.Reader().ReadInt();
//                    for (var i = 0; i < GameScr.vMob.size(); i++)
//                    {
//                        var mobb = (Mob) GameScr.vMob.elementAt(i);
//                        if (mobb.mobId == idmobb && mobb.status == Mob.MA_INHELL)
//                        {
//                            //LogDebug.println("moveeee mob  "+mobb.mobId);
//                            mobb.injureThenDie = false;
//                            mobb.status = Mob.MA_WALK;
//                            mobb.x = idmobx;
//                            mobb.y = idmoby - 10;
//                            mobb.mobName = namemobb;
//                            ServerEffect.addServerEffect(37, mobb.x, mobb.y - mobb.getH() / 4 - 10, 1);
//                            mobb.hp = hpmob > mobb.maxHp ? mobb.maxHp : hpmob;
//                        }
//                    }
//                }
//                else if (typemove == Constants.CAT_PLAYER)
//                {
//                    LogDebug.println(" -----> NHAN PLAYER KHAC DI CHUYEN");
//                    var charId = msg.Reader().ReadLong();
//                    var cxMoveLast = msg.Reader().ReadShort();
//                    var cyMoveLast = msg.Reader().ReadShort();
//                    var cName = msg.Reader().ReadUtf();
//                    var typePk = msg.Reader().ReadByte();
//                    for (var i = 0; i < GameScr.vCharInMap.size(); i++)
//                    {
//                        c = (Char) GameScr.vCharInMap.elementAt(i);
//                        if (c.charID == charId)
//                        {
//                            c.cxMoveLast = cxMoveLast;
//                            c.cyMoveLast = cyMoveLast;
//                            c.cName = cName;
//                            c.typePk = typePk;
//
//                            c.moveTo(c.cxMoveLast, c.cyMoveLast);
//                            c.lastUpdateTime = MSystem.currentTimeMillis();
//                        }
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                Debug.LogError(e);
//            }
//        }
//    }
//}