﻿using src.lib;
using src.main;
using src.network;
using src.screen;

namespace src.real.controller
{
    public class ServerMessageController : IController
    {
        public void processMessage(Message msg)
        {
            var typeDialog = msg.Reader().ReadByte();

            if (typeDialog == 0)
            {
                GameCanvas.StartOkDlg(msg.Reader().ReadUtf());
            }
            else if (typeDialog == 1)
            {
                GameCanvas.startYesNoDlg(msg.Reader().ReadUtf(), new Command("Yes", GameScr.GetInstance(), 1, null),
                    new Command("No", GameScr.GetInstance(), 0, null));
            }
            else if (typeDialog == 2)
            {
                GameCanvas.startDlgTime(msg.Reader().ReadUtf(),
                    new Command("Yes", GameCanvas.instance, 8882, null),
                    msg.Reader().ReadInt());
            }
            else if (typeDialog == 3)
            {
                GameCanvas.StartDglThongBao(msg.Reader().ReadUtf());
            }
            else if (typeDialog == 4)
            {
                GameCanvas.StartDglThongBao(msg.Reader().ReadUtf());
            }
        }
    }
}