﻿using MessagePack;
using src.network;
using src.Objectgame;
using src.real.dto.response;
using src.utils;

namespace src.real.controller
{
    public class ComeHomeController : IController
    {
        public void processMessage(Message msg)
        {
            var charGoHomeResponseData = IoUtils.ReadMessage(msg);
            var charGoHomeResponeDto = MessagePackSerializer.Deserialize<CharGoHomeResponseDTO>(charGoHomeResponseData);
            Char.myChar().statusMe = Char.A_FALL;
            Char.myChar().cHP = charGoHomeResponeDto.hp;
            Char.myChar().cMP = charGoHomeResponeDto.mp;
          
        }
    }
}