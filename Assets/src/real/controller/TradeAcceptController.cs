﻿using MessagePack;
using src.Gui;
using src.network;
using src.real.dto.response.trade;
using src.utils;
using src.lib;
using src.main;
using src.model;
using src.Objectgame;
using src.screen;

namespace src.real.controller
{
    [MessagePackObject]
    public class TradeAcceptController : IController
    {
        public void processMessage(Message msg)
        {
            var tradeAcceptData = IoUtils.ReadMessage(msg);
            var tradeAcceptResponeDTO = MessagePackSerializer.Deserialize<TradeAcceptResponseDTO>(tradeAcceptData);
            var charidP = tradeAcceptResponeDTO.id;
            var cTrade = GameScr.findCharInMap(charidP);
            GameCanvas.startYesNoDlg(cTrade.cName + tradeAcceptResponeDTO.content, TradeGui.cmdTradeEnd,
                new Command(MResources.CANCEL, GameCanvas.instance, 8882, null));
            //GameScr.gI().tradeGui = null;
            for (var i = 0; i < Char.myChar().ItemMyTrade.Length; i++)
            {
                Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
                Char.myChar().ItemMyTrade[i] = null;
            }
        }
    }
}