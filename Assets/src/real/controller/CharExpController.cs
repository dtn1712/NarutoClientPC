﻿using MessagePack;
using src.network;
using src.lib;
using src.real.dto.response;
using src.screen;
using src.utils;

namespace src.real.controller
{
    public class CharExpController : IController
    {
        public void processMessage(Message msg)
        {
            var charExpResponseData = IoUtils.ReadMessage(msg);
            var charExpResponseDto = MessagePackSerializer.Deserialize<CharExpResponseDTO>(charExpResponseData);
            var acc = GameScr.findCharInMap(charExpResponseDto.CharId);
            if (acc != null)
            {
                GameScr.startFlyText("+" + charExpResponseDto.Exp + "exp", acc.cx, acc.cy - acc.ch - 5, 0, -2, MFont.YELLOW);
            }
        }
    }
}