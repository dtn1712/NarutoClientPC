﻿using System;
using MessagePack;
using src.lib;
using src.network;
using src.real.dto.response;
using src.screen;
using src.utils;
using UnityEngine;
using Char = src.Objectgame.Char;

namespace src.real.controller
{
    public class PlayerOutController : IController
    {
        public void processMessage(Message msg)
        {
            try
            {
                var playerRemoveData = IoUtils.ReadMessage(msg);
                var playerRemoveResponseDto = MessagePackSerializer.Deserialize<PlayerOutResponseDTO>(playerRemoveData);
                
                var charIdRemove = playerRemoveResponseDto.id;
                if (charIdRemove == Char.myChar().charID)
                {
                    return;
                }
                
                for (var i = 0; i < GameScr.vCharInMap.size(); i++)
                {
                    var c = (Char) GameScr.vCharInMap.elementAt(i);
                    if (charIdRemove == c.charID)
                    {
                        GameScr.vCharInMap.removeElementAt(i);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
           
        }
    }
}