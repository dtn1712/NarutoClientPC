﻿using System;
using src.network;
using src.lib;
using src.utils;
using MessagePack;
using src.Gui;
using src.Objectgame;
using src.real.dto.character;
using UnityEngine;
using Char = src.Objectgame.Char;

namespace src.real.controller
{
    public class GetInventoryController : IController
    {
        public void processMessage(Message msg)
        {
            try
            {
                var charInventoryData = IoUtils.ReadMessage(msg);
                var charInventoryDto = MessagePackSerializer.Deserialize<CharInventoryDTO>(charInventoryData);

                Char.myChar().luong = charInventoryDto.Gold;
                Char.myChar().xu = charInventoryDto.Xu;
                Char.myChar().arrItemBag = new Item[charInventoryDto.Items.Count];
                TabNangCap.numberItemInBag = Char.myChar().arrItemBag.Length;
                for (var i = 0; i < Char.myChar().arrItemBag.Length; i++)
                {
                    var charItemDto = charInventoryDto.Items[i];
                    var idItem = charItemDto.PositionIndex;
                    var itemTemplateId = charItemDto.ItemTemplateId;
                    var lvItem = charItemDto.Level;
                    if (itemTemplateId != -1)
                    {
                        Char.myChar().arrItemBag[i] = new Item
                        {
                            itemId = idItem,
                            typeUI = Item.UI_BAG,
                            indexUI = i,
                            template = ItemTemplates.Get((short) itemTemplateId)
                        };
                        Char.myChar().arrItemBag[i].template.lvItem = (short) lvItem;
                        Char.myChar().arrItemBag[i].isUseInventory = charItemDto.IsUseInventory;

                        if (Char.myChar().arrItemBag[i].template != null &&  Item.isBlood(Char.myChar().arrItemBag[i].template.type)
                            || Char.myChar().arrItemBag[i].template != null && Item.isMP(Char.myChar().arrItemBag[i].template.type))
                        {
                            Char.myChar().arrItemBag[i].quantity = charItemDto.Quantity;
                            Char.myChar().arrItemBag[i].value = charItemDto.ValueBuff;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }
    }
}