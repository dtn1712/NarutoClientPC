﻿using src.lib;
using src.main;
using src.model;
using src.network;
using src.Objectgame;
using src.screen;
using UnityEngine;

namespace src.real.controller
{
    public class PartyController : IController
    {
        public void processMessage(Message message)
        {
            var typeParty = message.Reader().ReadByte();
            Debug.Log("type Party  " + typeParty);
            if (typeParty == PartyType.INVITE_PARTY)
            {
                Party.gI().charId = message.Reader().ReadShort();
                GameCanvas.startYesNoDlg(message.Reader().ReadUtf(), GameScr.GetInstance().cmdAcceptParty,
                    new Command(MResources.CANCEL, GameCanvas.instance, 8882, null));
            }
            else if (typeParty == PartyType.GET_INFOR_PARTY)
            {
                var PartyId = message.Reader().ReadShort();
                var CharLeaderid = message.Reader().ReadShort();
                if (CharLeaderid == Char.myChar().charID)
                    Party.gI().isLeader = true;
                var membersize = message.Reader().ReadByte();
                var charIDmeber = new short[membersize];
                var lvchar = new short[membersize];
                var idhead = new short[membersize];
                for (var i = 0; i < membersize; i++)
                {
                    charIDmeber[i] = message.Reader().ReadShort();
                    lvchar[i] = message.Reader().ReadShort();
                    idhead[i] = message.Reader().ReadShort();
                    //m.dos.writeShort(members.get(i).getLevel());
                    //  m.dos.writeShort(members.get(i).getIdPartHead());
                }
                GameScr.hParty.containsKey(PartyId + "");
                GameScr.hParty.put(PartyId + "",
                    new Party(PartyId, charIDmeber, CharLeaderid, lvchar, idhead));
            }
            else if (typeParty == PartyType.OUT_PARTY)
            {
                var idChar = message.Reader().ReadShort();
                if (idChar == Char.myChar().charID)
                {
                    GameScr.hParty.clear();
                    var party = (Party) GameScr.hParty.get(Char.myChar().idParty + "");
                    Party.vCharinParty.removeAllElements();
                }

                var chaRe = GameScr.findCharInMap(idChar);
                Party partyy = null;
                if (chaRe != null)
                {
                    partyy = (Party) GameScr.hParty.get(chaRe.idParty + "");
                    for (var i = 0; i < Party.vCharinParty.size(); i++)
                    {
                        var charRemove = (Char) Party.vCharinParty.elementAt(i);
                        if (charRemove.charID == idChar)
                        {
                            charRemove.idParty = -1;
                            charRemove.isLeader = false;
                            Party.vCharinParty.removeElementAt(i);
                        }
                    }
                    if (Party.vCharinParty.size() < 2)
                    {
                        GameScr.hParty.clear();
                        Party.vCharinParty.removeAllElements();
                    }
                }
                else
                {
                    chaRe = Char.myChar();
                    for (var i = 0; i < Party.vCharinParty.size(); i++)
                    {
                        var charRemove = (Char) Party.vCharinParty.elementAt(i);
                        if (charRemove.charID == idChar)
                        {
                            charRemove.idParty = -1;
                            charRemove.isLeader = false;
                            Party.vCharinParty.removeElementAt(i);
                        }
                    }
                }
                if (Party.vCharinParty.size() <= 0)
                    GameScr.hParty.remove(chaRe.idParty + "");
            }
            else if (typeParty == PartyType.DISBAND_PARTY)
            {
                var idParty = message.Reader().ReadShort();

                for (var i = 0; i < Party.vCharinParty.size(); i++)
                {
                    var ch = (Char) Party.vCharinParty.elementAt(i);
                    ch.idParty = -1;
                    ch.isLeader = false;
                    Party.vCharinParty.removeElementAt(i);
                }
                GameScr.hParty.remove(idParty + "");
            }
            else if (typeParty == PartyType.GET_INFOR_NEARCHAR)
            {
                GameScr.charnearByme.removeAllElements();
                var sizeCharlist = message.Reader().ReadByte();
                for (var i = 0; i < sizeCharlist; i++)
                {
                    var ch = new Char
                    {
                        charID = message.Reader().ReadShort(),
                        cName = message.Reader().ReadUtf(),
                        idParty = message.Reader().ReadShort()
                    };
                    GameScr.charnearByme.addElement(ch);
                }
            }
        }
    }
}