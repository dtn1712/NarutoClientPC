﻿using src.network;
using src.utils;
using MessagePack;
using src.Objectgame;
using src.real.dto.response;
using src.screen;

namespace src.real.controller
{
    public class BossAppearController : IController
    {
        public void processMessage(Message msg)
        {
            var bossAppearResponseData = IoUtils.ReadMessage(msg);
            var bossAppearResponseDTO =
                MessagePackSerializer.Deserialize<BossAppearResponseDTO>(bossAppearResponseData);
            var isTurnOnBoss = bossAppearResponseDTO.isTurnOnBoss;
            Mob.isBossAppear = isTurnOnBoss;
            var appear = bossAppearResponseDTO.idAppear;
            if (Mob.isBossAppear)
            {
                for (var i = 0; i < GameScr.vMob.size(); i++)
                {
                    var dem_mob = (Mob) GameScr.vMob.elementAt(i);
                    if (dem_mob != null && dem_mob.isBoss)
                    {
                        dem_mob.injureThenDie = false;
                        dem_mob.status = Mob.MA_WALK;
                        dem_mob.x = dem_mob.xFirst;
                        dem_mob.y = dem_mob.yFirst;
                        ServerEffect.addServerEffect(37, dem_mob.x, dem_mob.y - dem_mob.getH() / 4 - 10, 1);
                        dem_mob.hp = dem_mob.maxHp;
                        break;
                    }
                }
            }  
        }
    }
}