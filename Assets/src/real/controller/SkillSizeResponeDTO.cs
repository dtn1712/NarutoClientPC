﻿using MessagePack;
using System.Collections.Generic;
using src.real.dto.response;

namespace src.real.controller
{
    [MessagePackObject]
    public class SkillSizeResponeDTO
    {
        [Key("size")]
        public byte size { get; set; }

        [Key("listSkill")]
        public List<SKillCharResponeDTO> listSkill { get; set; }
    }
}