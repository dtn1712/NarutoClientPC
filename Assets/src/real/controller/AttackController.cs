﻿using src.lib;
using src.network;
using src.utils;
using MessagePack;
using src.real.dto.response;
using System.Collections.Generic;
using System.Linq;
using src.Objectgame;
using src.screen;

namespace src.real.controller
{
    public class AttackController : IController
    {
        public void processMessage(Message msg)
        {
            var attackData = IoUtils.ReadMessage(msg);
            var attackDto = MessagePackSerializer.Deserialize<AttackResponseDTO>(attackData);

            if (attackDto.AttackerType == Constants.TYPE_PLAYER)
            {
                var c = GameScr.findCharInMap(attackDto.AttackerId) ?? Char.myChar();
                c.cMP = attackDto.AttackerMp;
                if (attackDto.TargetType == Constants.TYPE_MONSTER)
                {
                    for (var i = 0; i < GameScr.vMob.size(); i++)
                    {
                        var target = attackDto.Targets.ElementAt(0);
                        var mob = (Mob) GameScr.vMob.elementAt(i);
                        if (mob.mobId == target.Id)
                        {
                            c.mobFocus = mob;
                            c.mobFocus.hp = target.Hp;
                            c.cdame = target.HitDamage;
                            c.mobFocus.hp = c.mobFocus.hp < 0 ? 0 : c.mobFocus.hp;
                            if (c.cdame > 0)
                            {
                                GameScr.startFlyText("-" + c.cdame, c.mobFocus.x, c.mobFocus.y - 2 * mob.h - 5,
                                    0, -2, MFont.RED);
                            }
                            else
                            {
                                GameScr.startFlyText("miss", c.mobFocus.x, c.mobFocus.y - 2 * mob.h - 5, 0, -2,
                                    MFont.RED);
                            }

                            if (attackDto.AttackerId != Char.myChar().charID)
                            {
                                c.cdir = c.cx - c.mobFocus.x > 0 ? -1 : 1;
                                c.mobFocus.setInjure();
                                c.mobFocus.injureBy = c;
                                c.mobFocus.status = Mob.MA_INJURE;
                            }
                            if (mob.hp <= 0)
                            {
                                mob.status = Mob.MA_DEADFLY;
                            }
                        }
                    }

                    if (attackDto.SkillAttackId > -1 && attackDto.SkillAttackId < GameScr.sks.Length - 1)
                    {
                        if (c.charID != Char.myChar().charID)
                        {
                            Music.play(attackDto.SkillAttackId == 0 ? Music.ATTACK_0 : Music.SKILL2, 0.5f);
                            c.setSkillPaint(GameScr.sks[attackDto.SkillEffectId], Skill.ATT_STAND);
                        }
                    }
                }
                else if (attackDto.TargetType == Constants.TYPE_PLAYER)
                {
                    //player attack player
                    c.cMP = attackDto.AttackerMp;
                    for (var i = 0; i < attackDto.Targets.Count; i++)
                    {
                        var target = attackDto.Targets.ElementAt(i);
                        var charTarget = GameScr.findCharInMap(target.Id);
                        if (charTarget != null)
                        {
                            charTarget.cHP = target.Hp;
                            charTarget.DoInjure(1, 0, false, 1);
                            c.cdir = c.cx - charTarget.cx > 0 ? -1 : 1;
                            ServerEffect.addServerEffect(25, charTarget.cx, charTarget.cy - 20, 1);
                            GameScr.startFlyText("-" + target.HitDamage, charTarget.cx, charTarget.cy - 60, 0, -2,
                                MFont.RED);
                        }
                    }
                    if (attackDto.SkillAttackId > -1 && attackDto.SkillAttackId < GameScr.sks.Length - 1)
                    {
                        if (c.charID != Char.myChar().charID)
                        {
                            Music.play(Music.ATTACK_0, 0.5f);
                            c.setSkillPaint(GameScr.sks[attackDto.SkillEffectId], Skill.ATT_STAND);
                        }
                    }
                }
                //paint skill for all char
            }
            else if (attackDto.AttackerType == Constants.TYPE_MONSTER)
            {
                var mobAttId = attackDto.AttackerId;
                Mob mob = null;
                for (var i = 0; i < GameScr.vMob.size(); i++)
                {
                    var mobs = (Mob) GameScr.vMob.elementAt(i);
                    if (mobs.mobId == mobAttId)
                    {
                        mob = mobs;
                    }
                }

                if (mob != null)
                {
                    var target = attackDto.Targets.ElementAt(0);
                    var victimChar = GameScr.findCharInMap(target.Id);
                    mob.cFocus = victimChar;
                    if (mob.cFocus == null)
                    {
                        mob.f = -1;
                        mob.cFocus = Char.myChar();
                    }
                    mob.dir = mob.x - mob.cFocus.cx > 0 ? -1 : 1;

                    var hpmat = target.Hp;
                    mob.cFocus.cHP = hpmat;
                    if (victimChar != null)
                        victimChar.cHP = hpmat;

                    mob.dame = target.HitDamage;

                    mob.status = Mob.MA_ATTACK;
                    GameScr.startFlyText("-" + mob.dame, mob.cFocus.cx, mob.cFocus.cy - 60, 0, -3, MFont.RED);

                    if (mob.typeMob == Mob.TYPE_MOB_TOOL)
                    {
                        ServerEffect.addServerEffect(21, mob.x, mob.y, 1);
                    }

                    mob.setAttack(mob.cFocus);
                }
            }
        }
    }
}