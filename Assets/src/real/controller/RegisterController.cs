﻿
using MessagePack;
using src.main;
using src.network;
using src.real.dto.request;
using src.real.dto.response;
using src.utils;

namespace src.real.controller
{
    public class RegisterController : IController
    {
        public void processMessage(Message msg)
        {
            var registerResponseData = IoUtils.ReadMessage(msg);
            var registerResponseDto = MessagePackSerializer.Deserialize<RegisterResponseDTO>(registerResponseData);
            GameCanvas.startOK(registerResponseDto.Info, 8882, null);
        }
    }
}