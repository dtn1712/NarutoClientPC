﻿using System.Collections.Generic;
using MessagePack;
using src.Gui;
using src.network;
using src.real.dto.response;
using src.utils;

namespace src.real.controller
{
    public class GetPricingController : IController
    {
        public void processMessage(Message msg)
        {
            var paymentInfoData = IoUtils.ReadMessage(msg);
            var paymentInfoDto = MessagePackSerializer.Deserialize<GetPricingResponseDTO>(paymentInfoData);          
            var listItem = new List<ItemPayment>();
            for (var i = 0; i < paymentInfoDto.pricingData.Count; i++)
            {
                var itemPayment = new ItemPayment
                {
                    gameMoneyAmount = paymentInfoDto.pricingData[i].gameMoneyAmount,
                    realMoneyAmount = paymentInfoDto.pricingData[i].realMoneyAmount,
                    gameMoneyUnit = paymentInfoDto.pricingData[i].gameMoneyUnit,
                    realMoneyUnit = paymentInfoDto.pricingData[i].realMoneyUnit
                };
                listItem.Add(itemPayment);
            }
            TabBag.ListItem = listItem;
        }
    }
}