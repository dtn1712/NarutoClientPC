﻿using MessagePack;
using src.lib;
using src.network;
using src.Objectgame;
using src.Objectgame.quest;
using src.real.dto;
using src.real.dto.quest;
using src.screen;
using src.utils;
using src.Gui;

namespace src.real.controller
{
    public class QuestController : IController
    {
        public void processMessage(Message message)
        {
            GuiQuest.listNhacNv.removeAllElements();
            var charQuestData = IoUtils.ReadMessage(message);
            var charQuestDto = MessagePackSerializer.Deserialize<CharQuestDTO>(charQuestData);
                    
            if (InfoItem.wcat == -1)
            {
                InfoItem.wcat = 10 * MGraphics.getImageWidth(LoadImageInterface.imgChatConner) - 10;
            }

            var quest = DTOMapper.mapQuestTemplateDTOToModel(charQuestDto.QuestTemplate);
            if (charQuestDto.Status == QuestStatus.NEW.ToString())
            {
                resetNpcQuest(0);
                MainQuestManager.getInstance().FinishQuest = null;
                MainQuestManager.getInstance().NewQuest = quest;
                MainQuestManager.getInstance().NewQuest.paint();
                        
            }
            else if (charQuestDto.Status == QuestStatus.WORKING.ToString())
            {
                resetNpcQuest(1);
                MainQuestManager.getInstance().NewQuest = null;
                MainQuestManager.getInstance().WorkingQuest = quest;
                MainQuestManager.getInstance().WorkingQuest.paint();
            }
            else if (charQuestDto.Status == QuestStatus.COMPLETED.ToString())
            {
                resetNpcQuest(2);
                MainQuestManager.getInstance().WorkingQuest = null;
                MainQuestManager.getInstance().FinishQuest = quest;
                MainQuestManager.getInstance().FinishQuest.paint();
            }

            foreach (var t in quest.RemindContentPaint)
            {
                GuiQuest.listNhacNv.add(new InfoItem(t));
            }
        }
        
        private static void resetNpcQuest(int typeNV)
        {
            for (var j = 0; j < GameScr.vNpc.size(); j++)
            {
                var npc = (Npc) GameScr.vNpc.elementAt(j);
                if (npc != null && npc.typeNV == typeNV)
                {
                    npc.typeNV = -1;
                }
            }
        }
    }
}