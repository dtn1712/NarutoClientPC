﻿using System;
using MessagePack;
using src.network;
using src.lib;
using src.model;
using src.real.dto.response;
using src.screen;
using src.utils;
using UnityEngine;
using Char = src.Objectgame.Char;

namespace src.real.controller
{
    public class CharPositionController : IController
    {
        public void processMessage(Message msg)
        {
            try
            {
                var charPositionData = IoUtils.ReadMessage(msg);
                var charPositionResponseDto = MessagePackSerializer.Deserialize<CharPositionResponseDTO>(charPositionData);
                var charId = charPositionResponseDto.id;
                var cx = charPositionResponseDto.x;
                var cy = charPositionResponseDto.y;
                var cName = charPositionResponseDto.name;
                var typePk = charPositionResponseDto.statusPK;
                for (var i = 0; i < GameScr.vCharInMap.size(); i++)
                {
                    var c = (Char) GameScr.vCharInMap.elementAt(i);
                    if (c.charID != charId) continue;
                    
                    var cStatus = GetStatus(cx - c.cx, cy - c.cy);
                    var cdir = GetDirection(cx - c.cx, cy - c.cy);
                    c.statusMe = cStatus;
                    c.cdir = cdir;
                    c.cx = cx;
                    c.cy = cy;
                    c.cName = cName;
                    c.typePk = typePk;

                    c.lastUpdateTime = MSystem.currentTimeMillis();
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        private int GetStatus(int dx, int dy)
        {
            if (dx == 0 && dy == 0)
            {
                return Char.A_STAND;
            }
            else if (dy == 0)
            {
                return Char.A_RUN;
                   
            }
            else if (dy != 0)
            {
                if (dy < 0)
                {
                    return Char.A_JUMP;
                } else if (dy > 0)
                {
                    return Char.A_FALL;
                }
            }
            return Char.A_NOTHING;
        }
        
        private int GetDirection(int dx, int dy)
        {
            var dir = 0;
            if (dy == 0)
            {
                if (dx > 0)
                {
                    dir = 1;
                }
                else if (dx < 0)
                {
                    dir = -1;
                }
                   
            }
            else if (dy != 0)
            {
                if (dx < 0)
                {
                    dir = -1;
                } else if (dx > 0)
                {
                    dir = 1;
                }
            }
            return dir;
        }
    }
}