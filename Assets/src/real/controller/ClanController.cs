﻿using src.network;
using src.lib;
using src.main;
using src.Objectgame;
using src.screen;

namespace src.real.controller
{
    public class ClanController : IController
    {
        public void processMessage(Message msg)
        {
            var typeClan = msg.Reader().ReadByte();
            switch (typeClan)
            {
                case ClanScreen.REQUETS_NAME:
                    // nhap ten clan
                    GameCanvas.StartDlgTField("Tạo Bang",
                        new Command("Tạo", GameCanvas.instance, GameCanvas.cTaoClan, null), null,
                        new Command("Cancel", GameCanvas.instance, GameCanvas.cEndDglTField, null));
                    return;
                case ClanScreen.GET_LIST_LOCAL:
                    ClanScreen.gI().ListUserLocal.removeAllElements();
                    ClanScreen.gI().clanLocalName = "";
                    ClanScreen.gI().clanLocalName = msg.Reader().ReadUtf();
                    int size_User = msg.Reader().ReadByte();
                    ClanScreen.gI().isBoss = false;
                    ClanScreen.gI().ListUserLocal.removeAllElements();
                    var idClanUserLocal = msg.Reader().ReadShort();
                    if (idClanUserLocal == Char.myChar().cIdDB)
                    {
                        ClanScreen.gI().isBoss = true;
                    }
                    for (var i = 0; i < size_User; i++)
                    {
                        var iduser = msg.Reader().ReadShort();
                        var nameClanUser = msg.Reader().ReadUtf();
                        short lvClanUser = msg.Reader().ReadByte();
                        var isHost = false;
                        var partHead = msg.Reader().ReadShort();

                        if (Char.myChar().cIdDB != iduser)
                        {
                            var otherClan = new OtherChar(idClanUserLocal, nameClanUser, lvClanUser,
                                isHost, iduser);
                            ClanScreen.gI().ListUserLocal.add(otherClan);
                        }
                    }
                    break;
                case ClanScreen.GET_LIST_GLOBAL:
                    ClanScreen.gI().ListUserGlobal.removeAllElements();
                    ClanScreen.gI().clanGolbalName = "";
                    ClanScreen.gI().clanGolbalName = msg.Reader().ReadUtf();
                    int sizeUserGobal = msg.Reader().ReadByte();

                    ClanScreen.gI().isBossGobal = false;
                    ClanScreen.gI().ListUserGlobal.removeAllElements();

                    var idClanUser = msg.Reader().ReadShort();
                    if (idClanUser == Char.myChar().cIdDB)
                        ClanScreen.gI().isBossGobal = true;
                    for (var i = 0; i < sizeUserGobal; i++)
                    {
                        var iduser = msg.Reader().ReadShort();
                        var nameClanUser = msg.Reader().ReadUtf();
                        var lvClanUser = msg.Reader().ReadShort();
                        var partHead = msg.Reader().ReadShort();
                        var isHost = false;

                        if (Char.myChar().cIdDB != iduser)
                        {
                            var otherClan = new OtherChar(idClanUser, nameClanUser, lvClanUser, isHost, iduser);
                            ClanScreen.gI().ListUserGlobal.add(otherClan);
                        }
                    }
                    break;
            }
        }
    }
}