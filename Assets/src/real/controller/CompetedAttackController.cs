﻿using src.network;
using src.lib;
using System;
using MessagePack;
using src.main;
using src.real.dto.response;
using src.utils;
using UnityEngine;

namespace src.real.controller
{
    public class CompetedAttackController : IController
    {
        public void processMessage(Message msg)
        {
            try
            {
                var completeAttackResponseData = IoUtils.ReadMessage(msg);
                var completeAttackResponseDto = MessagePackSerializer.Deserialize<CompetedAttackResponseDTO>(completeAttackResponseData);
                var type = completeAttackResponseDto.Type;
                var idchar = completeAttackResponseDto.PlayerInvitedId;
                var loimoi = completeAttackResponseDto.InviteInfo;
                GameCanvas.startYesNoDlg(loimoi,
                    new Command("Đồng ý", GameCanvas.instance, GameCanvas.cDongYThachDau, idchar + ""),
                    new Command("Không", GameCanvas.instance, 8882, null));
            }
            catch (Exception e)
            {
               Debug.LogError(e);
            }
        }
       
    }
}