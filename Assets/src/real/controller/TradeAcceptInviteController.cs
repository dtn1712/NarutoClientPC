﻿using MessagePack;
using src.main;
using src.network;
using src.Objectgame;
using src.real.dto.response.trade;
using src.screen;
using src.utils;

namespace src.real.controller
{
    public class TradeAcceptInviteController : IController
    {
        public void processMessage(Message msg)
        {
            var tradeAcceptInviteData = IoUtils.ReadMessage(msg);
            var tradeAcceptInviteDTO =
                MessagePackSerializer.Deserialize<TradeAcceptInviteResponseDTO>(tradeAcceptInviteData);
            var CharIDpartner = tradeAcceptInviteDTO.id;
            if (Char.myChar().partnerTrade == null)
                Char.myChar().partnerTrade = GameScr.findCharInMap(CharIDpartner);

            GameScr.isBag = false;
            GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
            GameCanvas.gameScr.guiMain.menuIcon.iconTrade.performAction();
            Service.GetInstance().RequestInventory();
        }
        
    }
}