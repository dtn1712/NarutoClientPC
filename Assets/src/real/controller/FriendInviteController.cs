﻿using MessagePack;
using src.network;
using src.real.dto.response.FriendDTO;
using src.utils;
using src.lib;
using src.main;
using src.model;
using src.Objectgame;
using src.screen;

namespace src.real.controller
{
    public class FriendInviteController : IController
    {
        public void processMessage(Message msg)
        {
            var friendAcceptData = IoUtils.ReadMessage(msg);
            var friendAcceptDTO = MessagePackSerializer.Deserialize<InviteFriendResponseDTO>(friendAcceptData);
            Char.myChar().idFriend = friendAcceptDTO.id;
            var tempDebug = friendAcceptDTO.content;
            GameCanvas.startYesNoDlg(tempDebug, GameScr.GetInstance().cmdComfirmFriend,
                new Command(MResources.CANCEL, GameCanvas.instance, 8882, null));
        }
    }
}