﻿using MessagePack;
using src.lib;
using src.network;
using src.Objectgame;
using src.Objectgame.quest;
using src.real.dto.response;
using src.screen;
using src.utils;

namespace src.real.controller
{
    public class NpcController : IController
    {
        public void processMessage(Message msg)
        {
            var npcInfoResponseData = IoUtils.ReadMessage(msg);
            var npcInfoResponseDto = MessagePackSerializer.Deserialize<NpcResponseDTO>(npcInfoResponseData);
            GameScr.vNpc.removeAllElements();
            var sizeNpc = npcInfoResponseDto.size;
            for (var i = 0; i < sizeNpc; i++)
            {
                var idNpc = npcInfoResponseDto.listNpc[i].id;
                var npcX = npcInfoResponseDto.listNpc[i].npcX;
                var npcY = npcInfoResponseDto.listNpc[i].npcY;
                var npcIdtemplate = npcInfoResponseDto.listNpc[i].templateId;

                var npcz = new Npc(npcX, npcY, npcIdtemplate);

                if (MainQuestManager.getInstance().NewQuest != null &&
                    MainQuestManager.getInstance().NewQuest.IdNpcReceive == npcIdtemplate)
                {
                    npcz.typeNV = 0;
                }

                if (MainQuestManager.getInstance().WorkingQuest != null &&
                    MainQuestManager.getInstance().WorkingQuest.IdNpcResolve == npcIdtemplate)
                {
                    npcz.typeNV = 1;
                }

                if (MainQuestManager.getInstance().FinishQuest != null &&
                    (MainQuestManager.getInstance().FinishQuest.IdNpcReceive == npcIdtemplate
                     || MainQuestManager.getInstance().FinishQuest.IdNpcResolve == npcIdtemplate))
                {
                    npcz.typeNV = 1;
                }

                GameScr.vNpc.addElement(npcz);
            }
        }
    }
}