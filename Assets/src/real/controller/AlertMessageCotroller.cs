﻿using MessagePack;
using src.main;
using src.network;
using src.real.dto.response.message;
using src.utils;

namespace src.real.controller
{
    public class AlertMessageCotroller : IController
    {
        public void processMessage(Message msg)
        {
            var alertMessageData = IoUtils.ReadMessage(msg);
            var alertMessageDto = MessagePackSerializer.Deserialize<AlertMessageResponseDTO>(alertMessageData);
            GameCanvas.StartOkDlg(alertMessageDto.info);
        }
    }
}