﻿using System;
using MessagePack;
using src.network;
using src.real.dto.response;
using src.utils;
using src.screen;
using UnityEngine;

namespace src.real.controller
{
    public class CivilWarDropKageController : IController
    {
        public void processMessage(Message msg)
        {
            try
            {
                var civilWarDropKageData = IoUtils.ReadMessage(msg);
                var dropKageItemResponeDTO =
                    MessagePackSerializer.Deserialize<DropKageItemResponseDTO>(civilWarDropKageData);
                var id = dropKageItemResponeDTO.id;
                var idTemplatee = dropKageItemResponeDTO.idTemplate;
                var itemX = dropKageItemResponeDTO.x;
                var itemY = dropKageItemResponeDTO.y;
               
                ItemMap itemDrop = null;    
                itemDrop = new ItemMap((short)id, (short)idTemplatee, itemX, itemY);
                
                GameScr.vItemMap.addElement(itemDrop);
                GameScr.vNhatItemMap.addElement(itemDrop);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
            
            
        }
    }
}