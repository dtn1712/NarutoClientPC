﻿using MessagePack;
using src.lib;
using src.main;
using src.network;
using src.Objectgame;
using src.real.dto.response;
using src.screen;
using src.utils;

namespace src.real.controller
{
    public class MenuNpcController : IController
    {
        public void processMessage(Message msg)
        {
            var menuNpcResponseData = IoUtils.ReadMessage(msg);
            var menuNpcResponseDto = MessagePackSerializer.Deserialize<MenuNpcResponeDTO>(menuNpcResponseData); 
            var idNpc = menuNpcResponseDto.IdNpc;
            int size = menuNpcResponseDto.Length;

            var vmenu = new Vector();
            for (var i = 0; i < size; i++)
            {
                vmenu.addElement(new Command(menuNpcResponseDto.ListMenu[i], GameCanvas.instance, GameCanvas.cMenuNpc, i + "")); //i index_menu
            }
            
            Npc npcDem = null;
            for (var i = 0; i < GameScr.vNpc.size(); i++)
            {
                var npc = (Npc) GameScr.vNpc.elementAt(i);
                if (npc != null && npc.template != null && npc.template.npcTemplateId == idNpc &&
                    npc.Equals(Char.myChar().npcFocus))
                    npcDem = npc;
            }
            if (npcDem == null || vmenu.size() <= 0) return;
            GameCanvas.menu.doCloseMenu();
            GameCanvas.menu.startAtNPC(vmenu, 0, npcDem.npcId, npcDem, "");
        }

    }
}