﻿using MessagePack;
using src.network;
using src.Objectgame;
using src.real.dto.response;
using src.utils;
using UnityEngine;

namespace src.real.controller
{
    public class SkillCharController : IController
    {
        public void processMessage(Message msg)
        {
            var skillCharResponseData = IoUtils.ReadMessage(msg);
            var sKillCharResponeDto = MessagePackSerializer.Deserialize<SkillSizeResponeDTO>(skillCharResponseData);
            int sizeKill = sKillCharResponeDto.size;
            for (var i = 0; i < sizeKill; i++)
            {
                var id = sKillCharResponeDto.listSkill[i].idSkill;
                var idIcon = sKillCharResponeDto.listSkill[i].idIcon;
                var idIconTron = sKillCharResponeDto.listSkill[i].idIconShort; //icon skill tron
                var name = sKillCharResponeDto.listSkill[i].nameSkill;
                var typeskill = sKillCharResponeDto.listSkill[i].type; //chu dong or bi dong
                var mota = sKillCharResponeDto.listSkill[i].description; //mo ta
                var ntaget = sKillCharResponeDto.listSkill[i].ntarget; //so luong muc tieu danh
                var typebuff = sKillCharResponeDto.listSkill[i].typeBuff; //loai skill buff
                var subeff = sKillCharResponeDto.listSkill[i].subEff; //so luong muc tieu danh
                var range = sKillCharResponeDto.listSkill[i].rangeaeo; //pham vi
                var ideff = sKillCharResponeDto.listSkill[i].idEffect;
                var nlevel = sKillCharResponeDto.listSkill[i].length;
                var skilltemplate = new SkillTemplate(id, name, idIcon, typeskill, ntaget, typebuff,
                    subeff, range, mota)
                {
                    iconTron = idIconTron,
                    ideff = ideff
                };
                skilltemplate.KhoitaoLevel(nlevel); //// tong so level cua 1 skill
                for (var j = 0; j < nlevel; j++)
                {
                    skilltemplate.levelChar = new short[nlevel];
                    skilltemplate.levelChar[j] = sKillCharResponeDto.listSkill[i].listLevel[j].lvChar; // mp hao
                    skilltemplate.mphao[j] = sKillCharResponeDto.listSkill[i].listLevel[j].mp; // mp hao
                    skilltemplate.cooldown[j] = sKillCharResponeDto.listSkill[i].listLevel[j].coolDown; // cooldow thoi gian cho skill
                    skilltemplate.timebuffSkill[j] = sKillCharResponeDto.listSkill[i].listLevel[j].timeBuffSkill; /// thoi gian buff cua skill
                    skilltemplate.pcSubEffAppear[j] = sKillCharResponeDto.listSkill[i].listLevel[j].pcSubEffAppear; // ty le xuat hien hieu ung phu
                    skilltemplate.tExistSubEff[j] = sKillCharResponeDto.listSkill[i].listLevel[j].tExistSubEff; // thoi gian ton tai cua hieu ung phu
                    skilltemplate.usehp[j] = sKillCharResponeDto.listSkill[i].listLevel[j].useHp; // ty le tang % su dung binh hp
                    skilltemplate.usemp[j] = sKillCharResponeDto.listSkill[i].listLevel[j].useMp; // ty le tang % su dung binh mp
                    skilltemplate.ntargetlv[j] = sKillCharResponeDto.listSkill[i].listLevel[j].ntarget;
                    skilltemplate.rangelv[j] = sKillCharResponeDto.listSkill[i].listLevel[j].range; //pháº¡m vi áº£nh hÆ°á»Ÿng cá»§a skill
                    skilltemplate.dame[j] = sKillCharResponeDto.listSkill[i].listLevel[j].dam;
                }

                SkillTemplates.add(skilltemplate); // lÃ m giá»‘ng nhÆ° itemtemplate
            }
        }
    }
}