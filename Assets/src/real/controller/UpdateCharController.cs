﻿using src.lib;
using src.network;
using src.Objectgame;
using src.screen;

namespace src.real.controller
{
    public class UpdateCharController : IController
    {
        public void processMessage(Message msg)
        {
            //UPDATE_BLOOD = 0;//UPDATE_EXP = 1;//su dung tang exp/// UPDATE_HP = 2; //UPDATE_MP = 3;
            var idchar = msg.Reader().ReadShort();
            var typee = msg.Reader().ReadByte();
            var ccharupdate = GameScr.findCharInMap(idchar);
            if (ccharupdate == null) return;
            switch (typee)
            {
                case 0:
                    var sizeThucAn = msg.Reader().ReadByte();
                    GuiMain.vecItemOther.removeAllElements();
                    for (var i = 0; i < sizeThucAn; i++)
                    {
                        var idtemplate = msg.Reader().ReadShort();
                        var time = msg.Reader().ReadShort();
                        GuiMain.vecItemOther.add(new ItemThucAn((short) i,
                            0, idtemplate, time, 100));
                    }

                    break;
                case 1:
                    Char.myChar().cEXP = msg.Reader().ReadByte();
                    Char.myChar().totalTN = msg.Reader().ReadInt();
                    break;
                case 2: //hp
                    ccharupdate.cMaxHP = msg.Reader().ReadInt(); //maxhp
                    ccharupdate.cHP = msg.Reader().ReadInt(); //hp
                    var hpcong = msg.Reader().ReadInt(); //hp cong them
                    GameScr.startFlyText("+" + hpcong, ccharupdate.cx, ccharupdate.cy - 50, 0, -2, MFont.RED);
                    break;
                case 3: //hp
                    ccharupdate.cMaxMP = msg.Reader().ReadInt(); //maxhp
                    ccharupdate.cMP = msg.Reader().ReadInt(); //hp
                    var mpcong = msg.Reader().ReadInt(); //hp cong them
                    GameScr.startFlyText("+" + mpcong, ccharupdate.cx, ccharupdate.cy - 50, 0, -2, MFont.GREEN);
                    break;
            }
        }
    }
}