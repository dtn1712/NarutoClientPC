﻿using src.lib;
using src.network;
using src.utils;
using MessagePack;
using src.Objectgame;
using src.real.dto.character;
using src.screen;

namespace src.real.controller
{
    public class PickUpItemController : IController
    {
        public void processMessage(Message msg)
        {
            var pickItemMapData = IoUtils.ReadMessage(msg);
            var pickItemMapDto = MessagePackSerializer.Deserialize<PickUpItemResponseDTO>(pickItemMapData);
            var typeItemPick = pickItemMapDto.Type;
            var idItemPick = pickItemMapDto.IdItem;
            if (typeItemPick == -1)
            {
                for (var i = 0; i < GameScr.vItemMap.size(); i++)
                {
                    var itempick = (ItemMap) GameScr.vItemMap.elementAt(i);
                    if (itempick.itemMapID == idItemPick)
                    {
                        if (Char.myChar().itemFocus != null && Char.myChar().itemFocus.itemMapID == idItemPick)
                        {
                            Char.myChar().itemFocus = null;
                        }
                            
                        GameScr.vItemMap.removeElementAt(i);
                        break;
                    }
                }
                return;
            }
            if (Char.myChar().itemFocus != null && Char.myChar().itemFocus.itemMapID == idItemPick)
            {
                Char.myChar().itemFocus = null;
                Char.myChar().clearFocus(10);
            }
            
            var idPlayerPick = pickItemMapDto.Id;
            var c = GameScr.findCharInMap(idPlayerPick);
            if (c != null)
            {
                for (var i = 0; i < GameScr.vItemMap.size(); i++)
                {
                    var itempick = (ItemMap) GameScr.vItemMap.elementAt(i);
                    if (itempick.itemMapID == idItemPick)
                    {
                        itempick.setPoint(c.cx, c.cy);
                        if (c.charID == Char.myChar().charID)
                            itempick.isMe = true;
                    }
                }
            }
        }
    }
}