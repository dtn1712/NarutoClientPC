﻿using src.lib;
using src.network;
using System;
using MessagePack;
using src.Objectgame;
using src.real.dto.response;
using src.screen;
using src.utils;
using UnityEngine;
using Char = src.Objectgame.Char;

namespace src.real.controller
{
    public class ItemTemplateController : IController
    {
        public void processMessage(Message msg)
        {
            try
            {
                var itemTemplateResponseData = IoUtils.ReadMessage(msg);
                var itemTemplateResponseDto =
                    MessagePackSerializer.Deserialize<ItemTemplateResponeDTO>(itemTemplateResponseData);
                GameScr.currentCharViewInfo = Char.myChar();

                var nItemTemplate = itemTemplateResponseDto.size; // so luong template ID
                for (var i = 0; i < nItemTemplate; i++)
                {
                    var templateId = itemTemplateResponseDto.listItem[i].id; // id temp
                    var typeItem = itemTemplateResponseDto.listItem[i].type; // loai
                    var gender = itemTemplateResponseDto.listItem[i].gender; //  gioi tinh
                    var name = itemTemplateResponseDto.listItem[i].name; // ten 
                    var des = itemTemplateResponseDto.listItem[i].description; // mo ta
                    var level = itemTemplateResponseDto.listItem[i].level; // level
                    var iconId = itemTemplateResponseDto.listItem[i].icon; // idIcon
                    var isUptoUp = itemTemplateResponseDto.listItem[i].isUpToUp; // cÃ³ nÃ¢ng cáº¥p Ä‘c k 
                    var giaitem = itemTemplateResponseDto.listItem[i].price;
                    var clazz = itemTemplateResponseDto.listItem[i].idClass;
                    var quocgia = itemTemplateResponseDto.listItem[i].country;
                    var part = itemTemplateResponseDto.listItem[i]
                        .idPartBody; // part cÆ¡ báº£n cháº¯c cháº¯n tháº±ng nÃ o cÅ©ng pháº£i cÃ³
                    short partquan = -1, partdau = -1;
                    if (typeItem == Item.TYPE_AO)
                    {
                        partquan = itemTemplateResponseDto.listItem[i].idPartLeg;
                        partdau = itemTemplateResponseDto.listItem[i].idPartHead;
                    }
                    var typeSell = itemTemplateResponseDto.listItem[i].sellByGold;
                    ItemTemplate itt = null;
                    if (typeItem != Item.TYPE_AO)
                        itt = new ItemTemplate(templateId, typeItem, gender, name, des, level, iconId, part,
                            isUptoUp);
                    else
                        itt = new ItemTemplate(templateId, typeItem, gender, name, des, level, iconId, part,
                            partquan, partdau, isUptoUp);
                    itt.despaint = FontManager.GetInstance().tahoma_7_white.SplitFontArray(des, 140);
                    itt.gia = giaitem;
                    itt.clazz = clazz;
                    itt.quocgia = quocgia;
                    itt.typeSell = typeSell;
                    ItemTemplates.Add(itt);
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }
    }
}