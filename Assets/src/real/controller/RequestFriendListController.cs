﻿using MessagePack;
using src.network;
using src.Objectgame;
using src.real.dto.response.FriendDTO;
using src.utils;

namespace src.real.controller
{
    public class RequestFriendListController : IController
    {
        public void processMessage(Message msg)
        {
            var requestFriendListData = IoUtils.ReadMessage(msg);
            var friendListResponseDTO = MessagePackSerializer.Deserialize<FriendListResponseDTO>(requestFriendListData);
            Char.myChar().vFriend.removeAllElements();
            var sizeFriendList = friendListResponseDTO.size;
            for (var i = 0; i < sizeFriendList; i++)
            {
                var ch = new Char();
                ch.isOnline = friendListResponseDTO.listFriend[i].isOnline;
                if (ch.isOnline)
                    ch.charID = friendListResponseDTO.listFriend[i].id; // không online defaut là 0							
                ch.cName = friendListResponseDTO.listFriend[i].charName;
                ch.clevel = friendListResponseDTO.listFriend[i].charLevel;
                //ch.head = message.reader().readShort();
                Char.myChar().vFriend.addElement(ch);
            }
        }
    }
}