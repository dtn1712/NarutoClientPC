﻿using MessagePack;
using src.Gui;
using src.lib;
using src.main;
using src.network;
using src.Objectgame;
using src.real.dto.response;
using src.screen;
using src.utils;

namespace src.real.controller
{
    public class CharDeadController : IController
    {
        public void processMessage(Message msg)
        {
            var actorDeadResponseData = IoUtils.ReadMessage(msg);
            var actorDeadResponseDto = MessagePackSerializer.Deserialize<ActorDeadResponseDTO>(actorDeadResponseData);

            if (actorDeadResponseDto.ActorType == Constants.CAT_PLAYER)
            {
                var playerId = actorDeadResponseDto.Id;
                if (playerId == Char.myChar().charID)
                {
                    GameScr.GetInstance().guiMain.moveClose = true;
                    if (MenuIcon.isShowTab)
                        GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                    if (GameScr.isBag)
                        TabScreenNew.cmdClose.performAction();
                }
                var player = GameScr.findCharInMap(playerId);

                if (player == null) return;
                player.cHP = 0;
                player.statusMe = Char.A_DEAD;
            }
            else if (actorDeadResponseDto.ActorType == Constants.CAT_MONSTER)
            {
                var mobId = actorDeadResponseDto.Id;
                for (var i = 0; i < GameScr.vMob.size(); i++)
                {
                    var mob = (Mob) GameScr.vMob.elementAt(i);
                    if (mob.mobId != mobId) continue;
                    mob.status = Mob.MA_DEADFLY;
                    mob.StartDie();

                    if (Char.myChar().mobFocus != null && Char.myChar().mobFocus.mobId == mobId)
                        Char.myChar().mobFocus = null;
                    break;
                }
            }
        }
    }
}