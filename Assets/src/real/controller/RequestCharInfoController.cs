﻿using System;
using MessagePack;
using src.lib;
using src.network;
using src.real.dto.request;
using src.real.dto.response;
using src.screen;
using src.utils;
using UnityEngine;
using Char = src.Objectgame.Char;

namespace src.real.controller
{
    public class RequestCharInfoController : IController
    {
        public void processMessage(Message msg)
        {
            try
            {
                var charInfoResponseData = IoUtils.ReadMessage(msg);
                var charInfoResponseDto = MessagePackSerializer.Deserialize<CharInfoResponseDTO>(charInfoResponseData);
                var charId = charInfoResponseDto.id;
                var charMaxHp = charInfoResponseDto.maxHp;
                var charHp = charInfoResponseDto.hp;
                var clevel = charInfoResponseDto.level;
                var typeBePhongAn = charInfoResponseDto.isJunchuuriki;
                var ninjaLuuDai = charInfoResponseDto.isNinjaExile;
                var cIdClanGlobal = charInfoResponseDto.globalClan;
                var cIdClanLocal = charInfoResponseDto.localClan;
                for (var i = 0; i < GameScr.vCharInMap.size(); i++)
                {
                    var ch = (Char) GameScr.vCharInMap.elementAt(i);
                    if (ch.charID != charId) continue;
                    ch.cMaxHP = charMaxHp;
                    ch.cHP = charHp;
                        
                    if (ch.clevel > 0 && ch.clevel < clevel)
                    {
                        Music.play(Music.LENLV, 10);
                        ServerEffect.addServerEffect(22, ch.cx, ch.cy, 1);
                    }
                        
                    if (typeBePhongAn == 1)
                    {
                        ServerEffect.addServerEffect(40, ch, true);
                    }
                    else
                    {
                        ServerEffect.removeEffect(40);
                    }

                    ch.clevel = clevel;
                    if (ch.cMaxHP <= 0) ch.cMaxHP = ch.cHP <= 0 ? 1 : ch.cHP;
                   
                    ch.cIdClanGlobal = (int)cIdClanGlobal;
                    ch.cIdClanLocal = (int)cIdClanLocal;
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
            

        }
    }
}