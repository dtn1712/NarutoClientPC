﻿using MessagePack;
using src.network;
using src.Objectgame;
using src.real.dto.response.trade;
using src.utils;

namespace src.real.controller
{
    public class MoveItemTradeController : IController
    {
        public void processMessage(Message msg)
        {
            var moveItemTradeData = IoUtils.ReadMessage(msg);
            var moveItemTradeDto = MessagePackSerializer.Deserialize<TradeMoveItemResponseDTO>(moveItemTradeData);
            long charId = moveItemTradeDto.id;
            if (charId == Char.myChar().charID)
            {
                for (var i = 0; i < Char.myChar().ItemMyTrade.Length; i++)
                {
                    if (Char.myChar().ItemMyTrade[i] == null)
                    {
                        Char.myChar().ItemMyTrade[i] = new Item();
                        var idItem = moveItemTradeDto.idItem;
                        Char.myChar().ItemMyTrade[i].itemId = idItem;
                        var iditemTemplate = moveItemTradeDto.idTemplate;
                        //LogDebug.println(IdItem + " iditemTemplate " + iditemTemplate);
                        if (iditemTemplate != -1)
                            Char.myChar().ItemMyTrade[i].template = ItemTemplates.Get(iditemTemplate);

                        break;
                    }
                }
            }
            else
            {
                for (var i = 0; i < Char.myChar().partnerTrade.ItemParnerTrade.Length; i++)
                {
                    // add vao danh sach item partner
                    if (Char.myChar().partnerTrade.ItemParnerTrade[i] == null)
                    {
                        Char.myChar().partnerTrade.ItemParnerTrade[i] = new Item();
                        var idItem = moveItemTradeDto.idItem;
                        Char.myChar().partnerTrade.ItemParnerTrade[i].itemId = idItem;
                        var iditemTemplate = moveItemTradeDto.idTemplate;

                        if (iditemTemplate != -1)
                            Char.myChar().partnerTrade.ItemParnerTrade[i].template =
                                ItemTemplates.Get(iditemTemplate);

                        break;
                    }
                }
            }
        }
    }
}