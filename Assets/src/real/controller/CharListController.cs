﻿using src.lib;
using src.network;
using src.utils;
using MessagePack;
using src.main;
using src.real.dto.response;
using src.screen;

namespace src.real.controller
{
    public class CharListController : IController
    {
        public void processMessage(Message msg)
        {
            var charListData = IoUtils.ReadMessage(msg);
            var charListDto = MessagePackSerializer.Deserialize<CharListResponseDTO>(charListData);
                    
            LoginScr.isLoggingIn = false;
            SelectCharScr.GetInstance().InitSelectChar();
            GameCanvas.EndDlg();
            if (charListDto.Chars.Count > 0)
            {
                for (var i = 0; i < charListDto.Chars.Count; i++)
                {
                    var charBasicInfo = charListDto.Chars[i];
                           
                    SelectCharScr.GetInstance().charIDDB[i] = charBasicInfo.Id;
                    SelectCharScr.GetInstance().name[i] = charBasicInfo.CharName;
                    SelectCharScr.GetInstance().gender[i] = charBasicInfo.Gender;
                    SelectCharScr.GetInstance().type[i] = charBasicInfo.CharClass;
                    SelectCharScr.GetInstance().lv[i] = charBasicInfo.Level;
                    SelectCharScr.GetInstance().parthead[i] = charBasicInfo.HeadId;
                    SelectCharScr.GetInstance().partbody[i] = charBasicInfo.BodyId;
                    SelectCharScr.GetInstance().partleg[i] = charBasicInfo.LegId;
                }
                DownloadImageScreen.gI().switchToMe();
                GameCanvas.EndDlg();
            }
            else
            {
                DownloadImageScreen.gI().switchToMe(1);
                GameCanvas.EndDlg();
            }
        }
    }
}