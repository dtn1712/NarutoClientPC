﻿using src.network;
using System;
using MessagePack;
using src.real.dto.response;
using src.screen;
using src.utils;
using UnityEngine;

namespace src.real.controller
{
    public class RegionInfoController : IController
    {
        public void processMessage(Message msg)
        {
            try
            {
                var regionInfoResponseData = IoUtils.ReadMessage(msg);
                var regionInfoResponseDto = MessagePackSerializer.Deserialize<RegionInfoResponseDTO>(regionInfoResponseData);

                var size = regionInfoResponseDto.ListRegion.Count;
                
                KhuScreen.GetInstance().listKhu = new sbyte[size][];
                
                for (var i = 0; i < size; i++)
                {
                    KhuScreen.GetInstance().listKhu[i] = new sbyte[1];
                }

                for (var i = 0; i < size; i++)
                {
                    KhuScreen.GetInstance().listKhu[i][0] = regionInfoResponseDto.ListRegion[i];
                }

                KhuScreen.GetInstance().srclist.selectedItem = -1;
                KhuScreen.GetInstance().switchToMe();
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }
        
    }
}