﻿using src.Gui;
using src.lib;
using src.main;
using src.model;
using src.network;
using src.Objectgame;
using src.screen;
using UnityEngine;

namespace src.real.controller
{
    public class TradeItemController : IController
    {
        public void processMessage(Message message)
        {
            var typeTrade = message.Reader().ReadByte();
            if (typeTrade == Constants.INVITE_TRADE)
            {
                // moi giao dich 
                var charIDpartner = message.Reader().ReadShort();
                Char.myChar().partnerTrade = null;
                Char.myChar().partnerTrade = GameScr.findCharInMap(charIDpartner);
                GameCanvas.startYesNoDlg(message.Reader().ReadUtf(), GameScr.cmdAcceptTrade,
                    new Command(MResources.CANCEL, GameCanvas.instance, 8882, null));
            }
            else if (typeTrade == Constants.ACCEPT_INVITE_TRADE)
            {
                // dong y giao dich 
                var charIDpartner = message.Reader().ReadShort();
                if (Char.myChar().partnerTrade == null)
                    Char.myChar().partnerTrade = GameScr.findCharInMap(charIDpartner);

                GameScr.isBag = false;
                GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                GameCanvas.gameScr.guiMain.menuIcon.iconTrade.performAction();
                Service.GetInstance().RequestInventory();
            }
            else if (typeTrade == Constants.MOVE_ITEM_TRADE)
            {
                var typeItem = message.Reader().ReadByte(); // loại
                var typeItemmove = message.Reader().ReadByte(); // đưa xuống hay đưa lên 
                var charId = message.Reader().ReadShort();
                if (typeItemmove == 0)
                {
                    // add item
                    if (charId == Char.myChar().charID)
                    {
                        for (var i = 0; i < Char.myChar().ItemMyTrade.Length; i++)
                        {
                            if (Char.myChar().ItemMyTrade[i] == null)
                            {
                                Char.myChar().ItemMyTrade[i] = new Item();
                                var IdItem = message.Reader().ReadShort();
                                Char.myChar().ItemMyTrade[i].itemId = IdItem;
                                var iditemTemplate = message.Reader().ReadShort();
                                //LogDebug.println(IdItem + " iditemTemplate " + iditemTemplate);
                                if (iditemTemplate != -1)
                                    Char.myChar().ItemMyTrade[i].template = ItemTemplates.Get(iditemTemplate);

                                break;
                            }
                        }
                    }
                    else
                    {
                        for (var i = 0; i < Char.myChar().partnerTrade.ItemParnerTrade.Length; i++)
                        {
                            // add vÃ o danh sÃ¡ch item cá»§a parner
                            if (Char.myChar().partnerTrade.ItemParnerTrade[i] == null)
                            {
                                Char.myChar().partnerTrade.ItemParnerTrade[i] = new Item();
                                var IdItem = message.Reader().ReadShort();
                                Char.myChar().partnerTrade.ItemParnerTrade[i].itemId = IdItem;
                                var iditemTemplate = message.Reader().ReadShort();

                                if (iditemTemplate != -1)
                                    Char.myChar().partnerTrade.ItemParnerTrade[i].template =
                                        ItemTemplates.Get(iditemTemplate);

                                break;
                            }
                        }
                    }
                }
                else
                {
                    // remove item
                    if (Char.myChar().charID == charId)
                    {
                        //	LogDebug.println(getClass(), "remove Me item ");
                        var IdItem = message.Reader().ReadShort();
                        for (var i = 0; i < Char.myChar().ItemMyTrade.Length; i++)
                            if (Char.myChar().ItemMyTrade[i] != null)
                                if (Char.myChar().ItemMyTrade[i].itemId == IdItem)
                                {
                                    //	LogDebug.println(getClass(),i+ "remove item null");
                                    Char.myChar().ItemMyTrade[i] = null;
                                    GameCanvas.gameScr.guiMain.menuIcon.trade.indexSelect1 = -1;
                                    break;
                                }
                    }
                    else
                    {
                        var IdItem = message.Reader().ReadShort();
                        for (var i = 0; i < Char.myChar().partnerTrade.ItemParnerTrade.Length; i++)
                            if (Char.myChar().partnerTrade.ItemParnerTrade[i] != null)
                                if (Char.myChar().partnerTrade.ItemParnerTrade[i].itemId == IdItem)
                                    Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
                    }
                }
            }
            else if (typeTrade == Constants.CANCEL_TRADE)
            {
                var charidP = message.Reader().ReadShort();
                var charPar = GameScr.findCharInMap(charidP);
                GameCanvas.gameScr.guiMain.menuIcon.trade = null;
                GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                GameCanvas.StartOkDlg(charPar.cName + message.Reader().ReadUtf());
            }
            else if (typeTrade == Constants.LOCK_TRADE)
            {
                var charidP = message.Reader().ReadShort();
                if (Char.myChar().charID != charidP)
                {
                    GameCanvas.gameScr.guiMain.menuIcon.trade.block2 = true;
                }
                else
                {
                    GameCanvas.gameScr.guiMain.menuIcon.trade.block1 = true;
                }
            }
            else if (typeTrade == Constants.TRADE)
            {
                var charidP = message.Reader().ReadShort();
                var cTrade = GameScr.findCharInMap(charidP);
                GameCanvas.startYesNoDlg(cTrade.cName + message.Reader().ReadUtf(), TradeGui.cmdTradeEnd,
                    new Command(MResources.CANCEL, GameCanvas.instance, 8882, null));
                //GameScr.gI().tradeGui = null;
                for (var i = 0; i < Char.myChar().ItemMyTrade.Length; i++)
                {
                    Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
                    Char.myChar().ItemMyTrade[i] = null;
                }
            }
            else if (typeTrade == Constants.END_TRADE)
            {
                for (var i = 0; i < Char.myChar().ItemMyTrade.Length; i++)
                {
                    Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
                    Char.myChar().ItemMyTrade[i] = null;
                }
                Char.myChar().partnerTrade = null;
                GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                GameCanvas.gameScr.guiMain.menuIcon.trade = null;
            }
        }
    }
}