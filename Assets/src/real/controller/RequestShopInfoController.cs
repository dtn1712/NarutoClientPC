﻿using System;
using MessagePack;
using src.Gui;
using src.lib;
using src.network;
using src.real.dto.response;
using src.utils;
using UnityEngine;

namespace src.real.controller
{
    public class RequestShopInfoController : IController
    {
        public void processMessage(Message msg)
        {
            try
            {
                var requestShopData = IoUtils.ReadMessage(msg);
                var requestShopResponseDto = MessagePackSerializer.Deserialize<ShopInfoResponeDTO>(requestShopData);
               
                var menuId = requestShopResponseDto.IdMenu;
                int itemlistSize = requestShopResponseDto.Size;
                ShopMain.indexidmenu = menuId;
                ShopMain.idItemtemplate = new short[itemlistSize];
                for (var i = 0; i < itemlistSize; i++)
                {
                    var idTemplate = requestShopResponseDto.ListId[i];
                    ShopMain.idItemtemplate[i] = idTemplate;
                }
                ShopMain.getItemList(ShopMain.indexidmenu, ShopMain.idItemtemplate);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
          
        }
    }
}