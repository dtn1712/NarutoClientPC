﻿using MessagePack;
using src.network;
using src.lib;
using src.main;
using src.model;
using src.Objectgame;
using src.utils;
using src.real.dto.response.trade;
using src.screen;

namespace src.real.controller
{
    public class TradeInviteController : IController
    {
        public void processMessage(Message msg)
        {
            var inviteTradeData = IoUtils.ReadMessage(msg);
            var inviteTradeResponseDTO = MessagePackSerializer.Deserialize<InviteTradeResponseDTO>(inviteTradeData);
            var CharIDpartner = inviteTradeResponseDTO.id;
            Char.myChar().partnerTrade = null;
            Char.myChar().partnerTrade = GameScr.findCharInMap(CharIDpartner);
            GameCanvas.startYesNoDlg(inviteTradeResponseDTO.content, GameScr.cmdAcceptTrade,
                new Command(MResources.CANCEL, GameCanvas.instance, 8882, null));
        }
    }
}