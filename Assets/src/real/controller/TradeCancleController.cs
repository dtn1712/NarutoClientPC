﻿using MessagePack;
using src.main;
using src.network;
using src.utils;
using src.real.dto.response.trade;
using src.screen;

namespace src.real.controller
{
    public class TradeCancleController : IController
    {
        public void processMessage(Message msg)
        {
            var tradeCancleData = IoUtils.ReadMessage(msg);
            var tradeCancleResponseDto = MessagePackSerializer.Deserialize<CancelTradeResponseDTO>(tradeCancleData);
            var charidP = tradeCancleResponseDto.id;
            var charPar = GameScr.findCharInMap(charidP);
            GameCanvas.gameScr.guiMain.menuIcon.trade = null;
            GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
            GameCanvas.StartOkDlg(charPar.cName + tradeCancleResponseDto.content);
        }
        
    }
}