﻿using System;
using MessagePack;
using src.lib;
using src.network;
using src.Objectgame;
using src.real.dto.response;
using src.utils;
using UnityEngine;


namespace src.real.controller
{
    public class MapTemplateController : IController
    {
        public void processMessage(Message msg)
        {
            try
            {
                var mapTemplateResponeData = IoUtils.ReadMessage(msg);
                var mapTemplateResponeDto = MessagePackSerializer.Deserialize<MapTemplateResponseDTO>(mapTemplateResponeData);
                int sizeee = mapTemplateResponeDto.size;               
                for (var i = 0; i < sizeee; i++)
                {
                    var idmap = mapTemplateResponeDto.listMapTemplate[i].id;
                    var idtile = mapTemplateResponeDto.listMapTemplate[i].tileId;
                    var name = mapTemplateResponeDto.listMapTemplate[i].name;
                    TileMap.listNameAllMap.put(idmap + "", name);
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
           
        }
    }
}