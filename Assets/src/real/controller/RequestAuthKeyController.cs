﻿using System;
using System.Text;
using MessagePack;
using MessagePack.Resolvers;
using src.lib;
using src.main;
using src.model;
using src.network;
using src.real.dto.response;
using src.screen;
using src.utils;
using UnityEngine;

namespace src.real.controller
{
    public class RequestAuthKeyController : IController
    {
        public void processMessage(Message message)
        {
            try
            {

                var requestAuthKeyResponseData = IoUtils.ReadMessage(message);
                var requestAuthKeyResponseDto =
                    MessagePackSerializer.Deserialize<RequestAuthKeyResponseDTO>(requestAuthKeyResponseData);

                Session.GetInstance().Key = Converter.ConvertByteToSbyte(requestAuthKeyResponseDto.Key);
                Session.GetInstance().SessionId = requestAuthKeyResponseDto.SessionId;
                var data = Encoding.Default.GetBytes(requestAuthKeyResponseDto.SessionId);
                IoUtils.WriteMessage(Cmd.HANDSHAKE, data);

                if (LoginScr.GetInstance().GetAction() == LoginScr.LOGIN_ACTION)
                {
                    Service.GetInstance().DoLogin(LoginScr.GetInstance().GetUser(), LoginScr.GetInstance().GetPass(),
                        (byte) MGraphics.zoomLevel);
                }
                else if (LoginScr.GetInstance().GetAction() == LoginScr.REGISTER_ACTION)
                {
                    Service.GetInstance()
                        .DoRegister(LoginScr.GetInstance().GetUser(), LoginScr.GetInstance().GetPass());
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                LoginScr.GetInstance().Clear();
                GameCanvas.ResetToLoginScr(false);
            }
        }
    }
}