﻿using MessagePack;
using src.network;
using src.Objectgame;
using src.real.dto.response;
using src.screen;
using src.utils;

namespace src.real.controller
{
    public class MonsterMoveController : IController
    {
        public void processMessage(Message msg)
        {
            var monsterMoveData = IoUtils.ReadMessage(msg);
            var monsterMoveResponeDTO = MessagePackSerializer.Deserialize<MonsterPositionResponseDTO>(monsterMoveData);
            var idmobb = monsterMoveResponeDTO.id;
            var idmobx = monsterMoveResponeDTO.monX;
            var idmoby = monsterMoveResponeDTO.monY;
            var namemobb = monsterMoveResponeDTO.name;
            var hpmob = monsterMoveResponeDTO.hp;
            for (var i = 0; i < GameScr.vMob.size(); i++)
            {
                var mobb = (Mob) GameScr.vMob.elementAt(i);
                if (mobb.mobId == idmobb && mobb.status == Mob.MA_INHELL)
                {
                    mobb.injureThenDie = false;
                    mobb.status = Mob.MA_WALK;
                    mobb.x = idmobx;
                    mobb.y = idmoby - 10;
                    mobb.mobName = namemobb;
                    ServerEffect.addServerEffect(37, mobb.x, mobb.y - mobb.getH() / 4 - 10, 1);
                    mobb.hp = hpmob > mobb.maxHp ? mobb.maxHp : hpmob;
                }
            }
        }
        
    }
}