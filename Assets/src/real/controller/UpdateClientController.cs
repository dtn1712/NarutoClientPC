﻿using MessagePack;
using src.main;
using src.network;
using src.real.dto.response;
using src.utils;

namespace src.real.controller
{
    public class UpdateClientController : IController
    {
        public void processMessage(Message msg)
        {
            var errorVersionResponseData = IoUtils.ReadMessage(msg);
            var errorVersionResponseDto = MessagePackSerializer.Deserialize<ErrorVersionResponeDTO>(errorVersionResponseData);
            var link = errorVersionResponseDto.Link;
            GameCanvas.startOK("Bạn đang dùng phiên bản cũ. Vui lòng tải phiên bản mới !", GameCanvas.cTaiVersionMoi, link);
        }
    }
}