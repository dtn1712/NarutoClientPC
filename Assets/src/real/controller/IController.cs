﻿using src.network;

namespace src.real.controller
{
    public interface IController
    {
        void processMessage(Message message);
    }
}