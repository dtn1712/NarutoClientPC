﻿using src.lib;
using src.main;
using src.network;

namespace src.real.controller
{
    public class RequestImageController : IController
    {
        public void processMessage(Message msg)
        {
            var subImg = msg.Reader().ReadByte();
            var idimg = msg.Reader().ReadInt();
            if (subImg == 0)
            {
                var dataImg = Utils.ReadByteArray(msg);
                if (GameCanvas.currentScreen == DownloadImageScreen.gI())
                {
                    DownloadImageScreen.isOKNext = true;
                }
                var path = SmallImage.getPathImage(idimg) + "" + idimg;
                Rms.saveRMS(MSystem.getPathRMS(path), dataImg);
            }
            else if (subImg == 1)
            {
                int sizeImg = msg.Reader().ReadByte();
                for (var i = 0; i < sizeImg; i++)
                {
                    var lengread = msg.Reader().ReadInt();
                    var dataImg = Utils.ReadByteArray(msg, lengread);
                    var path = SmallImage.getPathImage(idimg) + "" + idimg + "/_" + (sizeImg == 2 ? (i == 0 ? 1 : 4) : i + 1);
                    Rms.saveRMS(MSystem.getPathRMS(path), dataImg);
                }
            }
        }   
    }
}