﻿using MessagePack;
using src.lib;
using src.main;
using src.model;
using src.network;
using src.Objectgame;
using src.real.dto.response.FriendDTO;
using src.screen;
using src.utils;

namespace src.real.controller
{
    public class FriendController : IController
    {
        public void processMessage(Message msg)
        {
            var friendResponseData = IoUtils.ReadMessage(msg);
            var friendResponeseDto = MessagePackSerializer.Deserialize<FriendResponseDTO>(friendResponseData);
            var typeFriend = msg.Reader().ReadByte();
            if (typeFriend == Friend.INVITE_ADD_FRIEND)
            {
                Char.myChar().idFriend = msg.Reader().ReadShort();
                var tempDebug = msg.Reader().ReadUtf();
                GameCanvas.startYesNoDlg(tempDebug, GameScr.GetInstance().cmdComfirmFriend,
                    new Command(MResources.CANCEL, GameCanvas.instance, 8882, null));
            }
            else if (typeFriend == Friend.ACCEPT_ADD_FRIEND)
            {
                var charIdFriend = msg.Reader().ReadShort();
                var c = GameScr.findCharInMap(charIdFriend);
                Char.myChar().vFriend.addElement(c);
            }
            else if (typeFriend == Friend.REQUEST_FRIEND_LIST)
            {
                Char.myChar().vFriend.removeAllElements();
                var sizeFriendList = msg.Reader().ReadByte();
                for (var i = 0; i < sizeFriendList; i++)
                {
                    var ch = new Char();
                    ch.isOnline = msg.Reader().ReadBool();
                    if (ch.isOnline)
                        ch.charID = msg.Reader().ReadShort(); // không online defaut là 0							
                    ch.CharidDB = msg.Reader().ReadShort();
                    ch.cName = msg.Reader().ReadUtf();
                    ch.clevel = msg.Reader().ReadShort();
                    //ch.head = message.reader().readShort();
                    Char.myChar().vFriend.addElement(ch);
                }
            }
            else if (typeFriend == Friend.UNFRIEND)
            {
                var charId = msg.Reader().ReadShort();
                var charUser = msg.Reader().ReadShort();
                //	System.out.println("Unfriend -----> "+CharID);
                for (var i = 0; i < Char.myChar().vFriend.size(); i++)
                {
                    var chardel = (Char) Char.myChar().vFriend.elementAt(i);
                    if (chardel.charID != 0)
                    {
                        // khi online 
                        if (chardel.charID == charId)
                            Char.myChar().vFriend.removeElementAt(i);
                    }
                    else
                    {
                        if (chardel.CharidDB == charUser)
                            Char.myChar().vFriend.removeElementAt(i);
                    }
                }
            }
        }
    }
}