﻿using src.network;
using src.lib;
using System;
using MessagePack;
using src.real.dto.response;
using src.screen;
using src.utils;
using UnityEngine;
using Char = src.Objectgame.Char;

namespace src.real.controller
{
    public class StatusAttackController : IController
    {
        public void processMessage(Message msg)
        {
            try
            {
                var statusAttackData = IoUtils.ReadMessage(msg);
                var statusAttackDto = MessagePackSerializer.Deserialize<StatusAttackResponseDTO>(statusAttackData);
                var idchar = statusAttackDto.Id;
                var typeAttack = statusAttackDto.Status;

                if (Char.myChar().charID == idchar)
                {
                    FlagScreen.time = MSystem.currentTimeMillis();
                }
                
                var ch = GameScr.findCharInMap(idchar);
                if (ch != null) ch.typePk = typeAttack;
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }
    }
}