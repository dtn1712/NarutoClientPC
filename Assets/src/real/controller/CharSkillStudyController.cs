﻿using System;
using src.lib;
using src.network;
using MessagePack;
using src.Gui;
using src.Objectgame;
using src.utils;
using src.real.dto.response;
using UnityEngine;

namespace src.real.controller
{
    public class CharSkillStudyController : IController
    {
        public void processMessage(Message msg)
        {
            try
            {
                var charSkillStudiedResponeData = IoUtils.ReadMessage(msg);
                var charSkillStudiedResponeDto = MessagePackSerializer.Deserialize<CharSkillStudyResponseDTO>(charSkillStudiedResponeData);
                var dsSkillDaHoc = charSkillStudiedResponeDto.size; // id skill da hoc
                for (var i = 0; i < dsSkillDaHoc; i++)
                {
                    var idskill = charSkillStudiedResponeDto.listSkill[i].idSkill; // idSkill dahoc
                    if (i == 0)
                    {
                        QuickSlot.idSkillCoBan = idskill;
                    }
                    var idtem = charSkillStudiedResponeDto.listSkill[i].idTemplate; //icon skill tron
                    var levelskill = charSkillStudiedResponeDto.listSkill[i].level; // level skill hien tai

                    if (TabSkill.skillIndex != null && idskill == TabSkill.skillIndex.id)
                    {
                        if (TabSkill.skillIndex.level == -1 && levelskill > -1)
                        {
                            TabSkill.skillIndex.level = levelskill;
                            TabSkill.isDaHoc = true;
                        }
                        TabSkill.skillIndex.level = levelskill;
                    }
                    var skill = (SkillTemplate) SkillTemplates.hSkilltemplate.get("" + idskill);

                    skill.level = levelskill;
                    if (skill.level >= skill.nlevelSkill)
                    {
                        skill.level = (short) (skill.nlevelSkill - 1);
                    }
                }

                QuickSlot.loadRmsQuickSlot();
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
            
        }  
    }
}