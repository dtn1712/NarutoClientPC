﻿using src.network;
using System;
using src.utils;
using MessagePack;
using src.main;
using src.Objectgame;
using src.real.dto.response;
using src.screen;
using UnityEngine;
using Char = src.Objectgame.Char;

namespace src.real.controller
{
    public class ChangeRegionController : IController
    {
        public void processMessage(Message msg)
        {
            try
            {
                var changeRegionResponseData = IoUtils.ReadMessage(msg);
                var changeRegionResponseDto = MessagePackSerializer.Deserialize<ChangeRegionResponseDTO>(changeRegionResponseData);
                
                TileMap.zoneID = changeRegionResponseDto.idRegion;
                Char.myChar().cx = changeRegionResponseDto.charX;
                Char.myChar().cy = changeRegionResponseDto.charY;

                for (var i = 0; i < GameScr.vMob.size(); i++)
                {
                    var dem = (Mob) GameScr.vMob.elementAt(i);
                    dem.ResetToDie();
                }
                GameScr.vItemMap.removeAllElements();
                GameScr.vCharInMap.removeAllElements();
                XinChoScreen.isChangeKhu = true;
                XinChoScreen.GetInstance().switchToMe();
                GameScr.vCharInMap.add(Char.myChar());
                Char.ischangingMap = false;
                Char.myChar().statusMe = Char.A_FALL;
                GameScr.loadCamera(true, Char.myChar().cx, Char.myChar().cy);
                GameCanvas.EndDlg();
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }
    }
}