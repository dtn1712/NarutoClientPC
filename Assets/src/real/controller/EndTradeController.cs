﻿using System.Diagnostics;
using src.main;
using src.network;
using src.Objectgame;

namespace src.real.controller
{
    public class EndTradeController : IController
    {
        public void processMessage(Message msg)
        {
            for (var i = 0; i < Char.myChar().ItemMyTrade.Length; i++)
            {
                Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
                Char.myChar().ItemMyTrade[i] = null;
            }
            Char.myChar().partnerTrade = null;
            GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
            GameCanvas.gameScr.guiMain.menuIcon.trade = null;
        }
    }
}