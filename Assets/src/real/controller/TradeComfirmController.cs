﻿using MessagePack;
using src.Gui;
using src.network;
using src.utils;
using src.real.dto.response.trade;
using src.lib;
using src.main;
using src.model;
using src.Objectgame;
using src.screen;

namespace src.real.controller
{
    public class TradeComfirmController : IController
    {
        public void processMessage(Message msg)
        {
            var tradeComfirmData = IoUtils.ReadMessage(msg);
            var tradeComfirmDTO = MessagePackSerializer.Deserialize<TradeAcceptResponseDTO>(tradeComfirmData);
            var charidP = tradeComfirmDTO.id;
            var cTrade = GameScr.findCharInMap(charidP);
            GameCanvas.startYesNoDlg(cTrade.cName + tradeComfirmDTO.content, TradeGui.cmdTradeEnd,
                new Command(MResources.CANCEL, GameCanvas.instance, 8882, null));
            for (var i = 0; i < Char.myChar().ItemMyTrade.Length; i++)
            {
                Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
                Char.myChar().ItemMyTrade[i] = null;
            }
        }               

    }
}