﻿using MessagePack;
using src.network;
using src.real.dto.response;
using src.real.dto.response.message;
using src.screen;
using src.utils;

namespace src.real.controller
{
    public class ServerNotificationController : IController
    {
        public void processMessage(Message msg)
        {
            var serverNotificationResponseData = IoUtils.ReadMessage(msg);
            var serverNotificationResponseDto = MessagePackSerializer.Deserialize<ServerNotificationResponseDTO>(serverNotificationResponseData);
            GameScr.listInfoServer.add(new InfoServer(InfoServer.CHATSERVER, serverNotificationResponseDto.Content));
        }
    }
}