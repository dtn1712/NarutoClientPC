﻿using MessagePack;
using src.network;
using src.lib;
using src.main;
using src.utils;
using src.real.dto.response.message;

namespace src.real.controller
{
    public class AlertTimeController : IController
    {
        public void processMessage(Message msg)
        {
            var alertTimeData = IoUtils.ReadMessage(msg);
            var alertTimeDto = MessagePackSerializer.Deserialize<ServerAlertTimeResponseDTO>(alertTimeData);
            GameCanvas.startDlgTime(alertTimeDto.info, new Command("Yes", GameCanvas.instance, 8882, null), alertTimeDto.time);
        }
        
    }
}