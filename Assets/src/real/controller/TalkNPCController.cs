﻿using MessagePack;
using src.lib;
using src.main;
using src.network;
using src.Objectgame;
using src.real.dto.response;
using src.utils;

namespace src.real.controller
{
    public class TalkNPCController : IController
    {
        public void processMessage(Message message)
        {
            var talkNpcData = IoUtils.ReadMessage(message);
            var talkNpcDto = MessagePackSerializer.Deserialize<TalkNPCResponseDTO>(talkNpcData);
            GameCanvas.menu.startAtNPC(new Vector(), 0, Char.myChar().npcFocus.npcId, Char.myChar().npcFocus,
                talkNpcDto.ContentChat);
        }
    }
}