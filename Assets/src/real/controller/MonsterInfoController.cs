﻿using MessagePack;
using src.lib;
using src.network;
using src.Objectgame;
using src.real.dto.response;
using src.screen;
using src.utils;

namespace src.real.controller
{
    public class MonsterInfoController : IController
    {
        public void processMessage(Message msg)
        {
            var monterInfoResponseData = IoUtils.ReadMessage(msg);
            var monterInfoResponseDto = MessagePackSerializer.Deserialize<MonsterInfoResponseDTO>(monterInfoResponseData);
            var idmob = monterInfoResponseDto.id;
            var namemob = monterInfoResponseDto.name;
            var maxhp = monterInfoResponseDto.maxHp;
            var hp = monterInfoResponseDto.hp;
            for (var i = 0; i < GameScr.vMob.size(); i++)
            {
                var modInMap = (Mob) GameScr.vMob.elementAt(i);
                if (modInMap == null || modInMap.mobId != idmob) continue;
                modInMap.mobName = namemob;
                modInMap.maxHp = maxhp;
                modInMap.hp = hp;
            }
        }
    }
}