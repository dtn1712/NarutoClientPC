﻿using src.network;
using src.utils;
using MessagePack;
using src.real.dto;
using src.lib;
using System;
using src.Objectgame;
using src.real.dto.character;
using src.screen;
using UnityEngine;
using Char = src.Objectgame.Char;

namespace src.real.controller
{
    public class CharWearingController : IController
    {
        public void processMessage(Message msg)
        {
            try
            {
                var c = new Char();
                var listCharWearingData = IoUtils.ReadMessage(msg);
                var charWearingDto = MessagePackSerializer.Deserialize<CharWearingResponseDTO>(listCharWearingData);
                if (Char.myChar().charID != charWearingDto.id)
                {
                    var isAdded = false;
                    for (var i = 0; i < GameScr.vCharInMap.size(); i++)
                    {
                        var dem = (Char) GameScr.vCharInMap.elementAt(i);
                        if (dem != null && dem.charID == charWearingDto.id)
                        {
                            isAdded = true;
                            c = dem;
                        }
                    }
                    if (!isAdded)
                    {
                        c = new Char();
                        c.charID = charWearingDto.id;
                    }
                    readcharInmap(c, charWearingDto);
                    if (!isAdded)
                    {
                        GameScr.vCharInMap.addElement(c);
                    }
                }
                else
                {
                    Char.myChar().arrItemBody = new Item[charWearingDto.length];

                    for (var i = 0; i < Char.myChar().arrItemBody.Length; i++)
                    {
                        var idtemplate = charWearingDto.listItem[i];
                        if (idtemplate != -1)
                        {
                            var template = ItemTemplates.Get(idtemplate);

                            var indexUI = Item.getPosWearingItem(template.type);

                            if (indexUI >= Char.myChar().arrItemBody.Length)
                                continue;

                            Char.myChar().arrItemBody[indexUI] = new Item();

                            Char.myChar().arrItemBody[indexUI].indexUI = indexUI;

                            Char.myChar().arrItemBody[indexUI].typeUI = Item.UI_BODY;
                            Char.myChar().arrItemBody[indexUI].template = template;
                            Char.myChar().arrItemBody[indexUI].isLock = true;
                            if (template.type == Item.TYPE_AO)
                            {
                                Char.myChar().body = GameScr.currentCharViewInfo.arrItemBody[indexUI]
                                    .template.part;
                                Char.myChar().leg = GameScr.currentCharViewInfo.arrItemBody[indexUI]
                                    .template.partquan;
                                Char.myChar().head = GameScr.currentCharViewInfo.arrItemBody[indexUI]
                                    .template.partdau;
                            }
                            else if (template.type == Item.TYPE_NON)
                            {
                                Char.myChar().head = GameScr.currentCharViewInfo.arrItemBody[indexUI]
                                    .template.part;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }


        private void readcharInmap(Char c, CharWearingResponseDTO charWearingResponseDTO)
        {
            // add player trong map

            try
            {
                c.arrItemBody = new Item[13];

                for (var i = 0; i < c.arrItemBody.Length; i++)
                {
                    for (int j = 0; j < charWearingResponseDTO.listItem.Count; j++)
                    {
                        var idtemlate = charWearingResponseDTO.listItem[j];
                        if (idtemlate != -1)
                        {
                            var template = ItemTemplates.Get(idtemlate);
                            var indexUI = Item.getPosWearingItem(template.type);


                            c.arrItemBody[indexUI] = new Item();

                            c.arrItemBody[indexUI].indexUI = indexUI;

                            c.arrItemBody[indexUI].typeUI = Item.UI_BODY;
                            c.arrItemBody[indexUI].template = template;
                            c.arrItemBody[indexUI].isLock = true;
                            if (template.type == Item.TYPE_AO)
                            {
                                c.body = c.arrItemBody[indexUI].template.part;
                                c.leg = c.arrItemBody[indexUI].template.partquan;
                                c.head = c.arrItemBody[indexUI].template.partdau;
                            }
                            else if (template.type == Item.TYPE_NON)
                            {
                                c.head = c.arrItemBody[indexUI].template.part;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }
    }
}