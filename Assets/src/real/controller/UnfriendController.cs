﻿using MessagePack;
using src.network;
using src.Objectgame;
using src.real.dto.response.FriendDTO;
using src.utils;

namespace src.real.controller
{
    public class UnfriendController : IController
    {
        public void processMessage(Message msg)
        {
            var unfriendData = IoUtils.ReadMessage(msg);
            var removeFriendResponse = MessagePackSerializer.Deserialize<RemoveFriendRequestDTO>(unfriendData);
            var CharID = removeFriendResponse.id;
//            var charUser = removeFriendResponse.;
            //	System.out.println("Unfriend -----> "+CharID);
            for (var i = 0; i < Char.myChar().vFriend.size(); i++)
            {
                var chardel = (Char) Char.myChar().vFriend.elementAt(i);
                if (chardel.charID != 0)
                {
                    // khi online 
                    if (chardel.charID == CharID)
                        Char.myChar().vFriend.removeElementAt(i);
                }
                else
                {
                    if (chardel.CharidDB == CharID)
                        Char.myChar().vFriend.removeElementAt(i);
                }
            }
        }
    }
}