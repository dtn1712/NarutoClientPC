﻿using src.lib;
using src.network;
using src.utils;
using MessagePack;
using src.model;
using src.Objectgame;
using src.real.dto.response;
using src.screen;

namespace src.real.controller
{
    public class ChatController : IController
    {
        public void processMessage(Message msg)
        {

            var chatResponseData = IoUtils.ReadMessage(msg);
            var chatResponseDto = MessagePackSerializer.Deserialize<ChatResponseDTO>(chatResponseData);
           
            var typeChat = chatResponseDto.type;
            if (typeChat == ChatType.CHAT_MAP)
            {
                // chat map
                var useriD = chatResponseDto.playerSendId;
                var text = chatResponseDto.text;
                var c = Char.myChar().charID == useriD ? Char.myChar() : GameScr.findCharInMap(useriD);

                if (c == null)
                {
                    return;
                }
                ChatPopup.addChatPopup(text, 200, c);
                ChatManager.gI().addChat(MResources.PUBLICCHAT[0], c.cName, text);
            }
            else if (typeChat == ChatType.CHAT_WORLD)
            {
                ChatWorld.ChatWorldd(chatResponseDto);
            }
            else if (typeChat == ChatType.CHAT_FRIEND)
            {
                ChatPrivate.ChatFriend(chatResponseDto);
            }
        }
    }
}