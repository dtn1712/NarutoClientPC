﻿using MessagePack;
using src.main;
using src.network;
using src.real.dto.response.message;
using src.utils;

namespace src.real.controller
{
    public class ServerTopRightMsgController : IController
    {
        public void processMessage(Message msg)
        {
            var serverTopRightMsgData = IoUtils.ReadMessage(msg);
            var serverTopRightDTO = MessagePackSerializer.Deserialize<ServerTopRightResponseDTO>(serverTopRightMsgData);
            GameCanvas.StartDglThongBao(serverTopRightDTO.info);
        }
    }
}