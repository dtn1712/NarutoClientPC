﻿using MessagePack;
using src.network;
using src.Objectgame;
using src.real.dto.response;
using src.screen;
using src.utils;

namespace src.real.controller
{
    public class CharReviveController : IController
    {
        public void processMessage(Message msg)
        {
            var charReviveResposeData = IoUtils.ReadMessage(msg);
            var charReviveResposeDto = MessagePackSerializer.Deserialize<CharReviveResponeDTO>(charReviveResposeData);
            
            var idcharhs = charReviveResposeDto.id;
            var chs = GameScr.findCharInMap(idcharhs);

            if (chs == null) return;
            
            ServerEffect.addServerEffect(34, chs.cx, chs.cy, 3);
            chs.statusMe = Char.A_FALL;
            chs.cHP = charReviveResposeDto.hp;
            chs.cMP = charReviveResposeDto.mp;
        }   
    }
}