﻿using MessagePack;
using src.main;
using src.network;
using src.Objectgame;
using src.real.dto.response.trade;
using src.utils;

namespace src.real.controller
{
    public class TradeClockController : IController
    {
        public void processMessage(Message msg)
        {
            var tradeClockData = IoUtils.ReadMessage(msg);
            var tradeClockResponseDTO = MessagePackSerializer.Deserialize<TradeLockResponseDTO>(tradeClockData);
            var charidP = tradeClockResponseDTO.id;
            if (Char.myChar().charID != charidP)
            {
                GameCanvas.gameScr.guiMain.menuIcon.trade.block2 = true;
            }
            else
            {
                GameCanvas.gameScr.guiMain.menuIcon.trade.block1 = true;
            }
        }
    }
}