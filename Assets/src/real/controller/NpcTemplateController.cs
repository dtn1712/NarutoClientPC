﻿using System;
using System.Linq;
using src.lib;
using src.network;
using src.utils;
using MessagePack;
using src.Objectgame;
using src.real.dto.response;
using src.real.dto;
using UnityEngine;

namespace src.real.controller
{
    public class NpcTemplateController : IController
    {
        public void processMessage(Message msg)
        {
            try
            {
                var npcTemplateData = IoUtils.ReadMessage(msg);
                var npcTemplateResponseDto = MessagePackSerializer.Deserialize<NpcTemplateResponseDTO>(npcTemplateData);
                for (var i = 0; i < npcTemplateResponseDto.NpcTemplates.Count; i++)
                {
                    var npcTemplate =
                        DTOMapper.mapNpcTemplateDTOToModel(npcTemplateResponseDto.NpcTemplates.ElementAt(i));
                    Npc.arrNpcTemplate.put(npcTemplate.npcTemplateId + "", npcTemplate);
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
            
        }
    }
}