﻿using MessagePack;
using src.main;
using src.network;
using src.real.dto.response;
using src.utils;

namespace src.real.controller
{
    public class MakePaymentController : IController
    {
        public void processMessage(Message msg)
        {
            var makePaymentData = IoUtils.ReadMessage(msg);
            var makePaymentDto = MessagePackSerializer.Deserialize<PaymentResponseDTO>(makePaymentData);
            GameCanvas.StartDglThongBao(makePaymentDto.success
                ? "Bạn đã nạp thẻ thành công !!!"
                : makePaymentDto.errorMessage);
        }
    }
}