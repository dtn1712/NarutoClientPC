﻿using src.Gui;
using src.main;
using src.network;

namespace src.real.controller
{
    public class UpgradeItemController : IController
    {
        public void processMessage(Message msg)
        {
            var type_dapdo = msg.Reader().ReadByte();
            switch (type_dapdo)
            {
                case TabNangCap.SUB_UPDATE_INFO_UPGRADE
                : // ----------------------> Server trả về 3, xử lý sau khi nâng cấp, đã pass

                    int leng_item = msg.Reader().ReadByte();
                    var listiditem = new short[leng_item];
                    for (var i = 0; i < leng_item; i++)
                    {
                        var tempIdrece = msg.Reader().ReadShort();
                        listiditem[i] = tempIdrece;
                    }
                    var tab_nc = (TabNangCap) GameCanvas.AllInfo.VecTabScreen.elementAt(4);
                    if (tab_nc != null)
                        tab_nc.updateListItemNangCap(listiditem);
                    break;
                case TabNangCap.SUB_UPGRADE_FAIL: // -----------------------------> Server trả về 5
                    TabNangCap.isFail = true;
                    break;
                case TabNangCap.SUB_UPGRADE_SUCCESS: // -------------------------------> Server trả về 4
                    TabNangCap.isSuccess = true;
                    break;
            }
        }
    }
}