﻿using MessagePack;
using src.lib;
using src.network;
using src.real.dto.response;
using src.screen;
using src.utils;

namespace src.real.controller
{
    public class DropItemController : IController
    {
        public void processMessage(Message msg)
        {
            var dropItemData = IoUtils.ReadMessage(msg);
            var dropItemResponseDto = MessagePackSerializer.Deserialize<DropItemResponseDTO>(dropItemData);
            var typeDrop = dropItemResponseDto.typeItem;
            var typeItemRotRa = dropItemResponseDto.typeActorLive; //1 Ä‘Ã¡nh quÃ¡i rá»›t ra, 0 char vá»©t ra.
            var idMonterDie = dropItemResponseDto.actorLiveId;
            var idd = dropItemResponseDto.id;
            var idTemplatee = dropItemResponseDto.idTemplate;
            ItemMap itemDrop = null;
            if (typeItemRotRa == 1)
            {
                var mons = GameScr.findMobInMap(idMonterDie);
                if (mons != null)
                {
                    itemDrop = new ItemMap(idd, idTemplatee, mons.x, mons.yFirst + 10);
                    itemDrop.type = typeDrop;
                }
                GameScr.vItemMap.addElement(itemDrop);
                GameScr.vNhatItemMap.addElement(itemDrop);
            }
            else
            {
                var ch = GameScr.findCharInMap(idMonterDie);
                if (ch != null)
                {
                    itemDrop = new ItemMap(idd, idTemplatee, ch.cx, ch.cy);
                    itemDrop.type = typeDrop;
                    GameScr.vItemMap.addElement(itemDrop);
                    GameScr.vNhatItemMap.addElement(itemDrop);
                }
            }
            
        }
    }
}