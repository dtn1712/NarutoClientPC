﻿using src.Gui;
using src.network;

namespace src.real.controller
{
    public class ShowPaymentController : IController
    {
        public void processMessage(Message msg)
        {
            TabBag.isShowPayment = true;
        }
    }
}