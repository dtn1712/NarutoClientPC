﻿using System;
using MessagePack;
using src.Gui;
using src.lib;
using src.network;
using src.real.dto.response;
using src.utils;
using UnityEngine;

namespace src.real.controller
{
    public class RequestMenuShopController : IController
    {
        public void processMessage(Message msg)
        {
            try
            {
                var menuShopResponseData = IoUtils.ReadMessage(msg);
                var menuShopResponseDto = MessagePackSerializer.Deserialize<MenuShopResponseDTO>(menuShopResponseData);
                var menuShopsize = menuShopResponseDto.size; // soluong menu shop
                
                ShopMain.idMenu = new int[menuShopsize];
                ShopMain.nameMenu = new string[menuShopsize];
                ShopMain.dis = new string[menuShopsize];
                ShopMain.cmdShop = new Command[menuShopsize];
                // ShopMain.idItemtemplate = new int[menuShopsize];
                for (var i = 0; i < menuShopsize; i++)
                {
                    var idmenu = menuShopResponseDto.listKey[i].key;
                    ShopMain.idMenu[i] = idmenu;
                    var menuname = menuShopResponseDto.listKey[i].nameMenu;
                    ShopMain.nameMenu[i] = menuname;
                    var dis = menuShopResponseDto.listKey[i].descMenu;
                    ShopMain.dis[i] = dis;
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
           
        }
    }
}