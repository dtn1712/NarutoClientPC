﻿using System;
using src.lib;
using src.network;
using src.utils;
using MessagePack;
using src.real.dto;
using src.screen;
using UnityEngine;
using Char = src.Objectgame.Char;

namespace src.real.controller
{
    public class CharInfoController : IController
    {
        public void processMessage(Message msg)
        {
            try
            {
                var dataCharInfo = IoUtils.ReadMessage(msg);
                var mainCharInfoDto = MessagePackSerializer.Deserialize<MainCharInfoDTO>(dataCharInfo);

                Char.myChar().charID = (int) mainCharInfoDto.Id;
                Char.myChar().cName = mainCharInfoDto.CharName;
                Char.myChar().cClass = mainCharInfoDto.IdClass;
                Char.myChar().cgender = mainCharInfoDto.Gender;
                var clevel2 = mainCharInfoDto.Level;
                if (Char.myChar().clevel > 0 && Char.myChar().clevel < clevel2)
                {
                    Music.play(Music.LENLV, 10);
                    ServerEffect.addServerEffect(22, Char.myChar().cx, Char.myChar().cy, 1);
                }
                
                Char.myChar().clevel = clevel2;
                Char.myChar().cEXP = mainCharInfoDto.Percent;
                Char.myChar().cHP = mainCharInfoDto.Hp;
                Char.myChar().cMaxHP = mainCharInfoDto.MaxHp;
                Char.myChar().cMP = mainCharInfoDto.Mp;
                Char.myChar().cMaxMP = mainCharInfoDto.MaxMp;
                Char.myChar().diemTN = null;
                Char.myChar().diemTN = new int[8];
                Char.myChar().diemTN[0] = mainCharInfoDto.KhaiMon;
                Char.myChar().diemTN[1] = mainCharInfoDto.HuuMon;
                Char.myChar().diemTN[2] = mainCharInfoDto.SinhMon;
                Char.myChar().diemTN[3] = mainCharInfoDto.DoMon;
                Char.myChar().diemTN[4] = mainCharInfoDto.CanhMon;
                Char.myChar().diemTN[5] = mainCharInfoDto.ThuongMon;
                Char.myChar().diemTN[6] = mainCharInfoDto.KinhMon;
                Char.myChar().diemTN[7] = mainCharInfoDto.TuMon;
                Char.myChar().totalTN = mainCharInfoDto.BasePoint;

                //gui ve thong tin sub
                var sizeSub = (short) mainCharInfoDto.SizeAttribute;
                Char.myChar().subTn = new int[sizeSub];
                for (var i = 0; i < sizeSub; i++)
                {
                    var charAttribute = mainCharInfoDto.CharAttribute[i];
                    var indexMon = (sbyte) charAttribute.Id;
                    var valueSub = charAttribute.Value;
                }
                if (GameScr.findCharInMap((short) Char.myChar().charID) == null)
                    GameScr.vCharInMap.addElement(Char.myChar());

                Char.myChar().cCountry = mainCharInfoDto.Country;
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }
    }
}