﻿using MessagePack;
using src.main;
using src.network;
using src.Objectgame;
using src.real.dto.response.trade;
using src.utils;

namespace src.real.controller
{
    [MessagePackObject]
    public class TakeOffItemTradeController : IController
    {
        public void processMessage(Message msg)
        {
            var takeOffItemTradeData = IoUtils.ReadMessage(msg);
            var takeOffItemTradeDTO = MessagePackSerializer.Deserialize<TradeTakeOffResponseDTO>(takeOffItemTradeData);
            long charId = takeOffItemTradeDTO.id;
            if (Char.myChar().charID == charId)
            {
                var itemId = takeOffItemTradeDTO.itemId;
                for (var i = 0; i < Char.myChar().ItemMyTrade.Length; i++)
                    if (Char.myChar().ItemMyTrade[i] != null)
                        if (Char.myChar().ItemMyTrade[i].itemId == itemId)
                        {
                            Char.myChar().ItemMyTrade[i] = null;
                            GameCanvas.gameScr.guiMain.menuIcon.trade.indexSelect1 = -1;
                            break;
                        }
            }
            else
            {
                var itemId = takeOffItemTradeDTO.itemId;
                for (var i = 0; i < Char.myChar().partnerTrade.ItemParnerTrade.Length; i++)
                    if (Char.myChar().partnerTrade.ItemParnerTrade[i] != null)
                        if (Char.myChar().partnerTrade.ItemParnerTrade[i].itemId == itemId)
                            Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
            }
        }
    }
}