﻿using MessagePack;
using src.main;
using src.network;
using src.Objectgame;
using src.real.dto.response.trade;
using src.screen;
using src.utils;

namespace src.real.controller
{
    public class AcceptInviteTradeController : IController
    {
        public void processMessage(Message msg)
        {
            var acceptInviteTradeData = IoUtils.ReadMessage(msg);
            var acceptInviteTradeDataResponseDto =
                MessagePackSerializer.Deserialize<TradeAcceptInviteResponseDTO>(acceptInviteTradeData);
            var charIDpartner = acceptInviteTradeDataResponseDto.id;
            if (Char.myChar().partnerTrade == null)
                Char.myChar().partnerTrade = GameScr.findCharInMap(charIDpartner);

            GameScr.isBag = false;
            GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
            GameCanvas.gameScr.guiMain.menuIcon.iconTrade.performAction();
            Service.GetInstance().RequestInventory();
        }   
    }
}