﻿using src.lib;
using src.network;
using System;
using MessagePack;
using src.main;
using src.model;
using src.Objectgame;
using src.real.dto.response;
using src.screen;
using src.utils;
using UnityEngine;
using Char = src.Objectgame.Char;

namespace src.real.controller
{
    public class ChangeMapController : IController
    {
        public void processMessage(Message msg)
        {
            try
            {
                var mapInfoResponseData = IoUtils.ReadMessage(msg);
                var mapInfoResponseDto = MessagePackSerializer.Deserialize<MapInfoResponseDTO>(mapInfoResponseData);
                GameScr.GetInstance().LoadData();
                GameScr.GetInstance().guiMain.moveClose = false;
                GameScr.GetInstance().guiMain.menuIcon.indexpICon = 0;
                MenuIcon.isShowTab = false;
                GameScr.vNpc.removeAllElements();
                TileMap.GetInstance().vGo.removeAllElements();
                GameScr.vMob.removeAllElements();
                GameScr.vItemMap.removeAllElements();
                GameScr.vNhatItemMap.removeAllElements();
                Char.ischangingMap = true;
                GameScr.vCharInMap.removeAllElements();
                GameScr.vCharInMap.addElement(Char.myChar());
                TileMap.mapID = mapInfoResponseDto.id; // read changemap
                Char.myChar().mapId = TileMap.mapID;
                Effect.vEffect2.removeAllElements();
                TileMap.GetInstance().mapName = (string) TileMap.listNameAllMap.get(TileMap.mapID + "");
                TileMap.zoneID = mapInfoResponseDto.region; // read 
                var vsize = mapInfoResponseDto.nLine; // read
                for (var i = 0; i < vsize; i++)
                {
                    TileMap.GetInstance().vGo.addElement(new Waypoint(mapInfoResponseDto.listLineMap[i].mapTemplateId,
                        mapInfoResponseDto.listLineMap[i].minX, mapInfoResponseDto.listLineMap[i].minY,
                        mapInfoResponseDto.listLineMap[i].maxX, mapInfoResponseDto.listLineMap[i].maxY));
                } 

                Char.myChar().cx = mapInfoResponseDto.charX; // read x
                Char.myChar().cy = mapInfoResponseDto.charY; // read y
                Char.myChar().statusMe = Char.A_FALL;
                TileMap.tileID = mapInfoResponseDto.mapTempId; // read
                var nTile = mapInfoResponseDto.listMapHtData.Count; // read

                TileMap.GetInstance().tileIndex = new int[nTile][][];
                TileMap.GetInstance().tileType = new int[nTile][];
                try
                {
                    for (var i = 0; i < nTile; i++)
                    {
                        var nTypeSize = mapInfoResponseDto.listMapHtData[i].size;
                        TileMap.GetInstance().tileType[i] = new int[nTypeSize];
                        TileMap.GetInstance().tileIndex[i] = new int[nTypeSize][];

                        for (var a = 0; a < nTypeSize; a++)
                        {
                            TileMap.GetInstance().tileType[i][a] = mapInfoResponseDto.listMapHtData[i].listDataKeySet[a].keyType; // read
                            var sizeIndex = mapInfoResponseDto.listMapHtData[i].listDataKeySet[a].length; // read
                            TileMap.GetInstance().tileIndex[i][a] = new int[sizeIndex];
                            for (var b = 0; b < sizeIndex; b++)
                            {
                                TileMap.GetInstance().tileIndex[i][a][b] =
                                    mapInfoResponseDto.listMapHtData[i].listDataKeySet[a].data[b]; // read
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                }

                TileMap.GetInstance().loadimgTile(TileMap.tileID); // load hinh tile
                TileMap.GetInstance().loadMapfile(); // load file map
                TileMap.GetInstance().LoadMap(); // load va cham 
                FallingLeafEffect.Load();
                GameScr.loadMapItem(); // load toan bo file data object
                GameScr.loadMapTable(TileMap.mapID); // object trong 1 map filedata
                TileMap.vItemBg.removeAllElements();
                BgItem.imgPathLoad.clear();
                Mob.imgMob.clear();
                var idObjMap = new int [TileMap.vCurrItem.size()];
                for (var i = 0; i < TileMap.vCurrItem.size(); i++)
                {
                    var biMap = (BgItem) TileMap.vCurrItem.elementAt(i);
                    if (biMap != null)
                        idObjMap[i] = biMap.idImage;
                }
                var sizeMod = mapInfoResponseDto.monsterSize;
                Mob.arrMobTemplate = new MobTemplate[sizeMod];
                try
                {
                    for (var i = 0; i < Mob.arrMobTemplate.Length; i++)
                    {
                        Mob.arrMobTemplate[i] = new MobTemplate();
                        var idModtemp = mapInfoResponseDto.listMonster[i].id; // read
                        var idloadmob = mapInfoResponseDto.listMonster[i].idTemplate; // read
                        Mob.arrMobTemplate[i].idloadimage = idloadmob;
                        var mob = new Mob(idModtemp, mapInfoResponseDto.listMonster[i].hp,
                            mapInfoResponseDto.listMonster[i].level,
                            mapInfoResponseDto.listMonster[i].maxHp,
                            mapInfoResponseDto.listMonster[i].monX, mapInfoResponseDto.listMonster[i].monY
                            , mapInfoResponseDto.listMonster[i].minX,
                            mapInfoResponseDto.listMonster[i].minY, mapInfoResponseDto.listMonster[i].maxX,
                            mapInfoResponseDto.listMonster[i].maxY);
                        var isBoss = mapInfoResponseDto.listMonster[i].isBoss; // read
                        mob.isBoss = isBoss;
                        if (isBoss)
                        {
                            var sizeSmall = mapInfoResponseDto.listMonster[i].infoImageLength; // read
                            var dataSmall = Converter.ConvertByteToSbyte(mapInfoResponseDto.listMonster[i].infoImage);
                            var sizeFrame = mapInfoResponseDto.listMonster[i].infoFrameLength; // read
                            var dataFrame = Converter.ConvertByteToSbyte(mapInfoResponseDto.listMonster[i].infoFrame);
                            mob.typeMob = Mob.TYPE_MOB_TOOL;
                            Mob.arrMobTemplate[i].data = new EffectData(); // doc file data quai trong tool
                            var iss = new DataInputStream(dataSmall);

                            Mob.arrMobTemplate[i].data.readData222(iss);
                            var is2 = new DataInputStream(dataFrame);

                            Mob.arrMobTemplate[i].data.readhd(is2);
                        }

                        mob.idloadimage = idloadmob;
                        GameScr.vMob.addElement(mob);
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError("Bug Monster ChangeMapController ----> " + e.ToString() + " " + msg.Command);
                }

                try
                {
                    int sizeItemMap = mapInfoResponseDto.itemMapSize; // read

                    for (var i = 0; i < sizeItemMap; i++)
                    {
                        var iditemm = mapInfoResponseDto.listItem[i].id; // read
                        var idtemitem = mapInfoResponseDto.listItem[i].idTemplate; // read
                        int xitem = mapInfoResponseDto.listItem[i].itemX; // read
                        int yitem = mapInfoResponseDto.listItem[i].itemY; // read 
                        var itemDropmap = new ItemMap(iditemm, idtemitem, xitem, yitem);
         					
                        GameScr.vItemMap.addElement(itemDropmap);
                        GameScr.vNhatItemMap.addElement(itemDropmap);
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                }

                Service.GetInstance().RequestInventory();
                if (GameCanvas.currentScreen != GameCanvas.gameScr)
                {
                    GameCanvas.gameScr = new GameScr();
                }

                DownloadImageScreen.gI().switchToMe(SmallImage.ID_ADD_MAPOJECT, idObjMap);
                
                Char.ischangingMap = false;
                Char.myChar().statusMe = Char.A_FALL;
                GameScr.loadCamera(true, Char.myChar().cx, Char.myChar().cy);
                GameCanvas.EndDlg();

                Music.play(CRes.random(1, 4), 0.3f);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }
    }
}