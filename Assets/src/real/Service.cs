using System;
using System.Collections.Generic;
using MessagePack;
using src.Gui;
using src.lib;
using src.main;
using src.model;
using src.network;
using src.network.impl.reliable;
using src.Objectgame;
using src.real.dto.request;
using src.screen;
using src.utils;
using UnityEngine;
using Char = src.Objectgame.Char;

namespace src.real
{
    public class Service
    {
        private static readonly Service Instance = new Service();
        
        private Service(){}

        public static Service GetInstance()
        {
            return Instance;
        }

        public void DoLogin(string user, string pass, byte zoomlevel)
        {
            IoUtils.WriteMessage(Cmd.LOGIN, MessagePackSerializer.Serialize(new LoginRequestDTO
            {
                username = user,
                password = pass,
                zoomLevel = zoomlevel,
                clientVersion = GameMidlet.VERSION
            }));
        }

        public void CreateChar(sbyte typeChar, sbyte country, sbyte gender, string charName)
        {
            // tạo char
            IoUtils.WriteMessage(Cmd.CREATE_CHAR, MessagePackSerializer.Serialize(new CreateCharRequestDTO
            {
                country = country,
                charClass = typeChar,
                gender = gender,
                charName = charName
            }));
        }

        public void SelectChar(long charIdDb)
        {
            IoUtils.WriteMessage(Cmd.SELECT_CHAR, MessagePackSerializer.Serialize(new SelectCharRequestDTO
            {
                charId = charIdDb
            }));
        }

        public void CharMove()
        {
            var dx = Char.myChar().cx - Char.myChar().cxSend;
            var dy = Char.myChar().cy - Char.myChar().cySend;
            var isGuilen = CRes.abs(dx) > 5 || CRes.abs(dy) > 5 || Char.ischangingMap;
            if (!isGuilen)
            {
                return;
            }
            
            try
            {
                if (GameScr.isSendMove)
                    GameScr.isSendMove = false;

                Char.myChar().cxSend = Char.myChar().cx;
                Char.myChar().cySend = Char.myChar().cy;
                IoUtils.WriteMessage(Cmd.MOVE, MessagePackSerializer.Serialize(new CharMoveRequestDTO
                {
                    PosX = (short) Char.myChar().cx,
                    PosY = (short) Char.myChar().cy
                    
                }));
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        public void RequestImage(int id)
        {
            // yeu cau hinh tu server
            IoUtils.WriteMessage(Cmd.REQUEST_IMAGE, MessagePackSerializer.Serialize(new ImageRequest
            {
                imageId = id
            }));
        }

        // server tự biết nó đứng đâu mà chuyển đến map nào
        public void RequestChangeMap()
        {
            IoUtils.WriteMessage(Cmd.CHANGE_MAP);
        }

        // yeu cau request Inventory
        public void RequestInventory()
        {
            IoUtils.WriteMessage(Cmd.GET_ITEM_INVENTORY);
        }

        public void ChatMap(string text)
        {
            // yeu cau chat trong map
            IoUtils.WriteMessage(Cmd.CHAT, MessagePackSerializer.Serialize(new ChatRequestDTO
            {
                type = ChatType.CHAT_MAP,
                text = text
            }));
        }

        public void ChatGlobal(string text)
        {
            // yeu cau chat kenh the gioi
            IoUtils.WriteMessage(Cmd.CHAT, MessagePackSerializer.Serialize(new ChatRequestDTO
            {
                type = ChatType.CHAT_WORLD,
                text = text
            }));
        }
        
        public void ChatPrivate(long playerChatId, string text)
        {
            IoUtils.WriteMessage(Cmd.CHAT, MessagePackSerializer.Serialize(new ChatRequestDTO
            {
                type = ChatType.CHAT_FRIEND,
                playerChatId = playerChatId,
                text = text
            }));
        }

        public void RequestMonsterInfo(short monsterId)
        {
            // yeu cau thong tin quai khi focus
            IoUtils.WriteMessage(Cmd.MONSTER_INFO, MessagePackSerializer.Serialize(new MonsterInfoRequestDTO
            {
                MonsterId = monsterId
            }));
        }

        //Đang stuck send DTO MsgPack
        public void sendPlayerAttack(Vector vMob, Vector vChar, int type, int idskill, bool isMob)
        {
            // 0 player
            // 1 monter
        
            GameScr.isSendMove = true;
            var targetIds = new List<long>();
            if (isMob)
            {
                for (var i = 0; i < vMob.size(); i++)
                {
                    var b = (Mob) vMob.elementAt(i);
                    if (b != null)
                        targetIds.Add(b.mobId);
                }
            }
            else
            {
                for (var i = 0; i < vChar.size(); i++)
                {
                    var b = (Char) vChar.elementAt(i);
                    if (b != null)
                        targetIds.Add(b.charID);
                }
            }

            IoUtils.WriteMessage(Cmd.ATTACK, MessagePackSerializer.Serialize(new AttackRequestDTO
            {
                targetType = (sbyte) type,
                skillAttackId =  (sbyte) idskill,
                targetIds = targetIds
            }));
        }

        public void itemPick(sbyte type, short idItem)
        {
            // yêu cầu nhặt item
            IoUtils.WriteMessage(Cmd.PICK_UP_ITEM, MessagePackSerializer.Serialize(new PickUpItemRequestDTO
            {
                type = type,
                idItem = idItem
            }));
        }


        public void giveUpItem(sbyte index)
        {
            IoUtils.WriteMessage(Cmd.GIVEUP_ITEM, MessagePackSerializer.Serialize(new GiveUpItemRequestDTO
            {
                index = index
            }));
        }

        public void useItem(sbyte index)
        {
            IoUtils.WriteMessage(Cmd.USE_ITEM, MessagePackSerializer.Serialize(new UseItemRequestDTO
            {
                index = index
            }));
        }

        public void inviteParty(short charID, sbyte type)
        {
            IoUtils.WriteMessage(Cmd.PARTY, MessagePackSerializer.Serialize(new PartyRequestDTO
            {
                type = type,
                charId = charID
            }));
        }

        public void AcceptParty(sbyte type, short charID)
        {
            IoUtils.WriteMessage(Cmd.PARTY, MessagePackSerializer.Serialize(new PartyRequestDTO
            {
                type = type,
                charId = charID
            }));
        }

        public void leaveParty(sbyte type, short charID)
        {
            IoUtils.WriteMessage(Cmd.PARTY, MessagePackSerializer.Serialize(new PartyRequestDTO
            {
                type = type,
                charId = charID
            }));
        }

        public void kickPlayeLeaveParty(sbyte type, short charID)
        {
            IoUtils.WriteMessage(Cmd.PARTY, MessagePackSerializer.Serialize(new PartyRequestDTO
            {
                type = type,
                charId = charID
            }));
        }

        public void removeParty(sbyte type)
        {
            IoUtils.WriteMessage(Cmd.PARTY, MessagePackSerializer.Serialize(new PartyRequestDTO
            {
                type = type,
                charId = (short) Char.myChar().charID
            }));
        }

        public void requestJoinParty(sbyte type, short CharID)
        {
            // yeu cau vao party
            IoUtils.WriteMessage(Cmd.PARTY, MessagePackSerializer.Serialize(new PartyRequestDTO
            {
                type = type,
                charId = CharID
            }));
        }


        public void InviteAddFriend(long charId)
        {
            IoUtils.WriteMessage(Cmd.FRIEND, MessagePackSerializer.Serialize(new FriendRequestDTO
            {
                Type = Friend.INVITE_ADD_FRIEND,
                FriendId = charId
            }));
        }

        public void AcceptFriend(long charId)
        {
            IoUtils.WriteMessage(Cmd.FRIEND, MessagePackSerializer.Serialize(new FriendRequestDTO
            {
                Type = Friend.ACCEPT_ADD_FRIEND,
                FriendId = charId
            }));
        }

        public void RequestFriendList(long charId)
        {
            IoUtils.WriteMessage(Cmd.FRIEND, MessagePackSerializer.Serialize(new FriendRequestDTO
            {
                Type = Friend.REQUEST_FRIEND_LIST,
                FriendId = charId
            }));
        }

        public void DeleteFriend(long charId)
        {
            IoUtils.WriteMessage(Cmd.FRIEND, MessagePackSerializer.Serialize(new FriendRequestDTO
            {
                Type = Friend.UNFRIEND,
                FriendId = charId
            }));
        }

        

        public void MoveMoney(long amount, long charId)
        {
            IoUtils.WriteMessage(Cmd.TRADE_ITEM, MessagePackSerializer.Serialize(new TradeItemRequestDTO
            {
                TradeRequestType = Constants.MOVE_MONEY,
                PlayerJoinId = charId,
                Xu = amount
            }));
        }

        public void InviteTrade(long charId)
        {
            // mời trao đổi
            IoUtils.WriteMessage(Cmd.TRADE_ITEM, MessagePackSerializer.Serialize(new TradeItemRequestDTO
            {
                TradeRequestType = Constants.INVITE_TRADE,
                PlayerJoinId = charId
            }));
        }

        public void AcceptTrade(long charId)
        {
            // đồng ý trao đổi 
            IoUtils.WriteMessage(Cmd.TRADE_ITEM, MessagePackSerializer.Serialize(new TradeItemRequestDTO
            {
                TradeRequestType = Constants.ACCEPT_INVITE_TRADE,
                PlayerJoinId = charId
            }));
        }

        public void MoveItemTrade(long charId, sbyte moveItemType, short iditem)
        {
            // chuyển item trao đổi
            IoUtils.WriteMessage(Cmd.TRADE_ITEM, MessagePackSerializer.Serialize(new TradeItemRequestDTO
            {
                TradeRequestType = Constants.MOVE_ITEM_TRADE,
                PlayerJoinId = charId,
                MoveItemType = moveItemType,
                ItemId = iditem
            }));
        }

        public void CancelTrade(long charId)
        {
            // hủy trao đổi 
            IoUtils.WriteMessage(Cmd.TRADE_ITEM, MessagePackSerializer.Serialize(new TradeItemRequestDTO
            {
                TradeRequestType = Constants.CANCEL_TRADE,
                PlayerJoinId = charId
            }));
        }

        public void ConfirmTrade(long charId)
        {
            // lock
            IoUtils.WriteMessage(Cmd.TRADE_ITEM, MessagePackSerializer.Serialize(new TradeItemRequestDTO
            {
                TradeRequestType = Constants.LOCK_TRADE,
                PlayerJoinId = charId
            }));
        }

        public void DoTrade(long charId)
        {
            // giao dịch 
            IoUtils.WriteMessage(Cmd.TRADE_ITEM, MessagePackSerializer.Serialize(new TradeItemRequestDTO
            {
                TradeRequestType = Constants.TRADE,
                PlayerJoinId = charId
            }));
        }
        
        public void EndTrade(long charId)
        {
            // giao dịch 
            IoUtils.WriteMessage(Cmd.TRADE_ITEM, MessagePackSerializer.Serialize(new TradeItemRequestDTO
            {
                TradeRequestType = Constants.END_TRADE,
                PlayerJoinId = charId
            }));
        }

        public void RequestShopInfo(int idMenu)
        {
            // thong tin shop
            IoUtils.WriteMessage(Cmd.REQUEST_SHOP_INFO, MessagePackSerializer.Serialize(new ShopInfoRequestDTO
            {
                idMenu = (sbyte) idMenu
            }));
        }

        public void BuyItemShop(sbyte idmenu, short idItem)
        {
            // thong tin shop
            IoUtils.WriteMessage(Cmd.BUY_ITEM_FROM_SHOP, MessagePackSerializer.Serialize(new BuyItemRequestDTO
            {
                menuId = idmenu,
                itemIndex = idItem
            }));
        }

        public void RequestMenuShop()
        {
            IoUtils.WriteMessage(Cmd.REQUEST_MENU_SHOP);
        }

        public void Quest(sbyte type, string questType)
        {
            IoUtils.WriteMessage(Cmd.QUEST, MessagePackSerializer.Serialize(new QuestRequestDTO
            {
                actionType = type,
                questType = questType
            }));
        }

        public void PlayerOut()
        {
            IoUtils.WriteMessage(Cmd.PLAYER_OUT);
        }

        public void StudySkill(sbyte idSkill)
        {
            IoUtils.WriteMessage(Cmd.CHAR_SKILL_STUDY, MessagePackSerializer.Serialize(new CharSkillStudyRequestDTO
            {
                Type = Skill.STUDY_SKILL,
                IdSkill = idSkill
            }));
        }
        
        public void ImproveSkill(sbyte idSkill)
        {
            IoUtils.WriteMessage(Cmd.CHAR_SKILL_STUDY, MessagePackSerializer.Serialize(new CharSkillStudyRequestDTO
            {
                Type = Skill.IMPROVE_SKILL,
                IdSkill = idSkill
            }));
        }

        public void ReviveChar()
        {
            IoUtils.WriteMessage(Cmd.CHAR_REVIVE);
        }
        
        public void goHome()
        {
            IoUtils.WriteMessage(Cmd.COME_HOME);

        }

        public void DoRegister(string username, string password)
        {
            IoUtils.WriteMessage(Cmd.REGISTER, MessagePackSerializer.Serialize(new RegisterRequestDTO
            {
                Username  = username,
                Password = password,
                ClientVersion = GameMidlet.VERSION
            }));
        }

        public void RequestPlayerInfo(short idChar)
        {
            IoUtils.WriteMessage(Cmd.BASIC_CHAR_INFO, MessagePackSerializer.Serialize(new BasicCharInfoRequestDTO
            {
                CharId = idChar
            }));
        }

        public void RequestRegionInfo()
        {
            IoUtils.WriteMessage(Cmd.REGION_INFO);
        }

        public void RequestChangeRegion(sbyte regionId)
        {
            IoUtils.WriteMessage(Cmd.CHANGE_REGION, MessagePackSerializer.Serialize(new ChangeRegionRequestDTO
            {
                regionId = regionId
            }));
        }

        // số 1 chưa thống nhất vơi server là đặt tên gì
        public void RequestAddBasePoint(sbyte index, short value)
        {
            IoUtils.WriteMessage(Cmd.ADD_BASE_POINT, MessagePackSerializer.Serialize(new AddBasePointRequestDTO
            {
                type = 1,
                index = index,
                value = value
            }));
        }

        public void ThachDau(sbyte index, short idChar)
        {
            IoUtils.WriteMessage(Cmd.COMPETED_ATTACK, MessagePackSerializer.Serialize(new CompetedAttackRequestDTO
            {
                Type = index,
                PlayerInvitedId = idChar
            }));
        }

        public void ChangeFlag(sbyte idPk)
        {
            IoUtils.WriteMessage(Cmd.STATUS_ATTACK, MessagePackSerializer.Serialize(new StatusAttackRequestDTO
            {
                IdPk = idPk
            }));
        }

        public void SellItem(sbyte index)
        {
            IoUtils.WriteMessage(Cmd.SELL_ITEM, MessagePackSerializer.Serialize(new SellItemRequestDTO
            {
                index = index
            }));
        }

        public void TakeOffItem(sbyte itemIndex)
        {
            IoUtils.WriteMessage(Cmd.TAKE_OFF, MessagePackSerializer.Serialize(new TakeOffItemRequestDTO
            {
                ItemIndex = itemIndex
            }));
        }

        public void MenuNpc(long idNpc, sbyte idMenu)
        {
            IoUtils.WriteMessage(Cmd.MENU_NPC, MessagePackSerializer.Serialize(new MenuNpcRequestDTO
            {
                IdNpc = idNpc,
                IdMenu = idMenu
            }));
        }

        //Nâng cấp item
        public void NangCap_NangCap()
        {
            IoUtils.WriteMessage(Cmd.UPGRADE_ITEM, MessagePackSerializer.Serialize(new UpgradeItemRequestDTO
            {
                subUpgrade = TabNangCap.SUB_UPGRADE
            }));
        }

        public void NangCap_Dualen(sbyte indexItem)
        {
            IoUtils.WriteMessage(Cmd.UPGRADE_ITEM, MessagePackSerializer.Serialize(new UpgradeItemRequestDTO
            {
                subTakeOn = TabNangCap.SUB_TAKE_ON,
                indexItem = indexItem
            }));
        }

        public void NangCap_ThaoXuong(sbyte indexItem)
        {
            IoUtils.WriteMessage(Cmd.UPGRADE_ITEM, MessagePackSerializer.Serialize(new UpgradeItemRequestDTO
            {
                subTakeOff = TabNangCap.SUB_TAKE_OFF,
                indexItem = indexItem
            }));
        }
        
        public void MakePayment(string code, string seri, sbyte typeThe)
        {
            IoUtils.WriteMessage(Cmd.MAKE_PAYMENT, MessagePackSerializer.Serialize(new PaymentRequestDTO
            {
                Code = code,
                Seri = seri,
                TypeThe = typeThe
            }));
        }

        public void GetPricingInfo()
        {
            IoUtils.WriteMessage(Cmd.GET_PRICING);
        }
        

        public void Clan_getInfo()
        {
            IoUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
            {
                requestClan = ClanScreen.REQUEST_CLAN
            }));
        }

        public void Clan_createClanName(string name)
        {
            IoUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
            {
                requestName = ClanScreen.REQUETS_NAME,
                name = name
            }));
        }

        public void Clan_MoiBang(short idChar)
        {
            IoUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
            {
                inviteClan = ClanScreen.INVITA_CLAN,
                idChar = idChar
            }));
        }

        public void Clan_MoiBang_GLOBAL(short idChar)
        {
            Debug.Log("TOI MOI: " + ClanScreen.INVITE_CLAN_GLOBAL + " / idChar: " + idChar);
            Debug.Log("CharFocus IDDB: " + Char.myChar().charFocus.cIdDB + " | idGlobal: " +
                      Char.myChar().charFocus.cIdClanGlobal + " |idLocal: " + Char.myChar().charFocus.cIdClanLocal);
            IoUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
            {
                inviteClanGolbal = ClanScreen.INVITE_CLAN_GLOBAL,
                idChar = idChar
            }));
        }

        public void Clan_KichKhoiBang(long idChar)
        {
            IoUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
            {
                kick = (sbyte) ClanScreen.gI().KICK_CLAN,
                idCharKicked = ClanScreen.idCharClan
            }));
        }

        public void Clan_XoaBang()
        {
            IoUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
            {
                deleteClan = ClanScreen.DELETE_CLAN
            }));
        }

        public void Clan_RoiBang()
        {
            IoUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
            {
                leave = (sbyte) ClanScreen.gI().LEAVE_CLAN
            }));
        }

        public void Clan_GetlistUser_local()
        {
            IoUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
            {
                getListLocal = ClanScreen.GET_LIST_LOCAL
            }));
        }

        public void Clan_GetlistUser_gobal()
        {
            IoUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
            {
                getListGolbal = ClanScreen.GET_LIST_GLOBAL
            }));
        }

        public void Clan_GetlistClan_gobal()
        {
            IoUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
            {
                getListGolbal = ClanScreen.GET_LISTCLAN_GOLBAL
            }));
        }

        public void Clan_GetlistClan_local()
        {
            IoUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
            {
                getListLocal = ClanScreen.GET_LISTCLAN_LOCAL
            }));
        }

        

//        public void ReplyDialogFromServer(int cmd)
//        {
//            try
//            {
//                var m = new Message(Cmd.SERVER_DIALOG);
//                m.Writer().writeByte(cmd);
//                if (cmd == Cmd.CLAN)
//                {
//                    m.Writer().writeByte(ReliableMessageExecutor.cmdClanAvtive);
//                    if (ReliableMessageExecutor.cmdClanAvtive == Clan.KICKED)
//                    {
//                        m.Writer().writeByte(Clan.typeKick);
//                        m.Writer().writeShort((short) ClanScreen.idCharClan);
//                    }
//                    else if (ReliableMessageExecutor.cmdClanAvtive == Clan.LEAVE)
//                    {
//                        m.Writer().writeByte(Clan.typeLeave);
//                        ClanScreen.gI().ListUserGlobal.removeAllElements();
//                        ClanScreen.gI().clanGolbalName = "";
//                    }
//                    else if (ReliableMessageExecutor.cmdClanAvtive == Clan.INVITE)
//                    {
//                        m.Writer().writeByte(ReliableMessageExecutor.typeClanGL);
//                        m.Writer().writeLong(Char.myChar().cIdDB);
//                        m.Writer().writeShort(Clan.idClan);
//                    }
//                }
//                Session.GetInstance().SendMessage(m);
//            }
//            catch (Exception e)
//            {
//                Debug.LogError(e);
//            }
//        }
    }
}