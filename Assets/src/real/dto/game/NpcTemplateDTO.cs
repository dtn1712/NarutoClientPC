﻿using MessagePack;

namespace src.real.dto.game
{
    [MessagePackObject]
    public class NpcTemplateDTO
    {
        [Key("npcTemplateId")]
        public int NpcTemplateId { get; set; }
        
        [Key("name")]
        public string Name { get; set; }
        
        [Key("headId")]
        public int HeadId { get; set; }
        
        [Key("bodyId")]
        public int BodyId { get; set; }
        
        [Key("legId")]
        public int LegId { get; set; }
        
        [Key("avatar")]
        public short Avatar { get; set; }
    }
}