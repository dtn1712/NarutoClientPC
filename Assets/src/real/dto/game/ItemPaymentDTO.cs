﻿using MessagePack;

namespace src.real.dto.game
{
    [MessagePackObject]
    public class ItemPaymentDTO
    {
        [Key("gameMoneyAmount")]
        public int gameMoneyAmount { get; set; }
        
        [Key("gameMoneyUnit")]
        public string gameMoneyUnit { get; set; }
        
        [Key("realMoneyAmount")]
        public int realMoneyAmount { get; set; }
        
        [Key("realMoneyUnit")]
        public string realMoneyUnit { get; set; }
    }
}