﻿using MessagePack;

namespace src.real.dto.game
{
    [MessagePackObject]
    public class MenuKeyDTO
    {
        [Key("key")]
        public byte key { get; set; }

        [Key("nameMenu")]
        public string nameMenu { get; set; }

        [Key("descMenu")]
        public string descMenu { get; set; }
    }
}