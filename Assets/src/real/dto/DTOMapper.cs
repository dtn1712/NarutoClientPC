﻿using System.Linq;
using src.Objectgame;
using src.Objectgame.quest;
using src.real.dto.game;
using src.real.dto.quest;

namespace src.real.dto
{
    public static class DTOMapper
    {
        public static QuestTemplate mapQuestTemplateDTOToModel(QuestTemplateDTO questTemplateDto)
        {
            var requirements = questTemplateDto.Requirements.Select((t, i) => mapQuestRequirementDTOToModel(questTemplateDto.Requirements.ElementAt(i))).ToList();
            var questTemplate = new QuestTemplate
            {
                Id = questTemplateDto.Id,
                Content = questTemplateDto.Content,
                Name = questTemplateDto.Name,
                Lv = questTemplateDto.Lv,
                IdNpcReceive = questTemplateDto.IdNpcReceive,
                IdNpcResolve = questTemplateDto.IdNpcResolve,
                SupportContent = questTemplateDto.SupportContent,
                ResolveContent = questTemplateDto.ResolveContent,
                ShortContent = questTemplateDto.ShortContent,
                InfoFinish = questTemplateDto.InfoFinish,
                RemindContent = questTemplateDto.RemindContent,
                Exp = questTemplateDto.Exp,
                Gold = questTemplateDto.Gold,
                Xu = questTemplateDto.Xu,
                Requirements = requirements
            };
            return questTemplate;
        }

        public static QuestRequirement mapQuestRequirementDTOToModel(QuestRequirementDTO questRequirementDto)
        {
            return new QuestRequirement
            {
                Id = questRequirementDto.Id,
                QuestTemplateId = questRequirementDto.QuestTemplateId,
                Type = questRequirementDto.Type,
                Key = questRequirementDto.Key,
                Value = questRequirementDto.Value,
                Description = questRequirementDto.Description
            };
        }

        public static NpcTemplate mapNpcTemplateDTOToModel(NpcTemplateDTO npcTemplateDto)
        {
            var npcTemplate = new NpcTemplate
            {
                npcTemplateId = npcTemplateDto.NpcTemplateId,
                name = npcTemplateDto.Name,
                headId = npcTemplateDto.HeadId,
                bodyId = npcTemplateDto.BodyId,
                legId = npcTemplateDto.LegId,
                idavatar = npcTemplateDto.Avatar
            };
            
            if (npcTemplate.npcTemplateId != Npc.idXaPhu && npcTemplate.headId == -1 && npcTemplate.bodyId == -1 &&
                npcTemplate.legId == -1)
            {
                npcTemplate.typeKhu = -1;
            }

            return npcTemplate;
        }
        
    }
}