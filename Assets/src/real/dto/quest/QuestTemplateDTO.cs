﻿using System.Collections.Generic;
using MessagePack;

namespace src.real.dto.quest
{
    [MessagePackObject]
    public class QuestTemplateDTO
    {
        [Key("id")]
        public int Id { get; set; }

        [Key("lv")]
        public short Lv { get; set; }

        [Key("idNpcReceive")]
        public short IdNpcReceive { get; set; }

        [Key("idNpcResolve")]
        public short IdNpcResolve { get; set; }

        [Key("name")]
        public string Name { get; set; }

        [Key("content")]
        public string Content { get; set; }

        [Key("supportContent")]
        public string SupportContent { get; set; }

        [Key("resolveContent")]
        public string ResolveContent { get; set; }

        [Key("shortContent")]
        public string ShortContent { get; set; }

        [Key("infoFinish")]
        public string InfoFinish { get; set; }

        [Key("remindContent")]
        public string RemindContent { get; set; }

        [Key("exp")]
        public int Exp { get; set; }

        [Key("gold")]
        public int Gold { get; set; }

        [Key("xu")]
        public int Xu { get; set; }

        [Key("requirements")]
        public List<QuestRequirementDTO> Requirements { get; set; }
    }
}