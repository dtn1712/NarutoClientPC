﻿using System.Collections.Generic;
using MessagePack;

namespace src.real.dto.quest
{
    [MessagePackObject]
    public class CharQuestDTO
    {
        [Key("id")]
        public int Id { get; set; }

        [Key("charId")]
        public long CharId { get; set; }

        [Key("questTemplate")]
        public QuestTemplateDTO QuestTemplate { get; set; }

        [Key("startedAt")]
        public string StartedAt { get; set; }

        [Key("completedAt")]
        public string CompletedAt { get; set; }

        [Key("status")]
        public string Status { get; set; }

        [Key("progresses")]
        public List<QuestProgressDTO> Progresses { get; set; }
    }
}