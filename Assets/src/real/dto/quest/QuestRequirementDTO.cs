﻿using MessagePack;

namespace src.real.dto.quest
{
    [MessagePackObject]
    public class QuestRequirementDTO
    {
        [Key("id")]
        public int Id { get; set; }

        [Key("questTemplateId")]
        public int QuestTemplateId { get; set; }

        [Key("type")]
        public string Type { get; set; }

        [Key("key")]
        public string Key { get; set; }

        [Key("value")]
        public string Value { get; set; }

        [Key("description")]
        public string Description { get; set; }
    }
}