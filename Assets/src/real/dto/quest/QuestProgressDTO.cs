﻿using MessagePack;

namespace src.real.dto.quest
{
    [MessagePackObject]
    public class QuestProgressDTO
    {
        [Key("id")]
        public int Id { get; set; }

        [Key("currentProgress")]
        public string CurrentProgress { get; set; }

        [Key("requirement")]
        public QuestRequirementDTO Requirement { get; set; }

        [Key("status")]
        public string Status { get; set; }
    }
}