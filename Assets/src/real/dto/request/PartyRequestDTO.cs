﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class PartyRequestDTO
    {
        [Key("type")]
        public sbyte type { get; set; }

        [Key("charid")]
        public short charId { get; set; }
    }
}