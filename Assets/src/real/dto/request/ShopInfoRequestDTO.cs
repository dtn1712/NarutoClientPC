﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class ShopInfoRequestDTO
    {
        [Key("idMenu")]
        public sbyte idMenu { get; set; }
    }
}