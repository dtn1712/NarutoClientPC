﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class CompetedAttackRequestDTO
    {
        [Key("type")]
        public sbyte Type { get; set; }

        [Key("playerInvitedId")]
        public long PlayerInvitedId { get; set; }
    }
}