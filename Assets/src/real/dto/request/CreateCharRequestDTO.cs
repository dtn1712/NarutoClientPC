﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class CreateCharRequestDTO
    {
        [Key("country")]
        public sbyte country { get; set; }

        [Key("charClass")]
        public sbyte charClass { get; set; }

        [Key("gender")]
        public sbyte gender { get; set; }

        [Key("charName")]
        public string charName { get; set; }
    }
}