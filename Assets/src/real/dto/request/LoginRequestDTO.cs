﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class LoginRequestDTO
    {
        [Key("username")]
        public string username { get; set; }

        [Key("password")]
        public string password { get; set; }

        [Key("zoomLevel")]
        public byte zoomLevel { get; set; }

        [Key("clientVersion")]
        public string clientVersion { get; set; }
    }
}