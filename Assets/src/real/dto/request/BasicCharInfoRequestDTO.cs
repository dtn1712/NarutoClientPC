﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class BasicCharInfoRequestDTO
    {
        [Key("charId")]
        public long CharId { get; set; }
    }
}