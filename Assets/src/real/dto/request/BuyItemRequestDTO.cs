﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class BuyItemRequestDTO
    {
        [Key("menuId")]
        public sbyte menuId { get; set; }

        [Key("itemIndex")]
        public short itemIndex { get; set; }
    }
}