﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class CharMoveRequestDTO
    {
        [Key("x")]
        public short PosX { get; set; }

        [Key("y")]
        public short PosY { get; set; }
       
    }
}