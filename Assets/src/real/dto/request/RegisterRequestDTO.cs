﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class RegisterRequestDTO
    {
        [Key("username")]
        public string Username { get; set; }

        [Key("password")]
        public string Password { get; set; }

        [Key("clientVersion")]
        public string ClientVersion { get; set; }
    }
}