﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class MenuNpcRequestDTO
    {
        [Key("idNpc")]
        public long IdNpc { get; set; }

        [Key("idMenu")]
        public sbyte IdMenu { get; set; }
    }
}