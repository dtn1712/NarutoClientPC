﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class ChatRequestDTO
    {
        [Key("type")]
        public sbyte type { get; set; }

        [Key("text")]
        public string text { get; set; }

        [Key("playerChatId")]
        public long playerChatId { get; set; }
    }
}