﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class PickUpItemRequestDTO
    {
        [Key("type")]
        public sbyte type { get; set; }

        [Key("idItem")]
        public short idItem { get; set; }
    }
}