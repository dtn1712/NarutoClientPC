﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class StatusAttackRequestDTO
    {
        [Key("idPk")]
        public sbyte IdPk { get; set; }
    }
}