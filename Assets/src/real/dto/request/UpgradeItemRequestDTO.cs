﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class UpgradeItemRequestDTO
    {
        [Key("id")]
        public sbyte id { get; set; }

        [Key("subUpgrade")]
        public sbyte subUpgrade { get; set; }

        [Key("subTakeon")]
        public sbyte subTakeOn { get; set; }

        [Key("subTakeoff")]
        public sbyte subTakeOff { get; set; }

        [Key("indexItem")]
        public sbyte indexItem { get; set; }
    }
}