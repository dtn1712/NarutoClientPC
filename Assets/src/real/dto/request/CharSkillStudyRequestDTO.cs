﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class CharSkillStudyRequestDTO
    {
        [Key("type")]
        public sbyte Type { get; set; }

        [Key("idSkill")]
        public sbyte IdSkill { get; set; }
    }
}