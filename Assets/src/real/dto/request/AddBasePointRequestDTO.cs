﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class AddBasePointRequestDTO
    {
        [Key("type")]
        public sbyte type { get; set; }

        [Key("index")]
        public sbyte index { get; set; }

        [Key("value")]
        public short value { get; set; }
    }
}