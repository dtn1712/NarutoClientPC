﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class ClanRequestDTO
    {
        [Key("requestClan")]
        public sbyte requestClan { get; set; }

        [Key("requestName")]
        public sbyte requestName { get; set; }

        [Key("inviteClan")]
        public sbyte inviteClan { get; set; }

        [Key("inviteClanGolbal")]
        public sbyte inviteClanGolbal { get; set; }

        [Key("kick")]
        public sbyte kick { get; set; }

        [Key("deleteClan")]
        public sbyte deleteClan { get; set; }

        [Key("leave")]
        public sbyte leave { get; set; }

        [Key("getListLocal")]
        public sbyte getListLocal { get; set; }

        [Key("getListGolbal")]
        public sbyte getListGolbal { get; set; }

        [Key("name")]
        public string name { get; set; }

        [Key("idChar")]
        public long idChar { get; set; }

        [Key("idCharKicked")]
        public long idCharKicked { get; set; }
    }
}