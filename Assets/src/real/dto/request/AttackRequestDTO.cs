﻿using System.Collections.Generic;
using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class AttackRequestDTO
    {
        [Key("skillAttackId")]
        public sbyte skillAttackId { get; set; }

        [Key("targetType")]
        public sbyte targetType { get; set; }

        [Key("targetIds")]
        public List<long> targetIds { get; set; }
    }
}