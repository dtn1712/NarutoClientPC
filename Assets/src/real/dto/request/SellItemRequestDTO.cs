﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class SellItemRequestDTO
    {
        [Key("index")]
        public sbyte index { get; set; }
    }
}