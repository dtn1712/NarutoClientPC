﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class HoiSinhRequestDTO
    {
        [Key("type")]
        public sbyte Type { get; set; }
    }
}