﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class MonsterInfoRequestDTO
    {
        [Key("monsterId")]
        public short MonsterId { get; set; }
    }
}