﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class TradeItemRequestDTO
    {
        [Key("tradeRequestType")]
        public sbyte TradeRequestType { get; set; }

        [Key("playerJoinId")]
        public long PlayerJoinId { get; set; }

        [Key("moveItemType")]
        public sbyte MoveItemType { get; set; }

        [Key("itemId")]
        public short ItemId { get; set; }
        
        [Key("xu")]
        public long Xu { get; set; }
    }
}