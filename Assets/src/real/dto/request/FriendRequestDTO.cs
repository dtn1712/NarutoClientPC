﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class FriendRequestDTO
    {
        [Key("type")]
        public sbyte Type { get; set; }

        [Key("friendId")]
        public long FriendId { get; set; }
    }
}