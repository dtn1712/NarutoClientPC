﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class TakeOffItemRequestDTO
    {
        [Key("itemIndex")]
        public int ItemIndex { get; set; }
    }
}