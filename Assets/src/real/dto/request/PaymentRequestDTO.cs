﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class PaymentRequestDTO
    {
        [Key("code")]
        public string Code { get; set; }

        [Key("seri")]
        public string Seri { get; set; }

        [Key("typethe")]
        public sbyte TypeThe { get; set; }
    }
}