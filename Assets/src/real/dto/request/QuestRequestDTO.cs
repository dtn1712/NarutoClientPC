﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class QuestRequestDTO
    {
        [Key("actionType")]
        public sbyte actionType { get; set; }

        [Key("questType")]
        public string questType { get; set; }
    }
}