﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class UseItemRequestDTO
    {
        [Key("index")]
        public sbyte index { get; set; }
    }
}