﻿using System.Collections.Generic;
using MessagePack;

namespace src.real.dto.character
{
    [MessagePackObject]
    public class CharInventoryDTO
    {
        [Key("gold")]
        public long Gold { get; set; }

        [Key("xu")]
        public long Xu { get; set; }

        [Key("items")]
        public List<CharItemDTO> Items { get; set; }


        [MessagePackObject]
        public class CharItemDTO
        {
            [Key("positionIndex")]
            public int PositionIndex { get; set; }

            [Key("itemTemplateId")]
            public long ItemTemplateId { get; set; }

            [Key("level")]
            public int Level { get; set; }

            [Key("quantity")]
            public int Quantity { get; set; }

            [Key("valueBuff")]
            public int ValueBuff { get; set; }
            
            [Key("isUseInventory")]
            public bool IsUseInventory { get; set; }
        }
    }
}