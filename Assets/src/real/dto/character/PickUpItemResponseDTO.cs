﻿using MessagePack;

namespace src.real.dto.character
{
	[MessagePackObject]
    public class PickUpItemResponseDTO
    {

	    [Key("id")] 
	    public long Id { get; set; }

	    [Key("type")]
	    public byte Type { get; set; }

	    [Key("idItem")]
	    public short IdItem { get; set; }

    }

}

