﻿using MessagePack;

namespace src.real.dto.character
{
    [MessagePackObject]
    public class CharBasicInfoDTO
    {
        [Key("id")]
        public long Id { get; set; }
        
        [Key("charName")]
        public string CharName { get; set; }
        
        [Key("gender")]
        public sbyte Gender { get; set; }
        
        [Key("charClass")]
        public sbyte CharClass { get; set; }
        
        [Key("level")]
        public short Level { get; set; }
        
        [Key("headId")]
        public short HeadId { get; set; }
        
        [Key("bodyId")]
        public short BodyId { get; set; }
        
        [Key("legId")]
        public short LegId { get; set; }
    }
}