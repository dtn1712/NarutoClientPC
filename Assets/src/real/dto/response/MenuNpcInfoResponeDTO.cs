﻿using MessagePack;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class MenuNpcInfoResponeDTO
    {
        [Key("menuName")]
        public string menuName { get; set; }
    }
}