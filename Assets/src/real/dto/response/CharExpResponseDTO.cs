﻿using MessagePack;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class CharExpResponseDTO
    {
        [Key("charId")]
        public long CharId { get; set; }

        [Key("exp")]
        public long Exp { get; set; }
    }
}