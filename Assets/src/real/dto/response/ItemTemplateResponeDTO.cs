﻿using System.Collections.Generic;
using MessagePack;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class ItemTemplateResponeDTO
    {
        
        [Key("size")]
        public short size { get; set; }

        [Key("listItem")]
        public List<ItemTemplateInfoResponseDTO> listItem { get; set; }

    }
}