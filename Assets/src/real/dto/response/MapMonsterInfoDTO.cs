﻿using MessagePack;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class MapMonsterInfoDTO
    {
        [Key("id")]
        public sbyte id { get; set; }
        [Key("idTemplate")]
        public short idTemplate { get; set; }
        [Key("hp")]
        public int hp { get; set; }
        [Key("level")]
        public sbyte level { get; set; }
        [Key("maxHp")]
        public int maxHp { get; set; }
        [Key("monX")]
        public short monX { get; set; }
        [Key("monY")]
        public short monY { get; set; }
        [Key("minX")]
        public short minX { get; set; }
        [Key("minY")]
        public short minY { get; set; }
        [Key("maxX")]
        public short maxX { get; set; }
        [Key("maxY")]
        public short maxY { get; set; }
        [Key("isBoss")]
        public bool isBoss { get; set; }
        [Key("infoImageLength")]
        public int infoImageLength { get; set; }
        [Key("infoImage")]
        public byte[] infoImage { get; set; }
        [Key("infoFrameLength")]
        public int infoFrameLength { get; set; }
        [Key("infoFrame")]
        public byte[] infoFrame { get; set; }
    }
}