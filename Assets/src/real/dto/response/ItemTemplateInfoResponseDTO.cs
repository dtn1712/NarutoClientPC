﻿using MessagePack;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class ItemTemplateInfoResponseDTO
    {
        [Key("id")]
        public short id { get; set; }
        [Key("type")]
        public sbyte type { get; set; }
        [Key("gender")]
        public sbyte gender { get; set; }
        [Key("name")]
        public string name { get; set; }
        [Key("description")]
        public string description { get; set; }
        [Key("level")]
        public sbyte level { get; set; }
        [Key("icon")]
        public short icon { get; set; }
        [Key("isUpToUp")]
        public bool isUpToUp { get; set; }
        [Key("price")]
        public long price { get; set; }
        [Key("idClass")]
        public sbyte idClass { get; set; }
        [Key("country")]
        public sbyte country { get; set; }
        [Key("idPartBody")]
        public short idPartBody { get; set; }
        [Key("idPartLeg")]
        public short idPartLeg { get; set; }
        [Key("idPartHead")]
        public short idPartHead { get; set; }
        [Key("sellByGold")]
        public sbyte sellByGold { get; set; }
    }
}