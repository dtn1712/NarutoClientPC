﻿using MessagePack;
using System.Collections.Generic;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class NpcResponseDTO
    {
        [Key("size")]
        public byte size { get; set; }
        
        [Key("listNpc")]
        public  List<NpcInfoResponseDTO> listNpc { get; set; }

    }
}