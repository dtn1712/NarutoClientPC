﻿using MessagePack;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class MapTemplateInfoResponseDTO
    {

        [Key("id")]
        public short id { get; set; }

        [Key("tileId")]
        public short tileId { get; set; }

        [Key("name")]
        public string name { get; set; }

    }
}