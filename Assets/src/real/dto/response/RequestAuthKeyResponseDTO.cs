﻿using MessagePack;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class RequestAuthKeyResponseDTO
    {
        [Key("sessionId")]
        public string SessionId { get; set; }
        
        [Key("key")]
        public byte[] Key { get; set; }
    }
}