﻿using MessagePack;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class StatusAttackResponseDTO
    {
        [Key("id")]
        public long Id { get; set; }
        
        [Key("status")]
        public sbyte Status { get; set; }

    }
}