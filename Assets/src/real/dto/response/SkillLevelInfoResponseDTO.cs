﻿using MessagePack;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class SkillLevelInfoResponseDTO
    {
        [Key("lvChar")]
        public short lvChar { get; set; }
        [Key("mp")]
        public short mp { get; set; }
        [Key("coolDown")]
        public int coolDown { get; set; }
        [Key("timeBuffSkill")]
        public int timeBuffSkill { get; set; }
        [Key("pcSubEffAppear")]
        public sbyte pcSubEffAppear { get; set; }
        [Key("tExistSubEff")]
        public short tExistSubEff { get; set; }
        [Key("useHp")]
        public short useHp { get; set; }
        [Key("useMp")]
        public short useMp { get; set; }
        [Key("ntarget")]
        public sbyte ntarget { get; set; }
        [Key("range")]
        public short range { get; set; }
        [Key("dam")]
        public short dam { get; set; }
    }
}