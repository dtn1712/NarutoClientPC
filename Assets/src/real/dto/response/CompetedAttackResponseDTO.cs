﻿using MessagePack;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class CompetedAttackResponseDTO
    {
        [Key("type")]
        public sbyte Type { get; set; }

        [Key("playerInvitedId")]
        public long PlayerInvitedId { get; set; }
        
        [Key("inviteInfo")]
        public string InviteInfo { get; set; }
    }
}