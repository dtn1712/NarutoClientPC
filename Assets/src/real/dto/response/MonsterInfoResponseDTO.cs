﻿using MessagePack;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class MonsterInfoResponseDTO
    {
        [Key("id")]
        public long id { get; set; }
        
        [Key("name")]
        public string name { get; set; }
        
        [Key("maxHp")]
        public int maxHp { get; set; }
        
        [Key("hp")]
        public int hp { get; set; }
    }
}