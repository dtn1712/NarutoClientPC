﻿using MessagePack;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class MonsterPositionResponseDTO
    {
        [Key("type")]
        public byte type { get; set; }

        [Key("id")]
        public short id { get; set; }
        
        [Key("monX")]    
        public short monX { get; set; }
        
        [Key("monY")]
        public short monY { get; set; }
        
        [Key("name")]
        public string name { get; set; }
        
        [Key("hp")]
        public int hp { get; set; }
        
    }
}