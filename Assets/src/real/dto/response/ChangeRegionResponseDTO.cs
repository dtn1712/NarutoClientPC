﻿using MessagePack;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class ChangeRegionResponseDTO
    {
        [Key("idRegion")]
        public sbyte idRegion { get; set; }

        [Key("charX")]
        public short charX { get; set; }

        [Key("charY")]
        public short charY { get; set; }
    }
}