﻿using System.Collections;
using System.Collections.Generic;
using MessagePack;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class ShopInfoResponeDTO
    {
        [Key("idMenu")]
        public byte IdMenu { get; set; }
        
        [Key("size")]
        public short Size { get; set; }
        
        [Key("listId")]
        public List<short> ListId { get; set; }

    }
}