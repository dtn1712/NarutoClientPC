﻿using MessagePack;
using System.Collections.Generic;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class SKillCharResponeDTO
    {
        [Key("size")]
        public short size { get; set; }

        [Key("idSkill")]
        public sbyte idSkill { get; set; }

        [Key("idIcon")]
        public short idIcon { get; set; }
        
        [Key("idIconShort")]
        public short idIconShort { get; set; }
        
        [Key("nameSkill")]
        public string nameSkill { get; set; }
        
        [Key("type")]
        public sbyte type { get; set; }
        
        [Key("description")]
        public string description { get; set; }
        
        [Key("ntarget")]
        public sbyte ntarget { get; set; }
        
        [Key("typeBuff")]
        public sbyte typeBuff { get; set; }
        
        [Key("subEff")]
        public sbyte subEff { get; set; }
        
        [Key("rangeaeo")]
        public short rangeaeo { get; set; }
        
        [Key("idEffect")]
        public short idEffect { get; set; }
        
        [Key("length")]
        public sbyte length { get; set; }
        
        [Key("listLevel")]
        public List<SkillLevelInfoResponseDTO> listLevel;
        
    }
}