﻿using MessagePack;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class BossAppearResponseDTO
    {
        [Key("isTurnOnBoss")]
        public bool isTurnOnBoss { get; set; }

        [Key("idAppear")]
        public short idAppear { get; set; }
    }
}