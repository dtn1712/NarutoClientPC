﻿using MessagePack;
 using System.Collections.Generic;
 namespace src.real.dto.response
 {
     [MessagePackObject]
     public class MapHtDataDTO
     {
         [Key("size")]
         public sbyte size { get; set; }
         [Key("listDataKeySet")]
         public List<MapHtDataKeySetDTO> listDataKeySet { get; set; }
     }
 }