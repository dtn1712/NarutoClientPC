﻿using MessagePack;

namespace src.real.dto.response.message
{
    [MessagePackObject]
    public class ServerTopRightResponseDTO
    {
        [Key("info")]
        public string info { get; set; }
    }
}