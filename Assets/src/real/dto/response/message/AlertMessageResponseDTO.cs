﻿using MessagePack;
namespace src.real.dto.response.message
{
    [MessagePackObject]
    public class AlertMessageResponseDTO
    {
        [Key("info")]
        public string info { get; set; }
    }
}