﻿using MessagePack;

namespace src.real.dto.response.message
{
    [MessagePackObject]
    public class ServerNotificationResponseDTO
    {

        [Key("content")]
        public string Content { get; set; }
    }
}