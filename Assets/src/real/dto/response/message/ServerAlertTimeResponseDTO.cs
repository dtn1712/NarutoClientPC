﻿using MessagePack;
namespace src.real.dto.response.message
{
    [MessagePackObject]
    public class ServerAlertTimeResponseDTO
    {
        [Key("info")]
        public string info { get; set; }

        [Key("time")]
        public int time { get; set; }
    }
}