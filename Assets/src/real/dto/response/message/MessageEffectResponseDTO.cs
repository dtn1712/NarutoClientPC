﻿using MessagePack;

namespace src.real.dto.response.message
{
    [MessagePackObject]
    public class MessageEffectResponseDTO
    {
        [Key("id")]
        public long id { get; set; }
        [Key("type")]
        public byte type { get; set; }
        [Key("info")]
        public string info { get; set; }
    }
}