﻿using MessagePack;
using System.Collections.Generic;
using src.real.dto.game;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class MenuShopResponseDTO
    {
        [Key("size")]
        public byte size { get; set; }

        [Key("listKey")]
        public List<MenuKeyDTO> listKey { get; set; }
    }
}