﻿using MessagePack;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class ChatResponseDTO
    {
        [Key("type")]
        public byte type { get; set; }

        [Key("playerSendId")]
        public long playerSendId { get; set; }
        
        [Key("playerSendName")]
        public string playerSendName { get; set; }
        
        [Key("playerReceiveId")]
        public long playerReceiveId { get; set; }
        
        [Key("playerReceiveName")]
        public string playerReceiveName { get; set; }
        
        [Key("text")]
        public string text { get; set; }
    }
}