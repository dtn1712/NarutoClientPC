﻿using System.Collections.Generic;
using MessagePack;
using src.real.dto.game;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class PaymentResponseDTO
    {  
        [Key("success")]
        public bool success { get; set; }
        
        [Key("errorMessage")]
        public string errorMessage { get; set; }
        
        [Key("chargedAmount")]
        public long chargedAmount { get; set; }
        
        [Key("totalNewAmount")]
        public long totalNewAmount { get; set; }
        
    }

    
}