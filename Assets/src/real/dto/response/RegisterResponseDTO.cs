﻿using MessagePack;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class RegisterResponseDTO
    {
        
        [Key("info")]
        public string Info { get; set; }
    }
}