﻿using MessagePack;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class LineMapInfoResponseDTO
    {
        [Key("mapTemplateId")]
        public short mapTemplateId { get; set; }

        [Key("minX")]
        public short minX { get; set; }
        
        [Key("minY")]
        public short minY { get; set; }
        
        [Key("maxX")]
        public short maxX { get; set; }
        
        [Key("maxY")]
        public short maxY { get; set; }
    }
}