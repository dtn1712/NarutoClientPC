﻿using MessagePack;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class DropKageItemResponseDTO
    {
        [Key("idTemplate")]
        public int idTemplate { get; set; }

        [Key("x")]
        public int x { get; set; }
        
        [Key("y")]
        public int y { get; set; }
        
        [Key("id")]
        public long id { get; set; }


    }
}