﻿using MessagePack;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class DropItemResponseDTO
    {
        [Key("typeItem")]
        public sbyte typeItem { get; set; }

        [Key("typeActorLive")]
        public byte typeActorLive { get; set; }

        [Key("actorLiveId")]
        public short actorLiveId { get; set; }

        [Key("idTemplate")]
        public short idTemplate { get; set; }

        [Key("id")]
        public short id { get; set; }
    }
}