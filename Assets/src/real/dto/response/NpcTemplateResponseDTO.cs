﻿using System.Collections.Generic;
using MessagePack;
using src.real.dto.game;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class NpcTemplateResponseDTO
    {
        [Key("npcTemplates")]
        public List<NpcTemplateDTO> NpcTemplates { get; set; }
    }
}