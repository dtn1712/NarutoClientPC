﻿using MessagePack;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class PlayerOutResponseDTO
    {
        [Key("id")]
        public long id { get; set; }
    }
}