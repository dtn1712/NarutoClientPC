﻿using MessagePack;
namespace src.real.dto.response.FriendDTO
{
    [MessagePackObject]
    public class FriendResponseDTO
    {
        [Key("type")]
        public sbyte type { get; set; }

        [Key("id")]
        public long id { get; set; }
    }
}