﻿using MessagePack;
namespace src.real.dto.response.FriendDTO
{
    [MessagePackObject]
    public class FriendInfoDTO
    {
        [Key("id")]
        public long id { get; set; }
        [Key("isOnline")]
        public bool isOnline { get; set; }
        [Key("chatCase")]
        public short chatCase { get; set; }
        [Key("charName")]
        public string charName { get; set; }
        [Key("charLevel")]
        public short charLevel { get; set; }

    }
}