﻿using MessagePack;
namespace src.real.dto.response.FriendDTO
{
    [MessagePackObject]
    public class InviteFriendResponseDTO
    {
        [Key("type")]
        public byte type { get; set; }

        [Key("id")]
        public long id { get; set; }
        
        [Key("content")]
        public string content { get; set; }
    }
}