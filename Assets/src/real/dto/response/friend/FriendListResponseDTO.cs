﻿using System.Collections.Generic;
using MessagePack;
namespace src.real.dto.response.FriendDTO
{
    [MessagePackObject]
    public class FriendListResponseDTO
    {
        [Key("size")]
        public byte size { get; set; }

        [Key("listFriend")]
        public List<FriendInfoDTO> listFriend { get; set; }
    }
}