﻿using MessagePack;
namespace src.real.dto.response.FriendDTO
{
    [MessagePackObject]
    public class RemoveFriendRequestDTO
    {
        [Key("id")]
        public long id { get; set; }
    }
}