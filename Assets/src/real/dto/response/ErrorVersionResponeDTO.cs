﻿using System;
using MessagePack;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class ErrorVersionResponeDTO
    {
        [Key("link")]
        public string Link { get; set; }
    }
}