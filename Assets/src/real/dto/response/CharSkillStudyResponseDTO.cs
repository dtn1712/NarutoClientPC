﻿using MessagePack;
using System.Collections.Generic;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class CharSkillStudyResponseDTO
    {
        [Key("size")]
        public short size { get; set; }
        
        [Key("listSkill")]
        public List<SkillInfoStudyDTO> listSkill { get; set; }

    }
}