﻿using MessagePack;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class CharReviveResponeDTO
    {
        [Key("id")]
        public long id { get; set; }

        [Key("type")]
        public byte type { get; set; }
        
        [Key("hp")]
        public short hp { get; set; }
        
        [Key("mp")]
        public short mp { get; set; }
    }
}