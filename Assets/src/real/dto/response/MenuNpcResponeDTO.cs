﻿using System.Collections.Generic;
using MessagePack;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class MenuNpcResponeDTO
    {
        [Key("idNpc")]
        public int IdNpc { get; set; }

        [Key("length")]
        public sbyte Length { get; set; }

        [Key("listMenu")]
        public List<string> ListMenu { get; set; }
        
    }
}