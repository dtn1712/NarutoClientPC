﻿using System;
using System.Collections.Generic;
using MessagePack;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class RegionInfoResponseDTO
    {
        
        [Key("listRegion")]
        public List<SByte> ListRegion { get; set; }
    }
}