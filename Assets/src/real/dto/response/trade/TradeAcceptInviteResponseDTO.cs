﻿using MessagePack;
namespace src.real.dto.response.trade
{
    [MessagePackObject]
    public class TradeAcceptInviteResponseDTO
    {
        [Key("type")]
        public byte type { get; set; }

        [Key("id")]
        public long id { get; set; }
    }
}