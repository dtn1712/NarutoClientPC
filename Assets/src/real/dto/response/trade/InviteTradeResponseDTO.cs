﻿using MessagePack;
namespace src.real.dto.response.trade
{
    [MessagePackObject]
    public class InviteTradeResponseDTO
    {
        [Key("id")]
        public long id { get; set; }

        [Key("content")]
        public string content { get; set; }
    }
}