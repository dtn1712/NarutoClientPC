﻿using MessagePack;
namespace src.real.dto.response.trade
{
    [MessagePackObject]
    public class TradeLockResponseDTO
    {
        [Key("id")]
        public long id { get; set; }
    }
}