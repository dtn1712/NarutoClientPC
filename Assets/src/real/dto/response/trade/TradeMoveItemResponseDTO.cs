﻿using MessagePack;
namespace src.real.dto.response.trade
{
    [MessagePackObject]
    public class TradeMoveItemResponseDTO
    {
        [Key("typeItem")]
        public byte typeItem { get; set; }
        [Key("id")]
        public long id { get; set; }
        [Key("idItem")]
        public short idItem { get; set; }
        [Key("idTemplate")]
        public short idTemplate { get; set; }
    }
}