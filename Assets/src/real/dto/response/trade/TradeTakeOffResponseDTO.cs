﻿using MessagePack;
namespace src.real.dto.response.trade
{
    [MessagePackObject]
    public class TradeTakeOffResponseDTO
    {
        [Key("typeItem")]
        public byte typeItem { get; set; }
        [Key("id")]
        public long id { get; set; }
        [Key("itemId")]
        public short itemId { get; set; }
    }
}