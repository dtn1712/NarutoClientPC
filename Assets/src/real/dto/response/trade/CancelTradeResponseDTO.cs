﻿using MessagePack;
namespace src.real.dto.response.trade
{
    [MessagePackObject]
    public class CancelTradeResponseDTO
    {
        [Key("id")]
        public long id { get; set; }

        [Key("content")]
        public string content  {get; set; }
    }
}