﻿using MessagePack;
namespace src.real.dto.response.trade
{
    [MessagePackObject]
    public class TradeAcceptResponseDTO
    {
        [Key("id")]
        public long id;
       
        [Key("content")]
        public string content;
    }
}