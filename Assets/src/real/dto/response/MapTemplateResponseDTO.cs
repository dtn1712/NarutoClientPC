﻿using System.Collections.Generic;
using MessagePack;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class MapTemplateResponseDTO
    {
        [Key("size")]
        public short size { get; set; }

        [Key("listMapTemplate")]
        public List<MapTemplateInfoResponseDTO> listMapTemplate { get; set; }

    }
}