﻿using System.Collections.Generic;
using MessagePack;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class AttackResponseDTO
    {
        [Key("attackerId")]
        public long AttackerId { get; set; }
        
        [Key("attackerType")]
        public sbyte AttackerType { get; set; }
        
        [Key("targetType")]
        public sbyte TargetType { get; set; }
        
        [Key("targets")]
        public List<TargetDTO> Targets { get; set; }
        
        [Key("attackerMp")]
        public int AttackerMp { get; set; }
        
        [Key("skillAttackId")]
        public int SkillAttackId { get; set; }
        
        [Key("skillEffectId")]
        public int SkillEffectId { get; set; }

        
        [MessagePackObject]
        public class TargetDTO {
            
            [Key("id")]
            public long Id { get; set; }
            
            [Key("hp")]
            public int Hp { get; set; }
            
            [Key("hitDamage")]
            public int HitDamage { get; set; }
        }
    }
}