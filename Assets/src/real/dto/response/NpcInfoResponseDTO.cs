﻿using MessagePack;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class NpcInfoResponseDTO
    {

        [Key("id")]
        public short id { get; set; }
        [Key("npcX")]
        public short npcX { get; set; }
        [Key("npcY")]
        public short npcY { get; set; }
        [Key("templateId")]
        public short templateId { get; set; }
    }
}