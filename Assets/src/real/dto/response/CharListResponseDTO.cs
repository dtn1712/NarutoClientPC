﻿using System.Collections.Generic;
using MessagePack;
using src.real.dto.character;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class CharListResponseDTO
    {
        [Key("chars")]
        public List<CharBasicInfoDTO> Chars { get; set; }
    }
}