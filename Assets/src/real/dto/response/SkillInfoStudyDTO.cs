﻿using MessagePack;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class SkillInfoStudyDTO
    {
        [Key("idSkill")]
        public sbyte idSkill { get; set; }

        [Key("idTemplate")]
        public short idTemplate { get; set; }

        [Key("level")]
        public short level { get; set; }
    }
}