﻿using MessagePack;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class MapItemDTO
    {
        [Key("id")]
        public int id { get; set; }

        [Key("idTemplate")]
        public short idTemplate { get; set; }
        
        [Key("itemX")]
        public short itemX { get; set; }
        
        [Key("itemY")]
        public short itemY { get; set; }
    }
}