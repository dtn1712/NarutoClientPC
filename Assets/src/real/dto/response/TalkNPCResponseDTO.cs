﻿using MessagePack;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class TalkNPCResponseDTO
    {
        [Key("contentChat")]
        public string ContentChat { get; set; }
    }
}