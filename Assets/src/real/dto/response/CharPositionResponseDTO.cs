﻿using MessagePack;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class CharPositionResponseDTO
    {
        [Key("id")]
        public long id { get; set; }
        [Key("x")]
        public short x { get; set; }
        [Key("y")]
        public short y { get; set; }
        [Key("name")]
        public string name { get; set; }
        [Key("statusPK")]
        public sbyte statusPK { get; set; }

    }
}