﻿using MessagePack;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class CharInfoResponseDTO
    {
        [Key("id")]
        public long id { get; set; }

        [Key("maxHp")]
        public int maxHp { get; set; }

        [Key("hp")]
        public int hp { get; set; }

        [Key("level")]
        public short level { get; set; }

        [Key("isJunchuuriki")]
        public byte isJunchuuriki { get; set; }

        [Key("isNinjaExile")]
        public byte isNinjaExile { get; set; }

        [Key("globalClan")]
        public long globalClan { get; set; }

        [Key("localClan")]
        public long localClan { get; set; }
    }
}