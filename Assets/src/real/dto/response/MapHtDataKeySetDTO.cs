﻿using System;
using MessagePack;
using System.Collections.Generic;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class MapHtDataKeySetDTO
    {
        [Key("keyType")]
        public int keyType { get; set; }

        [Key("length")]
        public byte length { get; set; }

        [Key("data")]
        public List<Byte> data { get; set; }
    }
}