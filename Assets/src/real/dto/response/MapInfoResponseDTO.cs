﻿using MessagePack;
using System.Collections.Generic;
namespace src.real.dto.response
{
    [MessagePackObject]
    public class MapInfoResponseDTO
    {
        [Key("id")]
        public short id { get; set; }
        [Key("region")]
        public sbyte region { get; set; }
        [Key("totalLine")]
        public short nLine { get; set; }
        [Key("listLineMap")]
        public List<LineMapInfoResponseDTO> listLineMap { get; set; }
        [Key("charX")]
        public short charX { get; set; }
        [Key("charY")]
        public short charY { get; set; }
        [Key("mapTempId")]
        public byte mapTempId { get; set; }
        [Key("listMapHtData")]
        public List<MapHtDataDTO> listMapHtData { get; set; } // 1 
        [Key("monsterSize")]
        public byte monsterSize { get; set; }
        [Key("listMonster")]
        public List<MapMonsterInfoDTO> listMonster { get; set; }
        [Key("itemMapSize")]
        public short itemMapSize { get; set; }
        [Key("listItem")]
        public List<MapItemDTO> listItem { get; set; }
       
    }
}