﻿using System.Collections.Generic;
using MessagePack;
using src.real.dto.game;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class GetPricingResponseDTO
    {
        [Key("pricingData")]
        public List<ItemPaymentDTO> pricingData { get; set; }
    }
}