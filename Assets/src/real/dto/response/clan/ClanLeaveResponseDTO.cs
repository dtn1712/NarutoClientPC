﻿using MessagePack;
namespace src.real.dto.response.clan
{
    [MessagePackObject]
    public class ClanLeaveResponseDTO
    {
        [Key("content")]
        public string content { get; set; }
    }
}