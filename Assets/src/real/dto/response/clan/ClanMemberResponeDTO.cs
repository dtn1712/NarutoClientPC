﻿using MessagePack;
namespace src.real.dto.response.clan
{
    [MessagePackObject]
    public class ClanMemberResponeDTO
    {
        [Key("name")]
        public string name { get; set; }

        [Key("id")]
        public long id { get; set; }
        
        [Key("level")]
        public short level { get; set; }
        
        [Key("idTemplate")]
        public short idTemplate { get; set; }
    }
    
}