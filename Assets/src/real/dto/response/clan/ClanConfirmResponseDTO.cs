﻿using MessagePack;
namespace src.real.dto.response.clan
{
    [MessagePackObject]
    public class ClanConfirmResponseDTO
    {
        [Key("type")]
        public short type { get; set; }

        [Key("id")]
        public long id { get; set; }

        [Key("content")]
        public string content { get; set; }
    }
}