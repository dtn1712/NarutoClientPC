﻿using MessagePack;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class ActorDeadResponseDTO
    {
        [Key("actorType")]
        public sbyte ActorType { get; set; }
        
        [Key("id")]
        public long Id { get; set; }
    }
}