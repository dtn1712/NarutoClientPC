﻿using MessagePack;

namespace src.real.dto.response
{
    [MessagePackObject]
    public class LoginResponseDTO
    {

        [Key("njArrow")]
        public byte[] njArrow { get; set; }
        
        [Key("njEffect")]
        public byte[] njEffect { get; set; }
        
        [Key("njImages")]
        public byte[] njImages { get; set; }
        
        [Key("njPart")]
        public byte[] njPart { get; set; }
        
        [Key("njSkill")]
        public byte[] njSkill { get; set; }
    }
}