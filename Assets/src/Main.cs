using System;
using System.Threading;
using CI.HttpClient;
using src.lib;
using src.main;
using src.network;
using UnityEngine;

namespace src
{
    public class Main : MonoBehaviour
    {
        private static Main _main;
        private static MGraphics _graphic;
        
        public static string MainThreadName;

        private static bool _started; //isExit dung cho windows

        // hai cho quan trong can luu y khi build ban client moi -----------------------------
        public static bool isPC, isWindowsPhone, isIPhone, isSprite;

        public static bool isQuitApp, isReSume;
        public static string IMEI;
        public static int level;
        public Vector2 lastMousePos;
        private bool isRun;
        private int cout;

        private static Material lineMaterial;

        void Start()
        {
            if (_started) return;
            if (Thread.CurrentThread.Name != "Main")
                Thread.CurrentThread.Name = "Main";
                
            MainThreadName = Thread.CurrentThread.Name;
            isPC = true; //nếu build cho Iphone thì đóng chỗ này lại-----------------
            _started = true;
            level = Rms.loadRMSInt("levelScreenKN");
            Screen.orientation = ScreenOrientation.Landscape;
            Application.runInBackground = true;
            Application.targetFrameRate = 25;
            Screen.SetResolution(Screen.width, Screen.height, false); //enter res
            _graphic = new MGraphics();

            lastMousePos = new Vector2();
            Debug.Log("REAL SIZE OF PHONE -------> "+Screen.width+" "+Screen.height);
            
            SetupServerIP();
        }

        void OnGUI()
        {
            if (cout < 10)
                return;
            
            CheckInput();
            Session.GetInstance().Update();
            
            if (Event.current.type.Equals(EventType.Repaint))
            {
                GameMidlet.GetInstance().GameCanvas.paint(_graphic);
                _graphic.reset();
            }
        }

        private void CheckInput()
        {
            if (Input.GetMouseButtonDown(0))
            {    
                Vector3 touch = Input.mousePosition;

                GameMidlet.GetInstance().GameCanvas.pointerPressed((int) (touch.x),
                    (int) (Screen.height - touch.y) + MGraphics.addYWhenOpenKeyBoard * MGraphics.zoomLevel);
                lastMousePos.x = touch.x;
                lastMousePos.y = touch.y + MGraphics.addYWhenOpenKeyBoard * MGraphics.zoomLevel;
            }
            if (Input.GetMouseButton(0))
            {
                Vector3 touch = Input.mousePosition;

                GameMidlet.GetInstance().GameCanvas.pointerDragged((int) touch.x,
                    (int) (Screen.height - touch.y) + MGraphics.addYWhenOpenKeyBoard * MGraphics.zoomLevel);
                lastMousePos.x = touch.x;
                lastMousePos.y = touch.y + MGraphics.addYWhenOpenKeyBoard * MGraphics.zoomLevel;
            }
            if (Input.GetMouseButtonUp(0))
            {
                Vector3 touch = Input.mousePosition;
                lastMousePos.x = touch.x;
                lastMousePos.y = touch.y + MGraphics.addYWhenOpenKeyBoard * MGraphics.zoomLevel;

                GameMidlet.GetInstance().GameCanvas.pointerReleased((int) touch.x,
                    (int) (Screen.height - touch.y) + MGraphics.addYWhenOpenKeyBoard * MGraphics.zoomLevel);
            }


            if (Input.anyKeyDown && Event.current.type == EventType.KeyDown)
            {
                int keyCode = MyKeyMap.map(Event.current.keyCode);
                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                {
                    switch (Event.current.keyCode)
                    {
                        case KeyCode.Alpha2:
                            keyCode = 64;
                            break;
                        case KeyCode.Minus:
                            keyCode = 95;
                            break;
                    }
                }

                if (keyCode != 0)
                    GameMidlet.GetInstance().GameCanvas.KeyPressed(keyCode);
            }
            if (Event.current.type == EventType.KeyUp)
            {
                int keyCode = MyKeyMap.map(Event.current.keyCode);
                if (keyCode != 0)


                    GameMidlet.GetInstance().GameCanvas.keyReleasedd(keyCode);
            }   
        }

        void OnApplicationQuit()
        {
            Session.GetInstance().Disconnect();
            if (isPC)
            {
                Application.Quit();
            }
        }

        

        void OnApplicationPause(bool paused)
        {
            isReSume = !paused;
            // khi build cho may mac dong khoi lenh nay lai 
//            if (TouchScreenKeyboard.visible)
//            {
//                TField.kb.active = false;
//                TField.kb = null;
//            }

            if (TField.kb != null)
            {
                TField.kb.active = false;
                TField.kb = null;
            }

            if (isQuitApp)
                Application.Quit();
        }
       
        
        void FixedUpdate()
        {
           
            
            Rms.update();
            cout++;
            if (cout < 10)
                return;
            if (!isRun)
            {
                isRun = true;
//                UnityEngine.Screen.orientation = ScreenOrientation.Landscape;
//                Application.runInBackground = true;
//                Application.targetFrameRate = 30;
                useGUILayout = false;
                if (_main == null)
                    _main = this;


                IMEI = SystemInfo.deviceUniqueIdentifier;
                isIPhone = isPC != true;

                if (isPC)
                    Screen.fullScreen = false;

                _graphic.CreateLineMaterial();
                if (MGraphics.zoomLevel == 1 && !isWindowsPhone)
                {
                    isSprite = true;
                }
            }
            GameMidlet.GetInstance().GameCanvas.update();
            Image.update();
        }

        private void SetupServerIP()
        {
            HttpClient httpClient = new HttpClient();
            httpClient.GetString(new Uri(Constants.WEBSITE_URL + "getserverip.php"), (r) =>
            {
                if (r.IsSuccessStatusCode && r.Data != null)
                {
                    GameMidlet.IP = r.Data;
                }
            });
        }
    }
}