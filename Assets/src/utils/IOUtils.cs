﻿using System;
using src.lib;
using src.network;
using UnityEngine;

namespace src.utils
{
    public static class IoUtils
    {
        public static void WriteMessage(sbyte cmd, byte[] data)
        {
            Session.GetInstance().SendMessage(new Message(cmd, data));
        }

        public static void WriteMessage(sbyte cmd)
        {
            Session.GetInstance().SendMessage(new Message(cmd));
        }

        public static byte[] ReadMessage(Message message)
        {
            return message.Data;
        }
    }
}